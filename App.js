import React from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import logger from "redux-logger";
import Goapp from "./src/";
import reducer from "./src/Reducer";
import rootSaga from "./src/Saga";

const sagaMiddleware = createSagaMiddleware();
var middlewares = [sagaMiddleware];
console.log("__DEV__", __DEV__);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

if (__DEV__) {
	middlewares = [...middlewares, logger];
}

const store = createStore(
	reducer(),
	composeEnhancers(applyMiddleware(...middlewares))
);
export const runSaga = sagaMiddleware.run;
runSaga(rootSaga);

store.asyncReducers = {};

export const getNewReducer = newModuleInfo => {
	store.asyncReducers[newModuleInfo.name] = newModuleInfo.reducer;
	store.replaceReducer(reducer(store.asyncReducers));
};

export const removeExistingReducer = moduleName => {
	store.asyncReducers[moduleName] = null;
	store.replaceReducer(reducer(store.asyncReducers));
};

console.disableYellowBox = true;
global.Blob = null;

const App = props => (
	<Provider store={store}>
		<Goapp {...props} />
	</Provider>
);

export default App;
