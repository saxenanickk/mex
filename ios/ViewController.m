//
//  ViewController.m
//  SwiggyAddress
//
//  Created by shashi shekhar on 23/07/19.
//  Copyright © 2019 quantcommune. All rights reserved.
//

#import "ViewController.h"
#import "Mixpanel.h"
#import "ReactNativeConfig.h"

@interface ViewController ()
@property(strong,nonatomic) UIActivityIndicatorView *spinner;

@end

@implementation ViewController

float height;
float width;

- (void)viewDidLoad {
    [super viewDidLoad];
	
    _baseURL = [ReactNativeConfig envFor:@"MEX_ADDRESS"];
    _confirmOrderURL = [ReactNativeConfig envFor:@"SWIGGY_CONFIRM_ORDER"];
	
    _prePopulateData = [NSArray arrayWithObjects:@"company_name",@"floor",@"campus",@"building", nil];
    // initialize width and height according to the device
    height = self.view.bounds.size.height;
    width = self.view.bounds.size.width;
    _companyName = @"";
    _floorName = @"";
    _buildingName = @"";
    _campusName = @"";
		_orderCreated = @"Not Started";
    NSLog(@"height %f and width %f",height,width);
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor grayColor];
    [self addMainView];
    [self getAsyncAddress];
}

-(void)resetViewState{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->_companyInput setText:self->_companyName];
        [self->_floorInput setText:self->_floorName];
        [self->_buildingInput setText:self->_buildingName];
        [self->_campusInput setText:self->_campusName];
        [self.view addSubview:self->_mainView];
        [self->_confirmButton addTarget:self action:@selector(confirmClicked) forControlEvents:UIControlEventTouchUpInside];
        [self->_spinner stopAnimating];
    });
}

-(void) addMainView{
    _mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(width * 0.02, height * 0.04, width * 0.96, height* 0.92)];
    _mainView.scrollEnabled = YES;
    _mainView.contentSize = CGSizeMake(width * 0.96, height * 1.5);
    _mainView.showsVerticalScrollIndicator = NO;
    _mainView.backgroundColor = [UIColor whiteColor];
    _mainView.layer.cornerRadius = 10;
//    [self.view addSubview:_mainView];
    
    [self addHeaderText];
    
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.spinner setCenter:[_mainView center]];
    
    [self.view addSubview:self.spinner];
    [self.spinner startAnimating];
}

-(void) addHeaderText{
    _headerText = [[UILabel alloc] initWithFrame:CGRectMake(width * 0.035,height * 0.015, width * 0.8, height * 0.05)];
    _headerText.text = @"MEX. Swiggy Desk Delivery";
    _headerText.font = [UIFont fontWithName:@"Avenir-Heavy" size:height / 40];
    _headerText.layer.borderWidth = 0;
    _headerText.textColor = [UIColor colorWithRed:(235/255.0) green:(130/255.0) blue:(88/255.0) alpha:1] ;
    [self.mainView addSubview:_headerText];
  _closeButton = [[UIButton alloc] initWithFrame:CGRectMake(width * 0.875, height * 0.029, width*0.04, width * 0.04)];
    UIImage *img_x = [UIImage imageNamed:@"X"];
    [_closeButton setImage:img_x forState:UIControlStateNormal];
    [_closeButton addTarget:self action:@selector(closeButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:_closeButton];
    //create secondary header text and add it to subview
    UILabel *sheaderText = [[UILabel alloc] initWithFrame:CGRectMake(width * 0.035 ,height * 0.07, width * 0.89, height * 0.09)];
    sheaderText.numberOfLines = 3;
    sheaderText.text = @"Thanks for ordering through MEX. Please help us with the information below to enable desk delivery.";
    sheaderText.font = [UIFont fontWithName:@"Avenir" size:height / 55];
    sheaderText.textColor = [UIColor colorWithRed:(160/255.0) green:(160/255.0) blue:(160/255.0) alpha:1] ;
    [self.mainView addSubview:sheaderText];
    
    //create text field with label
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(width * 0.035, height  * 0.15, width * 0.8, height * 0.05)];
    label.text = @"Company Name";
    label.font = [UIFont fontWithName:@"Avenir" size:height / 45];
    label.textColor = [UIColor colorWithRed:(160/255.0) green:(160/255.0) blue:(160/255.0) alpha:1] ;
    [self.mainView addSubview:label];
    //add company text input
    _companyInput = [[UITextField alloc] initWithFrame:CGRectMake(width * 0.035, height  * 0.21, width * 0.89, height * 0.05)];
    [self customizeTextInput:_companyInput];
    [self.mainView addSubview:_companyInput];
    
    //add floor label
    label = [[UILabel alloc] initWithFrame:CGRectMake(width * 0.035, height  * 0.28, width * 0.44, height * 0.05)];
    label.text = @"Floor No.";
    label.font = [UIFont fontWithName:@"Avenir" size:height / 45];
    label.textColor = [UIColor colorWithRed:(160/255.0) green:(160/255.0) blue:(160/255.0) alpha:1] ;
    [self.mainView addSubview:label];
    //add floor number Text input
    _floorInput =[[UITextField alloc] initWithFrame:CGRectMake(width * 0.035, height  * 0.34, width * 0.42, height * 0.05)];
    [self customizeTextInput:_floorInput];
    [self.mainView addSubview:_floorInput];
    
    //add building label
    label = [[UILabel alloc] initWithFrame:CGRectMake(width * 0.5, height  * 0.28, width * 0.42, height * 0.05)];
    label.text = @"Building";
    label.font = [UIFont fontWithName:@"Avenir" size:height / 45];
    label.textColor = [UIColor colorWithRed:(160/255.0) green:(160/255.0) blue:(160/255.0) alpha:1] ;
    [self.mainView addSubview:label];
    //add building number Text input
    _buildingInput =[[UITextField alloc] initWithFrame:CGRectMake(width * 0.5, height  * 0.34, width * 0.42, height * 0.05)];
    [self customizeTextInput:_buildingInput];
    [self.mainView addSubview:_buildingInput];
    
    //create text field with label
    label = [[UILabel alloc] initWithFrame:CGRectMake(width * 0.035, height  * 0.41, width * 0.89, height * 0.05)];
    label.text = @"Campus";
    label.font = [UIFont fontWithName:@"Avenir" size:height / 45];
    label.textColor = [UIColor colorWithRed:(160/255.0) green:(160/255.0) blue:(160/255.0) alpha:1] ;
    [self.mainView addSubview:label];
    //add campus text input
    _campusInput = [[UITextField alloc] initWithFrame:CGRectMake(width * 0.035, height  * 0.47, width * 0.89, height * 0.05)];
    [self customizeTextInput:_campusInput];
    [self.mainView addSubview:_campusInput];
    
    //add confirm button
    _confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(width * 0.275, height * 0.56, width * 0.4, height * 0.05)];
    _confirmButton.layer.cornerRadius = 5;
    NSDictionary *dict1 = @{
                            NSForegroundColorAttributeName:[UIColor colorWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:1],
                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Heavy" size:height / 45]};
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"CONFIRM" attributes:dict1];
    [self.confirmButton setAttributedTitle:attributedString forState:UIControlStateNormal];
//    [_confirmButton addTarget:self action:@selector(confirmClicked) forControlEvents:UIControlEventTouchUpInside];
    _confirmButton.backgroundColor = [UIColor colorWithRed:(235/255.0) green:(130/255.0) blue:(88/255.0) alpha:1] ;
    [self.mainView addSubview:_confirmButton];
    
    //add no desk delivery button
    _noDeskButton = [[UIButton alloc] initWithFrame:CGRectMake(width * 0.075, height * 0.63, width * 0.8, height * 0.05)];
    dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),
                            NSForegroundColorAttributeName:[UIColor colorWithRed:(47/255.0) green:(157/255.0) blue:(212/255.0) alpha:1],
                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Medium" size:height / 45]};
    attributedString = [[NSAttributedString alloc] initWithString:@"No Desk Delivery Required" attributes:dict1];
    [self.noDeskButton setAttributedTitle:attributedString forState:UIControlStateNormal];
    [_noDeskButton addTarget:self action:@selector(noDeskButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:_noDeskButton];
    //add suggestion label
    _suggestionButton = [[UIButton alloc] initWithFrame:CGRectMake(width * 0.08, height * 0.7, width * 0.8, height * 0.05)];
    dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),
              NSForegroundColorAttributeName:[UIColor colorWithRed:(160/255.0) green:(160/255.0) blue:(160/255.0) alpha:1],
              NSFontAttributeName:[UIFont fontWithName:@"Avenir-Medium" size:height / 45]};
    attributedString = [[NSAttributedString alloc] initWithString:@"How it Works?" attributes:dict1];
    [self.suggestionButton setAttributedTitle:attributedString forState:UIControlStateNormal];
    [self.mainView addSubview:_suggestionButton];
    
    //add hint Image
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(width * 0.035, height * 0.775, width * 0.89, height * 0.4)];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"swiggy_desk"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.clipsToBounds = YES;
    UIImage *img_desk = [UIImage imageNamed:@"swiggy_desk"];
    imageView.layer.borderWidth = 0;
    [imageView setImage:img_desk];
    [self.mainView addSubview:imageView];
    //add Notes
    UILabel *label1;
    label1 = [[UILabel alloc] initWithFrame:CGRectMake(width * 0.035, height * 1.2, width * 0.89, height * 0.04)];
    label1.text = @"Note:";
    label1.font = [UIFont fontWithName:@"Avenir-Heavy" size:height / 45];
    label1.textColor = [UIColor colorWithRed:(160/255.0) green:(160/255.0) blue:(160/255.0) alpha:1] ;
    
    [self.mainView addSubview:label1];
    //add first Note
    label1 = [[UILabel alloc] initWithFrame:CGRectMake(width * 0.035, height * 1.24, width * 0.89, height * 0.05)];
    label1.text = @"\u2022 Our Delivery Executive(DE) would come to your reception and contact you.";
    label1.font = [UIFont fontWithName:@"Avenir-Medium" size:height / 65];
    label1.textColor = [UIColor colorWithRed:(160/255.0) green:(160/255.0) blue:(160/255.0) alpha:1] ;
    label1.numberOfLines = 2;
    [self.mainView addSubview:label1];
//    //add second Note
    label1 = [[UILabel alloc] initWithFrame:CGRectMake(width * 0.035, height * 1.29, width * 0.89, height * 0.05)];
    label1.text = @"\u2022 If you get a call to come down, inform the DE to hand it over at Swiggy Exchange Point.";
    label1.font = [UIFont fontWithName:@"Avenir-Medium" size:height / 65];
    label1.textColor = [UIColor colorWithRed:(160/255.0) green:(160/255.0) blue:(160/255.0) alpha:1] ;
    label1.numberOfLines = 2;
    [self.mainView addSubview:label1];
//    //add third Note
    label1 = [[UILabel alloc] initWithFrame:CGRectMake(width * 0.035, height * 1.34, width * 0.89, height * 0.05)];
    label1.text = @"\u2022 Once your order gets delivered to Exchange point you will see a delivery confirmation.";
    label1.font = [UIFont fontWithName:@"Avenir-Medium" size:height / 65];
    label1.textColor = [UIColor colorWithRed:(160/255.0) green:(160/255.0) blue:(160/255.0) alpha:1] ;
    label1.numberOfLines = 2;
    [self.mainView addSubview:label1];
//    //add forth Note
    label1 = [[UILabel alloc] initWithFrame:CGRectMake(width * 0.035, height * 1.39, width * 0.89, height * 0.05)];
    label1.text = @"\u2022 We would take further upto 15 minuutes to deliver it on your floor.";
    label1.font = [UIFont fontWithName:@"Avenir-Medium" size:height / 65];
    label1.textColor = [UIColor colorWithRed:(160/255.0) green:(160/255.0) blue:(160/255.0) alpha:1] ;
    label1.numberOfLines = 2;
    [self.mainView addSubview:label1];
    
}



-(void) confirmClicked{
    NSLog(@"confirm button is clicked");
    _companyName = [_companyInput text];
    _floorName = [_floorInput text];
    _buildingName = [_buildingInput text];
    _campusName = [_campusInput text];
	// [self sendMixPanelEvent];
		[self confirmOrder];
    [self saveAddress];
}

-(void) sendMixPanelEvent {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* order_id = [defaults objectForKey:@"swiggy_order_id"];
    
    if(order_id == Nil) {
        return;
    }
    
    NSLog(@"sendMixPanelEvent: %@ %@ %@ %@", _companyName,_buildingName,_floorName,_campusName);
    
    if([_companyInput text] == Nil || [[_companyInput text] isKindOfClass:[NSNull class]]) {
        _companyName = @"";
    }
    if(_companyName == Nil) {
        _companyName = @"";
    }
    
    if(_buildingName == Nil || [_buildingName isKindOfClass:[NSNull class]]) {
        _buildingName = @"";
    }
    if(_floorName == Nil || [_floorName isKindOfClass:[NSNull class]]) {
        _floorName = @"";
    }
    if(_campusName == Nil || [_campusName isKindOfClass:[NSNull class]]) {
        _campusName = @"";
    }
	
    Mixpanel *instance = [Mixpanel sharedInstanceWithToken:[ReactNativeConfig envFor:@"MIX_PANEL_TOKEN"]];
    if(_companyName && _buildingName && _floorName && _campusName) {
			NSDictionary *dict = @{ @"order_id" : order_id,@"company" : _companyName,@"building":_buildingName,@"floor": _floorName,@"campus":_campusName,@"order_created":_orderCreated};
        [instance track:@"swiggy-order-flow-confirm" properties:dict];
    }else {
        NSDictionary *dict = @{ @"order_id" : order_id};
        [instance track:@"swiggy-order-flow-confirm" properties:dict];
    }
    [instance flush];
}

-(void) noDeskButtonClicked{
    NSLog(@"No desk delivery button clicked");
}

-(void) closeButtonClicked{
    NSLog(@"close button is clicked");
    _companyName = [_companyInput text];
    _floorName = [_floorInput text];
    _buildingName = [_buildingInput text];
    _campusName = [_campusInput text];
    // [self sendMixPanelEvent];
    [self confirmOrder];
    [self closeSelf];
}

-(void) customizeTextInput:(UITextField*)view{
    view.backgroundColor = [UIColor whiteColor];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5,height * 0.05)];
		view.textColor = [UIColor blackColor];
    view.text = @"";
    view.leftView = paddingView;
    view.leftViewMode = UITextFieldViewModeAlways;
    view.layer.cornerRadius = 5;
    view.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    view.layer.shadowRadius = 1;
    view.layer.shadowOpacity = 0.5;
    view.layer.masksToBounds = false;
    view.layer.shadowColor = [[UIColor blackColor] CGColor];
}

// assign value to the variables
-(void) saveAddressValue:(NSString*)key andValue:(NSString*)value{
    if([key isEqualToString:@"company_name"]){
        _companyName = value;
    }else if([key isEqualToString:@"floor"]){
        _floorName = value;
    }else if([key isEqualToString:@"building"]){
        _buildingName = value;
    }else if([key isEqualToString:@"campus"]){
        _campusName = value;
    }else{
        NSLog(@"No success comparision");
    }
}

-(void) getAsyncAddress{
    NSLog(@"getAsyncAddress called");

    @try {
        __block NSMutableDictionary *resultsDictionary;
        NSString *apiEndPoint = [NSString stringWithFormat:@"%@getAddresses?app_name=swiggy",_baseURL];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString* token = [defaults objectForKey:@"mex_app_token"];
        
        NSURL *url = [NSURL URLWithString:apiEndPoint];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setTimeoutInterval:5];
        [request setValue:token forHTTPHeaderField:@"x-access-token"];
        __block NSError *error1 = [[NSError alloc] init];
        
        //use async way to connect network
        [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse* response,NSData* data,NSError* error)
         {
             NSLog(@"getAsyncAddress response got");
             if ([data length]>0 && error == nil) {
                 resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
                 NSNumber *success = resultsDictionary[@"success"];
                 NSNumber *isOk = [NSNumber numberWithInt:1];
                 if([success isEqualToNumber:isOk]){
                     NSDictionary *addressData = resultsDictionary[@"data"];
                     for(id key in self->_prePopulateData)
                     {
                         NSString *value = addressData[key];
                         if (value == (id)[NSNull null] || value.length == 0 ){
                             NSLog(@"null value");
                         }else{
                             NSLog(@"%@ is %@",key,value);
                             [self saveAddressValue:key andValue:value];
                         }
                     }
                     [self resetViewState];
                     
                 }
             } else if ([data length]==0 && error ==nil) {
                 NSLog(@" download data is null");
                 [self resetViewState];
             } else if( error!=nil) {
                 NSLog(@" error is %@",error);
                 [self resetViewState];
             }
         }];
    }@catch (NSException *exception) {
        NSLog(@"Exception while retriving address");
//        [exception description];
        [self resetViewState];
    }
}

-(void) closeSelf{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:NO completion:Nil];
    });
}

-(void) confirmOrder {
	@try {
		__block NSMutableDictionary *resultsDictionary;
		
		//		Get user details
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		NSString* token = [defaults objectForKey:@"mex_app_token"];
		NSString* name = [defaults objectForKey:@"mex_user_name"];
		//		set defaults if value is not available
		if (!name) {
			name = @"";
		}
		NSString* phone = [defaults objectForKey:@"mex_user_phone"];
		if (!phone) {
			phone = @"";
		}
		
		NSString* order_id = [defaults objectForKey:@"swiggy_order_id"];

    if(order_id == Nil) {
        return;
    }
		
		// order details entered by user or got from getAddress
		NSDictionary *addressJSON = [[NSDictionary alloc] initWithObjectsAndKeys:
																 [_floorInput text], @"floor",
																 [_campusInput text], @"campus",
																 [_buildingInput text], @"building",
																 @"swiggy", @"app_name",
																 [_companyInput text], @"company",
																 @"PENDING", @"app_order_status",
																 order_id, @"app_order_id",
																 name, @"name",
																 phone, @"phone",
																 nil];
		NSLog(@"address is:%@",[addressJSON description]);
		
		if ([NSJSONSerialization isValidJSONObject:addressJSON]) {
			NSError *error;
			NSData *jsonData = [NSJSONSerialization dataWithJSONObject:addressJSON options:NSJSONWritingPrettyPrinted error:&error];
			NSString *apiEndPoint = [NSString stringWithFormat:@"%@", _confirmOrderURL];
			NSURL *url = [NSURL URLWithString:apiEndPoint];
			NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
			[request setHTTPMethod:@"POST"];
			[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
			[request setValue:token forHTTPHeaderField:@"x-access-token"];
			[request setHTTPBody:jsonData];
			__block NSError *error1 = [[NSError alloc] init];
			
			//use async way to connect network
			[NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse* response, NSData*  data, NSError* error) {
				if ([data length] > 0 && error == nil) {
					resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
					NSLog(@"resultsDictionary for confirm order is %@",resultsDictionary);
					
					int success = [[resultsDictionary valueForKey:@"success"] integerValue];
					if(success == 1) {
						_orderCreated = @"Success";
					} else {
						_orderCreated = @"Failed";
					}
					[self sendMixPanelEvent];
					[self closeSelf];
				} else if([data length] == 0 && error == nil) {
					_orderCreated = @"Failed";
					[self sendMixPanelEvent];
					[self closeSelf];
					NSLog(@" download data is null");
				} else if (error != nil) {
					_orderCreated = @"Failed";
					[self sendMixPanelEvent];
					[self closeSelf];
					NSLog(@" error is %@",error);
				}
			}];
		}
	} @catch (NSException *exception) {
		NSLog(@"exception occured");
	}
}

//funtion to save address
// function to save the address to the server
-(void) saveAddress{
    @try{
        __block NSMutableDictionary *resultsDictionary;
        NSDictionary  *addressJSON = [[NSDictionary alloc] initWithObjectsAndKeys:[_floorInput text],@"floor",[_campusInput text],@"campus",[_buildingInput text],@"building",@"swiggy",@"app_name",[_companyInput text],@"company_name", nil];
        NSLog(@"address is:%@",[addressJSON description]);
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString* token = [defaults objectForKey:@"mex_app_token"];
        
        if([NSJSONSerialization isValidJSONObject:addressJSON]){
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:addressJSON options:NSJSONWritingPrettyPrinted error:&error];
            NSString *apiEndPoint = [NSString stringWithFormat:@"%@saveAddress",_baseURL];
            NSURL *url = [NSURL URLWithString:apiEndPoint];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setValue:token forHTTPHeaderField:@"x-access-token"];
            [request setHTTPBody:jsonData];
            __block NSError *error1 = [[NSError alloc] init];
            
            //use async way to connect network
            [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse* response,NSData* data,NSError* error)
             {
                 if ([data length]>0 && error == nil) {
                     resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
                     NSLog(@"resultsDictionary for save address is %@",resultsDictionary);
                     [self closeSelf];
                 } else if ([data length]==0 && error ==nil) {
                     [self closeSelf];
                     NSLog(@" download data is null");
                 } else if( error!=nil) {
                     [self closeSelf];
                     NSLog(@" error is %@",error);
                 }
             }];
            
        }
    }
    @catch (NSException *exception){
        NSLog(@"exception occured");
//        [exception description];
    }
}
@end
