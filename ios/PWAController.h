//
//  PWAController.h
//  RNGoApp
//
//  Created by Ashish Agrawal on 05/06/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface PWAController : UIViewController

-(id)initWithURL:(NSString *)app_ url:(NSString *)url_;
@end
