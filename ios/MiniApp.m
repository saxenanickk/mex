//
//  MiniApp.m
//  RNGoApp
//
//  Created by Ashish Agrawal on 03/04/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MiniApp.h"
#import "FreshchatSDK.h"
#import "SwiggyViewController.h"
#import "PWAController.h"

@implementation MiniApp

// To export a module named CalendarManager
RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(setSessionToken:(NSString *)token) {
  [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"mex_app_token"];
  [[NSUserDefaults standardUserDefaults] synchronize];
}

RCT_EXPORT_METHOD(setUserInfo:(NSString *)name email:(NSString *) email phone : (NSString *) phone) {
  [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"mex_user_name"];
  [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"mex_user_email"];
	NSString* phoneFormatted = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
  [[NSUserDefaults standardUserDefaults] setObject:phoneFormatted forKey:@"mex_user_phone"];
  [[NSUserDefaults standardUserDefaults] synchronize];
}

RCT_EXPORT_METHOD(setSiteInfo:(NSString *)siteId refsiteID:(NSString *)refsiteID siteName:(NSString *) siteName) {
  [[NSUserDefaults standardUserDefaults] setObject:siteId forKey:@"qc_site_id"];
  [[NSUserDefaults standardUserDefaults] setObject:refsiteID forKey:@"mex_site_id"];
  [[NSUserDefaults standardUserDefaults] setObject:siteName forKey:@"mex_site_name"];
  [[NSUserDefaults standardUserDefaults] synchronize];
}

RCT_EXPORT_METHOD(setTenantInfo:(NSString *)tenantId tenantName:(NSString *) tenantName) {
  [[NSUserDefaults standardUserDefaults] setObject:tenantId forKey:@"mex_tenant_id"];
  [[NSUserDefaults standardUserDefaults] setObject:tenantName forKey:@"mex_tenant_name"];
  [[NSUserDefaults standardUserDefaults] synchronize];
}

RCT_EXPORT_METHOD(launchWebApp: (NSString *)name link:(NSString *) link) {

//	[v1 pushViewController:pwa animated:YES ];
    dispatch_async(dispatch_get_main_queue(), ^{
				UIViewController *pwa = [[PWAController alloc] initWithURL:name url:link];
			//	UIViewController *v = [[[UIApplication sharedApplication] keyWindow] rootViewController];
				UINavigationController *v1 = [[[UIApplication sharedApplication] keyWindow] rootViewController];
			//	[[v1 interactivePopGestureRecognizer] setEnabled:YES];
			//	[v addChildViewController:pwa];
				[v1 pushViewController:pwa animated:YES];
//				[v1 showViewController:pwa sender:Nil];
    });
    return;
}

RCT_EXPORT_METHOD(launchApp: (NSString *)name)
{
    if([name isEqualToString:@"swiggy"]) {
        dispatch_async(dispatch_get_main_queue(), ^{

            UIViewController *swiggy = [[SwiggyViewController alloc] init];
            UINavigationController *v1 = [[[UIApplication sharedApplication] keyWindow] rootViewController];
            [[v1 interactivePopGestureRecognizer] setEnabled:YES];
            [v1 pushViewController:swiggy animated:YES ];
        });

        return;
    }

    if([name isEqualToString:@"BBInstant"]) {
        return;
    }
    
    //check for token if not try to do init again
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];

    
}

RCT_EXPORT_METHOD(launchSupportChat) {
	 
	dispatch_async(dispatch_get_main_queue(), ^{
				UINavigationController *v1 = [[[UIApplication sharedApplication] keyWindow] rootViewController];
				[[Freshchat sharedInstance] showConversations:[v1 topViewController]];
			});
}

//get the device uuid
RCT_EXPORT_METHOD(getDeviceId:(RCTResponseSenderBlock)callback) {
    NSString* identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
    callback(@[[NSNull null], identifier]);
}

@end
