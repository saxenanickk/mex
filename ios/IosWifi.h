//
//  IosWifi.h
//  goapp
//
//  Created by shashi shekhar on 16/09/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#ifndef IosWifi_h
#define IosWifi_h
#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface IOSWifi : NSObject <RCTBridgeModule>

@end

#endif /* IosWifi_h */
