//
//  ViewController.h
//  SwiggyAddress
//
//  Created by shashi shekhar on 23/07/19.
//  Copyright © 2019 quantcommune. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

// impportant input fields
@property(strong,atomic) UITextField *companyInput;
@property(strong,atomic) UITextField *floorInput;
@property(strong,atomic) UITextField *buildingInput;
@property(strong,atomic) UITextField *campusInput;

// top right close screen button
@property(strong,atomic) UIButton *closeButton;
// confirm button
@property(strong,atomic) UIButton *confirmButton;
// no desk delivery required button
@property(strong,atomic) UIButton *noDeskButton;


// UISetup components
@property(strong,atomic) UIScrollView *mainView;
@property(strong,atomic) UILabel *headerText;
@property(strong,atomic) UITextView *label;
@property(strong,atomic) UIButton *suggestionButton;
@property (weak,nonatomic) NSString *companyName;
@property (weak,nonatomic) NSString *floorName;
@property (weak,nonatomic) NSString *buildingName;
@property (weak,nonatomic) NSString *campusName;
@property (weak,nonatomic) NSString *orderCreated;
@property (readonly) const NSString *baseURL;
@property (readonly) const NSString *confirmOrderURL;
@property (readonly) const NSArray *prePopulateData;
@property (strong,atomic) UITableView *notesView;
@end

