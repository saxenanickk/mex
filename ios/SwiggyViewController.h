//
//  SwiggyViewController.h
//  RNGoApp
//
//  Created by Ashish Agrawal on 04/06/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface SwiggyViewController : UIViewController <WKScriptMessageHandler>

@end
