//
//  MiniApp.h
//  RNGoApp
//
//  Created by Ashish Agrawal on 03/04/19.
//  Copyright © 2019 Facebook. All rights reserved.
//
#import <UIKit/UIKit.h>

#import <React/RCTBridgeModule.h>
//#if __has_include("RCTBridgeModule.h")
////#import "RCTBridgeModule.h"
//#import "RCTRootView.h"
//#else
////#import <React/RCTBridgeModule.h>
//#import <React/RCTRootView.h>
//#endif
@interface MiniApp : NSObject <RCTBridgeModule>


@end

