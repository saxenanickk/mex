//
//  IosWifi.m
//  goapp
//
//  Created by shashi shekhar on 16/09/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IOSWifi.h"
#import <NetworkExtension/NetworkExtension.h>
#import <SystemConfiguration/CaptiveNetwork.h>
// If using official settings URL
#import <UIKit/UIKit.h>

@implementation IOSWifi
RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(connectToSSID:(NSString*)ssid
									resolver:(RCTPromiseResolveBlock)resolve
									rejecter:(RCTPromiseRejectBlock)reject) {
	
	if (@available(iOS 11.0, *)) {
		NEHotspotConfiguration* configuration = [[NEHotspotConfiguration alloc] initWithSSID:ssid];
		configuration.joinOnce = true;
		
		[[NEHotspotConfigurationManager sharedManager] applyConfiguration:configuration completionHandler:^(NSError * _Nullable error) {
			if (error != nil) {
				reject(@"nehotspot_error", @"Error while configuring WiFi", error);
			} else {
				resolve(@"success");
			}
		}];
		
	} else {
		reject(@"ios_error", @"Not supported in iOS<11.0", nil);
	}
}

RCT_EXPORT_METHOD(disconnectFromSSID:(NSString*)ssid
									resolver:(RCTPromiseResolveBlock)resolve
									rejecter:(RCTPromiseRejectBlock)reject) {
	if (@available(iOS 11.0, *)) {
		[[NEHotspotConfigurationManager sharedManager] getConfiguredSSIDsWithCompletionHandler:^(NSArray<NSString *> *ssids) {
			if (ssids != nil && [ssids indexOfObject:ssid] != NSNotFound) {
				[[NEHotspotConfigurationManager sharedManager] removeConfigurationForSSID:ssid];
			}
			resolve(@"disconnected");
		}];
	} else {
		reject(@"ios_error", @"Not supported in iOS<11.0", nil);
	}
	
}

RCT_REMAP_METHOD(currentSSID,
				resolver:(RCTPromiseResolveBlock)resolve
				rejecter:(RCTPromiseRejectBlock)reject) {
	
	NSString *kSSID = (NSString*) kCNNetworkInfoKeySSID;
	
	NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
	for (NSString *ifnam in ifs) {
		NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
		if (info[kSSID]) {
			resolve(info[kSSID]);
			return;
		}
	}
	
	reject(@"cannot_detect_ssid", @"Cannot detect SSID", nil);
}

// Apple Rejected Application Because of this App-Prefs:root=WIFI, So I changed this.

- (NSDictionary*)constantsToExport {
	return @{
		@"settingsURL": UIApplicationOpenSettingsURLString
	};
}

@end
