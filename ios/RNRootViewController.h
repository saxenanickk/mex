//
//  RNRootViewController.h
//  goapp
//
//  Created by Ashish Agrawal on 14/11/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#ifndef RNRootViewController_h
#define RNRootViewController_h

#import <UIKit/UIKit.h>

@interface RNRootViewController : UIViewController
@end

#endif /* RNRootViewController_h */
