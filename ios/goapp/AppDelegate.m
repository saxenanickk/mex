/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

#import "AppDelegate.h"

#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <GoogleMaps/GoogleMaps.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Firebase.h>
#import "RNFirebaseNotifications.h"
#import "RNFirebaseMessaging.h"
#import <RNBranch/RNBranch.h>
#import <RNGoogleSignin/RNGoogleSignin.h>
#import "ReactNativeConfig.h"
#import "RNRootViewController.h"
#import "FreshchatSDK.h"
#import "SplashScreen.h"
#import "Mixpanel.h"
#import "ReactNativeConfig.h"
@import GooglePlaces;

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	// Google Places Api Key
	[GMSPlacesClient provideAPIKey:@"AIzaSyDmxdLvK373Nbwa8EpXaf6E53bS3SNHn00"];
	[GMSServices provideAPIKey:@"AIzaSyDmxdLvK373Nbwa8EpXaf6E53bS3SNHn00"];
	
	FreshchatConfig *config = [[FreshchatConfig alloc]initWithAppID:@"8513eeeb-e972-4c2b-be05-1ae46bfe326c"  andAppKey:@"971234e2-9e27-4b19-a53e-4bbd01db7898"];
	[[Freshchat sharedInstance] initWithConfig:config];
	
	//Mixpanel passing options to track notifications
	[Mixpanel sharedInstanceWithToken:[ReactNativeConfig envFor:@"MIX_PANEL_TOKEN"] launchOptions:launchOptions];

	// Notifications
	[FIRApp configure];
	[[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
  [RNFirebaseNotifications configure];
	
  //	Branch
	[RNBranch initSessionWithLaunchOptions:launchOptions isReferrable:YES];
	
	[[FBSDKApplicationDelegate sharedInstance] application:application
													 didFinishLaunchingWithOptions:launchOptions];
	
	RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
	RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
																									 moduleName:@"Goapp"
																						initialProperties:nil];
	
	rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];
	
	self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
//	UIViewController *rootViewController = [UIViewController new];
	UIViewController *rootViewController = [RNRootViewController new];
	rootViewController.view = rootView;
	UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
//	[navigationController setNavigationBarHidden:YES];
	self.window.rootViewController = navigationController;
	[self.window makeKeyAndVisible];
	[SplashScreen show];
	return YES;
}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
#if DEBUG
	return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
#else
	return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
}

- (BOOL)application:(UIApplication *)application
						openURL:(NSURL *)url
						options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
	
	if (![RNBranch.branch application:application openURL:url options:options]) {
			// do other deep link routing for the Facebook SDK, Pinterest SDK, etc
		BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
																																	openURL:url
																												sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
																															 annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
										];
		return handled;
	}
	return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *restorableObjects))restorationHandler {
    return [RNBranch continueUserActivity:userActivity];
}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
  [[RNFirebaseNotifications instance] didReceiveLocalNotification:notification];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler{
  [[RNFirebaseNotifications instance] didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
  [[RNFirebaseMessaging instance] didRegisterUserNotificationSettings:notificationSettings];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
  [FIRMessaging messaging].APNSToken = deviceToken;
	
  [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:@"ios_device_token"];
  [[NSUserDefaults standardUserDefaults] synchronize];
	
	Mixpanel *mixpanel = [Mixpanel sharedInstance];
	[mixpanel.people addPushDeviceToken:deviceToken];
}
@end
