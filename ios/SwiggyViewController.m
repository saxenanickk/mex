//
//  SwiggyViewController.m
//  RNGoApp
//
//  Created by Ashish Agrawal on 04/06/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "SwiggyViewController.h"
#import "Mixpanel.h"
#import "ViewController.h"

@interface SwiggyViewController () <WKNavigationDelegate, WKUIDelegate, WKScriptMessageHandler>
@property(strong,nonatomic) WKWebView *theWebView;
@end

@implementation SwiggyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
		NSString *path = @"https://www.swiggy.com/?utm_source=RMZ&utm_campaign=RMZ&platform=RMZ&token=61B667C88FB270F558FB9254F94A2D73";
		NSURL *URL = [NSURL URLWithString:path];
		NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:URL];

		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		NSString *cookie = [defaults objectForKey:@"swiggy_cookie"];
		if(cookie != Nil) {
			[request setValue:cookie forHTTPHeaderField:@"x-platform-cookie"];
		}
		WKWebViewConfiguration *theConfiguration =
			[[WKWebViewConfiguration alloc] init];

		[theConfiguration.userContentController
			addScriptMessageHandler:self name:@"setCookie"];
		[theConfiguration.userContentController
			addScriptMessageHandler:self name:@"fireEvent"];

		CGRect initRect = self.view.frame;

		CGRect navigationRect = self.navigationController.navigationBar.frame;
		initRect.origin.y = initRect.origin.y + navigationRect.origin.y + navigationRect.size.height;
		initRect.size.height = initRect.size.height - navigationRect.origin.y - navigationRect.size.height;

		self.view.insetsLayoutMarginsFromSafeArea = YES;
		_theWebView = [[WKWebView alloc] initWithFrame:initRect
																	 configuration:theConfiguration];

		[_theWebView loadRequest:request];

		self.theWebView.navigationDelegate = self;
		[self.view addSubview:_theWebView];
		[self.view layoutIfNeeded];

    //test code
//    [defaults setObject:@"ignore" forKey:@"swiggy_order_id"];
//    [defaults synchronize];
//
//    UIViewController *swiggy = [[ViewController alloc] init];
//    self.definesPresentationContext = YES; //self is presenting view controller
//    swiggy.view.backgroundColor = [UIColor clearColor];
//    swiggy.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//    [self presentViewController:swiggy animated:YES completion:nil];
//    //test code ends
    
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {

}
//Since ViewController is a WKScriptMessageHandler, as declared in the ViewController interface, it must implement the userContentController:didReceiveScriptMessage method. This is the method that is triggered each time 'interOp' is sent a message from the JavaScript code.

- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message{
    
    NSString *name = (NSString*)message.name;
    if ([name isEqualToString:@"setCookie"]) {
        NSString *sentData = (NSString*)message.body;
        
        if(sentData != Nil) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:sentData forKey:@"swiggy_cookie"];
            [defaults synchronize];
        }
    } else if ([name isEqualToString:@"fireEvent"]) {
        NSDictionary *sentData = (NSDictionary*)message.body;
        NSString *order_id = [sentData objectForKey:@"value"];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:order_id forKey:@"swiggy_order_id"];
        [defaults synchronize];
        
        UIViewController *swiggy = [[ViewController alloc] init];
        self.definesPresentationContext = YES; //self is presenting view controller
        swiggy.view.backgroundColor = [UIColor clearColor];
        swiggy.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        [self presentViewController:swiggy animated:YES completion:nil];
        
        //        Mixpanel *instance = [Mixpanel sharedInstanceWithToken:@"5a6c6ce1312e7af46fd8d6a3902e46d2"];
        //        NSDictionary *dict = @{ @"order_id" : order_id};
        //        [instance track:@"swiggy-order-confirm" properties:dict];
        //        [instance flush];
    }
}

//In the example code above you can see a WKScriptMessage is received from JavaScript. Since WKWebKit defines JSON as the data transport protocol, the JavaScript associative array sent as the message's body has already been converted into an NSDictionary before we have access to the message. We can then use this NSDictionary to retrieve an int that is the value associated with the 'count' label. The JSON conversation creates NSNumbers for numeric type values so the code example retrieves the NSNumber's intValue, modifies it, and then sends the modified value back to JavaScript.
//
//One of the very nice features of WKWebKit framework is that the WKWebView runs independently of the main, or User Interface, thread. This keeps our apps responsive to user input. The storeAndShow method will not execute in the app's main thread.

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
