//
//  Access.m
//  goapp
//
//  Created by Ashish Agrawal on 18/11/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

//    public void init(String token, Callback success, Callback error) {
//    public void isInitialized(Callback initilized) {
//    public void openLock(Callback success, Callback error) {

@interface RCT_EXTERN_MODULE(Access, NSObject)

RCT_EXTERN_METHOD(init:(NSString *)token success: (RCTResponseSenderBlock)success error: (RCTResponseSenderBlock)error);
RCT_EXTERN_METHOD(isInitialized:(RCTResponseSenderBlock) callback);
RCT_EXTERN_METHOD(openLock:(RCTResponseSenderBlock)success error: (RCTResponseSenderBlock)error);
RCT_EXTERN_METHOD(factoryReset);

//RCT_EXTERN_METHOD(clearMiniApp)

@end

