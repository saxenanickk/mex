//
//  Splash.h
//  goapp
//
//  Created by Subramanya Chakravarthy on 25/11/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#ifndef Splash_h
#define Splash_h
#import <React/RCTBridgeModule.h>
#import <UIKit/UIKit.h>

@interface SplashScreen : NSObject<RCTBridgeModule>
+ (void)show;
+ (void)hide;
@end

#endif /* Splash_h */
