//
//  SwiggyViewController.m
//  RNGoApp
//
//  Created by Ashish Agrawal on 04/06/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "PWAController.h"
#import "Mixpanel.h"
#import "AFNetworking.h"
#import "AFURLSessionManager.h"

#import "ReactNativeConfig.h"

@interface PWAController () <WKNavigationDelegate>
@property(strong,nonatomic) WKWebView *theWebView;
@property(strong,nonatomic) UIActivityIndicatorView *spinner;
@property(strong,nonatomic) NSString *app;
@property(strong,nonatomic) NSString *url;
@end

@implementation PWAController

-(id)initWithURL:(NSString *)app_ url:(NSString *)url_
{
    self = [super init];
    if (self) {
        self.app = app_;
        self.url = url_;
    }
    return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	if(self.url == Nil) {
			[self.navigationController popViewControllerAnimated:YES];
			return;
	}
	
	if([self.app isEqualToString:@"Azgo"]) {
		[self loadAzgo];
	} else if([self.app isEqualToString:@"Hungerbox"]) {
		[self loadHB];
	}else {
		[self loadPWA];
	}
}

- (void)loadPWA {
	NSString *path = self.url;
	NSURL *URL = [NSURL URLWithString:path];
	NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:URL];
	
	WKWebViewConfiguration *theConfiguration =
	[[WKWebViewConfiguration alloc] init];
	
	_theWebView = [[WKWebView alloc] initWithFrame:[UIScreen mainScreen].bounds
																	 configuration:theConfiguration];
	
	self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	[self.spinner setCenter:[self.theWebView center]];
	
	[self.theWebView addSubview:self.spinner];
	[self.spinner startAnimating];
	
	[_theWebView loadRequest:request];
	self.theWebView.navigationDelegate = self;
	
	self.view = _theWebView;
}

- (void)loadAzgo {
			WKWebViewConfiguration *theConfiguration =
			[[WKWebViewConfiguration alloc] init];
			
			self.theWebView = [[WKWebView alloc] initWithFrame:[UIScreen mainScreen].bounds
																					 configuration:theConfiguration];
			self.theWebView = [[WKWebView alloc] initWithFrame:[UIScreen mainScreen].bounds];
			
			[self.theWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
			
			self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
			[self.spinner setCenter:[self.theWebView center]];

			[self.theWebView addSubview:self.spinner];
			[self.spinner startAnimating];
			
			self.view = self.theWebView;

			NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			NSString* token = [defaults objectForKey:@"mex_app_token"];
			if(token != Nil) {
					AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]]; //Intialialize AFURLSessionManager
					
					 NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[ReactNativeConfig envFor:@"AZGO_TOKEN_URL"] parameters:nil error:nil];
					
					[req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
					[req setValue:token forHTTPHeaderField:@"X-ACCESS-TOKEN"];
				
					NSURLSessionDataTask *checkVersionTask = [manager dataTaskWithRequest:req uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
						
							if (!error) {
									NSLog(@"Reply JSON: %@", responseObject);
									
									if ([responseObject isKindOfClass:[NSDictionary class]]) {
											NSDictionary *responseDict = [[NSDictionary alloc] initWithDictionary:responseObject];
											NSNumber * success = [responseDict objectForKey:@"success"];
											
											if([success boolValue] == YES) {
													NSString * azgoSessionToken = [responseDict objectForKey:@"sessionToken"];
													if(azgoSessionToken !=  nil) {
															NSString *path = [[ReactNativeConfig envFor:@"AZGO_URL_PREFIX"] stringByAppendingString:azgoSessionToken];
															NSLog(@"AzGo path: %@", path);

															NSURL *URL = [NSURL URLWithString:path];
															NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:URL];
															
															[self.theWebView loadRequest:request];
															self.theWebView.navigationDelegate = self;
													}
											} else {
													[self.navigationController popViewControllerAnimated:NO];
													return;
											}
									} else {
											[self.navigationController popViewControllerAnimated:NO];
											return;
									}
							} else {
									[self.navigationController popViewControllerAnimated:NO];
									return;
//                    NSLog(@"Error: %@, %@, %@", error, response, responseObject);
							}
							
					}];
					[checkVersionTask resume];
					
					//https://rmz.azgo.app/?session=
			} else {
					//No Token for Azgo
					[self.navigationController popViewControllerAnimated:NO];
					return;
			}
}

- (void)loadHB {
			WKWebViewConfiguration *theConfiguration =
			[[WKWebViewConfiguration alloc] init];
			
			self.theWebView = [[WKWebView alloc] initWithFrame:[UIScreen mainScreen].bounds
																					 configuration:theConfiguration];
			self.theWebView = [[WKWebView alloc] initWithFrame:[UIScreen mainScreen].bounds];
			
			[self.theWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
			
			self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
			[self.spinner setCenter:[self.theWebView center]];

			[self.theWebView addSubview:self.spinner];
			[self.spinner startAnimating];
			
			self.view = self.theWebView;

			NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			NSString* token = [defaults objectForKey:@"mex_app_token"];
			if(token != Nil) {
					AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]]; //Intialialize AFURLSessionManager
					
					 NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[ReactNativeConfig envFor:@"HB_TOKEN_URL"] parameters:nil error:nil];
					
					[req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
					[req setValue:token forHTTPHeaderField:@"X-ACCESS-TOKEN"];
					
					NSURLSessionDataTask *checkVersionTask = [manager dataTaskWithRequest:req uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
							
							if (!error) {
									NSLog(@"Reply JSON: %@", responseObject);
									
									if ([responseObject isKindOfClass:[NSDictionary class]]) {
											NSDictionary *responseDict = [[NSDictionary alloc] initWithDictionary:responseObject];
											NSNumber * success = [responseDict objectForKey:@"success"];
											NSDictionary *dataDict = [responseDict objectForKey:@"data"];

											if([success boolValue] == YES) {
													NSString * hbSessionToken = [dataDict objectForKey:@"access_token"];
													if(hbSessionToken !=  nil) {
														NSURL *URL = [NSURL URLWithString:[ReactNativeConfig envFor:@"HB_URL_PREFIX"]];
														NSString* site_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"qc_site_id"];
														
														NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:URL];
														NSString *body = [NSString stringWithFormat: @"access_token=%@&location_identifier=%@", hbSessionToken,site_id];
														[request setHTTPMethod: @"POST"];
														[request setHTTPBody: [body dataUsingEncoding: NSUTF8StringEncoding]];
														[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

														[self.theWebView loadRequest: request];
														self.theWebView.navigationDelegate = self;
													}
											} else {
													[self.navigationController popViewControllerAnimated:NO];
													return;
											}
									} else {
											[self.navigationController popViewControllerAnimated:NO];
											return;
									}
							} else {
									[self.navigationController popViewControllerAnimated:NO];
									return;
							}
							
					}];
					[checkVersionTask resume];
				} else {
					//No Token for Azgo
					[self.navigationController popViewControllerAnimated:NO];
					return;
			}
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [self.spinner stopAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
