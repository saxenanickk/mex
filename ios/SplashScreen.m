//
//  Splash.m
//  goapp
//
//  Created by Subramanya Chakravarthy on 25/11/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "SplashScreen.h"
#import <React/RCTBridge.h>
#import <Foundation/Foundation.h>

static bool waiting = true;
static bool addedJsLoadErrorObserver = false;
static UIView* loadingView = nil;

@implementation SplashScreen

- (dispatch_queue_t) methodQueue{
	return dispatch_get_main_queue();
}

RCT_EXPORT_MODULE(SplashScreen)

+ (void)show {
	if (!addedJsLoadErrorObserver) {
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jsLoadError:) name:RCTJavaScriptDidFailToLoadNotification object:nil];
		addedJsLoadErrorObserver = true;
	}
	
	while (waiting) {
		NSDate* later = [NSDate dateWithTimeIntervalSinceNow:0.1];
		[[NSRunLoop mainRunLoop] runUntilDate:later];
	}
}

+ (void)hide {
	if (waiting) {
		dispatch_async(dispatch_get_main_queue(), ^{
			waiting = false;
		});
	} else {
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
			[loadingView removeFromSuperview];
		});
	}
}

+ (void) jsLoadError:(NSNotification*)notification {
	// If there was an error loading javascript, hide the splash screen so it can be shown.  Otherwise the splash screen will remain forever, which is a hassle to debug.
	[SplashScreen hide];
}

RCT_EXPORT_METHOD(show) {
	[SplashScreen show];
}

RCT_EXPORT_METHOD(hide) {
	[SplashScreen hide];
}


@end
