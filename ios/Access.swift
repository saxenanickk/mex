//
//  Access.swift
//  goapp
//
//  Created by Ashish Agrawal on 19/11/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
#if (arch(arm64))
import LockniOSFrameWork
#endif

@objc(Access)
class Access: NSObject {
	@objc(init:success:error:)
	func `init`(token: String, success: @escaping (NSArray) -> (),error: @escaping (NSArray) -> () ) -> Void {
		#if (arch(arm64))

			let lockniOSSDK = LockniOSBLESDK()
			let completionCallBack : (LockniOSFrameWork.LockniOSBLESDK.LocknStatusEnum) -> Void = {
				result in
				switch result {
					case LockniOSBLESDK.LocknStatusEnum.LOCKED:
						error(["Locked."])
						break
					case LockniOSBLESDK.LocknStatusEnum.UNLOCKED:
						error(["Unlocked."])
						break
					case LockniOSBLESDK.LocknStatusEnum.SUCCESS:
						success(["Access Granted."]);
						break
					case LockniOSBLESDK.LocknStatusEnum.FAILED:
						error(["Access Reader not in range.\nPlease try again."])
						break
					case LockniOSBLESDK.LocknStatusEnum.SOMETHING_WENT_WRONG:
						error(["Connection Timeout!\nPlease try again."])
						break
					case LockniOSBLESDK.LocknStatusEnum.UPDATING:
						error(["Updating."])
						break
					case LockniOSBLESDK.LocknStatusEnum.BLUETOOTH_COMMUNICATION_FAILED:
						error(["Connection timeout!\nAccess Reader not in range."])
						break
					case LockniOSBLESDK.LocknStatusEnum.BLUETOOTH_OR_GPS_OFF:
						error(["Bluetooth or gps off."])
						break
					case LockniOSBLESDK.LocknStatusEnum.TIMED_OUT:
						error(["Connection timeout!\nPlease try again."])
						break
					case LockniOSBLESDK.LocknStatusEnum.OUT_OF_RANGE:
						error(["Connection timeout!\nAccess Reader not in range."])
						break
					case LockniOSBLESDK.LocknStatusEnum.UNAUTHORISED:
						error(["Unauthorised."])
						break
					case LockniOSBLESDK.LocknStatusEnum.TOO_MANY_DEVICES_IN_RANGE:
						error(["Too many devices in range."])
						break
					case LockniOSBLESDK.LocknStatusEnum.NO_DEVICES_IN_RANGE:
						error(["Access Reader not in range.\nPlease try again."])
						break
					case LockniOSBLESDK.LocknStatusEnum.INITIALIZATION_NO_DONE:
						error(["Access not initialized.\nPlease restart the app."])
						break
					case LockniOSBLESDK.LocknStatusEnum.USER_TEMPID_ACTIVATION_SUCCESSFUL:
						success(["MEX. Access Activated."])
						break
					case LockniOSBLESDK.LocknStatusEnum.USER_TEMPID_ACTIVATION_FAILED:
						error(["Invalid Code, Retry or connect\nwith your Organisation Admin."])
						break
					case LockniOSBLESDK.LocknStatusEnum.DEVICE_NOT_SUPPORTED:
						error(["Device not supported.\nPlease connect MEX. Support."])
						break
					default:
						error(["Something went wrong."])
						break
				}
			}
			lockniOSSDK.initilizeSDK(
				authString: "e1hcNHbYzp7CVUVofqzH43xVZnCHOJC3zpLfXYCc",
				userTempId: token,
				completionCallBack: completionCallBack
			)
		#endif
	}
	
	@objc(isInitialized:)
	func isInitialized(callback: (NSArray) -> () ) -> Void {
		#if (arch(arm64))

		let lockniOSSDK = LockniOSBLESDK()
		let isInit = lockniOSSDK.isSDKInitialized();

		callback([isInit])
		#endif
	}
	
	@objc(openLock:error:)
	func openLock(success: @escaping (NSArray) -> (),error: @escaping (NSArray) -> () ) -> Void {
		#if (arch(arm64))

			let lockniOSSDK = LockniOSBLESDK()
			let completionCallBack : (LockniOSFrameWork.LockniOSBLESDK.LocknStatusEnum) -> Void = {
				result in
					print("Default Callback Completion")
				switch result {
					case LockniOSBLESDK.LocknStatusEnum.LOCKED:
						error(["Locked."])
						break
					case LockniOSBLESDK.LocknStatusEnum.UNLOCKED:
						error(["Unlocked."])
						break
					case LockniOSBLESDK.LocknStatusEnum.SUCCESS:
						success(["Access Granted."]);
						break
					case LockniOSBLESDK.LocknStatusEnum.FAILED:
						error(["Access Reader not in range.\nPlease try again."])
						break
					case LockniOSBLESDK.LocknStatusEnum.SOMETHING_WENT_WRONG:
						error(["Connection Timeout!\nPlease try again."])
						break
					case LockniOSBLESDK.LocknStatusEnum.UPDATING:
						error(["Updating."])
						break
					case LockniOSBLESDK.LocknStatusEnum.BLUETOOTH_COMMUNICATION_FAILED:
						error(["Connection timeout!\nAccess Reader not in range."])
						break
					case LockniOSBLESDK.LocknStatusEnum.BLUETOOTH_OR_GPS_OFF:
						error(["Bluetooth or gps off."])
						break
					case LockniOSBLESDK.LocknStatusEnum.TIMED_OUT:
						error(["Connection timeout!\nPlease try again."])
						break
					case LockniOSBLESDK.LocknStatusEnum.OUT_OF_RANGE:
						error(["Connection timeout!\nAccess Reader not in range."])
						break
					case LockniOSBLESDK.LocknStatusEnum.UNAUTHORISED:
						error(["Unauthorised."])
						break
					case LockniOSBLESDK.LocknStatusEnum.TOO_MANY_DEVICES_IN_RANGE:
						error(["Too many devices in range."])
						break
					case LockniOSBLESDK.LocknStatusEnum.NO_DEVICES_IN_RANGE:
						error(["Access Reader not in range.\nPlease try again."])
						break
					case LockniOSBLESDK.LocknStatusEnum.INITIALIZATION_NO_DONE:
						error(["Access not initialized.\nPlease restart the app."])
						break
					case LockniOSBLESDK.LocknStatusEnum.USER_TEMPID_ACTIVATION_SUCCESSFUL:
						success(["MEX. Access Activated."])
						break
					case LockniOSBLESDK.LocknStatusEnum.USER_TEMPID_ACTIVATION_FAILED:
						error(["Invalid Code, Retry or connect\nwith your Organisation Admin."])
						break
					case LockniOSBLESDK.LocknStatusEnum.DEVICE_NOT_SUPPORTED:
						error(["Device not supported.\nPlease connect MEX. Support."])
						break
					default:
						error(["Something went wrong."])
						break
				}
			}
			
			if(lockniOSSDK.isSDKInitialized()) {
				lockniOSSDK.openGateFunction(completionCallBack: completionCallBack)
			} else {
				error(["Access not initialized.\nPlease restart the app."]);
			}
		#endif

	}
	
	@objc(factoryReset)
	func factoryReset() -> Void {
		#if (arch(arm64))

		let lockniOSSDK = LockniOSBLESDK()
		lockniOSSDK.factoryReset();
		#endif
	}
	
}
