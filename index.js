import React from "react";
import { AppRegistry } from "react-native";
import App from "./App";

export default class Goapp extends React.Component {
	render() {
		return <App {...this.props} />;
	}
}

AppRegistry.registerComponent("Goapp", () => Goapp);
