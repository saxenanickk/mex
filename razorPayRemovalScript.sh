#! /bin/sh
echo "Starting razor pay debug architecture removal"
cd node_modules/react-native-razorpay/ios/ &&
lipo -remove i386 Razorpay.framework/Razorpay -output Razorpay.framework/Razorpay &&
lipo -remove x86_64 Razorpay.framework/Razorpay -output Razorpay.framework/Razorpay &&

rm Razorpay.framework/Modules/Razorpay.swiftmodule/i386.swiftdoc &&
rm Razorpay.framework/Modules/Razorpay.swiftmodule/i386.swiftmodule &&
rm Razorpay.framework/Modules/Razorpay.swiftmodule/x86_64.swiftdoc &&
rm Razorpay.framework/Modules/Razorpay.swiftmodule/x86_64.swiftmodule && cd .. && cd .. && cd ..
echo "Completed!"