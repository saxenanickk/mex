import RazorpayCheckout from "react-native-razorpay";
import Config from "react-native-config";

/**
 *	Params =>
 *	amount: "Amount to be processed"
 *	color: "Theme color to be shown on Razorpay Screen"
 *	description: "The description to be shown on Razorpay Screen"
 */
export default function Razorpay(params) {
	return new Promise((resolve, reject) => {
		let AMOUNT = params.amount * 100;
		let COLOR = typeof params.color === "undefined" ? "#e94e83" : params.color;
		let DESCRIPTION = params.description;
		var options = {
			description: DESCRIPTION,
			image:
				"https://res.cloudinary.com/quantified-community-services-private-limited/image/upload/v1576821540/d0b13790-22ed-11ea-8c2d-13af66f68280_100x100.png",
			currency: "INR",
			key:
				Config.SERVER_TYPE === "live"
					? "rzp_live_YliyVpXqu7xLCZ"
					: "rzp_test_RfDTYZSFVSAziw",
			amount: AMOUNT,
			name: params.app,
			theme: { color: COLOR },
			prefill: {
				email: params.email ? params.email : "",
				contact: params.contact ? params.contact : "",
				name: params.name ? params.name : ""
			}
		};
		RazorpayCheckout.open(options)
			.then(response => {
				console.log("Razorpay: ", response);
				resolve(response);
			})
			.catch(error => reject(error));
	});
}
