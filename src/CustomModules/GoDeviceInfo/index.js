import GoDeviceInfo from "./GoDeviceInfo";
import { Platform, NativeModules } from "react-native";

const { MiniApp } = NativeModules;
export function DeviceId() {
	if (Platform.OS === "ios") {
		return new Promise(function(resolve, reject) {
			MiniApp.getDeviceId((error, uuid) => {
				if (!error) {
					console.log("uuid is", uuid);
					resolve(uuid);
				} else {
					reject(error);
				}
			});
		});
	} else {
		return new Promise(function(resolve, reject) {
			GoDeviceInfo.getDeviceId(
				deviceId => {
					resolve(deviceId);
				},
				error => {
					reject(error);
				}
			);
		});
	}
}
