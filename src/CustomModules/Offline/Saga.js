import { takeEvery, takeLatest, put, select, call } from "redux-saga/effects";
import {
	retrieveData,
	USER_PROFILE,
	HOME_PAGE_TRENDING_APPS,
	HOME_PAGE_BANNERS,
	HOME_PAGE_CONTEXT,
	SERVICES,
	PARTNERS
} from ".";
import {
	mexSaveUserProfile,
	mexProfileLoading
} from "../ApplicationToken/Saga";
import {
	mexSaveContext,
	mexSaveBanners,
	mexSaveTrendingApps,
	mexHomePageFailure
} from "../../Components/Feed/Saga";
import {
	mexSaveServices,
	mexServicesLoading
} from "../../Components/Service/Saga";
import {
	mexSavePartners,
	mexPartnersLoading
} from "../../Components/Partners/Saga";

export const WITH_OFFLINE_CAPABILITY = "WITH_OFFLINE_CAPABILITY";
export const GET_FROM_ASYNC_STORAGE = "GET_FROM_ASYNC_STORAGE";
export const ADD_TO_OFFLINE_QUEUE = "ADD_TO_OFFLINE_QUEUE";
export const DISPATCH_OFFLINE_QUEUE = "DISPATCH_OFFLINE_QUEUE";
export const EMPTY_OFFLINE_QUEUE = "EMPTY_OFFLINE_QUEUE";

export const withOfflineCapability = payload => ({
	type: WITH_OFFLINE_CAPABILITY,
	payload
});

export const getFromAsyncStorage = payload => ({
	type: GET_FROM_ASYNC_STORAGE,
	payload
});

export const addToOfflineQueue = payload => ({
	type: ADD_TO_OFFLINE_QUEUE,
	payload
});

export const dispatchOfflineQueue = payload => ({
	type: DISPATCH_OFFLINE_QUEUE,
	payload
});

export const emptyOfflineQueue = payload => ({
	type: EMPTY_OFFLINE_QUEUE,
	payload
});

/**
 * To get network status from the store.
 */
export const checkInternetStatus = state => state.netInfo.status;

/**
 * To get the offline actions queue from the store.
 */
export const getOfflineQueue = state => state.offline.queue;

export function* offlineSaga(dispatch) {
	yield takeEvery(WITH_OFFLINE_CAPABILITY, handleWithOfflineCapability);
	yield takeEvery(GET_FROM_ASYNC_STORAGE, handleGetFromAsyncStorage);
	yield takeLatest(DISPATCH_OFFLINE_QUEUE, handleDispatchOfflineQueue);
}

function* handleWithOfflineCapability(action) {
	try {
		/**
		 * Check if internet is present ?
		 */
		const isInternet = yield select(checkInternetStatus);
		// If internet is present
		if (isInternet) yield put(action.payload);
		// If internet is not there
		else {
			yield put(getFromAsyncStorage(action.payload.type));
			yield put(addToOfflineQueue(action.payload));
		}
	} catch (error) {
		console.log("Error in Offline Capability Saga: ", error);

		/**
		 * If above check fails. Then let it go through the normal flow.
		 */
		yield put(action.payload);
	}
}

function* handleGetFromAsyncStorage(action) {
	try {
		/**
		 * Fetch  already stored information from AsyncStorage.
		 *
		 * Use "payload.action.type" for identifying the key.
		 *
		 * Keys can be -
		 * 		MEX_GET_PROFILE,
		 * 		MEX_GET_CONTEXT,
		 * 		MEX_GET_BANNERS,
		 * 		MEX_GET_TRENDING_APPS,
		 * 		MEX_GET_SERVICES
		 *
		 * for now.
		 */

		switch (action.payload) {
			case "MEX_GET_PROFILE":
				// Get UserProfile from AsyncStorage
				const userProfile = yield call(retrieveData, USER_PROFILE);

				// Success
				if (userProfile) yield put(mexSaveUserProfile(userProfile));

				yield put(mexProfileLoading(false));
				break;
			case "MEX_GET_CONTEXT":
				// Get Context from AsyncStorage
				const context = yield call(retrieveData, HOME_PAGE_CONTEXT);

				if (context) {
					// Success
					yield put(mexSaveContext(context));
				} else {
					// Failure
					yield put(mexHomePageFailure("context"));
				}
				break;
			case "MEX_GET_BANNERS":
				// Get Banners from AsyncStorage
				const banners = yield call(retrieveData, HOME_PAGE_BANNERS);

				if (banners) {
					// Success
					yield put(mexSaveBanners(banners));
				} else {
					// Failure
					yield put(mexHomePageFailure("banners"));
				}
				break;
			case "MEX_GET_TRENDING_APPS":
				// Get Trending Apps from AsyncStorage
				const trendingApps = yield call(retrieveData, HOME_PAGE_TRENDING_APPS);

				if (trendingApps) {
					// Success
					yield put(mexSaveTrendingApps(trendingApps));
				} else {
					// Failure
					yield put(mexHomePageFailure("trendingApps"));
				}
				break;
			case "MEX_GET_SERVICES":
				// Get Services from AsyncStorage
				const services = yield call(retrieveData, SERVICES);

				// Success
				if (services) {
					yield put(mexSaveServices(services));
				}
				yield put(mexServicesLoading(false));
				break;
			case "MEX_GET_PARTNERS":
				// Get Partners from AsyncStorage
				const partners = yield call(retrieveData, PARTNERS);

				// Success
				if (partners) {
					yield put(mexSavePartners(partners));
				}
				yield put(mexPartnersLoading(false));
				break;
			default:
				break;
		}
	} catch (error) {
		console.log("handleFetchFromAsyncStorage Error: ", error);
	}
}

function* handleDispatchOfflineQueue(action) {
	try {
		/**
		 * Get offline actions queue from the store.
		 *
		 * Loop over the whole queue and dispatch actions.
		 */

		const offlineQueue = yield select(getOfflineQueue);

		let iterator = 0;

		let keys = Object.keys(offlineQueue);

		while (iterator < keys.length) {
			// Use item to Dispatch Actions
			yield put(offlineQueue[keys[iterator]]);
			iterator++;
		}
	} catch (error) {
		console.log("Error EmptyOfflineQueue: ", error);
		// TODO:
	} finally {
		// Empty the whole Queue
		yield put(emptyOfflineQueue());
	}
}
