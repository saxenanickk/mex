import AsyncStorage from "@react-native-community/async-storage";

/**
 * Different type of Keys
 * These are just identifiers to uniquely identify the data.
 */
const USER_PROFILE = "@user_profile";
const HOME_PAGE_CONTEXT = "@home_page_context";
const HOME_PAGE_BANNERS = "@home_page_banners";
const HOME_PAGE_TRENDING_APPS = "@home_page_tending_apps";
const SERVICES = "@services";
const PARTNERS = "@partners";
const ACCESS_16_DIGIT_CODE = "@accees_16_digit_code";

const OfflineStore = {};

/**
 * This function saves the data to AsyncStorage.
 *
 * @param {String} type
 * @param {String} value
 */
const saveData = async (type, value) => {
	try {
		await AsyncStorage.setItem(type, JSON.stringify(value));
	} catch (error) {
		console.log(error);
	}
};

/**
 * This function retrieves the data from AsyncStorage.
 *
 * @param {String} type
 */
const retrieveData = async type => {
	try {
		const data = await AsyncStorage.getItem(type);
		return JSON.parse(data);
	} catch (error) {
		console.log(error);
		return {};
	}
};

/**
 * This function clears the data of AsyncStorage.
 */
const clearData = async type => {
	try {
		await AsyncStorage.removeItem(type);
	} catch (error) {
		console.log(error);
	}
};

export default OfflineStore;
export {
	USER_PROFILE,
	HOME_PAGE_CONTEXT,
	HOME_PAGE_BANNERS,
	HOME_PAGE_TRENDING_APPS,
	SERVICES,
	PARTNERS,
	ACCESS_16_DIGIT_CODE,
	saveData,
	retrieveData,
	clearData
};

/**
 * Default behaviour -> Get Data from API
 * Fallback behaviour -> AsyncStorage
 *
 * Push the API call in array
 *
 * Listen for Network availability
 */
