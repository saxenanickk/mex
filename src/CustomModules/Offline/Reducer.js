import { ADD_TO_OFFLINE_QUEUE, EMPTY_OFFLINE_QUEUE } from "./Saga";

const initialState = {
	queue: {}
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case ADD_TO_OFFLINE_QUEUE:
			return {
				...state,
				queue: {
					...state.queue,
					[action.payload.type]: action.payload
				}
			};
		case EMPTY_OFFLINE_QUEUE:
			return {
				...state,
				queue: {}
			};
		default:
			return state;
	}
};
