import { NativeModules } from "react-native";

const { SplashScreen: NativeSplashScreen } = NativeModules;

export const SplashScreen = {
	hide: () => {
		NativeSplashScreen.hide();
	}
};
