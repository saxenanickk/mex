import { NativeModules } from "react-native";
import {
	retrieveData,
	ACCESS_16_DIGIT_CODE,
	saveData,
	clearData
} from "../Offline";

let { Access } = NativeModules;

Access = {
	...Access,
	saveAccessCode: code => {
		saveData(ACCESS_16_DIGIT_CODE, code);
	},
	getLastUsedAccessCode: async () => {
		return await retrieveData(ACCESS_16_DIGIT_CODE);
	},
	removeAccessCode: () => clearData(ACCESS_16_DIGIT_CODE)
};

export default Access;
