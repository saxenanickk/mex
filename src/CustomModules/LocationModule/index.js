import React from "react";
import { Platform } from "react-native";
import RNAndroidLocationEnabler from "react-native-android-location-enabler";
import Geolocation from "@react-native-community/geolocation";
import { connect } from "react-redux";
import { saveLocation } from "./Saga";
import LocationApi from "./Api";
import Location from "./golocation";

class LocationModule extends React.Component {
	/**
	 * Default Props:
	 *
	 * onError: {{ function }} : Runs in case of error
	 * onLoad: {{ function }} : Runs in case of loading
	 * for location
	 */
	static defaultProps = {
		onError: () => {},
		onLoad: () => {},
		onLocationReceived: () => {}
	};

	componentDidMount() {
		if (Platform.OS === "ios") {
			Geolocation.getCurrentPosition(
				position => {
					this.props.onLoad();
					LocationApi.reverseGeocoding({
						latitude: position.coords.latitude,
						longitude: position.coords.longitude
					}).then(response => {
						let locality = null;
						let area = null;
						let address = null;
						if (
							response &&
							response.results &&
							response.results[0] &&
							response.results[0].address_components
						) {
							response.results[0].address_components.map((item, index) => {
								item.types.map((value, id) => {
									if (value === "locality") {
										locality = item.long_name;
									}
								});
							});
							area = response.results[0].address_components[3].short_name;
							address = response.results[0].formatted_address;
						}
						this.props.dispatch(
							saveLocation({
								status: true,
								latitude: position.coords.latitude,
								longitude: position.coords.longitude,
								area: area,
								locality: locality,
								address: address
							})
						);
						this.props.onLocationReceived({
							status: true,
							latitude: position.coords.latitude,
							longitude: position.coords.longitude
						});
					});
				},
				error => {
					console.log(error);
					if (this.props.location.status === null) {
						this.props.dispatch(
							saveLocation({
								status: false,
								latitude: null,
								longitude: null,
								locality: null,
								address: null
							})
						);
					}
					this.props.onError();
				}
			);
		} else {
			Location.checkLocationPermissions(
				result => {
					RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
						interval: 10000,
						fastInterval: 5000
					})
						.then(data => {
							this.props.onLoad();
							console.log("Prompt: ", data);
							/**
							 * The user has accepted to enable the location services
							 * Data can be :
							 * - "already-enabled" if the location services has been already enabled
							 * - "enabled" if user has clicked on OK button in the popup
							 */
							Location.getCurrentPosition(
								position => {
									console.log("position: ", position);
									LocationApi.reverseGeocoding({
										latitude: position.coords.latitude,
										longitude: position.coords.longitude
									}).then(response => {
										let locality;
										response.results[0].address_components.map(
											(item, index) => {
												item.types.map((value, id) => {
													if (value === "locality") {
														locality = item.long_name;
													}
												});
											}
										);
										this.props.dispatch(
											saveLocation({
												status: true,
												latitude: position.coords.latitude,
												longitude: position.coords.longitude,
												area:
													response.results[0].address_components[3].short_name,
												locality: locality,
												address: response.results[0].formatted_address
											})
										);
										this.props.onLocationReceived({
											status: true,
											latitude: position.coords.latitude,
											longitude: position.coords.longitude
										});
									});
								},
								error => {
									console.log(error);
									if (this.props.location.status === null) {
										this.props.dispatch(
											saveLocation({
												status: false,
												latitude: null,
												longitude: null,
												area: null,
												locality: null,
												address: null
											})
										);
									}
									this.props.onError();
								}
							);
						})
						.catch(err => {
							/**
							 * The user has not accepted to enable the location services or something went wrong during the process
							 * "err" : { "code" : "ERR00|ERR01|ERR02", "message" : "message"}
							 * codes :
							 *	- ERR00 : The user has clicked on Cancel button in the popup
							 *	- ERR01 : If the Settings change are unavailable
							 *	- ERR02 : If the popup has failed to open
							 */
							console.log("=> ", err);
							this.props.dispatch(
								saveLocation({
									status: false,
									latitude: null,
									longitude: null,
									area: null,
									locality: null,
									address: null
								})
							);
							this.props.onError();
						});
				},
				error => {
					if (this.props.location.status === null) {
						this.props.dispatch(
							saveLocation({
								status: false,
								latitude: null,
								longitude: null,
								area: null,
								locality: null,
								address: null
							})
						);
					}
					this.props.onError(error);
				}
			);
		}
	}

	render() {
		return null;
	}
}

function mapStatetoProps(state) {
	return {
		location: state.location
	};
}

export default connect(mapStatetoProps)(LocationModule);
