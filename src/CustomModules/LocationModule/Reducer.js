import {
	SAVE_LOCATION,
	SAVE_GLOBAL_ORIGIN,
	SAVE_GLOBAL_DESTINATION
} from "./Saga";
import AsyncStorage from "@react-native-community/async-storage";

const initialState = {
	status: null,
	latitude: null,
	longitude: null,
	locality: null,
	address: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case SAVE_LOCATION:
			const { status, latitude, longitude, locality, address } = action.payload;
			AsyncStorage.setItem(
				"origin",
				JSON.stringify({
					latitude,
					longitude,
					locality,
					address
				})
			).catch(error => {
				console.log("Error while saving the data: ", error);
			});
			return {
				...state,
				status,
				latitude,
				longitude,
				locality,
				address
			};
		case SAVE_GLOBAL_ORIGIN: {
			const { latitude, longitude, locality, address } = action.payload;
			console.log("xxxxxxx", "writing origin to storage", action.payload);

			AsyncStorage.setItem(
				"origin",
				JSON.stringify({
					latitude,
					longitude,
					locality,
					address
				})
			).catch(error => {
				console.log("Error while saving the data: ", error);
			});
			return state;
		}
		case SAVE_GLOBAL_DESTINATION: {
			const { latitude, longitude, locality, address } = action.payload;
			console.log("xxxxxxx", "writing destination to storage", action.payload);

			AsyncStorage.setItem(
				"destination",
				JSON.stringify({
					latitude,
					longitude,
					locality,
					address
				})
			).catch(error => {
				console.log("Error while saving the data: ", error);
			});
			return state;
		}
		default:
			return state;
	}
};
