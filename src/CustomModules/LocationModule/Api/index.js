import I18n from "../../../Assets/strings/i18n";
class LocationApi {
	/**
	 * Geocoding Api (Address => { latitude, longitude })
	 */
	geocoding(params) {
		let address = params.address;
		return new Promise(function(resolve, reject) {
			fetch(
				"https://maps.googleapis.com/maps/api/geocode/json?" +
					"key=AIzaSyDmxdLvK373Nbwa8EpXaf6E53bS3SNHn00&sensor=false&" +
					"address=" +
					address +
					"",
				{
					method: "GET"
				}
			)
				.then(response => {
					if (response.status === 200) {
						response.json().then(res => {
							if (res.status === "OK") {
								resolve(res);
							}
						});
					} else {
						reject(response);
					}
				})
				.catch(error => {
					console.log("This is error of API (Reverse Geocoding): ", error);
					reject(error);
				});
		});
	}

	/**
	 * Reverse Geocoding Api ({ latitude, longitude } => Address)
	 */
	reverseGeocoding(params) {
		let latitude = params.latitude;
		let longitude = params.longitude;

		return new Promise(function(resolve, reject) {
			fetch(
				"https://maps.googleapis.com/maps/api/geocode/json?" +
					`language=${I18n.currentLocale()}&key=AIzaSyDmxdLvK373Nbwa8EpXaf6E53bS3SNHn00&` +
					"latlng=" +
					latitude +
					"," +
					longitude +
					"",
				{
					method: "GET"
				}
			)
				.then(response => {
					if (response.status === 200) {
						response.json().then(res => {
							resolve(res);
						});
					} else {
						reject(response);
					}
				})
				.catch(err => {
					reject(err);
					console.log(err);
				});
		});
	}
}

export default new LocationApi();
