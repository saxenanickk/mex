import { takeLatest, call } from "redux-saga/effects";
import Api from "./Api";

/**
 * Constants
 */
export const GEOCODING = "GEOCODING";
export const REVERSE_GEOCODING = "REVERSE_GEOCODING";
export const SAVE_LOCATION = "SAVE_LOCATION";
export const SAVE_GLOBAL_ORIGIN = "SAVE_GLOBAL_ORIGIN";
export const SAVE_GLOBAL_DESTINATION = "SAVE_GLOBAL_DESTINATION";

/**
 * Action Creators
 */
export const geocoding = payload => ({ type: GEOCODING, payload });
export const reverseGeocoding = payload => ({
	type: REVERSE_GEOCODING,
	payload
});
export const saveLocation = payload => ({ type: SAVE_LOCATION, payload });
export const saveGlobalOrigin = payload => ({
	type: SAVE_GLOBAL_ORIGIN,
	payload
});
export const saveGlobalDestination = payload => ({
	type: SAVE_GLOBAL_DESTINATION,
	payload
});

/**
 * Saga
 */
export function* locationSaga(dispatch) {
	yield takeLatest(GEOCODING, handleGeocoding);
	yield takeLatest(REVERSE_GEOCODING, handleReverseGeocoding);
}

/**
 * Handler Function
 */
function* handleGeocoding(action) {
	try {
		let geocoding = yield call(Api.geocoding, action.payload);
		console.log("Geocoding Saga Data: ", geocoding);
		return geocoding;
	} catch (error) {
		console.log("Geocoding Saga Error: ", error);
	}
}

function* handleReverseGeocoding(action) {
	try {
		let reverseGeocoding = yield call(Api.reverseGeocoding, action.payload);
		console.log("Reverse Geocoding Saga Data: ", reverseGeocoding);
		return reverseGeocoding;
	} catch (error) {
		console.log("Reverse Geocoding Saga Error: ", error);
	}
}
