/**
 * MEX.
 * Author: Nikhil Saxena
 * @flow
 */

import React, { Component } from "react";
import { View, TouchableOpacity, Dimensions, Platform } from "react-native";
import FBSDK, {
	AccessToken,
	GraphRequest,
	GraphRequestManager
} from "react-native-fbsdk";
import { GoappTextRegular } from "../../Components/GoappText";
import styles from "./styles";
import GoAppAnalytics from "../../utils/GoAppAnalytics";

const { height } = Dimensions.get("window");
const { LoginManager } = FBSDK;

const FacebookButton = ({ onPress }) => (
	<TouchableOpacity activeOpacity={0.75} onPress={onPress}>
		<View style={styles.facebookButton}>
			<GoappTextRegular size={height / 50} color={"#FFFFFF"}>
				{"Login via Facebook"}
			</GoappTextRegular>
		</View>
	</TouchableOpacity>
);

const FBLoginCallback = async (error, result) => {
	if (error) {
		// Error
		console.log(error);
	} else {
		// Success
		console.log(result);
		LoginManager.logOut();
	}
};

Platform.OS === "ios"
	? null
	: LoginManager.setLoginBehavior("native_with_fallback");

export const FacebookLogout = async (fields, callback = FBLoginCallback) => {
	try {
		const accessData = await AccessToken.getCurrentAccessToken();
		// Create a graph request asking for user information
		if (accessData && accessData.accessToken) {
			const infoRequest = new GraphRequest(
				"/me/permissions/",
				{
					accessToken: accessData.accessToken,
					httpMethod: "DELETE"
				},
				callback.bind(this)
			);
			// Execute the graph request created above
			new GraphRequestManager().addRequest(infoRequest).start();
		}
	} catch (error) {
		console.log(error);
	}
};

export default class FacebookLogin extends Component {
	fblogin = async (fields, callback) => {
		GoAppAnalytics.trackWithProperties("login", {
			type: "facebook",
			status: "initiate"
		});
		const accessData = await AccessToken.getCurrentAccessToken();
		// Create a graph request asking for user information
		const infoRequest = new GraphRequest(
			"/me",
			{
				accessToken: accessData.accessToken,
				parameters: {
					fields: {
						string: fields
					}
				}
			},
			callback.bind(this)
		);
		// Execute the graph request created above
		new GraphRequestManager().addRequest(infoRequest).start();
	};

	FBLoginCallback = async (error, result) => {
		if (error) {
			// Error
			console.log(error);
			GoAppAnalytics.trackWithProperties("login", {
				type: "facebook",
				status: "fail"
			});
			this.props.onError(error);
		} else {
			// Success
			console.log(result);
			GoAppAnalytics.trackWithProperties("login", {
				type: "facebook",
				status: "success"
			});
			this.props.onSuccess(result);
		}
	};

	doLogin = async () => {
		try {
			const result = await LoginManager.logInWithPermissions([
				"public_profile",
				"email"
			]);

			if (result.isCancelled) {
				console.log("Login was cancelled");
			} else {
				this.fblogin("id, email, name", this.FBLoginCallback);
			}
		} catch (error) {
			console.log(error);
			this.props.onError(error);
		}
	};

	render() {
		return <FacebookButton onPress={() => this.doLogin()} />;
	}
}
