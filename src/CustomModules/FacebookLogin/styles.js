import { StyleSheet, Platform, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	facebookButton: {
		padding: width / 30,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#3956a0",
		borderRadius: height / 10,
		...Platform.select({
			android: { elevation: 3 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.2
			}
		})
	},
	facebookText: {
		color: "#5A5A5A",
		marginLeft: width / 30,
		fontSize: 16
	}
});

export default styles;
