import { NativeModules } from "react-native";
import GoAppAnalytics from "./../utils/GoAppAnalytics";
const NativeMiniApp = NativeModules.MiniApp;

const MiniApp = {
	launchApp: (navigation, app) => {
		if (navigation) {
			navigation.navigate(app);
		}
	},
	launchNativeApp: app => {
		NativeMiniApp.launchApp(app);
	},
	clearMiniApp: navigation => {
		if (navigation) {
			navigation.goBack();
		}
	},
	launchWebApp: (app, url = "") => {
		NativeMiniApp.launchWebApp(app, url);
	},
	launchPhoneVerifiedApp: (app, userProfile, navigation) => {
		if (userProfile.phoneverified && userProfile.phonenum) {
			if (app.toLowerCase() === "bbinstant") {
				NativeMiniApp.launchApp("bbinstant");
			} else {
				NativeMiniApp.launchWebApp(app, "");
			}
		} else {
			navigation.navigate("VerifyPhone", {
				navigateTo: app
			});
		}
	},
	launchCorporateVerifyApp: (app, userProfile, navigation, link = "") => {
		if (userProfile && userProfile.tenantdto) {
			const tenantDto = JSON.parse(userProfile.tenantdto);
			if (tenantDto.tenant_id !== null && tenantDto.tenant_id !== undefined) {
				if (link !== "") {
					navigation.navigate(app, link);
				} else {
					navigation.navigate(app);
				}
			} else {
				navigation.navigate("CorpVerify", {
					navigateTo: app
				});
			}
		} else {
			navigation.navigate("CorpVerify", {
				navigateTo: app
			});
		}
	},
	setSessionToken: token => {
		NativeMiniApp.setSessionToken(token);
	},
	setUserInfo: (
		name,
		email,
		phone,
		ss_user_id,
		profile_pic,
		gender,
		dob,
		groups_count
	) => {
		NativeMiniApp.setUserInfo(name, email, phone);
		if (ss_user_id) {
			GoAppAnalytics.init(`${ss_user_id}`, { email, name, phone });
			GoAppAnalytics.setAvatar(profile_pic);
			GoAppAnalytics.setGender(gender);
			GoAppAnalytics.setDOB(dob);
			GoAppAnalytics.setGroupsJoined(groups_count);
		}
	},
	setSiteInfo: (site_id, ref_site_id, site_name) => {
		NativeMiniApp.setSiteInfo(site_id, ref_site_id, site_name);
		//Set site super props in case the user doesnot have site selected or changes site
		GoAppAnalytics.setSuperProperties({
			site_id: site_id,
			ref_site_id: ref_site_id,
			site_name: site_name
		});
		GoAppAnalytics.setPeopleProperties({
			site_id: site_id,
			ref_site_id: ref_site_id,
			site_name: site_name
		});
	},
	setTenantInfo: (tenant_id, tenant_name) => {
		NativeMiniApp.setTenantInfo(`$(tenant_id)`, tenant_name);
		//Set site super props in case the user doesnot have site selected or changes site
		GoAppAnalytics.setSuperProperties({ tenant_id, tenant_name });
		GoAppAnalytics.setPeopleProperties({ tenant_id, tenant_name });
	}
};

export default MiniApp;
