import { NativeModules } from "react-native";

const IOSWifi = NativeModules.IOSWifi;

const wifiConnect = ssid => IOSWifi.connectToSSID(ssid);

const wifiDisconnect = ssid => IOSWifi.disconnectFromSSID(ssid);

const wifiCurrentSSID = () => IOSWifi.currentSSID();

export { wifiConnect, wifiDisconnect, wifiCurrentSSID };
