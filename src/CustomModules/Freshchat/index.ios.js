import { NativeModules } from "react-native";

class Freshchat {
	launchSupportChat() {
		NativeModules.MiniApp.launchSupportChat();
	}
}

export default new Freshchat();
