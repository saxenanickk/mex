import { Freshchat as FC, FreshchatConfig } from "react-native-freshchat-sdk";

const FRESHCHAT_APP_ID = "8513eeeb-e972-4c2b-be05-1ae46bfe326c";
const FRESHCHAT_APP_KEY = "971234e2-9e27-4b19-a53e-4bbd01db7898";

var freshchatConfig = new FreshchatConfig(FRESHCHAT_APP_ID, FRESHCHAT_APP_KEY);

FC.init(freshchatConfig);

class Freshchat {
	launchSupportChat() {
		FC.showConversations();
	}
}

export default new Freshchat();
