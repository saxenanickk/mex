import { takeLatest, call, put } from "redux-saga/effects";
import Api from "../../Containers/Goapp/Api";
import MiniApp from "../MiniApp";
import { saveData, USER_PROFILE } from "../Offline";

export const GET_APP_TOKEN = "GET_APP_TOKEN";
export const SAVE_APP_TOKEN = "SAVE_APP_TOKEN";
export const MEX_GET_PROFILE = "MEX_GET_PROFILE";
export const MEX_SAVE_USER_PROFILE = "MEX_SAVE_USER_PROFILE";
export const MEX_SAVE_SELECTED_SITE = "MEX_SAVE_SELECTED_SITE";
export const MEX_PROFILE_LOADING = "MEX_PROFILE_LOADING";

export const getAppToken = payload => ({ type: GET_APP_TOKEN, payload });

export const saveAppToken = payload => ({ type: SAVE_APP_TOKEN, payload });

export const mexGetProfile = payload => ({
	type: MEX_GET_PROFILE,
	payload
});

export const mexSaveUserProfile = payload => ({
	type: MEX_SAVE_USER_PROFILE,
	payload
});

export const mexSaveSelectedSite = payload => ({
	type: MEX_SAVE_SELECTED_SITE,
	payload
});

export const mexProfileLoading = payload => ({
	type: MEX_PROFILE_LOADING,
	payload
});

export function* appTokenSaga(dispatch) {
	yield takeLatest(GET_APP_TOKEN, handleGetAppToken);
	yield takeLatest(MEX_GET_PROFILE, handleMexGetProfile);
}

function* handleGetAppToken(action) {}

const getMiniUserInfo = userProfile => {
	return new Promise(function(resolve, reject) {
		try {
			let obj = {};
			obj.contact = userProfile.phonenum;
			obj.email = userProfile.email;
			obj.name = userProfile.name;
			obj.site_id = userProfile.site_info ? userProfile.site_info.site_id : "";
			obj.site_name = userProfile.site_info ? userProfile.site_info.name : "";
			const tenantDto = userProfile.tenantdto
				? JSON.parse(userProfile.tenantdto)
				: undefined;
			const userdevices = userProfile.userdevices
				? JSON.parse(userProfile.userdevices)
				: undefined;
			obj.ss_user_id = userdevices ? userdevices[0].userId : undefined;
			obj.tenant_name = tenantDto ? tenantDto.name : "";
			obj.tenant_id = tenantDto ? tenantDto.tenant_id : "";
			resolve(obj);
		} catch (error) {
			resolve({
				contact: "",
				email: "",
				name: "",
				site_id: "",
				site_name: "",
				tenant_id: "",
				tenant_name: ""
			});
		}
	});
};

/**
 * Handlers
 */

function* handleMexGetProfile(action) {
	try {
		const resp = yield call(Api.getMyProfile, action.payload);
		if (resp.success === 1 && resp.data !== null) {
			const user = yield call(getMiniUserInfo, resp.data);
			MiniApp.setUserInfo(
				resp.data.name,
				resp.data.email,
				resp.data.phonenum,
				user.ss_user_id,
				resp.data.image,
				resp.data.gender,
				resp.data.date_of_birth,
				resp.data.club_info ? resp.data.club_info.length : 0
			);

			MiniApp.setTenantInfo(user.tenant_id, user.tenant_name);
			// For offline usage
			saveData(USER_PROFILE, { userProfile: resp.data, user });
			yield put(mexSaveUserProfile({ userProfile: resp.data, user }));
		}
	} catch (error) {
		console.log(error);
		// TODO
	} finally {
		yield put(mexProfileLoading(false));
	}
}
