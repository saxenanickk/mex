import {
	SAVE_APP_TOKEN,
	MEX_SAVE_USER_PROFILE,
	MEX_SAVE_SELECTED_SITE,
	MEX_GET_PROFILE,
	MEX_PROFILE_LOADING
} from "./Saga";

const initialState = {
	token: null,
	user: null,
	userProfile: null,
	selectedSite: null,
	isLoading: false
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case MEX_GET_PROFILE:
			return { ...state, isLoading: true };
		case SAVE_APP_TOKEN:
			return {
				...state,
				token: action.payload.token,
				user: action.payload.user
			};
		case MEX_SAVE_USER_PROFILE:
			return {
				...state,
				userProfile: action.payload.userProfile,
				user: action.payload.user
			};
		case MEX_SAVE_SELECTED_SITE:
			return {
				...state,
				selectedSite: action.payload
			};
		case MEX_PROFILE_LOADING:
			return {
				...state,
				isLoading: action.payload
			};
		default:
			return state;
	}
};
