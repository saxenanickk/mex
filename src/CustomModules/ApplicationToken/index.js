import React from "react";
import { connect } from "react-redux";
import { saveAppToken } from "./Saga";
import Token from "./token";

class ApplicationToken extends React.Component {
	componentDidMount() {
		!this.props.appToken &&
			Token.getUserTokenAndInfo(
				(
					appToken,
					name,
					email,
					phone,
					site_id,
					site_name,
					tenant_id,
					tenant_name
				) => {
					// Token.logTokenResponse(appToken + name + email + phone);
					console.log(
						"APP TOKEN: ",
						appToken,
						name,
						email,
						phone,
						site_id,
						site_name,
						tenant_id,
						tenant_name
					);
					if (appToken !== null) {
						this.props.dispatch(
							saveAppToken({
								token: appToken,
								user: {
									name: name,
									email: email,
									contact: phone,
									site_id,
									site_name,
									tenant_id,
									tenant_name
								}
							})
						);
					} else {
						this.props.dispatch(
							saveAppToken({
								token:
									"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJjMmFmMGFhMC1lZTNjLTExZTgtODQxOC0zMDdmMzJlYWE5NjciLCJwYXJ0bmVyX2lkIjoiNTM2MzExNTAtZWUzYy0xMWU4LTk3YzgtNmY4ZTEzMTZhOTE1IiwidGltZXN0YW1wIjoiMjAxOC0xMi0wNFQxMjozNjoyOS41NzBaIiwiaWF0IjoxNTQzOTI2OTg5fQ.FIfBh0OMq_ZwFl7epWwGBLuHCGy383U2TBfKcZKEfkY",
								user: {
									name: "Platify",
									email: "shashi.shekhar@quantcommune.com",
									contact: "8757706108"
								}
							})
						);
					}
				}
			);
	}

	render() {
		return null;
	}
}

function mapStatetoProps(state) {
	return {
		appToken: state.appToken.token
	};
}

export default connect(mapStatetoProps)(ApplicationToken);
