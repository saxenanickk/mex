import React, { Component } from "react";
import { TouchableOpacity, View, Dimensions } from "react-native";
import { GoogleSignin } from "@react-native-community/google-signin";
import Config from "react-native-config";
import { GoappTextRegular } from "../../Components/GoappText";
import styles from "./styles";
import GoAppAnalytics from "../../utils/GoAppAnalytics";

const { height } = Dimensions.get("window");

async function googleConfigure() {
	// Add any configuration settings here:
	await GoogleSignin.configure({
		webClientId: Config.WEB_CLIENT_ID,
		iosClientId: Config.IOS_CLIENT_ID
	});
}

export const signOut = async () => {
	try {
		await GoogleSignin.signOut();
	} catch (error) {
		console.error(error);
	}
};

// Calling this function will open Google for login.
export const doLogin = async props => {
	try {
		GoAppAnalytics.trackWithProperties("login", {
			type: "google",
			status: "initiate"
		});
		const data = await GoogleSignin.signIn();
		console.log(data);
		props.onSuccess(data);
	} catch (error) {
		console.error(error);
		GoAppAnalytics.trackWithProperties("login", {
			type: "google",
			status: "fail"
		});
		props.onError(error);
	}
};

const GoogleButton = ({ onPress }) => (
	<TouchableOpacity activeOpacity={0.75} onPress={onPress}>
		<View style={styles.googleButton}>
			<GoappTextRegular size={height / 50} color={"#FFFFFF"}>
				{"Login via Google"}
			</GoappTextRegular>
		</View>
	</TouchableOpacity>
);
export default class GoogleLogin extends Component {
	constructor() {
		super();
		googleConfigure();
	}

	render() {
		return <GoogleButton onPress={() => doLogin(this.props)} />;
	}
}
