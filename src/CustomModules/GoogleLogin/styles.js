import { StyleSheet, Platform, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	googleButton: {
		padding: width / 30,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#2c98f0",
		borderRadius: height / 10,
		...Platform.select({
			android: { elevation: 3 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.2
			}
		})
	}
});

export default styles;
