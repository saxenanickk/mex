import React from "react";
import NetInfo from "@react-native-community/netinfo";
import { connect } from "react-redux";
import { netInfo } from "./Saga";
import { dispatchOfflineQueue } from "../Offline/Saga";

class NetInfoModule extends React.Component {
	componentDidMount() {
		NetInfo.addEventListener(this.handleConnectivityChange);
	}

	componentWillUnmount() {
		// NetInfo.removeEventListener(this.handleConnectivityChange);
	}

	handleConnectivityChange = ({ isConnected }) => {
		if (isConnected === true && isConnected !== this.props.isInternet) {
			if (Object.keys(this.props.offlineQueue).length) {
				this.props.dispatch(dispatchOfflineQueue());
			}
			this.props.dispatch(
				netInfo({
					status: true
				})
			);
		} else if (isConnected !== true && isConnected !== this.props.isInternet) {
			this.props.dispatch(
				netInfo({
					status: false
				})
			);
		}
	};

	render() {
		return null;
	}
}

const mapStateToProps = state => {
	return {
		isInternet: state.netInfo.status,
		offlineQueue: state.offline.queue
	};
};

export default connect(mapStateToProps)(NetInfoModule);
