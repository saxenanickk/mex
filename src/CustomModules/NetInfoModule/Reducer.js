import { NET_INFO } from "./Saga";

const initialState = {
	status: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case NET_INFO:
			return {
				...state,
				status: action.payload.status
			};
		default:
			return state;
	}
};
