export const getKeyboardType = string => {
	const availableTypes = [
		"default",
		"number-pad",
		"decimal-pad",
		"numeric",
		"email-address",
		"phone-pad"
	];
	if (availableTypes.includes(string)) {
		return string;
	}
	return "default";
};
