import { take, cancel, fork } from "redux-saga/effects";
const ABORT_ON_UNMOUNT = "ABORT_ON_UNMOUNT";
const takeLatestCancellable = (patternOrChannel, saga, ...args) =>
	fork(function*() {
		let lastTask;
		while (true) {
			const action = yield take(patternOrChannel);
			if (action.payload === ABORT_ON_UNMOUNT) {
				yield cancel(lastTask);
				continue;
			}
			if (lastTask) {
				yield cancel(lastTask); // cancel is no-op if the task has already terminated
			}
			lastTask = yield fork(saga, ...args.concat(action));
		}
	});

export { takeLatestCancellable, ABORT_ON_UNMOUNT };
