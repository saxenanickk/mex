/**
 * Returns true if valid
 */
export function validateEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

export function validatePhoneNumber(number) {
	var re = /^\d{10}$/;
	return re.test(number);
}

export function validateInternationalPhoneNumber(isd_code, number) {
	let isUsdValid = validateIsdCode(isd_code);
	if (isUsdValid) {
		var re = /^\d{8,14}$/;
		let result = re.test(number) ? true : false;
		return result;
	} else {
		return false;
	}
}

export function validatePhoneNumberDigit(number) {
	var re = /([+]?\d{2,4}[.-\s]?)?(\d{10})/g;
	return re.test(number) ? true : false;
}

export function validateIsdCode(isd_code) {
	var re = /^[\+]\d{2}$/;
	var re1 = /^\d{3}$/;
	return re.test(isd_code) ? true : re1.test(isd_code) ? true : false;
}

export function validateNumber(number) {
	var re = /^\d+$/;
	return re.test(number);
}

export function validateDecimalNumber(number) {
	var re = /^\d+\.?\d{0,2}$/;
	return re.test(number);
}

export function validateSpecialChars(string) {
	var re = /[ `~!@#$%^&₹*()_+\=\[\]{};':"\\|,.<>\/?]/;
	return re.test(string);
}

export function validateAlphaNumeric(string) {
	var re = /[^A-Za-z0-9]+/g;
	return re.test(string);
}
