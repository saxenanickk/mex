export const REQUEST = "REQUEST";
export const SUCCESS = "SUCCESS";
export const FAILURE = "FAILURE";

/**
 * creates action constants
 * @param {String} base
 */
export const createRequestTypes = base => {
	return [REQUEST, SUCCESS, FAILURE].reduce((acc, type) => {
		acc[type] = `${base}_${type}`;
		return acc;
	}, {});
};

/**
 *
 * @param {String} type Action Name
 * @param {Object} payload
 * @returns {{type: string, payload: Object}}
 */
export const action = (type, payload = {}) => {
	return { type, payload };
};
