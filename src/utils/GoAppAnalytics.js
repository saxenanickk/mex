import MixPanel, { MixpanelInstance } from "react-native-mixpanel";
import firebase from "react-native-firebase";
import Config from "react-native-config";

const { MIX_PANEL_TOKEN, SERVER_TYPE } = Config;

class Analytics {
	parseArray(value) {
		let parsedValue = "";
		value.map(item => {
			parsedValue += item + ",";
		});
		return parsedValue;
	}

	async init(userId, userProps) {
		const mixpanel = new MixpanelInstance(MIX_PANEL_TOKEN);
		await mixpanel.initialize();
		MixPanel.identify(userId);
		if (SERVER_TYPE === "live") {
			firebase.analytics().setUserId(userId);
		}
		const { email, name, phone } = userProps;
		if (email) {
			MixPanel.set({ $email: email });
		}
		if (name) {
			MixPanel.set({ $name: name });
		}
		if (phone) {
			MixPanel.set({ $phone: phone });
		}
	}

	reset() {
		MixPanel.reset();
	}

	trackWithProperties(eventName, property) {
		Object.keys(property).map(key => {
			if (property[key] instanceof Array) {
				property[key] = this.parseArray(property[key]);
			}
		});
		console.log("tracking event with", eventName, JSON.stringify(property));
		MixPanel.sharedInstanceWithToken(MIX_PANEL_TOKEN).then(function(result) {
			MixPanel.trackWithProperties(eventName, property);
		});
	}

	trackChargeWithProperties(amount, orderId) {
		MixPanel.sharedInstanceWithToken(MIX_PANEL_TOKEN).then(function(result) {
			MixPanel.trackChargeWithProperties(amount, { orderID: orderId });
		});
	}

	setPageView(appName, screenName) {
		this.trackWithProperties("page_view", {
			application: appName,
			screen: screenName
		});
	}

	setSuperProperties(properties) {
		MixPanel.sharedInstanceWithToken(MIX_PANEL_TOKEN).then(function(result) {
			MixPanel.registerSuperProperties(properties);
		});
	}

	setAvatar(url) {
		if (url) {
			MixPanel.sharedInstanceWithToken(MIX_PANEL_TOKEN).then(function(result) {
				MixPanel.set({ $avatar: url });
			});
		}
	}

	setGender(gender) {
		if (gender) {
			MixPanel.sharedInstanceWithToken(MIX_PANEL_TOKEN).then(function(result) {
				MixPanel.set({ Gender: gender });
			});
		}
	}

	setDOB(dob) {
		if (!dob) {
			return;
		}
		let [date, month, year] = dob.split("/");
		if (!date || !month || !year) {
			return;
		}

		MixPanel.sharedInstanceWithToken(MIX_PANEL_TOKEN).then(function(result) {
			MixPanel.set({
				"Date Of Birth": new Date(year, month, date).toISOString()
			});
		});
	}

	setGroupsJoined(number) {
		if (number) {
			MixPanel.sharedInstanceWithToken(MIX_PANEL_TOKEN).then(function(result) {
				MixPanel.set({ "Joined Community Groups": number });
			});
		}
	}

	setPeopleProperties(properties) {
		MixPanel.sharedInstanceWithToken(MIX_PANEL_TOKEN).then(function(result) {
			MixPanel.set(properties);
		});
	}

	logChangeOfScreen = (appName, currentScreen) => {
		try {
			this.setPageView(appName, currentScreen);
		} catch (e) {
			console.log("error while navigation tracking", e);
		}
	};

	logChangeOfScreenInStack = (appName, prevState, currState) => {
		try {
			let prevStateRouteLength = prevState.routes.length;
			let currStateRouteLength = currState.routes.length;
			if (prevStateRouteLength > 0 && currStateRouteLength > 0) {
				let prevRoute = prevState.routes[prevStateRouteLength - 1];
				let currRoute = currState.routes[currStateRouteLength - 1];
				if (prevRoute.routeName !== currRoute.routeName) {
					this.setPageView(appName, currRoute.routeName);
				}
			}
		} catch (error) {
			console.log("error while navigation tracking", error);
		}
	};

	logChangeOfScreenInDrawer = (appName, prevState, currState) => {
		try {
			let prevStateRouteIndex = prevState.index;
			let currStateRouteIndex = currState.index;
			if (prevStateRouteIndex !== currStateRouteIndex) {
				let prevRoute = prevState.routes[prevStateRouteIndex];
				let currRoute = currState.routes[currStateRouteIndex];
				if (prevRoute.routeName !== currRoute.routeName) {
					this.setPageView(appName, currRoute.routeName);
				}
			}
		} catch (error) {
			console.log("error while navigation tracking", error);
		}
	};
}

const GoAppAnalytics = new Analytics();

export default GoAppAnalytics;

export const mpAppLaunch = params =>
	GoAppAnalytics.trackWithProperties("app_launch", { ...params });
