/* eslint-disable radix */
export const getSince = (currentTimestamp, timestamp) => {
	let diff = currentTimestamp - timestamp;
	const min = 1000 * 60;
	const hour = min * 60;
	const day = hour * 24;
	const week = day * 7;
	const month = day * 30;
	const year = day * 365;

	if (diff < min) {
		return "few moments ago";
	}

	if (diff > year) {
		let time = parseInt(diff / month);
		return `${time} year${time === 1 ? "" : "s"} ago`;
	}
	if (diff > month) {
		let time = parseInt(diff / month);
		return `${time} month${time === 1 ? "" : "s"} ago`;
	}

	if (diff > week) {
		let time = parseInt(diff / week);
		return `${time} week${time === 1 ? "" : "s"} ago`;
	}

	if (diff > day) {
		let time = parseInt(diff / day);
		return `${time} day${time === 1 ? "" : "s"} ago`;
	}
	if (diff > hour) {
		let time = parseInt(diff / hour);
		return `${time} hour${time === 1 ? "" : "s"} ago`;
	}
	if (diff > min) {
		let time = parseInt(diff / min);
		return `${time} minute${time === 1 ? "" : "s"} ago`;
	}
};
