import React from "react";
import { ABORT_ON_UNMOUNT } from "./sagaHelperAction";
/**
 * this HOC will help to cancel sagas on unmounting of the WrappedComponent
 * points to remember while using is always use withCancellableSaga inside connect()
 * @param WrappedComponent
 * @param args
 */
function withCancellableSaga(WrappedComponent, args) {
	return class extends React.Component {
		constructor(props) {
			super(props);
			this.state = {};
		}

		componentWillUnmount() {
			if (args.actions && args.actions.length > 0) {
				args.actions.forEach(action => {
					this.props.dispatch(action(ABORT_ON_UNMOUNT));
				});
			}
		}

		render() {
			return <WrappedComponent {...this.props} />;
		}
	};
}
export default withCancellableSaga;
