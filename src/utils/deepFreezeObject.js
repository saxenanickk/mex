// Object.freeze() will not freeze nested objects. The function below
// will freeze all nested objects for immutability.

let isObject = val => val && typeof val === "object";
function deepFreezeObject(obj) {
	if (isObject(obj) && !Object.isFrozen(obj)) {
		// Recusively call until all child objects are frozen
		Object.keys(obj).forEach(name => deepFreezeObject(obj[name]));
		Object.freeze(obj);
	}
	return obj;
}

export default deepFreezeObject;
