export const getUrlParams = url => {
	let hashes = url.slice(url.indexOf("?") + 1).split("&");
	let params = {};
	hashes.map(hash => {
		let [key, val] = hash.split("=");
		params[key] = decodeURIComponent(val);
	});

	return params;
};
