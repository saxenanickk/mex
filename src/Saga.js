import { fork, all } from "redux-saga/effects";
import { locationSaga } from "./CustomModules/LocationModule/Saga";
import { appTokenSaga } from "./CustomModules/ApplicationToken/Saga";
import communitySaga from "./Containers/Community/Saga";
import redbusSaga from "./Containers/Redbus/Saga";
import eventsSaga from "./Containers/Events/Saga";
import freewifiSaga from "./Containers/FreeWiFi/Saga";
import igniteSaga from "./Containers/Ignite/Saga";
import loginSaga from "./Containers/LoginScreen/Saga";
import offersSaga from "./Containers/Offers/Saga";
import olaSaga from "./Containers/Ola/Saga";
import orderSaga from "./Containers/Order/Saga";
import clearTripSaga from "./Containers/Cleartrip/Saga";
import clearTripHotelSaga from "./Containers/CleartripHotel/Saga";
import goappSaga from "./Containers/Goapp/Saga";
import pollingSaga from "./Containers/Polling/Saga";
import vmsSaga from "./Containers/Vms/Saga";
import feedSaga from "./Components/Feed/Saga";
import servicesSaga from "./Components/Service/Saga";
import partnersSaga from "./Components/Partners/Saga";
import campusAssistSaga from "./Containers/CampusAssist/Saga";
import { offlineSaga } from "./CustomModules/Offline/Saga";

export const MEX_SAVE_GLOBAL_NAVIGATION_STATE =
	"MEX_SAVE_GLOBAL_NAVIGATION_STATE";

export const mexSaveGlobalNavigationState = payload => ({
	type: MEX_SAVE_GLOBAL_NAVIGATION_STATE,
	payload
});

export default function* rootSaga(dispatch) {
	yield all([
		fork(locationSaga),
		fork(appTokenSaga),
		fork(communitySaga),
		fork(redbusSaga),
		fork(eventsSaga),
		fork(freewifiSaga),
		fork(igniteSaga),
		fork(loginSaga),
		fork(offersSaga),
		fork(olaSaga),
		fork(orderSaga),
		fork(clearTripSaga),
		fork(clearTripHotelSaga),
		fork(goappSaga),
		fork(vmsSaga),
		fork(pollingSaga),
		fork(feedSaga),
		fork(servicesSaga),
		fork(partnersSaga),
		fork(offlineSaga),
		fork(campusAssistSaga)
	]);
}
