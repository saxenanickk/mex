import { combineReducers } from "redux";

import { reducer as appToken } from "./CustomModules/ApplicationToken/Reducer";
import { reducer as netInfo } from "./CustomModules/NetInfoModule/Reducer";
import { reducer as location } from "./CustomModules/LocationModule/Reducer";
import { reducer as feed } from "./Components/Feed/Reducer";
import { reducer as services } from "./Components/Service/Reducer";
import { reducer as partners } from "./Components/Partners/Reducer";
import { reducer as offline } from "./CustomModules/Offline/Reducer";

export default function reducer(asyncReducers) {
	return combineReducers({
		appToken,
		netInfo,
		location,
		feed,
		services,
		partners,
		offline,
		...asyncReducers
	});
}
