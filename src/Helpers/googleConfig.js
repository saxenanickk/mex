import { GoogleSignin } from "react-native-google-signin";
import Config from "../../Constants/config";

const googleConfig = {
	iosClientId: Config.GOOGLESIGNIN_IOSCLINTID,
	webClientId: Config.GOOGLESIGNIN_WEBCLIENTID
};

GoogleSignin.configure(googleConfig);

export default GoogleSignin;
