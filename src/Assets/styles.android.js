import { Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const fontFamily_light = "LatoLight";
export const fontFamily = "LatoRegular";
export const fontFamily_medium = "LatoMedium";
export const fontFamily_semi_bold = "LatoBold";
export const fontFamily_monserrat = "LatoRegular";
export const fontFamily_monserrat_medium = "LatoMedium";
export const fontFamily_monserrat_light = "LatoLight";
export const fontFamily_monserrat_semi_bold = "LatoSemiBold";
export const fontFamily_monserrat_bold = "LatoBold";
export const icon_moon_font = "Icomoon";

export const font_one = "LatoLight";
export const font_two = "LatoRegular";
export const font_three = "LatoMedium";
export const font_four = "LatoBold";
export const font_five = "LatoLight";
export const font_six = "LatoRegular";
export const font_seven = "LatoMedium";
export const font_eight = "LatoSemiBold";
export const font_nine = "LatoBold";
export const font_ten = "LatoThin";

export const headerStyle = {
	width: width,
	height: height / 10,
	backgroundColor: "#000000",
	elevation: 5,
	flexDirection: "row",
	alignItems: "center",
	padding: width / 35
};
