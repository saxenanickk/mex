import { Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const fontFamily_light = "Avenir";
export const fontFamily = "Avenir";
export const fontFamily_medium = "Avenir";
export const fontFamily_semi_bold = "Avenir";
export const fontFamily_monserrat = "Avenir";
export const fontFamily_monserrat_medium = "Avenir";
export const fontFamily_monserrat_light = "Avenir";
export const fontFamily_monserrat_semi_bold = "Avenir";
export const fontFamily_monserrat_bold = "Avenir";
export const icon_moon_font = "icomoon";

export const font_one = "Avenir";
export const font_two = "Avenir";
export const font_three = "Avenir";
export const font_four = "Avenir";
export const font_five = "Avenir";
export const font_six = "Avenir";
export const font_seven = "Avenir";
export const font_eight = "Avenir";
export const font_nine = "Avenir";
export const font_ten = "Avenir";
export const headerStyle = {
	width: width,
	height: height / 10,
	backgroundColor: "#000000",
	elevation: 5,
	flexDirection: "row",
	alignItems: "center",
	padding: width / 35
};
