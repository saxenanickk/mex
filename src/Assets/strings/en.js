export default {
	/**
	 * Common Strings in whole App
	 */
	from: "From",
	delete: "Delete",
	done_text: "Done",
	where_to: "Where To ?",
	try_again: "Please try again later",
	coming_soon: "Coming Soon. Use live apps.",
	press_again_to_go_back: "Press again to go back.",
	feature_not_available: "Feature is not available in demo app.",
	internet_found: "Internet is available now.",
	internet_not_found: "Internet connectivity is not available.",
	locale: "en_IN",
	help: "Help",
	error: "Error",
	internet_issue: "Internet issue",
	information: "Information",
	token_error: "Failed to Authenticate",

	/**
	 * MEX App Home Page Strings
	 */
	home: "HOME",
	cart: "CART",
	wallet: "WALLET",
	orders: "ORDERS",
	recent: "RECENT",
	unknown: "Unknown",
	all_apps: "All Apps",
	live_apps: "Live Apps",
	search_here: "Search Here",
	coming_soon_header: "Coming Soon",
	searching: "Searching....",
	ready_to_use: "Ready to use.",
	fully_native_apps: "Fully native apps.",
	no_need_to_download: "No need to download.",
	cab_movies_bills: "Cabs, Movies, Bills,",
	recharge_flight: "Recharge, Flights",
	and_more: "And more.",
	app_support: "Support",

	/**
	 * string for date translation in all app
	 */

	date: {
		formats: {
			post_date: "%B %d at %I:%M %p",
			normal_day: "%a, %d %b",
			header_day: "%d %b",
			date_of_birth: "%d %b, %Y"
		}
	},

	/*
	 *string for question plural singular
	 */
	question: {
		one: "Question",
		other: "Questions",
		zero: "Question"
	},

	/*
	 *string for campus-assist
	 */

	reopen: "Re-open",
	comment: "Comment",
	no_issues_reported: "No Issues Reported!",
	report_issue: "Report Issue",
	cancel: "CANCEL",
	ok: "OK",
	Warning: "Warning"
};
