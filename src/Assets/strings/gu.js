export default {
	/**
	 * Common Strings in whole App
	 */
	from: "પ્રારંભ સ્થાન",
	delete: "કાઢી નાંખો",
	done_text: "થઈ ગયું",
	where_to: "ક્યાં સુધી?",
	try_again: "પછીથી ફરી પ્રયત્ન કરો",
	coming_soon: "ટૂક સમયમાં આવી રહ્યું છે. લાઇવ એપ્લિકેશન્સનો ઉપયોગ કરો",
	press_again_to_go_back: "પાછા જવા માટે ફરીથી દબાવો.",
	feature_not_available: "વિશેષતા ડેમો એપ્લિકેશનમાં ઉપલબ્ધ નથી",
	internet_found: "ઇન્ટરનેટ હવે ઉપલબ્ધ છે",
	internet_not_found: "ઇન્ટરનેટ કનેક્ટિવિટી ઉપલબ્ધ નથી.",
	locale: "gu",
	help: "મદદ",
	error: "ભૂલ",
	internet_issue: "ઇન્ટરનેટ સમસ્યા",
	information: "માહિતી",
	token_error: "પ્રમાણિત કરવામાં નિષ્ફળ",
	app_support: "સહાય",

	/**
	 * MEX App Home Page Strings
	 */
	home: "હોમ",
	cart: "કાર્ટ",
	wallet: "વૉલેટ",
	orders: "ઓર્ડર",
	recent: "હમણાનું",
	unknown: "અજ્ઞાત",
	all_apps: "બધા એપ્લિકેશન્સ",
	live_apps: "લાઇવ એપ્સ",
	search_here: "અહીં શોધો",
	coming_soon_header: "ટૂક સમયમાં આવી રહ્યું છે",
	searching: "શોધી રહ્યું છે ....",
	ready_to_use: "વાપરવા માટે તૈયાર.",
	fully_native_apps: "સંપૂર્ણપણે મૂળ એપ્લિકેશન્સ.",
	no_need_to_download: "ડાઉનલોડ કરવાની જરૂર નથી.",
	cab_movies_bills: "કેબ, મૂવીઝ, બિલ્સ,",
	recharge_flight: "રીચાર્જ, ફ્લાઇટ્સ",
	and_more: "અને વધુ."
};
