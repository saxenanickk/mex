export default {
	/**
	 * Common Strings in whole App
	 */
	from: "から",
	delete: "削除する",
	done_text: "完了",
	where_to: "どこへ ?",
	try_again: "後でもう一度やり直してください",
	coming_soon: "近日公開。Use live apps.",
	press_again_to_go_back: "もう一度押すと元に戻ります.",
	feature_not_available: "機能は利用できません",
	internet_found: "インターネットが利用可能になりました.",
	internet_not_found: "インターネット接続が利用できません.",
	locale: "ja",
	help: "助けて",
	error: "エラー",
	internet_issue: "インターネットの問題",
	information: "情報",
	token_error: "認証に失敗しました",
	app_support: "手伝う",

	/**
	 * MEX App Home Page Strings
	 */
	home: "家",
	cart: "カート",
	wallet: "財布",
	orders: "注文",
	recent: "最近",
	unknown: "道の",
	all_apps: "すべて アプリ",
	live_apps: "ライブ アプリ",
	search_here: "サーチ ここに",
	coming_soon_header: "到来 まもなく",
	searching: "検索しています....",
	ready_to_use: "使用する準備ができて。",
	fully_native_apps: "完全ネイティブアプリ",
	no_need_to_download: "ダウンロードする必要はありません。",
	cab_movies_bills: "タクシー、映画、請求書、",
	recharge_flight: "充電、フライト",
	and_more: "もっと。"
};
