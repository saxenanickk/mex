export default {
	/**
	 * Common Strings in whole App
	 */
	from: "कहाँ से",
	delete: "Delete",
	done_text: "ठीक है",
	where_to: "कहाँ जाएँगे ?",
	try_again: "बाद में पुन: प्रयास करें",
	coming_soon: "जल्द आ रहा है। लाइव ऐप का प्रयोग करें।",
	feature_not_available: "सुविधा डेमो ऐप में उपलब्ध नहीं है.",
	press_again_to_go_back: "वापस जाने के लिए फिर से दबाएं.",
	internet_found: "इंटरनेट अब उपलब्ध है।",
	internet_not_found: "इंटरनेट कनेक्टिविटी उपलब्ध नहीं है।",
	locale: "hi_IN",
	help: "मदद",
	error: "त्रुटि",
	internet_issue: "इंटरनेट की समस्या",
	information: "जानकारी",
	token_error: "प्रमाणित करने में विफल",
	app_support: "सहायता",

	/**
	 * MEX App Home Page Strings
	 */
	home: "होम",
	cart: "कार्ट",
	recent: "रीसेंट",
	wallet: "वॉलेट",
	orders: "ऑर्डर्स",
	unknown: "Unknown",
	live_apps: "लाइव ऐप्स",
	search_here: "यहाँ ढूँढे",
	all_apps: "सभी एप्लीकेशन",
	coming_soon_header: "जल्द आ रहा है",
	searching: "खोज कर....",
	ready_to_use: "उपयोग करने के लिए तैयार।",
	fully_native_apps: "पूरी तरह से देशी ऐप्स।",
	no_need_to_download: "डाउनलोड करने की आवश्यकता नहीं है।",
	cab_movies_bills: "टैक्सी, सिनेमा, बिल,",
	recharge_flight: "रिचार्ज, उड़ानें",
	and_more: "और अधिक।"
};
