import { NativeModules, Platform } from "react-native";
import I18n from "i18n-js";
import en from "./en";
import hi from "./hi";
import gu from "./gu";
import ja from "./ja";

I18n.defaultLocale = "en";
I18n.fallbacks = true;

if (Platform.OS === "android") {
	I18n.locale = NativeModules.I18nManager.localeIdentifier || "";
} else if (Platform.OS === "ios") {
	I18n.locale = NativeModules.SettingsManager.settings.AppleLocale || "";
}

if (I18n.locale.indexOf("_") > -1) {
	I18n.locale = I18n.locale.split("_")[0];
} else if (I18n.locale.indexOf("-") > -1) {
	I18n.locale = I18n.locale.split("-")[0];
}

I18n.translations = {
	en,
	hi,
	gu,
	ja
};

export default I18n;
