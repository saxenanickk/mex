import { all } from "redux-saga/effects";
import { vmsMeetingsListSaga } from "./Containers/Home/Saga";
import { vmsEditMeetingSaga } from "./Containers/EditMeeting/Saga";
import { vmsCreateMeetingSaga } from "./Containers/CreateMeeting/Saga";
import { vmsAddVisitorSaga } from "./Containers/AddVisitor/Saga";
import { vmsMeetingSaga } from "./Containers/InvitationDetails/Saga";
// Import different Sagas here

export default function* vmsSaga() {
	yield all([
		vmsMeetingsListSaga(),
		vmsEditMeetingSaga(),
		vmsCreateMeetingSaga(),
		vmsAddVisitorSaga(),
		vmsMeetingSaga()
	]);
}
