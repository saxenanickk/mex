import React from "react";
import { HeaderBackButton } from "@react-navigation/stack";
import { Stack } from "../../utils/Navigators";
import Home from "./Containers/Home";
import Splash from "./Containers/Splash";
import InvitationDetails from "./Containers/InvitationDetails";
import EditMeeting from "./Containers/EditMeeting";
import AddVisitor from "./Containers/AddVisitor";
import CreateMeeting from "./Containers/CreateMeeting";

let data = null;
// Todo refactor the header style code
const BackButton = ({ navigation }) => (
	<HeaderBackButton onPress={() => navigation.goBack()} />
);

const Navigator = () => (
	<Stack.Navigator
		headerMode="float"
		screenOptions={{ headerBackTitleVisible: false }}>
		<Stack.Screen
			options={{ headerShown: false }}
			name={"Splash"}
			component={props => <Splash {...data} {...props} />}
		/>
		<Stack.Screen
			options={({ navigation }) => ({
				title: "Invite Visitor",
				headerLeft: () => <BackButton navigation={navigation} />,
				headerForceInset: {
					top: "never",
					bottom: "never"
				}
			})}
			name={"Home"}
			component={Home}
		/>
		<Stack.Screen
			options={({ navigation }) => ({
				title: "Invitation Details",
				headerLeft: () => <BackButton navigation={navigation} />
			})}
			name={"InvitationDetails"}
			component={InvitationDetails}
		/>
		<Stack.Screen
			options={({ navigation }) => ({
				headerTitle: "Invite Visitor",
				headerLeft: () => <BackButton navigation={navigation} />,
				headerForceInset: {
					top: "never",
					bottom: "never"
				}
			})}
			name={"CreateMeeting"}
			component={CreateMeeting}
		/>
		<Stack.Screen
			options={({ route }) => ({
				title: "Edit Meeting",
				headerForceInset: {
					top: "never",
					bottom: "never"
				}
			})}
			name={"EditMeeting"}
			component={EditMeeting}
		/>
		<Stack.Screen name={"AddVisitor"} component={AddVisitor} />
	</Stack.Navigator>
);

const RegisterScreen = props => {
	data = props;
	return (
		<Navigator
			data={data}
			onNavigationStateChange={(prevState, currState) =>
				props.onNavigationStateChange(prevState, currState)
			}
		/>
	);
};

export default RegisterScreen;
