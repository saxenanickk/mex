import { VMS_GET_LOCATIONS, VMS_EDIT_MEETING } from "./Saga";
import { REQUEST, SUCCESS, FAILURE } from "../../../../utils/reduxUtils";

const initialState = {
	isLocationsLoading: false,
	locations: [],
	locationObject: {},
	isLocationsError: false,
	isEditMeetingLoading: false,
	isEditMeetingSuccess: null,
	isEditMeetingError: false
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case VMS_GET_LOCATIONS[REQUEST]:
			return { ...state, isLocationsLoading: true, isLocationsError: false };
		case VMS_GET_LOCATIONS[SUCCESS]:
			return {
				...state,
				isLocationsLoading: false,
				locations: payload.response.locations,
				locationObject: payload.response.locationObject
			};
		case VMS_GET_LOCATIONS[FAILURE]:
			return {
				...state,
				isLocationsLoading: false,
				isLocationsError: true
			};
		case VMS_EDIT_MEETING[REQUEST]:
			return {
				...state,
				isEditMeetingLoading: true,
				isEditMeetingError: false,
				isEditMeetingSuccess: null
			};
		case VMS_EDIT_MEETING[SUCCESS]:
			return {
				...state,
				isEditMeetingSuccess: payload.response,
				isEditMeetingLoading: false
			};
		case VMS_EDIT_MEETING[FAILURE]:
			return {
				...state,
				isEditMeetingError: true,
				isEditMeetingLoading: false
			};
		default:
			return state;
	}
};
