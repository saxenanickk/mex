import {
	createRequestTypes,
	action,
	REQUEST,
	SUCCESS,
	FAILURE
} from "../../../../utils/reduxUtils";
import VmsApi from "../../Api";
import { put, call, takeLatest } from "redux-saga/effects";
import { Action } from "redux";

/**
 * Constants
 */

export const VMS_EDIT_MEETING = createRequestTypes("VMS_EDIT_MEETING");
export const VMS_GET_LOCATIONS = createRequestTypes("VMS_GET_LOCATIONS");

/**
 * Action Creators
 */
export const vmsEditMeeting = {
	request: (appToken, body) =>
		action(VMS_EDIT_MEETING[REQUEST], { appToken, body }),
	success: response => action(VMS_EDIT_MEETING[SUCCESS], { response }),
	failure: error => action(VMS_EDIT_MEETING[FAILURE], { error })
};

export const vmsGetLocations = {
	request: (appToken, tenantId) =>
		action(VMS_GET_LOCATIONS[REQUEST], { appToken, tenantId }),
	success: response => action(VMS_GET_LOCATIONS[SUCCESS], { response }),
	failure: error => action(VMS_GET_LOCATIONS[FAILURE], error)
};

export function* vmsEditMeetingSaga(dispatch) {
	yield takeLatest(VMS_EDIT_MEETING[REQUEST], handleEditMeeting);
	yield takeLatest(VMS_GET_LOCATIONS[REQUEST], handleGetLocations);
}

/**
 * Handlers
 */

/**
 *
 * @param {Action} action
 */
function* handleEditMeeting(action) {
	try {
		let editMeetingResponse = yield call(VmsApi.editIvitation, action.payload);
		console.log(editMeetingResponse);
		if (editMeetingResponse.message === "Invite Updated Successfully") {
			yield put(vmsEditMeeting.success(true));
		} else {
			yield put(vmsEditMeeting.failure(false));
		}
	} catch (error) {
		yield put(vmsEditMeeting.failure(error));
	}
}

function* handleGetLocations(action) {
	try {
		let locations = yield call(VmsApi.tenantZoneDetails, action.payload);
		let locationObject = locations.reduce((acc, curVal) => {
			return { ...acc, [curVal.zoneId]: curVal.address };
		}, {});
		yield put(vmsGetLocations.success({ locations, locationObject }));
	} catch (error) {
		yield put(vmsGetLocations.failure(error));
	}
}
