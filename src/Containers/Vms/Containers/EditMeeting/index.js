import React, { Component } from "react";
import {
	View,
	TouchableOpacity,
	Dimensions,
	BackHandler,
	KeyboardAvoidingView
} from "react-native";
import { styles } from "./style";
import EditMeetingDetail from "../../Components/EditMeetingDetails";
import { GoappTextBold } from "../../../../Components/GoappText";
import { connect } from "react-redux";
import { vmsGetLocations, vmsEditMeeting } from "./Saga";
import { ProgressScreen, GoToast } from "../../../../Components";
import DialogContext from "../../../../DialogContext";
import { VERIFIED_CORPORATE } from "../../../../Assets/Img/Image";

const { width, height } = Dimensions.get("window");

/**
 * @typedef {Object} Props
 * @property {NavigationScreenProps} navigation
 */

/**
 * @augments {Component<Props, State>}
 */
class EditMeeting extends Component {
	constructor(props) {
		super(props);
		const { route } = this.props;
		const { item } = route.params ? route.params : {};

		const selectedLocation = item.zoneId
			? props.locationObject[item.zoneId]
			: null;
		this.state = {
			scheduleStart: item.scheduledStart,
			scheduleEnd: item.scheduledEnd,
			purpose: item.purpose,
			locations: [],
			selectedLocation: selectedLocation,
			isFormDirty: false,
			isValid: true,
			validationMsg: ""
		};
	}

	componentDidMount() {
		// Do this to know navigation in form edited or not
		this.props.navigation.setOptions({ handleBackPress: this.handleBackPress });
		if (this.props.locations.length === 0) {
			this.props.dispatch(
				vmsGetLocations.request(this.props.appToken, this.props.tenantId)
			);
		}
		BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
	}

	static getDerivedStateFromProps(nextProps, prevState) {
		let nextState = {};
		// Choose a location when locations are loaded
		if (
			nextProps.locations.length !== prevState.locations.length &&
			nextProps.locations.length > 0
		) {
			// If zone id is present select it or else select 1st zone
			const { route } = nextProps;
			const { item } = route.params ? route.params : {};

			if (item.zoneId) {
				nextState.selectedLocation = nextProps.locationObject[item.zoneId];
				nextState.locations = nextProps.locations;
			} else {
				nextState.selectedLocation = nextProps.locations[0].address;
				nextState.locations = nextProps.locations;
			}
		}
		return nextState;
	}

	componentDidUpdate(prevProps, prevState) {
		if (!prevProps.isEditMeetingError && this.props.isEditMeetingError) {
			GoToast.show("Sorry, we are unable to update invitation", "Error", 1000);
		}
		if (!prevProps.isEditMeetingSuccess && this.props.isEditMeetingSuccess) {
			this.props.route.params &&
				this.props.route.params.onBack &&
				this.props.route.params.onBack();
			this.props.navigation.goBack();
			this.context.current.openDialog({
				type: "Information",
				title: "Success!",
				message: "Invitation has been updated successfully.",

				icon: VERIFIED_CORPORATE
			});
		}
	}

	componentWillUnmount() {
		this.props.dispatch(vmsEditMeeting.success(null));
		BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
	}

	handleBackPress = () => {
		if (this.state.isFormDirty) {
			this.context.current.openDialog({
				type: "Confirmation",
				title: "Warning",
				message: "Do you want to discard changes?",
				rightPress: () => {
					this.props.navigation.goBack();
				}
			});
			return true;
		}
		this.props.navigation.goBack();
		return false;
	};

	handleDateChange = date => {
		// If user selects minimum date set time to current time
		if (
			new Date().getDate() === new Date(date).getDate() &&
			new Date().getMonth() === new Date(date).getMonth()
		) {
			this.setState({
				scheduleStart: Date.parse(new Date()),
				scheduleEnd: new Date(new Date()).setMinutes(
					new Date().getMinutes() + 10
				),
				isFormDirty: true
			});
		}
		// else preserve start time and end time
		else {
			let startTime = new Date(this.state.scheduleStart);
			let endTime = new Date(this.state.scheduleEnd);
			this.setState(
				{
					// scheduleStart: date,
					scheduleStart: new Date(date).setHours(
						startTime.getHours(),
						startTime.getMinutes()
					),
					scheduleEnd: new Date(date).setHours(
						endTime.getHours(),
						endTime.getMinutes()
					),
					isFormDirty: true
				},
				() => this.state.scheduleStart,
				"schedulestart"
			);
		}
	};

	handleStartTimeChange = date => {
		let startTime = new Date(date);
		let endTime = new Date(this.state.scheduleEnd);
		let meetingStartTime =
			new Date(this.state.scheduleStart).toDateString() +
			" " +
			new Date(startTime).toTimeString();
		if (Date.parse(endTime) < Date.parse(startTime)) {
			this.setState({
				scheduleStart: Date.parse(meetingStartTime),
				scheduleEnd: new Date(meetingStartTime).setMinutes(
					startTime.getMinutes() + 10
				),
				isFormDirty: true
			});
		} else {
			this.setState({
				scheduleStart: Date.parse(meetingStartTime),
				scheduleEnd: new Date(meetingStartTime).setMinutes(
					startTime.getMinutes() + 10
				),
				isFormDirty: true
			});
		}
	};

	handleEndTimeChange = date => {
		let meetingEndTime =
			new Date(this.state.scheduleEnd).toDateString() +
			" " +
			new Date(date).toTimeString();
		this.setState({
			scheduleEnd: Date.parse(meetingEndTime),
			isFormDirty: true
		});
	};

	handlePurposeChange = text => {
		let purposeRegex = /^[A-Za-z .]+$/;
		let isPurposeValid = purposeRegex.test(text);
		if (!isPurposeValid && text.length > 0) {
			GoToast.show(
				"Purpose cannot contain numbers and special characters",
				"Error"
			);
			this.setState({
				purpose: text,
				isFormDirty: true,
				isValid: false,
				validationMsg: "Pupose can't have special characters"
			});
		} else {
			this.setState({
				purpose: text,
				isFormDirty: true,
				isValid: true,
				validationMsg: ""
			});
		}
	};

	handleLocationChange = updatedLocation => {
		this.setState({
			selectedLocation: updatedLocation,
			isFormDirty: true
		});
	};

	handleUpdate = () => {
		const { route } = this.props;
		const { item } = route.params ? route.params : {};

		const { zoneId } = this.state.locations.find(location => {
			return location.address === this.state.selectedLocation;
		});
		if (!this.state.purpose.length > 0) {
			GoToast.show("Please add Purpose of the meeting", "Error", 2000);
			return;
		}
		let purposeRegex = /[A-Za-z .]+$/;
		let isPurposeValid = purposeRegex.test(this.state.purpose);
		if (!isPurposeValid) {
			GoToast.show("Purpose cannot contain special characters", "Error", 2000);
			return;
		}
		if (!zoneId) {
			GoToast.show("Failed to load Location of meeting", "Error", 2000);
			return;
		}
		this.props.dispatch(
			vmsEditMeeting.request(this.props.appToken, {
				scheduleStart: this.state.scheduleStart,
				scheduleEnd: this.state.scheduleEnd,
				hostId: this.props.hostId,
				invitationInfoId: item.invitationInfoId,
				zoneId: zoneId,
				purpose: this.state.purpose
			})
		);
	};

	render() {
		return (
			<KeyboardAvoidingView behavior={"position"}>
				<View style={styles.container}>
					<EditMeetingDetail
						date={this.state.scheduleStart}
						startTime={this.state.scheduleStart}
						endTime={this.state.scheduleEnd}
						purpose={this.state.purpose}
						locations={this.state.locations}
						selectedLocation={this.state.selectedLocation}
						isLocationsLoading={this.props.isLocationsLoading}
						isLocationsError={this.props.isLocationsError}
						handleLocationChange={(updatedLocation, index) =>
							this.handleLocationChange(updatedLocation, index)
						}
						handleDateChange={date => this.handleDateChange(date)}
						handlePurposeChange={purposeText =>
							this.handlePurposeChange(purposeText)
						}
						handleStartTimeChange={date => this.handleStartTimeChange(date)}
						handleEndTimeChange={date => this.handleEndTimeChange(date)}
					/>
					{/* </KeyboardAvoidingView> */}
					<TouchableOpacity
						disabled={!this.state.isFormDirty}
						style={[
							styles.updateButton,
							!this.state.isFormDirty ? { backgroundColor: "grey" } : {}
						]}
						onPress={this.handleUpdate}>
						<GoappTextBold color={"#fff"} size={20} style={styles.updateText}>
							Update & Send Invitation
						</GoappTextBold>
					</TouchableOpacity>
					{this.props.isEditMeetingLoading ? (
						<ProgressScreen
							isMessage={true}
							primaryMessage={"Hang On"}
							message={"Updating Invitation"}
							indicatorSize={height / 30}
							indicatorColor={"#2C98F0"}
							messageStyle={{ fontSize: height / 65, width: width / 2.7 }}
							primaryMessageStyles={{
								fontSize: height / 60,
								width: width / 2.7
							}}
						/>
					) : null}
				</View>
			</KeyboardAvoidingView>
		);
	}
}

const mapStateToProps = state => ({
	appToken: state.appToken.token,
	tenantId: state.appToken.userProfile.tenantdto
		? JSON.parse(state.appToken.userProfile.tenantdto).tenant_id
		: null,
	hostId: JSON.parse(state.appToken.userProfile.userdevices)[0].userId,
	locations: state.vms && state.vms.editMeetingsReducer.locations,
	locationObject: state.vms && state.vms.editMeetingsReducer.locationObject,
	isLocationsLoading:
		state.vms && state.vms.editMeetingsReducer.isLocationsLoading,
	isLocationsError: state.vms && state.vms.editMeetingsReducer.isLocationsError,
	isEditMeetingLoading:
		state.vms && state.vms.editMeetingsReducer.isEditMeetingLoading,
	isEditMeetingError:
		state.vms && state.vms.editMeetingsReducer.isEditMeetingError,
	isEditMeetingSuccess:
		state.vms && state.vms.editMeetingsReducer.isEditMeetingSuccess
});

EditMeeting.contextType = DialogContext;

export default connect(mapStateToProps)(EditMeeting);
