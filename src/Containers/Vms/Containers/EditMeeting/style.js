import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: {
		height: height,
		paddingHorizontal: width * 0.05,
		paddingVertical: height * 0.04,
		backgroundColor: "#ffffff"
	},
	updateButton: {
		backgroundColor: "#2c98f0",
		alignItems: "center",
		borderRadius: width * 0.1,
		marginVertical: height * 0.01,
		marginHorizontal: width * 0.03
	},
	updateText: {
		paddingVertical: height * 0.01
	}
});
