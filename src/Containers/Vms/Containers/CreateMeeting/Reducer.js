import { VMS_CREATE_MEETING } from "./Saga";
import { REQUEST, SUCCESS, FAILURE } from "../../../../utils/reduxUtils";

const initialState = {
	isCreateMeetingLoading: false,
	isCreateMeetingSuccess: false,
	isCreateMeetingError: false
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case VMS_CREATE_MEETING[REQUEST]:
			return {
				...state,
				isCreateMeetingLoading: true,
				isCreateMeetingSuccess: false,
				isCreateMeetingError: false
			};
		case VMS_CREATE_MEETING[SUCCESS]:
			return {
				...state,
				isCreateMeetingSuccess: true,
				isCreateMeetingLoading: false
			};
		case VMS_CREATE_MEETING[FAILURE]:
			return {
				...state,
				isCreateMeetingLoading: false,
				isCreateMeetingError: true
			};
		default:
			return state;
	}
};
