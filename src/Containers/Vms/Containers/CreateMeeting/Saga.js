import { put, call, takeLatest } from "redux-saga/effects";
import {
	createRequestTypes,
	action,
	REQUEST,
	SUCCESS,
	FAILURE
} from "../../../../utils/reduxUtils";
import VmsApi from "../../Api";

/**
 * Constants
 */

export const VMS_CREATE_MEETING = createRequestTypes("VMS_CREATE_MEETING");

/**
 * Action creators
 */
export const vmsCreateMeeting = {
	request: (appToken, body) =>
		action(VMS_CREATE_MEETING[REQUEST], { appToken, body }),
	success: response => action(VMS_CREATE_MEETING[SUCCESS], { response }),
	failure: error => action(VMS_CREATE_MEETING[FAILURE], { error })
};

export function* vmsCreateMeetingSaga(dispatch) {
	yield takeLatest(VMS_CREATE_MEETING[REQUEST], handleCreateMeeting);
}

/**
 * @param {Action} action
 */
function* handleCreateMeeting(action) {
	try {
		let createMeetingResponse = yield call(
			VmsApi.createInvitation,
			action.payload
		);
		console.log(createMeetingResponse);
		yield put(vmsCreateMeeting.success(true));
	} catch (error) {
		yield put(vmsCreateMeeting.failure(error));
	}
}
