import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: {
		paddingTop: height * 0.02,
		paddingHorizontal: width * 0.04,
		backgroundColor: "#ffffff"
	},
	horizontalLine: {
		borderWidth: 1,
		borderColor: "#eff1f4",
		marginVertical: height * 0.01
	},
	addMoreVisitor: {
		marginVertical: height * 0.01,
		marginHorizontal: width * 0.04
	},
	visitorText: {
		marginHorizontal: width * 0.04
	},
	addmoreButton: {
		borderWidth: 1,
		borderColor: "#2c98f0",
		alignSelf: "flex-start",
		borderRadius: width * 0.1,
		marginVertical: height * 0.01
	},
	addmoreLabel: {
		paddingHorizontal: width * 0.03,
		paddingVertical: height * 0.01
	},
	sendInviteButton: {
		backgroundColor: "#2c98f0",
		borderRadius: width * 0.1,
		alignItems: "center",
		marginVertical: height * 0.01,
		marginHorizontal: width * 0.03
	},
	sendInviteText: {
		paddingVertical: height * 0.01
	}
});
