import React, { Component } from "react";
import {
	ScrollView,
	View,
	TouchableOpacity,
	Dimensions,
	KeyboardAvoidingView,
	Platform
} from "react-native";
import { HeaderHeightContext } from "@react-navigation/stack";
import { Icon } from "../../../../Components";
import {
	GoappTextBold,
	GoappTextRegular
} from "../../../../Components/GoappText";
import DialogContext from "../../../../DialogContext";
import EditMeetingDetails from "../../Components/EditMeetingDetails";
import EditVisitor from "../../Components/EditVisitor";
import { styles } from "./style";
import { connect } from "react-redux";
import { vmsGetLocations } from "../EditMeeting/Saga";
import { GoToast, ProgressScreen } from "../../../../Components";
import { vmsCreateMeeting } from "./Saga";
import { VERIFIED_CORPORATE } from "../../../../Assets/Img/Image";
import { ifIphoneX } from "../../../../utils/iphonexHelper";
import {
	validatePhoneNumberDigit,
	validateEmail
} from "../../../../utils/validation";
import { cloneDeep } from "lodash";

const { height, width } = Dimensions.get("window");

/**
 * @typedef {{isValid: boolean, value: string, validationMsg: string}} FormField
 */

/**
 * @typedef {{name: FormField, email: FormField, mobile: FormField, countryCode: FormField}} Visitor
 */

/**
 * @type {Visitor}
 */
export const visitor = {
	name: {
		isValid: false,
		value: "",
		validationMsg: "Please Enter Visitor Name"
	},
	email: {
		isValid: true,
		value: "",
		validationMsg: ""
	},
	mobile: {
		isValid: false,
		value: "",
		validationMsg: "Please Enter Visitor Mobile Number"
	},
	countryCode: {
		isValid: true,
		value: "+91",
		validationMsg: ""
	}
};

const MandatoryFields = ["name", "mobile"];

/**
 * @typedef {Object} Props
 * @property {string} appToken
 * @property {string} [tenantId]
 * @property {string} [hostId]
 * @property {Array} locations
 * @property {boolean} isLocationsLoading
 * @property {boolean} isLocationsError
 * @property {boolean} isCreateMeetingLoading
 * @property {boolean} isCreateMeetingSuccess
 * @property {boolean} isCreateMeetingError
 */

/**
 * @typedef {Object} State
 * @property {number} scheduleStart
 * @property {number} scheduleEnd
 * @property {FormField} purpose
 * @property {Visitor[]} visitors
 * @property {Visitor} tempVisitor
 * @property {Array} locations
 * @property {any} selectedLocation
 *
 */

/**
 * @augments {Component<Props, State>}
 */
class CreateMeeting extends Component {
	state = {
		scheduleStart: Date.now() + 24 * 60 * 60 * 1000,
		scheduleEnd: Date.now() + 25 * 60 * 60 * 1000,
		purpose: {
			isValid: true,
			value: "",
			validationMsg: ""
		},
		visitors: [],
		tempVisitor: cloneDeep(visitor),
		locations: [],
		selectedLocation: null
	};
	validationError = null;

	componentDidMount() {
		this.props.dispatch(
			vmsGetLocations.request(this.props.appToken, this.props.tenantId)
		);
	}

	shouldComponentUpdate(props, state) {
		if (
			props.locations &&
			props.locations !== this.props.locations &&
			props.locations.length > 0
		) {
			this.setState({
				locations: props.locations,
				selectedLocation: props.locations[0].address
			});
		}
		return true;
	}

	componentDidUpdate(prevProps, prevState) {
		if (
			!prevProps.isCreateMeetingSuccess &&
			this.props.isCreateMeetingSuccess
		) {
			const { route } = this.props;
			const { onBack } = route.params ? route.params : {};

			onBack();
			this.props.navigation.goBack();
			this.context.current.openDialog({
				type: "Information",
				title: "Success!",
				message: "Invitation has been sent successfully.",
				icon: VERIFIED_CORPORATE
			});
		}

		if (!prevProps.isCreateMeetingError && this.props.isCreateMeetingError) {
			GoToast.show("Failed to create invitation", "Error", 1000);
		}
	}

	handleLocationChange = updatedLocation => {
		this.setState({
			selectedLocation: updatedLocation
		});
	};

	handleDateChange = date => {
		// If user selects minimum date set time to current time

		if (
			new Date().getDate() === new Date(date).getDate() &&
			new Date().getMonth() === new Date(date).getMonth()
		) {
			this.setState({
				scheduleStart: Date.parse(new Date()),
				scheduleEnd: new Date(new Date()).setMinutes(
					new Date().getMinutes() + 10,
					1000
				)
			});
		}
		// else preserve start time and end time
		else {
			let startTime = new Date(this.state.scheduleStart);
			let endTime = new Date(this.state.scheduleEnd);
			this.setState({
				scheduleStart: new Date(date).setHours(
					startTime.getHours(),
					startTime.getMinutes()
				),
				scheduleEnd: new Date(date).setHours(
					endTime.getHours(),
					endTime.getMinutes()
				)
			});
		}
	};

	handleStartTimeChange = date => {
		let startTime = new Date(date);
		let endTime = new Date(this.state.scheduleEnd);
		let meetingStartTime =
			new Date(this.state.scheduleStart).toDateString() +
			" " +
			new Date(startTime).toTimeString();
		if (Date.parse(endTime) < Date.parse(startTime)) {
			this.setState({
				scheduleStart: Date.parse(meetingStartTime),
				scheduleEnd: new Date(meetingStartTime).setMinutes(
					startTime.getMinutes() + 10,
					1000
				)
			});
		} else {
			this.setState({
				scheduleStart: Date.parse(meetingStartTime),
				scheduleEnd: new Date(meetingStartTime).setMinutes(
					startTime.getMinutes() + 10,
					1000
				)
			});
		}
	};

	handleEndTimeChange = date => {
		let meetingEndTime =
			new Date(this.state.scheduleEnd).toDateString() +
			" " +
			new Date(date).toTimeString();
		this.setState({ scheduleEnd: Date.parse(meetingEndTime) });
	};

	handlePurposeChange = text => {
		// Purpose cannot have numerals and special characters
		// Sparten spaces doesn't accept it
		let purposeRegex = /[A-Za-z .]+$/;
		let isPurposeValid = purposeRegex.test(text);
		if (!isPurposeValid && text.length > 0) {
			GoToast.show(
				"Purpose cannot contain numbers and special characters",
				"Error"
			);
			this.setState({
				purpose: {
					value: text,
					isValid: false,
					validationMsg: "Purpose cannot contain numbers and special characters"
				}
			});
		} else {
			this.setState({
				purpose: {
					value: text,
					isValid: true,
					validationMsg: ""
				}
			});
		}
	};

	addVisitor = () => {
		let { tempVisitor } = this.state;
		let tempVisitorKeys = Object.keys(tempVisitor);
		let isValidTempVisitor = tempVisitorKeys.every(key => {
			// Check Mandatory fields filled
			if (
				MandatoryFields.includes(key) &&
				!tempVisitor[key].value.length === 0
			) {
				GoToast.show(`Please fill Mandatory fields`, "Error", 2000);
				return false;
			}
			// check filled fileds are valid or not
			if (!tempVisitor[key].isValid) {
				GoToast.show(tempVisitor[key].validationMsg, "Error", 2000);
				return false;
			}
			// check non mandatory fields
			let isValidEmail = validateEmail(tempVisitor.email.value);
			if (tempVisitor.email.value.length > 0 && !isValidEmail) {
				GoToast.show("Please enter valid email", "Error", 2000);
				return false;
			}
			return true;
		});
		if (!isValidTempVisitor) {
			return;
		}
		let isDuplicateVisitor = this.state.visitors.findIndex(visitor => {
			if (visitor.mobile.value === tempVisitor.mobile.value) {
				GoToast.show("Duplicate visitor mobile number", "Error");
				return true;
			}
			if (
				tempVisitor.email.value.length > 0 &&
				visitor.email.value === tempVisitor.email.value
			) {
				GoToast.show("Duplicate visitor email", "Error");
				return true;
			}
			return false;
		});
		if (isDuplicateVisitor < 0) {
			this.setState((state, props) => ({
				visitors: [...state.visitors, this.state.tempVisitor],
				tempVisitor: cloneDeep(visitor)
			}));
		}
	};

	deleteVisitor = index => {
		const updatedVisitors = [
			...this.state.visitors.slice(0, index),
			...this.state.visitors.slice(index + 1, this.state.visitors.length)
		];
		this.setState({
			visitors: updatedVisitors
		});
	};

	/**
	 * @param {string} text
	 * @param {("name" | "email" | "mobile")} type
	 */
	handleVisitorChange = (text, type) => {
		let { tempVisitor } = this.state;
		tempVisitor[type].value = text;
		switch (type) {
			case "name":
				let nameRegex = /^[A-Za-z .]+$/;
				let isNameValid = nameRegex.test(text);
				if (!isNameValid && text.length > 0) {
					GoToast.show(
						"Name cannot contain numbers and special characters",
						"Error"
					);
					tempVisitor[type].isValid = false;
					tempVisitor[type].validationMsg =
						"Name cannot contain numbers and special characters";
					this.setState({ tempVisitor });
				} else {
					tempVisitor[type].isValid = true;
					tempVisitor[type].validationMsg = "";
					this.setState({ tempVisitor });
				}
				break;
			case "mobile":
				let phoneRegex = /^[0-9]+$/;
				let isPhoneValid = phoneRegex.test(text);
				if (!isPhoneValid && text.length > 0) {
					GoToast.show("Please enter valid mobile number", "Error");
					tempVisitor[type].isValid = false;
					tempVisitor[type].validationMsg = "Please enter valid mobile number";
					this.setState({ tempVisitor });
				} else {
					tempVisitor[type].isValid = true;
					tempVisitor[type].validationMsg = "";
					this.setState({ tempVisitor });
				}
			default:
				this.setState({ tempVisitor });
				break;
		}
	};

	/**
	 * @param {{name: FormField, email: FormField, mobile: FormField, countryCode: FormField}} visitor
	 */
	verifyPhoneNumber = visitor => {
		let number = `${visitor.countryCode.value}-${visitor.mobile.value}`;
		return validatePhoneNumberDigit(number);
	};

	/**
	 * @param {visitor[]} visitorList
	 * @returns {boolean}
	 */
	validate = visitorList => {
		return visitorList.every(visitor => {
			// Name validation
			if (visitor.name.value.length <= 0 || !visitor.name.isValid) {
				if (!visitor.name.isValid) {
					GoToast.show("Name cannot contain special characters");
				} else {
					GoToast.show("Name is mandatory");
				}
				return false;
			}
			// Mobile number validation
			if (!visitor.mobile.value.length > 0 || !visitor.mobile.isValid) {
				if (!visitor.mobile.value.length > 0) {
					GoToast.show("phone number is mandatory");
				} else {
					GoToast.show("Phone number is not valid");
				}
				return false;
			} else if (!this.verifyPhoneNumber(visitor)) {
				GoToast.show("Phone number is not valid");
				return false;
			}
			// Email validation
			if (
				visitor.email.value.length > 0 &&
				!validateEmail(visitor.email.value)
			) {
				GoToast.show("Email is not valid");
				return false;
			}
			return true;
		});
	};

	handleClearForm = () => {
		this.setState({
			tempVisitor: { ...visitor }
		});
	};

	handleSubmit = () => {
		let {
			scheduleStart,
			scheduleEnd,
			purpose,
			visitors: visitorList,
			tempVisitor
		} = this.state;
		const { hostId } = this.props;
		let zoneId = null;
		if (
			this.state.locations.length > 0 &&
			this.state.selectedLocation !== null
		) {
			zoneId = this.state.locations.find(location => {
				return location.address === this.state.selectedLocation;
			});
		} else if (
			this.state.locations === null ||
			this.state.locations.length <= 0
		) {
			GoToast.show("Please Select the location", "Error", 2000);
			return;
		}
		// Location check
		if (!zoneId) {
			GoToast.show("Failed to Load Location of meeting", "Error", 2000);
			return;
		} else {
			zoneId = zoneId.zoneId;
		}
		// purpose check
		if (!purpose.value.length > 0) {
			GoToast.show("Please fill Purpose of visit", "Error", 2000);
			return;
		}
		if (!purpose.isValid) {
			GoToast.show("Purpose cannot contain special characters", "Error", 2000);
			return;
		}
		console.log([tempVisitor], "--visitor List--");

		if (
			tempVisitor.name.value.length > 0 ||
			tempVisitor.mobile.value.length > 0
		) {
			let isValidTempVisitor = this.validate([tempVisitor]);
			let isDuplicateVisitor = this.state.visitors.findIndex(
				visitor =>
					visitor.mobile.value === tempVisitor.mobile.value ||
					(tempVisitor.email.value.length > 0 &&
						visitor.email.value === tempVisitor.email.value)
			);
			if (isValidTempVisitor && !(isDuplicateVisitor > -1)) {
				visitorList = [...visitorList, tempVisitor];
			} else {
				if (!isValidTempVisitor) {
					// GoToast.show(`${this.validationError}`, "Error")
				} else {
					GoToast.show("Don't create duplicate visitors!", "Error");
				}
				return;
			}
		}
		// Number of visitors check
		if (visitorList.length === 0) {
			GoToast.show("Please fill visitor details");
			return;
		}
		// Modify visitorList according to server format
		visitorList = visitorList.map(visitor => ({
			...visitor,
			mobile: `${visitor.countryCode.value}${visitor.mobile.value}`,
			name: `${visitor.name.value}`,
			email: `${visitor.email.value}`
		}));
		const body = {
			scheduleStart,
			scheduleEnd,
			purpose: purpose.value,
			hostId,
			zoneId,
			visitorList
		};
		this.props.dispatch(vmsCreateMeeting.request(this.props.appToken, body));
	};

	render() {
		return (
			<HeaderHeightContext.Consumer>
				{headerHeight => (
					<KeyboardAvoidingView
						{...(Platform.OS === "ios" ? { behavior: "padding" } : {})}
						keyboardVerticalOffset={Platform.select({
							ios: headerHeight + ifIphoneX(24, 0),
							android: 0
						})}>
						<ScrollView
							showsVerticalScrollIndicator={false}
							style={styles.container}
							keyboardShouldPersistTaps={"always"}>
							<EditMeetingDetails
								date={this.state.scheduleStart}
								handleDateChange={date => this.handleDateChange(date)}
								startTime={this.state.scheduleStart}
								handleStartTimeChange={date => this.handleStartTimeChange(date)}
								endTime={this.state.scheduleEnd}
								handleEndTimeChange={date => this.handleEndTimeChange(date)}
								purpose={this.state.purpose.value}
								handlePurposeChange={text => this.handlePurposeChange(text)}
								locations={this.state.locations}
								selectedLocation={this.state.selectedLocation}
								isLocationsLoading={this.props.isLocationsLoading}
								isLocationsError={this.props.isLocationsError}
								handleLocationChange={(updatedLocation, index) =>
									this.handleLocationChange(updatedLocation, index)
								}
							/>
							<View style={styles.horizontalLine} />
							<GoappTextBold style={styles.addMoreVisitor}>
								Add Visitor Details
							</GoappTextBold>
							{this.state.visitors.map((visitor, index) => (
								<View key={`${index}`}>
									<GoappTextRegular
										style={styles.visitorText}>{`Visitor #${index +
										1}`}</GoappTextRegular>
									<View
										style={{
											flex: 1,
											flexDirection: "row",
											borderWidth: 2,
											borderRadius: 15,
											borderColor: "#eaeaea",
											padding: 10,
											marginVertical: 10,
											justifyContent: "space-between"
										}}>
										<View>
											<GoappTextBold>{visitor.name.value}</GoappTextBold>
											<GoappTextRegular>
												{visitor.mobile.value}
											</GoappTextRegular>
											{visitor.email.length > 0 ? (
												<GoappTextRegular>
													{visitor.email.value}
												</GoappTextRegular>
											) : null}
										</View>
										<View
											style={{
												justifyContent: "center"
											}}>
											<TouchableOpacity
												onPress={() => this.deleteVisitor(index)}>
												<Icon
													iconType={"material_community_icon"}
													iconName={"delete-outline"}
													iconSize={width / 16}
												/>
											</TouchableOpacity>
										</View>
									</View>
								</View>
							))}
							<View
								style={{
									// borderWidth: 2,
									borderRadius: height / 50,
									borderColor: "#eaeaea"
								}}>
								<View
									style={{
										flexDirection: "row",
										justifyContent: "space-between"
									}}>
									<GoappTextRegular style={styles.visitorText}>{`Visitor #${this
										.state.visitors.length + 1}`}</GoappTextRegular>
									{this.state.visitors.length > 1 ? (
										<TouchableOpacity
											style={styles.visitorText}
											onPress={this.handleClearForm}>
											<Icon
												iconType={"material_community_icon"}
												iconName={"delete-outline"}
												iconSize={width / 16}
											/>
										</TouchableOpacity>
									) : null}
								</View>
								<EditVisitor
									visitor={this.state.tempVisitor}
									onVisitorChange={(text, type) =>
										this.handleVisitorChange(text, type)
									}
								/>
							</View>
							<TouchableOpacity
								style={styles.addmoreButton}
								onPress={this.addVisitor}>
								<GoappTextRegular color={"#2c98f0"} style={styles.addmoreLabel}>
									Add more visitors +
								</GoappTextRegular>
							</TouchableOpacity>
							<TouchableOpacity
								style={styles.sendInviteButton}
								onPress={this.handleSubmit}>
								<GoappTextBold
									color={"#fff"}
									size={20}
									style={styles.sendInviteText}>
									Send Invitation
								</GoappTextBold>
							</TouchableOpacity>
							{this.props.isCreateMeetingLoading ? (
								<ProgressScreen
									isMessage={true}
									primaryMessage={"Hang On"}
									message={"Creating Invitation"}
									indicatorSize={height / 30}
									indicatorColor={"#2C98F0"}
									messageStyle={{ fontSize: height / 65, width: width / 2.7 }}
									primaryMessageStyles={{
										fontSize: height / 60,
										width: width / 2.7
									}}
								/>
							) : null}
						</ScrollView>
					</KeyboardAvoidingView>
				)}
			</HeaderHeightContext.Consumer>
		);
	}
}

CreateMeeting.contextType = DialogContext;

const mapStateToProps = state => ({
	appToken: state.appToken.token,
	tenantId: state.appToken.userProfile.tenantdto
		? JSON.parse(state.appToken.userProfile.tenantdto).tenant_id
		: null,
	hostId: state.appToken.userProfile.userdevices
		? JSON.parse(state.appToken.userProfile.userdevices)[0].userId
		: null,
	locations: state.vms && state.vms.editMeetingsReducer.locations,
	isLocationsLoading:
		state.vms && state.vms.editMeetingsReducer.isLocationsLoading,
	isLocationsError: state.vms && state.vms.editMeetingsReducer.isLocationsError,
	isCreateMeetingLoading:
		state.vms && state.vms.createMeetingsReducer.isCreateMeetingLoading,
	isCreateMeetingSuccess:
		state.vms && state.vms.createMeetingsReducer.isCreateMeetingSuccess,
	isCreateMeetingError:
		state.vms && state.vms.createMeetingsReducer.isCreateMeetingError
});

export default connect(mapStateToProps)(CreateMeeting);
