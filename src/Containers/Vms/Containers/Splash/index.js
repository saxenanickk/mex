import React, { Component } from "react";
import { Text, View } from "react-native";
import { CommonActions } from "@react-navigation/native";

export default class Splash extends Component {
	componentDidMount() {
		this.resetNavigation();
	}

	resetNavigation = () => {
		this.props.navigation.dispatch(
			CommonActions.reset({
				index: 0,
				routes: [{ name: "Home" }]
			})
		);
	};

	render() {
		return (
			<View>
				<Text> Visitor Management System </Text>
			</View>
		);
	}
}
