import { takeLatest, put, call } from "redux-saga/effects";
import {
	createRequestTypes,
	REQUEST,
	SUCCESS,
	FAILURE,
	action
} from "../../../../utils/reduxUtils";
import VmsApi from "../../Api";

/**
 * Constants
 */

export const VMS_APPROVE_PHOTO = createRequestTypes("VMS_APPROVE_PHOTO");
export const VMS_REJECT_PHOTO = createRequestTypes("VMS_REJECT_PHOTO");
export const VMS_CANCEL_MEETING = createRequestTypes("VMS_CANCEL_MEETING");
export const VMS_VIEW_INVITATION = createRequestTypes("VMS_VIEW_INVITATION");

/**
 * Action Creators
 */

export const vmsApprovePhoto = {
	request: (appToken, body) =>
		action(VMS_APPROVE_PHOTO[REQUEST], { appToken, body }),
	success: response => action(VMS_APPROVE_PHOTO[SUCCESS], { response }),
	failure: error => action(VMS_APPROVE_PHOTO[FAILURE], { error })
};

export const vmsRejectPhoto = {
	request: (appToken, body) =>
		action(VMS_REJECT_PHOTO[REQUEST], { appToken, body }),
	success: response => action(VMS_REJECT_PHOTO[SUCCESS], { response }),
	failure: error => action(VMS_REJECT_PHOTO[FAILURE], { error })
};

export const vmsCancelMeeting = {
	request: (appToken, body) =>
		action(VMS_CANCEL_MEETING[REQUEST], { appToken, body }),
	success: response => action(VMS_CANCEL_MEETING[SUCCESS], { response }),
	failure: error => action(VMS_CANCEL_MEETING[FAILURE], { error })
};

export const vmsViewInvitation = {
	request: (appToken, invitationInfoId) =>
		action(VMS_VIEW_INVITATION[REQUEST], { appToken, invitationInfoId }),
	success: response => action(VMS_VIEW_INVITATION[SUCCESS], { response }),
	failure: error => action(VMS_VIEW_INVITATION[FAILURE], { error })
};

export function* vmsMeetingSaga(dispatch) {
	yield takeLatest(VMS_APPROVE_PHOTO[REQUEST], handleVmsApprovePhoto);
	yield takeLatest(VMS_REJECT_PHOTO[REQUEST], handleVmsRejectPhoto);
	yield takeLatest(VMS_CANCEL_MEETING[REQUEST], handleVmsCancelMeeting);
	yield takeLatest(VMS_VIEW_INVITATION[REQUEST], handleVmsViewMeeting);
}

/**
 * Handlers
 */

function* handleVmsApprovePhoto(action) {
	try {
		const approvePhotoResponse = yield call(
			VmsApi.approvePhoto,
			action.payload
		);
		yield put(vmsApprovePhoto.success(approvePhotoResponse));
		yield put(
			vmsViewInvitation.request(
				action.payload.appToken,
				action.payload.body.invitationInfoId
			)
		);
	} catch (error) {
		yield put(vmsApprovePhoto.failure(error));
	}
}

function* handleVmsRejectPhoto(action) {
	try {
		const rejectPhotoResponse = yield call(VmsApi.rejectPhoto, action.payload);
		yield put(vmsRejectPhoto.success(rejectPhotoResponse));
		yield put(
			vmsViewInvitation.request(
				action.payload.appToken,
				action.payload.body.invitationInfoId
			)
		);
	} catch (error) {
		yield put(vmsRejectPhoto.failure(error));
	}
}

function* handleVmsCancelMeeting(action) {
	try {
		const cancelMeetingResponse = yield call(
			VmsApi.cancelInvitation,
			action.payload
		);
		yield put(vmsCancelMeeting.success(cancelMeetingResponse));
	} catch (error) {
		yield put(vmsCancelMeeting.failure(error));
	}
}

function* handleVmsViewMeeting(action) {
	try {
		const viewInvitationResponse = yield call(
			VmsApi.viewInvitation,
			action.payload
		);
		console.log(viewInvitationResponse);
		yield put(vmsViewInvitation.success(viewInvitationResponse));
	} catch (error) {
		yield put(vmsViewInvitation.failure(error));
	}
}
