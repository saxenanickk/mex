import React, { Component } from "react";
import {
	View,
	TouchableOpacity,
	Dimensions,
	ActivityIndicator,
	ScrollView,
	Platform
} from "react-native";
import FastImage from "react-native-fast-image";
import { connect } from "react-redux";
import { styles } from "./style";
import {
	GoappTextRegular,
	GoappTextBold
} from "../../../../Components/GoappText";
import { EMPTYIMAGE, VERIFIED_CORPORATE } from "../../../../Assets/Img/Image";
import { _24to12 } from "../../../../utils/dateUtils";
import Icon from "../../../../Components/Icon";
import DialogContext from "../../../../DialogContext";
import { ProgressScreen, GoToast } from "../../../../Components";
import { vmsRemoveVisitor } from "../AddVisitor/Saga";
import {
	vmsRejectPhoto,
	vmsApprovePhoto,
	vmsCancelMeeting,
	vmsViewInvitation
} from "./Saga";

const { width, height } = Dimensions.get("window");

class InvitationDetails extends Component {
	static navigationOptions = ({ navigation }) => ({
		title: "Invite Visitor",
		headerForceInset: {
			top: "never",
			bottom: "never"
		},
		headerTitleStyle: {
			fontFamily: Platform.OS === "ios" ? "Avenir" : "LatoRegular"
		}
	});

	state = {
		// Label to show minimum one guest is requires
		showMinGuest: false
	};

	componentDidMount() {
		const { route } = this.props;
		const { invitationInfoId } = route.params ? route.params : {};

		this.props.dispatch(
			vmsViewInvitation.request(this.props.appToken, invitationInfoId)
		);
	}

	componentDidUpdate(prevProps, prevState) {
		const { route } = this.props;
		const { onBack } = route && route.params ? route.params : {};
		if (
			!prevProps.isRemoveVisitorSuccess &&
			this.props.isRemoveVisitorSuccess
		) {
			onBack();
			this.context.current.openDialog({
				type: "Information",
				title: "Done!",
				message: "Guest removed successfully!",
				icon: VERIFIED_CORPORATE
			});
		}
		if (!prevProps.isRemoveVisitorError && this.props.isRemoveVisitorError) {
			GoToast.show("Sorry, we are unable to remove guest", "Error", 1000);
		}
		if (
			!prevProps.isCancelMeetingSuccess &&
			this.props.isCancelMeetingSuccess
		) {
			onBack();
			this.props.navigation.goBack();
			this.context.current.openDialog({
				type: "Information",
				title: "Done!",
				message: "Invitation cancelled successfully",
				icon: VERIFIED_CORPORATE
			});
		}
		if (!prevProps.isCancelMeetingError && this.props.isCancelMeetingError) {
			GoToast.show("Sorry, we are unable to remove meeting", "Error", 1000);
		}
		if (!prevProps.isApprovePhotoSuccess && this.props.isApprovePhotoSuccess) {
			this.context.current.openDialog({
				type: "Information",
				title: "Info Alert!",
				message: "Photo has been accepted",
				icon: VERIFIED_CORPORATE
			});
		}
		if (!prevProps.isApprovePhotoError && this.props.isApprovePhotoError) {
			GoToast.show("Sorry, we are unable to Approve picture", "Error", 1000);
		}

		if (!prevProps.isRejectPhotoSuccess && this.props.isRejectPhotoSuccess) {
			this.context.current.openDialog({
				type: "Information",
				title: "Info Alert!",
				message: "Photo has been rejected",
				icon: VERIFIED_CORPORATE
			});
		}
		if (!prevProps.isRejectPhotoError && this.props.isRejectPhotoError) {
			GoToast.show("Sorry, we are unable to Reject picture", "Error", 1000);
		}
	}

	componentWillUnmount() {
		this.props.dispatch(vmsViewInvitation.success(null));
	}

	handleEditMeeting = () => {
		const { item } = this.props;
		this.props.navigation.navigate("EditMeeting", {
			item,
			onBack: () => this.handleBackFromOtherScreen()
		});
	};

	handleCancelMeeting = () => {
		const { item } = this.props;
		const body = {
			invitationInfoId: item.invitationInfoId,
			comment: "Sorry, meeting is cancelled"
		};
		this.context.current.openDialog({
			type: "Confirmation",
			title: "Cancel Meeting",
			message: "Are you sure you want to cancel the meeting?",
			rightTitle: "Yes",
			rightPress: () =>
				this.props.dispatch(vmsCancelMeeting.request(this.props.appToken, body))
		});
	};

	handleRemoveGuest = attendee => {
		const { item } = this.props;
		if (!(item.visitorList.length > 1)) {
			this.setState({ showMinGuest: true });
			return;
		}
		const body = {
			invitationInfoId: item.invitationInfoId,
			visitorId: attendee.id,
			comment: "Sorry, this event is cancelled"
		};
		this.context.current.openDialog({
			type: "Confirmation",
			title: "Remove Visitor",
			message: "Are you sure you want to remove the guest?",
			leftTitle: "No",
			rightTitle: "Yes",
			rightPress: () =>
				this.props.dispatch(vmsRemoveVisitor.request(this.props.appToken, body))
		});
	};

	/**
	 * @param {string} visitorId
	 */
	handleRejectPhoto = visitorId => {
		const { item } = this.props;
		const body = {
			invitationInfoId: item.invitationInfoId,
			visitorId
		};
		this.props.dispatch(vmsRejectPhoto.request(this.props.appToken, body));
	};

	/**
	 * @param {string} visitorId
	 */
	handleApprovePhoto = visitorId => {
		const { item } = this.props;
		const body = {
			invitationInfoId: item.invitationInfoId,
			visitorId
		};
		this.props.dispatch(vmsApprovePhoto.request(this.props.appToken, body));
	};

	handleBackFromOtherScreen = () => {
		const { route } = this.props;
		const { invitationInfoId, onBack } = route.params ? route.params : {};

		this.props.dispatch(
			vmsViewInvitation.request(this.props.appToken, invitationInfoId)
		);
		onBack();
	};

	render() {
		const { route } = this.props;
		const { location } = route.params ? route.params : {};

		if (this.props.isViewInvitationLoading) {
			return (
				<View style={styles.loadingContainer}>
					<ActivityIndicator />
				</View>
			);
		}
		if (this.props.isViewInvitationError) {
			return (
				<View style={styles.errorContainer}>
					<GoappTextRegular size={24}>
						Unable to load Invitation Details
					</GoappTextRegular>
					<TouchableOpacity
						style={styles.retryButton}
						onPress={this.handleRetry}>
						<GoappTextRegular style={styles.retryText}>Retry</GoappTextRegular>
					</TouchableOpacity>
				</View>
			);
		}
		const { item } = this.props;
		// If invitation is not loaded return null
		if (!item) {
			return null;
		}
		const scheduleStart = new Date(item.scheduledStart).toDateString();
		const scheduleEnd = new Date(item.scheduledEnd).toDateString();
		const scheduleStartTime = new Date(item.scheduledStart);
		const scheduleEndTime = new Date(item.scheduledEnd);
		return (
			<View style={styles.containerBox}>
				<View style={styles.container}>
					<View
						style={{
							flexDirection: "row",
							justifyContent: "space-between"
						}}>
						<View style={styles.detailPair}>
							<GoappTextRegular color={"#727272"} style={styles.label}>
								Start Time
							</GoappTextRegular>
							<GoappTextRegular
								style={styles.input}>{`${scheduleStart} ${_24to12(
								scheduleStartTime
							)}`}</GoappTextRegular>
						</View>
						<View style={[styles.editDelete, styles.detailPair]}>
							<TouchableOpacity onPress={this.handleEditMeeting}>
								<Icon
									iconType={"material"}
									iconName={"mode-edit"}
									iconSize={height / 26}
									iconColor={"#2c98f0"}
								/>
							</TouchableOpacity>
							<View style={styles.verticalLine} />
							<TouchableOpacity onPress={this.handleCancelMeeting}>
								<Icon
									iconType={"material_community_icon"}
									iconName={"delete-outline"}
									iconSize={height / 26}
									iconColor={"#2c98f0"}
								/>
							</TouchableOpacity>
						</View>
					</View>
					<View style={styles.detailPair}>
						<GoappTextRegular color={"#727272"} style={styles.label}>
							End Time
						</GoappTextRegular>
						<GoappTextRegular style={styles.input}>{`${scheduleEnd} ${_24to12(
							scheduleEndTime
						)}`}</GoappTextRegular>
					</View>
					<View style={styles.detailPair}>
						<GoappTextRegular color={"#727272"} style={styles.label}>
							Purpose of Visit
						</GoappTextRegular>
						<GoappTextRegular style={styles.input}>
							{item.purpose}
						</GoappTextRegular>
					</View>
					{location ? (
						<View style={styles.detailPair}>
							<GoappTextRegular color={"#727272"} style={styles.label}>
								Location of Meeting
							</GoappTextRegular>
							<GoappTextRegular style={styles.input}>
								{location}
							</GoappTextRegular>
						</View>
					) : null}
					<View style={styles.detailPair}>
						<GoappTextRegular style={styles.label} color={"#727272"}>{`Guest (${
							item.visitorList.length
						})`}</GoappTextRegular>
						<ScrollView
							horizontal={true}
							showsHorizontalScrollIndicator={false}>
							<View style={styles.guestsContainer}>
								{item.visitorList.map(attendee => (
									<View style={styles.guest} key={`${attendee.id}`}>
										<View>
											<TouchableOpacity
												style={styles.closeImage}
												onPress={() => this.handleRemoveGuest(attendee)}>
												<View
													style={{
														backgroundColor: "#fff",
														borderRadius: height / 30
													}}>
													<Icon
														iconType={"feather"}
														iconName={"x-circle"}
														iconSize={height / 30}
														iconColor={"#E02020"}
													/>
												</View>
											</TouchableOpacity>
											{attendee.image === null ? (
												<FastImage
													source={EMPTYIMAGE}
													style={{ width: 65, height: 65, borderRadius: 75 }}
												/>
											) : (
												<FastImage
													source={{ uri: attendee.image }}
													style={{ width: 65, height: 65, borderRadius: 75 }}
												/>
											)}
										</View>
										<GoappTextRegular size={height / 45}>
											{attendee.name}
										</GoappTextRegular>
										<View style={styles.guestSection}>
											{attendee.imageStatus === 1 ? (
												<GoappTextRegular>Photo Pending</GoappTextRegular>
											) : attendee.imageStatus === 2 ? (
												<React.Fragment>
													<TouchableOpacity
														onPress={() => this.handleRejectPhoto(attendee.id)}>
														<GoappTextRegular
															size={height / 55}
															color={"#2c98f0"}>
															Reject
														</GoappTextRegular>
													</TouchableOpacity>
													<TouchableOpacity
														style={styles.approvedButton}
														onPress={() =>
															this.handleApprovePhoto(attendee.id)
														}>
														<GoappTextRegular
															size={height / 55}
															color={"white"}>
															Approve
														</GoappTextRegular>
													</TouchableOpacity>
												</React.Fragment>
											) : attendee.imageStatus === 3 ? (
												<GoappTextRegular>Approved</GoappTextRegular>
											) : attendee.imageStatus === 4 ? (
												<GoappTextRegular>Rejected</GoappTextRegular>
											) : (
												<GoappTextRegular>Unknown Status</GoappTextRegular>
											)}
										</View>
									</View>
								))}
							</View>
						</ScrollView>
					</View>
					{this.state.showMinGuest ? (
						<GoappTextRegular color={"#e02020"}>
							Note: Minimum one guest is required!
						</GoappTextRegular>
					) : null}
					<TouchableOpacity
						style={styles.addVisitor}
						onPress={() =>
							this.props.navigation.navigate("AddVisitor", {
								item: this.props.item,
								onBack: () => this.handleBackFromOtherScreen()
							})
						}>
						<GoappTextBold size={20} style={styles.addVisitorText}>
							{"Add Visitors +"}
						</GoappTextBold>
					</TouchableOpacity>
					{this.props.isRemoveVisitorLoading ||
					this.props.isCancelMeetingLoading ? (
						<ProgressScreen
							isMessage={true}
							primaryMessage={"Hang On"}
							message={
								this.props.isRemoveVisitorLoading
									? "Removing Visitor"
									: "Cancelling Invitation"
							}
							indicatorSize={height / 30}
							indicatorColor={"#2C98F0"}
							messageStyle={{ fontSize: height / 65, width: width / 2.7 }}
							primaryMessageStyles={{
								fontSize: height / 60,
								width: width / 2.7
							}}
						/>
					) : null}
				</View>
			</View>
		);
	}
}

const mapStateToProps = state => ({
	appToken: state.appToken.token,
	isRemoveVisitorLoading:
		state.vms && state.vms.addVisitorReducer.isRemoveVisitorLoading,
	isRemoveVisitorSuccess:
		state.vms && state.vms.addVisitorReducer.isRemoveVisitorSuccess,
	isRemoveVisitorError:
		state.vms && state.vms.addVisitorReducer.isRemoveVisitorError,
	isViewInvitationLoading:
		state.vms && state.vms.vmsMeetingReducer.isViewInvitationLoading,
	item: state.vms && state.vms.vmsMeetingReducer.viewInvitation,
	isViewInvitationError:
		state.vms && state.vms.vmsMeetingReducer.isViewInvitationError,
	isCancelMeetingLoading:
		state.vms && state.vms.vmsMeetingReducer.isCancelMeetingLoading,
	isCancelMeetingError:
		state.vms && state.vms.vmsMeetingReducer.isCancelMeetingError,
	isCancelMeetingSuccess:
		state.vms && state.vms.vmsMeetingReducer.isCancelMeetingSuccess,
	isApprovePhotoError:
		state.vms && state.vms.vmsMeetingReducer.isApprovePhotoError,
	isApprovePhotoSuccess:
		state.vms && state.vms.vmsMeetingReducer.isApprovePhotoSuccess,
	isRejectPhotoError:
		state.vms && state.vms.vmsMeetingReducer.isRejectPhotoError,
	isRejectPhotoSuccess:
		state.vms && state.vms.vmsMeetingReducer.isRejectPhotoSuccess
});

InvitationDetails.contextType = DialogContext;

export default connect(mapStateToProps)(InvitationDetails);
