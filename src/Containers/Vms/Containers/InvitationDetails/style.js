import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	containerBox: { backgroundColor: "#ffffff", flex: 1 },
	loadingContainer: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center"
	},
	errorContainer: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center"
	},
	retryButton: {
		borderWidth: 1
	},
	retryText: {
		paddingHorizontal: width * 0.03,
		paddingVertical: width * 0.01
	},
	container: {
		backgroundColor: "#fff",
		marginVertical: height * 0.05,
		marginHorizontal: width * 0.03,
		padding: width * 0.05,
		borderWidth: 1,
		borderColor: "#EBEBEB",
		borderRadius: 20,
		shadowOpacity: 0.1,
		shadowRadius: 3,
		shadowOffset: {
			height: 2,
			width: 2
		}
	},
	editDelete: {
		flexDirection: "row",
		justifyContent: "flex-end"
	},
	verticalLine: {
		borderWidth: 1,
		borderColor: "#ececec",
		marginHorizontal: width * 0.02,
		height: height / 24
	},
	detailPair: {
		marginVertical: height * 0.01
	},
	label: {
		marginBottom: height * 0.01
	},
	guestsContainer: {
		flexDirection: "row"
	},
	guest: {
		marginVertical: width * 0.01,
		marginRight: width * 0.02,
		alignItems: "center",
		borderWidth: 1,
		borderColor: "#A2D2F8",
		borderRadius: height / 50,
		padding: height / 100
	},
	guestSection: {
		flexDirection: "row",
		justifyContent: "space-around",
		width: width / 2.5,
		paddingVertical: height / 120
	},
	addVisitor: {
		borderWidth: 1,
		borderColor: "#2c98f0",
		width: width * 0.5,
		alignItems: "center",
		marginVertical: height * 0.01,
		marginHorizontal: width * 0.03,
		borderRadius: width * 0.1
	},
	addVisitorText: {
		paddingVertical: height * 0.01,
		color: "#2c98f0"
	},
	approvedButton: {
		backgroundColor: "#2C98F0",
		paddingHorizontal: width / 30,
		paddingVertical: height / 370,
		borderRadius: height / 50
	},
	closeImage: {
		position: "absolute",
		top: -3,
		right: -3,
		zIndex: 4
	},
	input: {
		fontSize: height / 50
	}
});
