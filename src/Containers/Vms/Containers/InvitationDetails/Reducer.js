import {
	VMS_APPROVE_PHOTO,
	VMS_REJECT_PHOTO,
	VMS_CANCEL_MEETING,
	VMS_VIEW_INVITATION
} from "./Saga";
import {
	REQUEST,
	SUCCESS,
	FAILURE,
	action
} from "../../../../utils/reduxUtils";

const initialState = {
	isApprovePhotoLoading: false,
	isApprovePhotoSuccess: false,
	isApprovePhotoError: false,
	isRejectPhotoLoading: false,
	isRejectPhotoSuccess: false,
	isRejectPhotoError: false,
	isCancelMeetingLoading: false,
	isCancelMeetingSuccess: false,
	isCancelMeetingError: false,
	isViewInvitationLoading: false,
	viewInvitation: null,
	isViewInvitationError: false
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case VMS_APPROVE_PHOTO[REQUEST]:
			return {
				...state,
				isApprovePhotoLoading: true,
				isApprovePhotoError: false
			};
		case VMS_APPROVE_PHOTO[SUCCESS]:
			return {
				...state,
				isApprovePhotoLoading: false,
				isApprovePhotoSuccess: true
			};
		case VMS_APPROVE_PHOTO[FAILURE]:
			return {
				...state,
				isApprovePhotoLoading: false,
				isApprovePhotoError: true
			};

		case VMS_REJECT_PHOTO[REQUEST]:
			return {
				...state,
				isRejectPhotoLoading: true,
				isRejectPhotoError: false
			};
		case VMS_REJECT_PHOTO[SUCCESS]:
			return {
				...state,
				isRejectPhotoLoading: false,
				isRejectPhotoSuccess: true
			};
		case VMS_REJECT_PHOTO[FAILURE]:
			return {
				...state,
				isRejectPhotoLoading: false,
				isRejectPhotoError: true
			};

		case VMS_CANCEL_MEETING[REQUEST]:
			return {
				...state,
				isCancelMeetingLoading: true,
				isCancelMeetingError: false,
				isCancelMeetingSuccess: false
			};
		case VMS_CANCEL_MEETING[SUCCESS]:
			return {
				...state,
				isCancelMeetingLoading: false,
				isCancelMeetingSuccess: true
			};
		case VMS_CANCEL_MEETING[FAILURE]:
			return {
				...state,
				isCancelMeetingLoading: false,
				isCancelMeetingError: true
			};
		case VMS_VIEW_INVITATION[REQUEST]:
			return {
				...state,
				isViewInvitationLoading: true,
				viewInvitation: null,
				isViewInvitationError: false
			};
		case VMS_VIEW_INVITATION[SUCCESS]:
			return {
				...state,
				isViewInvitationLoading: false,
				viewInvitation: payload.response
			};
		case VMS_VIEW_INVITATION[FAILURE]:
			return {
				...state,
				isViewInvitationLoading: false,
				isViewInvitationError: true
			};
		default:
			return state;
	}
};
