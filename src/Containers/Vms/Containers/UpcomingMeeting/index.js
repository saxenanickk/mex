import React, { Component } from "react";
import {
	View,
	ActivityIndicator,
	TouchableOpacity,
	FlatList,
	Platform
} from "react-native";
import { connect } from "react-redux";
import {
	GoappTextBold,
	GoappTextRegular
} from "../../../../Components/GoappText";
import { styles } from "./style";
import Icon from "../../../../Components/Icon";
import MeetingItem from "../../Components/MeetingItem";
import { vmsUpcomingMeetings } from "../Home/Saga";

class UpcomingMeeting extends Component {
	handleRetry = () => {
		// fetch upcoming meetings again
	};

	handleBackFromOtherScreen = () => {
		this.props.dispatch(
			vmsUpcomingMeetings.request(this.props.appToken, this.props.hostId)
		);
	};

	render() {
		if (this.props.isLoading) {
			return (
				<View style={styles.loadingContainer}>
					<ActivityIndicator />
				</View>
			);
		}

		if (this.props.error) {
			return (
				<View style={styles.errorContainer}>
					<GoappTextRegular size={24}>
						Failed to fetch meetings
					</GoappTextRegular>
					<TouchableOpacity
						style={styles.retryButton}
						onPress={this.handleRetry}>
						<GoappTextRegular style={styles.retryText}>Retry</GoappTextRegular>
					</TouchableOpacity>
				</View>
			);
		}

		if (this.props.upcomingMeetings.length == 0) {
			return (
				<View style={styles.noMeetingContainer}>
					<Icon
						iconType={"evilicon"}
						iconName={"calendar"}
						iconSize={48}
						iconColor={"#2c98f0"}
					/>
					<GoappTextBold>No meetings Yet!</GoappTextBold>
				</View>
			);
		}

		return (
			<FlatList
				nestedScrollEnabled={true}
				refreshing={this.props.isLoading}
				data={this.props.upcomingMeetings}
				onRefresh={() =>
					this.props.dispatch(
						vmsUpcomingMeetings.request(this.props.appToken, this.props.hostId)
					)
				}
				extraData={[this.props.isLoading]}
				renderItem={({ item }) => (
					<MeetingItem
						purpose={item.purpose}
						startTime={item.scheduleStart}
						endTime={item.scheduleEnd}
						location={this.props.location[item.zoneId]}
						attendees={item.visitors}
						navigate={() =>
							this.props.navigation.navigate("InvitationDetails", {
								invitationInfoId: item.invitationInfoId,
								location: this.props.location[item.zoneId],
								onBack: () => this.handleBackFromOtherScreen()
							})
						}
					/>
				)}
				keyExtractor={(item, index) => index.toString()}
			/>
		);
	}
}

const mapStateToProps = state => ({
	appToken: state.appToken.token,
	hostId: Array.isArray(JSON.parse(state.appToken.userProfile.userdevices))
		? JSON.parse(state.appToken.userProfile.userdevices)[0].userId
		: null,
	isLoading: state.vms && state.vms.meetingsReducer.isUpcomingMeetingsLoading,
	error: state.vms && state.vms.meetingsReducer.isUpcomingMeetingsError,
	upcomingMeetings: state.vms && state.vms.meetingsReducer.upcomingMeetings,
	location: state.vms && state.vms.editMeetingsReducer.locationObject
});

export default connect(mapStateToProps)(UpcomingMeeting);
