import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	loadingContainer: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center"
	},
	noMeetingContainer: {
		flex: 1,
		alignItems: "center",
		justifyContent: "flex-start",
		marginTop: height * 0.15
	},
	errorContainer: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center"
	},
	retryButton: {
		borderWidth: 1,
		borderRadius: height / 20,
		marginTop: height / 40
	},
	retryText: {
		paddingHorizontal: width * 0.03,
		paddingVertical: width * 0.01,
		fontSize: height / 50
	}
});
