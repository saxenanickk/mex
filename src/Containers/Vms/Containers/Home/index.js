import React, { Component } from "react";
import { TouchableOpacity, View, Dimensions } from "react-native";
import { connect } from "react-redux";
import {
	GoappTextRegular,
	GoappTextBold
} from "../../../../Components/GoappText";
import Icon from "../../../../Components/Icon";
import { styles } from "./style";
import MeetingTabView from "./MeetingTabView";
import { vmsUpcomingMeetings, vmsPastMeetings } from "./Saga";

import { vmsGetLocations } from "../EditMeeting/Saga";
import { ProgressScreen } from "../../../../Components";
const { height, width } = Dimensions.get("window");

/**
 * @typedef {Object} Props
 * @property {string} appToken
 * @property {string} hostId
 * @property {typeof vmsUpcomingMeetings.request} requestUpcomingMeetings
 * @property {typedef vmsPastMeetings.request} requestPastMeetings
 */

/**
 * @typedef {Object} State
 */

/**
 * @augments {Component<Props, State>}
 */
class Home extends Component {
	componentDidMount() {
		if (this.props.hostId) {
			this.props.dispatch(
				vmsUpcomingMeetings.request(this.props.appToken, this.props.hostId)
			);
			this.props.dispatch(
				vmsPastMeetings.request(this.props.appToken, this.props.hostId)
			);
			this.props.dispatch(
				vmsGetLocations.request(this.props.appToken, this.props.tenantId)
			);
		} else {
			// put an error message
			console.log("host id is null");
		}
	}

	handleBackFromOtherScreen = () => {
		this.props.dispatch(
			vmsUpcomingMeetings.request(this.props.appToken, this.props.hostId)
		);
	};

	handleInviteVisitor = () => {
		this.props.navigation.navigate("CreateMeeting", {
			onBack: () => this.handleBackFromOtherScreen()
		});
	};

	handleRetry = () => {
		this.props.dispatch(
			vmsGetLocations.request(this.props.appToken, this.props.tenantId)
		);
	};

	render() {
		return (
			<View style={{ flex: 1, backgroundColor: "#ffffff" }}>
				<View>
					<TouchableOpacity
						style={styles.inviteButton}
						onPress={this.handleInviteVisitor}>
						<Icon
							iconName={"mail-read"}
							iconType={"octicons"}
							iconColor={"#fff"}
							iconSize={22}
						/>
						<GoappTextRegular style={styles.inviteButtonText}>
							Invite Visitor
						</GoappTextRegular>
					</TouchableOpacity>
				</View>
				{this.props.locations.length > 0 ? (
					<MeetingTabView navigation={this.props.navigation} />
				) : this.props.isLocationsError ? (
					<View style={styles.errorContainer}>
						<GoappTextBold>Something went wrong</GoappTextBold>
						<TouchableOpacity
							style={styles.retryButton}
							onPress={this.handleRetry}>
							<GoappTextRegular style={styles.retryText}>
								Retry
							</GoappTextRegular>
						</TouchableOpacity>
					</View>
				) : (
					<ProgressScreen
						isMessage={true}
						primaryMessage={"Hang On"}
						message={"Loading meetings"}
						indicatorSize={height / 30}
						indicatorColor={"#2C98F0"}
						messageStyle={{ fontSize: height / 65, width: width / 2.7 }}
						primaryMessageStyles={{
							fontSize: height / 60,
							width: width / 2.7
						}}
					/>
				)}
			</View>
		);
	}
}

const mapStateToProps = state => ({
	appToken: state.appToken.token,
	hostId: Array.isArray(JSON.parse(state.appToken.userProfile.userdevices))
		? JSON.parse(state.appToken.userProfile.userdevices)[0].userId
		: null,
	tenantId: state.appToken.userProfile.tenantdto
		? JSON.parse(state.appToken.userProfile.tenantdto).tenant_id
		: null,
	locations: state.vms && state.vms.editMeetingsReducer.locations,
	isLocationsError: state.vms && state.vms.editMeetingsReducer.isLocationsError,
	locationObject: state.vms && state.vms.editMeetingsReducer.locationObject
});

export default connect(mapStateToProps)(Home);
