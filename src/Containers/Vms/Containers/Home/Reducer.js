import { VMS_UPCOMING_MEETINGS, VMS_PAST_MEETINGS } from "./Saga";
import { REQUEST, SUCCESS, FAILURE } from "../../../../utils/reduxUtils";

const initialState = {
	isUpcomingMeetingsLoading: false,
	upcomingMeetings: [],
	isUpcomingMeetingsError: false,
	isPastMeetingsLoading: false,
	pastMeetings: [],
	ispastMeetingsError: false
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case VMS_UPCOMING_MEETINGS[REQUEST]:
			return {
				...state,
				isUpcomingMeetingsLoading: true,
				isUpcomingMeetingsError: false
			};
		case VMS_UPCOMING_MEETINGS[SUCCESS]:
			return {
				...state,
				isUpcomingMeetingsLoading: false,
				...payload.response
			};
		case VMS_UPCOMING_MEETINGS[FAILURE]:
			return {
				...state,
				isUpcomingMeetingsError: true,
				isUpcomingMeetingsLoading: false
			};
		case VMS_PAST_MEETINGS[REQUEST]:
			return {
				...state,
				isPastMeetingsLoading: true,
				ispastMeetingsError: false
			};
		case VMS_PAST_MEETINGS[SUCCESS]:
			return { ...state, isPastMeetingsLoading: false, ...payload.response };
		case VMS_PAST_MEETINGS[FAILURE]:
			return {
				...state,
				ispastMeetingsError: true,
				isPastMeetingsLoading: false
			};
		default:
			return state;
	}
};
