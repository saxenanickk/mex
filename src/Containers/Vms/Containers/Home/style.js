import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("screen");

export const styles = StyleSheet.create({
	inviteButton: {
		backgroundColor: "#2C98F0",
		paddingHorizontal: width / 80,
		paddingVertical: height / 150,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 10,
		flexDirection: "row",
		width: width / 1.4,
		alignSelf: "center",
		marginVertical: height / 50
	},
	inviteButtonText: {
		color: "#fff",
		fontSize: 18,
		padding: height * 0.01,
		paddingLeft: width * 0.03
	},
	errorContainer: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center"
	},
	retryButton: {
		borderRadius: height / 20,
		marginTop: height / 40,
		backgroundColor: "#2C98F0"
	},
	retryText: {
		paddingHorizontal: width * 0.03,
		paddingVertical: width * 0.01,
		fontSize: height / 50,
		color: "#fff"
	}
});
