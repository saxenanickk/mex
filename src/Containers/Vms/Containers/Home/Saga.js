import { takeLatest, put, call } from "redux-saga/effects";
import {
	createRequestTypes,
	action,
	REQUEST,
	SUCCESS,
	FAILURE
} from "../../../../utils/reduxUtils";
import VmsApi from "../../Api";
import { Action } from "redux";

/**
 * Constants
 */
export const VMS_UPCOMING_MEETINGS = createRequestTypes(
	"VMS_UPCOMING_MEETINGS"
);
export const VMS_PAST_MEETINGS = createRequestTypes("VMS_PAST_MEETINGS");

/**
 * Action Creators
 */
export const vmsUpcomingMeetings = {
	request: (appToken, hostId) =>
		action(VMS_UPCOMING_MEETINGS[REQUEST], { appToken, hostId }),
	success: response => action(VMS_UPCOMING_MEETINGS[SUCCESS], { response }),
	failure: error => action(VMS_UPCOMING_MEETINGS[FAILURE], { error })
};

export const vmsPastMeetings = {
	request: (appToken, hostId) =>
		action(VMS_PAST_MEETINGS[REQUEST], { appToken, hostId }),
	success: response => action(VMS_PAST_MEETINGS[SUCCESS], { response }),
	failure: error => action(VMS_PAST_MEETINGS[FAILURE], { error })
};

export function* vmsMeetingsListSaga(dispatch) {
	yield takeLatest(
		VMS_UPCOMING_MEETINGS[REQUEST],
		handleFetchVmsUpcomingMeetings
	);
	yield takeLatest(VMS_PAST_MEETINGS[REQUEST], handleFetchVmsPastMeetings);
}

/**
 * @param {Action} action
 */
function* handleFetchVmsUpcomingMeetings(action) {
	try {
		let upcomingMeetings = yield call(
			VmsApi.getUpcomingMeetings,
			action.payload
		);
		console.log(upcomingMeetings.response);
		if (Array.isArray(upcomingMeetings.response)) {
			yield put(
				vmsUpcomingMeetings.success({
					upcomingMeetings: upcomingMeetings.response
				})
			);
		} else {
			yield put(vmsUpcomingMeetings.failure("something went wrong!"));
		}
	} catch (error) {
		yield put(vmsUpcomingMeetings.failure(error));
	}
}

function* handleFetchVmsPastMeetings(action) {
	try {
		let pastMeetings = yield call(VmsApi.getPastMeeting, action.payload);
		console.log(pastMeetings);
		if (Array.isArray(pastMeetings.response)) {
			yield put(
				vmsPastMeetings.success({
					pastMeetings: pastMeetings.response
				})
			);
		} else {
			yield put(vmsPastMeetings.failure("something went wrong!"));
		}
	} catch (error) {
		yield put(vmsPastMeetings.failure(error));
	}
}
