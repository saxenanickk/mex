import React from "react";
import { Dimensions } from "react-native";
import { TabView, TabBar } from "react-native-tab-view";
import UpcomingMeeting from "../UpcomingMeeting";
import PastMeeting from "../PastMeeting";

class MeetingTabView extends React.Component {
	state = {
		index: 0,
		routes: [
			{ key: "upcoming", title: "Upcoming Meetings" },
			{ key: "past", title: "Past Meetings" }
		]
	};

	render() {
		return (
			<TabView
				navigationState={this.state}
				renderScene={({ route }) => {
					switch (route.key) {
						case "upcoming":
							return <UpcomingMeeting navigation={this.props.navigation} />;
						case "past":
							return <PastMeeting navigation={this.props.navigation} />;
						default:
							return null;
					}
				}}
				onIndexChange={index => this.setState({ index })}
				initialLayout={{ width: Dimensions.get("window").width, height: 0 }}
				renderTabBar={props => (
					<TabBar
						{...props}
						style={{ backgroundColor: "#fff" }}
						labelStyle={{ color: "#2c98f0" }}
						indicatorStyle={{ backgroundColor: "#2c98f0" }}
						getLabelText={({ route }) => route.title}
					/>
				)}
			/>
		);
	}
}
export default MeetingTabView;
