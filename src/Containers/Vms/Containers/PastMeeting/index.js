import React, { Component } from "react";
import {
	View,
	ActivityIndicator,
	TouchableOpacity,
	FlatList
} from "react-native";
import { connect } from "react-redux";
import {
	GoappTextBold,
	GoappTextRegular
} from "../../../../Components/GoappText";
import { styles } from "./style";
import Icon from "../../../../Components/Icon";
import MeetingItem from "../../Components/MeetingItem";
import { vmsPastMeetings } from "../Home/Saga";

class PastMeeting extends Component {
	handleRetry = () => {
		// fetch past meetings again
		if (this.props.hostId) {
			this.props.dispatch(
				vmsPastMeetings.request(this.props.appToken, this.props.hostId)
			);
		}
	};

	render() {
		if (this.props.isLoading) {
			return (
				<View style={styles.loadingContainer}>
					<ActivityIndicator />
				</View>
			);
		}

		if (this.props.error) {
			return (
				<View style={styles.errorContainer}>
					<GoappTextRegular size={24}>
						Failed to fetch meetings
					</GoappTextRegular>
					<TouchableOpacity
						style={styles.retryButton}
						onPress={this.handleRetry}>
						<GoappTextRegular style={styles.retryText}>Retry</GoappTextRegular>
					</TouchableOpacity>
				</View>
			);
		}

		if (this.props.pastMeetings.length == 0) {
			return (
				<View style={styles.noMeetingContainer}>
					<Icon
						iconType={"evilicon"}
						iconName={"calendar"}
						iconSize={48}
						iconColor={"#2c98f0"}
					/>
					<GoappTextBold>No meetings Yet!</GoappTextBold>
				</View>
			);
		}

		return (
			<View>
				<FlatList
					data={this.props.pastMeetings}
					refreshing={this.props.isLoading}
					onRefresh={() =>
						this.props.dispatch(
							vmsPastMeetings.request(this.props.appToken, this.props.hostId)
						)
					}
					renderItem={({ item }) => (
						<MeetingItem
							purpose={item.purpose}
							startTime={item.scheduleStart}
							endTime={item.scheduleEnd}
							location={this.props.location[item.zoneId]}
							attendees={item.visitors}
							disabled={true}
							navigate={() =>
								this.props.navigation.navigate("InvitationDetails", {
									invitationInfoId: item.invitationInfoId
								})
							}
						/>
					)}
					keyExtractor={(item, index) => index.toString()}
				/>
			</View>
		);
	}
}
const mapStateToProps = state => ({
	isLoading: state.vms && state.vms.meetingsReducer.isPastMeetingsLoading,
	error: state.vms && state.vms.meetingsReducer.ispastMeetingsError,
	pastMeetings: state.vms && state.vms.meetingsReducer.pastMeetings,
	appToken: state.appToken.token,
	hostId: Array.isArray(JSON.parse(state.appToken.userProfile.userdevices))
		? JSON.parse(state.appToken.userProfile.userdevices)[0].userId
		: null,
	location: state.vms && state.vms.editMeetingsReducer.locationObject
});

export default connect(mapStateToProps)(PastMeeting);
