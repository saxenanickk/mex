import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: {
		marginHorizontal: width * 0.05,
		marginVertical: height * 0.04
	},
	inviteButton: {
		backgroundColor: "#2c98f0",
		alignItems: "center",
		justifyContent: "center",
		borderRadius: width * 0.1,
		marginVertical: height * 0.01
	},
	inviteText: {
		paddingVertical: height * 0.01
	},
	heading: {
		marginHorizontal: width * 0.05,
		marginBottom: height * 0.01
	}
});
