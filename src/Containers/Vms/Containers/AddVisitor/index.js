import React, { Component } from "react";
import { TouchableOpacity, View, Dimensions, Platform } from "react-native";
import { connect } from "react-redux";
import EditVisitor from "../../Components/EditVisitor";
import { styles } from "./style";
import { GoappTextBold } from "../../../../Components/GoappText";
import { GoToast, ProgressScreen } from "../../../../Components";
import DialogContext from "../../../../DialogContext";
import { vmsAddVisitor } from "./Saga";
import { get } from "lodash";
import { VERIFIED_CORPORATE } from "../../../../Assets/Img/Image";
import {
	validatePhoneNumberDigit,
	validateEmail
} from "../../../../utils/validation";

const { width, height } = Dimensions.get("window");

class AddVisitor extends Component {
	static navigationOptions = {
		title: "Add Visitor",
		headerForceInset: {
			top: "never",
			bottom: "never"
		},
		headerTitleStyle: {
			fontFamily: Platform.OS === "ios" ? "Avenir" : "LatoRegular"
		}
	};

	state = {
		visitor: {
			name: {
				isValid: false,
				value: "",
				validationMsg: "Please Enter Visitor Name"
			},
			email: {
				isValid: true,
				value: "",
				validationMsg: ""
			},
			mobile: {
				isValid: false,
				value: "",
				validationMsg: "Please Enter Visitor Mobile Number"
			},
			countryCode: {
				isValid: true,
				value: "+91",
				validationMsg: ""
			},
			isValid: true,
			validationMsg: ""
		}
	};

	componentDidUpdate(prevProps, prevState) {
		if (!prevProps.isAddVisitorSuccess && this.props.isAddVisitorSuccess) {
			const { route } = this.props;
			const { onBack } = route.params ? route.params : {};

			onBack();
			this.props.navigation.goBack();
			this.context.current.openDialog({
				type: "Information",
				title: "Done!",
				message: "Visitor added successfully!",
				icon: VERIFIED_CORPORATE
			});
		}

		if (!prevProps.isAddVisitorError && this.props.isAddVisitorError) {
			GoToast.show("Failed to add Guest", "Error", 1000);
		}
	}

	handleVisitorChange = (text, type) => {
		let { visitor } = this.state;
		visitor[type].value = text;
		if (type === "name") {
			let nameRegex = /^[A-Za-z .]+$/;
			let isNameValid = nameRegex.test(text);
			if (!isNameValid && text.length > 0) {
				GoToast.show(
					"Name cannot contain numbers and special characters",
					"Error"
				);
				visitor[type].isValid = false;
				visitor[type].validationMsg =
					"Name cannot contain numbers and special characters";
				this.setState({ visitor });
			} else {
				visitor[type].isValid = true;
				visitor[type].validationMsg = "";
				this.setState({ visitor });
			}
		}
		if (type === "mobile") {
			let phoneRegex = /^[0-9]+$/;
			let isPhoneValid = phoneRegex.test(text);
			if (!isPhoneValid && text.length > 0) {
				GoToast.show("Please enter valid mobile number", "Error");
				visitor[type].isValid = false;
				visitor[type].validationMsg = "Please enter valid mobile number";
				this.setState({ visitor });
			} else {
				visitor[type].isValid = true;
				visitor[type].validationMsg = "";
				this.setState({ visitor });
			}
		}
	};

	handleSubmit = () => {
		const { route } = this.props;
		const { item } = route.params ? route.params : {};

		// Todo handle this properly

		if (!this.state.visitor.name.value.length > 0) {
			GoToast.show("Name is mandatory", "Error");
			return;
		}
		if (!this.state.visitor.name.isValid) {
			GoToast.show("Name cannot contain special characters", "Error");
			return;
		}
		if (!this.state.visitor.mobile.value.length > 0) {
			GoToast.show("Please Enter phone number", "Error");
			return;
		}
		let number = `${this.state.visitor.countryCode.value}${
			this.state.visitor.mobile.value
		}`;
		let isValidNumber = validatePhoneNumberDigit(number);
		if (!isValidNumber || !this.state.visitor.mobile.isValid) {
			GoToast.show("Invalid Mobile number", "Error");
			return;
		}
		let isNumberExistsInVisitorList = item.visitorList.some(
			visitor => visitor.mobile.slice(-10) === number.slice(-10)
		);
		if (isNumberExistsInVisitorList) {
			GoToast.show("Mobile Number already exists in visitors", "Error");
			return;
		}
		const isValidEmail = !validateEmail(this.state.visitor.email.value);
		if (this.state.visitor.email.value.length > 0 && isValidEmail) {
			GoToast.show("Please enter valid email id");
			return;
		}
		let isEmailExistsInVisitorList = item.visitorList.includes(
			visitor => visitor.email === this.state.visitor.email.value
		);
		if (isEmailExistsInVisitorList) {
			GoToast.show("Email already exists in visitor list", "Error");
			return;
		}
		const body = {
			invitationInfoId: item.invitationInfoId,
			visitorList: [
				{
					mobile: number,
					name: this.state.visitor.name.value,
					email: this.state.visitor.email.value
				}
			]
		};
		// Dispatch action
		this.props.dispatch(vmsAddVisitor.request(this.props.appToken, body));
	};

	render() {
		return (
			<View style={styles.container}>
				<GoappTextBold style={styles.heading}>
					Add Visitor Details
				</GoappTextBold>
				<EditVisitor
					visitor={this.state.visitor}
					onVisitorChange={(text, type) => this.handleVisitorChange(text, type)}
				/>
				<TouchableOpacity
					style={styles.inviteButton}
					onPress={this.handleSubmit}>
					<GoappTextBold color={"#fff"} size={20} style={styles.inviteText}>
						Send Invitation
					</GoappTextBold>
				</TouchableOpacity>
				{this.props.isAddVisitorLoading ? (
					<ProgressScreen
						isMessage={true}
						primaryMessage={"Hang On"}
						message={"Adding Visitor"}
						indicatorSize={height / 30}
						indicatorColor={"#2C98F0"}
						messageStyle={{ fontSize: height / 65, width: width / 2.7 }}
						primaryMessageStyles={{
							fontSize: height / 60,
							width: width / 2.7
						}}
					/>
				) : null}
			</View>
		);
	}
}

const mapStateToProps = state => ({
	appToken: state.appToken.token,
	isAddVisitorLoading: get(
		state,
		"vms.addVisitorReducer.isAddVisitorLoading",
		false
	),
	isAddVisitorSuccess: get(
		state,
		"vms.addVisitorReducer.isAddVisitorSuccess",
		null
	),
	isAddVisitorError: get(
		state,
		"vms.addVisitorReducer.isAddVisitorError",
		false
	)
});

AddVisitor.contextType = DialogContext;

export default connect(mapStateToProps)(AddVisitor);
