import { takeLatest, put, call } from "redux-saga/effects";
import VmsApi from "../../Api";
import {
	createRequestTypes,
	action,
	REQUEST,
	SUCCESS,
	FAILURE
} from "../../../../utils/reduxUtils";
import { vmsViewInvitation } from "../InvitationDetails/Saga";

/**
 * Constants
 */

export const VMS_ADD_VISITOR = createRequestTypes("VMS_ADD_VISITOR");
export const VMS_REMOVE_VISITOR = createRequestTypes("VMS_REMOVE_VISITOR");

/**
 * Action Creators
 */

export const vmsAddVisitor = {
	request: (appToken, body) =>
		action(VMS_ADD_VISITOR[REQUEST], { appToken, body }),
	success: response => action(VMS_ADD_VISITOR[SUCCESS], { response }),
	failure: error => action(VMS_ADD_VISITOR[FAILURE], { error })
};

export const vmsRemoveVisitor = {
	request: (appToken, body) =>
		action(VMS_REMOVE_VISITOR[REQUEST], { appToken, body }),
	success: response => action(VMS_REMOVE_VISITOR[SUCCESS], { response }),
	failure: error => action(VMS_REMOVE_VISITOR[FAILURE], { error })
};

export function* vmsAddVisitorSaga(dispatch) {
	yield takeLatest(VMS_ADD_VISITOR[REQUEST], handleAddVisitor);
	yield takeLatest(VMS_REMOVE_VISITOR[REQUEST], handleRemoveVisitor);
}

function* handleAddVisitor(action) {
	try {
		const addVisitorResponse = yield call(VmsApi.addGuest, action.payload);
		console.log(addVisitorResponse);
		yield put(vmsAddVisitor.success(addVisitorResponse));
	} catch (error) {
		yield put(vmsAddVisitor.failure(error));
	}
}

function* handleRemoveVisitor(action) {
	try {
		const removeVisitorResponse = yield call(
			VmsApi.removeGuest,
			action.payload
		);
		yield put(vmsRemoveVisitor.success(removeVisitorResponse));
		yield put(
			vmsViewInvitation.request(
				action.payload.appToken,
				action.payload.body.invitationInfoId
			)
		);
	} catch (error) {
		yield put(vmsRemoveVisitor.failure(error));
	}
}
