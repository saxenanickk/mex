import { VMS_ADD_VISITOR, VMS_REMOVE_VISITOR } from "./Saga";
import { REQUEST, SUCCESS, FAILURE } from "../../../../utils/reduxUtils";

const initialState = {
	isAddVisitorLoading: false,
	isAddVisitorSuccess: null,
	isAddVisitorError: false,
	isRemoveVisitorLoading: false,
	isRemoveVisitorSuccess: null,
	isRemoveVisitorError: false
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case VMS_ADD_VISITOR[REQUEST]:
			return {
				...state,
				isAddVisitorLoading: true,
				isAddVisitorSuccess: null,
				isAddVisitorError: false
			};
		case VMS_ADD_VISITOR[SUCCESS]:
			return {
				...state,
				isAddVisitorLoading: false,
				isAddVisitorSuccess: true
			};
		case VMS_ADD_VISITOR[FAILURE]:
			return { ...state, isAddVisitorLoading: false, isAddVisitorError: true };
		case VMS_REMOVE_VISITOR[REQUEST]:
			return {
				...state,
				isRemoveVisitorLoading: true,
				isRemoveVisitorSuccess: null,
				isRemoveVisitorError: false
			};
		case VMS_REMOVE_VISITOR[SUCCESS]:
			return {
				...state,
				isRemoveVisitorLoading: false,
				isRemoveVisitorSuccess: true
			};
		case VMS_REMOVE_VISITOR[FAILURE]:
			return {
				...state,
				isRemoveVisitorLoading: false,
				isRemoveVisitorError: true
			};
		default:
			return state;
	}
};
