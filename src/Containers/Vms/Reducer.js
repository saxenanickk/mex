import { combineReducers } from "redux";
// Import Different Reducers Here
import meetingsReducer from "./Containers/Home/Reducer";
import editMeetingsReducer from "./Containers/EditMeeting/Reducer";
import createMeetingsReducer from "./Containers/CreateMeeting/Reducer";
import addVisitorReducer from "./Containers/AddVisitor/Reducer";
import vmsMeetingReducer from "./Containers/InvitationDetails/Reducer";

const vmsReducer = combineReducers({
	meetingsReducer,
	editMeetingsReducer,
	createMeetingsReducer,
	addVisitorReducer,
	vmsMeetingReducer
});

export default vmsReducer;
