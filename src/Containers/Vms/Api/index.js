import Config from "react-native-config";

const { SERVER_BASE_URL_VMS } = Config;

class VmsApi {
	constructor() {
		console.log("Vms Api instantiated");
	}

	/**
	 * Get Upcoming Meetings
	 * @param {Object} params
	 * @param {string} params.hostId User Id
	 * @param {string} params.appToken
	 */
	getUpcomingMeetings(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_VMS}/currentMeeting?hostId=${params.hostId}`, {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"Content-Type": "application/json"
					}
				})
					.then(res =>
						res
							.json()
							.then(response => {
								if (res.status === 200 && response.success === 1) {
									resolve(response.data);
								} else if (
									response.success === 0 &&
									response.message === "failed to get current meeting."
								) {
									resolve({ response: [] });
								} else {
									reject("Something went wrong");
								}
							})
							.catch(err => reject(err))
					)
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	getPastMeeting(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_VMS}/pastMeeting?hostId=${params.hostId}`, {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"Content-Type": "application/json"
					}
				})
					.then(res =>
						res
							.json()
							.then(response => {
								if (res.status === 200 && response.success === 1) {
									resolve(response.data);
								} else if (
									response.success === 0 &&
									response.message === "failed to view past meeting."
								) {
									resolve({ response: [] });
								} else {
									reject("Something went wrong");
								}
							})
							.catch(err => reject(err))
					)
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * @param {Object} params
	 * @param {String} params.invitationInfoId
	 * @param {String} params.appToken
	 */
	viewInvitation(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(
					`${SERVER_BASE_URL_VMS}/viewInvitation?invitationInfoId=${
						params.invitationInfoId
					}`,
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": params.appToken,
							"Content-Type": "application/json"
						}
					}
				)
					.then(res =>
						res
							.json()
							.then(response => {
								if (res.status === 200 && response.success === 1) {
									resolve(response.data.response);
								} else {
									reject("Something went wrong");
								}
							})
							.catch(err => reject(err))
					)
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	createInvitation(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_VMS}/createInvitation`, {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"Content-Type": "application/json"
					},
					body: JSON.stringify(params.body)
				})
					.then(res =>
						res
							.json()
							.then(response => {
								if (res.status === 200 && response.success === 1) {
									console.log(response);
									resolve(response.data);
								} else {
									reject("Something went wrong");
								}
							})
							.catch(err => reject(err))
					)
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 *
	 * @param {Object} params
	 * @param {string} params.appToken
	 * @param {Object} params.body
	 */
	editIvitation(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_VMS}/editInvitation`, {
					method: "PUT",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"Content-Type": "application/json"
					},
					body: JSON.stringify(params.body)
				})
					.then(res =>
						res
							.json()
							.then(response => {
								if (res.status === 200 && response.success === 1) {
									resolve(response.data);
								} else {
									reject("Something went wrong");
								}
							})
							.catch(err => reject(err))
					)
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 *
	 * @param {Object} params
	 * @param {String} params.appToken
	 * @param {Object} params.body
	 */
	addGuest(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_VMS}/addGuest`, {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"Content-Type": "application/json"
					},
					body: JSON.stringify(params.body)
				})
					.then(res =>
						res
							.json()
							.then(response => {
								if (
									res.status === 200 &&
									response.success === 1 &&
									response.data.message === "Guest added Successfully"
								) {
									resolve(response.data);
								} else {
									reject("Something went wrong");
								}
							})
							.catch(err => reject(err))
					)
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * @param {Object} params
	 * @param {string} params.appToken
	 * @param {object} params.body
	 */
	removeGuest(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_VMS}/removeGuest`, {
					method: "PUT",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"Content-Type": "application/json"
					},
					body: JSON.stringify(params.body)
				})
					.then(res =>
						res
							.json()
							.then(response => {
								console.log(response);
								if (
									res.status === 200 &&
									response.success === 1 &&
									response.data.message === "Guest removed Successfully"
								) {
									resolve(response.data);
								} else {
									reject("Something went wrong");
								}
							})
							.catch(err => reject(err))
					)
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * @param {Object} params
	 * @param {string} params.appToken
	 * @param {object} params.body
	 */
	approvePhoto(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_VMS}/approvePhoto`, {
					method: "PUT",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"Content-Type": "application/json"
					},
					body: JSON.stringify(params.body)
				})
					.then(res =>
						res
							.json()
							.then(response => {
								console.log(response);
								if (
									res.status === 200 &&
									response.success === 1 &&
									response.data.message === "Visitor Photo Approved"
								) {
									resolve(response.data);
								} else {
									reject("Something went wrong");
								}
							})
							.catch(err => reject(err))
					)
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * @param {Object} params
	 * @param {string} params.appToken
	 * @param {object} params.body
	 */
	rejectPhoto(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_VMS}/rejectPhoto`, {
					method: "PUT",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"Content-Type": "application/json"
					},
					body: JSON.stringify(params.body)
				})
					.then(res =>
						res
							.json()
							.then(response => {
								console.log(response);
								if (res.status === 200 && response.success === 1) {
									resolve(response.data);
								} else {
									reject("Something went wrong");
								}
							})
							.catch(err => reject(err))
					)
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * @param {Object} params
	 * @param {string} params.appToken
	 * @param {object} params.body
	 */
	cancelInvitation(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_VMS}/cancelInvitation`, {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"Content-Type": "application/json"
					},
					body: JSON.stringify(params.body)
				})
					.then(res =>
						res
							.json()
							.then(response => {
								console.log(response);
								if (
									res.status === 200 &&
									response.success === 1 &&
									response.data.response.invitationStatus === 2
								) {
									resolve(response.data);
								} else {
									reject("Something went wrong");
								}
							})
							.catch(err => reject(err))
					)
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 *
	 * @param {Object} params
	 * @param {string} params.tenantId
	 * @param {string} params.appToken
	 */
	tenantZoneDetails(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(
					`${SERVER_BASE_URL_VMS}/tenantZoneDetails?tenant_id=${
						params.tenantId
					}`,
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": params.appToken,
							"Content-Type": "application/json"
						}
					}
				).then(res =>
					res
						.json()
						.then(response => {
							if (res.status === 200 && response.success === 1) {
								let zones = response.data.response.map(zone => {
									return {
										zoneId: zone.zoneId,
										status: zone.status,
										address: zone.address
									};
								});
								resolve(zones);
							} else {
								reject("Something  went wrong");
							}
						})
						.catch(err => reject(err))
						.catch(err => reject(err))
				);
			} catch (error) {
				reject(error);
			}
		});
	}
}

export default new VmsApi();
