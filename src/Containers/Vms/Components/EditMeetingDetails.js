import React, { PureComponent } from "react";
import { StyleSheet, View, TouchableOpacity, Dimensions } from "react-native";
import {
	GoappTextRegular,
	GoappTextInputRegular
} from "../../../Components/GoappText";
import { Icon, GoToast } from "../../../Components";
import Dropdown from "../../../Components/Dropdown";
import GoDateTimePicker from "../../../Components/GoDateTimePicker";
import { _24to12 } from "../../../utils/dateUtils";

const { width, height } = Dimensions.get("window");
const currentDate = new Date();
const maximumDate = new Date().setMonth(currentDate.getMonth() + 2);

/**
 * @typedef {("date" | "time" | null)} DatePickerType
 * @typedef {("date" | "startTime" | "endTime" | null)} Type
 */

/**
 * @typedef {Object} Props
 * @property {Date} date
 * @property {(date: Date) => void} handleDateChange
 * @property {Date} startTime
 * @property {(date: Date) => void} handleStartTimeChange
 * @property {Date} endTime
 * @property {(date: Date) => void} handleEndTimeChange
 * @property {string} campus
 * @property {(campusName: string) => void} handleCampusChange
 * @property {string} floor
 * @property {(floorName: string) => void} handleFloorChange
 * @property {string} purpose
 * @property {(purposeText: string) => void} handlePurposeChange
 * @property {Array} location
 * @property {string} selectedLocation
 * @property {(updatedLocation: string) => void} handleLocationChange
 * @property {boolean} isLocationsLoading
 * @property {boolean} isLocationsError
 */

/**
 * @typedef {Object} State
 * @property {boolean} isDatePickerVisible
 * @property {DatePickerType} datePickerType
 * @property {Type} type
 */

/**
 * @augments {Component<Props, State>}
 */
export default class EditMeetingDetail extends PureComponent {
	static defaultProps = {
		date: new Date(),
		handleDateChange: () => null,
		startTime: new Date().getTime(),
		handleStartTimeChange: () => null,
		endTime: new Date().getTime(),
		handleEndTimeChange: () => null,
		campus: "",
		handleCampusChange: () => null,
		floor: "",
		handleFloorChange: () => null,
		purpose: "",
		handlePurposeChange: () => null,
		locations: [],
		selectedLocation: "Select",
		handleLocationChange: () => null,
		isLocationsLoading: true,
		isLocationsError: false
	};

	/**
	 * @type {State}
	 */
	state = {
		isDatePickerVisible: false,
		datePickerType: null,
		// Selected Picker
		type: null,
		pickerDate: null,
		errorMessage: null
	};

	static getDerivedStateFromProps(nextProps, prevState) {}

	/**
	 * @param {DatePickerType} datePickerType
	 * @param {Type} type
	 * @param {Date} pickerDate
	 */
	showDatePicker = (datePickerType, type, pickerDate) => {
		if (!this.state.isDatePickerVisible) {
			this.setState({
				isDatePickerVisible: true,
				datePickerType,
				type,
				pickerDate,
				errorMessage: null
			});
		} else {
			this.hideDatePicker();
		}
	};

	hideDatePicker = errorMessage => {
		this.setState(
			{
				isDatePickerVisible: false,
				datePickerType: null,
				type: null,
				pickerDate: null,
				errorMessage
			},
			() => {
				if (this.state.errorMessage) {
					setTimeout(() => {
						GoToast.show(errorMessage, "Error", 2000);
					}, 400);
				}
			}
		);
	};

	handleConfirm = date => {
		let errorMessage = null;
		this.setState({ isDatePickerVisible: false });
		if (this.state.type === "date") {
			this.props.handleDateChange(Date.parse(date));
		}
		if (this.state.type === "startTime") {
			const currentDate = new Date();
			if (
				new Date(this.props.date).toDateString() == currentDate.toDateString()
			) {
				if (
					date.toTimeString() == currentDate.toTimeString() ||
					date.toTimeString() <= currentDate.toTimeString()
				) {
					errorMessage = "Start Time cannot be less than current time";
				} else {
					this.props.handleStartTimeChange(Date.parse(date));
				}
			} else {
				this.props.handleStartTimeChange(Date.parse(date));
			}
		}
		if (this.state.type === "endTime") {
			let meetingEndTime =
				new Date(this.props.endTime).toDateString() + " " + date.toTimeString();
			if (this.props.startTime > Date.parse(meetingEndTime)) {
				errorMessage = "End Time cannot be less than start time";
			} else if (
				Date.parse(meetingEndTime) - this.props.startTime <
				10 * 60 * 1000
			) {
				errorMessage = "Meeting should be atleast 10 minutes";
			} else {
				this.props.handleEndTimeChange(Date.parse(date));
			}
		}
		this.hideDatePicker(errorMessage);
	};

	render() {
		const { locations, selectedLocation } = this.props;
		let newLocation = [];
		locations.map(building => newLocation.push({ name: building.address }));
		return (
			<View>
				<GoappTextRegular color={"#5c6170"} style={styles.labelText}>
					Date
				</GoappTextRegular>
				<TouchableOpacity
					onPress={() =>
						this.showDatePicker("date", "date", new Date(this.props.startTime))
					}
					style={styles.button}>
					<GoappTextRegular style={styles.input}>
						{new Date(this.props.startTime).toDateString()}
					</GoappTextRegular>
					<Icon
						iconType={"font_awesome"}
						iconName={"caret-down"}
						iconColor={"#2c98f0"}
					/>
				</TouchableOpacity>
				<View style={styles.pairContainer}>
					<View style={[styles.flex, styles.pairItemLeftContainer]}>
						<GoappTextRegular style={styles.labelText} color={"#5c6170"}>
							Start Time
						</GoappTextRegular>
						<TouchableOpacity
							style={styles.button}
							onPress={() => {
								const { startTime } = this.props;
								this.showDatePicker("time", "startTime", new Date(startTime));
							}}>
							<GoappTextRegular>
								{_24to12(this.props.startTime)}
							</GoappTextRegular>
							<Icon
								iconType={"font_awesome"}
								iconName={"caret-down"}
								iconColor={"#2c98f0"}
							/>
						</TouchableOpacity>
					</View>
					<View style={[styles.flex, styles.pairItemRightContainer]}>
						<GoappTextRegular style={styles.labelText} color={"#5c6170"}>
							End Time
						</GoappTextRegular>
						<TouchableOpacity
							style={styles.button}
							onPress={() => {
								const { endTime } = this.props;
								this.showDatePicker("time", "endTime", new Date(endTime));
							}}>
							<GoappTextRegular>{_24to12(this.props.endTime)}</GoappTextRegular>
							<Icon
								iconType={"font_awesome"}
								iconName={"caret-down"}
								iconColor={"#2c98f0"}
							/>
						</TouchableOpacity>
					</View>
				</View>
				<View>
					<GoappTextRegular color={"#5c6170"} style={styles.labelText}>
						Location of Meeting
						<GoappTextRegular color={"#ff0000"}>*</GoappTextRegular>
					</GoappTextRegular>
					{this.props.isLocationsLoading ? (
						<View style={styles.locationLoading}>
							<GoappTextRegular>Loading Locations</GoappTextRegular>
						</View>
					) : (
						<View style={styles.picker}>
							<Dropdown
								data={newLocation}
								onPress={this.props.handleLocationChange}
								selectedValue={selectedLocation}
							/>
						</View>
					)}
				</View>
				<View>
					<GoappTextRegular color={"#5c6170"} style={styles.labelText}>
						Purpose of Visit
						<GoappTextRegular color={"#ff0000"}>*</GoappTextRegular>
					</GoappTextRegular>
					<GoappTextInputRegular
						style={styles.purposeInput}
						value={this.props.purpose}
						onChangeText={text => this.props.handlePurposeChange(text)}
						placeholder={"Enter details of the visit"}
						returnKeyType={"done"}
					/>
				</View>
				<GoDateTimePicker
					isVisible={this.state.isDatePickerVisible}
					mode={this.state.datePickerType}
					onConfirm={this.handleConfirm}
					onCancel={this.hideDatePicker}
					date={this.state.pickerDate}
					maximumDate={maximumDate}
					minimumDate={new Date()}
					headerTextIOS={
						this.state.datePickerType === "date" ? "Pick a date" : "Pick time"
					}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	flex: {
		flex: 1
	},
	labelText: {
		paddingHorizontal: width * 0.06
	},
	button: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		backgroundColor: "#f5f6f9",
		paddingHorizontal: width * 0.06,
		paddingVertical: height * 0.02,
		borderRadius: width * 0.5,
		marginVertical: height * 0.01
	},
	pairContainer: {
		marginVertical: height * 0.01,
		flexDirection: "row",
		justifyContent: "space-between"
	},
	pairItemLeftContainer: {
		marginRight: width * 0.03
	},
	pairItemRightContainer: {
		marginLeft: width * 0.03
	},
	purposeInput: {
		backgroundColor: "#f5f6f9",
		paddingHorizontal: width * 0.06,
		paddingVertical: height * 0.02,
		borderRadius: width * 0.5,
		marginVertical: height * 0.01
	},
	locationPicker: {
		justifyContent: "space-between",
		backgroundColor: "#f5f6f9",
		marginVertical: height * 0.01,
		paddingVertical: height * 0.015,
		paddingHorizontal: width * 0.06,
		borderRadius: width * 0.5
	},
	locationLoading: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		marginVertical: height * 0.01,
		paddingVertical: height * 0.015
	},
	picker: {
		paddingLeft: width * 0.04,
		backgroundColor: "#f5f6f9",
		borderRadius: width * 0.5,
		marginVertical: height * 0.01,
		paddingVertical: height / 50
	},
	input: {
		fontSize: height / 50
	}
});
