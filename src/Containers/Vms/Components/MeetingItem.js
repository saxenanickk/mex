import React, { Component } from "react";
import { View, StyleSheet, Dimensions, TouchableOpacity } from "react-native";
import FastImage from "react-native-fast-image";
import { truncate } from "lodash";
import { GoappTextBold } from "../../../Components/GoappText";
import { EMPTYIMAGE } from "../../../../src/Assets/Img/Image";
import { _24to12 } from "../../../utils/dateUtils";
import I18n from "../../../Assets/strings/i18n";

const { width, height } = Dimensions.get("window");

/**
 *
 * @param {Object} param
 * @param {String} param.purpose
 * @param {String} param.date
 * @param {String} param.location
 * @param {Array} param.attendees
 * @param {boolean} param.disabled
 */
const MeetingItem = ({
	purpose,
	startTime,
	endTime,
	location,
	attendees = [],
	navigate,
	disabled
}) => (
	<TouchableOpacity
		activeOpacity={disabled ? 1 : 0.6}
		onPress={disabled ? null : navigate}
		style={styles.container}>
		<GoappTextBold color={"grey"}>Purpose</GoappTextBold>
		<GoappTextBold>{purpose}</GoappTextBold>
		<GoappTextBold color={"grey"} style={{ marginTop: height / 50 }}>
			When
		</GoappTextBold>
		<GoappTextBold>
			{I18n.strftime(new Date(startTime), "%d %b %Y")} {_24to12(startTime)} -{" "}
			{_24to12(endTime)}
		</GoappTextBold>
		<GoappTextBold color={"grey"} style={{ marginTop: height / 50 }}>
			Where
		</GoappTextBold>
		<GoappTextBold>{location}</GoappTextBold>
		<GoappTextBold color={"grey"} style={{ marginTop: height / 50 }}>
			Who
		</GoappTextBold>
		<View style={{ flexDirection: "row", alignItems: "center" }}>
			{attendees.length > 0
				? attendees.map((attendee, index) =>
						index < 2 ? (
							<View style={styles.attendeeContainer} key={attendee.id}>
								{attendee.image === null ? (
									<FastImage
										source={EMPTYIMAGE}
										style={{ width: 30, height: 30, borderRadius: 70 }}
									/>
								) : (
									<FastImage
										source={{ uri: attendee.image }}
										style={{ width: 30, height: 30, borderRadius: 70 }}
									/>
								)}
								<GoappTextBold numberOfLines={1}>
									{truncate(attendee.name, { length: 15, separator: " " })}
								</GoappTextBold>
							</View>
						) : null
				  )
				: null}
			{attendees.length > 2 ? (
				<GoappTextBold>
					{`+${attendees.length - 2} Visitor${
						attendees.length - 2 > 1 ? "s" : ""
					}`}{" "}
				</GoappTextBold>
			) : null}
		</View>
	</TouchableOpacity>
);

const styles = StyleSheet.create({
	container: {
		marginHorizontal: width * 0.05,
		marginTop: height * 0.03,
		padding: width * 0.03,
		borderWidth: 1,
		borderColor: "#eaeaea",
		borderRadius: 20,
		shadowOpacity: 0.1,
		shadowRadius: 3,
		shadowOffset: {
			height: 2,
			width: 2
		}
	},
	attendeeContainer: {
		alignItems: "center",
		justifyContent: "center",
		padding: width * 0.02
	}
});

export default MeetingItem;
