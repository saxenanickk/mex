import React, { Component } from "react";
import { StyleSheet, View, Dimensions } from "react-native";
import {
	GoappTextBold,
	GoappTextInputBold,
	GoappTextInputRegular,
	GoappTextRegular
} from "../../../Components/GoappText";

const { width, height } = Dimensions.get("window");

/**
 * @typedef {Object} Props
 * @property {typeof import ("../Containers/CreateMeeting/index").visitor} visitor
 * @property {(text: string, type: ("name" | "mobile" | "email", "countryCode")) => void} onVisitorChange
 */

/**
 * @typedef {Object} State
 */

/**
 * @augments {Component<Props, State>}
 */
export default class EditVisitor extends Component {
	static defaultProps = {
		visitor: {
			name: "",
			email: "",
			mobile: "",
			countryCode: "+91"
		}
	};

	render() {
		return (
			<View>
				<View>
					<GoappTextRegular color={"#5c6170"} style={styles.labelText}>
						Name
						<GoappTextRegular color={"#ff0000"}>*</GoappTextRegular>
					</GoappTextRegular>
					<GoappTextInputRegular
						style={styles.input}
						placeholder={"Enter Name"}
						defaultValue={this.props.visitor.name.value.trim()}
						onChangeText={text => this.props.onVisitorChange(text, "name")}
						returnKeyType={"done"}
					/>
				</View>
				<View>
					<GoappTextRegular color={"#5c6170"} style={styles.labelText}>
						Phone
						<GoappTextRegular color={"#ff0000"}>*</GoappTextRegular>
					</GoappTextRegular>
					<View style={styles.phoneInputContainer}>
						<GoappTextInputRegular
							placeholder="XXX"
							maxLength={3}
							keyboardType="numeric"
							style={styles.countryCodeInput}
							defaultValue={this.props.visitor.countryCode.value.trim()}
							onChangeText={text =>
								this.props.onVisitorChange(text, "countryCode")
							}
							returnKeyType={"done"}
						/>
						<GoappTextRegular color={"rgba(0,0,0,0.5)"}>{"-"}</GoappTextRegular>
						<GoappTextInputRegular
							maxLength={10}
							placeholder="XXXXXXXXXX"
							keyboardType="numeric"
							style={styles.mobileInput}
							defaultValue={this.props.visitor.mobile.value.trim()}
							onChangeText={text => this.props.onVisitorChange(text, "mobile")}
							returnKeyType={"done"}
						/>
					</View>
				</View>
				<View>
					<GoappTextRegular color={"#5c6170"} style={styles.labelText}>
						Email
					</GoappTextRegular>
					<GoappTextInputRegular
						style={styles.input}
						placeholder={"Enter Email Address"}
						defaultValue={this.props.visitor.email.value.trim()}
						onChangeText={text => this.props.onVisitorChange(text, "email")}
						returnKeyType={"done"}
					/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	input: {
		backgroundColor: "#f5f6f9",
		paddingHorizontal: width * 0.06,
		paddingVertical: height * 0.02,
		borderRadius: width * 0.5,
		marginVertical: height * 0.01,
		fontSize: height / 50
	},
	labelText: {
		paddingHorizontal: width * 0.06
	},
	labelVisitor: {
		paddingHorizontal: width * 0.06,
		paddingVertical: height * 0.01
	},
	phoneInputContainer: {
		flexDirection: "row",
		alignItems: "center",
		backgroundColor: "#EFF1F4",
		borderRadius: width * 0.5,
		paddingHorizontal: width * 0.06,
		paddingVertical: height * 0.02,
		marginVertical: height * 0.01
	},
	countryCodeInput: {
		fontSize: height / 60,
		marginRight: width * 0.01
	},
	mobileInput: {
		fontSize: height / 60,
		marginLeft: width * 0.01,
		width: "100%",
		height: "100%"
	}
});
