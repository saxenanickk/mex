import App from "./App";
import React from "react";
import { AppRegistry } from "react-native";
import { connect } from "react-redux";
import reducer from "./Reducer";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";
import { getNewReducer, removeExistingReducer } from "../../../App";

class Cleartrip extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "cleartrip" });
		getNewReducer({ name: "cleartrip", reducer: reducer });
	}

	componentWillUnmount() {
		removeExistingReducer("cleartrip");
	}

	render() {
		return <App {...this.props} />;
	}
}

export default connect()(Cleartrip);

AppRegistry.registerComponent("Cleartrip", () => Cleartrip);
