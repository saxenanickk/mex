import React from "react";
import { Stack } from "../../utils/Navigators";
import Splash from "./Containers/Splash";
import SelectFlight from "./Containers/SelectFlight";
import SearchFlight from "./Containers/SearchFlight";
import TravellerDetails from "./Containers/TravellerDetails";
import ConfirmFlight from "./Containers/ConfirmFlight";
import TripDetails from "./Containers/TripDetails";
import Filter from "./Containers/SelectFlight/Filter";
import SearchPlace from "./Containers/SearchFlight/SearchPlace";
import Traveller from "./Containers/SearchFlight/Traveller";
import CancelPolicy from "./Containers/SelectFlight/CancelPolicy";
import FillTravellerDetails from "./Containers/TravellerDetails/FillTravellerDetails";

const Navigator = () => (
	<Stack.Navigator headerMode={"none"}>
		<Stack.Screen name={"Splash"} component={Splash} />
		<Stack.Screen name={"SearchFlight"} component={SearchFlight} />
		<Stack.Screen name={"SelectFlight"} component={SelectFlight} />
		<Stack.Screen name={"TravellerDetails"} component={TravellerDetails} />
		<Stack.Screen name={"ConfirmFlight"} component={ConfirmFlight} />
		<Stack.Screen name={"TripDetails"} component={TripDetails} />
		<Stack.Screen name={"Filter"} component={Filter} />
		<Stack.Screen name={"SearchPlace"} component={SearchPlace} />
		<Stack.Screen name={"Traveller"} component={Traveller} />
		<Stack.Screen name={"CancelPolicy"} component={CancelPolicy} />
		<Stack.Screen
			name={"FillTravellerDetails"}
			component={FillTravellerDetails}
		/>
	</Stack.Navigator>
);

export default Navigator;
