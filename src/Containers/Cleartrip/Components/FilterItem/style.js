import { Dimensions, StyleSheet } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	filterItemButton: {
		height: height / 16.5,
		paddingHorizontal: width / 26.3,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between"
	},
	centeredRow: {
		flexDirection: "row",
		alignItems: "center"
	},
	filterIconImage: {
		marginLeft: width / 35,
		width: width / 20,
		height: width / 20
	},
	filterTitleText: {
		marginHorizontal: width / 35,
		color: "#000000",
		fontSize: height / 45.7
	},
	filterDescription: {
		fontSize: height / 50.5
	}
});

export default styles;
