import React from "react";
import { View, Image, Dimensions, TouchableOpacity } from "react-native";
import { Icon } from "../../../../Components";
import { CleartripTextRegular } from "../CleartripText";
import styles from "./style";

const { width, height } = Dimensions.get("window");

const FilterItem = props => {
	return (
		<TouchableOpacity
			style={styles.filterItemButton}
			activeOpacity={1}
			key={props.key && props.key}
			onPress={props.onPress}>
			<View style={styles.centeredRow}>
				{props.value ? (
					<Icon
						iconType={"material"}
						iconColor={"#3366cc"}
						iconName={"check-box"}
						iconSize={height / 35}
					/>
				) : (
					<Icon
						iconType={"material"}
						iconColor={"#1d4098"}
						iconName={"check-box-outline-blank"}
						iconSize={height / 35}
					/>
				)}
				<Image style={styles.filterIconImage} source={props.image} />
				<CleartripTextRegular style={styles.filterTitleText}>
					{props.title}
				</CleartripTextRegular>
			</View>
			{props.rightDescRequired && (
				<CleartripTextRegular style={styles.filterDescription}>
					{props.desc}
				</CleartripTextRegular>
			)}
		</TouchableOpacity>
	);
};

export default FilterItem;
