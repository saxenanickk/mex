import { Dimensions, StyleSheet } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: {
		width: width,
		height: height,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "rgba(0,0,0,0.5)"
	},
	centerContainer: {
		width: width / 1.3,
		height: height / 2.5,
		backgroundColor: "#fff",
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 3
	},
	noNetworkMessage: {
		fontSize: height / 35,
		color: "#000",
		marginTop: height / 30
	},
	reconnectMessage: {
		fontSize: height / 50,
		color: "rgba(0,0,0,0.6)",
		flexWrap: "wrap",
		marginTop: height / 100,
		textAlign: "center",
		width: width / 1.7
	},
	retryMessage: {
		fontSize: height / 45,
		color: "#3366cc",
		marginTop: height / 15
	},
	noNetworkImage: {
		width: width / 6,
		height: width / 6
	}
});

export default styles;
