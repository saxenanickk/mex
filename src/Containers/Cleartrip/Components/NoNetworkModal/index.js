import React from "react";
import { Modal, TouchableOpacity, Image } from "react-native";
import { CleartripTextRegular, CleartripTextMedium } from "../CleartripText";
import { CLEARTRIPNONETWORK } from "../../Assets/Img/Image";
import styles from "./style";
import I18n from "../../Assets/Strings/i18n";

export const NoNetworkModal = props => {
	return (
		<Modal
			transparent={true}
			visible={props.visible}
			hardwareAccelerated={true}
			onRequestClose={() => props.close()}>
			<TouchableOpacity style={styles.container} onPress={() => props.close()}>
				<TouchableOpacity style={styles.centerContainer} activeOpacity={1}>
					<Image
						source={CLEARTRIPNONETWORK}
						resizeMode={"contain"}
						style={styles.noNetworkImage}
					/>
					<CleartripTextMedium style={styles.noNetworkMessage}>
						{I18n.t("no_net_no_candy")}
					</CleartripTextMedium>
					<CleartripTextRegular style={styles.reconnectMessage}>
						{I18n.t("try_connecting_network")}
					</CleartripTextRegular>
					<TouchableOpacity onPress={() => props.onRetryPress()}>
						<CleartripTextMedium style={styles.retryMessage}>
							{props.retryMessage}
						</CleartripTextMedium>
					</TouchableOpacity>
				</TouchableOpacity>
			</TouchableOpacity>
		</Modal>
	);
};

NoNetworkModal.defaultProps = {
	close: function() {
		console.log("pass close modal method from the props");
	},
	retryMessage: "RETRY"
};
