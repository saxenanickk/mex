import React from "react";
import { Text } from "react-native";
import {
	font_one,
	font_two,
	font_three,
	font_four
} from "../../../Assets/styles";

export const CleartripTextLight = props => (
	<Text
		style={[{ fontFamily: font_one }, props.style]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

export const CleartripTextRegular = props => (
	<Text
		style={[{ fontFamily: font_two }, props.style]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

export const CleartripTextMedium = props => (
	<Text
		style={[{ fontFamily: font_three }, props.style]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

export const CleartripTextBold = props => (
	<Text
		style={[{ fontFamily: font_four }, props.style]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);
