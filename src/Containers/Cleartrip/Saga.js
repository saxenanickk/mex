import { all, takeLatest, put } from "redux-saga/effects";
import { searchFlightSaga } from "./Containers/SearchFlight/Saga";
import { confirmFlightSaga } from "./Containers/ConfirmFlight/Saga";
import { TravellerDetailsSaga } from "./Containers/TravellerDetails/Saga";
import { SelectFlightSaga } from "./Containers/SelectFlight/Saga";
import { tripDetailsSaga } from "./Containers/TripDetails/Saga";

export const CLEARTRIP_NETWORK_ERROR = "CLEARTRIP_NETWORK_ERROR";
export const SAVE_CLEARTRIP_DEEPLINK_STATE = "SAVE_CLEARTRIP_DEEPLINK_STATE";
export const CLEARTRIP_HANDLE_RETRY = "CLEARTRIP_HANDLE_RETRY";

export const cleartripNetworkError = payload => ({
	type: CLEARTRIP_NETWORK_ERROR,
	payload
});

export const saveCleartripDeeplinkState = payload => ({
	type: SAVE_CLEARTRIP_DEEPLINK_STATE,
	payload
});

export const cleartripHandleRetry = payload => ({
	type: CLEARTRIP_HANDLE_RETRY,
	payload
});

function* cleartripRetrySaga(dispatch) {
	yield takeLatest(CLEARTRIP_HANDLE_RETRY, handleRetry);
}

function* handleRetry(action, payload) {
	if (action.payload) {
		/**
		 * for retry search while network request fails
		 */
		yield put(cleartripNetworkError(null));
		yield put({
			type: action.payload.actionType,
			payload: action.payload.payload
		});
	} else {
		/**
		 * to close the no network screen
		 */
		yield put(cleartripNetworkError(null));
	}
}

export default function* clearTripSaga(dispatch) {
	yield all([
		cleartripRetrySaga(dispatch),
		searchFlightSaga(dispatch),
		confirmFlightSaga(dispatch),
		TravellerDetailsSaga(dispatch),
		SelectFlightSaga(dispatch),
		tripDetailsSaga(dispatch)
	]);
}
