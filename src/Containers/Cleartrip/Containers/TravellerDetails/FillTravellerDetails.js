import React from "react";
import {
	View,
	ScrollView,
	StatusBar,
	TouchableOpacity,
	TextInput,
	Dimensions,
	BackHandler
} from "react-native";
import { Icon, GoToast, DatePicker } from "../../../../Components";
import {
	CleartripTextMedium,
	CleartripTextBold,
	CleartripTextRegular
} from "../../Components/CleartripText";
import I18n from "../../Assets/Strings/i18n";
import styles from "./style";

class FillTravellerDetailsScreen extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			type: props.type ? props.type : "adult",
			firstname: "",
			lastname: "",
			gender: "",
			dob: "",
			passport_number: "",
			passport_exp_date: "",
			passport_issuing_country: "",
			passport_issue_date: "",
			showDatePicker: false
		};
		this.passportDateType = null;
		BackHandler.addEventListener("hardwareBackPress", this.handleBackClick);
	}

	handleBackClick = () => {
		this.props.navigation.goBack();
		return true;
	};

	componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.handleBackClick);
	}

	passportDatePicker = type => {
		this.passportDateType = type;
		this.setState({
			date: new Date(),
			minDate: type === "expiry" ? new Date() : new Date("01/01/1920"),
			maxDate: type === "expiry" ? new Date("12/31/2099") : new Date(),
			showDatePicker: true
		});
	};

	openDatePicker = () => {
		let minDate = new Date("01/01/1900");
		let maxDate = new Date();
		let tempArray = this.props.selectedFlights.data[0].segment[0].departureDate.split(
			"/"
		);
		tempArray = [tempArray[1], tempArray[0], tempArray[2]];
		let now = new Date(tempArray.join("/"));
		console.log(now);
		// Change now to departure date
		var now_year = now.getFullYear();
		var now_month = now.getMonth();
		var now_day = now.getDate();
		var infant_min = new Date(now_year - 2, now_month, now_day);
		infant_min.setDate(infant_min.getDate() + 1);

		console.log(now_year, now_month, now_day);

		var child_max = new Date(now_year - 2, now_month, now_day);
		var child_min = new Date(now_year - 12, now_month, now_day);
		child_min.setDate(child_min.getDate() + 1);

		if (this.state.type === "child") {
			minDate = child_min;
			maxDate = child_max;
		} else if (this.state.type === "infant") {
			minDate = infant_min;
			maxDate = now;
		}

		console.log(minDate, maxDate);

		this.setState({
			date: new Date(),
			maxDate: maxDate,
			minDate: minDate,
			showDatePicker: true
		});
	};

	isAirAsiaFlight = () => {
		let data = this.props.selectedFlights.data;
		for (let index in data) {
			for (let index_segment in data[index].segment) {
				if (data[index].segment[index_segment].airlineName === "I5") {
					return true;
				}
			}
		}
		return false;
	};

	isPassportIssueDateRequired = () => {
		let flightArray = ["XY", "SV", "MH", "AI", "U2"];
		let data = this.props.selectedFlights.data;
		for (let index in data) {
			for (let index_segment in data[index].segment) {
				if (
					flightArray.indexOf(data[index].segment[index_segment].airlineName) >
					0
				) {
					return true;
				}
			}
		}
		return false;
	};

	validateModal = () => {
		if (!this.state.gender) {
			return "Gender is required.";
		}
		if (!this.state.firstname) {
			return "First Name is required.";
		}
		if (!this.state.lastname) {
			return "Last Name is required.";
		}
		if (this.state.type !== "adult" && !this.state.dob) {
			return "Date Of Birth is required.";
		}
		if (this.isAirAsiaFlight() || this.props.selectedFlights.isInternational) {
			if (!this.state.dob) {
				return "Date of birth Required";
			}
			if (this.props.selectedFlights.isInternational) {
				if (!this.state.passport_issuing_country) {
					return "Passport Issuing Country required.";
				}
				if (this.state.passport_issuing_country.length > 2) {
					return "Passport Issuing Country code must be of 2 characters";
				}
				if (!this.state.passport_number) {
					return "Passport number required.";
				}
				if (!this.state.passport_exp_date) {
					return "Passport expiry date is required.";
				}
				if (this.isPassportIssueDateRequired()) {
					if (!this.state.passport_issue_date) {
						return "Passport Issue date is required.";
					}
				}
			}
		}
	};

	renderForm = () => {
		return (
			<View style={{ flex: 1 }}>
				{this.state.type === "adult" ? (
					<View>
						<View style={styles.renderFormBar}>
							<TouchableOpacity
								activeOpacity={1}
								style={styles.genderButton}
								onPress={() => this.setState({ gender: "Mr" })}>
								<CleartripTextRegular
									style={
										this.state.gender === "Mr"
											? styles.genderTextOne
											: styles.genderTextTwo
									}>
									{I18n.t("mr")}
								</CleartripTextRegular>
							</TouchableOpacity>
							<TouchableOpacity
								activeOpacity={1}
								style={styles.genderButton}
								onPress={() => this.setState({ gender: "Ms" })}>
								<CleartripTextRegular
									style={
										this.state.gender === "Ms"
											? styles.genderTextOne
											: styles.genderTextTwo
									}>
									{I18n.t("ms")}
								</CleartripTextRegular>
							</TouchableOpacity>
							<TouchableOpacity
								activeOpacity={1}
								style={styles.lastGenderButton}
								onPress={() => this.setState({ gender: "Mrs" })}>
								<CleartripTextRegular
									style={
										this.state.gender === "Mrs"
											? styles.genderTextOne
											: styles.genderTextTwo
									}>
									{I18n.t("mrs")}
								</CleartripTextRegular>
							</TouchableOpacity>
						</View>
						<View style={styles.phonePadView}>
							<TextInput
								keyboardType={"name-phone-pad"}
								style={styles.textInput}
								underlineColorAndroid={"transparent"}
								onChangeText={text => this.setState({ firstname: text })}
								placeholder={I18n.t("firstname")}
								placeholderTextColor={"#cccccc"}
							/>
						</View>
						<View style={styles.phonePadView}>
							<TextInput
								keyboardType={"name-phone-pad"}
								style={styles.textInput}
								underlineColorAndroid={"transparent"}
								onChangeText={text => this.setState({ lastname: text })}
								placeholder={I18n.t("lastname")}
								placeholderTextColor={"#cccccc"}
							/>
						</View>
					</View>
				) : (
					<View>
						<View style={styles.childGenderView}>
							<TouchableOpacity
								activeOpacity={1}
								style={styles.genderButton}
								onPress={() => this.setState({ gender: "Mstr" })}>
								<CleartripTextRegular
									style={
										this.state.gender === "Mstr"
											? styles.genderTextOne
											: styles.genderTextTwo
									}>
									{I18n.t("mstr")}
								</CleartripTextRegular>
							</TouchableOpacity>
							<TouchableOpacity
								activeOpacity={1}
								style={styles.lastGenderButton}
								onPress={() => this.setState({ gender: "Miss" })}>
								<CleartripTextRegular
									style={
										this.state.gender === "Miss"
											? styles.genderTextOne
											: styles.genderTextTwo
									}>
									{I18n.t("miss")}
								</CleartripTextRegular>
							</TouchableOpacity>
						</View>
						<View style={styles.phonePadView}>
							<TextInput
								keyboardType={"name-phone-pad"}
								style={styles.textInput}
								underlineColorAndroid={"transparent"}
								onChangeText={text => this.setState({ firstname: text })}
								placeholder={I18n.t("firstname")}
								placeholderTextColor={"#cccccc"}
							/>
						</View>
						<View style={styles.phonePadView}>
							<TextInput
								keyboardType={"name-phone-pad"}
								style={styles.textInput}
								underlineColorAndroid={"transparent"}
								onChangeText={text => this.setState({ lastname: text })}
								placeholder={I18n.t("lastname")}
								placeholderTextColor={"#cccccc"}
							/>
						</View>
					</View>
				)}
				{this.state.type !== "adult" ||
				this.props.selectedFlights.isInternational ||
				this.isAirAsiaFlight() ? (
					<View>
						<TouchableOpacity
							activeOpacity={1}
							style={styles.datePickerButton}
							onPress={() => this.openDatePicker()}>
							<CleartripTextRegular
								style={
									this.state.dob ? styles.dateTextOne : styles.dateTextTwo
								}>
								{this.state.dob
									? this.state.dob.dd +
									  "/" +
									  this.state.dob.mm +
									  "/" +
									  this.state.dob.yyyy
									: I18n.t("date_of_birth")}
							</CleartripTextRegular>
						</TouchableOpacity>
						{this.props.selectedFlights.isInternational && (
							<View>
								<View style={styles.additionalInfoView}>
									<CleartripTextRegular style={styles.dateTextOne}>
										{I18n.t("additional_information")}
									</CleartripTextRegular>
								</View>
								<View style={styles.phonePadView}>
									<TextInput
										value={this.state.passport_number}
										keyboardType={"name-phone-pad"}
										style={styles.textInput}
										underlineColorAndroid={"transparent"}
										onChangeText={text =>
											this.setState({ passport_number: text })
										}
										placeholder={I18n.t("passport_number")}
										placeholderTextColor={"#cccccc"}
									/>
								</View>
								<TouchableOpacity
									activeOpacity={1}
									style={styles.datePickerButton}
									onPress={() => this.passportDatePicker("expiry")}>
									<CleartripTextRegular
										style={
											this.state.passport_exp_date
												? styles.dateTextOne
												: styles.dateTextTwo
										}>
										{this.state.passport_exp_date
											? this.state.passport_exp_date
											: I18n.t("passport_expiry_date")}
									</CleartripTextRegular>
								</TouchableOpacity>
								{this.isPassportIssueDateRequired() && (
									<TouchableOpacity
										activeOpacity={1}
										style={styles.datePickerButton}
										onPress={() => this.passportDatePicker("issue")}>
										<CleartripTextRegular
											style={
												this.state.passport_issue_date
													? styles.dateTextOne
													: styles.dateTextTwo
											}>
											{this.state.passport_issue_date
												? this.state.passport_issue_date
												: I18n.t("passport_issue_date")}
										</CleartripTextRegular>
									</TouchableOpacity>
								)}
								<View style={styles.phonePadView}>
									<TextInput
										value={this.state.passport_issuing_country}
										keyboardType={"name-phone-pad"}
										style={styles.textInput}
										underlineColorAndroid={"transparent"}
										onChangeText={text =>
											this.setState({ passport_issuing_country: text })
										}
										placeholder={I18n.t("passport_country")}
										placeholderTextColor={"#cccccc"}
									/>
								</View>
							</View>
						)}
					</View>
				) : null}
			</View>
		);
	};

	render() {
		console.log(this.props);
		return (
			<View style={styles.fullFlex}>
				<StatusBar backgroundColor="#535353" barStyle="light-content" />
				<View style={styles.formModalView}>
					<View style={styles.centeredRow}>
						<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
							<View style={styles.goBackButton}>
								<Icon
									iconType={"material"}
									iconSize={Dimensions.get("window").height / 24}
									iconName={"arrow-back"}
									iconColor={"#ffffff"}
								/>
							</View>
						</TouchableOpacity>
						<CleartripTextMedium style={styles.fillDetailText}>
							{I18n.t("fill_details")}
						</CleartripTextMedium>
					</View>
				</View>
				<ScrollView>
					{this.renderForm()}
					<TouchableOpacity
						activeOpacity={1}
						onPress={() => {
							var validationErrorMsg = this.validateModal();
							if (validationErrorMsg != null) {
								GoToast.show(validationErrorMsg, I18n.t("error"));
							} else {
								this.props.saveDetails(this.state);
								this.props.navigation.goBack();
							}
						}}
						style={styles.doneButton}>
						<CleartripTextBold style={styles.doneText}>
							{I18n.t("cleartrip_done")}
						</CleartripTextBold>
					</TouchableOpacity>
				</ScrollView>
				{this.state.showDatePicker ? (
					<DatePicker
						date={this.state.date}
						minimumDate={this.state.minDate}
						maximumDate={this.state.maxDate}
						onDateChange={date => {
							if (date !== null) {
								let dob_picker = {
									dd: date.day,
									mm: date.month + 1,
									yyyy: date.year
								};
								if ((dob_picker.mm + "").length === 1) {
									dob_picker.mm = "0" + dob_picker.mm;
								}
								if ((dob_picker.dd + "").length === 1) {
									dob_picker.dd = "0" + dob_picker.dd;
								}
								if (this.passportDateType !== null) {
									if (this.passportDateType === "expiry") {
										this.passportDateType = null;
										this.setState({
											showDatePicker: false,
											passport_exp_date:
												dob_picker.yyyy +
												"-" +
												dob_picker.mm +
												"-" +
												dob_picker.dd
										});
									} else {
										this.passportDateType = null;
										this.setState({
											showDatePicker: false,
											passport_issue_date:
												dob_picker.yyyy +
												"-" +
												dob_picker.mm +
												"-" +
												dob_picker.dd
										});
									}
								} else {
									this.setState({
										dob: dob_picker,
										showDatePicker: false
									});
								}
							} else {
								this.passportDateType = null;
								this.setState({
									showDatePicker: false
								});
							}
						}}
					/>
				) : null}
			</View>
		);
	}
}

const FillTravellerDetails = props => (
	<FillTravellerDetailsScreen {...props} {...props.route.params} />
);

export default FillTravellerDetails;
