import { StyleSheet, Dimensions } from "react-native";
import { font_two } from "../../../../Assets/styles";

const { height, width } = Dimensions.get("window");

export default StyleSheet.create({
	centeredRow: {
		flexDirection: "row",
		alignItems: "center"
	},
	goBackButton: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	fullFlex: { flex: 1, backgroundColor: "#fff" },
	flexStartRow: {
		flexDirection: "row",
		alignItems: "flex-start"
	},
	spaceBetweenRow: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	justifyCenter: {
		justifyContent: "center"
	},
	justifySpaceBetween: {
		justifyContent: "space-between"
	},
	justifyFlexEnd: {
		justifyContent: "flex-end"
	},
	justifySpaceAround: {
		justifyContent: "space-around"
	},
	textInput: {
		// borderWidth: 1,
		// marginLeft: -width / 100,
		fontFamily: font_two,
		fontSize: height / 41.7,
		backgroundColor: "#ffffff",
		color: "#000000",
		paddingBottom: 0,
		paddingLeft: 0
	},
	headerBar: {
		width: width,
		elevation: 5,
		backgroundColor: "#3366cc",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		justifyContent: "space-between",
		paddingRight: width / 26.3
	},
	fullWidth: { width: width },
	travellerText: {
		fontSize: width / 25
	},
	renderFormBar: {
		height: height / 15.6,
		width: width / 2,
		marginTop: height / 33.1,
		flexDirection: "row"
	},
	genderButton: {
		height: height / 27.42,
		marginTop: height / 35.5,
		paddingLeft: width / 26.3,
		paddingRight: width / 33.75,
		justifyContent: "flex-start",
		borderRightWidth: width / 500,
		borderRightColor: "#ececec"
	},
	genderTextOne: {
		color: "#000",
		fontSize: height / 50.5
	},
	genderTextTwo: {
		color: "#cccccc",
		fontSize: height / 50.5
	},
	lastGenderButton: {
		height: height / 27.42,
		marginTop: height / 35.5,
		paddingLeft: width / 26.3,
		paddingRight: width / 33.75,
		justifyContent: "flex-start"
	},
	phonePadView: {
		marginTop: height / 100,
		justifyContent: "flex-end",
		marginHorizontal: width / 26.3,
		height: height / 14,
		borderBottomWidth: height / 960,
		borderBottomColor: "#ececec"
	},
	childGenderView: {
		height: height / 15.6,
		width: width / 2,
		marginTop: height / 33.1,
		flexDirection: "row"
	},
	datePickerButton: {
		marginTop: height / 50,
		marginHorizontal: width / 26.3,
		height: height / 15.6,
		justifyContent: "flex-end",
		backgroundColor: "#ffffff",
		borderBottomWidth: height / 960,
		borderBottomColor: "#ececec",
		padding: 0
	},
	dateTextOne: {
		fontSize: height / 41.6,
		color: "#000"
	},
	dateTextTwo: {
		fontSize: height / 41.6,
		color: "#cccccc"
	},
	additionalInfoView: {
		marginTop: height / 100,
		justifyContent: "flex-end",
		marginHorizontal: width / 26.3,
		height: height / 14
	},
	travellerDetailText: {
		color: "#ffffff",
		fontSize: height / 38.4,
		marginLeft: width / 35,
		marginRight: width / 35
	},
	scrollViewContent: {
		width: width,
		backgroundColor: "#ffffff"
	},
	headingBar: {
		paddingTop: height / 35,
		paddingHorizontal: width / 26.3,
		height: height / 12.5,
		backgroundColor: "#ececec",
		flexDirection: "row",
		justifyContent: "space-between"
	},
	travellerTextHeader: {
		fontSize: width / 30
	},
	contactDetailBar: {
		marginTop: height / 40,
		paddingTop: height / 35,
		paddingHorizontal: width / 26.3,
		height: height / 12.5,
		backgroundColor: "#ececec",
		flexDirection: "row",
		justifyContent: "space-between"
	},
	continueButton: {
		width: width / 1.08,
		margin: width / 26.3,
		elevation: 3,
		borderRadius: height / 192,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#f26822",
		height: height / 15.7
	},
	continueButtonText: {
		color: "#ffffff",
		fontSize: width / 25
	},
	formModalView: {
		width: width,
		elevation: 5,
		backgroundColor: "#777777",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		justifyContent: "space-between",
		paddingRight: width / 26.3
	},
	fillDetailText: {
		color: "#ffffff",
		fontSize: height / 38.4,
		marginLeft: width / 35,
		marginRight: width / 35
	},
	doneButton: {
		marginTop: height / 25,
		height: height / 15.6,
		borderRadius: height / 192,
		marginHorizontal: width / 26.3,
		backgroundColor: "#3366cc",
		alignItems: "center",
		justifyContent: "center"
	},
	doneText: {
		color: "#ffffff",
		fontSize: height / 45
	},
	priceChangedText: {
		position: "absolute",
		width: width / 1.5,
		height: height / 3,
		backgroundColor: "#ffffff"
	}
});
