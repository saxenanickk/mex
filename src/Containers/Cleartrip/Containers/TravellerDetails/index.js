import React, { Component } from "react";
import {
	View,
	TouchableOpacity,
	Dimensions,
	ScrollView,
	TextInput,
	Keyboard
} from "react-native";
import { connect } from "react-redux";
import I18n from "../../Assets/Strings/i18n";
import {
	CleartripTextRegular,
	CleartripTextMedium,
	CleartripTextBold
} from "../../Components/CleartripText";
import Button from "../../../../Components/Button";
import Icon from "../../../../Components/Icon";
import {
	saveAdultDetails,
	saveChildDetails,
	saveInfantDetails,
	resetTravellers
} from "./Saga";
import {
	ProgressScreen,
	DialogModal,
	DatePicker
} from "../../../../Components";
import styles from "./style";
import { GoToast } from "../../../../Components";
const { width, height } = Dimensions.get("window");

class TravellerDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
			index: 0,
			type: "adult",
			showSpinner: false,
			isPriceChanged: false,
			firstname: "",
			lastname: "",
			gender: "",
			mobile: "",
			email: "",
			passport_number: "",
			passport_exp_date: "",
			passport_issuing_country: "",
			passport_issue_date: "",
			showDatePicker: false
		};
		this.passportDateType = null;
		this.renderTravellers = this.renderTravellers.bind(this);
		this.bookFlight = this.bookFlight.bind(this);
		this.saveDetails = this.saveDetails.bind(this);
		this.validateDetails = this.validateDetails.bind(this);
		this.validateEmail = this.validateEmail.bind(this);
		this.isAirAsiaFlight = this.isAirAsiaFlight.bind(this);
	}

	passportDatePicker = type => {
		this.passportDateType = type;
		this.setState({
			date: new Date(),
			minDate: type === "expiry" ? new Date() : new Date("01/01/1920"),
			maxDate: type === "expiry" ? new Date("12/31/2099") : new Date(),
			showDatePicker: true
		});
	};

	openDatePicker = () => {
		let minDate = new Date("01/01/1900");
		let maxDate = new Date();
		let tempArray = this.props.selectedFlights.data[0].segment[0].departureDate.split(
			"/"
		);
		tempArray = [tempArray[1], tempArray[0], tempArray[2]];
		let now = new Date(tempArray.join("/"));
		console.log(now);
		// Change now to departure date
		var now_year = now.getFullYear();
		var now_month = now.getMonth();
		var now_day = now.getDate();
		var infant_min = new Date(now_year - 2, now_month, now_day);
		infant_min.setDate(infant_min.getDate() + 1);

		console.log(now_year, now_month, now_day);

		var child_max = new Date(now_year - 2, now_month, now_day);
		var child_min = new Date(now_year - 12, now_month, now_day);
		child_min.setDate(child_min.getDate() + 1);

		if (this.state.type === "child") {
			minDate = child_min;
			maxDate = child_max;
		} else if (this.state.type === "infant") {
			minDate = infant_min;
			maxDate = now;
		}

		console.log(minDate, maxDate);

		this.setState({
			date: new Date(),
			maxDate: maxDate,
			minDate: minDate,
			showDatePicker: true
		});
	};

	validateDetails(type) {
		// type - true if more than one traveller(Modal), false if only one traveller(SinglePage)
		if (!type) {
			if (!this.state.gender) {
				return "Gender is required.";
			}
			if (!this.state.firstname) {
				return "First Name is required.";
			}
			if (this.state.firstname.length < 2) {
				return "Length of the First Name must be atleast 2.";
			}
			if (!this.state.lastname) {
				return "Last Name is required.";
			}
			if (this.state.lastname.length < 2) {
				return "Length of the Last Name must be atleast 2.";
			}
			if (
				this.isAirAsiaFlight() ||
				this.props.selectedFlights.isInternational
			) {
				if (!this.state.dob) {
					return "Date of birth Required";
				}
				if (this.props.selectedFlights.isInternational) {
					if (!this.state.passport_issuing_country) {
						return "Passport Issuing Country required.";
					}
					if (this.state.passport_issuing_country.length > 2) {
						return "Passport Issuing Country code must be of 2 characters";
					}
					if (!this.state.passport_number) {
						return "Passport number required.";
					}
					if (!this.state.passport_exp_date) {
						return "Passport expiry date is required.";
					}
					if (this.isPassportIssueDateRequired()) {
						if (!this.state.passport_issue_date) {
							return "Passport Issue date is required.";
						}
					}
				}
			}
		}
		if (!this.state.mobile) {
			return "Mobile number is required.";
		}
		if (this.state.mobile.length < 10) {
			return "Mobile number must be atleast 10 digit";
		}
		if (!this.state.email) {
			return "Email is required.";
		}
		if (!this.validateEmail(this.state.email)) {
			return "Email address is not valid.";
		}
		return null;
	}

	validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}

	/**
	 * Handling booking of flight and calls makeCreateItineraryApiCall function if there
	 * are validation errors
	 */
	bookFlight() {
		Keyboard.dismiss();
		var validationErrorMsg;
		if (
			this.props.adults === 1 &&
			this.props.children === 0 &&
			this.props.infants === 0
		) {
			validationErrorMsg = this.validateDetails(false);
		} else {
			validationErrorMsg = this.validateDetails(true);
		}

		if (validationErrorMsg != null) {
			GoToast.show(validationErrorMsg, I18n.t("error"));
		} else {
			if (
				this.props.adults === 1 &&
				this.props.children === 0 &&
				this.props.infants === 0
			) {
				this.props.dispatch(
					saveAdultDetails({
						id: this.state.index,
						gender: this.state.gender,
						firstname: this.state.firstname,
						lastname: this.state.lastname,
						dob: this.state.dob
							? this.state.dob.yyyy +
							  "-" +
							  this.state.dob.mm +
							  "-" +
							  this.state.dob.dd
							: "",
						passport_detail: this.state.passport_number
							? {
									passport_number: this.state.passport_number,
									passport_exp_date: this.state.passport_exp_date,
									passport_issuing_country: this.state.passport_issuing_country,
									passport_issue_date: this.state.passport_issue_date
							  }
							: ""
					})
				);
			}
			this.props.navigation.navigate("ConfirmFlight", {
				email: this.state.email,
				mobile: this.state.mobile
			});
		}
	}

	renderTravellers() {
		let travellers = [];
		for (let i = 1; i <= this.props.adults; i++) {
			travellers.push(
				<Button
					key={"adult" + i}
					onPress={() => {
						this.setState({
							index: i - 1,
							type: "adult"
						});
						this.props.navigation.navigate("FillTravellerDetails", {
							type: "adult",
							selectedFlights: this.props.selectedFlights,
							saveDetails: this.saveDetails
						});
					}}
					style={styles.fullWidth}>
					<CleartripTextRegular style={styles.travellerText}>
						{this.props.adultsDetails[i - 1]
							? this.props.adultsDetails[i - 1].gender +
							  " " +
							  this.props.adultsDetails[i - 1].firstname +
							  " " +
							  this.props.adultsDetails[i - 1].lastname
							: I18n.t("adult_noun", { count: 1 }) + " " + i}
					</CleartripTextRegular>
				</Button>
			);
		}
		if (this.props.children !== 0) {
			for (let i = 1; i <= this.props.children; i++) {
				travellers.push(
					<Button
						key={"child" + i}
						onPress={() => {
							this.setState({
								index: i - 1,
								type: "child"
							});
							this.props.navigation.navigate("FillTravellerDetails", {
								type: "child",
								selectedFlights: this.props.selectedFlights,
								saveDetails: this.saveDetails
							});
						}}
						style={styles.fullWidth}>
						<CleartripTextRegular style={styles.travellerText}>
							{this.props.childrenDetails[i - 1]
								? this.props.childrenDetails[i - 1].gender +
								  " " +
								  this.props.childrenDetails[i - 1].firstname +
								  " " +
								  this.props.childrenDetails[i - 1].lastname
								: I18n.t("child_noun", { count: 1 }) + " " + i}
						</CleartripTextRegular>
					</Button>
				);
			}
		}
		if (this.props.infants !== 0) {
			for (let i = 1; i <= this.props.infants; i++) {
				travellers.push(
					<Button
						key={"infant" + i}
						onPress={() => {
							this.setState({
								index: i - 1,
								type: "infant"
							});
							this.props.navigation.navigate("FillTravellerDetails", {
								type: "infant",
								selectedFlights: this.props.selectedFlights,
								saveDetails: this.saveDetails
							});
						}}
						style={styles.fullWidth}>
						<CleartripTextRegular style={styles.travellerText}>
							{this.props.infantDetails[i - 1]
								? this.props.infantDetails[i - 1].gender +
								  " " +
								  this.props.infantDetails[i - 1].firstname +
								  " " +
								  this.props.infantDetails[i - 1].lastname
								: I18n.t("infants_noun", { count: 1 }) + " " + i}
						</CleartripTextRegular>
					</Button>
				);
			}
		}

		return travellers;
	}

	saveDetails(data) {
		if (data.type === "adult" && data.firstname && data.lastname) {
			this.props.dispatch(
				saveAdultDetails({
					id: this.state.index,
					gender: data.gender,
					firstname: data.firstname,
					lastname: data.lastname,
					dob: data.dob
						? data.dob.yyyy + "-" + data.dob.mm + "-" + data.dob.dd
						: "",
					passport_detail: data.passport_number
						? {
								passport_number: data.passport_number,
								passport_exp_date: data.passport_exp_date,
								passport_issuing_country: data.passport_issuing_country
						  }
						: ""
				})
			);
		} else if (data.type === "child") {
			this.props.dispatch(
				saveChildDetails({
					id: this.state.index,
					gender: data.gender,
					firstname: data.firstname,
					lastname: data.lastname,
					dob: data.dob
						? data.dob.yyyy + "-" + data.dob.mm + "-" + data.dob.dd
						: "",
					passport_detail: data.passport_number
						? {
								passport_number: data.passport_number,
								passport_exp_date: data.passport_exp_date,
								passport_issuing_country: data.passport_issuing_country
						  }
						: ""
				})
			);
		} else if (data.type === "infant") {
			this.props.dispatch(
				saveInfantDetails({
					id: this.state.index,
					gender: data.gender,
					firstname: data.firstname,
					lastname: data.lastname,
					dob: data.dob
						? data.dob.yyyy + "-" + data.dob.mm + "-" + data.dob.dd
						: "",
					passport_detail: data.passport_number
						? {
								passport_number: data.passport_number,
								passport_exp_date: data.passport_exp_date,
								passport_issuing_country: data.passport_issuing_country
						  }
						: ""
				})
			);
		}
		this.setState({ index: undefined });
	}

	isAirAsiaFlight() {
		let data = this.props.selectedFlights.data;
		for (let index in data) {
			for (let index_segment in data[index].segment) {
				if (data[index].segment[index_segment].airlineName === "I5") {
					return true;
				}
			}
		}
		return false;
	}

	isPassportIssueDateRequired() {
		let flightArray = ["XY", "SV", "MH", "AI", "U2"];
		let data = this.props.selectedFlights.data;
		for (let index in data) {
			for (let index_segment in data[index].segment) {
				if (
					flightArray.indexOf(data[index].segment[index_segment].airlineName) >
					0
				) {
					return true;
				}
			}
		}
		return false;
	}

	renderForm = () => {
		return (
			<View>
				{this.state.type === "adult" ? (
					<View>
						<View style={styles.renderFormBar}>
							<TouchableOpacity
								activeOpacity={1}
								style={styles.genderButton}
								onPress={() => this.setState({ gender: "Mr" })}>
								<CleartripTextRegular
									style={
										this.state.gender === "Mr"
											? styles.genderTextOne
											: styles.genderTextTwo
									}>
									{I18n.t("mr")}
								</CleartripTextRegular>
							</TouchableOpacity>
							<TouchableOpacity
								activeOpacity={1}
								style={styles.genderButton}
								onPress={() => this.setState({ gender: "Ms" })}>
								<CleartripTextRegular
									style={
										this.state.gender === "Ms"
											? styles.genderTextOne
											: styles.genderTextTwo
									}>
									{I18n.t("ms")}
								</CleartripTextRegular>
							</TouchableOpacity>
							<TouchableOpacity
								activeOpacity={1}
								style={styles.lastGenderButton}
								onPress={() => this.setState({ gender: "Mrs" })}>
								<CleartripTextRegular
									style={
										this.state.gender === "Mrs"
											? styles.genderTextOne
											: styles.genderTextTwo
									}>
									{I18n.t("mrs")}
								</CleartripTextRegular>
							</TouchableOpacity>
						</View>
						<View style={styles.phonePadView}>
							<TextInput
								keyboardType={"name-phone-pad"}
								style={styles.textInput}
								underlineColorAndroid={"transparent"}
								onChangeText={text => this.setState({ firstname: text })}
								placeholder={I18n.t("firstname")}
								placeholderTextColor={"#cccccc"}
							/>
						</View>
						<View style={styles.phonePadView}>
							<TextInput
								keyboardType={"name-phone-pad"}
								style={styles.textInput}
								underlineColorAndroid={"transparent"}
								onChangeText={text => this.setState({ lastname: text })}
								placeholder={I18n.t("lastname")}
								placeholderTextColor={"#cccccc"}
							/>
						</View>
					</View>
				) : (
					<View>
						<View style={styles.childGenderView}>
							<TouchableOpacity
								activeOpacity={1}
								style={styles.genderButton}
								onPress={() => this.setState({ gender: "Mstr" })}>
								<CleartripTextRegular
									style={
										this.state.gender === "Mstr"
											? styles.genderTextOne
											: styles.genderTextTwo
									}>
									{I18n.t("mstr")}
								</CleartripTextRegular>
							</TouchableOpacity>
							<TouchableOpacity
								activeOpacity={1}
								style={styles.lastGenderButton}
								onPress={() => this.setState({ gender: "Miss" })}>
								<CleartripTextRegular
									style={
										this.state.gender === "Miss"
											? styles.genderTextOne
											: styles.genderTextTwo
									}>
									{I18n.t("miss")}
								</CleartripTextRegular>
							</TouchableOpacity>
						</View>
						<View style={styles.phonePadView}>
							<TextInput
								keyboardType={"name-phone-pad"}
								style={styles.textInput}
								underlineColorAndroid={"transparent"}
								onChangeText={text => this.setState({ firstname: text })}
								placeholder={I18n.t("firstname")}
								placeholderTextColor={"#cccccc"}
							/>
						</View>
						<View style={styles.phonePadView}>
							<TextInput
								keyboardType={"name-phone-pad"}
								style={styles.textInput}
								underlineColorAndroid={"transparent"}
								onChangeText={text => this.setState({ lastname: text })}
								placeholder={I18n.t("lastname")}
								placeholderTextColor={"#cccccc"}
							/>
						</View>
					</View>
				)}
				{this.state.type !== "adult" ||
				this.props.selectedFlights.isInternational ||
				this.isAirAsiaFlight() ? (
					<View>
						<TouchableOpacity
							activeOpacity={1}
							style={styles.datePickerButton}
							onPress={() => this.openDatePicker()}>
							<CleartripTextRegular
								style={
									this.state.dob ? styles.dateTextOne : styles.dateTextTwo
								}>
								{this.state.dob
									? this.state.dob.dd +
									  "/" +
									  this.state.dob.mm +
									  "/" +
									  this.state.dob.yyyy
									: I18n.t("date_of_birth")}
							</CleartripTextRegular>
						</TouchableOpacity>
						{this.props.selectedFlights.isInternational && (
							<View>
								<View style={styles.additionalInfoView}>
									<CleartripTextRegular style={styles.dateTextOne}>
										{I18n.t("additional_information")}
									</CleartripTextRegular>
								</View>
								<View style={styles.phonePadView}>
									<TextInput
										value={this.state.passport_number}
										keyboardType={"name-phone-pad"}
										style={styles.textInput}
										underlineColorAndroid={"transparent"}
										onChangeText={text =>
											this.setState({ passport_number: text })
										}
										placeholder={I18n.t("passport_number")}
										placeholderTextColor={"#cccccc"}
									/>
								</View>
								<TouchableOpacity
									activeOpacity={1}
									style={styles.datePickerButton}
									onPress={() => this.passportDatePicker("expiry")}>
									<CleartripTextRegular
										style={
											this.state.passport_exp_date
												? styles.dateTextOne
												: styles.dateTextTwo
										}>
										{this.state.passport_exp_date
											? this.state.passport_exp_date
											: I18n.t("passport_expiry_date")}
									</CleartripTextRegular>
								</TouchableOpacity>
								{this.isPassportIssueDateRequired() && (
									<TouchableOpacity
										activeOpacity={1}
										style={styles.datePickerButton}
										onPress={() => this.passportDatePicker("issue")}>
										<CleartripTextRegular
											style={
												this.state.passport_issue_date
													? styles.dateTextOne
													: styles.dateTextTwo
											}>
											{this.state.passport_issue_date
												? this.state.passport_issue_date
												: I18n.t("passport_issue_date")}
										</CleartripTextRegular>
									</TouchableOpacity>
								)}
								<View style={styles.phonePadView}>
									<TextInput
										value={this.state.passport_issuing_country}
										keyboardType={"name-phone-pad"}
										style={styles.textInput}
										underlineColorAndroid={"transparent"}
										onChangeText={text =>
											this.setState({ passport_issuing_country: text })
										}
										placeholder={I18n.t("passport_country")}
										placeholderTextColor={"#cccccc"}
									/>
								</View>
							</View>
						)}
					</View>
				) : null}
			</View>
		);
	};

	componentWillUnmount() {
		console.log("Traveller Details", "componentWillUnmount");
		this.props.dispatch(resetTravellers());
	}

	render() {
		return (
			<View style={styles.fullFlex}>
				<View style={styles.headerBar}>
					<View style={styles.centeredRow}>
						<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
							<View style={styles.goBackButton}>
								<Icon
									iconType={"material"}
									iconSize={height / 24}
									iconName={"arrow-back"}
									iconColor={"#ffffff"}
								/>
							</View>
						</TouchableOpacity>
						<CleartripTextMedium style={styles.travellerDetailText}>
							{I18n.t("traveller_details")}
						</CleartripTextMedium>
					</View>
				</View>
				<ScrollView
					keyboardShouldPersistTaps={"always"}
					contentContainerStyle={styles.scrollViewContent}>
					<View style={styles.headingBar}>
						<View style={styles.centeredRow}>
							<CleartripTextBold style={styles.travellerTextHeader}>
								{I18n.t("traveller_details")}
							</CleartripTextBold>
						</View>
					</View>
					<View>
						{this.props.adults === 1 &&
						this.props.children === 0 &&
						this.props.infants === 0 ? (
							<View>{this.renderForm()}</View>
						) : (
							this.renderTravellers()
						)}
					</View>
					<View style={styles.contactDetailBar}>
						<View style={styles.centeredRow}>
							<CleartripTextBold style={styles.travellerTextHeader}>
								{I18n.t("contact_details")}
							</CleartripTextBold>
						</View>
					</View>
					<View style={styles.phonePadView}>
						<TextInput
							value={this.state.mobile}
							keyboardType={"phone-pad"}
							style={styles.textInput}
							underlineColorAndroid={"transparent"}
							onChangeText={text => this.setState({ mobile: text })}
							placeholder={I18n.t("mobile_number")}
							placeholderTextColor={"#cccccc"}
							returnKeyType={"done"}
						/>
					</View>
					<View style={styles.phonePadView}>
						<TextInput
							value={this.state.email}
							keyboardType={"email-address"}
							style={styles.textInput}
							underlineColorAndroid={"transparent"}
							onChangeText={text => this.setState({ email: text })}
							placeholder={I18n.t("email_id")}
							placeholderTextColor={"#cccccc"}
						/>
					</View>
					<TouchableOpacity
						activeOpacity={1}
						style={styles.continueButton}
						onPress={this.bookFlight}>
						<CleartripTextBold style={styles.continueButtonText}>
							{I18n.t("continue_booking").toUpperCase()}
						</CleartripTextBold>
					</TouchableOpacity>
				</ScrollView>
				{this.isPriceChanged && (
					<DialogModal>
						<View style={styles.priceChangedText}>
							<CleartripTextRegular>
								{I18n.t("price_changed")}
							</CleartripTextRegular>
						</View>
					</DialogModal>
				)}
				{this.state.showSpinner && (
					<ProgressScreen indicatorColor={"#EC6733"} />
				)}
				{this.state.showDatePicker ? (
					<DatePicker
						date={this.state.date}
						minimumDate={this.state.minDate}
						maximumDate={this.state.maxDate}
						onDateChange={date => {
							if (date !== null) {
								let date_object = {
									dd: date.day,
									mm: date.month + 1,
									yyyy: date.year
								};
								if ((date_object.mm + "").length === 1) {
									date_object.mm = "0" + date_object.mm;
								}
								if ((date_object.dd + "").length === 1) {
									date_object.dd = "0" + date_object.dd;
								}
								if (this.passportDateType !== null) {
									if (this.passportDateType === "expiry") {
										this.passportDateType = null;
										this.setState({
											showDatePicker: false,
											passport_exp_date:
												date_object.yyyy +
												"-" +
												date_object.mm +
												"-" +
												date_object.dd
										});
									} else {
										this.passportDateType = null;
										this.setState({
											showDatePicker: false,
											passport_issue_date:
												date_object.yyyy +
												"-" +
												date_object.mm +
												"-" +
												date_object.dd
										});
									}
								} else {
									this.setState({
										dob: date_object,
										showDatePicker: false
									});
								}
							} else {
								this.passportDateType = null;
								this.setState({
									showDatePicker: false
								});
							}
						}}
					/>
				) : null}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		adults: state.cleartrip && state.cleartrip.searchFlight.adults,
		children: state.cleartrip && state.cleartrip.searchFlight.children,
		infants: state.cleartrip && state.cleartrip.searchFlight.infants,
		appToken: state.appToken.token,
		domestic: state.cleartrip && state.cleartrip.searchFlight.domestic,
		international:
			state.cleartrip && state.cleartrip.searchFlight.international,
		flights: state.cleartrip && state.cleartrip.searchFlight.flights,
		adultsDetails:
			state.cleartrip && state.cleartrip.travellerDetails.adultsDetails,
		childrenDetails:
			state.cleartrip && state.cleartrip.travellerDetails.childrenDetails,
		infantDetails:
			state.cleartrip && state.cleartrip.travellerDetails.infantsDetails,
		multicity: state.cleartrip && state.cleartrip.searchFlight.multicity,
		selectedFlights:
			state.cleartrip && state.cleartrip.selectFlight.selectedFlights,
		searchInfo:
			state.cleartrip &&
			state.cleartrip.searchFlight &&
			state.cleartrip.searchFlight.searchInfo
				? state.cleartrip.searchFlight.searchInfo
				: null
	};
}

export default connect(mapStateToProps)(TravellerDetails);
