import { takeLatest, takeEvery, put, call } from "redux-saga/effects";

export const SAVE_ADULT_DETAILS = "SAVE_ADULT_DETAILS";
export const SAVE_CHILD_DETAILS = "SAVE_CHILD_DETAILS";
export const SAVE_INFANT_DETAILS = "SAVE_INFANT_DETAILS";
export const RESET_TRAVELLERS = "RESET_TRAVELLERS";
export const SAVE_ITINERARY_DETAILS = "SAVE_ITINERARY_DETAILS";
export const RESET_ITINERARY_DETAILS = "RESET_ITINERARY_DETAILS";

export const saveAdultDetails = payload => ({
	type: SAVE_ADULT_DETAILS,
	payload
});
export const saveChildDetails = payload => ({
	type: SAVE_CHILD_DETAILS,
	payload
});
export const saveInfantDetails = payload => ({
	type: SAVE_INFANT_DETAILS,
	payload
});
export const saveItineraryDetails = payload => ({
	type: SAVE_ITINERARY_DETAILS,
	payload
});

export const resetItineraryDetails = payload => ({
	type: RESET_ITINERARY_DETAILS,
	payload
});

export const resetTravellers = payload => ({ type: RESET_TRAVELLERS, payload });

export function* TravellerDetailsSaga(dispatch) {
	// yield takeEvery(SAVE_ADULT_DETAILS, handleSaveAdultDetails);
}

function* handleSaveAdultDetails(action) {
	yield put(saveAdultDetails(action));
}
