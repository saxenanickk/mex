import {
	SAVE_ADULT_DETAILS,
	SAVE_CHILD_DETAILS,
	SAVE_INFANT_DETAILS,
	RESET_TRAVELLERS,
	SAVE_ITINERARY_DETAILS,
	RESET_ITINERARY_DETAILS
} from "./Saga";

const initialState = {
	adultsDetails: [],
	childrenDetails: [],
	infantsDetails: [],
	itinerary_id: "",
	itineraryDetails: null
};

export const reducer = (state = initialState, action) => {
	var index;
	switch (action.type) {
		case SAVE_ADULT_DETAILS:
			if (action.payload) {
				index = action.payload.id;
				return {
					...state,
					adultsDetails: [
						...state.adultsDetails.slice(0, index),
						action.payload,
						...state.adultsDetails.slice(index + 1)
					]
				};
			}
			return {
				...state,
				adultsDetails: []
			};
		case SAVE_CHILD_DETAILS:
			if (action.payload) {
				index = action.payload.id;
				return {
					...state,
					childrenDetails: [
						...state.childrenDetails.slice(0, index),
						action.payload,
						...state.childrenDetails.slice(index + 1)
					]
				};
			}
			return {
				...state,
				childrenDetails: []
			};
		case SAVE_INFANT_DETAILS:
			if (action.payload) {
				index = action.payload.id;
				return {
					...state,
					infantsDetails: [
						...state.infantsDetails.slice(0, index),
						action.payload,
						...state.infantsDetails.slice(index + 1)
					]
				};
			}
			return {
				...state,
				infantsDetails: []
			};
		case RESET_TRAVELLERS:
			return {
				...state,
				adultsDetails: [],
				childrenDetails: [],
				infantsDetails: []
			};
		case SAVE_ITINERARY_DETAILS:
			return {
				...state,
				itineraryDetails: action.payload
			};
		case RESET_ITINERARY_DETAILS:
			return {
				...state,
				itineraryDetails: null
			};
		default:
			return state;
	}
};
