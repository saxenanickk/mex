/* eslint-disable radix */
import React, { Component } from "react";
import { View, Image, StatusBar } from "react-native";
import { connect } from "react-redux";

import { SPLASH } from "../../Assets/Img/Image";
import LocationModule from "../../../../CustomModules/LocationModule";
import CleartripApi from "../../Api";
import { saveOrigin, searchFlights } from "../SearchFlight/Saga";
import styles from "./style";
import { GoToast } from "../../../../Components";
import I18n from "../../Assets/Strings/i18n";
import { CommonActions } from "@react-navigation/native";

class Splash extends Component {
	constructor(props) {
		super(props);
		this.apicall = false;
		this.fetchOrigin = this.fetchOrigin.bind(this);
	}

	isNavigated = false;

	componentDidMount() {
		const {
			origin,
			destination,
			departDate,
			returnDate = "",
			adults = 1,
			children = 0,
			infants = 0,
			type
		} = this.props?.deeplink?.params ? this.props.deeplink.params : {};

		if (this.props.appToken) {
			if (type === "searchflight") {
				this.handleDeeplink(
					origin,
					destination,
					departDate,
					returnDate,
					adults,
					children,
					infants,
					this.props.appToken
				);
			} else {
				console.log("HELLO ");
				this.fetchOrigin(this.props.location, this.props.appToken);
				this.isNavigated = true;
				setTimeout(() => {
					this.props.navigation.dispatch(
						CommonActions.reset({
							index: 0,
							routes: [{ name: "SearchFlight" }]
						})
					);
				}, 1000);
			}
		}
	}

	shouldComponentUpdate(props) {
		const {
			origin,
			destination,
			departDate,
			returnDate = "",
			adults = 1,
			children = 0,
			infants = 0,
			type
		} = this.props?.deeplink?.params ? this.props.deeplink.params : {};

		if (props.appToken !== null && !this.isNavigated) {
			if (type === "searchflight") {
				this.handleDeeplink(
					origin,
					destination,
					departDate,
					returnDate,
					adults,
					children,
					infants,
					props.appToken
				);
			} else {
				this.fetchOrigin(props.location, props.appToken);
				setTimeout(() => {
					this.props.navigation.dispatch(
						CommonActions.reset({
							index: 0,
							routes: [{ name: "SearchFlight" }]
						})
					);
				}, 1000);
			}
		}
		return true;
	}

	handleDeeplink = (
		origin,
		destination,
		departDate,
		returnDate = "",
		adults = 1,
		children = 0,
		infants = 0,
		token
	) => {
		console.log();
		let date = new Date();
		this.props.dispatch(
			searchFlights({
				appToken: token,
				dateofdeparture: departDate
					? departDate
					: `${date.getFullYear}-${date.getMonth}-${date.getDate}`,
				dateofreturn: returnDate,
				source: origin.toUpperCase(),
				destination: destination.toUpperCase(),
				adults: parseInt(adults),
				children: children,
				infants: infants,
				location: this.props.location
			})
		);
		this.props.navigation.dispatch(
			CommonActions.reset({
				index: 1,
				routes: [{ name: "SearchFlight" }, { name: "SelectFlight" }]
			})
		);
	};

	calculateMetersBetweenLatAndLng(lat1, lon1, lat2, lon2) {
		// generally used geo measurement function
		let R = 6378.137; // Radius of earth in KM
		let P = Math.PI / 180;
		let dLat = lat2 * P - lat1 * P;
		let dLon = lon2 * P - lon1 * P;
		let a =
			Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.cos((lat1 * Math.PI) / 180) *
				Math.cos((lat2 * Math.PI) / 180) *
				Math.sin(dLon / 2) *
				Math.sin(dLon / 2);
		let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		let d = R * c;
		return d * 1000; // meters
	}

	fetchOrigin(location, token) {
		if (location.status && token !== null) {
			if (!this.apicall) {
				this.apicall = true;
				CleartripApi.fetchNearbyAirports({
					lat: location.latitude,
					lng: location.longitude,
					appToken: token
				})
					.then(data => {
						console.log("=> ", data);
						if (data && data.success) {
							let allAirport = data.map(item => {
								let distance = this.calculateMetersBetweenLatAndLng(
									location.latitude,
									location.longitude,
									item.latitude,
									item.longitude
								);
								return {
									...item,
									distance: distance
								};
							});
							allAirport.sort(function(first_airport, second_airport) {
								return first_airport.distance - second_airport.distance;
							});
							console.log("all airport are", allAirport);
							if (typeof allAirport[0].code !== "undefined") {
								CleartripApi.airportAutocomplete({
									appToken: token,
									query: allAirport[0].code
								})
									.then(res => {
										console.log(res);
										if (this.props.origin === null) {
											this.props.dispatch(
												saveOrigin({
													code: res[0].code,
													city: res[0].city
												})
											);
										}
									})
									.catch(err => {
										console.log(err);
									});
							}
						}
					})
					.catch(error => {
						console.log("Error: ", error);
					});
			}
		}
	}

	render() {
		return (
			<View>
				<LocationModule
					onError={() => {
						GoToast.show(I18n.t("no_location_found"), I18n.t("information"));
					}}
				/>
				<StatusBar backgroundColor="#923e0b" barStyle="light-content" />
				<Image style={styles.splashImage} source={SPLASH} />
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		location: state.location,
		origin: state.cleartrip && state.cleartrip.searchFlight.origin,
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		deeplink: state.cleartrip && state.cleartrip.cleartripRoot.deeplink
	};
}

export default connect(mapStateToProps)(Splash);
