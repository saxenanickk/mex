import { takeEvery, put, call } from "redux-saga/effects";
import Api, { NO_NETWORK_ERROR } from "../../Api/";
import { cleartripNetworkError } from "../../Saga";
/**
 *	Constants.
 */
export const BOOK_FLIGHT = "BOOK_FLIGHT";
export const SAVE_BOOKING_DETAILS = "SAVE_BOOKING_DETAILS";
export const SAVE_ITINERARY_ID = "SAVE_ITINERARY_ID";
export const PRICE_CHECK = "PRICE_CHECK";
export const PRICE_CHECK_RESPONSE = "PRICE_CHECK_RESPONSE";
export const VIEW_ITINERARY = "VIEW_ITINERARY";
export const ITINERARY_DETAILS = "ITINERARY_DETAILS";
export const RETRIEVE_BOOKING_DETAILS = "GET_BOOKING_DETAILS";
export const CLEARTRIP_CONFIRM_FLIGHT_ERROR = "CLEARTRIP_CONFIRM_FLIGHT_ERROR";
/**
 *	Action Creators.
 */
export const bookFlight = payload => ({ type: BOOK_FLIGHT, payload });
export const saveBookingDetails = payload => ({
	type: SAVE_BOOKING_DETAILS,
	payload
});
export const saveItineraryId = payload => ({
	type: SAVE_ITINERARY_ID,
	payload
});
export const priceCheck = payload => ({ type: PRICE_CHECK, payload });
export const priceCheckResponse = payload => ({
	type: PRICE_CHECK_RESPONSE,
	payload
});
export const viewItinerary = payload => ({ type: VIEW_ITINERARY, payload });
export const itineraryDetails = payload => ({
	type: ITINERARY_DETAILS,
	payload
});
export const retrieveBookingDetail = payload => ({
	type: RETRIEVE_BOOKING_DETAILS,
	payload
});
export const ctFlightConfirmFlightError = payload => ({
	type: CLEARTRIP_CONFIRM_FLIGHT_ERROR,
	payload
});
export function* confirmFlightSaga(dispatch) {
	yield takeEvery(BOOK_FLIGHT, handleBookFlight);
	yield takeEvery(PRICE_CHECK, handlePriceCheck);
	yield takeEvery(VIEW_ITINERARY, handleViewItinerary);
	yield takeEvery(RETRIEVE_BOOKING_DETAILS, handleRetrieveBooking);
}

function* handleBookFlight(action) {
	try {
		let bookingStatus = yield call(Api.bookFlight, action.payload);
		console.log("bookingStatus :", bookingStatus);
		if (
			bookingStatus &&
			bookingStatus.error &&
			bookingStatus.errorCode === NO_NETWORK_ERROR
		) {
			yield put(
				cleartripNetworkError({
					actionType: action.type,
					payload: action.payload
				})
			);
			return;
		}
		yield put(saveBookingDetails(bookingStatus));
	} catch (error) {
		console.log("Book Flight saga error :", error);
	}
}

function* handlePriceCheck(action) {
	try {
		if (action.payload == null) {
			yield put(priceCheckResponse(null));
		} else {
			let price = yield call(Api.priceCheck, action.payload);
			console.log("price check response :", price);
			if (price && price.error && price.errorCode === NO_NETWORK_ERROR) {
				yield put(
					cleartripNetworkError({
						actionType: action.type,
						payload: action.payload
					})
				);
				return;
			}
			yield put(priceCheckResponse(price));
		}
	} catch (error) {
		console.log("Price Check saga error :", error);
	}
}

function* handleViewItinerary(action) {
	try {
		let itinerary = yield call(Api.viewItinerary, action.payload);
		console.log("after price check failure iteneray :", itinerary);
		if (
			itinerary &&
			itinerary.error &&
			itinerary.errorCode === NO_NETWORK_ERROR
		) {
			yield put(
				cleartripNetworkError({
					actionType: action.type,
					payload: action.payload
				})
			);
			return;
		}
		yield put(itineraryDetails(itinerary));
	} catch (error) {
		console.log("View Itinerary saga error :", error);
	}
}

function* handleRetrieveBooking(action) {
	try {
		let bookingDetails = yield call(Api.retrieveBooking, action.payload);
		console.log("BookingStatus :", bookingDetails);
		let str = bookingDetails;
		if (str && str.error && str.errorCode === NO_NETWORK_ERROR) {
			yield put(
				cleartripNetworkError({
					actionType: action.type,
					payload: action.payload
				})
			);
			return;
		}
		yield put(saveBookingDetails(JSON.parse(str)));
	} catch (error) {
		console.log("BookingDetails saga error :", error);
	}
}
