import {
	SAVE_BOOKING_DETAILS,
	SAVE_ITINERARY_ID,
	PRICE_CHECK_RESPONSE,
	ITINERARY_DETAILS,
	CLEARTRIP_CONFIRM_FLIGHT_ERROR
} from "./Saga";

const initialState = {
	bookingDetails: null,
	itinerary_id: null,
	priceCheckResponse: null,
	itineraryDetails: null,
	flightError: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case SAVE_BOOKING_DETAILS:
			return {
				...state,
				bookingDetails: action.payload
			};
		case SAVE_ITINERARY_ID:
			return {
				...state,
				itinerary_id: action.payload.itineraryid
			};
		case PRICE_CHECK_RESPONSE:
			return {
				...state,
				priceCheckResponse: action.payload
			};
		case ITINERARY_DETAILS:
			return {
				...state,
				itineraryDetails: action.payload
			};
		case CLEARTRIP_CONFIRM_FLIGHT_ERROR:
			return {
				...state,
				flightError: action.payload
			};
		default:
			return state;
	}
};
