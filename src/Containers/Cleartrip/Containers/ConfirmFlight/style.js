import { Dimensions, StyleSheet } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	headerBar: {
		width: width,
		elevation: 5,
		backgroundColor: "#3366cc",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		justifyContent: "space-between",
		paddingRight: width / 26.3
	},
	centeredRow: {
		flexDirection: "row",
		alignItems: "center"
	},
	goBackButton: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	confirmFlightHeaderText: {
		color: "#ffffff",
		fontSize: height / 38.4,
		marginLeft: width / 35,
		marginRight: width / 35
	},
	errorView: {
		height: height,
		justifyContent: "center",
		alignItems: "center"
	},
	errorPrimaryText: {
		fontSize: width / 25
	},
	errorSecondaryText: {
		fontSize: width / 25,
		marginTop: width / 40
	},
	goHomeButton: {
		width: width / 1.08,
		marginTop: width / 10,
		alignItems: "center",
		justifyContent: "center",
		borderRadius: height / 192,
		height: height / 15.6,
		backgroundColor: "#f26822"
	},
	goHomeText: {
		color: "#fff",
		alignSelf: "center",
		fontSize: height / 45.7
	},
	inProgressView: {
		backgroundColor: "#ffffff",
		height: height,
		alignItems: "center",
		justifyContent: "center"
	},
	inProgressText: {
		color: "#242424",
		textAlign: "center",
		fontSize: height / 40,
		marginBottom: height / 30
	},
	bookingFailedView: {
		backgroundColor: "#ffffff",
		height: height - height / 13.33,
		justifyContent: "center",
		alignItems: "center"
	},
	bookingFailedPrimaryText: {
		textAlign: "center",
		fontSize: height / 40
	},
	bookingFailedSecondaryText: {
		marginBottom: height / 24,
		textAlign: "center",
		fontSize: height / 56.5
	},
	bookingDetailsView: {
		backgroundColor: "#ececec"
	},
	tripInfoView: {
		paddingTop: height / 35,
		paddingHorizontal: width / 26.3,
		height: height / 8.53,
		backgroundColor: "#ececec",
		flexDirection: "row",
		justifyContent: "space-between"
	},
	bookingCompletedText: {
		fontSize: width / 30
	},
	yourTripText: {
		fontSize: height / 56.5
	},
	priceView: {
		width: width,
		backgroundColor: "#fff",
		alignSelf: "center"
	},
	priceInnerView: {
		borderBottomColor: "#ececec",
		borderBottomWidth: height / 960,
		paddingTop: height / 33.1,
		height: height / 7.2,
		marginHorizontal: width / 26.3
	},
	priceText: {
		color: "#777777",
		fontSize: height / 40
	},
	totalFareText: {
		fontSize: height / 30,
		color: "#242424"
	},
	tripIdView: {
		paddingTop: height / 33.1,
		height: height / 7.2,
		paddingHorizontal: width / 26.3
	},
	tripIdText: {
		color: "#777777",
		fontSize: height / 40
	},
	tripIdInfoText: {
		fontSize: height / 30,
		color: "#242424"
	},
	experienceView: {
		paddingTop: height / 35,
		paddingHorizontal: width / 26.3,
		height: height / 8.53,
		backgroundColor: "#ececec",
		flexDirection: "row",
		justifyContent: "space-between"
	},
	rateExperienceText: {
		fontSize: width / 30
	},
	rateExperienceQuestionText: {
		fontSize: height / 56.5
	},
	rateStarView: {
		width: width,
		height: height / 10,
		backgroundColor: "#ececec",
		paddingHorizontal: width / 26.3,
		justifyContent: "space-around"
	},
	rateStarButton: {
		width: height / 20.2,
		height: height / 20.2,
		borderRadius: height / 40.4,
		alignItems: "center",
		justifyContent: "center"
	},
	selectedStar: {
		fontSize: height / 43.6,
		color: "#fff"
	},
	unselectedStar: {
		fontSize: height / 43.6,
		color: "#444444"
	},
	viewTripDetailButton: {
		width: width / 1.08,
		margin: width / 26.3,
		elevation: 3,
		borderRadius: height / 192,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#3366cc",
		height: height / 15.7
	},
	viewTripDetailText: {
		color: "#ffffff",
		fontSize: width / 25
	},
	centerJustifiedColumn: {
		justifyContent: "center"
	},
	rateStarSeparator: { width: width / 200 },
	selectedStarButton: {
		width: height / 20.2,
		height: height / 20.2,
		borderRadius: height / 38.4,
		backgroundColor: "#f26822",
		alignItems: "center",
		justifyContent: "center"
	},
	unselectedStarButton: {
		width: height / 24,
		height: height / 24,
		borderRadius: height / 48,
		backgroundColor: "#bdbdbd",
		alignItems: "center",
		justifyContent: "center"
	}
});

export default styles;
