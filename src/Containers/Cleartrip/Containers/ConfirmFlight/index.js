/* eslint-disable radix */
import React, { Component } from "react";
import {
	View,
	ScrollView,
	Dimensions,
	FlatList,
	TouchableOpacity,
	BackHandler,
	Alert
} from "react-native";
import { CommonActions } from "@react-navigation/native";

import { connect } from "react-redux";
import {
	bookFlight,
	saveBookingDetails,
	saveItineraryId,
	priceCheck,
	retrieveBookingDetail
} from "./Saga";
import { resetItineraryDetails } from "../TravellerDetails/Saga";
import {
	CleartripTextRegular,
	CleartripTextMedium,
	CleartripTextBold
} from "../../Components/CleartripText";
import { Icon, ProgressScreen } from "../../../../Components";
import I18n from "../../Assets/Strings/i18n";
import Razorpay from "../../../../CustomModules/RazorPayModule";
import styles from "./style";
import { GoToast } from "../../../../Components";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";

const { height } = Dimensions.get("window");
const PROGRESS_MESSAGE = "Booking is in progress";

class ConfirmFlight extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			rating: null,
			paymentFlag: false,
			razorPayReturnedData: {},
			status: 1
		};
		this.transactionId = null;
		this.bookTicketApiCall = true;
		this.sendPaymentCall = false;
		this.backButtonPress = false;
		this.isInterval = false;
		this.isBookingDone = false;
		this.ratingExperience = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];
		this.trip = this.trip.bind(this);
		this.renderScreen = this.renderScreen.bind(this);
		this.tripDetails = this.tripDetails.bind(this);
		this.sendToPayment = this.sendToPayment.bind(this);
		this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
		this.priceCheckFunc = this.priceCheckFunc.bind(this);
		this.bookingAfterPriceCheck = this.bookingAfterPriceCheck.bind(this);
	}

	priceCheckFunc() {
		let PAXINFOLIST = [];
		for (let i in this.props.adultsDetails) {
			let adult = this.props.adultsDetails[i];
			let size = PAXINFOLIST.length;
			PAXINFOLIST[size] = {
				TITLE: adult.gender,
				FIRSTNAME: adult.firstname,
				LASTNAME: adult.lastname,
				TYPE: "ADT",
				DOB: adult.dob,
				PAXNATIONALITY: "IN",
				POIDETAILS: null,
				PASSPORTDETAILS: adult.passport_detail
			};
		}
		for (let i in this.props.childrenDetails) {
			let child = this.props.childrenDetails[i];
			let size = PAXINFOLIST.length;

			PAXINFOLIST[size] = {
				TITLE: child.gender,
				FIRSTNAME: child.firstname,
				LASTNAME: child.lastname,
				TYPE: "CHD",
				DOB: child.dob,
				PAXNATIONALITY: "IN",
				POIDETAILS: null,
				PASSPORTDETAILS: child.passport_detail
			};
		}
		for (let i in this.props.infantDetails) {
			let infant = this.props.infantDetails[i];
			let size = PAXINFOLIST.length;

			PAXINFOLIST[size] = {
				TITLE: infant.gender,
				FIRSTNAME: infant.firstname,
				LASTNAME: infant.lastname,
				TYPE: "INF",
				DOB: infant.dob,
				PAXNATIONALITY: "IN",
				POIDETAILS: null,
				PASSPORTDETAILS: infant.passport_detail
			};
		}

		const { route } = this.props;
		const { email, mobile } = route.params ? route.params : {};

		let CONTACTDETAILS = {
			TITLE: this.props.adultsDetails[0].gender,
			FIRSTNAME: this.props.adultsDetails[0].firstname,
			LASTNAME: this.props.adultsDetails[0].lastname,
			EMAIl: email,
			ADDRESS: " 6/1, 1st Cross Rd, Stage 2, Hoysala Nagar, Indiranagar",
			MOBILE: mobile,
			CITY: "Bangalore",
			STATE: "Karnataka",
			COUNTRY: "India",
			PINCODE: "560038"
		};
		if (this.props.itineraryDetails !== null) {
			this.props.dispatch(
				priceCheck({
					PAXINFOLIST: PAXINFOLIST,
					CONTACTDETAILS: CONTACTDETAILS,
					itineraryid: this.props.itineraryid,
					appToken: this.props.appToken,
					location: this.props.location
				})
			);
		}
	}

	sendToPayment(changedFare) {
		if (this.props.itineraryDetails !== null && !this.backButtonPress) {
			this.sendPaymentCall = true;
			this.props.dispatch(
				saveItineraryId({ itineraryid: this.props.itineraryid })
			);
			console.log(this.props.itineraryDetails);
			var response = this.props.itineraryDetails.response;
			let AMOUNT = changedFare
				? parseInt(changedFare)
				: parseInt(response.pricing_summary.total_fare);

			Razorpay({
				app: "Cleartrip",
				color: "#f26822",
				description: this.props.itineraryDetails.description,
				amount: AMOUNT
			})
				.then(response => {
					response.razorpay_payment_id;
					if (this.bookTicketApiCall) {
						this.bookTicketApiCall = false;
						this.transactionId = response.razorpay_payment_id;
						try {
							GoAppAnalytics.trackChargeWithProperties(
								AMOUNT,
								response.razorpay_payment_id
							);
						} catch (error) {
							console.log(
								"Error GoAppAnalytics.trackChargeWithProperties",
								error
							);
						}

						this.props.dispatch(
							bookFlight({
								itineraryid: this.props.itineraryid,
								appToken: this.props.appToken,
								payment_id: response.razorpay_payment_id,
								location: this.props.location
							})
						);
					}
					this.setState({ paymentFlag: true });
				})
				.catch(error => {
					console.log("RazorPay Error: ", error);
					GoToast.show(error.description, I18n.t("error"));
					this.props.navigation.goBack();
					if (error.code === 0) {
						Alert.alert(
							I18n.t("cancelled"),
							I18n.t("user_cancelled"),
							[
								{
									text: I18n.t("cancel"),
									onPress: () => console.log("Cancel Pressed"),
									style: "cancel"
								},
								{
									text: I18n.t("ok"),
									onPress: () => console.log("OK Pressed")
								}
							],
							{ cancelable: false }
						);
					}
				});
		}
	}

	/**
	 * Back Button Handler
	 */
	handleBackButtonClick() {
		if (this.props.bookingDetails === null && !this.bookTicketApiCall) {
			// TODO: add alert dialog for booking is ongoing and handles respective scenarios
			return true;
		} else if (this.props.bookingDetails !== null && !this.backButtonPress) {
			this.backButtonPress = true;
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "SearchFlight" }]
				})
			);
			this.props.dispatch(resetItineraryDetails());
			return true;
		} else {
			this.backButtonPress = true;
			this.props.dispatch(resetItineraryDetails());
			this.props.navigation.goBack();
			return true;
		}
	}

	componentDidMount() {
		BackHandler.addEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
		this.priceCheckFunc();
	}

	componentWillReceiveProps(props) {
		console.log("props are", props);
		var msg = "";
		if (
			props.hasOwnProperty("priceCheckResponse") &&
			props.priceCheckResponse != null
		) {
			if (typeof props.priceCheckResponse.succes_message !== "undefined") {
				msg = props.priceCheckResponse.succes_message;
				if (!this.state.paymentFlag) {
					this.sendToPayment();
				}
			} else if (
				typeof props.priceCheckResponse.error_message !== "undefined"
			) {
				const responseErrorMsgArray = props.priceCheckResponse.error_message.split(
					"."
				);

				const changedFare = responseErrorMsgArray[0].match(/[0-9]+\w/g);
				if (props.priceCheckResponse.error_code === 533) {
					if (props.bookingDetails === null) {
						Alert.alert(
							`Fare changed from ₹ ${changedFare[0]} to ₹ ${changedFare[1]}`,
							"Do you want to proceed with the changed fare?",
							[
								{
									text: "No",
									onPress: () => this.props.navigation.goBack(),
									style: "cancel"
								},
								{
									text: "Yes",
									onPress: () => {
										if (changedFare.length >= 2) {
											this.sendToPayment(changedFare[1]);
										} else {
											this.sendToPayment();
										}
									}
								}
							],
							{ cancelable: false }
						);
					}
				} else {
					if (!this.backButtonPress) {
						this.bookingAfterPriceCheck(props.priceCheckResponse.error_message);
					}
				}
			} else if (!this.state.paymentFlag && !this.backButtonPress) {
				this.bookingAfterPriceCheck();
			}
		}

		if (
			props.bookingDetails &&
			props.bookingDetails !== this.props.bookingDetails &&
			props.bookingDetails.hasOwnProperty("message") &&
			props.bookingDetails.message === PROGRESS_MESSAGE
		) {
			if (this.isInterval === false) {
				let self = this;
				this.interval = setInterval(function() {
					self.props.dispatch(
						retrieveBookingDetail({
							itineraryid: self.props.itineraryid,
							appToken: self.props.appToken,
							location: self.props.location,
							transactionId: self.transactionId
						})
					);
				}, 15000);
				this.isInterval = true;
			}

			this.timeOutId = setTimeout(() => {
				clearInterval(this.interval);
				if (!this.isBookingDone) {
					this.isInterval = false;
					this.setState({ status: 0 });
				}
			}, 180000);
		}
		if (
			props.bookingDetails &&
			props.bookingDetails !== this.props.bookingDetails &&
			(props.bookingDetails.hasOwnProperty("itinerary_id") ||
				props.bookingDetails.hasOwnProperty("error_code"))
		) {
			clearInterval(this.interval);
			clearTimeout(this.timeOutId);
			this.isBookingDone = true;
		}
	}

	bookingAfterPriceCheck(toastMsg = null) {
		if (!this.backButtonPress) {
			GoToast.show(
				toastMsg ? toastMsg : I18n.t("price_check_fail"),
				I18n.t("information"),
				"LONG"
			);
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "SearchFlight" }]
				})
			);
		}
	}

	componentWillUnmount() {
		this.props.dispatch(saveBookingDetails(null));
		clearInterval(this.interval);
		clearTimeout(this.timeOutId);
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
	}

	tripDetails() {
		clearInterval(this.interval);
		clearTimeout(this.timeOutId);
		this.props.navigation.navigate("TripDetails");
	}

	trip(data) {
		let flight = data.flights;
		let source = "",
			destination = "";
		console.log("flights are", flight);
		if (flight.constructor === Array) {
			source = this.props.airports[flight[0].segments[1].departure_airport].c;
			destination = this.props.airports[
				flight[0].segments[Object.keys(flight[0].segments).length]
					.arrival_airport
			].c;
		} else {
			source = this.props.airports[flight.segments[1].departure_airport].c;
			destination = this.props.airports[
				flight.segments[Object.keys(flight.segments).length].arrival_airport
			].c;
		}
		return source + " to " + destination;
	}

	renderScreen(data) {
		console.log("called render with data", data);
		if (
			this.props.bookingDetails === null ||
			(this.props.bookingDetails &&
				(this.props.bookingDetails.hasOwnProperty("message") ||
					this.props.bookingDetails.hasOwnProperty("error_code")))
		) {
			if (
				data === null ||
				data.message === PROGRESS_MESSAGE ||
				(data.hasOwnProperty("book") && data.book.message === PROGRESS_MESSAGE)
			) {
				return (
					<View style={styles.inProgressView}>
						<CleartripTextRegular style={styles.inProgressText}>
							{I18n.t("please_wait_while_booking")}
						</CleartripTextRegular>
						<ProgressScreen indicatorColor={"#f26822"} indicatorSize={50} />
					</View>
				);
			} else if (data.hasOwnProperty("error_code")) {
				return (
					<View style={styles.bookingFailedView}>
						<CleartripTextMedium style={styles.bookingFailedPrimaryText}>
							{I18n.t("booking_failed")}
						</CleartripTextMedium>
						<CleartripTextRegular style={styles.bookingFailedSecondaryText}>
							{data.error_message}
						</CleartripTextRegular>
						<TouchableOpacity
							onPress={() => {
								this.props.navigation.dispatch(
									CommonActions.reset({
										index: 0,
										routes: [{ name: "SearchFlight" }]
									})
								);
							}}
							style={styles.goHomeButton}>
							<CleartripTextMedium style={styles.goHomeText}>
								{I18n.t("go_back_home")}
							</CleartripTextMedium>
						</TouchableOpacity>
					</View>
				);
			}
		} else {
			data = this.props.bookingDetails;
			return (
				<ScrollView contentContainerStyle={styles.bookingDetailsView}>
					<View style={styles.tripInfoView}>
						<View style={styles.centerJustifiedColumn}>
							<CleartripTextBold style={styles.bookingCompletedText}>
								{I18n.t("booking_fligh_complete")}
							</CleartripTextBold>
							<CleartripTextMedium style={styles.yourTripText}>
								{I18n.t("your_trip_from") +
									" " +
									this.trip(data) +
									" " +
									I18n.t("trip_confirmed")}
							</CleartripTextMedium>
						</View>
					</View>
					<View style={styles.priceView}>
						<View style={styles.priceInnerView}>
							<CleartripTextMedium style={styles.priceText}>
								{I18n.t("ct_price")}
							</CleartripTextMedium>
							<CleartripTextBold style={styles.totalFareText}>
								{"₹"}
								{I18n.toNumber(data.pricing_summary.total_fare, {
									precision: 0
								})}
							</CleartripTextBold>
						</View>
						<View style={styles.tripIdView}>
							<CleartripTextMedium style={styles.tripIdText}>
								{"Trip id"}
							</CleartripTextMedium>
							<CleartripTextRegular style={styles.tripIdInfoText}>
								{data.trip_id}
							</CleartripTextRegular>
						</View>
					</View>
					<View style={styles.experienceView}>
						<View style={styles.centerJustifiedColumn}>
							<CleartripTextBold style={styles.rateExperienceText}>
								{I18n.t("rate_your_exp")}
							</CleartripTextBold>
							<CleartripTextMedium style={styles.rateExperienceQuestionText}>
								{I18n.t("how_is_your_exp")}
							</CleartripTextMedium>
						</View>
					</View>
					<View style={styles.rateStarView}>
						<View>
							<FlatList
								showsHorizontalScrollIndicator={false}
								data={this.ratingExperience}
								ItemSeparatorComponent={() => (
									<View style={styles.rateStarSeparator} />
								)}
								renderItem={({ item }) => {
									return (
										<TouchableOpacity
											activeOpacity={1}
											style={styles.rateStarButton}
											onPress={() => this.setState({ rating: item })}>
											<View
												style={
													this.state.rating === item
														? styles.selectedStarButton
														: styles.unselectedStarButton
												}>
												<CleartripTextMedium
													style={
														this.state.rating === item
															? styles.selectedStar
															: styles.unselectedStar
													}>
													{item}
												</CleartripTextMedium>
											</View>
										</TouchableOpacity>
									);
								}}
								horizontal={true}
								scrollEnabled={true}
							/>
						</View>
					</View>
					<TouchableOpacity
						activeOpacity={1}
						style={styles.viewTripDetailButton}
						onPress={this.tripDetails}>
						<CleartripTextBold style={styles.viewTripDetailText}>
							{I18n.t("view_trip_detail_cap")}
						</CleartripTextBold>
					</TouchableOpacity>
				</ScrollView>
			);
		}
	}

	render() {
		var data = null;
		if (
			this.props.bookingDetails != null &&
			this.props.hasOwnProperty("bookingDetails")
		) {
			data = this.props.bookingDetails;
		} else if (
			this.props.bookingDetails != null &&
			this.props.hasOwnProperty("bookingDetails") &&
			this.props.bookingDetails.hasOwnProperty("error")
		) {
			data = this.props.bookingDetails;
		}

		return (
			<View>
				<View style={styles.headerBar}>
					<View style={styles.centeredRow}>
						{data === null || !data.hasOwnProperty("error_code") ? (
							<TouchableOpacity
								onPress={() => {
									if (
										this.props.bookingDetails === null &&
										!this.bookTicketApiCall
									) {
										return true;
									} else if (
										this.props.bookingDetails !== null &&
										!this.backButtonPress
									) {
										this.backButtonPress = true;
										this.props.navigation.dispatch(
											CommonActions.reset({
												index: 0,
												routes: [{ name: "SearchFlight" }]
											})
										);
										this.props.dispatch(resetItineraryDetails());
									} else {
										this.backButtonPress = true;
										this.props.dispatch(resetItineraryDetails());
										this.props.navigation.goBack();
									}
								}}>
								<View style={styles.goBackButton}>
									{this.props.bookingDetails !== null ? (
										<Icon
											iconType={"material"}
											iconSize={height / 28}
											iconName={"close"}
											iconColor={"#ffffff"}
										/>
									) : (
										<Icon
											iconType={"material"}
											iconSize={height / 24}
											iconName={"arrow-back"}
											iconColor={"#ffffff"}
										/>
									)}
								</View>
							</TouchableOpacity>
						) : null}
						<CleartripTextMedium style={styles.confirmFlightHeaderText}>
							{I18n.t("confirm_flight")}
						</CleartripTextMedium>
					</View>
				</View>
				{this.state.status === 1 ? (
					this.renderScreen(data)
				) : (
					<View style={styles.errorView}>
						<CleartripTextRegular style={styles.errorPrimaryText}>
							{I18n.t("sorry_for_error_while_booking")}
						</CleartripTextRegular>
						<CleartripTextRegular style={styles.errorSecondaryText}>
							{I18n.t("deducted_amount_will_be_return")}
						</CleartripTextRegular>
						<TouchableOpacity
							onPress={() => {
								this.props.navigation.dispatch(
									CommonActions.reset({
										index: 0,
										routes: [{ name: "SearchFlight" }]
									})
								);
							}}
							style={styles.goHomeButton}>
							<CleartripTextMedium style={styles.goHomeText}>
								{I18n.t("go_back_home")}
							</CleartripTextMedium>
						</TouchableOpacity>
					</View>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	console.log("=========>", state);
	return {
		airports:
			state.cleartrip &&
			state.cleartrip.searchFlight &&
			state.cleartrip.searchFlight.airlines &&
			state.cleartrip.searchFlight.airlines.airports
				? state.cleartrip.searchFlight.airlines.airports
				: null,
		appToken: state.appToken.token,
		itineraryid:
			state.cleartrip &&
			state.cleartrip.travellerDetails.itineraryDetails !== null
				? state.cleartrip.travellerDetails.itineraryDetails.response
						.itinerary_id
				: "",
		bookingDetails:
			state.cleartrip && state.cleartrip.confirmFlight.bookingDetails,
		itineraryDetails:
			state.cleartrip && state.cleartrip.travellerDetails.itineraryDetails,
		priceCheckResponse:
			state.cleartrip && state.cleartrip.confirmFlight.priceCheckResponse,
		adultsDetails:
			state.cleartrip && state.cleartrip.travellerDetails.adultsDetails,
		childrenDetails:
			state.cleartrip && state.cleartrip.travellerDetails.childrenDetails,
		infantDetails:
			state.cleartrip && state.cleartrip.travellerDetails.infantsDetails,
		location: state.location,
		adults: state.cleartrip && state.cleartrip.searchFlight.adults,
		children: state.cleartrip && state.cleartrip.searchFlight.children,
		infants: state.cleartrip && state.cleartrip.searchFlight.infants
	};
}

export default connect(mapStateToProps)(ConfirmFlight);
