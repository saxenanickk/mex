import React, { Component } from "react";
import {
	View,
	Image,
	Dimensions,
	ScrollView,
	FlatList,
	BackHandler,
	TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import { saveTripDetails, retrieveBooking } from "./Saga";

import {
	CleartripTextRegular,
	CleartripTextMedium,
	CleartripTextBold
} from "../../Components/CleartripText";
import { Icon, ProgressScreen } from "../../../../Components";
import styles from "./style";
import I18n from "../../Assets/Strings/i18n";
import { CommonActions } from "@react-navigation/native";
const { width, height } = Dimensions.get("window");

class TripDetails extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {};
		this.renderScreen = this.renderScreen.bind(this);
		this.getTime = this.getTime.bind(this);
		this.getDate = this.getDate.bind(this);
		this.getDurationInHour = this.getDurationInHour.bind(this);
		this.getDuration = this.getDuration.bind(this);
		this.calculateDuration = this.calculateDuration.bind(this);
	}

	componentWillMount() {
		const { route } = this.props;
		const { tripId = null } = route.params ? route.params : {};
		this.props.dispatch(
			retrieveBooking({
				tripId: tripId,
				appToken: this.props.appToken,
				location: this.props.location
			})
		);
	}

	componentDidMount() {
		BackHandler.addEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
	}

	/**
	 * Back Button Handler
	 */
	handleBackButtonClick = () => {
		this.props.navigation.goBack();
		return true;
	};

	componentWillUnmount() {
		this.props.dispatch(saveTripDetails(null));
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
	}

	getTime(dateTime) {
		let date = new Date(dateTime);
		let minutes = date.getMinutes();
		if (minutes < 10) {
			minutes = "0" + minutes;
		}
		return date.getHours() + ":" + minutes;
	}

	getDate(dateTime) {
		let date = new Date(dateTime).toGMTString().split(" ");
		return date[0] + " " + date[2] + " " + date[1];
	}

	getDurationInHour(seconds) {
		var days = Math.floor(seconds / (3600 * 24));
		seconds -= days * 3600 * 24;
		var hrs = Math.floor(seconds / 3600);
		seconds -= hrs * 3600;
		var mnts = Math.floor(seconds / 60);
		seconds -= mnts * 60;
		var duration = "";
		duration += days !== 0 ? days + "d " : "";
		duration += hrs !== 0 ? hrs + "h " : "";
		duration += mnts !== 0 ? mnts + "m " : "";
		return duration;
	}

	calculateDuration(data) {
		let total = data / 60;
		let hours = Math.floor(total / 60);
		let mins = total % 60;
		return hours + "h " + mins + "m";
	}

	getDuration(date1, time1, date2, time2) {
		var parts1 = date1.split("/");
		var parts2 = date2.split("/");
		// console.log(parts1);
		// console.log(parts2);
		var dt1 = new Date(
			parts1[1] + "/" + parts1[0] + "/" + parts1[2] + " " + time1
		);
		// console.log(dt1);
		var dt2 = new Date(
			parts2[1] + "/" + parts2[0] + "/" + parts2[2] + " " + time2
		);
		// console.log(dt2);
		var diff = (dt2 - dt1) / 1000;
		return this.calculateDuration(diff);
	}

	getAirportInfo = (query, type) => {
		let airport = this.props.tripDetails.ancillary_data.airports.filter(
			airport => airport.code === query
		);
		return airport.length === 1 ? airport[0][type] : "";
	};

	shouldComponentUpdate(props, state) {
		return true;
	}
	getAirlineInfo = query => {
		let airline = this.props.tripDetails.ancillary_data.airlines.filter(
			airline => airline.code === query
		)[0];
		return airline.name;
	};
	renderScreen(data) {
		console.log("data for flight details", data);
		if (data === null) {
			return (
				<View style={styles.waitView}>
					<CleartripTextRegular style={styles.waitText}>
						{I18n.t("wait_while_retrieve_trip_detail")}
					</CleartripTextRegular>
					<ProgressScreen indicatorColor={"#f26822"} indicatorSize={50} />
				</View>
			);
		} else if (data.hasOwnProperty("error_code")) {
			return (
				<View style={styles.errorView}>
					<CleartripTextRegular style={styles.errorPrimaryText}>
						{I18n.t("error_while_fetching_flight_details")}
					</CleartripTextRegular>
					<TouchableOpacity
						onPress={() => {
							this.props.navigation.dispatch(
								CommonActions.reset({
									index: 0,
									routes: [{ name: "SearchFlight" }]
								})
							);
						}}
						style={styles.goHomeButton}>
						<CleartripTextMedium style={styles.goHomeText}>
							{I18n.t("go_back_home")}
						</CleartripTextMedium>
					</TouchableOpacity>
				</View>
			);
		} else {
			var airports = [];
			var airline = "";
			let flight = [];
			let flights = [];
			let segments = [];
			let a = [];
			if (data.flights.constructor === Array) {
				data.flights.map(data => {
					let segment = [];
					Object.keys(data.segments).map(keyData => {
						segment.push(data.segments[keyData]);
					});
					flights.push(segment);
				});
			}
			if (data.hasOwnProperty("ancillary_data")) {
				airports = data.ancillary_data.airports.airport;
				airline = data.ancillary_data.airlines.airline;
				console.log("flight are", flights);
				return (
					<View style={styles.flightInfoView}>
						<FlatList
							data={flights}
							renderItem={({ item }) => {
								return (
									<View>
										<View style={styles.flightInfoInnerView}>
											<View style={styles.centeredRow}>
												<Icon
													iconType={"font_awesome"}
													iconName={"plane"}
													iconSize={width / 25}
													iconColor={"rgba(0,0,0,0.5)"}
												/>
												<CleartripTextBold style={styles.cityNameText}>
													{item.length > 0 &&
														this.getAirportInfo(
															item[0].departure_airport,
															"city"
														) +
															" to " +
															this.getAirportInfo(
																item[item.length - 1].arrival_airport,
																"city"
															)}
												</CleartripTextBold>
											</View>
										</View>
										<FlatList
											data={item}
											renderItem={({ item }) => {
												return (
													<View style={styles.airlineInfoView}>
														<View style={styles.airlineInfoInner}>
															<Image
																style={styles.airlineImage}
																source={{
																	uri:
																		"https://www.cleartrip.com/images/logos/air-logos/" +
																		item.airline +
																		".png"
																}}
															/>
															<CleartripTextMedium
																style={styles.airlineInfoText}>
																{this.getAirlineInfo(item.airline)}
															</CleartripTextMedium>
															<CleartripTextMedium
																style={styles.airlineInfoText}>
																{item.airline + " " + item.flight_number}
															</CleartripTextMedium>
														</View>
														<View style={styles.spaceBetweenRow}>
															<View style={styles.departureDateView}>
																<View style={styles.flexStartRow}>
																	<CleartripTextRegular
																		style={styles.depAirportText}>
																		{item.departure_airport}{" "}
																	</CleartripTextRegular>
																	<CleartripTextBold style={styles.depTimeText}>
																		{this.getTime(item.departure_date)}
																	</CleartripTextBold>
																</View>
																<CleartripTextRegular
																	style={styles.depDateText}>
																	{this.getDate(item.departure_date)}
																</CleartripTextRegular>
																<CleartripTextRegular
																	numberOfLines={1}
																	ellipsizeMode={"tail"}
																	style={styles.depAirportName}>
																	{this.getAirportInfo(
																		item.departure_airport,
																		"name"
																	)}
																</CleartripTextRegular>
																<CleartripTextRegular
																	style={styles.depAirportCityText}>
																	{this.getAirportInfo(
																		item.departure_airport,
																		"city"
																	)}
																</CleartripTextRegular>
															</View>
															<View style={styles.durationView}>
																<View style={styles.durationViewInner}>
																	<Icon
																		iconType={"ionicon"}
																		iconName={"md-time"}
																		iconSize={height / 34}
																		iconColor={"rgba(0,0,0,0.4)"}
																	/>
																</View>
																<CleartripTextRegular
																	numberOfLines={1}
																	ellipsizeMode={"tail"}
																	style={styles.durationText}>
																	{this.getDurationInHour(item.duration)}
																</CleartripTextRegular>
															</View>
															<View style={styles.arrivalTimeView}>
																<View style={styles.arrivalTimeInnerView}>
																	<CleartripTextBold
																		style={styles.arrivalTimeText}>
																		{this.getTime(item["arrival-date"])}
																	</CleartripTextBold>
																	<CleartripTextRegular
																		style={styles.arrivalAirportText}>
																		{" " + item.arrival_airport}
																	</CleartripTextRegular>
																</View>
																<CleartripTextRegular
																	style={styles.arrivalDateText}>
																	{this.getDate(item["arrival-date"])}
																</CleartripTextRegular>
																<CleartripTextRegular
																	numberOfLines={1}
																	ellipsizeMode={"tail"}
																	style={styles.arrivalAirportNameText}>
																	{this.getAirportInfo(
																		item.arrival_airport,
																		"name"
																	)}
																</CleartripTextRegular>
																<CleartripTextRegular
																	style={styles.arrivalAirportCityText}>
																	{this.getAirportInfo(
																		item.arrival_airport,
																		"city"
																	)}
																</CleartripTextRegular>
															</View>
														</View>
													</View>
												);
											}}
										/>
									</View>
								);
							}}
						/>
						<View style={styles.travellerInfoView}>
							<View style={styles.centeredRow}>
								<CleartripTextBold style={styles.travellerText}>
									{I18n.t("travellers")}
								</CleartripTextBold>
							</View>
						</View>
						{data.pax_info_list.constructor === Array ? (
							data.pax_info_list.map((data, key) => {
								return (
									<View key={key} style={styles.paxListView}>
										<Icon
											iconType={"ionicon"}
											iconName={"ios-person"}
											iconSize={width / 16}
											iconColor={"rgba(0,0,0,0.5)"}
										/>
										<CleartripTextRegular style={styles.paxNameText}>
											{data.title +
												" " +
												data.first_name +
												" " +
												data.last_name}
										</CleartripTextRegular>
									</View>
								);
							})
						) : (
							<View style={styles.paxListView}>
								<Icon
									iconType={"ionicon"}
									iconName={"ios-person"}
									iconSize={width / 20}
									iconColor={"rgba(0,0,0,0.5)"}
								/>
								<CleartripTextRegular style={styles.paxNameText}>
									{data.pax_info_list.title +
										" " +
										data.pax_info_list.first_name +
										" " +
										data.pax_info_list.last_name}
								</CleartripTextRegular>
							</View>
						)}
						<View style={styles.paymentHeaderView}>
							<View style={styles.centeredRow}>
								<CleartripTextBold style={styles.travellerText}>
									{"Payment Details"}
								</CleartripTextBold>
							</View>
						</View>
						<View style={styles.baseFareView}>
							<View style={styles.baseFareInnerView}>
								<CleartripTextRegular style={styles.baseFareText}>
									{I18n.t("base_fare")}
								</CleartripTextRegular>
								<CleartripTextRegular style={styles.baseFareInfoText}>
									{I18n.toCurrency(data.pricing_summary.base_fare, {
										precision: 0,
										unit: "₹"
									})}
								</CleartripTextRegular>
							</View>
							{data.pricing_summary.markup &&
							data.pricing_summary.markup !== 0 ? (
								<View style={styles.baseFareInnerView}>
									<CleartripTextRegular style={styles.baseFareText}>
										{I18n.t("markup")}
									</CleartripTextRegular>
									<CleartripTextRegular style={styles.baseFareInfoText}>
										{I18n.toCurrency(data.pricing_summary.markup, {
											precision: 0,
											unit: "₹"
										})}
									</CleartripTextRegular>
								</View>
							) : null}
							<View style={styles.baseFareInnerView}>
								<CleartripTextRegular style={styles.baseFareText}>
									{I18n.t("taxes_and_fees")}
								</CleartripTextRegular>
								<CleartripTextRegular style={styles.baseFareInfoText}>
									{I18n.toCurrency(data.pricing_summary.taxes, {
										unit: "₹",
										precision: 0
									})}
								</CleartripTextRegular>
							</View>
							{data.pricing_summary.cashback &&
							data.pricing_summary.cashback !== "0.0" ? (
								<View style={styles.baseFareInnerView}>
									<CleartripTextRegular style={styles.baseFareText}>
										{I18n.t("cashback")}
									</CleartripTextRegular>
									<CleartripTextRegular style={styles.baseFareInfoText}>
										{I18n.toCurrency(data.pricing_summary.cashback, {
											precision: 0,
											unit: "₹"
										})}
									</CleartripTextRegular>
								</View>
							) : null}
							{data.pricing_summary.discount &&
							data.pricing_summary.discount !== 0 ? (
								<View style={styles.baseFareInnerView}>
									<CleartripTextRegular style={styles.baseFareText}>
										{I18n.t("discount")}
									</CleartripTextRegular>
									<CleartripTextRegular style={styles.baseFareInfoText}>
										{I18n.toCurrency(Math.abs(data.pricing_summary.discount), {
											unit: "₹",
											precision: 0
										})}
									</CleartripTextRegular>
								</View>
							) : null}
							<View style={styles.totalFareInnerView}>
								<CleartripTextRegular style={styles.baseFareText}>
									{I18n.t("total_fare")}
								</CleartripTextRegular>
								<CleartripTextRegular style={styles.baseFareInfoText}>
									{I18n.toCurrency(data.pricing_summary.total_fare, {
										precision: 0,
										unit: "₹"
									})}
								</CleartripTextRegular>
							</View>
						</View>
					</View>
				);
			}
		}
	}

	render() {
		var data = this.props.tripDetails || null;
		return (
			<View style={styles.fullFlex}>
				<View style={styles.header}>
					<View style={styles.centeredRow}>
						<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
							<View style={styles.goBackButton}>
								<Icon
									iconType={"material"}
									iconSize={height / 24}
									iconName={"arrow-back"}
									iconColor={"#ffffff"}
								/>
							</View>
						</TouchableOpacity>
						<CleartripTextMedium style={styles.headerText}>
							{I18n.t("trip_details")}
						</CleartripTextMedium>
					</View>
				</View>
				<ScrollView showsVerticalScrollIndicator={false}>
					{this.renderScreen(data)}
				</ScrollView>
			</View>
		);
	}
}

function mapStateToProps(state) {
	console.log(state);
	return {
		appToken: state.appToken.token,
		itineraryid: state.cleartrip && state.cleartrip.confirmFlight.itinerary_id,
		tripDetails: state.cleartrip && state.cleartrip.tripDetails.tripDetails,
		location: state.location,
		bookingDetails:
			state.cleartrip && state.cleartrip.confirmFlight.bookingDetails
	};
}

export default connect(
	mapStateToProps,
	null,
	null
)(TripDetails);
