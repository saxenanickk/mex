import { takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api/";
import { NO_NETWORK_ERROR } from "../../Api/";
import { cleartripNetworkError } from "../../Saga";

/**
 *	Constants.
 */
export const RETRIEVE_BOOKING = "RETRIEVE_BOOKING";
export const SAVE_TRIP_DETAILS = "SAVE_TRIP_DETAILS";
/**
 *	Action Creators.
 */
export const retrieveBooking = payload => ({ type: RETRIEVE_BOOKING, payload });
export const saveTripDetails = payload => ({
	type: SAVE_TRIP_DETAILS,
	payload
});

export function* tripDetailsSaga(dispatch) {
	yield takeEvery(RETRIEVE_BOOKING, handleRetrieveBooking);
}

function* handleRetrieveBooking(action) {
	console.log("handleRetrieveBooking called.....");
	try {
		let bookingDetails = yield call(Api.viewTrip, action.payload);
		console.log("BookingDetails :", bookingDetails);
		if (
			bookingDetails.hasOwnProperty("error") &&
			bookingDetails.errorCode === NO_NETWORK_ERROR
		) {
			yield put(
				cleartripNetworkError({
					actionType: action.type,
					payload: action.payload
				})
			);
			return;
		}
		yield put(saveTripDetails(JSON.parse(bookingDetails)));
	} catch (error) {
		console.log("BookingDetails saga error :", error);
	}
}
