import { SAVE_TRIP_DETAILS } from "./Saga";

const initialState = {
	tripDetails: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case SAVE_TRIP_DETAILS:
			return {
				...state,
				tripDetails: action.payload
			};
		default:
			return state;
	}
};
