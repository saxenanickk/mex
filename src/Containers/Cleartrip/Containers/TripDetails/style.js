import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

export default StyleSheet.create({
	centeredRow: {
		flexDirection: "row",
		alignItems: "center"
	},
	goBackButton: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	fullFlex: { flex: 1, backgroundColor: "#fff" },
	flexStartRow: {
		flexDirection: "row",
		alignItems: "flex-start"
	},
	spaceBetweenRow: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	justifyCenter: {
		justifyContent: "center"
	},
	justifySpaceBetween: {
		justifyContent: "space-between"
	},
	justifyFlexEnd: {
		justifyContent: "flex-end"
	},
	justifySpaceAround: {
		justifyContent: "space-around"
	},
	waitView: {
		backgroundColor: "#ffffff",
		height: height,
		alignItems: "center",
		justifyContent: "center"
	},
	waitText: {
		color: "#242424",
		textAlign: "center",
		fontSize: height / 40,
		marginBottom: height / 30
	},
	flightInfoView: {
		marginVertical: width / 35,
		backgroundColor: "#e9e9e9",
		borderColor: "#ffffff",
		borderRadius: 5,
		elevation: 5,
		width: width / 1.1,
		alignSelf: "center"
	},
	flightInfoInnerView: {
		paddingTop: height / 35,
		paddingHorizontal: width / 26.3,
		height: height / 12.5,
		backgroundColor: "#ececec",
		flexDirection: "row",
		justifyContent: "space-between"
	},
	cityNameText: {
		fontSize: width / 30,
		marginLeft: width / 35
	},
	airlineInfoView: {
		width: width / 1.1,
		height: height / 5,
		backgroundColor: "#FFF",
		padding: width / 35
	},
	airlineInfoInner: {
		flexDirection: "row",
		alignItems: "flex-start"
	},
	airlineImage: {
		width: height / 26.5,
		height: height / 26.5
	},
	airlineInfoText: {
		justifyContent: "flex-start",
		alignItems: "flex-start",
		color: "#999999",
		fontSize: height / 48,
		marginLeft: width / 33.75
	},
	departureDateView: {
		width: width / 2.6 - width / 35,
		alignItems: "flex-end"
	},
	depAirportText: {
		fontSize: width / 20,
		color: "rgba(0,0,0,0.8)"
	},
	depTimeText: {
		fontSize: width / 20,
		color: "rgba(0,0,0,1)"
	},
	depDateText: {
		fontSize: height / 50
	},
	depAirportName: {
		textAlign: "right",
		width: width / 2.6 - width / 35,
		fontSize: height / 68.5
	},
	depAirportCityText: {
		textAlign: "right",
		fontSize: height / 68.5
	},
	durationView: {
		width: width - 2 * (width / 2.6) - 2 * (width / 35),
		alignItems: "center"
	},
	durationViewInner: {
		height: height / 28
	},
	durationText: {
		width: width - 2 * (width / 2.5) - 3 * (width / 35),
		textAlign: "center",
		fontSize: height / 50,
		color: "rgba(0,0,0,0.4)"
	},
	arrivalTimeView: {
		width: width / 2.5 - width / 35
	},
	arrivalTimeInnerView: {
		height: height / 28,
		flexDirection: "row"
	},
	arrivalTimeText: {
		fontSize: width / 20,
		color: "rgba(0,0,0,1)"
	},
	arrivalAirportText: {
		fontSize: width / 20,
		color: "rgba(0,0,0,0.8)"
	},
	arrivalDateText: {
		fontSize: height / 50
	},
	arrivalAirportNameText: {
		width: width / 2.5 - width / 35,
		fontSize: height / 68.5
	},
	arrivalAirportCityText: {
		fontSize: height / 68.5
	},
	travellerInfoView: {
		paddingTop: height / 35,
		paddingHorizontal: width / 26.3,
		height: height / 12.5,
		backgroundColor: "#ececec",
		justifyContent: "center"
	},
	travellerText: {
		fontSize: width / 30
	},
	paxListView: {
		width: width / 1.1,
		height: height / 14,
		paddingHorizontal: width / 35,
		backgroundColor: "#ffffff",
		alignItems: "center",
		justifyContent: "flex-start",
		flexDirection: "row"
	},
	paxNameText: {
		fontSize: width / 23,
		marginLeft: width / 35,
		color: "#000"
	},
	paymentHeaderView: {
		paddingTop: height / 35,
		paddingHorizontal: width / 26.3,
		height: height / 12.5,
		backgroundColor: "#ececec",
		justifyContent: "center"
	},
	baseFareView: {
		borderBottomLeftRadius: height / 192,
		borderBottomRightRadius: height / 192,
		paddingHorizontal: width / 26.3,
		width: width / 1.1,
		justifyContent: "center",
		backgroundColor: "#ffffff"
	},
	baseFareInnerView: {
		flexDirection: "row",
		justifyContent: "space-between",
		borderBottomWidth: height / 1500,
		borderBottomColor: "rgba(0,0,0,0.3)",
		height: height / 16.13,
		alignItems: "center"
	},
	baseFareText: {
		fontSize: height / 50.5,
		color: "#000"
	},
	baseFareInfoText: {
		fontSize: height / 50.5
	},
	totalFareInnerView: {
		flexDirection: "row",
		justifyContent: "space-between",
		height: height / 16.13,
		alignItems: "center"
	},
	header: {
		width: width,
		elevation: 5,
		backgroundColor: "#3366cc",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		justifyContent: "space-between",
		paddingRight: width / 26.3
	},
	headerText: {
		color: "#ffffff",
		fontSize: height / 38.4,
		marginLeft: width / 35,
		marginRight: width / 35
	},
	errorView: {
		height: height,
		justifyContent: "center",
		alignItems: "center"
	},
	errorPrimaryText: {
		fontSize: width / 25
	},
	errorSecondaryText: {
		fontSize: width / 25,
		marginTop: width / 40
	},
	goHomeButton: {
		width: width / 1.08,
		marginTop: width / 10,
		alignItems: "center",
		justifyContent: "center",
		borderRadius: height / 192,
		height: height / 15.6,
		backgroundColor: "#f26822"
	},
	goHomeText: {
		color: "#fff",
		alignSelf: "center",
		fontSize: height / 45.7
	}
});
