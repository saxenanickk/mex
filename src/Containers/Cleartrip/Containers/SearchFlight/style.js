import { StyleSheet, Dimensions, Platform } from "react-native";
import { font_two } from "../../../../Assets/styles";

const { height, width } = Dimensions.get("window");

const styles = StyleSheet.create({
	class: {
		width: width,
		borderWidth: 0,
		padding: width / 50,
		flexDirection: "row",
		height: height / 12,
		justifyContent: "flex-start"
	},
	container: {
		// padding: width / 26.3,
		backgroundColor: "#ffffff"
	},
	placeContainer: {
		flexDirection: "row",
		height: height / 6.18,
		justifyContent: "space-between",
		alignItems: "center"
	},
	tripSelectionContainer: {
		paddingHorizontal: width / 26.3,
		height: height / 15.13,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between"
	},
	dateSelectionContainer: {
		height: height / 8,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	classSelectionContainer: {
		paddingHorizontal: width / 26.3,
		height: height / 8,
		flexDirection: "column",
		justifyContent: "center"
	},
	travellerSelectionContainer: {
		height: height / 8,
		paddingHorizontal: width / 26.3,
		flexDirection: "column",
		justifyContent: "center"
	},
	checkedRadioOuter: {
		width: width / 24,
		height: width / 24,
		borderRadius: width / 48,
		borderColor: "#3366cc",
		borderWidth: width / 250,
		marginRight: width / 70,
		justifyContent: "center",
		alignItems: "center"
	},
	checkedRadioInner: {
		width: width / 42,
		height: width / 42,
		backgroundColor: "#3366cc",
		borderRadius: width / 84
	},
	uncheckedRadio: {
		width: width / 24,
		height: width / 24,
		borderRadius: width / 48,
		borderColor: "#999999",
		borderWidth: width / 250,
		marginRight: width / 70
	},
	fullFlexView: { flex: 1 },
	headerBar: {
		width: width,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		backgroundColor: "#3366cc",
		justifyContent: "space-between",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		paddingHorizontal: width / 40
	},
	searchFlightText: {
		color: "#ffffff",
		fontSize: height / 45.71,
		marginLeft: width / 26.3
	},
	supportTextButton: {
		backgroundColor: "#ffffff",
		flexDirection: "row",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		paddingVertical: height / 90,
		borderRadius: width / 20,
		justifyContent: "flex-start",
		paddingHorizontal: width / 50,
		alignItems: "center",
		marginRight: width / 26.3
	},
	supportText: {
		color: "#3366cc",
		width: width / 7
	},
	tripTypeButton: {
		height: height / 15.13,
		width: width / 3.5,
		alignItems: "center",
		flexDirection: "row"
	},
	centerJustifiedColumn: {
		justifyContent: "center"
	},
	outerUnselectedRadio: {
		width: width / 24,
		height: width / 24,
		borderRadius: width / 48,
		borderColor: "#999999",
		borderWidth: width / 250,
		marginRight: width / 70
	},
	outerSelectedRadio: {
		width: width / 24,
		height: width / 24,
		borderRadius: width / 48,
		borderColor: "#3366cc",
		borderWidth: width / 250,
		marginRight: width / 70,
		justifyContent: "center",
		alignItems: "center"
	},
	innerSelectedRadio: {
		width: width / 42,
		height: width / 42,
		backgroundColor: "#3366cc",
		borderRadius: width / 84
	},
	tripTypeText: {
		color: "#000000",
		fontSize: height / 48
	},
	flexEndJustifyColumn: {
		justifyContent: "flex-end"
	},
	separatorPaddingView: {
		paddingHorizontal: width / 26.3
	},
	addMoreButton: {
		flexDirection: "row",
		alignItems: "center",
		height: height / 10.75
	},
	addMoreText: {
		color: "#3366cc",
		fontSize: width / 30,
		marginLeft: width / 35
	},
	oneWayView: {
		paddingHorizontal: width / 26.3,
		height: height / 3.5
	},
	fromButton: {
		width: width / 2.9
	},
	fromText: {
		color: "#999999",
		fontSize: width / 25
	},
	noFromText: {
		color: "#000000",
		fontSize: width / 10
	},
	flexEndAlignColumn: {
		alignItems: "flex-end"
	},
	interchangeImage: { width: width / 9.25, height: width / 11.17 },
	roundTripButtonOne: {
		width: width / 2
	},
	roundTripButtonTwo: {
		width: width
	},
	chooseDateText: {
		color: "#000000",
		fontSize: height / 36
	},
	fullWidthView: { width: width },
	multicityButton: {
		margin: width / 26.3,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		borderRadius: height / 192,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#f26822",
		height: height / 11.5
	},
	dialogModalBackgroundStyle: {
		backgroundColor: "#000000",
		opacity: 0.6
	},
	travellerView: {
		position: "absolute",
		top: 0,
		left: 0,
		bottom: 0,
		right: 0,
		backgroundColor: "#fff"
	},
	classTypeView: {
		position: "absolute",
		width: width / 1.46,
		height: height / 4.7,
		borderRadius: height / 192,
		backgroundColor: "#ffffff",
		left: width / 6.35,
		top: height / 2.54,
		justifyContent: "center",
		paddingHorizontal: width / 26.3
	},
	classTypeButton: {
		height: height / 15.7,
		flexDirection: "row",
		alignItems: "center"
	},
	classTypeText: {
		fontSize: width / 25,
		color: "#000"
	},
	searchCityView: { flex: 1, backgroundColor: "#ffffff" },
	searchCityHeader: {
		width: width,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		backgroundColor: "#777777",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		justifyContent: "space-between",
		paddingRight: width / 26.3
	},
	centeredRow: {
		flexDirection: "row",
		alignItems: "center"
	},
	goBackButton: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	searchQueryTextInput: {
		height: height / 13.33,
		width: (4.2 * width) / 6,
		backgroundColor: "transparent",
		color: "#ffffff",
		fontFamily: font_two,
		fontSize: height / 45.7
	},
	searchCityClearButton: {
		width: (0.8 * width) / 6,
		height: height / 13.33,
		alignItems: "center",
		justifyContent: "center",
		right: 0
	},
	emptyView: {
		width: height / 50,
		height: height / 50
	},
	popularCityView: {
		paddingTop: height / 25,
		paddingHorizontal: width / 26.3,
		height: height / 12.5,
		backgroundColor: "#ececec"
	},
	popularCityText: {
		color: "#777777",
		fontSize: height / 50
	},
	cityNameButton: {
		height: height / 16.55,
		flexDirection: "row",
		justifyContent: "flex-start",
		alignItems: "center",
		borderBottomColor: "#ececec",
		borderBottomWidth: height / 1000
	},
	cityNameButtonInnerView: {
		width: width / 6,
		paddingLeft: width / 26.3
	},
	cityCodeView: {
		width: width / 10.81,
		height: height / 40,
		backgroundColor: "#777777",
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 86
	},
	cityCodeText: {
		fontSize: width / 38,
		color: "#ffffff"
	},
	cityInfoText: {
		width: (4 * width) / 5,
		fontSize: height / 56,
		color: "#000000"
	}
});

export default styles;
