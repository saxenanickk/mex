import React, { Component } from "react";
import {
	View,
	Dimensions,
	Image,
	ScrollView,
	TouchableOpacity,
	StatusBar,
	Keyboard,
	UIManager,
	LayoutAnimation,
	Platform
} from "react-native";
import { connect } from "react-redux";
import Seperator from "../../../../Components/Seperator";
import Icon from "../../../../Components/Icon";
import Dropdown from "../../../../Components/Dropdown";
import {
	searchFlights,
	saveTotalTravellers,
	searchFlightsMultiCity,
	saveSearchFlightInformation
} from "./Saga";
import {
	CleartripTextRegular,
	CleartripTextMedium,
	CleartripTextBold,
	CleartripTextLight
} from "../../Components/CleartripText";
import FromToRow from "./FromToRow";
import { INTERCHANGESOURCEDESTINATION } from "../../Assets/Img/Image";
import I18n from "../../Assets/Strings/i18n";
import { DatePicker, HomeBack } from "../../../../Components";
import styles from "./style";
import Freshchat from "../../../../CustomModules/Freshchat";
import { GoToast } from "../../../../Components";

const { width, height } = Dimensions.get("window");
const passanger_class = [
	{ name: "Economy" },
	{ name: "Business" },
	{ name: "Premium Economy" }
];

if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}

class SearchFlight extends Component {
	constructor(props) {
		super(props);
		this.state = {
			index: 0,
			datePickerType: "depart",
			isMulti: false,
			mode: "one_way",
			modalVisible: false,
			modalType: null,
			travellerModal: false,
			adults: 1,
			totalMaxIndex: 1,
			children: 0,
			infants: 0,
			isInverted: false,
			class: "Economy",
			origin: props.origin
				? props.origin
				: {
						code: null,
						city: null
				  },
			destination: {
				code: null,
				city: null
			},
			locationType: "origin",
			departDate: {
				dd: new Date().getDate(),
				mm: new Date().getMonth() + 1,
				yyyy: new Date().getFullYear()
			},
			returnDate: {
				dd: null,
				mm: null,
				yyyy: null
			},
			multicity: [
				{
					origin: {
						code: null,
						city: null
					},
					destination: {
						code: null,
						city: null
					},
					departDate: {
						dd: null,
						mm: null,
						yyyy: null
					}
				},
				{
					origin: {
						code: null,
						city: null
					},
					destination: {
						code: null,
						city: null
					},
					departDate: {
						dd: null,
						mm: null,
						yyyy: null
					}
				}
			]
		};
		this.month = [
			"01",
			"02",
			"03",
			"04",
			"05",
			"06",
			"07",
			"08",
			"09",
			"10",
			"11",
			"12"
		];
		this.num = [1, 2, 3, 4, 5, 6, 7, 8, 9];
		this.dateActionDepart = this.dateActionDepart.bind(this);
		this.dateActionReturn = this.dateActionReturn.bind(this);
		this.travellers = this.travellers.bind(this);
		this.interchange = this.interchange.bind(this);
		this.searchFlight = this.searchFlight.bind(this);
		this.searchFlightMultiCity = this.searchFlightMultiCity.bind(this);
		this.displayPicker = this.displayPicker.bind(this);
		this.XMark = this.XMark.bind(this);
		this.displayPickerMultiCity = this.displayPickerMultiCity.bind(this);
		this.validateData = this.validateData.bind(this);
		this.saveTravellerDetail = this.saveTravellerDetail.bind(this);
		this.getDateToDisplay = this.getDateToDisplay.bind(this);
		this.onDateChange = this.onDateChange.bind(this);
	}

	componentDidMount() {
		// setting the return date
		let depDate = this.state.departDate;
		let returnDate = new Date(
			new Date(depDate.mm + "/" + depDate.dd + "/" + depDate.yyyy).getTime() +
				86400000
		);
		this.setState({
			returnDate: {
				dd: returnDate.getDate(),
				mm: returnDate.getMonth() + 1,
				yyyy: returnDate.getFullYear()
			}
		});
	}

	componentWillUnmount() {
		this.props.dispatch(
			saveTotalTravellers({
				adults: 1,
				children: 0,
				infants: 0
			})
		);
		this.props.dispatch(searchFlights(null));
	}

	interchange(data) {
		this.setState({
			origin: {
				code: this.state.destination.code,
				city: this.state.destination.city
			},
			destination: {
				code: data.code,
				city: data.city
			}
		});
	}

	async dateActionDepart(isMulti, index) {
		LayoutAnimation.spring();
		this.setState({
			showDatePicker: true,
			datePickerType: "depart",
			isMulti,
			index
		});
	}

	async dateActionReturn() {
		LayoutAnimation.spring();
		this.setState({
			showDatePicker: true,
			datePickerType: "return"
		});
	}

	travellers() {
		let traveller = "";
		traveller = `${traveller} ${this.state.adults} ${I18n.t("adult_noun", {
			count: this.state.adults
		})}`;
		traveller =
			this.state.children > 0
				? `${traveller}, ${this.state.children} ${I18n.t("child_noun", {
						count: this.state.children
				  })}`
				: traveller;
		traveller =
			this.state.infants > 0
				? `${traveller}, ${this.state.infants} ${I18n.t("infants_noun", {
						count: this.state.infants
				  })}`
				: traveller;
		return traveller;
	}

	onBackPress = () => {
		this.props.navigation.navigate("Goapp");
	};

	openModal() {
		this.props.navigation.navigate("SearchPlace", {
			origin: this.state.origin,
			destination: this.state.destination,
			onPressItem: data => {
				Keyboard.dismiss();
				if (this.state.mode === "multicity") {
					let multicity = this.state.multicity;
					if (this.state.locationType === "origin") {
						multicity[this.state.index].origin = {
							code: data.item.code,
							city: data.item.city
						};
					} else {
						multicity[this.state.index].destination = {
							code: data.item.code,
							city: data.item.city
						};
						if (this.state.index + 1 in multicity) {
							multicity[this.state.index + 1].origin = {
								code: data.item.code,
								city: data.item.city
							};
						}
					}
					this.setState({ multicity: multicity });
				} else {
					if (this.state.locationType === "origin") {
						this.setState({
							origin: {
								code: data.item.code,
								city: data.item.city
							}
						});
					} else {
						this.setState({
							destination: {
								code: data.item.code,
								city: data.item.city
							}
						});
					}
				}
			}
		});
	}

	displayPicker(data, location) {
		if (data) {
			this.setState({ modalVisible: true, modalType: "traveller" });
		} else if (location === "origin") {
			this.setState({
				locationType: "origin"
			});
			this.openModal();
		} else {
			this.setState({
				locationType: "destination"
			});
			this.openModal();
		}
	}

	displayPickerMultiCity(data, location, index) {
		if (data) {
			this.setState({ modalVisible: true, modalType: "traveller" });
		} else if (location === "origin") {
			this.setState({
				locationType: "origin",
				index: index
			});
			this.openModal();
		} else {
			this.setState({
				locationType: "destination",
				index: index
			});
			this.openModal();
		}
	}

	saveTravellerDetail(isApply, travellerDetail) {
		if (isApply === true) {
			this.setState({
				adults: travellerDetail.adults,
				children: travellerDetail.children,
				infants: travellerDetail.infants
			});
			this.props.dispatch(
				saveTotalTravellers({
					adults: travellerDetail.adults,
					children: travellerDetail.children,
					infants: travellerDetail.infants
				})
			);
		}
		this.setState({ travellerModal: false });
	}

	addRow() {
		if (this.state.mode === "multicity") {
			let multicity = this.state.multicity;
			let totalMaxIndex = this.state.totalMaxIndex + 1;
			multicity[totalMaxIndex] = {
				origin: {
					code:
						totalMaxIndex !== 1
							? multicity[totalMaxIndex - 1].destination.code
							: "...",
					city: "FROM"
				},
				destination: {
					code: "...",
					city: "TO"
				},
				departDate: {
					dd: null,
					mm: null,
					yyyy: null
				}
			};
			this.setState({ multicity: multicity, totalMaxIndex: totalMaxIndex });
		}
	}

	XMark(index) {
		if (this.state.mode === "multicity") {
			let multicity = this.state.multicity;
			multicity.splice(index, 1);
			this.setState({
				multicity: multicity,
				totalMaxIndex: this.state.multicity.length - 1
			});
		}
	}

	validateMultiCityData = () => {
		let msg = "";
		let prevDate = new Date();
		this.state.multicity.forEach(item => {
			if (!item.origin.code) {
				msg = "Invalid depart city";
			}
			if (!item.destination.code) {
				msg = "Invalid arrival city";
			}
			if (!item.departDate.dd) {
				msg = "Invalid depart date";
			}
			if (
				item.origin.code &&
				item.destination.code &&
				item.origin.code === item.destination.code
			) {
				msg = "Departure & Arrival city can't be the same";
			}
			if (
				new Date(
					`${item.departDate.mm}-${item.departDate.dd}-${item.departDate.yyyy}`
				) < prevDate
			) {
				msg = "Invalid depart date";
			}
			prevDate = new Date(
				`${item.departDate.mm}-${item.departDate.dd}-${item.departDate.yyyy}`
			);
		});
		return msg;
	};
	validateData() {
		let msg = "";
		let currDate = new Date();
		let departDate = new Date(
			this.state.departDate.yyyy +
				"-" +
				this.state.departDate.mm +
				"-" +
				this.state.departDate.dd +
				" 23:59:00"
		);
		let returnDate =
			this.state.returnDate.dd !== null
				? new Date(
						this.state.returnDate.yyyy +
							"-" +
							this.state.returnDate.mm +
							"-" +
							this.state.returnDate.dd +
							" 23:59:00"
				  )
				: null;
		if (this.state.origin.code === null) {
			msg = "Depart city isn't valid";
		} else if (this.state.destination.code === null) {
			msg = "Arrival city isn't valid";
		} else if (departDate.getTime() < currDate.getTime()) {
			msg = "Depart date isn't valid";
		} else if (departDate.getTime() > returnDate.getTime()) {
			msg = "Return date isn't valid";
		} else if (
			(this.state.mode === "round_trip" &&
				returnDate !== null &&
				(returnDate.getTime() < currDate.getTime() ||
					returnDate.getTime() < departDate.getTime())) ||
			(this.state.mode === "round_trip" && returnDate === null)
		) {
			msg = "Return date isn't valid";
		} else if (this.state.origin.code === this.state.destination.code) {
			msg = "Departure & Arrival city can't be the same";
		}
		return msg;
	}

	searchFlight() {
		let msg = this.validateData();
		if (msg === "") {
			this.props.dispatch(
				saveSearchFlightInformation({
					isMulticity: false,
					adults: this.state.adults,
					source: this.state.origin,
					infants: this.state.infants,
					children: this.state.children,
					destination: this.state.destination,
					isRoundTrip: this.state.mode === "round_trip",
					dateofdeparture:
						this.state.departDate.yyyy +
						"-" +
						this.state.departDate.mm +
						"-" +
						this.state.departDate.dd,
					dateofreturn:
						this.state.returnDate.dd !== null &&
						this.state.mode === "round_trip"
							? this.state.returnDate.yyyy +
							  "-" +
							  this.state.returnDate.mm +
							  "-" +
							  this.state.returnDate.dd
							: ""
				})
			);
			this.props.dispatch(
				searchFlights({
					appToken: this.props.appToken,
					dateofdeparture:
						this.state.departDate.yyyy +
						"-" +
						this.state.departDate.mm +
						"-" +
						this.state.departDate.dd,
					dateofreturn:
						this.state.returnDate.dd !== null &&
						this.state.mode === "round_trip"
							? this.state.returnDate.yyyy +
							  "-" +
							  this.state.returnDate.mm +
							  "-" +
							  this.state.returnDate.dd
							: "",
					source: this.state.origin.code,
					destination: this.state.destination.code,
					adults: this.state.adults,
					children: this.state.children,
					infants: this.state.infants,
					location: this.props.location
				})
			);
			this.props.navigation.navigate("SelectFlight");
		} else {
			GoToast.show(msg, I18n.t("information"));
		}
	}
	handleDropDownOnPress = selectedItemName => {
		this.setState({
			class: selectedItemName
		});
	};
	searchFlightMultiCity() {
		let msg = this.validateMultiCityData();
		if (msg === "") {
			this.props.dispatch(
				saveSearchFlightInformation({
					isMulticity: true,
					multicity: this.state.multicity,
					adults: this.state.adults,
					children: this.state.children,
					infants: this.state.infants,
					location: this.props.location
				})
			);
			this.props.dispatch(
				searchFlightsMultiCity({
					appToken: this.props.appToken,
					multicity: this.state.multicity,
					adults: this.state.adults,
					children: this.state.children,
					infants: this.state.infants,
					location: this.props.location
				})
			);
			this.props.navigation.navigate("SelectFlight");
		} else {
			GoToast.show(msg, I18n.t("information"));
		}
	}

	getDateToDisplay(date) {
		return I18n.strftime(date, "%a, %d %b");
	}

	getDepartDatePickerDate = () => {
		let start_date = !this.state.isMulti
			? this.state.datePickerType === "depart"
				? new Date(
						this.state.departDate.yyyy +
							"-" +
							this.state.departDate.mm +
							"-" +
							this.state.departDate.dd
				  )
				: this.state.returnDate.dd !== null
				? new Date(
						this.state.returnDate.yyyy +
							"-" +
							this.state.returnDate.mm +
							"-" +
							this.state.returnDate.dd
				  )
				: new Date()
			: this.state.multicity[0].departDate.dd === null
			? new Date()
			: this.state.multicity[this.state.index].departDate.dd === null
			? new Date(
					this.state.multicity[this.state.index - 1].departDate.yyyy +
						"-" +
						this.state.multicity[this.state.index - 1].departDate.mm +
						"-" +
						this.state.multicity[this.state.index - 1].departDate.dd
			  )
			: new Date(
					this.state.multicity[this.state.index].departDate.yyyy +
						"-" +
						this.state.multicity[this.state.index].departDate.mm +
						"-" +
						this.state.multicity[this.state.index].departDate.dd
			  );
		if (start_date == "Invalid Date") {
			start_date = new Date();
		}
		return start_date;
	};

	onDateChange = date => {
		if (date !== null) {
			let selectedDate = new Date(`${date.month + 1}-${date.day}-${date.year}`);
			if (!this.state.isMulti) {
				if (this.state.datePickerType === "depart") {
					let returnDate = {
						dd: this.state.returnDate.dd,
						mm: this.state.returnDate.mm,
						yyyy: this.state.returnDate.yyyy
					};
					if (
						returnDate.dd &&
						selectedDate.getTime() >
							new Date(
								`${returnDate.mm}-${returnDate.dd}-${returnDate.yyyy}`
							).getTime()
					) {
						returnDate = {
							dd: date.day < 10 ? `0${date.day}` : date.day,
							mm: date.month < 9 ? `0${date.month + 1}` : date.month + 1,
							yyyy: date.year
						};
					}
					this.setState({
						departDate: {
							dd: date.day < 10 ? `0${date.day}` : date.day,
							mm: date.month < 9 ? `0${date.month + 1}` : date.month + 1,
							yyyy: date.year
						},
						showDatePicker: false,
						returnDate
					});
				} else {
					this.setState({
						returnDate: {
							dd: date.day < 10 ? `0${date.day}` : date.day,
							mm: date.month < 9 ? `0${date.month + 1}` : date.month + 1,
							yyyy: date.year
						},
						showDatePicker: false
					});
				}
			} else {
				let multicity = this.state.multicity.slice();
				let departDate = {
					dd: date.day < 10 ? `0${date.day}` : date.day,
					mm: date.month < 9 ? `0${date.month + 1}` : date.month + 1,
					yyyy: date.year
				};
				multicity[this.state.index].departDate = departDate;
				this.setState({
					multicity: multicity,
					showDatePicker: false,
					isMulti: false
				});
			}
		} else {
			this.setState({
				showDatePicker: false,
				isMulti: false
			});
		}
	};

	render() {
		return (
			<View style={styles.fullFlexView}>
				<StatusBar backgroundColor="#1f3d7a" barStyle="light-content" />
				<View style={styles.headerBar}>
					<HomeBack
						navigation={this.props.navigation}
						headerText={I18n.t("search_flights_capitalize")}
						headerTextStyle={styles.searchFlightText}
					/>
					<TouchableOpacity
						style={styles.supportTextButton}
						onPress={() => Freshchat.launchSupportChat()}>
						<CleartripTextMedium style={styles.supportText} numberOfLines={1}>
							{I18n.t("app_support")}
						</CleartripTextMedium>
						<Icon
							iconType={"material"}
							iconSize={height / 40}
							iconName={"headset"}
							iconColor={"#3366cc"}
						/>
					</TouchableOpacity>
				</View>
				<ScrollView style={styles.container}>
					<View style={styles.tripSelectionContainer}>
						<TouchableOpacity
							activeOpacity={1}
							style={styles.tripTypeButton}
							onPress={() => {
								this.setState({ mode: "one_way" });
							}}>
							{this.state.mode === "one_way" ? (
								<View style={styles.outerSelectedRadio}>
									<View style={styles.innerSelectedRadio} />
								</View>
							) : (
								<View style={styles.outerUnselectedRadio} />
							)}
							<CleartripTextRegular style={styles.tripTypeText}>
								{I18n.t("one_way")}
							</CleartripTextRegular>
						</TouchableOpacity>
						<TouchableOpacity
							activeOpacity={1}
							style={[styles.tripTypeButton, styles.centerJustifiedColumn]}
							onPress={() => this.setState({ mode: "round_trip" })}>
							{this.state.mode === "round_trip" ? (
								<View style={styles.outerSelectedRadio}>
									<View style={styles.innerSelectedRadio} />
								</View>
							) : (
								<View style={styles.outerUnselectedRadio} />
							)}
							<CleartripTextRegular style={styles.tripTypeText}>
								{I18n.t("round_trip")}
							</CleartripTextRegular>
						</TouchableOpacity>
						<TouchableOpacity
							activeOpacity={1}
							style={[styles.tripTypeButton, styles.flexEndJustifyColumn]}
							onPress={() => this.setState({ mode: "multicity" })}>
							{this.state.mode === "multicity" ? (
								<View style={styles.outerSelectedRadio}>
									<View style={styles.innerSelectedRadio} />
								</View>
							) : (
								<View style={styles.outerUnselectedRadio} />
							)}
							<CleartripTextRegular style={styles.tripTypeText}>
								{I18n.t("multicity")}
							</CleartripTextRegular>
						</TouchableOpacity>
					</View>
					<View style={styles.separatorPaddingView}>
						<Seperator
							width={width / 1.08}
							height={height / 500}
							color={"#ececec"}
						/>
					</View>
					{this.state.mode === "multicity" ? (
						<View style={styles.separatorPaddingView}>
							{this.state.multicity.map((item, key) => {
								return (
									<View key={key}>
										<FromToRow
											origin={item.origin}
											destination={item.destination}
											departDate={item.departDate}
											displayPickerMultiCity={this.displayPickerMultiCity}
											dateAction={this.dateActionDepart}
											index={key}
											xMark={this.XMark}
											getDateToDisplay={this.getDateToDisplay}
										/>
										{key <= 3 && (
											<Seperator
												width={width}
												height={height / 500}
												color={"#ececec"}
											/>
										)}
									</View>
								);
							})}
							{this.state.totalMaxIndex < 3 && (
								<TouchableOpacity
									activeOpacity={1}
									style={styles.addMoreButton}
									onPress={() => this.addRow()}>
									<Icon
										iconName={"add-circle"}
										iconColor={"#3366cc"}
										iconType={"material"}
										iconSize={width / 18}
									/>
									<CleartripTextBold style={styles.addMoreText}>
										{I18n.t("add_one_more")}
									</CleartripTextBold>
								</TouchableOpacity>
							)}
						</View>
					) : (
						<View style={styles.oneWayView}>
							<View style={styles.placeContainer}>
								<TouchableOpacity
									activeOpacity={1}
									style={styles.fromButton}
									onPress={() => this.displayPicker(false, "origin")}>
									<CleartripTextMedium style={styles.fromText}>
										{this.state.origin.city == null
											? I18n.t("from_caps")
											: this.state.origin.city.toUpperCase()}
									</CleartripTextMedium>
									{this.state.origin.code === null ? (
										<CleartripTextLight style={styles.noFromText}>
											{this.state.origin.code === null
												? "..."
												: this.state.origin.code}
										</CleartripTextLight>
									) : (
										<CleartripTextLight style={styles.noFromText}>
											{this.state.origin.code === null
												? "..."
												: this.state.origin.code}
										</CleartripTextLight>
									)}
								</TouchableOpacity>
								<TouchableOpacity
									activeOpacity={1}
									style={styles.interchangeImage}
									onPress={() => this.interchange(this.state.origin)}>
									<Image
										style={styles.interchangeImage}
										source={INTERCHANGESOURCEDESTINATION}
									/>
								</TouchableOpacity>
								<TouchableOpacity
									activeOpacity={1}
									style={[styles.fromButton, styles.flexEndAlignColumn]}
									onPress={() => this.displayPicker(false, "destination")}>
									<CleartripTextMedium style={styles.fromText}>
										{this.state.destination.city == null
											? I18n.t("to_caps")
											: this.state.destination.city.toUpperCase()}
									</CleartripTextMedium>

									{this.state.origin.code === null ? (
										<CleartripTextLight style={styles.noFromText}>
											{this.state.destination.code === null
												? "..."
												: this.state.destination.code}
										</CleartripTextLight>
									) : (
										<CleartripTextLight style={styles.noFromText}>
											{this.state.destination.code === null
												? "..."
												: this.state.destination.code}
										</CleartripTextLight>
									)}
								</TouchableOpacity>
							</View>
							<Seperator
								width={width / 1.08}
								height={height / 500}
								color={"#ececec"}
							/>
							<View style={styles.dateSelectionContainer}>
								<TouchableOpacity
									activeOpacity={1}
									style={
										this.state.mode === "round_trip"
											? styles.roundTripButtonOne
											: styles.roundTripButtonTwo
									}
									onPress={() => this.dateActionDepart(false, 0)}>
									<CleartripTextMedium style={styles.fromText}>
										{I18n.t("depart_on")}
									</CleartripTextMedium>
									<CleartripTextRegular style={styles.chooseDateText}>
										{this.state.departDate.dd != null
											? this.getDateToDisplay(
													new Date(
														this.state.departDate.mm +
															"/" +
															this.state.departDate.dd +
															"/" +
															this.state.departDate.yyyy
													)
											  )
											: I18n.t("choose_date")}
									</CleartripTextRegular>
								</TouchableOpacity>
								{this.state.mode === "round_trip" && (
									<TouchableOpacity
										activeOpacity={1}
										style={[styles.Button, styles.flexEndAlignColumn]}
										onPress={this.dateActionReturn}>
										<CleartripTextMedium style={styles.fromText}>
											{I18n.t("return_on")}
										</CleartripTextMedium>
										<CleartripTextRegular style={styles.chooseDateText}>
											{this.state.returnDate.dd != null
												? this.getDateToDisplay(
														new Date(
															this.state.returnDate.mm +
																"/" +
																this.state.returnDate.dd +
																"/" +
																this.state.returnDate.yyyy
														)
												  )
												: I18n.t("choose_date")}
										</CleartripTextRegular>
									</TouchableOpacity>
								)}
							</View>
						</View>
					)}
					{this.state.mode === "multicity" &&
					this.state.totalMaxIndex > 2 ? null : (
						<View style={styles.separatorPaddingView}>
							<Seperator
								width={width / 1.08}
								height={height / 500}
								color={"#ececec"}
							/>
						</View>
					)}
					<View style={styles.travellerSelectionContainer}>
						<TouchableOpacity
							activeOpacity={1}
							style={[styles.Button, styles.fullWidthView]}
							onPress={() => {
								this.props.navigation.navigate("Traveller", {
									adults: this.state.adults,
									children: this.state.children,
									infants: this.state.infants,
									saveTravellerDetail: this.saveTravellerDetail
								});
							}}>
							<CleartripTextMedium style={styles.fromText}>
								{I18n.t("travellers")}
							</CleartripTextMedium>
							<CleartripTextRegular style={styles.chooseDateText}>
								{this.travellers()}
							</CleartripTextRegular>
						</TouchableOpacity>
					</View>
					<View style={styles.separatorPaddingView}>
						<Seperator
							width={width / 1.08}
							height={height / 500}
							color={"#ececec"}
						/>
					</View>
					<View style={styles.classSelectionContainer}>
						<TouchableOpacity
							activeOpacity={1}
							style={[styles.Button, styles.fullWidthView]}>
							<CleartripTextMedium style={styles.fromText}>
								{I18n.t("class")}
							</CleartripTextMedium>
							<Dropdown
								onPress={this.handleDropDownOnPress}
								data={passanger_class}
								dataStyle={styles.chooseDateText}
							/>
						</TouchableOpacity>
					</View>
					<View style={styles.separatorPaddingView}>
						<Seperator
							width={width / 1.08}
							height={height / 500}
							color={"#ececec"}
						/>
					</View>
					<TouchableOpacity
						activeOpacity={1}
						style={styles.multicityButton}
						onPress={
							this.state.mode === "multicity"
								? this.searchFlightMultiCity
								: this.searchFlight
						}>
						<CleartripTextBold style={styles.searchFlightText}>
							{I18n.t("search_flights")}
						</CleartripTextBold>
					</TouchableOpacity>
				</ScrollView>
				{this.state.showDatePicker ? (
					<DatePicker
						date={this.getDepartDatePickerDate()}
						minimumDate={
							!this.state.isMulti
								? this.state.datePickerType === "depart"
									? new Date()
									: new Date(
											this.state.departDate.yyyy +
												"-" +
												this.state.departDate.mm +
												"-" +
												this.state.departDate.dd
									  )
								: this.state.index === 0 ||
								  this.state.multicity[0].departDate.dd === null
								? new Date()
								: new Date(
										this.state.multicity[this.state.index - 1].departDate.yyyy +
											"-" +
											this.state.multicity[this.state.index - 1].departDate.mm +
											"-" +
											this.state.multicity[this.state.index - 1].departDate.dd
								  )
						}
						onDateChange={this.onDateChange}
					/>
				) : null}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		origin: state.cleartrip && state.cleartrip.searchFlight.origin,
		location: state.location
	};
}

export default connect(mapStateToProps)(SearchFlight);
