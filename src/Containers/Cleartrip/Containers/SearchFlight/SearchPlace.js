import React from "react";
import {
	View,
	StatusBar,
	FlatList,
	TouchableOpacity,
	TextInput,
	Dimensions
} from "react-native";
import { connect } from "react-redux";
import styles from "./style";
import { Icon } from "../../../../Components";
import {
	CleartripTextMedium,
	CleartripTextBold
} from "../../Components/CleartripText";
import I18n from "../../Assets/Strings/i18n";
import { airportAutocomplete } from "./Saga";

const { height } = Dimensions.get("window");

const airportData = [
	{
		code: "BLR",
		city: "Bangalore",
		country: "India",
		country_code: "IN",
		dst_indicator: "N",
		id: "31959",
		latitude: 12.960100173950195,
		listing_display: "true",
		longitude: 77.65519714355469,
		name: "Bengaluru",
		pseudonyms: "Bangalore,Bengaluru,Bangalooru",
		region: "TC1",
		timezone: 5.5
	},
	{
		code: "BOM",
		city: "Mumbai",
		country: "India",
		country_code: "IN",
		dst_indicator: "N",
		id: "31961",
		latitude: 19.09119987487793,
		listing_display: "true",
		longitude: 72.86599731445312,
		name: "Chatrapati Shivaji",
		pseudonyms: "Bombay,Mumbai",
		region: "TC1",
		timezone: 5.5
	},
	{
		code: "MAA",
		city: "Chennai",
		country: "India",
		country_code: "IN",
		dst_indicator: "N",
		id: "32047",
		latitude: 12.991100311279297,
		listing_display: "true",
		longitude: 80.16940307617188,
		name: "Chennai",
		pseudonyms: "Chennai,Madras",
		region: "TC1",
		timezone: 5.5
	},
	{
		code: "CCU",
		city: "Kolkata",
		country: "India",
		country_code: "IN",
		dst_indicator: "N",
		id: "31965",
		latitude: 22.645299911499023,
		listing_display: "true",
		longitude: 88.44010162353516,
		name: "Netaji Subhas Chandra Bose",
		pseudonyms: "Kolkota,Calcutta",
		region: "TC1",
		timezone: 5.5
	},
	{
		code: "DEL",
		city: "New Delhi",
		country: "India",
		country_code: "IN",
		dst_indicator: "N",
		id: "31973",
		latitude: 28.563199996948242,
		listing_display: "true",
		longitude: 77.11830139160156,
		name: "Indira Gandhi",
		pseudonyms: "New Delhi,Delhi",
		region: "TC1",
		timezone: 5.5
	},
	{
		code: "HYD",
		city: "Hyderabad",
		country: "India",
		country_code: "IN",
		dst_indicator: "N",
		id: "31993",
		latitude: 17.45199966430664,
		listing_display: "true",
		longitude: 78.46549987792969,
		name: "Shamshabad Rajiv Gandhi",
		pseudonyms: "",
		region: "TC1",
		timezone: 5.5
	},
	{
		code: "PNQ",
		city: "Pune",
		country: "India",
		country_code: "IN",
		dst_indicator: "N",
		id: "32055",
		latitude: 18.581199645996094,
		listing_display: "true",
		longitude: 73.92070007324219,
		name: "Lohegaon",
		pseudonyms: "",
		region: "TC1",
		timezone: 5.5
	}
];

class SearchPlaceScreen extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			searchQuery: "",
			popularCitiesCheck: true,
			airports: airportData
		};
		this.removeData = this.removeData.bind(this);
		this.showModal = this.showModal.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		this.setState({ airports: nextProps.airports, popularCitiesCheck: false });
	}

	componentDidMount() {
		this.showModal();
	}

	shouldComponentUpdate(props, state) {
		console.log(state);
		if (
			state.searchQuery.length > 2 &&
			state.searchQuery.length !== this.state.searchQuery.length
		) {
			this.props.dispatch(
				airportAutocomplete({
					query: state.searchQuery,
					appToken: this.props.appToken,
					location: this.props.location
				})
			);
		}
		return true;
	}

	removeData(arr, code) {
		var returnArr = [];
		for (var i = 0; i < arr.length; i++) {
			if (arr[i].code !== code) {
				returnArr.push(arr[i]);
			}
		}
		return returnArr;
	}

	showModal() {
		var setData = airportData;
		if (this.props.destination.city != null) {
			setData = this.removeData(setData, this.props.destination.code);
		}
		if (this.props.origin.city != null) {
			setData = this.removeData(setData, this.props.origin.code);
		}
		this.setState({ airports: setData });
	}

	render() {
		return (
			<View style={styles.searchCityView}>
				<StatusBar backgroundColor="#535353" barStyle="light-content" />
				<View style={styles.searchCityHeader}>
					<View style={styles.centeredRow}>
						<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
							<View style={styles.goBackButton}>
								<Icon
									iconType={"material"}
									iconSize={height / 24}
									iconName={"arrow-back"}
									iconColor={"#ffffff"}
								/>
							</View>
						</TouchableOpacity>
						<TextInput
							underlineColorAndroid={"transparent"}
							autoFocus={true}
							style={styles.searchQueryTextInput}
							placeholder={I18n.t("search_city_airport")}
							placeholderTextColor={"#bbbbbb"}
							value={this.state.searchQuery}
							onChangeText={text => this.setState({ searchQuery: text })}
						/>
					</View>
					{this.state.searchQuery.length > 0 ? (
						<TouchableOpacity
							onPress={() =>
								this.setState({
									searchQuery: "",
									popularCitiesCheck: true,
									airports: airportData
								})
							}
							style={styles.searchCityClearButton}>
							<Icon
								iconName={"circle-with-cross"}
								iconType={"entypo"}
								iconColor={"#ffffff"}
								iconSize={height / 50}
							/>
						</TouchableOpacity>
					) : (
						<View style={styles.emptyView} />
					)}
				</View>
				{this.state.popularCitiesCheck && (
					<View style={styles.popularCityView}>
						<CleartripTextMedium style={styles.popularCityText}>
							{I18n.t("popular_cities")}
						</CleartripTextMedium>
					</View>
				)}
				<FlatList
					keyboardShouldPersistTaps={"always"}
					data={this.state.airports}
					keyExtractor={(item, index) => index.toString()}
					renderItem={data => (
						<TouchableOpacity
							style={styles.cityNameButton}
							activeOpacity={1}
							onPress={() => {
								this.props.onPressItem(data);
								this.props.navigation.goBack();
							}}>
							<View style={styles.cityNameButtonInnerView}>
								<View style={styles.cityCodeView}>
									<CleartripTextBold style={styles.cityCodeText}>
										{data.item.code}
									</CleartripTextBold>
								</View>
							</View>
							<CleartripTextMedium
								numberOfLines={1}
								ellipsizeMode={"tail"}
								style={styles.cityInfoText}>
								{data.item.city +
									", " +
									data.item.country_code +
									" - " +
									data.item.name +
									" (" +
									data.item.code +
									")"}
							</CleartripTextMedium>
						</TouchableOpacity>
					)}
				/>
			</View>
		);
	}
}

class SearchPlace extends React.Component {
	render() {
		return <SearchPlaceScreen {...this.props} {...this.props.route.params} />;
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		airports:
			state.cleartrip &&
			state.cleartrip.searchFlight &&
			state.cleartrip.searchFlight.airports
				? state.cleartrip.searchFlight.airports
				: null,
		origin: state.cleartrip.searchFlight.origin,
		location: state.location
	};
}

export default connect(mapStateToProps)(SearchPlace);
