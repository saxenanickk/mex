import React from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { Icon } from "../../../../../Components";
import I18n from "../../../Assets/Strings/i18n";
import {
	CleartripTextMedium,
	CleartripTextLight,
	CleartripTextBold,
	CleartripTextRegular
} from "../../../Components/CleartripText";
import styles from "./style";
const { width, height } = Dimensions.get("window");

const FromToRow = props => {
	return (
		<View style={styles.placeContainer}>
			<TouchableOpacity
				activeOpacity={1}
				style={styles.fromButton}
				onPress={() =>
					props.displayPickerMultiCity(false, "origin", props.index)
				}>
				<CleartripTextMedium style={styles.headerText}>
					{props.origin.city != null
						? props.origin.city.toUpperCase()
						: I18n.t("from_caps")}
				</CleartripTextMedium>

				{props.origin.code != null ? (
					<CleartripTextLight style={styles.fromText}>
						{props.origin.code != null ? props.origin.code : "..."}
					</CleartripTextLight>
				) : (
					<CleartripTextBold style={styles.fromText}>
						{props.origin.code != null ? props.origin.code : "..."}
					</CleartripTextBold>
				)}
			</TouchableOpacity>
			<TouchableOpacity
				activeOpacity={1}
				style={styles.fromButton}
				onPress={() =>
					props.displayPickerMultiCity(false, "destination", props.index)
				}>
				<CleartripTextMedium style={styles.headerText}>
					{props.destination.city != null
						? props.destination.city.toUpperCase()
						: I18n.t("to_caps")}
				</CleartripTextMedium>
				{props.destination.code != null ? (
					<CleartripTextLight style={styles.fromText}>
						{props.destination.code != null ? props.destination.code : "..."}
					</CleartripTextLight>
				) : (
					<CleartripTextBold style={styles.fromText}>
						{props.destination.code != null ? props.destination.code : "..."}
					</CleartripTextBold>
				)}
			</TouchableOpacity>
			<TouchableOpacity
				activeOpacity={1}
				style={styles.fromButton}
				onPress={() => props.dateAction(true, props.index)}>
				<CleartripTextMedium style={styles.headerText}>
					{I18n.t("depart_on")}
				</CleartripTextMedium>

				{props.departDate && props.departDate.dd != null ? (
					<CleartripTextRegular style={styles.chooseDateText}>
						{props.getDateToDisplay(
							new Date(
								props.departDate.mm +
									"/" +
									props.departDate.dd +
									"/" +
									props.departDate.yyyy
							)
						)}
					</CleartripTextRegular>
				) : (
					<CleartripTextBold style={styles.fromText}>
						{"..."}{" "}
					</CleartripTextBold>
				)}
			</TouchableOpacity>
			{props.index > 1 && (
				<TouchableOpacity
					activeOpacity={1}
					style={styles.xMarkButton}
					onPress={() => props.xMark(props.index)}>
					<Icon
						iconType={"ionicon"}
						iconName={"ios-close"}
						iconSize={width / 20}
						iconColor={"#666666"}
					/>
				</TouchableOpacity>
			)}
		</View>
	);
};

export default FromToRow;
