import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

const styles = StyleSheet.create({
	class: {
		width: width,
		borderWidth: 0,
		padding: width / 50,
		flexDirection: "row",
		height: height / 12,
		justifyContent: "flex-start"
	},
	Button: {
		flexDirection: "column",
		justifyContent: "center",
		alignItems: "flex-start"
	},
	container: {
		flex: 1,
		backgroundColor: "#ffffff"
	},
	placeContainer: {
		flexDirection: "row",
		height: height / 8,
		justifyContent: "space-between",
		alignItems: "center"
	},
	tripSelectionContainer: {
		height: height / 10,
		flexDirection: "row"
	},
	classSelectionContainer: {
		height: height / 8,
		flexDirection: "column",
		justifyContent: "center"
	},
	travellerSelectionContainer: {
		height: height / 8,
		flexDirection: "column",
		justifyContent: "space-between"
	},
	fromButton: { borderWidth: 0, width: width / 3.56 },
	fromText: {
		color: "#000000",
		fontSize: width / 20
	},
	xMarkButton: {
		position: "absolute",
		width: width / 19.28,
		height: height / 8,
		backgroundColor: "#ececec",
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		right: -width / 26.31
	},
	headerText: {
		color: "#999999",
		fontSize: width / 25
	},
	chooseDateText: {
		color: "#000000",
		fontSize: height / 36
	}
});

export default styles;
