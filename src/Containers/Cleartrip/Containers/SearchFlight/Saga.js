import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api/";
import { NO_NETWORK_ERROR } from "../../Api/";
import { saveItineraryDetails } from "../TravellerDetails/Saga";
import { cleartripNetworkError } from "../../Saga";

/**
 *	Constants.
 */
export const SAVE_ORIGIN = "SAVE_ORIGIN";
export const API_FAILURE = "API_FAILURE";
export const SAVE_AIRPORTS = "SAVE_AIRPORTS";
export const SEARCH_FLIGHTS = "SEARCH_FLIGHTS";
export const SAVE_ITINERARY = "SAVE_ITINERARY";
export const DOMESTIC_FLIGHT = "DOMESTIC_FLIGHT";
export const SAVE_DESTINATION = "SAVE_DESTINATION";
export const CREATE_ITINERARY = "CREATE_ITINERARY";
export const INTERNATIONAL_FLIGHT = "INTERNATIONAL_FLIGHT";
export const MULTICITY_FLIGHT = "MULTICITY_FLIGHT";
export const AIRPORT_AUTOCOMPLETE = "AIRPORT_AUTOCOMPLETE";
export const FETCH_NEARBY_AIRPORTS = "FETCH_NEARBY_AIRPORTS";
export const SAVE_TOTAL_TRAVELLERS = "SAVE_TOTAL_TRAVELLERS";
export const SAVE_AIRLINES_AIRPORTS = "SAVE_AIRLINES_AIRPORTS";
export const SEARCH_FLIGHTS_MULTICITY = "SEARCH_FLIGHTS_MULTICITY";
export const SAVE_SEARCH_FLIGHT_INFORMATION = "SAVE_SEARCH_FLIGHT_INFORMATION";

/**
 *	Action Creators.
 */
export const saveOrigin = payload => ({ type: SAVE_ORIGIN, payload });
export const apiFailure = payload => ({ type: API_FAILURE, payload });
export const saveAirports = payload => ({ type: SAVE_AIRPORTS, payload });
export const searchFlights = payload => ({ type: SEARCH_FLIGHTS, payload });
export const saveItinerary = payload => ({ type: SAVE_ITINERARY, payload });
export const domesticFlight = payload => ({ type: DOMESTIC_FLIGHT, payload });
export const saveDestination = payload => ({ type: SAVE_DESTINATION, payload });
export const createItinerary = payload => ({ type: CREATE_ITINERARY, payload });
export const multiCityFlight = payload => ({ type: MULTICITY_FLIGHT, payload });

export const internationalFlight = payload => ({
	type: INTERNATIONAL_FLIGHT,
	payload
});
export const airportAutocomplete = payload => ({
	type: AIRPORT_AUTOCOMPLETE,
	payload
});
export const fetchNearbyAirports = payload => ({
	type: FETCH_NEARBY_AIRPORTS,
	payload
});
export const saveTotalTravellers = payload => ({
	type: SAVE_TOTAL_TRAVELLERS,
	payload
});
export const saveAirlinesAndAiports = payload => ({
	type: SAVE_AIRLINES_AIRPORTS,
	payload
});
export const searchFlightsMultiCity = payload => ({
	type: SEARCH_FLIGHTS_MULTICITY,
	payload
});
export const saveSearchFlightInformation = payload => ({
	type: SAVE_SEARCH_FLIGHT_INFORMATION,
	payload
});

export function* searchFlightSaga(dispatch) {
	yield takeLatest(SEARCH_FLIGHTS, handleSearchFlights);
	yield takeLatest(SEARCH_FLIGHTS_MULTICITY, handleSearchFlightsMultiCity);
	yield takeLatest(CREATE_ITINERARY, handleCreateItinerary);
	yield takeEvery(AIRPORT_AUTOCOMPLETE, handleAirportAutocomplete);
	yield takeLatest(FETCH_NEARBY_AIRPORTS, handleFetchNearbyAirports);
}

/**
 *	Function for Parsing Flight Key (fk in content key i.e. response of Search Flights Api).
 *	This function returns an object which contains Airline, Flight Number, Time of Flight & Class.
 */
function parseFlightKey(flightKey) {
	return new Promise(function(resolve, reject) {
		try {
			let valueToReturn = [];
			let flightKeyItems = flightKey.split("_");
			for (let i = 0; i < flightKeyItems.length; i++) {
				if (flightKeyItems[i].indexOf("-") >= 0) {
					let flightDetail = flightKeyItems[i].split("-");
					valueToReturn.push({
						airlineName: flightDetail[0],
						flightNumber: flightDetail[1]
					});
				} else if (!isNaN(flightKeyItems[i])) {
					var date = flightKeyItems[i];
					var formattedDate =
						date.slice(0, 2) + "/" + date.slice(2, 4) + "/" + date.slice(4, 8);
					valueToReturn.push(formattedDate);
				} else if (flightKeyItems[i].indexOf(":") >= 0) {
					valueToReturn.push(flightKeyItems[i]);
				} else if (flightKeyItems[i].length === 1) {
					valueToReturn.push(flightKeyItems[i]);
				}
			}
			resolve(valueToReturn);
		} catch (error) {
			reject(error);
		}
	});
}

/**
 * Create a Segment
 */
function createSegment(data) {
	let parsedFlightKey = parseFlightKey(data.fk);
	// console.log(data);
	return {
		airlineName: parsedFlightKey._55[0].airlineName, // Airline Name i.e. Airindia, GoAir, Jetairways etc.
		arrivalTime: data.hasOwnProperty("a") && data.a != null && data.a,
		arrivalDate: data.hasOwnProperty("ad") && data.ad != null && data.ad,
		cabin: parsedFlightKey._55[3], // Economy, Business etc
		dayDifference: data.hasOwnProperty("dd") && data.dd != null && data.dd,
		departureDate: parsedFlightKey._55[1], // Date on which Plane take off.
		duration: data.hasOwnProperty("dr") && data.dr != null && data.dr,
		departureTerminal: data.hasOwnProperty("dt") && data.dt != null && data.dt,
		departureTime: parsedFlightKey._55[2], // Time when Plane take off.
		flightKey: data.hasOwnProperty("fk") && data.fk != null && data.fk,
		flightNumber: parsedFlightKey._55[0].flightNumber, // Flight Number
		from: data.hasOwnProperty("fr") && data.fr != null && data.fr,
		to: data.hasOwnProperty("to") && data.to != null && data.to,
		availability:
			data.hasOwnProperty("sa") && data.sa != null && !(data.sa < 1),
		via: data.hasOwnProperty("v") && data.v != null && data.v
	};
}

/**
 * function for international fare
 */
function getFare(data, fareType) {
	// console.log('getfare',data)
	return {
		type: fareType === "R" ? "Refundable" : "Non-Refundable", // Refundable or Non-Refundable
		basePrice: data.hasOwnProperty("bp") && data.bp != null && data.bp, // Base Price
		cashBack: data.hasOwnProperty("cb") && data.cb != null && data.cb, // Total Cashback
		congestionFee: data.hasOwnProperty("cf") && data.cf != null && data.cf, // Congestion Fee
		convinienceFee:
			data.hasOwnProperty("cfee") && data.cfee != null && data.cfee, // Convinience Fee
		discount: data.hasOwnProperty("ds") && data.ds != null && data.ds, // Discount
		fareBasisCode: data.hasOwnProperty("fb") && data.fb != null && data.fb, // Fare Basis Code
		fareKey: data.hasOwnProperty("fk") && data.fk != null && data.fk, // Fare Key
		fuelSurcharge: data.hasOwnProperty("fs") && data.fs != null && data.fs,
		promoCode: data.hasOwnProperty("pc") && data.pc != null && data.pc, // Promo Code
		serviceFee: data.hasOwnProperty("sf") && data.sf != null && data.sf, // Service Fee
		serviceTax: data.hasOwnProperty("st") && data.st != null && data.st, // Service Tax
		totalPrice: data.hasOwnProperty("pr") && data.pr != null && data.pr, // Total Price
		totalTax: data.hasOwnProperty("t") && data.t != null && data.t, // Total Tax
		UDFArrival:
			data.hasOwnProperty("udf_arr") && data.udf_arr != null && data.udf_arr, // User Dev Fee Arrival
		UDFDeparture:
			data.hasOwnProperty("udf_dep") && data.udf_dep != null && data.udf_dep // User Dev Fee Departure
	};
}

/**
 *	Handler for Fetching Nearby Airports.
 */
function* handleFetchNearbyAirports(action) {
	// console.log(action);
	try {
		let nearbyAirports = yield call(Api.fetchNearbyAirports, action.payload);
		console.log("Nearby Airports Saga Result : ", nearbyAirports);
		yield put(saveAirports(nearbyAirports));
	} catch (error) {
		console.log("Nearby Airports Saga Error: ", error);
	}
}

/**
 *	Handler for Fetching Airports (Autocomplete).
 */
function* handleAirportAutocomplete(action) {
	// console.log(action);
	try {
		let airports = yield call(Api.airportAutocomplete, action.payload);
		console.log("Airports Autocomplete Saga Result : ", airports);
		yield put(saveAirports(airports));
	} catch (error) {
		console.log("Airports Autocomplete Saga Error: ", error);
	}
}

/**
 * Search Flight Handler
 */
function* handleSearchFlights(action) {
	try {
		if (!action.payload) {
			return;
		}
		let flightSearch = yield call(Api.searchFlight, action.payload);
		console.log("Search Flights Saga Result : ", flightSearch);
		let i = 0;
		if (flightSearch.error && flightSearch.errorCode === NO_NETWORK_ERROR) {
			yield put(
				cleartripNetworkError({
					actionType: action.type,
					payload: action.payload
				})
			);
			return;
		}
		if (!flightSearch) {
			console.log("no response from API");
			yield put(apiFailure(true));
			return;
		}
		if (!flightSearch.jsons) {
			console.log("no response from API");
			yield put(apiFailure(true));
			return;
		}
		if (
			typeof flightSearch.mapping === "undefined" ||
			Object.keys(flightSearch.mapping).length === 0
		) {
			console.log("no response from API");
			yield put(apiFailure(true));
			return;
		}

		/**
		 * To Check whether our trip is domestic or international.
		 */
		if (
			flightSearch &&
			flightSearch.jsons &&
			flightSearch.jsons.searchType[action.payload.source] ===
				flightSearch.jsons.searchType[action.payload.destination]
		) {
			/**
			 * Domestic
			 */

			/**
			 * To check whether our trip is one way or round trip.
			 */
			if (!flightSearch.jsons.searchType.roundTrip) {
				/**
				 * One Way
				 */
				let onward = [];

				/**
				 * Loop on whole onward list to get the mapping between content & fare.
				 */
				while (
					flightSearch.mapping &&
					flightSearch.mapping.onward &&
					i < flightSearch.mapping.onward.length
				) {
					let segment = [];

					/**
					 * Creation of Fare object.
					 */
					let farePath =
						flightSearch.fare[flightSearch.mapping.onward[i].f][
							flightSearch.fare[flightSearch.mapping.onward[i].f].dfd
						];
					let fare = getFare(
						farePath,
						flightSearch.fare[flightSearch.mapping.onward[i].f].dfd
					);
					let j = 0;

					/**
					 * Flight data.
					 */
					while (j < flightSearch.mapping.onward[i].c.length) {
						let segmentPath =
							flightSearch.content[flightSearch.mapping.onward[i].c[j]];
						segment.push(createSegment(segmentPath));
						j++;
					}
					onward.push({
						fare: fare,
						segment: segment
					});
					i++;
				}
				yield put(
					saveAirlinesAndAiports({
						airlines: flightSearch.jsons.airline_names,
						airports: flightSearch.jsons.ap
					})
				);
				let adult =
					action.payload.adults > 1
						? action.payload.adults + " Adults"
						: action.payload.adults + " Adult";
				let child =
					action.payload.children !== 0
						? action.payload.children > 1
							? ", " + action.payload.children + " Children"
							: ", " + action.payload.children + " Child"
						: "";
				let infant =
					action.payload.infants !== 0
						? action.payload.infants > 1
							? ", " + action.payload.infants + " Infants"
							: ", " + action.payload.infants + " Infant"
						: "";
				yield put(
					domesticFlight({
						origin: action.payload.source,
						destination: action.payload.destination,
						roundTrip: flightSearch.jsons.searchType.roundTrip,
						passengers: adult + child + infant,
						airlines: flightSearch.jsons.airline_names,
						airports: flightSearch.jsons.ap,
						onward: onward
					})
				);
			} else {
				/**
				 * Round Trip
				 */
				let onward = [];
				let backward = [];

				/**
				 * Loop on whole onward list to get the mapping between content & fare.
				 */
				while (i < flightSearch.mapping.onward.length) {
					let segment = [];
					let splrt = [];

					// console.log(i + " ", flightSearch.fare[flightSearch.mapping.onward[i].f]);
					if (
						flightSearch.fare[flightSearch.mapping.onward[i].f].dfd ===
						undefined
					) {
						i++;
						continue;
					}
					/**
					 * Creation of Fare object.
					 */
					let farePath =
						flightSearch.fare[flightSearch.mapping.onward[i].f][
							flightSearch.fare[flightSearch.mapping.onward[i].f].dfd
						];
					let fare = getFare(
						farePath,
						flightSearch.fare[flightSearch.mapping.onward[i].f].dfd
					);

					/**
					 * To check whether there is special round trip discount available
					 */
					if (
						typeof flightSearch.fare[flightSearch.mapping.onward[i].f].SPLRT !==
						"undefined"
					) {
						let key = Object.keys(
							flightSearch.fare[flightSearch.mapping.onward[i].f].SPLRT
						);
						let k = 0;
						while (k < key.length) {
							let matchedFlights = [];
							let farePath =
								flightSearch.fare[flightSearch.mapping.onward[i].f].SPLRT[
									key[k]
								][
									flightSearch.fare[flightSearch.mapping.onward[i].f].SPLRT[
										key[k]
									].dfd
								];
							let fare = getFare(
								farePath,
								flightSearch.fare[flightSearch.mapping.onward[i].f].SPLRT[
									key[k]
								].dfd
							);
							matchedFlights.push(key[k].split(","));
							splrt.push({
								matchedFlights: matchedFlights,
								fare: fare
							});
							k++;
						}
					}
					let j = 0;

					/**
					 * Flight data.
					 */
					while (j < flightSearch.mapping.onward[i].c.length) {
						let segmentPath =
							flightSearch.content[flightSearch.mapping.onward[i].c[j]];
						segment.push(createSegment(segmentPath));
						j++;
					}
					onward.push({
						fare: fare,
						splrt: splrt,
						segment: segment
					});
					i++;
				}
				i = 0;

				/**
				 * Loop on whole return list to get the mapping between content & fare.
				 */
				while (i < flightSearch.mapping.return.length) {
					let segment = [];
					let splrt = [];
					// console.log(i + " ", flightSearch.fare[flightSearch.mapping.return[i].f]);

					if (
						flightSearch.fare[flightSearch.mapping.return[i].f].dfd ===
						undefined
					) {
						// console.log("BREAK get fare", flightSearch.fare[flightSearch.mapping.return[i].f]);
						i++;
						continue;
					}

					/**
					 * Creation of Fare Object
					 */
					let farePath =
						flightSearch.fare[flightSearch.mapping.return[i].f][
							flightSearch.fare[flightSearch.mapping.return[i].f].dfd
						];
					let fare = getFare(
						farePath,
						flightSearch.fare[flightSearch.mapping.return[i].f].dfd
					);

					/**
					 * To check whether there is special round trip discount available
					 */
					if (
						typeof flightSearch.fare[flightSearch.mapping.return[i].f].SPLRT !==
						"undefined"
					) {
						let key = Object.keys(
							flightSearch.fare[flightSearch.mapping.return[i].f].SPLRT
						);
						let k = 0;
						while (k < key.length) {
							let matchedFlights = [];
							let farePath =
								flightSearch.fare[flightSearch.mapping.return[i].f].SPLRT[
									key[k]
								][
									flightSearch.fare[flightSearch.mapping.return[i].f].SPLRT[
										key[k]
									].dfd
								];
							let fare = getFare(
								farePath,
								flightSearch.fare[flightSearch.mapping.return[i].f].SPLRT[
									key[k]
								].dfd
							);
							matchedFlights.push(key[k].split(","));
							splrt.push({
								matchedFlights: matchedFlights,
								fare: fare
							});
							k++;
						}
					}
					let j = 0;

					/**
					 * Flight data.
					 */
					while (j < flightSearch.mapping.return[i].c.length) {
						let segmentPath =
							flightSearch.content[flightSearch.mapping.return[i].c[j]];
						segment.push(createSegment(segmentPath));
						j++;
					}
					backward.push({
						fare: fare,
						splrt: splrt,
						segment: segment
					});
					i++;
				}
				yield put(
					saveAirlinesAndAiports({
						airlines: flightSearch.jsons.airline_names,
						airports: flightSearch.jsons.ap
					})
				);
				let adult =
					action.payload.adults > 1
						? action.payload.adults + " Adults"
						: action.payload.adults + " Adult";
				let child =
					action.payload.children !== 0
						? action.payload.children > 1
							? ", " + action.payload.children + " Children"
							: ", " + action.payload.children + " Child"
						: "";
				let infant =
					action.payload.infants !== 0
						? action.payload.infants > 1
							? ", " + action.payload.infants + " Infants"
							: ", " + action.payload.infants + " Infant"
						: "";
				yield put(
					domesticFlight({
						origin: action.payload.source,
						destination: action.payload.destination,
						roundTrip: flightSearch.jsons.searchType.roundTrip,
						passengers: adult + child + infant,
						airlines: flightSearch.jsons.airline_names,
						airports: flightSearch.jsons.ap,
						onward: onward,
						backward: backward
					})
				);
			}
		} else {
			/**
			 * International
			 */

			/**
			 * To Check whether trip is one way or round trip
			 */
			if (!flightSearch.jsons.searchType.roundTrip) {
				/**
				 * One Way
				 */
				let onward = [];
				let fare = [];

				/**
				 * Loop on whole onward list to get the mapping between content & fare.
				 */
				while (
					flightSearch.mapping &&
					flightSearch.mapping.onward &&
					i < flightSearch.mapping.onward.length
				) {
					let segment = [];

					if (
						flightSearch.fare[flightSearch.mapping.onward[i].f].dfd ===
						undefined
					) {
						i++;
						continue;
					}

					/**
					 * Creation of Fare object.
					 */
					let farePath =
						flightSearch.fare[flightSearch.mapping.onward[i].f][
							flightSearch.fare[flightSearch.mapping.onward[i].f].dfd
						];
					let faredata = getFare(
						farePath,
						flightSearch.fare[flightSearch.mapping.onward[i].f].dfd
					);
					fare.push({
						fare: faredata
					});
					let j = 0;

					/**
					 * Flight data.
					 */
					while (j < flightSearch.mapping.onward[i].c[0].length) {
						let segmentPath =
							flightSearch.content[flightSearch.mapping.onward[i].c[0][j]];
						segment.push(createSegment(segmentPath));
						j++;
					}
					onward.push({
						fare: faredata,
						segment: segment
					});
					i++;
				}
				yield put(
					saveAirlinesAndAiports({
						airlines: flightSearch.jsons.airline_names,
						airports: flightSearch.jsons.ap
					})
				);
				let adult =
					action.payload.adults > 1
						? action.payload.adults + " Adults"
						: action.payload.adults + " Adult";
				let child =
					action.payload.children !== 0
						? action.payload.children > 1
							? ", " + action.payload.children + " Children"
							: ", " + action.payload.children + " Child"
						: "";
				let infant =
					action.payload.infants !== 0
						? action.payload.infants > 1
							? ", " + action.payload.infants + " Infants"
							: ", " + action.payload.infants + " Infant"
						: "";
				yield put(
					internationalFlight({
						origin: action.payload.source,
						destination: action.payload.destination,
						roundTrip: flightSearch.jsons.searchType.roundTrip,
						passengers: adult + child + infant,
						airlines: flightSearch.jsons.airline_names,
						airports: flightSearch.jsons.ap,
						fare: fare,
						onward: onward
					})
				);
			} else {
				/**
				 * Round Trip
				 */
				let onward = [];
				let backward = [];
				let fare = [];

				/**
				 * Loop on whole onward list to get the mapping between content & fare.
				 */
				while (i < flightSearch.mapping.onward.length) {
					let segment = [];
					let splrt = [];

					if (
						flightSearch.fare[flightSearch.mapping.onward[i].f].dfd ===
						undefined
					) {
						i++;
						continue;
					}

					/**
					 * Creation of Fare Object
					 */
					let farePath =
						flightSearch.fare[flightSearch.mapping.onward[i].f][
							flightSearch.fare[flightSearch.mapping.onward[i].f].dfd
						];
					let faredata = getFare(
						farePath,
						flightSearch.fare[flightSearch.mapping.onward[i].f].dfd
					);

					/**
					 * To check whether there is special round trip discount available
					 */
					if (
						typeof flightSearch.fare[flightSearch.mapping.onward[i].f].SPLRT !==
						"undefined"
					) {
						let key = Object.keys(
							flightSearch.fare[flightSearch.mapping.onward[i].f].SPLRT
						);
						let k = 0;
						while (k < key.length) {
							let matchedFlights = [];
							let farePath =
								flightSearch.fare[flightSearch.mapping.onward[i].f].SPLRT[
									key[k]
								][
									flightSearch.fare[flightSearch.mapping.onward[i].f].SPLRT[
										key[k]
									].dfd
								];
							let fare = getFare(
								farePath,
								flightSearch.fare[flightSearch.mapping.onward[i].f].SPLRT[
									key[k]
								].dfd
							);
							matchedFlights.push(key[k].split(","));
							splrt.push({
								matchedFlights: matchedFlights,
								fare: fare
							});
							k++;
						}
					}
					fare.push({
						fare: faredata,
						splrt: splrt
					});
					let j = 0;

					/**
					 * Loop on whole onward list to get the mapping between content & fare.
					 */
					while (j < flightSearch.mapping.onward[i].c[0].length) {
						let segmentPath =
							flightSearch.content[flightSearch.mapping.onward[i].c[0][j]];
						segment.push(createSegment(segmentPath));
						j++;
					}
					onward.push({
						fare: faredata,
						segment: segment
					});
					j = 0;
					segment = [];

					/**
					 * Loop on whole return list to get the mapping between content & fare.
					 */
					while (j < flightSearch.mapping.onward[i].c[1].length) {
						let segmentPath =
							flightSearch.content[flightSearch.mapping.onward[i].c[1][j]];
						segment.push(createSegment(segmentPath));
						j++;
					}
					backward.push({
						fare: faredata,
						segment: segment
					});
					i++;
				}
				yield put(
					saveAirlinesAndAiports({
						airlines: flightSearch.jsons.airline_names,
						airports: flightSearch.jsons.ap
					})
				);
				let adult =
					action.payload.adults > 1
						? action.payload.adults + " Adults"
						: action.payload.adults + " Adult";
				let child =
					action.payload.children !== 0
						? action.payload.children > 1
							? ", " + action.payload.children + " Children"
							: ", " + action.payload.children + " Child"
						: "";
				let infant =
					action.payload.infants !== 0
						? action.payload.infants > 1
							? ", " + action.payload.infants + " Infants"
							: ", " + action.payload.infants + " Infant"
						: "";
				yield put(
					internationalFlight({
						origin: action.payload.source,
						destination: action.payload.destination,
						roundTrip: flightSearch.jsons.searchType.roundTrip,
						passengers: adult + child + infant,
						airlines: flightSearch.jsons.airline_names,
						airports: flightSearch.jsons.ap,
						fare: fare,
						onward: onward,
						backward: backward
					})
				);
			}
		}
	} catch (error) {
		console.log("Search Flights Saga Error: ", error);
		yield put(apiFailure(true));
	}
}

/**
 * Search Flight Handler MultiCity
 */
function* handleSearchFlightsMultiCity(action) {
	console.log("searchFlightMulticity Parameter", action);
	try {
		if (!action.payload) {
			return;
		}
		let flightSearch = yield call(Api.searchFlightMultiCity, action.payload);
		console.log("Search Flights Saga Result : ", flightSearch);
		if (
			flightSearch &&
			flightSearch.hasOwnProperty("error") &&
			flightSearch.errorCode === NO_NETWORK_ERROR
		) {
			yield put(
				cleartripNetworkError({
					actionType: action.type,
					payload: action.payload
				})
			);
			return;
		}
		if (!flightSearch) {
			yield put(apiFailure(true));
			return;
		}
		if (!flightSearch.jsons) {
			yield put(apiFailure(true));
			return;
		}

		if (
			typeof flightSearch.mapping === "undefined" ||
			Object.keys(flightSearch.mapping).length === 0
		) {
			console.log("no response from API");
			yield put(apiFailure(true));
			return;
		}

		let x = 0;
		let mainMultiCityObject = {};
		while (x < action.payload.multicity.length) {
			let source = action.payload.multicity[x].origin.code;
			let destination = action.payload.multicity[x].destination.code;
			var multicity = {};
			var key = source + "_" + destination;
			multicity.key = [];
			let i = 0;
			/**
			 * To Check whether our trip is domestic or international.
			 */
			if (
				flightSearch.jsons &&
				flightSearch.jsons.searchType[source] ===
					flightSearch.jsons.searchType[destination]
			) {
				/**
				 * Domestic
				 */
				let items = [];
				/**
				 * Loop on whole onward list to get the mapping between content & fare.
				 */
				// console.log(flightSearch.mapping);
				while (i < flightSearch.mapping[key].length) {
					let segment = [];

					/**
					 * Creation of Fare object.
					 */
					let farePath =
						flightSearch.fare[flightSearch.mapping[key][i].f][
							flightSearch.fare[flightSearch.mapping[key][i].f].dfd
						];
					let fare = getFare(
						farePath,
						flightSearch.fare[flightSearch.mapping[key][i].f].dfd
					);
					let j = 0;

					/**
					 * Flight data.
					 */
					while (j < flightSearch.mapping[key][i].c.length) {
						let segmentPath =
							flightSearch.content[flightSearch.mapping[key][i].c[j]];
						segment.push(createSegment(segmentPath));
						j++;
					}
					items.push({
						fare: fare,
						segment: segment
					});
					i++;
				}
				multicity.roundTrip = flightSearch.jsons.searchType.roundTrip;
				multicity.passengers = flightSearch.passengers;
				multicity.airline_names = flightSearch.jsons.airline_names;
				multicity.roundTrip = flightSearch.jsons.searchType.roundTrip;
				multicity.items = items;
				mainMultiCityObject[key] = multicity;
			} else {
				/**
				 * International
				 */

				/**
				 * To Check whether trip is one way or round trip
				 */
				/**
				 * One Way
				 */
				// let onward = [];
				let fare = [];
				let items = [];
				/**
				 * Loop on whole onward list to get the mapping between content & fare.
				 */
				while (i < flightSearch.mapping.onward.length) {
					let segment = {};
					/**
					 * Creation of Fare object.
					 */
					let farePath =
						flightSearch.fare[flightSearch.mapping.onward[i].f][
							flightSearch.fare[flightSearch.mapping.onward[i].f].dfd
						];
					let faredata = getFare(
						farePath,
						flightSearch.fare[flightSearch.mapping.onward[i].f].dfd
					);

					let j = 0;
					while (j < flightSearch.mapping.onward[i].c.length) {
						let k = 0;
						let tempSegment = [];
						while (k < flightSearch.mapping.onward[i].c[j].length) {
							let segmentPath =
								flightSearch.content[flightSearch.mapping.onward[i].c[j][k]];
							tempSegment.push(createSegment(segmentPath));
							k++;
						}
						segment[j] = tempSegment;
						j++;
					}
					items.push({
						segment: segment,
						fare: faredata
					});
					i++;
				}
				multicity.roundTrip = flightSearch.jsons.searchType.roundTrip;
				multicity.isInternational = true;
				multicity.airline_names = flightSearch.jsons.airline_names;
				multicity.roundTrip = flightSearch.jsons.searchType.roundTrip;
				multicity.items = items;
				yield put(
					saveAirlinesAndAiports({
						airlines: flightSearch.jsons.airline_names,
						airports: flightSearch.jsons.ap
					})
				);
				yield put(
					multiCityFlight({
						airports: flightSearch.jsons.ap,
						cities: action.payload.multicity,
						multicity: multicity
					})
				);
				return;
			}
			// console.log(x);
			x++;
		}
		// console.log("MULTI CITY");
		// console.log(mainMultiCityObject);
		yield put(
			saveAirlinesAndAiports({
				airlines: flightSearch.jsons.airline_names,
				airports: flightSearch.jsons.ap
			})
		);
		yield put(
			multiCityFlight({
				airports: flightSearch.jsons.ap,
				cities: action.payload.multicity,
				multicity: mainMultiCityObject
			})
		);
	} catch (error) {
		console.log("Search Flights Saga Error: ", error);
		if (error instanceof SyntaxError) {
			yield put(apiFailure(true));
		}
	}
}

/**
 *	Handler for Creating Itinerary.
 */
function* handleCreateItinerary(action) {
	// console.log(action);
	try {
		let itinerary = yield call(Api.createItinerary, action.payload);
		console.log("Create Itinerary Saga Result : ", itinerary);
		if (
			itinerary.hasOwnProperty("error") &&
			itinerary.errorCode === NO_NETWORK_ERROR
		) {
			yield put(
				cleartripNetworkError({
					actionType: action.type,
					payload: action.payload
				})
			);
			return;
		}
		if (itinerary.hasOwnProperty("error_message")) {
			yield put(apiFailure(true));
		} else {
			yield put(saveItineraryDetails({ response: itinerary }));
		}
	} catch (error) {
		console.log("Create Itinerary Saga Error: ", error);
		yield put(apiFailure(true));
	}
}
