import React, { Component } from "react";
import { View, Dimensions, TouchableOpacity, StatusBar } from "react-native";
import I18n from "../../../Assets/Strings/i18n";
import {
	CleartripTextRegular,
	CleartripTextMedium
} from "../../../Components/CleartripText";
import { Icon } from "../../../../../Components";
import styles from "./style";
const { height, width } = Dimensions.get("window");

const INCREMENT = true;
const DECREMENT = false;
export default class Traveller extends Component {
	constructor(props) {
		super(props);

		const { route } = props;
		const {
			adults = 1,
			children = 0,
			infants = 0,
			saveTravellerDetail = () => {}
		} = route.params ? route.params : {};

		this.saveTravellerDetail = saveTravellerDetail;

		this.state = {
			adults: adults,
			children: children,
			infants: infants
		};
	}

	isAdultDisable = (count, type) => {
		if (type === INCREMENT && count === 9) {
			return true;
		} else if (type === DECREMENT && count === 1) {
			return true;
		} else {
			return false;
		}
	};

	changeAdult = type => {
		if (type === INCREMENT) {
			if (this.state.adults + this.state.children === 9) {
				this.setState({
					adults: this.state.adults + 1,
					children: this.state.children - 1
				});
			} else {
				this.setState({ adults: this.state.adults + 1 });
			}
		} else {
			if (this.state.adults === this.state.infants) {
				this.setState({
					adults: this.state.adults - 1,
					infants: this.state.infants - 1
				});
			} else {
				this.setState({ adults: this.state.adults - 1 });
			}
		}
	};

	isChildDisable = (count, type) => {
		if (type === INCREMENT && count === 9) {
			return true;
		} else if (type === DECREMENT && count === 0) {
			return true;
		} else {
			return false;
		}
	};

	changeChild = type => {
		if (type === INCREMENT) {
			this.setState({ children: this.state.children + 1 });
		} else {
			this.setState({ children: this.state.children - 1 });
		}
	};

	isInfantDisable = (count, type) => {
		if (type === INCREMENT && count === this.state.adults) {
			return true;
		} else if (type === DECREMENT && count === 0) {
			return true;
		} else {
			return false;
		}
	};

	changeInfant = type => {
		if (type === INCREMENT) {
			this.setState({ infants: this.state.infants + 1 });
		} else {
			this.setState({ infants: this.state.infants - 1 });
		}
	};
	render() {
		return (
			<View style={styles.container}>
				<StatusBar backgroundColor="#535353" barStyle="light-content" />
				<View style={styles.header}>
					<View style={styles.backButtonContainer}>
						<TouchableOpacity
							style={styles.backButton}
							onPress={() => {
								this.saveTravellerDetail(false, {});
								this.props.navigation.goBack();
							}}>
							<Icon
								iconType={"material"}
								iconSize={height / 26}
								iconName={"arrow-back"}
								iconColor={"#ffffff"}
							/>
						</TouchableOpacity>
						<CleartripTextMedium style={styles.roomAndGuestsText}>
							{I18n.t("travellers")}
						</CleartripTextMedium>
					</View>
					<TouchableOpacity
						style={styles.backButton}
						onPress={() => {
							this.saveTravellerDetail(true, {
								adults: this.state.adults,
								children: this.state.children,
								infants: this.state.infants
							});
							this.props.navigation.goBack();
						}}>
						<Icon
							iconType={"material"}
							iconSize={height / 26}
							iconName={"check"}
							iconColor={"#ffffff"}
						/>
					</TouchableOpacity>
				</View>
				<View style={styles.roomItemContainer}>
					<View style={styles.roomBar}>
						<CleartripTextRegular style={styles.roomText}>
							{I18n.t("select_travellers")}
						</CleartripTextRegular>
					</View>
					<View style={styles.bar}>
						<CleartripTextRegular style={styles.text}>
							{I18n.t("adults")}
						</CleartripTextRegular>
						<View style={styles.buttonBar}>
							<TouchableOpacity
								style={styles.button}
								disabled={this.isAdultDisable(this.state.adults, DECREMENT)}
								onPress={() => {
									this.changeAdult(DECREMENT);
								}}>
								<View
									style={[
										styles.increment,
										{
											backgroundColor: !this.isAdultDisable(
												this.state.adults,
												DECREMENT
											)
												? "#777777"
												: "#DFDDE0"
										}
									]}>
									<Icon
										iconType={"ionicon"}
										iconSize={width / 30}
										iconName={"md-remove"}
										iconColor={"#ffffff"}
									/>
								</View>
							</TouchableOpacity>
							<CleartripTextRegular style={styles.text}>
								{this.state.adults}
							</CleartripTextRegular>
							<TouchableOpacity
								style={styles.button}
								disabled={this.isAdultDisable(this.state.adults, INCREMENT)}
								onPress={() => {
									this.changeAdult(INCREMENT);
								}}>
								<View
									style={[
										styles.increment,
										{
											backgroundColor: !this.isAdultDisable(
												this.state.adults,
												INCREMENT
											)
												? "#777777"
												: "#DFDDE0"
										}
									]}>
									<Icon
										iconType={"ionicon"}
										iconSize={width / 30}
										iconName={"md-add"}
										iconColor={"#ffffff"}
									/>
								</View>
							</TouchableOpacity>
						</View>
					</View>
					<View style={styles.bar}>
						<CleartripTextRegular style={styles.text}>
							{I18n.t("children")}
						</CleartripTextRegular>
						<View style={styles.buttonBar}>
							<TouchableOpacity
								style={styles.button}
								disabled={this.isChildDisable(this.state.children, DECREMENT)}
								onPress={() => {
									this.changeChild(DECREMENT);
								}}>
								<View
									style={[
										styles.increment,
										{
											backgroundColor: !this.isChildDisable(
												this.state.children,
												DECREMENT
											)
												? "#777777"
												: "#DFDDE0"
										}
									]}>
									<Icon
										iconType={"ionicon"}
										iconSize={width / 30}
										iconName={"md-remove"}
										iconColor={"#ffffff"}
									/>
								</View>
							</TouchableOpacity>
							<CleartripTextRegular style={styles.text}>
								{this.state.children}
							</CleartripTextRegular>
							<TouchableOpacity
								style={styles.button}
								disabled={this.isChildDisable(
									this.state.children + this.state.adults,
									INCREMENT
								)}
								onPress={() => {
									this.changeChild(INCREMENT);
								}}>
								<View
									style={[
										styles.increment,
										{
											backgroundColor: !this.isChildDisable(
												this.state.children + this.state.adults,
												INCREMENT
											)
												? "#777777"
												: "#DFDDE0"
										}
									]}>
									<Icon
										iconType={"ionicon"}
										iconSize={width / 30}
										iconName={"md-add"}
										iconColor={"#ffffff"}
									/>
								</View>
							</TouchableOpacity>
						</View>
					</View>
					<View style={styles.bar}>
						<CleartripTextRegular style={styles.text}>
							{I18n.t("infants")}
						</CleartripTextRegular>
						<View style={styles.buttonBar}>
							<TouchableOpacity
								style={styles.button}
								disabled={this.isInfantDisable(this.state.infants, DECREMENT)}
								onPress={() => {
									this.changeInfant(DECREMENT);
								}}>
								<View
									style={[
										styles.increment,
										{
											backgroundColor: !this.isInfantDisable(
												this.state.infants,
												DECREMENT
											)
												? "#777777"
												: "#DFDDE0"
										}
									]}>
									<Icon
										iconType={"ionicon"}
										iconSize={width / 30}
										iconName={"md-remove"}
										iconColor={"#ffffff"}
									/>
								</View>
							</TouchableOpacity>
							<CleartripTextRegular style={styles.text}>
								{this.state.infants}
							</CleartripTextRegular>
							<TouchableOpacity
								style={styles.button}
								disabled={this.isInfantDisable(this.state.infants, INCREMENT)}
								onPress={() => {
									this.changeInfant(INCREMENT);
								}}>
								<View
									style={[
										styles.increment,
										{
											backgroundColor: !this.isInfantDisable(
												this.state.infants,
												INCREMENT
											)
												? "#777777"
												: "#DFDDE0"
										}
									]}>
									<Icon
										iconType={"ionicon"}
										iconSize={width / 30}
										iconName={"md-add"}
										iconColor={"#ffffff"}
									/>
								</View>
							</TouchableOpacity>
						</View>
					</View>
				</View>
			</View>
		);
	}
}
