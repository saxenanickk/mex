import {
	API_FAILURE,
	SAVE_ORIGIN,
	SAVE_AIRPORTS,
	SAVE_ITINERARY,
	DOMESTIC_FLIGHT,
	MULTICITY_FLIGHT,
	SAVE_DESTINATION,
	INTERNATIONAL_FLIGHT,
	SAVE_TOTAL_TRAVELLERS,
	SAVE_SELECTED_FLIGHTS,
	SAVE_AIRLINES_AIRPORTS,
	SEARCH_FLIGHTS_MULTICITY,
	SAVE_SEARCH_FLIGHT_INFORMATION
} from "./Saga";

const initialState = {
	adults: 1,
	infants: 0,
	flights: [],
	children: 0,
	cities: null,
	origin: null,
	failure: null,
	airports: null,
	airlines: null,
	domestic: null,
	itinerary: null,
	multicity: null,
	searchInfo: null,
	destination: null,
	international: null,
	multicityAirports: null,
	networkError: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case SAVE_ORIGIN:
			return {
				...state,
				origin: action.payload
			};
		case API_FAILURE:
			return {
				...state,
				failure: action.payload
			};
		case SAVE_AIRPORTS:
			return {
				...state,
				airports: action.payload
			};
		case SAVE_DESTINATION:
			return {
				...state,
				destination: action.payload
			};
		case SAVE_TOTAL_TRAVELLERS:
			return {
				...state,
				adults: action.payload.adults,
				children: action.payload.children,
				infants: action.payload.infants
			};
		case DOMESTIC_FLIGHT:
			return {
				...state,
				domestic: action.payload,
				failure: !action.payload
			};
		case MULTICITY_FLIGHT:
			return {
				...state,
				multicity: action.payload ? action.payload.multicity : null,
				cities: action.payload ? action.payload.cities : null,
				multicityAirports: action.payload ? action.payload.airports : null,
				failure: action.payload ? false : true
			};
		case INTERNATIONAL_FLIGHT:
			return {
				...state,
				international: action.payload,
				failure: !action.payload
			};
		case SAVE_AIRLINES_AIRPORTS:
			return {
				...state,
				airlines: action.payload
			};
		case SAVE_ITINERARY:
			return {
				...state,
				itinerary: action.payload
			};
		case SEARCH_FLIGHTS_MULTICITY:
			return {
				...state,
				failure: !action.payload
			};
		case SAVE_SELECTED_FLIGHTS:
			console.log("FAULT");
			return {
				...state
			};
		case SAVE_SEARCH_FLIGHT_INFORMATION:
			console.log("save search flight", action.payload);
			return {
				...state,
				searchInfo: action.payload
			};
		default:
			return state;
	}
};
