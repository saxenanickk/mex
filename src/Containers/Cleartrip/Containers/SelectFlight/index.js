import React, { Component } from "react";
import {
	Alert,
	ScrollView,
	View,
	FlatList,
	Dimensions,
	TouchableOpacity,
	BackHandler,
	TouchableWithoutFeedback,
	Image,
	LayoutAnimation,
	NativeModules,
	Platform
} from "react-native";
import { connect } from "react-redux";
import { saveSelectedFlights } from "./Saga";
import SingleSearchItem from "./SingleSearchItem";
import {
	domesticFlight,
	internationalFlight,
	multiCityFlight,
	apiFailure,
	createItinerary
} from "../SearchFlight/Saga";
import {
	ProgressScreen,
	Seperator,
	Icon,
	DialogModal
} from "../../../../Components";
import ProgressBar from "../../Components/ProgressBar";
import FlightInfo from "./FlightInfo";
import {
	FILTERAPPLIED,
	EARLYMORNING,
	EVENING,
	NORESULT,
	RIGHTARROW,
	NONSTOP,
	ROUNDTRIPARROW
} from "../../Assets/Img/Image";
import {
	CleartripTextRegular,
	CleartripTextMedium,
	CleartripTextBold
} from "../../Components/CleartripText";
import I18n from "../../Assets/Strings/i18n";
import styles from "./style";
import { GoToast } from "../../../../Components";

const { width, height } = Dimensions.get("window");
const { UIManager } = NativeModules;

if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}
class SelectFlight extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showSpinner: false,
			selectedFlight: null,
			finalFare: 0,
			flightsData: [],
			flightInfo: false,
			filterModal: false,
			nonStop: false,
			earlyMorning: false,
			morning: false,
			midDay: false,
			evening: false,
			night: false,
			hideMultiAirline: false,
			filteredAirlines: [],
			allFiltersModal: false,
			itemToDisplay: true,
			progress: 10,
			specialFare: 0,
			direction: false
		};
		this.Month = [
			"Jan",
			"Feb",
			"Mar",
			"Apr",
			"May",
			"Jun",
			"Jul",
			"Aug",
			"Sep",
			"Oct",
			"Nov",
			"Dec"
		];
		this.filtersApplied = false;
		this.counter = 0;
		this.onNoItemToDisplay = this.onNoItemToDisplay.bind(this);
		this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
		this.calculateDuration = this.calculateDuration.bind(this);
		this.getDuration = this.getDuration.bind(this);
		this.pushDataToState = this.pushDataToState.bind(this);
		this.fillTravellerDetails = this.fillTravellerDetails.bind(this);
		this.updateFinalFare = this.updateFinalFare.bind(this);
		this.disposeFlightInfo = this.disposeFlightInfo.bind(this);
		this.continueButton = this.continueButton.bind(this);
		this.onClickNonStop = this.onClickNonStop.bind(this);
		this.onClickEarlyMorning = this.onClickEarlyMorning.bind(this);
		this.onClickEvening = this.onClickEvening.bind(this);
		this.onClickReset = this.onClickReset.bind(this);
		this.onAllFilterModal = this.onAllFilterModal.bind(this);
		this.setDirection = this.setDirection.bind(this);
	}

	componentWillMount() {
		BackHandler.addEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
		let self = this;
		this.interval = setInterval(function() {
			if (self.state.progress < 90) {
				self.setState({
					progress: self.state.progress + 3
				});
			}
		}, 1000);
	}

	shouldComponentUpdate(props, state) {
		//console.log(props, state)
		if (props.failure) {
			GoToast.show(I18n.t("cant_process_request"), I18n.t("information"));
			this.props.navigation.goBack();
			return true;
		}
		if (
			props.itineraryDetails &&
			props.itineraryDetails !== this.props.itineraryDetails
		) {
			let response = props.itineraryDetails;
			if (response.error_message) {
				this.setState({ showSpinner: false });
				if (response.error_code) {
					/**
					 * Case where fare has changed
					 */
					GoToast.show(response.error_message, I18n.t("error"));
					if (response.error_code === 513) {
						const responseErrorMsgArray = response.error_message.split(".");
						const changedFare = response.error_message.match(/[0-9]+\.[0-9]*/g);
						if (
							responseErrorMsgArray.length > 0 &&
							changedFare !== null &&
							changedFare.length >= 2
						) {
							Alert.alert(
								`${I18n.t("fare_changed")} ₹ ${changedFare[0]} to ₹ ${
									changedFare[1]
								}`,
								I18n.t("fare_change_query"),
								[
									{
										text: I18n.t("no"),
										onPress: () => this.props.navigation.goBack(),
										style: "cancel"
									},
									{
										text: I18n.t("yes"),
										onPress: () => {
											const updatedParams = this.createBookingOptions(
												changedFare[1]
											);
											this.makeCreateItineraryApiCall(updatedParams);
										}
									}
								],
								{ cancelable: false }
							);
						} else {
							GoToast.show(
								I18n.t("cant_process_request"),
								I18n.t("information")
							);
							this.props.navigation.goBack();
						}
					} else if (response.error_code === 502) {
						// Input Validation error alert(Tested for mobile no.)
						alert(response.error_message);
					} else {
						this.props.navigation.goBack();
					}
				}
			} else {
				this.setState({
					showSpinner: false,
					flightInfo: true,
					currentFlightDetail: this.state.flightData,
					fareBrakup: false
				});
			}
		}
		return true;
	}

	componentWillUnmount() {
		if (this.props.domestic !== null) {
			this.props.dispatch(domesticFlight(null));
		} else if (this.props.multicity != null) {
			this.props.dispatch(multiCityFlight(null));
		} else {
			this.props.dispatch(internationalFlight(null));
		}
		this.props.dispatch(apiFailure(null));
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
		clearInterval(this.interval);
	}

	componentWillReceiveProps(props) {
		/**
		 * Check to show toast when API is not failed but no data is returned
		 */
		if (
			!props.isSearching &&
			!props.failure &&
			(props.domestic === null ||
				(props.domestic.onward && props.domestic.onward.length < 1) ||
				(props.domestic.backward && props.domestic.backward.length < 1)) &&
			(props.international === null ||
				(props.international.onward && props.international.onward.length < 1) ||
				(props.international.backward &&
					props.international.backward.length < 1)) &&
			props.multicity === null
		) {
			GoToast.show(
				I18n.t("no_flight_change_date"),
				I18n.t("information"),
				"LONG"
			);
			props.navigation.goBack();
		}
	}
	/**
	 * Back Button Handler
	 */
	handleBackButtonClick() {
		if (this.state.flightInfo) {
			this.setState({ flightInfo: false });
		} else {
			this.props.navigation.goBack();
		}
		return true;
	}

	continueButton() {
		this.props.dispatch(
			saveSelectedFlights({
				isInternational: this.props.isInternational,
				isMultiCity: this.props.isMultiCity,
				isRoundTrip: this.props.isRoundTrip,
				data: this.state.flightsData
			})
		);
		this.fillTravellerDetails();
	}

	updateFinalFare() {
		var dat = this.state.flightsData;
		if (dat == null || dat === undefined) {
			dat = this.props.defaultData;
		}
		if (!dat) {
			return;
		}

		let data = dat != null ? dat : this.state.flightsData;
		var price = 0;
		for (var i = 0; i < data.length; i++) {
			if (typeof data[i] !== "undefined") {
				price += parseInt(data[i].fare.totalPrice);
			}
		}
		this.setState({
			finalFare: price,
			flightsData: data
		});
	}

	pushDataToState(value, index) {
		var flightsData = this.state.flightsData;
		let updateSum = 0;
		let checkingIndex = index === 1 ? 0 : 1;
		if (this.props.isRoundTrip && this.state.flightsData.length === 2) {
			if (
				flightsData[checkingIndex].splrt &&
				value.splrt &&
				flightsData[checkingIndex].splrt.length === 1 &&
				value.splrt.length === 1 &&
				flightsData[checkingIndex].splrt[0].matchedFlights[0].indexOf(
					value.segment[0].flightNumber
				) > -1 &&
				flightsData[checkingIndex].segment[0].airlineName ===
					value.segment[0].airlineName
			) {
				updateSum =
					flightsData[checkingIndex].splrt[0].fare.totalPrice +
					value.splrt[0].fare.totalPrice;
			}
		}
		flightsData[index] = value && value.item ? value.item : value;
		this.setState({ flightsData: flightsData, specialFare: updateSum });
		this.updateFinalFare();
	}

	/**
	 * function to set the direction of the lis
	 */
	setDirection = direction => {
		LayoutAnimation.easeInEaseOut();
		this.setState({ direction: direction });
	};

	getFare = (index, keyName) => {
		console.log("flights data in state", this.state.flightsData);
		if (this.props.isRoundTrip && this.state.flightsData.length === 2) {
			let flightsData = this.state.flightsData;
			if (
				flightsData[0].splrt &&
				flightsData[1].splrt &&
				flightsData[0].splrt.length === 1 &&
				flightsData[1].splrt.length === 1 &&
				flightsData[0].splrt[0].matchedFlights[0].indexOf(
					flightsData[1].segment[0].flightNumber
				) > -1 &&
				flightsData[0].segment[0].airlineName ===
					flightsData[1].segment[0].airlineName
			) {
				return flightsData[index].splrt[0].fare[keyName];
			}
		}
		let flight = this.state.flightsData[index];
		return flight.fare[keyName];
	};

	fillTravellerDetails(flightDetail) {
		let params = this.createBookingOptions();
		this.makeCreateItineraryApiCall(params);
	}

	createBookingOptions(updatedFare = null) {
		let options = {};
		var continuer;
		//console.log("createBookingOptions function:")
		//console.log(this.state.flightsData)
		let data = this.state.flightsData;
		if (this.props.isMultiCity) {
			if (this.props.isInternational) {
				let flight = data[0];
				options.appToken = this.props.appToken;
				options.FAREDETAILS = [];
				options.FLIGHTS = [];
				options.FAREDETAILS[0] = {
					AMOUNT: updatedFare ? updatedFare : flight.fare.totalPrice,
					FAREKEY: flight.fare.fareKey
				};
				options.CABINTYPE = flight.segment[0][0].cabin;
				var mainCtr = 1;
				for (
					let index = 0;
					index < Object.keys(flight.segment).length;
					index++
				) {
					options.FLIGHTS.push({
						SEGMENTS: {}
					});
					for (let y = 0; y < flight.segment[index].length; y++, mainCtr++) {
						const element = flight.segment[index][y];
						let departureDate = element.departureDate
							.replace("/", "")
							.replace("/", "");
						let departureDateFormatted =
							departureDate.slice(4) +
							"-" +
							departureDate.slice(2, 4) +
							"-" +
							departureDate.slice(0, 2);
						options.FLIGHTS[index].SEGMENTS[mainCtr] = {
							DEPARTUREAIRPORT: element.from,
							ARRIVALAIRPORT: element.to,
							FLIGHTNUMBER: element.flightNumber,
							AIRLINE: element.airlineName,
							OPERATINGAIRLINE: element.airlineName,
							DEPARTUREDATE: departureDateFormatted
						};
					}
				}
			} else {
				options.appToken = this.props.appToken;
				options.FAREDETAILS = [];
				options.FLIGHTS = [];
				for (var x = 0; x < data.length; x++) {
					let flight = data[x];
					options.CABINTYPE = flight.segment[0].cabin;
					options.FAREDETAILS[x] = {
						AMOUNT: updatedFare ? updatedFare : flight.fare.totalPrice,
						FAREKEY: flight.fare.fareKey
					};
					options.FLIGHTS.push({
						SEGMENTS: {}
					});
					var segmentCtr = 1;
					for (
						let index = 1;
						index < flight.segment.length + 1;
						index++, segmentCtr++
					) {
						const element = flight.segment[index - 1];
						let departureDate = element.departureDate
							.replace("/", "")
							.replace("/", "");
						let departureDateFormatted =
							departureDate.slice(4) +
							"-" +
							departureDate.slice(2, 4) +
							"-" +
							departureDate.slice(0, 2);

						options.FLIGHTS[x].SEGMENTS[segmentCtr] = {
							DEPARTUREAIRPORT: element.from,
							ARRIVALAIRPORT: element.to,
							FLIGHTNUMBER: element.flightNumber,
							AIRLINE: element.airlineName,
							OPERATINGAIRLINE: element.airlineName,
							DEPARTUREDATE: departureDateFormatted
						};
					}
				}
			}
		} else if (this.props.isInternational) {
			let isRoundTrip = this.props.isRoundTrip;
			let flight = data[0];
			options.appToken = this.props.appToken;
			options.CABINTYPE = flight.segment[0].cabin;
			options.FAREDETAILS = [];
			options.FAREDETAILS[0] = {
				AMOUNT: updatedFare ? updatedFare : flight.fare.totalPrice,
				FAREKEY: flight.fare.fareKey
			};
			options.FLIGHTS = [{}];
			options.FLIGHTS[0].SEGMENTS = {};
			let index;
			for (index = 1; index < flight.segment.length + 1; index++) {
				const element = flight.segment[index - 1];
				let departureDate = element.departureDate
					.replace("/", "")
					.replace("/", "");
				let departureDateFormatted =
					departureDate.slice(4) +
					"-" +
					departureDate.slice(2, 4) +
					"-" +
					departureDate.slice(0, 2);
				//console.log("index", index)
				options.FLIGHTS[0].SEGMENTS[index] = {
					DEPARTUREAIRPORT: element.from,
					ARRIVALAIRPORT: element.to,
					FLIGHTNUMBER: element.flightNumber,
					AIRLINE: element.airlineName,
					OPERATINGAIRLINE: element.airlineName,
					DEPARTUREDATE: departureDateFormatted
				};
			}

			if (isRoundTrip) {
				let returnFlight = data[1];
				options.FLIGHTS.push({});
				options.FLIGHTS[1].SEGMENTS = {};
				continuer = index;
				for (let index = 1; index < returnFlight.segment.length + 1; index++) {
					const element = returnFlight.segment[index - 1];
					let departureDate = element.departureDate
						.replace("/", "")
						.replace("/", "");
					let departureDateFormatted =
						departureDate.slice(4) +
						"-" +
						departureDate.slice(2, 4) +
						"-" +
						departureDate.slice(0, 2);
					options.FLIGHTS[1].SEGMENTS[continuer] = {
						DEPARTUREAIRPORT: element.from,
						ARRIVALAIRPORT: element.to,
						FLIGHTNUMBER: element.flightNumber,
						AIRLINE: element.airlineName,
						OPERATINGAIRLINE: element.airlineName,
						DEPARTUREDATE: departureDateFormatted
					};
				}
			}
		} else {
			let isRoundTrip = this.props.isRoundTrip;
			let flight = data[0];
			//console.log(flight)
			options.appToken = this.props.appToken;
			options.CABINTYPE = flight.segment[0].cabin;
			options.FAREDETAILS = [];
			options.FAREDETAILS[0] = {
				AMOUNT: updatedFare ? updatedFare : this.getFare(0, "totalPrice"),
				FAREKEY: this.getFare(0, "fareKey")
			};
			options.FLIGHTS = [{}];
			options.FLIGHTS[0].SEGMENTS = {};
			let index;
			for (index = 1; index < flight.segment.length + 1; index++) {
				const element = flight.segment[index - 1];
				let departureDate = element.departureDate
					.replace("/", "")
					.replace("/", "");
				let departureDateFormatted =
					departureDate.slice(4) +
					"-" +
					departureDate.slice(2, 4) +
					"-" +
					departureDate.slice(0, 2);

				options.FLIGHTS[0].SEGMENTS[index] = {
					DEPARTUREAIRPORT: element.from,
					ARRIVALAIRPORT: element.to,
					FLIGHTNUMBER: element.flightNumber,
					AIRLINE: element.airlineName,
					OPERATINGAIRLINE: element.airlineName,
					DEPARTUREDATE: departureDateFormatted
				};
			}
			if (isRoundTrip) {
				let returnFlight = data[1];
				options.FAREDETAILS[1] = {
					AMOUNT: updatedFare ? updatedFare : this.getFare(1, "totalPrice"),
					FAREKEY: this.getFare(1, "fareKey")
				};
				options.FLIGHTS.push({});
				options.FLIGHTS[1].SEGMENTS = {};
				continuer = index;
				for (
					let index = 1;
					index < returnFlight.segment.length + 1;
					index++, continuer++
				) {
					const element = returnFlight.segment[index - 1];
					let departureDate = element.departureDate
						.replace("/", "")
						.replace("/", "");
					let departureDateFormatted =
						departureDate.slice(4) +
						"-" +
						departureDate.slice(2, 4) +
						"-" +
						departureDate.slice(0, 2);
					options.FLIGHTS[1].SEGMENTS[continuer] = {
						DEPARTUREAIRPORT: element.from,
						ARRIVALAIRPORT: element.to,
						FLIGHTNUMBER: element.flightNumber,
						AIRLINE: element.airlineName,
						OPERATINGAIRLINE: element.airlineName,
						DEPARTUREDATE: departureDateFormatted
					};
				}
			}
		}
		options.adults = this.props.adults;
		options.children = this.props.children;
		options.infants = this.props.infants;
		options.isMultiCity = this.props.isMultiCity;
		return options;
	}

	generateDescription() {
		if (this.props.searchInfo.isMulticity === false) {
			return (
				this.props.searchInfo.source.code +
				" -> " +
				this.props.searchInfo.destination.code
			);
		} else {
			let placeMap = "";
			if (this.props.searchInfo.multicity.length > 0) {
				let allKeysLength = this.props.searchInfo.multicity.length;
				this.props.searchInfo.multicity.map((row, index) => {
					if (parseInt(index) + 1 === allKeysLength) {
						placeMap = placeMap + row.origin.code + " -> ";
						placeMap = placeMap + row.destination.code;
					} else {
						placeMap = placeMap + row.origin.code + " -> ";
					}
				});
			}
			return placeMap;
		}
	}

	makeCreateItineraryApiCall = params => {
		this.setState({ showSpinner: true });
		this.props.dispatch(createItinerary(params));
	};

	calculateDuration(data) {
		let total = data / 60;
		let hours = Math.floor(total / 60);
		let mins = total % 60;
		return hours + "h " + mins + "m";
	}

	getDuration(date1, time1, date2, time2) {
		var parts1 = date1.split("/");
		var parts2 = date2.split("/");
		// console.log(parts1);
		// console.log(parts2);
		var dt1 = new Date(
			parts1[1] + "/" + parts1[0] + "/" + parts1[2] + " " + time1
		);
		// console.log(dt1);
		var dt2 = new Date(
			parts2[1] + "/" + parts2[0] + "/" + parts2[2] + " " + time2
		);
		// console.log(dt2);
		var diff = (dt2 - dt1) / 1000;
		return this.calculateDuration(diff);
	}

	disposeFlightInfo() {
		this.setState({
			flightInfo: false
		});
	}

	itinaryMap() {
		let placeMap = [];
		if (this.props.searchInfo.multicity.length > 0) {
			let allKeysLength = this.props.searchInfo.multicity.length;
			this.props.searchInfo.multicity.map((row, index) => {
				if (!this.props.searchInfo.multicity[index + 1]) {
					placeMap.push({
						place: row.origin.code,
						date:
							row.departDate.yyyy +
							"-" +
							row.departDate.mm +
							"-" +
							row.departDate.dd
					});
					placeMap.push({ place: row.destination.code, date: "" });
				} else {
					placeMap.push({
						place: row.origin.code,
						date:
							row.departDate.yyyy +
							"-" +
							row.departDate.mm +
							"-" +
							row.departDate.dd
					});
				}
			});
		}
		//console.log("placemap is", placeMap)
		return placeMap;
	}

	findDate(date) {
		if (date !== "") {
			date = date.split("-");
			return date[2] + " " + this.Month[parseInt(date[1]) - 1];
		} else {
			return "    ";
		}
	}

	/** For Filtering Non-Stop Flights  */
	onClickNonStop() {
		if (
			!this.state.nonStop ||
			this.state.earlyMorning ||
			this.state.morning ||
			this.state.midDay ||
			this.state.evening ||
			this.state.night ||
			this.state.hideMultiAirline ||
			this.state.filteredAirlines.length !== 0
		) {
			this.filtersApplied = true;
		} else {
			this.filtersApplied = false;
		}
		this.setState({ nonStop: !this.state.nonStop });
	}

	/** To Filter Early Morning Flights (12AM-8AM) */
	onClickEarlyMorning() {
		if (
			!this.state.earlyMorning ||
			this.state.nonStop ||
			this.state.morning ||
			this.state.midDay ||
			this.state.evening ||
			this.state.night ||
			this.state.hideMultiAirline ||
			this.state.filteredAirlines.length !== 0
		) {
			this.filtersApplied = true;
		} else {
			this.filtersApplied = false;
		}
		this.setState({
			earlyMorning: !this.state.earlyMorning
		});
	}

	/** To Filter Evening Flights (4PM-8PM) */
	onClickEvening() {
		if (
			!this.state.evening ||
			this.state.earlyMorning ||
			this.state.nonStop ||
			this.state.morning ||
			this.state.midDay ||
			this.state.night ||
			this.state.hideMultiAirline ||
			this.state.filteredAirlines.length !== 0
		) {
			this.filtersApplied = true;
		} else {
			this.filtersApplied = false;
		}
		this.setState({
			evening: !this.state.evening
		});
	}

	onNoItemToDisplay(data) {
		//console.log("item to display", data)
		this.setState({
			itemToDisplay: data,
			filterModal: this.state.filterModal && data
		});
	}

	onAllFilterModal() {
		this.openFilters();
	}

	/** To Reset All The Filters */
	onClickReset() {
		this.filtersApplied = false;
		this.setState({
			filterModal: false,
			nonStop: false,
			earlyMorning: false,
			morning: false,
			midDay: false,
			evening: false,
			night: false,
			hideMultiAirline: false,
			filteredAirlines: [],
			itemToDisplay: true
		});
	}

	onClickApply = data => {
		if (
			data.midDay ||
			data.nonStop ||
			data.earlyMorning ||
			data.morning ||
			data.evening ||
			data.night ||
			data.hideMultiAirline ||
			data.filteredAirlines.length !== 0
		) {
			this.filtersApplied = true;
			this.setState({ ...data, itemToDisplay: true });
		} else {
			this.filtersApplied = false;
		}
	};

	openFilters = () => {
		this.props.navigation.navigate("Filter", {
			airlines: this.props.airlines,
			isSearching: this.props.isSearching,
			nonStop: this.state.nonStop,
			hideMultiAirline: this.state.hideMultiAirline,
			earlyMorning: this.state.earlyMorning,
			morning: this.state.morning,
			midDay: this.state.midDay,
			evening: this.state.evening,
			night: this.state.night,
			filteredAirlines: this.state.filteredAirlines,
			onClickReset: this.onClickReset,
			onClickApply: data => this.onClickApply(data)
		});
	};

	render() {
		return (
			<View style={styles.fullFlex}>
				{this.state.showSpinner && (
					<ProgressScreen
						isMessage={true}
						indicatorColor={"#EC6733"}
						indicatorSize={width / 15}
						primaryMessage={I18n.t("hang_on")}
						message={I18n.t("creating_itinerary")}
						messageBar={styles.messageBar}
					/>
				)}
				<View style={styles.headerBar}>
					<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
						<View style={styles.goBackButton}>
							<Icon
								iconType={"material"}
								iconSize={height / 24}
								iconName={"arrow-back"}
								iconColor={"#ffffff"}
							/>
						</View>
					</TouchableOpacity>
					{this.props.isSearching ? (
						<CleartripTextRegular style={styles.searchingFlightText}>
							{I18n.t("searching_flights")}
						</CleartripTextRegular>
					) : this.props.multicity ? (
						<View>
							<ScrollView
								showsHorizontalScrollIndicator={false}
								horizontal={true}
								contentContainerStyle={styles.centeredRow}>
								{this.props.cities.map((data, key) => {
									return (
										<View>
											<View key={key} style={styles.centeredRow}>
												<CleartripTextRegular style={styles.originCodeText}>
													{data.origin.code}
												</CleartripTextRegular>
												<Icon
													iconType={"font_awesome"}
													iconSize={width / 35}
													iconName={"long-arrow-right"}
													iconColor={"#ffffff"}
												/>
											</View>
										</View>
									);
								})}
								<CleartripTextRegular style={styles.cityCodeText}>
									{
										this.props.cities[this.props.cities.length - 1].destination
											.code
									}
								</CleartripTextRegular>
							</ScrollView>
							<ScrollView
								showsHorizontalScrollIndicator={false}
								horizontal={true}
								style={styles.scrollViewMargin}
								contentContainerStyle={styles.flexStartRow}>
								{this.props.cities.map((data, key) => {
									return (
										<View key={key}>
											<View style={styles.flexStartRow}>
												<View style={styles.dot} />
												<Seperator
													width={
														key < this.props.cities.length - 1
															? width / 9.2
															: width / 6.9
													}
													height={1}
													color={"#fff"}
													style={styles.separatorMargin}
												/>
											</View>
											<CleartripTextRegular style={styles.headerText}>
												{data.departDate.dd +
													" " +
													this.Month[parseInt(data.departDate.mm) - 1]}
											</CleartripTextRegular>
										</View>
									);
								})}
								<View style={styles.square} />
							</ScrollView>
						</View>
					) : (
						<View style={styles.airportNameView}>
							<View style={styles.airportNameInnerView}>
								<CleartripTextRegular style={styles.airportNameText}>
									{this.props.domestic
										? this.props.domestic.airports[this.props.domestic.origin].c
										: this.props.international
										? this.props.international.airports[
												this.props.international.origin
										  ].c
										: I18n.t("origin")}
								</CleartripTextRegular>
								{(this.props.domestic && this.props.domestic.roundTrip) ||
								(this.props.international &&
									this.props.international.roundTrip) ? (
									<Image
										source={ROUNDTRIPARROW}
										style={styles.roundTripArrowImage}
									/>
								) : (
									<Image source={RIGHTARROW} style={styles.rightArrowImage} />
								)}
								<CleartripTextRegular
									style={[styles.airportNameText, { marginLeft: width / 40 }]}>
									{this.props.domestic
										? this.props.domestic.airports[
												this.props.domestic.destination
										  ].c
										: this.props.international
										? this.props.international.airports[
												this.props.international.destination
										  ].c
										: I18n.t("destination")}
								</CleartripTextRegular>
							</View>
							<View>
								<CleartripTextRegular style={styles.originNameText}>
									{this.props.domestic
										? this.props.domestic.passengers
										: this.props.international
										? this.props.international.airports[
												this.props.international.origin
										  ].c
										: I18n.t("origin")}
								</CleartripTextRegular>
							</View>
						</View>
					)}
				</View>
				{this.props.isSearching ? (
					// <ProgressBarAndroid styleAttr={"Horizontal"} color={"#EC6733"} />
					<ProgressBar
						width={width}
						height={height / 200}
						value={this.state.progress}
						backgroundColor="#EC6733"
						barEasing={"linear"}
					/>
				) : null}
				{this.props.failure !== undefined &&
				this.props.failure === false &&
				this.state.itemToDisplay ? (
					<View style={styles.singleSearchView}>
						{this.props.multicity && this.props.multicity.isInternational && (
							<SingleSearchItem
								nonStop={this.state.nonStop}
								earlyMorning={this.state.earlyMorning}
								morning={this.state.morning}
								midDay={this.state.midDay}
								evening={this.state.evening}
								night={this.state.night}
								hideMultiAirline={this.state.hideMultiAirline}
								filteredAirlines={this.state.filteredAirlines}
								data={this.props.multicity.items}
								ctr={0}
								airlines={this.props.airlines}
								airports={this.props.airports}
								pushDataToState={this.pushDataToState}
								updateFinalFare={this.updateFinalFare}
								calculateDuration={this.calculateDuration}
								isMultiInternational={this.props.multicity.isInternational}
								getDuration={this.getDuration}
								fillTravellerDetails={this.fillTravellerDetails}
								onNoItemToDisplay={this.onNoItemToDisplay}
								onAllFilterModal={this.onAllFilterModal}
								itemToDisplay={this.state.itemToDisplay}
								setDirection={this.setDirection}
								direction={this.state.direction}
								flightsData={this.state.flightsData}
							/>
						)}
						{this.props.multicity && !this.props.multicity.isInternational && (
							<FlatList
								showsHorizontalScrollIndicator={false}
								keyExtractor={(item, index) => index.toString()}
								data={Object.keys(this.props.multicity)}
								horizontal={true}
								ItemSeparatorComponent={() => (
									<Seperator
										height={height}
										width={height / 920}
										color={"#e0e0e0"}
									/>
								)}
								extraData={this.state}
								renderItem={({ item }) => (
									<SingleSearchItem
										nonStop={this.state.nonStop}
										earlyMorning={this.state.earlyMorning}
										morning={this.state.morning}
										midDay={this.state.midDay}
										evening={this.state.evening}
										night={this.state.night}
										hideMultiAirline={this.state.hideMultiAirline}
										filteredAirlines={this.state.filteredAirlines}
										fillTravellerDetails={this.fillTravellerDetails}
										data={this.props.multicity[item].items}
										pushDataToState={this.pushDataToState}
										getDuration={this.getDuration}
										updateFinalFare={this.updateFinalFare}
										ctr={Object.keys(this.props.multicity).indexOf(item)}
										airlines={this.props.airlines}
										airports={this.props.airports}
										itemToDisplay={this.state.itemToDisplay}
										onAllFilterModal={this.onAllFilterModal}
										calculateDuration={this.calculateDuration}
										size={
											Object.keys(this.props.multicity).size === 2 ? 2 : 1.5
										}
										onNoItemToDisplay={this.onNoItemToDisplay}
										setDirection={this.setDirection}
										direction={this.state.direction}
										flightsData={this.state.flightsData}
									/>
								)}
							/>
						)}
						{/* FIX UI FOR ROUND TRIP - TO:DO */}
						{this.props.domestic &&
						this.state.itemToDisplay &&
						!this.props.isSearching ? (
							<FlatList
								data={
									this.props.domestic.backward
										? ["onward", "backward"]
										: ["onward"]
								}
								horizontal={true}
								showsHorizontalScrollIndicator={false}
								keyExtractor={(item, index) => index.toString()}
								ItemSeparatorComponent={() => (
									<Seperator
										height={height}
										width={height / 920}
										color={"#e0e0e0"}
									/>
								)}
								extraData={this.state}
								renderItem={({ item }) => {
									this.counter++;
									return (
										<SingleSearchItem
											counter={this.counter}
											nonStop={this.state.nonStop}
											earlyMorning={this.state.earlyMorning}
											morning={this.state.morning}
											midDay={this.state.midDay}
											evening={this.state.evening}
											night={this.state.night}
											hideMultiAirline={this.state.hideMultiAirline}
											filteredAirlines={this.state.filteredAirlines}
											data={this.props.domestic[item]}
											pushDataToState={this.pushDataToState}
											ctr={item === "backward" ? 1 : 0}
											fillTravellerDetails={this.fillTravellerDetails}
											airlines={this.props.airlines}
											airports={this.props.airports}
											updateFinalFare={this.updateFinalFare}
											getDuration={this.getDuration}
											calculateDuration={this.calculateDuration}
											onNoItemToDisplay={this.onNoItemToDisplay}
											onAllFilterModal={this.onAllFilterModal}
											itemToDisplay={this.state.itemToDisplay}
											size={this.props.domestic.backward ? 2 : 1}
											flightsData={this.state.flightsData}
											setDirection={this.setDirection}
											direction={this.state.direction}
											isRoundTrip={this.props.isRoundTrip}
										/>
									);
								}}
							/>
						) : null}
						{this.props.international && (
							<FlatList
								data={
									this.props.international.backward
										? ["onward", "backward"]
										: ["onward"]
								}
								horizontal={true}
								showsHorizontalScrollIndicator={false}
								ItemSeparatorComponent={() => (
									<Seperator height={height} width={1} color={"#1d4098"} />
								)}
								renderItem={({ item }) => (
									<SingleSearchItem
										nonStop={this.state.nonStop}
										earlyMorning={this.state.earlyMorning}
										morning={this.state.morning}
										midDay={this.state.midDay}
										evening={this.state.evening}
										night={this.state.night}
										hideMultiAirline={this.state.hideMultiAirline}
										filteredAirlines={this.state.filteredAirlines}
										pushDataToState={this.pushDataToState}
										updateFinalFare={this.updateFinalFare}
										ctr={item === "backward" ? 1 : 0}
										fillTravellerDetails={this.fillTravellerDetails}
										data={this.props.international[item]}
										airlines={this.props.airlines}
										airports={this.props.airports}
										getDuration={this.getDuration}
										calculateDuration={this.calculateDuration}
										onNoItemToDisplay={this.onNoItemToDisplay}
										onAllFilterModal={this.onAllFilterModal}
										itemToDisplay={this.state.itemToDisplay}
										size={this.props.international.backward ? 2 : 1}
										flightsData={this.state.flightsData}
										setDirection={this.setDirection}
										direction={this.state.direction}
										isRoundTrip={this.props.isRoundTrip}
									/>
								)}
							/>
						)}
					</View>
				) : (
					<CleartripTextRegular>
						{this.props.failure !== undefined && this.props.failure === true
							? I18n.t("error_while_fetching_flight_details")
							: null}
					</CleartripTextRegular>
				)}
				{!this.props.isSearching && this.state.itemToDisplay && (
					<View style={styles.totalFareSectionView}>
						<View>
							<CleartripTextRegular style={styles.totalFareText}>
								{I18n.t("total_fare")}
							</CleartripTextRegular>
							<View style={styles.centeredRow}>
								{this.state.specialFare !== 0 &&
									this.state.specialFare !== this.state.finalFare && (
										<CleartripTextMedium style={styles.specialFareText}>
											{"₹ " +
												I18n.toNumber(this.state.specialFare, { precision: 0 })}
										</CleartripTextMedium>
									)}
								<CleartripTextMedium
									style={
										this.state.specialFare === 0 ||
										this.state.finalFare === this.state.specialFare
											? styles.finalFareOne
											: styles.finalFareTwo
									}>
									{"₹ " + I18n.toNumber(this.state.finalFare, { precision: 0 })}
								</CleartripTextMedium>
							</View>
						</View>
						<TouchableOpacity
							onPress={() => this.continueButton()}
							style={styles.continueButton}>
							<CleartripTextMedium style={styles.continueText}>
								{I18n.t("continue")}
							</CleartripTextMedium>
						</TouchableOpacity>
					</View>
				)}
				{!this.props.isSearching &&
				!this.state.itemToDisplay &&
				this.props.failure === false ? (
					<View style={styles.centerJustifyAlign}>
						<Image style={styles.noResultImage} source={NORESULT} />
						<CleartripTextRegular style={styles.manyFiltersText}>
							{I18n.t("oops_too_many_filter")}
						</CleartripTextRegular>
						<CleartripTextRegular style={styles.tryAgainText}>
							{I18n.t("ease_up_your_preferences")}
						</CleartripTextRegular>
						<TouchableOpacity
							activeOpacity={1}
							onPress={() => this.onAllFilterModal()}
							style={styles.backToFilterButton}>
							<CleartripTextMedium style={styles.backToFiltersText}>
								{I18n.t("back_to_filter_caps")}
							</CleartripTextMedium>
						</TouchableOpacity>
					</View>
				) : null}
				{this.state.flightInfo && (
					<View
						style={
							this.state.flightInfo
								? styles.flightInfoViewOne
								: styles.flightInfoViewTwo
						}>
						<FlightInfo
							navigation={this.props.navigation}
							airports={this.props.airports}
							continueBooking={() => {
								this.disposeFlightInfo();
								this.props.navigation.navigate("TravellerDetails");
							}}
							fareBreakup={this.state.fareBreakup}
							calculateDuration={this.calculateDuration}
							disposeModal={this.disposeFlightInfo}
							getDuration={this.getDuration}
							flightDetail={{
								isInternational: this.props.isInternational,
								isMultiCity: this.props.isMultiCity,
								isRoundTrip: this.props.isRoundTrip,
								data: this.state.flightsData
							}}
							airlines={this.props.airlines}
							searchInfo={this.props.searchInfo}
							showFareBreakup={this.showFareBreakup}
							itineraryDetails={this.props.itineraryDetails}
						/>
					</View>
				)}
				{!this.props.isSearching &&
					this.state.itemToDisplay &&
					!this.state.direction &&
					!this.state.flightInfo && (
						<TouchableOpacity
							style={
								this.state.filterModal
									? styles.filterButtonTwo
									: styles.filterButtonOne
							}
							activeOpacity={1}
							onPress={() => {
								LayoutAnimation.easeInEaseOut();
								this.setState({ filterModal: true });
							}}>
							{!this.state.filterModal ? (
								<React.Fragment>
									<Icon
										iconType={"font_awesome"}
										iconColor={"#ffffff"}
										iconName={"filter"}
										iconSize={height / 45}
									/>
									<CleartripTextMedium
										numberOfLines={1}
										ellipsizeMode={"tail"}
										style={styles.filtersText}>
										{I18n.t("filters_caps")}
									</CleartripTextMedium>
								</React.Fragment>
							) : (
								<Icon
									iconType={"font_awesome"}
									iconColor={"#ffffff"}
									iconName={"close"}
									iconSize={height / 45}
								/>
							)}
						</TouchableOpacity>
					)}
				{this.filtersApplied &&
					this.state.itemToDisplay &&
					!this.state.direction &&
					!this.state.flightInfo && (
						<Image style={styles.filterAppliedImage} source={FILTERAPPLIED} />
					)}
				<DialogModal
					dialogModalBackgroundStyle={styles.dialogModalBackgroundStyle}
					visible={this.state.filterModal}
					onRequestClose={() => this.setState({ filterModal: false })}>
					<View style={styles.dialogModalInnerView}>
						<View style={styles.dialogModalSubInnerView}>
							<CleartripTextRegular style={styles.quickFilterText}>
								{I18n.t("quick_filters")}
							</CleartripTextRegular>
						</View>
						<TouchableWithoutFeedback onPress={this.onClickNonStop}>
							<View style={styles.filterPrimaryButton}>
								<View style={styles.centeredRow}>
									<Image style={styles.filterPrimaryImage} source={NONSTOP} />
									<CleartripTextRegular style={styles.filterPrimaryText}>
										{I18n.t("non_stop")}
									</CleartripTextRegular>
								</View>

								{this.state.nonStop ? (
									<Icon
										iconType={"material"}
										iconColor={"#3366cc"}
										iconName={"check-box"}
										iconSize={height / 45.2}
									/>
								) : (
									<Icon
										iconType={"material"}
										iconColor={"#3366cc"}
										iconName={"check-box-outline-blank"}
										iconSize={height / 45.2}
									/>
								)}
							</View>
						</TouchableWithoutFeedback>
						<TouchableWithoutFeedback onPress={this.onClickEarlyMorning}>
							<View style={styles.filterPrimaryButton}>
								<View style={styles.centeredRow}>
									<Image
										style={styles.filterPrimaryImage}
										source={EARLYMORNING}
									/>
									<View style={styles.earlyMorningView}>
										<CleartripTextRegular style={styles.earlyMorningText}>
											{I18n.t("early_morning")}
										</CleartripTextRegular>
										<CleartripTextRegular style={styles.timeText}>
											{"(12am - 8am)"}
										</CleartripTextRegular>
									</View>
								</View>
								{this.state.earlyMorning ? (
									<Icon
										iconType={"material"}
										iconColor={"#3366cc"}
										iconName={"check-box"}
										iconSize={height / 45.2}
									/>
								) : (
									<Icon
										iconType={"material"}
										iconColor={"#3366cc"}
										iconName={"check-box-outline-blank"}
										iconSize={height / 45.2}
									/>
								)}
							</View>
						</TouchableWithoutFeedback>
						<TouchableWithoutFeedback onPress={this.onClickEvening}>
							<View style={styles.filterPrimaryButton}>
								<View style={styles.centeredRow}>
									<Image style={styles.filterPrimaryImage} source={EVENING} />
									<View style={styles.earlyMorningView}>
										<CleartripTextRegular style={styles.earlyMorningText}>
											{I18n.t("evening")}
										</CleartripTextRegular>
										<CleartripTextRegular style={styles.timeText}>
											{"(4pm - 8pm)"}
										</CleartripTextRegular>
									</View>
								</View>
								{this.state.evening ? (
									<Icon
										iconType={"material"}
										iconColor={"#3366cc"}
										iconName={"check-box"}
										iconSize={height / 45.2}
									/>
								) : (
									<Icon
										iconType={"material"}
										iconColor={"#3366cc"}
										iconName={"check-box-outline-blank"}
										iconSize={height / 45.2}
									/>
								)}
							</View>
						</TouchableWithoutFeedback>
						<TouchableOpacity
							activeOpacity={1}
							style={styles.resetButton}
							onPress={this.onClickReset}>
							<View style={styles.centeredRow}>
								<CleartripTextBold style={styles.resetAllText}>
									{I18n.t("reset_all")}
								</CleartripTextBold>
							</View>
						</TouchableOpacity>
						<TouchableOpacity
							activeOpacity={1}
							style={styles.moreFilterButton}
							onPress={() => {
								this.setState({ filterModal: false, allFiltersModal: true });
								this.openFilters();
							}}>
							<View style={styles.centeredRow}>
								<CleartripTextRegular style={styles.moreFilterText}>
									{I18n.t("see_more_filters")}
								</CleartripTextRegular>
								<Icon
									iconName={"ios-arrow-forward"}
									iconColor={"#3366cc"}
									iconSize={width / 25}
									iconType={"ionicon"}
								/>
							</View>
						</TouchableOpacity>
					</View>
				</DialogModal>
			</View>
		);
	}
}

function mapStateToProps(state) {
	let isInternational = false;
	let isMultiCity = false;
	let isRoundTrip = false;
	let isSearching = true;
	let defaultData;
	if (state.cleartrip && state.cleartrip.searchFlight.domestic) {
		isSearching = false;
		if (state.cleartrip.searchFlight.domestic.backward) {
			isRoundTrip = true;
			defaultData = [
				state.cleartrip.searchFlight.domestic.onward[0],
				state.cleartrip.searchFlight.domestic.backward[0]
			];
		} else {
			defaultData = [state.cleartrip.searchFlight.domestic.onward[0]];
		}
	} else if (state.cleartrip && state.cleartrip.searchFlight.international) {
		isInternational = true;
		isSearching = false;
		if (state.cleartrip.searchFlight.international.backward) {
			isRoundTrip = true;
			defaultData = [
				state.cleartrip.searchFlight.international.onward[0],
				state.cleartrip.searchFlight.international.onward[1]
			];
		} else {
			defaultData = [state.cleartrip.searchFlight.international.onward[0]];
		}
	} else if (state.cleartrip && state.cleartrip.searchFlight.multicity) {
		isMultiCity = true;
		defaultData = [];
		isSearching = false;
		let id = 0;
		if (state.cleartrip.searchFlight.multicity.isInternational) {
			isInternational = true;
			defaultData = [state.cleartrip.searchFlight.multicity.items[0]];
		} else {
			defaultData = [];
			var trips = Object.keys(state.cleartrip.searchFlight.multicity);
			for (var i = 0; i < trips.length; i++) {
				defaultData.push(
					state.cleartrip.searchFlight.multicity[trips[i]].items[0]
				);
			}
		}
	}

	return {
		cities:
			state.cleartrip &&
			state.cleartrip.searchFlight &&
			state.cleartrip.searchFlight.cities
				? state.cleartrip.searchFlight.cities
				: null,
		domestic:
			state.cleartrip &&
			state.cleartrip.searchFlight &&
			state.cleartrip.searchFlight.domestic
				? state.cleartrip.searchFlight.domestic
				: null,
		international:
			state.cleartrip &&
			state.cleartrip.searchFlight &&
			state.cleartrip.searchFlight.international
				? state.cleartrip.searchFlight.international
				: null,
		flights:
			state.cleartrip &&
			state.cleartrip.searchFlight &&
			state.cleartrip.searchFlight.flights
				? state.cleartrip.searchFlight.flights
				: null,
		multicity:
			state.cleartrip &&
			state.cleartrip.searchFlight &&
			state.cleartrip.searchFlight.multicity
				? state.cleartrip.searchFlight.multicity
				: null,
		multicityAirports:
			state.cleartrip &&
			state.cleartrip.searchFlight &&
			state.cleartrip.searchFlight.multicityAirports
				? state.cleartrip.searchFlight.multicityAirports
				: null,
		failure:
			state.cleartrip &&
			state.cleartrip.searchFlight &&
			state.cleartrip.searchFlight.failure
				? state.cleartrip.searchFlight.failure
				: false,
		airlines:
			state.cleartrip &&
			state.cleartrip.searchFlight &&
			state.cleartrip.searchFlight.airlines &&
			state.cleartrip.searchFlight.airlines.airlines
				? state.cleartrip.searchFlight.airlines.airlines
				: null,
		airports:
			state.cleartrip &&
			state.cleartrip.searchFlight &&
			state.cleartrip.searchFlight.airlines &&
			state.cleartrip.searchFlight.airlines.airports
				? state.cleartrip.searchFlight.airlines.airports
				: null,
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		isRoundTrip: isRoundTrip,
		isInternational: isInternational,
		isMultiCity: isMultiCity,
		defaultData: defaultData,
		isSearching: isSearching,
		adults: state.cleartrip && state.cleartrip.searchFlight.adults,
		children: state.cleartrip && state.cleartrip.searchFlight.children,
		infants: state.cleartrip && state.cleartrip.searchFlight.infants,
		selectedFlights:
			state.cleartrip && state.cleartrip.selectFlight.selectedFlights,
		searchInfo:
			state.cleartrip &&
			state.cleartrip.searchFlight &&
			state.cleartrip.searchFlight.searchInfo
				? state.cleartrip.searchFlight.searchInfo
				: null,
		networkError: state.cleartrip && state.cleartrip.searchFlight.networkError,
		location: state.location,
		itineraryDetails:
			state.cleartrip && state.cleartrip.travellerDetails.itineraryDetails
	};
}

export default connect(mapStateToProps)(SelectFlight);
