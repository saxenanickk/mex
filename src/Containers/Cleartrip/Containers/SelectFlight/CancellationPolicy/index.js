import React from "react";
import { WebView } from "react-native-webview";
import { connect } from "react-redux";
import { getCancelPolicy, ctFlightCancelPolicyError } from "../Saga";
import { ProgressScreen } from "../../../../../Components";
import { font_two } from "../../../../../Assets/styles";
import { GoToast } from "../../../../../Components";
import I18n from "../../../Assets/Strings/i18n";

class CancellationPolicy extends React.Component {
	componentWillMount() {
		var fullDate = new Date();
		var date =
			fullDate.getDate() +
			"-" +
			(fullDate.getMonth() + 1) +
			"-" +
			fullDate.getFullYear();
		var data = {};
		data.airline = this.props.selectedFlights.data[0].segment[0].airlineName;
		data.fare_basis_code = this.props.selectedFlights.data[0].fare.fareBasisCode;
		data.booking_class = this.props.selectedFlights.data[0].segment[0].cabin;
		data.ts = date;
		data.domain = "api.cleartrip.com";
		if (this.props.selectedFlights.isInternational) {
			data.air_booking_type = "I";
		} else {
			data.air_booking_type = "D";
		}
		this.props.dispatch(
			getCancelPolicy({
				appToken: this.props.appToken,
				params: data,
				location: this.props.location
			})
		);
	}

	shouldComponentUpdate(props, state) {
		if (
			props.cancelPolicyError &&
			props.cancelPolicyError !== this.props.cancelPolicyError
		) {
			GoToast.show(I18n.t("not_retrieve_cancel_policy"), I18n.t("information"));
			this.props.closeCancellationModal();
		}
		return true;
	}

	componentWillUnmount() {
		this.props.dispatch(ctFlightCancelPolicyError(false));
	}
	render() {
		if (this.props.cancelPolicy === null) {
			return <ProgressScreen indicatorColor={"#EC6733"} />;
		} else {
			return (
				<WebView
					source={{
						html:
							"<body style='font-size:12;font-family:'" +
							font_two +
							"'" +
							">" +
							this.props.cancelPolicy +
							"</body>"
					}}
				/>
			);
		}
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		selectedFlights:
			state.cleartrip && state.cleartrip.selectFlight.selectedFlights,
		cancelPolicy: state.cleartrip && state.cleartrip.selectFlight.cancelPolicy,
		location: state.location,
		cancelPolicyError:
			state.cleartrip && state.cleartrip.selectFlight.cancelPolicyError
	};
}

export default connect(mapStateToProps)(CancellationPolicy);
