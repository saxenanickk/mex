import {
	SAVE_SELECTED_FLIGHTS,
	STORE_CANCEL_POLICY,
	CLEARTRIP_CANCEL_POLICY_ERROR
} from "./Saga";

const initialState = {
	selectedFlights: null,
	cancelPolicy: null,
	cancelPolicyError: false
};

export const reducer = (state = initialState, action) => {
	console.log("Reducer--SelectFlight");
	console.log(action.type);
	switch (action.type) {
		case SAVE_SELECTED_FLIGHTS:
			return {
				...state,
				selectedFlights: action.payload
			};
		case STORE_CANCEL_POLICY:
			return {
				...state,
				cancelPolicy: action.payload
			};
		case CLEARTRIP_CANCEL_POLICY_ERROR:
			return {
				...state,
				cancelPolicyError: action.payload
			};
		default:
			return state;
	}
};
