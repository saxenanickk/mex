import React from "react";
import {
	View,
	StatusBar,
	ScrollView,
	TouchableOpacity,
	TouchableWithoutFeedback,
	Dimensions
} from "react-native";
import {
	CleartripTextMedium,
	CleartripTextBold
} from "../../Components/CleartripText";
import { Icon } from "../../../../Components";
import FilterItem from "../../Components/FilterItem";
import {
	NONSTOP,
	HIDEMULTIAIRLINE,
	EARLYMORNING,
	MORNING,
	MIDDAY,
	EVENING,
	NIGHT
} from "../../Assets/Img/Image";
import styles from "./style";
import I18n from "../../Assets/Strings/i18n";

const { width, height } = Dimensions.get("window");

class FilterScreen extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			/** Push the props on to the state */
			nonStop: this.props.nonStop,
			earlyMorning: this.props.earlyMorning,
			morning: this.props.morning,
			midDay: this.props.midDay,
			evening: this.props.evening,
			night: this.props.night,
			hideMultiAirline: this.props.hideMultiAirline,
			filteredAirlines: this.props.filteredAirlines
		};
		/** Bind all the functions */
		this.onClickNonStop = this.onClickNonStop.bind(this);
		this.onClickHideMultiAirline = this.onClickHideMultiAirline.bind(this);
		this.onClickEarlyMorning = this.onClickEarlyMorning.bind(this);
		this.onClickMorning = this.onClickMorning.bind(this);
		this.onClickMidDay = this.onClickMidDay.bind(this);
		this.onClickEvening = this.onClickEvening.bind(this);
		this.onClickNight = this.onClickNight.bind(this);
		this.onClickFilterAirlines = this.onClickFilterAirlines.bind(this);
	}

	/** To Filter Non-Stop Flights  */
	onClickNonStop() {
		if (
			!this.state.nonStop ||
			this.state.earlyMorning ||
			this.state.morning ||
			this.state.midDay ||
			this.state.evening ||
			this.state.night ||
			this.state.hideMultiAirline ||
			this.state.filteredAirlines.length !== 0
		) {
			this.filtersApplied = true;
		} else {
			this.filtersApplied = false;
		}
		this.setState({ nonStop: !this.state.nonStop });
	}

	/** To Hide The Multi-Airline */
	onClickHideMultiAirline() {
		if (
			!this.state.hideMultiAirline ||
			this.state.nonStop ||
			this.state.earlyMorning ||
			this.state.morning ||
			this.state.midDay ||
			this.state.evening ||
			this.state.night ||
			this.state.filteredAirlines.length !== 0
		) {
			this.filtersApplied = true;
		} else {
			this.filtersApplied = false;
		}
		this.setState({
			hideMultiAirline: !this.state.hideMultiAirline
		});
	}

	/** To Filter Early Morning Flights (12AM-8AM) */
	onClickEarlyMorning() {
		if (
			!this.state.earlyMorning ||
			this.state.nonStop ||
			this.state.morning ||
			this.state.midDay ||
			this.state.evening ||
			this.state.night ||
			this.state.hideMultiAirline ||
			this.state.filteredAirlines.length !== 0
		) {
			this.filtersApplied = true;
		} else {
			this.filtersApplied = false;
		}
		this.setState({
			earlyMorning: !this.state.earlyMorning
		});
	}

	/** To Filter Morning Flights (8AM-12PM) */
	onClickMorning() {
		if (
			!this.state.morning ||
			this.state.nonStop ||
			this.state.earlyMorning ||
			this.state.midDay ||
			this.state.evening ||
			this.state.night ||
			this.state.hideMultiAirline ||
			this.state.filteredAirlines.length !== 0
		) {
			this.filtersApplied = true;
		} else {
			this.filtersApplied = false;
		}
		this.setState({
			morning: !this.state.morning
		});
	}

	/** To Filter MidDay Flights (12PM-4PM) */
	onClickMidDay() {
		if (
			!this.state.midDay ||
			this.state.nonStop ||
			this.state.earlyMorning ||
			this.state.morning ||
			this.state.evening ||
			this.state.night ||
			this.state.hideMultiAirline ||
			this.state.filteredAirlines.length !== 0
		) {
			this.filtersApplied = true;
		} else {
			this.filtersApplied = false;
		}
		this.setState({
			midDay: !this.state.midDay
		});
	}

	/** To Filter Evening Flights (4PM-8PM) */
	onClickEvening() {
		if (
			!this.state.evening ||
			this.state.earlyMorning ||
			this.state.nonStop ||
			this.state.morning ||
			this.state.midDay ||
			this.state.night ||
			this.state.hideMultiAirline ||
			this.state.filteredAirlines.length !== 0
		) {
			this.filtersApplied = true;
		} else {
			this.filtersApplied = false;
		}
		this.setState({
			evening: !this.state.evening
		});
	}

	/** To Filter Night Flights (8PM-12AM) */
	onClickNight() {
		if (
			!this.state.night ||
			this.state.nonStop ||
			this.state.earlyMorning ||
			this.state.morning ||
			this.state.midDay ||
			this.state.evening ||
			this.state.hideMultiAirline ||
			this.state.filteredAirlines.length !== 0
		) {
			this.filtersApplied = true;
		} else {
			this.filtersApplied = false;
		}
		this.setState({
			night: !this.state.night
		});
	}

	/** To Filter Airlines */
	onClickFilterAirlines(data) {
		let findResult = this.state.filteredAirlines.indexOf(data);
		if (findResult > -1) {
			if (
				!this.state.night &&
				!this.state.nonStop &&
				!this.state.earlyMorning &&
				!this.state.morning &&
				!this.state.midDay &&
				!this.state.evening &&
				!this.state.hideMultiAirline &&
				this.state.filteredAirlines.length === 1
			) {
				this.filtersApplied = false;
			}
			this.setState({
				filteredAirlines: [...this.state.filteredAirlines].filter(
					item => item !== data
				)
			});
		} else {
			this.filtersApplied = true;
			this.setState({
				filteredAirlines: [...this.state.filteredAirlines, data]
			});
		}
	}

	/** To Reset All The Filters */
	onClickReset() {}

	/** To Apply The Filters */
	onClickApply() {
		this.props.onClickApply(this.state);
		this.props.navigation.goBack();
	}

	render() {
		return (
			<View style={{ flex: 1 }}>
				<StatusBar backgroundColor="#535353" barStyle="light-content" />
				<View style={styles.filterModalHeaderView}>
					<View style={styles.centeredRow}>
						<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
							<View style={styles.goBackButton}>
								<Icon
									iconType={"material"}
									iconSize={height / 24}
									iconName={"arrow-back"}
									iconColor={"#ffffff"}
								/>
							</View>
						</TouchableOpacity>
						<CleartripTextMedium style={styles.filtersTextHeader}>
							{I18n.t("filters_norm")}
						</CleartripTextMedium>
					</View>
				</View>
				<ScrollView
					showsVerticalScrollIndicator={false}
					style={styles.filterItemScrollStyle}>
					<FilterItem
						onPress={this.onClickNonStop}
						value={this.state.nonStop}
						image={NONSTOP}
						title={I18n.t("non_stop_flight_only")}
					/>
					<View style={styles.filterModalInsideView}>
						<View style={styles.centeredRow}>
							<CleartripTextBold style={styles.filterModalInsideText}>
								{I18n.t("airline_details")}
							</CleartripTextBold>
						</View>
					</View>
					<FilterItem
						onPress={this.onClickHideMultiAirline}
						value={this.state.hideMultiAirline}
						image={HIDEMULTIAIRLINE}
						title={I18n.t("hide_multi_airline")}
					/>
					<View style={styles.filterModalInsideView}>
						<View style={styles.centeredRow}>
							<CleartripTextBold style={styles.filterModalInsideText}>
								{I18n.t("onward_deprture_time")}
							</CleartripTextBold>
						</View>
					</View>
					<FilterItem
						rightDescRequired={true}
						desc={"12am - 8am"}
						onPress={this.onClickEarlyMorning}
						value={this.state.earlyMorning}
						image={EARLYMORNING}
						title={I18n.t("early_morning")}
					/>
					<FilterItem
						rightDescRequired={true}
						desc={"8am - 12pm"}
						onPress={this.onClickMorning}
						value={this.state.morning}
						image={MORNING}
						title={I18n.t("morning")}
					/>
					<FilterItem
						rightDescRequired={true}
						desc={"12pm - 4pm"}
						onPress={this.onClickMidDay}
						value={this.state.midDay}
						image={MIDDAY}
						title={I18n.t("mid_day")}
					/>
					<FilterItem
						rightDescRequired={true}
						desc={"4pm - 8pm"}
						onPress={this.onClickEvening}
						value={this.state.evening}
						image={EVENING}
						title={I18n.t("evening")}
					/>
					<FilterItem
						rightDescRequired={true}
						desc={"8pm - 12am"}
						onPress={this.onClickNight}
						value={this.state.night}
						image={NIGHT}
						title={I18n.t("cl_night")}
					/>
					<View style={styles.filterModalInsideView}>
						<View style={styles.centeredRow}>
							<CleartripTextBold style={styles.filterModalInsideText}>
								{I18n.t("preferred_airline")}
							</CleartripTextBold>
						</View>
					</View>
					{this.props.airlines !== null &&
						!this.props.isSearching &&
						Object.keys(this.props.airlines).map((data, key) => {
							return (
								<FilterItem
									key={key}
									onPress={() => this.onClickFilterAirlines(data)}
									value={
										this.state.filteredAirlines.length > 0
											? this.state.filteredAirlines.find(item => {
													if (item === data) {
														return true;
													} else {
														return false;
													}
											  })
											: false
									}
									image={{
										uri:
											"https://www.cleartrip.com/images/logos/air-logos/" +
											data +
											".png"
									}}
									title={this.props.airlines[data]}
								/>
							);
						})}
				</ScrollView>
				<View style={styles.filterModalReset}>
					<TouchableWithoutFeedback
						onPress={() => {
							this.props.onClickReset();
							this.props.navigation.goBack();
						}}>
						<View style={styles.filterModalResetInner}>
							<CleartripTextMedium style={styles.filterModalResetAllText}>
								{I18n.t("reset_all_caps")}
							</CleartripTextMedium>
						</View>
					</TouchableWithoutFeedback>
					<TouchableWithoutFeedback onPress={() => this.onClickApply()}>
						<View style={styles.applyButton}>
							<CleartripTextMedium style={styles.applyText}>
								{I18n.t("apply_caps")}
							</CleartripTextMedium>
						</View>
					</TouchableWithoutFeedback>
				</View>
			</View>
		);
	}
}

export default class Filter extends React.Component {
	render() {
		return <FilterScreen {...this.props} {...this.props.route.params} />;
	}
}
