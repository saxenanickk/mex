import React, { Component } from "react";
import { View, Dimensions, TouchableOpacity, BackHandler } from "react-native";
import { CleartripTextRegular } from "../../../Components/CleartripText";
import CancellationPolicy from "../CancellationPolicy";
import BaggagePolicy from "../BaggagePolicy";
import styles from "./style";
import I18n from "../../../Assets/Strings/i18n";
import ViewPager from "@react-native-community/viewpager";
import GoAppAnalytics from "../../../../../utils/GoAppAnalytics";
import { Icon } from "../../../../../Components";

const { height } = Dimensions.get("window");

export default class CancelPolicy extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedPage: 0
		};

		this.viewPager = React.createRef();

		this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
		BackHandler.addEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
	}

	/**
	 * Back Button Handler
	 */
	handleBackButtonClick() {
		this.props.navigation.goBack();
		return true;
	}

	/**
	 * @param  {Number} index whether the selected index is for coming soon or now showing
	 * change the screen from now showing to coming soon
	 */
	changeSelectedIndex = index => {
		if (index === 0) {
			GoAppAnalytics.setPageView("cleartrip", "CancellationPolicy");
			this.setState({ selectedPage: 0 });
		} else {
			GoAppAnalytics.setPageView("cleartrip", " BaggagePolicy");
			this.setState({ selectedPage: 1 });
		}
	};

	render() {
		return (
			<View style={styles.fullFlex}>
				<View style={styles.headerBar}>
					<View style={styles.centeredRow}>
						<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
							<View style={styles.goBackButton}>
								<Icon
									iconType={"material"}
									iconSize={height / 24}
									iconName={"arrow-back"}
									iconColor={"#ffffff"}
								/>
							</View>
						</TouchableOpacity>
						<CleartripTextRegular style={styles.cancellationPolicyText}>
							{I18n.t("cancel_and_baggage_policy")}
						</CleartripTextRegular>
					</View>
				</View>
				<View style={styles.header}>
					<TouchableOpacity
						style={[
							styles.headerTab,
							this.state.selectedPage === 0
								? styles.headerTabOne
								: styles.headerTabTwo
						]}
						onPress={() => {
							this.viewPager.current.setPage(0);
						}}>
						<CleartripTextRegular style={styles.headerText}>
							{I18n.t("cancel_policy_header")}
						</CleartripTextRegular>
					</TouchableOpacity>
					<TouchableOpacity
						style={[
							styles.headerTab,
							this.state.selectedPage === 1
								? styles.headerTabOne
								: styles.headerTabTwo
						]}
						onPress={() => {
							this.viewPager.current.setPage(1);
						}}>
						<CleartripTextRegular style={styles.headerText}>
							{I18n.t("baggage_policy")}
						</CleartripTextRegular>
					</TouchableOpacity>
				</View>
				<ViewPager
					ref={this.viewPager}
					style={styles.fullFlex}
					initialPage={0}
					onPageSelected={event => {
						this.changeSelectedIndex(event.nativeEvent.position);
					}}>
					<View style={styles.whiteBackGround} key={1}>
						<CancellationPolicy
							closeCancellationModal={() => this.props.navigation.goBack()}
						/>
					</View>
					<View style={styles.whiteBackGround} key={2}>
						<BaggagePolicy />
					</View>
				</ViewPager>
			</View>
		);
	}
}
