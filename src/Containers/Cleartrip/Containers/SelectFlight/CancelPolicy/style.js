import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

export default StyleSheet.create({
	header: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "flex-start",
		backgroundColor: "#3366cc",
		height: height / 11
	},
	headerBar: {
		width: width,
		elevation: 5,
		backgroundColor: "#3366cc",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		justifyContent: "space-between",
		paddingRight: width / 26.3
	},
	centeredRow: {
		flexDirection: "row",
		alignItems: "center"
	},
	goBackButton: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	headerTab: {
		width: width / 2,
		height: height / 11,
		justifyContent: "center",
		alignItems: "center",
		borderBottomWidth: 3
	},
	cancellationPolicyText: {
		color: "#ffffff",
		fontSize: height / 38.4,
		marginLeft: width / 35,
		marginRight: width / 35
	},
	headerText: {
		fontSize: width / 22,
		color: "#fff"
	},
	fullFlex: { flex: 1 },
	headerTabOne: {
		borderBottomColor: "#ffeb3b"
	},
	headerTabTwo: {
		borderBottomColor: "#3366cc"
	},
	whiteBackGround: {
		flex: 1,
		backgroundColor: "#fff"
	}
});
