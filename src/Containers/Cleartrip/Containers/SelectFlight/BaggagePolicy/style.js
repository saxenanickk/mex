import { StyleSheet, Dimensions } from "react-native";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
	baggageMsg: {
		fontSize: width / 29,
		color: "#000"
	},
	baggageInfo: {
		fontSize: width / 29,
		color: "rgba(0,0,0,0.5)"
	},
	mainView: { flex: 1, backgroundColor: "#e9e9e9" },
	fullFlex: { flex: 1 },
	toFromFlightText: {
		fontSize: width / 27,
		padding: width / 30,
		color: "rgba(0,0,0,0.5)"
	},
	checkInText: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: width / 30
	},
	cabinText: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: width / 30,
		marginTop: width / 30
	},
	listStyle: {
		marginVertical: width / 30,
		paddingVertical: width / 30,
		backgroundColor: "#fff"
	}
});

export default styles;
