import React from "react";
import { View, FlatList, Dimensions, StyleSheet } from "react-native";
import { connect } from "react-redux";
import { CleartripTextRegular } from "../../../Components/CleartripText";
import styles from "./style";
import I18n from "../../../Assets/Strings/i18n";
const { width, height } = Dimensions.get("window");

class BaggagePolicy extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<View style={styles.mainView}>
				{this.props.baggagePolicy && (
					<View>
						{Object.keys(this.props.baggagePolicy).map((item, key) => {
							return (
								<FlatList
									key={key}
									keyExtractor={(data, index) => index.toString()}
									data={this.props.baggagePolicy[item]}
									renderItem={(data, index) => {
										let key = Object.keys(data.item)[0];
										let baggageInfo = data.item[key];
										return (
											<View key={index} style={styles.fullFlex}>
												<CleartripTextRegular style={styles.toFromFlightText}>
													{this.props.airports[key.split("_")[0]].c +
														" -> " +
														this.props.airports[key.split("_")[1]].c}
												</CleartripTextRegular>
												<View style={styles.checkInText}>
													<CleartripTextRegular style={styles.baggageMsg}>
														{I18n.t("flight_check_in")}
													</CleartripTextRegular>
													<CleartripTextRegular style={styles.baggageInfo}>
														{baggageInfo.cib}
													</CleartripTextRegular>
												</View>
												<View style={styles.cabinText}>
													<CleartripTextRegular style={styles.baggageMsg}>
														{I18n.t("flight_cabin")}
													</CleartripTextRegular>
													<CleartripTextRegular style={styles.baggageInfo}>
														{baggageInfo.cab}
													</CleartripTextRegular>
												</View>
											</View>
										);
									}}
									style={styles.listStyle}
								/>
							);
						})}
					</View>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		description:
			state.cleartrip &&
			state.cleartrip.travellerDetails.itineraryDetails.description,
		baggagePolicy:
			state.cleartrip &&
			state.cleartrip.travellerDetails.itineraryDetails &&
			state.cleartrip.travellerDetails.itineraryDetails.response
				? state.cleartrip.travellerDetails.itineraryDetails.response[
						"baggage-allowances"
				  ]
				: null,
		airports: state.cleartrip && state.cleartrip.searchFlight.airlines.airports
	};
}

export default connect(
	mapStateToProps,
	null,
	null
)(BaggagePolicy);
