import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

export default StyleSheet.create({
	centeredRow: {
		flexDirection: "row",
		alignItems: "center"
	},
	goBackButton: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	fullFlex: { flex: 1 },
	flexStartRow: {
		flexDirection: "row",
		alignItems: "flex-start"
	},
	spaceBetweenRow: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	container: {
		justifyContent: "space-around",
		height: height / 10
	},
	airportText: {
		color: "rgba(0,0,0,0.6)",
		fontSize: height / 62
	},
	imageStyle: {
		justifyContent: "center",
		alignItems: "center",
		width: width / 8
	},
	imageHeaderStyle: {
		width: width,
		paddingVertical: height / 50,
		alignItems: "center",
		flexDirection: "row",
		justifyContent: "space-between",
		paddingLeft: width / 45,
		paddingRight: width / 22,
		borderBottomWidth: 1,
		borderBottomColor: "#cccccc"
	},
	operationsBar: {
		height: height / 15.5,
		flexDirection: "row",
		backgroundColor: "#ffffff",
		alignItems: "center"
	},
	justifyCenter: {
		justifyContent: "center"
	},
	justifySpaceBetween: {
		justifyContent: "space-between"
	},
	justifyFlexEnd: {
		justifyContent: "flex-end"
	},
	justifySpaceAround: {
		justifyContent: "space-around"
	},
	sortByView: {
		paddingLeft: width / 26.3,
		height: height / 15.5,
		justifyContent: "center"
	},
	sortByText: {
		fontSize: height / 53.3,
		color: "#999999"
	},
	departureBarOne: {
		paddingRight: width / 26.3,
		flexDirection: "row",
		height: height / 15.5,
		width: width / 2 - 1,
		alignItems: "center"
	},
	departureBarTwo: {
		paddingRight: width / 26.3,
		flexDirection: "row",
		height: height / 15.5,
		width: width / 1.67,
		alignItems: "center"
	},
	departureTextOne: {
		color: "#000000",
		fontSize: height / 64,
		marginRight: width / 90
	},
	departureTextTwo: {
		color: "#3366cc",
		fontSize: height / 64,
		marginRight: width / 90
	},
	arrowImage: {
		width: width / 100,
		height: height / 75
	},
	multiInternationalListSeparator: {
		width: width,
		height: width / 30,
		backgroundColor: "#e9e9e9"
	},
	selectFlightButtonOne: {
		backgroundColor: "#d3e9ff",
		alignItems: "center"
	},
	selectFlightButtonTwo: {
		backgroundColor: "#ffffff",
		alignItems: "center"
	},
	selectFlightInnerBar: {
		width: width / 1,
		borderWidth: 0,
		elevation: 2,
		flexDirection: "row"
	},
	renderFlightSectionOne: {
		justifyContent: "space-around",
		width: width,
		borderBottomWidth: 1,
		borderBottomColor: "#cccccc",
		marginTop: height / 50
	},
	renderFlightSectionTwo: {
		justifyContent: "space-around",
		width: width,
		borderBottomWidth: 0,
		borderBottomColor: "#cccccc",
		marginTop: height / 50
	},
	renderFlightInnerView: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingLeft: width / 45
	},
	renderFlightImageView: {
		justifyContent: "center",
		alignItems: "center"
	},
	flightInfoView: {
		flexDirection: "row",
		justifyContent: "space-between",
		width: width / 1.3,
		marginRight: width / 20,
		borderWidth: 0,
		height: height / 30,
		alignItems: "center"
	},
	departureTimeText: {
		color: "#000000",
		fontSize: height / 50
	},
	totalTimeText: {
		fontSize: width / 30,
		color: "#000"
	},
	stopNonStopText: {
		fontSize: height / 62
	},
	priceText: {
		color: "#000000",
		fontSize: width / 25
	},
	isNotMultiInternationalButtonOne: {
		width: 0.66 * width,
		height: height / 11.4,
		alignItems: "flex-start"
	},
	isNotMultiInternationalButtonTwo: {
		width: 0.5 * width,
		height: height / 10.78,
		alignItems: "flex-start"
	},
	isNotMultiInternationalButtonThree: {
		width: width,
		height: height / 11.4,
		alignItems: "flex-start"
	},
	spaceAroundRow: {
		flexDirection: "row",
		justifyContent: "space-around"
	},
	airlineImageView: {
		paddingLeft: width / 26.3,
		paddingRight: width / 50,
		height: height / 11.4,
		justifyContent: "center"
	},
	airlineImageViewOne: {
		width: width / 5.0625
	},
	airlineImageViewTwo: {
		width: width / 6
	},
	airlineImageViewThree: {
		width: width / 3.375
	},
	airlineImage: {
		alignContent: "center",
		width: height / 25.2,
		height: height / 25.2,
		marginBottom: height / 120
	},
	airlineImageText: {
		color: "#999999",
		fontSize: height / 64
	},
	timingSegmentOne: {
		paddingLeft: width / 30
	},
	timingSegmentTwo: {
		paddingLeft: 0
	},
	timingSegmentInnerOne: {
		width: width / 3.55
	},
	timingSegmentInnerTwo: {
		width: width / 3.4
	},
	timingSegmentInnerThree: {
		width: width / 2.35
	},
	depTimeSegmentOne: {
		borderWidth: 0,
		marginTop: height / 60,
		flexDirection: "row",
		alignItems: "center"
	},
	depTimeSegmentTwo: {
		borderWidth: 0,
		marginTop: height / 150,
		flexDirection: "row",
		alignItems: "center"
	},
	depTimeSegmentThree: {
		borderWidth: 0,
		marginTop: height / 90,
		flexDirection: "row",
		alignItems: "center"
	},
	depTimeTextOne: {
		color: "#000",
		fontSize: height / 50.5
	},
	depTimeTextTwo: {
		color: "#000",
		fontSize: height / 41.7
	},
	offerImage: {
		alignContent: "center",
		width: height / 50,
		height: height / 50,
		marginLeft: width / 50
	},
	durationViewOne: {
		marginTop: height / 500,
		flexDirection: "row"
	},
	durationViewTwo: {
		marginTop: height / 200,
		flexDirection: "row"
	},
	durationText: {
		color: "#999999",
		fontSize: height / 60
	},
	fareTextOne: {
		color: "#000",
		fontSize: height / 48
	},
	fareTextTwo: {
		color: "#000",
		fontSize: height / 50.5
	},
	fareTextThree: {
		color: "#000",
		fontSize: height / 40
	},
	totalFareViewOne: {
		width: width / 5.4,
		alignItems: "flex-end",
		paddingRight: width / 26.3
	},
	totalFareViewTwo: {
		width: width / 3.58,
		alignItems: "flex-end",
		paddingRight: width / 26.3
	},
	totalFareTextOne: {
		color: "#000",
		fontSize: height / 48,
		marginTop: height / 60
	},
	totalFareTextTwo: {
		color: "#000",
		fontSize: height / 40,
		marginTop: height / 90
	},
	flightHeaderView: {
		flexDirection: "row",
		alignItems: "center",
		marginLeft: width / 50
	},
	flightHeaderImage: {
		alignContent: "center",
		width: height / 25.2,
		height: height / 25.2
	},
	flightHeaderText: {
		color: "#999999",
		fontSize: height / 54,
		marginLeft: width / 50
	},
	flightHeaderTotalText: {
		color: "#000",
		fontSize: height / 54
	},
	rightArrowMargin: { marginHorizontal: width / 60 }
});
