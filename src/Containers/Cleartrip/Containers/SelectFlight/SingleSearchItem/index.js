import React from "react";
import {
	View,
	FlatList,
	Dimensions,
	TouchableOpacity,
	Image
} from "react-native";
import { connect } from "react-redux";
import Seperator from "../../../../../Components/Seperator";
import { Icon } from "../../../../../Components";
import {
	UPARROWBLACK,
	DOWNARROWBLACK,
	DOWNARROWBLUE,
	MULTIAIRLINE,
	OFFER
} from "../../../Assets/Img/Image";
import {
	CleartripTextRegular,
	CleartripTextMedium,
	CleartripTextBold,
	CleartripTextLight
} from "../../../Components/CleartripText";
import styles from "./style";
import I18n from "../../../Assets/Strings/i18n";
const { width, height } = Dimensions.get("window");

class SingleSearchItem extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedFlight: 0,
			filter: "price",
			order: "ascending",
			toggleRender: false
		};
		this.data = [];
		this.filterCheck = true;
		this.isFiltered = true;
		this.getDurationInHour = this.getDurationInHour.bind(this);
		this.calculateDuration = this.calculateDuration.bind(this);
		this.isMultiAirline = this.isMultiAirline.bind(this);
		this.durationCheck = this.durationCheck.bind(this);
		this.props.updateFinalFare();
	}

	shouldComponentUpdate(props, state) {
		if (
			props.nonStop !== this.props.nonStop ||
			props.earlyMorning !== this.props.earlyMorning ||
			props.morning !== this.props.morning ||
			props.midDay !== this.props.midDay ||
			props.evening !== this.props.evening ||
			props.night !== this.props.night ||
			props.hideMultiAirline !== this.props.hideMultiAirline ||
			state.filter !== this.state.filter ||
			state.order !== this.state.order ||
			props.filteredAirlines !== this.props.filteredAirlines
		) {
			this.isFiltered = true;
			this.setState({ selectedFlight: 0 });
			this.getFilteredData(props, state.filter, state.order);
		}
		return true;
	}

	componentDidMount() {
		this.getFilteredData(this.props, this.state.filter, this.state.order);
	}

	checkFilteredAirline(prevAirlines, nextAirlines) {
		console.log(prevAirlines, nextAirlines);
		prevAirlines.map(item => {
			if (nextAirlines.indexOf(item) < 0) {
				return true;
			}
		});
		return false;
	}
	/**
	 * Filters the data returned by APIs to store it in compoenent state
	 * @param {object} props
	 */
	getFilteredData(props, filter, order) {
		let data = props.data;
		let ar = [];
		let timeFilteredData = [];
		let airlineFilteredData = [];

		if (props.nonStop) {
			data = props.data.filter(item => {
				if (item.segment && item.segment.length === 1) {
					return item;
				}
			});
		}
		if (props.earlyMorning) {
			data.filter(item => {
				let timeArray = item.segment[0].departureTime.split(":");
				if (
					parseInt(timeArray[0]) < 8 ||
					(!props.morning &&
						parseInt(timeArray[0]) === 8 &&
						parseInt(timeArray[1]) === 0)
				) {
					timeFilteredData.push(item);
				}
			});
		}
		if (props.morning) {
			data.filter(item => {
				let timeArray = item.segment[0].departureTime.split(":");
				if (
					(parseInt(timeArray[0]) > 7 && parseInt(timeArray[0]) < 12) ||
					(!props.midDay &&
						parseInt(timeArray[0]) === 12 &&
						parseInt(timeArray[1]) === 0)
				) {
					timeFilteredData.push(item);
				}
			});
		}
		if (props.midDay) {
			data.filter(item => {
				let timeArray = item.segment[0].departureTime.split(":");
				if (
					(parseInt(timeArray[0]) > 11 && parseInt(timeArray[0]) < 16) ||
					(!props.evening &&
						parseInt(timeArray[0]) === 16 &&
						parseInt(timeArray[1]) === 0)
				) {
					timeFilteredData.push(item);
				}
			});
		}
		if (props.evening) {
			data.filter(item => {
				let timeArray = item.segment[0].departureTime.split(":");
				if (
					(parseInt(timeArray[0]) > 15 && parseInt(timeArray[0]) < 20) ||
					(!props.night &&
						parseInt(timeArray[0]) === 20 &&
						parseInt(timeArray[1]) === 0)
				) {
					timeFilteredData.push(item);
				}
			});
		}
		if (props.night) {
			data.filter(item => {
				let timeArray = item.segment[0].departureTime.split(":");
				if (parseInt(timeArray[0]) > 19 && parseInt(timeArray[0]) <= 23) {
					timeFilteredData.push(item);
				}
			});
		}
		if (
			timeFilteredData.length !== 0 ||
			props.earlyMorning === true ||
			props.morning === true ||
			props.midDay === true ||
			props.evening === true ||
			props.night === true
		) {
			data = timeFilteredData;
		}
		if (props.filteredAirlines && props.filteredAirlines.length > 0) {
			props.filteredAirlines.map((item, key) => {
				let tempData = data;
				tempData.filter(value => {
					if (value.segment[0].airlineName === item) {
						airlineFilteredData.push(value);
					}
				});
			});
		}
		if (
			airlineFilteredData.length !== 0 ||
			(props.filteredAirlines && props.filteredAirlines.length > 0)
		) {
			data = airlineFilteredData;
		}
		if (this.filterCheck) {
			this.filterCheck = false;
			if (data) {
				ar = data.sort((a, b) => {
					if (filter !== null && filter === "duration") {
						if (order === "ascending") {
							let parts1, dt1, parts2, dt2, duration1, duration2;
							if (a.segment.length > 1) {
								duration1 = this.getTotalTimeInSeconds(a.segment);
							} else {
								duration1 = a.segment[0].duration;
							}
							if (b.segment.length > 1) {
								duration2 = this.getTotalTimeInSeconds(b.segment);
							} else {
								duration2 = b.segment[0].duration;
							}
							return duration1 - duration2;
						} else {
							let parts1, dt1, parts2, dt2, duration1, duration2;
							if (a.segment.length > 1) {
								duration1 = this.getTotalTimeInSeconds(a.segment);
							} else {
								duration1 = a.segment[0].duration;
							}
							if (b.segment.length > 1) {
								duration2 = this.getTotalTimeInSeconds(b.segment);
							} else {
								duration2 = b.segment[0].duration;
							}
							return duration2 - duration1;
						}
					} else if (filter !== null && filter === "price") {
						if (order === "ascending") {
							return a.fare.totalPrice - b.fare.totalPrice;
						} else {
							return b.fare.totalPrice - a.fare.totalPrice;
						}
					} else if (filter !== null && filter === "departure") {
						var parts1 = a.segment[0].departureDate.split("/");
						var dt1 = new Date(
							parts1[1] +
								"/" +
								parts1[0] +
								"/" +
								parts1[2] +
								" " +
								a.segment[0].departureTime
						);
						var parts2 = b.segment[0].departureDate.split("/");
						var dt2 = new Date(
							parts2[1] +
								"/" +
								parts2[0] +
								"/" +
								parts2[2] +
								" " +
								b.segment[0].departureTime
						);
						if (order === "ascending") {
							return dt1 - new Date() - (dt2 - new Date());
						} else {
							return dt2 - new Date() - (dt1 - new Date());
						}
					}
				});
			}
		}
		if (data.length === 0) {
			this.props.onNoItemToDisplay(false);
		} else if (data.length !== 0 && !this.props.itemToDisplay) {
			this.props.onNoItemToDisplay(true);
		}
		if (this.isFiltered) {
			this.props.pushDataToState(data[0], props.ctr);
			this.isFiltered = false;
		}
		this.data = data;
		this.setState({ toggleRender: !this.state.toggleRender });
	}

	getDurationInHour(seconds) {
		var days = Math.floor(seconds / (3600 * 24));
		seconds -= days * 3600 * 24;
		var hrs = Math.floor(seconds / 3600);
		seconds -= hrs * 3600;
		var mnts = Math.floor(seconds / 60);
		seconds -= mnts * 60;
		var duration = "";
		duration += days !== 0 ? days + "d " : "";
		duration += hrs !== 0 ? hrs + "h " : "";
		duration += mnts !== 0 ? mnts + "m" : "";
		return duration;
	}

	calculateDuration(item) {
		let seconds = 0;
		for (var i = 0; i < item.length; i++) {
			seconds = seconds + item[i].duration;
		}
		return this.getDurationInHour(seconds);
	}

	isMultiAirline(item) {
		let airlineSet = new Set();
		item.segment.map((value, index) => {
			if (!airlineSet.has(value.airlineName)) {
				airlineSet.add(value.airlineName);
			}
		});
		return airlineSet.size > 1;
	}

	durationCheck(date1, time1, date2, time2) {
		var parts1 = date1.split("/");
		var parts2 = date2.split("/");
		// console.log(parts1);
		// console.log(parts2);
		var dt1 = new Date(
			parts1[1] + "/" + parts1[0] + "/" + parts1[2] + " " + time1
		);
		// console.log(dt1);
		var dt2 = new Date(
			parts2[1] + "/" + parts2[0] + "/" + parts2[2] + " " + time2
		);
		// console.log(dt2);
		var diff = (dt2 - dt1) / 1000;
		return diff;
	}

	getDateInString(data, dateParam, timeParam) {
		let date = data[dateParam].split("/");
		date = date[2] + "-" + date[1] + "-" + date[0];
		let mins = data[timeParam];
		return new Date(date + "T" + mins);
	}

	getDuration(duration) {
		let total = duration / 60;
		let hours = Math.floor(total / 60);
		let mins = total % 60;
		mins = mins < 10 ? "0" + mins : mins;
		return hours + "h " + mins + "m";
	}

	getTotalTime(data) {
		let duration = 0;
		data.map((item, index) => {
			duration += item.duration;
			if (index > 0) {
				let prevArrival = this.getDateInString(
					data[index - 1],
					"arrivalDate",
					"arrivalTime"
				).getTime();
				let nextDeparture = this.getDateInString(
					data[index],
					"departureDate",
					"departureTime"
				).getTime();
				duration += (nextDeparture - prevArrival) / 1000;
			}
		});
		return this.getDuration(duration);
	}

	getTotalTimeInSeconds(data) {
		let duration = 0;
		data.map((item, index) => {
			duration += item.duration;
			if (index > 0) {
				let prevArrival = this.getDateInString(
					data[index - 1],
					"arrivalDate",
					"arrivalTime"
				).getTime();
				let nextDeparture = this.getDateInString(
					data[index],
					"departureDate",
					"departureTime"
				).getTime();
				duration += (nextDeparture - prevArrival) / 1000;
			}
		});
		return duration;
	}
	renderFlightImage = segment => {
		let flightSet = new Set();
		segment.map(flight => {
			flightSet.add(flight.airlineName);
		});
		if (flightSet.size > 1) {
			return (
				<View style={styles.imageStyle}>
					<Image style={styles.airlineImage} source={MULTIAIRLINE} />
					<CleartripTextRegular
						style={styles.airlineImageText}
						numberOfLines={1}
						ellipsizeMode={"tail"}>
						{I18n.t("multi_airlines")}
					</CleartripTextRegular>
				</View>
			);
		} else {
			return (
				<View style={styles.imageStyle}>
					<Image
						resizeMode={"contain"}
						style={styles.airlineImage}
						source={{
							uri:
								"https://www.cleartrip.com/images/logos/air-logos/" +
								segment[0].airlineName +
								".png"
						}}
					/>
					<CleartripTextRegular
						style={styles.airlineImageText}
						numberOfLines={1}
						ellipsizeMode={"tail"}>
						{this.props.airlines[segment[0].airlineName]}
					</CleartripTextRegular>
				</View>
			);
		}
	};

	renderFlightHeader = data => {
		let flightSet = new Set();
		Object.keys(data.segment).map(key => {
			data.segment[key].map(flight => {
				flightSet.add(flight.airlineName);
			});
		});

		if (flightSet.size > 1) {
			return (
				<View style={styles.imageHeaderStyle}>
					<View style={styles.flightHeaderView}>
						<Image
							resizeMode={"contain"}
							style={styles.flightHeaderImage}
							source={MULTIAIRLINE}
						/>
						<CleartripTextRegular
							style={styles.flightHeaderText}
							numberOfLines={1}
							ellipsizeMode={"tail"}>
							{I18n.t("multi_airlines")}
						</CleartripTextRegular>
					</View>
					<CleartripTextMedium style={styles.flightHeaderTotalText}>
						{"₹" + I18n.toNumber(data.fare.totalPrice, { precision: 0 })}
					</CleartripTextMedium>
				</View>
			);
		} else {
			return (
				<View style={styles.imageHeaderStyle}>
					<View style={styles.flightHeaderView}>
						<Image
							resizeMode={"contain"}
							style={styles.flightHeaderImage}
							source={{
								uri:
									"https://www.cleartrip.com/images/logos/air-logos/" +
									data.segment[0][0].airlineName +
									".png"
							}}
						/>
						<CleartripTextRegular
							style={styles.flightHeaderText}
							numberOfLines={1}
							ellipsizeMode={"tail"}>
							{this.props.airlines[data.segment[0][0].airlineName]}
						</CleartripTextRegular>
					</View>
					<CleartripTextMedium style={styles.flightHeaderTotalText}>
						{"₹" + I18n.toNumber(data.fare.totalPrice, { precision: 0 })}
					</CleartripTextMedium>
				</View>
			);
		}
	};

	isSpecialFareFlight = value => {
		if (this.props.flightsData.length === 2 && this.props.ctr === 1) {
			let flightsData = this.props.flightsData;
			if (
				flightsData[0].splrt &&
				value.splrt &&
				flightsData[0].splrt.length === 1 &&
				value.splrt.length === 1 &&
				flightsData[0].splrt[0].matchedFlights[0].indexOf(
					value.segment[0].flightNumber
				) > -1 &&
				flightsData[0].segment[0].airlineName === value.segment[0].airlineName
			) {
				return (
					flightsData[0].fare.totalPrice + value.fare.totalPrice >
					flightsData[0].splrt[0].fare.totalPrice +
						value.splrt[0].fare.totalPrice
				);
			}
		}
		return false;
	};

	render() {
		let data = this.data;
		return (
			<View style={styles.fullFlex}>
				<View
					style={[
						styles.operationsBar,
						this.props.size === 2
							? styles.justifyCenter
							: this.props.size === 1
							? styles.justifySpaceBetween
							: styles.justifyFlexEnd
					]}>
					{this.props.size === 1 && (
						<View style={styles.sortByView}>
							<CleartripTextMedium style={styles.sortByText}>
								{I18n.t("sort_by")}
							</CleartripTextMedium>
						</View>
					)}
					<View
						style={[
							this.props.size === 2
								? styles.justifySpaceAround
								: styles.justifySpaceBetween,
							this.props.size === 2
								? styles.departureBarOne
								: styles.departureBarTwo
						]}>
						<TouchableOpacity
							activeOpacity={1}
							style={[styles.operationsBar, styles.justifySpaceBetween]}
							onPress={() => {
								this.filterCheck = true;
								if (this.state.filter !== "departure") {
									this.setState({
										filter: "departure",
										order: "ascending",
										selectedFlight: 0
									});
								} else {
									if (this.state.order === "ascending") {
										this.setState({ order: "descending", selectedFlight: 0 });
									} else {
										this.setState({ order: "ascending", selectedFlight: 0 });
									}
								}
							}}>
							<CleartripTextRegular
								style={
									this.state.filter === "departure"
										? styles.departureTextOne
										: styles.departureTextTwo
								}>
								{I18n.t("departure")}
							</CleartripTextRegular>
							{this.state.filter === "departure" &&
							this.state.order === "ascending" ? (
								<Image style={styles.arrowImage} source={UPARROWBLACK} />
							) : (
								<Image
									style={styles.arrowImage}
									source={
										this.state.filter === "departure"
											? DOWNARROWBLACK
											: DOWNARROWBLUE
									}
								/>
							)}
						</TouchableOpacity>
						{this.props.size !== 2 && (
							<Seperator
								height={height / 21.3}
								width={height / 920}
								color={"#999999"}
							/>
						)}
						{this.props.size !== 2 && (
							<TouchableOpacity
								activeOpacity={1}
								style={[styles.operationsBar, styles.justifySpaceBetween]}
								onPress={() => {
									this.filterCheck = true;
									if (this.state.filter !== "duration") {
										this.setState({
											filter: "duration",
											order: "ascending",
											selectedFlight: 0
										});
									} else {
										if (this.state.order === "ascending") {
											this.setState({
												order: "descending",
												selectedFlight: 0
											});
										} else {
											this.setState({ order: "ascending", selectedFlight: 0 });
										}
									}
								}}>
								<CleartripTextRegular
									style={
										this.state.filter === "duration"
											? styles.departureTextOne
											: styles.departureTextTwo
									}>
									{I18n.t("duration")}
								</CleartripTextRegular>
								{this.state.filter === "duration" &&
								this.state.order === "ascending" ? (
									<Image style={styles.arrowImage} source={UPARROWBLACK} />
								) : (
									<Image
										style={styles.arrowImage}
										source={
											this.state.filter === "duration"
												? DOWNARROWBLACK
												: DOWNARROWBLUE
										}
									/>
								)}
							</TouchableOpacity>
						)}
						<Seperator
							height={height / 21.3}
							width={height / 920}
							color={"#999999"}
						/>
						<TouchableOpacity
							activeOpacity={1}
							style={[styles.operationsBar, styles.justifySpaceBetween]}
							onPress={() => {
								this.filterCheck = true;
								if (this.state.filter !== "price") {
									this.setState({
										filter: "price",
										order: "ascending",
										selectedFlight: 0
									});
								} else {
									if (this.state.order === "ascending") {
										this.setState({ order: "descending", selectedFlight: 0 });
									} else {
										this.setState({ order: "ascending", selectedFlight: 0 });
									}
								}
							}}>
							<CleartripTextRegular
								style={
									this.state.filter === "price"
										? styles.departureTextOne
										: styles.departureTextTwo
								}>
								{I18n.t("ct_price")}
							</CleartripTextRegular>
							{this.state.filter === "price" &&
							this.state.order === "ascending" ? (
								<Image style={styles.arrowImage} source={UPARROWBLACK} />
							) : (
								<Image
									style={styles.arrowImage}
									source={
										this.state.filter === "price"
											? DOWNARROWBLACK
											: DOWNARROWBLUE
									}
								/>
							)}
						</TouchableOpacity>
					</View>
				</View>
				<Seperator
					height={height / 920}
					width={width / this.props.size - 1}
					color={"#e0e0e0"}
				/>
				{this.props.isMultiInternational ? (
					<FlatList
						keyboardShouldPersistTaps={"handled"}
						data={data}
						ItemSeparatorComponent={() => {
							return <View style={styles.multiInternationalListSeparator} />;
						}}
						keyExtractor={(item, index) => index.toString()}
						extraData={this.state}
						onScroll={event => {
							var currentOffset = event.nativeEvent.contentOffset.y;
							var direction = currentOffset > this.offset ? true : false;
							this.offset = currentOffset;
							if (direction !== this.props.direction) {
								if (currentOffset === 0) {
									this.props.setDirection(false);
								} else {
									this.props.setDirection(direction);
								}
							}
						}}
						renderItem={data => (
							<TouchableOpacity
								style={
									this.state.selectedFlight === data.index
										? styles.selectFlightButtonOne
										: styles.selectFlightButtonTwo
								}
								onPress={() => {
									this.props.pushDataToState(data.item, 0);
									this.setState({
										selectedFlight: data.index
									});
								}}>
								<View
									style={[
										this.state.selectedFlight === data.index
											? styles.selectFlightButtonOne
											: styles.selectFlightButtonTwo,
										styles.selectFlightInnerBar
									]}>
									<View>
										{this.renderFlightHeader(data.item)}
										{Object.keys(data.item.segment).map((it, key) => {
											return (
												<View
													key={key}
													style={
														key !== Object.keys(data.item.segment).length - 1
															? styles.renderFlightSectionOne
															: styles.renderFlightSectionTwo
													}>
													<View style={styles.renderFlightInnerView}>
														<View style={styles.renderFlightImageView}>
															{this.renderFlightImage(data.item.segment[it])}
														</View>
														<View>
															<View style={styles.flightInfoView}>
																<View style={styles.centeredRow}>
																	<CleartripTextMedium
																		style={styles.departureTimeText}>
																		{data.item.segment[it][0].departureTime +
																			" - "}
																	</CleartripTextMedium>
																	<CleartripTextRegular
																		style={styles.departureTimeText}>
																		{
																			data.item.segment[it][
																				data.item.segment[it].length - 1
																			].arrivalTime
																		}
																	</CleartripTextRegular>
																</View>
																<CleartripTextRegular
																	style={styles.totalTimeText}>
																	{this.getTotalTime(data.item.segment[it])}
																</CleartripTextRegular>
															</View>

															<View style={styles.flightInfoView}>
																<View style={styles.centeredRow}>
																	<CleartripTextLight
																		style={styles.airportText}>
																		{data.item.segment[it][0].from}
																	</CleartripTextLight>
																	<Icon
																		iconType={"font_awesome"}
																		iconName={"long-arrow-right"}
																		iconSize={height / 80}
																		iconColor={"rgba(0,0,0,0.6)"}
																		style={styles.rightArrowMargin}
																	/>
																	<CleartripTextRegular
																		style={styles.airportText}>
																		{
																			data.item.segment[it][
																				data.item.segment[it].length - 1
																			].to
																		}
																	</CleartripTextRegular>
																</View>
																{data.item.segment[it].length > 1 ? (
																	<CleartripTextLight
																		style={styles.stopNonStopText}>
																		{data.item.segment[it].length - 1 + " Stop"}
																	</CleartripTextLight>
																) : (
																	<CleartripTextLight
																		style={styles.stopNonStopText}>
																		{I18n.t("non_stop")}
																	</CleartripTextLight>
																)}
															</View>
														</View>
													</View>
													{/* TO DO FIX THE BELOW LINE CODE FOR PADDING PROPERLY - @NIKHIL */}
													<CleartripTextBold style={styles.priceText}>
														{it === 0
															? "₹" + data.item &&
															  data.item.fare &&
															  I18n.toNumber(data.item.fare.totalPrice, {
																	precision: 0
															  })
															: ""}
													</CleartripTextBold>
												</View>
											);
										})}
									</View>
								</View>
							</TouchableOpacity>
						)}
					/>
				) : (
					this.props.isMultiInternational == null && (
						<FlatList
							keyboardShouldPersistTaps={"handled"}
							data={data}
							onScroll={event => {
								var currentOffset = event.nativeEvent.contentOffset.y;
								var direction = currentOffset > this.offset ? true : false;
								this.offset = currentOffset;
								if (
									(direction !== this.props.direction &&
										this.props.ctr === 1) ||
									!this.props.isRoundTrip
								) {
									if (currentOffset === 0) {
										this.props.setDirection(false);
									} else {
										this.props.setDirection(direction);
									}
								}
							}}
							ItemSeparatorComponent={() => (
								<Seperator
									width={
										this.props.size === 1.5
											? width / 1.5
											: this.props.size === 2
											? width / 2
											: width
									}
									height={height / 920}
									color={"#e0e0e0"}
								/>
							)}
							showsVerticalScrollIndicator={false}
							keyExtractor={(item, index) => index.toString()}
							extraData={this.state}
							renderItem={data => (
								<TouchableOpacity
									activeOpacity={1}
									style={[
										this.state.selectedFlight === data.index
											? styles.selectFlightButtonOne
											: styles.selectFlightButtonTwo,
										this.props.size === 1.5
											? styles.isNotMultiInternationalButtonOne
											: this.props.size === 2
											? styles.isNotMultiInternationalButtonTwo
											: styles.isNotMultiInternationalButtonThree
									]}
									onPress={() => {
										this.props.pushDataToState(data.item, this.props.ctr);
										this.setState({
											selectedFlight: data.index
										});
									}}>
									<View
										style={[
											this.state.selectedFlight === data.index
												? styles.selectFlightButtonOne
												: styles.selectFlightButtonTwo,
											styles.justifyCenter
										]}>
										<View style={styles.spaceAroundRow}>
											<View
												style={[
													styles.airlineImageView,
													this.props.size === 1.5
														? styles.airlineImageViewOne
														: this.props.size === 2
														? styles.airlineImageViewTwo
														: styles.airlineImageViewThree
												]}>
												{!this.isMultiAirline(data.item) ? (
													<View>
														<Image
															style={styles.airlineImage}
															source={{
																uri:
																	"https://www.cleartrip.com/images/logos/air-logos/" +
																	data.item.segment[0].airlineName +
																	".png"
															}}
														/>
														<CleartripTextRegular
															style={styles.airlineImageText}
															numberOfLines={1}
															ellipsizeMode={"tail"}>
															{
																this.props.airlines[
																	data.item.segment[0].airlineName
																]
															}
														</CleartripTextRegular>
													</View>
												) : (
													<View>
														<Image
															style={styles.airlineImage}
															source={MULTIAIRLINE}
														/>
														<CleartripTextRegular
															style={styles.airlineImageText}
															numberOfLines={1}
															ellipsizeMode={"tail"}>
															{I18n.t("multi_airline_image")}
														</CleartripTextRegular>
													</View>
												)}
											</View>
											<View
												style={
													this.props.size === 2
														? styles.timingSegmentOne
														: styles.timingSegmentTwo
												}>
												<View
													style={
														this.props.size === 1.5
															? styles.timingSegmentInnerOne
															: this.props.size === 2
															? styles.timingSegmentInnerTwo
															: styles.timingSegmentInnerThree
													}>
													<View
														style={
															this.props.size === 1.5
																? styles.depTimeSegmentOne
																: this.props.size === 2
																? styles.depTimeSegmentTwo
																: styles.depTimeSegmentThree
														}>
														<CleartripTextBold
															style={
																this.props.size === 1.5
																	? styles.depTimeTextOne
																	: this.props.size === 2
																	? styles.depTimeTextOne
																	: styles.depTimeTextTwo
															}>
															{data.item.segment[0].departureTime}
														</CleartripTextBold>
														<CleartripTextRegular
															style={
																this.props.size === 1.5
																	? styles.depTimeTextOne
																	: this.props.size === 2
																	? styles.depTimeTextOne
																	: styles.depTimeTextTwo
															}>
															{" - " +
																data.item.segment[data.item.segment.length - 1]
																	.arrivalTime}
														</CleartripTextRegular>
														{this.isSpecialFareFlight(data.item) && (
															<Image style={styles.offerImage} source={OFFER} />
														)}
													</View>
													<View
														style={
															this.props.size === 2
																? styles.durationViewOne
																: styles.durationViewTwo
														}>
														<CleartripTextRegular style={styles.durationText}>
															{data.item.segment.length > 1
																? this.getTotalTime(data.item.segment) + " | "
																: this.props.calculateDuration(
																		data.item.segment[0].duration
																  ) + " | "}
														</CleartripTextRegular>
														{data.item.segment.length > 1 ? (
															<CleartripTextRegular style={styles.durationText}>
																{data.item.segment.length -
																	1 +
																	" " +
																	I18n.t("stop")}
															</CleartripTextRegular>
														) : (
															<CleartripTextRegular style={styles.durationText}>
																{I18n.t("non_stop")}
															</CleartripTextRegular>
														)}
													</View>
												</View>
												{this.props.size === 2 && (
													<CleartripTextBold
														style={
															this.props.size === 1.5
																? styles.fareTextOne
																: this.props.size === 2
																? styles.fareTextTwo
																: styles.fareTextThree
														}>
														{"₹" +
															(data.item.fare && data.item.fare.totalPrice
																? I18n.toNumber(data.item.fare.totalPrice, {
																		precision: 0
																  })
																: "-NA-")}
													</CleartripTextBold>
												)}
											</View>
											{this.props.size !== 2 && (
												<View
													style={
														this.props.size === 1.5
															? styles.totalFareViewOne
															: styles.totalFareViewTwo
													}>
													<CleartripTextBold
														style={
															this.props.size === 1.5
																? styles.totalFareTextOne
																: styles.totalFareTextTwo
														}>
														{"₹" +
															(data.item.fare && data.item.fare.totalPrice
																? I18n.toNumber(data.item.fare.totalPrice, {
																		precision: 0
																  })
																: "-NA-")}
													</CleartripTextBold>
												</View>
											)}
										</View>
									</View>
								</TouchableOpacity>
							)}
						/>
					)
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {};
}

export default connect(mapStateToProps)(SingleSearchItem);
