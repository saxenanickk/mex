import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

export default StyleSheet.create({
	centeredRow: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	goBackButton: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	fullFlex: { flex: 1, backgroundColor: "#fff" },
	flexStartRow: {
		flexDirection: "row",
		alignItems: "flex-start"
	},
	spaceBetweenRow: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	justifyCenter: {
		justifyContent: "center"
	},
	justifySpaceBetween: {
		justifyContent: "space-between"
	},
	justifyFlexEnd: {
		justifyContent: "flex-end"
	},
	justifySpaceAround: {
		justifyContent: "space-around"
	},
	headerBar: {
		width: width,
		elevation: 5,
		backgroundColor: "#3366cc",
		justifyContent: "flex-start",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		paddingRight: width / 26.3
	},
	dot: {
		width: width / 60,
		height: width / 60,
		borderRadius: width / 120,
		backgroundColor: "#fff"
	},
	headerText: {
		fontSize: height / 70,
		color: "#fff"
	},
	messageBar: {
		paddingHorizontal: width / 30,
		paddingVertical: height / 40,
		backgroundColor: "#fff",
		justifyContent: "space-around",
		alignItems: "center",
		flexDirection: "row",
		borderRadius: width / 70
	},
	square: {
		width: width / 60,
		height: width / 60,
		backgroundColor: "#fff"
	},
	searchingFlightText: {
		color: "#ffffff",
		fontSize: height / 45.5,
		marginLeft: width / 35,
		marginRight: width / 35
	},
	originCodeText: {
		color: "#ffffff",
		fontSize: height / 65,
		marginLeft: width / 55,
		marginRight: width / 55
	},
	cityCodeText: {
		color: "#ffffff",
		fontSize: height / 65,
		marginLeft: width / 55
	},
	scrollViewMargin: { marginLeft: width / 55 },
	airportNameView: {
		height: height / 13.33,
		justifyContent: "center",
		marginLeft: width / 35
	},
	airportNameInnerView: {
		flexDirection: "row",
		alignItems: "center",
		marginBottom: height / 192
	},
	airportNameText: {
		color: "#ffffff",
		fontSize: height / 45.5,
		marginRight: width / 35
	},
	roundTripArrowImage: {
		width: width / 35,
		height: height / 70
	},
	rightArrowImage: {
		width: width / 35,
		height: height / 135
	},
	originNameText: {
		color: "#b4c4e3",
		fontSize: height / 60
	},
	singleSearchView: {
		height: height - height / 12 - height / 8
	},
	totalFareSectionView: {
		paddingHorizontal: width / 26.3,
		width: width,
		height: height / 13.33,
		position: "absolute",
		bottom: 0,
		backgroundColor: "#3366cc",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		alignSelf: "flex-end"
	},
	totalFareText: {
		color: "#b4c4e3",
		fontSize: height / 64
	},
	specialFareText: {
		color: "#ffffff",
		fontSize: height / 34.2,
		marginRight: width / 50
	},
	finalFareOne: {
		color: "#fff",
		fontSize: height / 34.2,
		textDecorationLine: "none"
	},
	finalFareTwo: {
		color: "#fff",
		fontSize: height / 60,
		textDecorationLine: "line-through"
	},
	continueButton: {
		backgroundColor: "#f26822",
		width: width / 4,
		height: height / 21.5,
		borderRadius: height / 274,
		justifyContent: "center",
		alignItems: "center"
	},
	continueText: {
		color: "#ffffff",
		fontSize: height / 50.5
	},
	centerJustifyAlign: {
		justifyContent: "center",
		alignItems: "center"
	},
	noResultImage: {
		marginVertical: height / 30,
		width: width / 2.86,
		height: height / 5.4
	},
	manyFiltersText: {
		fontSize: height / 50.5
	},
	tryAgainText: {
		marginTop: height / 200,
		fontSize: height / 50.5
	},
	backToFilterButton: {
		marginTop: height / 34.2,
		width: width / 1.1,
		paddingHorizontal: width / 26.3,
		backgroundColor: "#777777",
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 5,
		height: height / 15.6
	},
	backToFiltersText: {
		fontSize: height / 43.6,
		color: "#ffffff"
	},
	flightInfoViewOne: {
		position: "absolute",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0
	},
	flightInfoViewTwo: {
		position: null,
		top: 0,
		bottom: 0,
		left: 0,
		right: 0
	},
	filterButtonOne: {
		position: "absolute",
		height: height / 14,
		paddingHorizontal: width / 25,
		backgroundColor: "#3366cc",
		borderColor: "#3366cc",
		bottom: height / 11,
		right: width / 26.3,
		elevation: 5,
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 10
	},
	filterButtonTwo: {
		position: "absolute",
		width: height / 14,
		height: height / 14,
		backgroundColor: "#3366cc",
		borderColor: "#3366cc",
		bottom: height / 11,
		right: width / 26.3,
		elevation: 10,
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 10
	},
	filtersText: {
		marginLeft: width / 35,
		fontSize: height / 53,
		color: "#ffffff"
	},
	filterAppliedImage: {
		position: "absolute",
		right: 0,
		elevation: 10,
		bottom: height / 8,
		width: width / 10,
		height: width / 10
	},
	dialogModalBackgroundStyle: {
		backgroundColor: "#000000",
		opacity: 0.2
	},
	dialogModalInnerView: {
		borderRadius: height / 42.6,
		borderColor: "#ffffff",
		elevation: 5,
		position: "absolute",
		bottom: height / 4.5,
		right: width / 35,
		height: height / 2.76,
		backgroundColor: "#ffffff",
		justifyContent: "space-between"
	},
	dialogModalSubInnerView: {
		borderTopLeftRadius: height / 42.6,
		borderTopRightRadius: height / 42.6,
		backgroundColor: "#f7f7f7",
		height: height / 20.9,
		justifyContent: "center",
		paddingLeft: width / 50,
		paddingRight: width / 26.3
	},
	quickFilterText: {
		fontSize: height / 54.8
	},
	filterPrimaryButton: {
		width: width / 2.4,
		height: height / 14.2,
		paddingLeft: width / 50,
		paddingRight: width / 50,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between"
	},
	filterPrimaryImage: {
		height: height / 45.2,
		width: height / 45.2
	},
	filterPrimaryText: {
		marginLeft: width / 120,
		color: "#000000",
		fontSize: height / 50.5
	},
	earlyMorningView: {
		marginLeft: width / 120
	},
	earlyMorningText: {
		color: "#000000",
		fontSize: height / 50.5
	},
	timeText: {
		fontSize: height / 68.5,
		color: "#767676"
	},
	resetButton: {
		height: height / 20.9,
		paddingLeft: width / 50,
		paddingRight: width / 26.3,
		flexDirection: "row",
		alignItems: "center"
	},
	resetAllText: {
		color: "#000000",
		fontSize: height / 56.5
	},
	moreFilterButton: {
		borderBottomLeftRadius: height / 42.6,
		borderBottomRightRadius: height / 42.6,
		height: height / 20.9,
		paddingLeft: width / 50,
		paddingRight: width / 26.3,
		flexDirection: "row",
		alignItems: "center"
	},
	moreFilterText: {
		color: "#3366cc",
		fontSize: height / 50.5,
		marginRight: width / 35
	},
	filterModalHeaderView: {
		width: width,
		elevation: 5,
		backgroundColor: "#777777",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		justifyContent: "space-between",
		paddingRight: width / 26.3
	},
	filtersTextHeader: {
		color: "#ffffff",
		fontSize: height / 38.4,
		marginLeft: width / 35,
		marginRight: width / 35
	},
	filterItemScrollStyle: {
		backgroundColor: "#ffffff",
		marginBottom: height / 15.5
	},
	filterModalInsideView: {
		paddingTop: height / 35,
		paddingHorizontal: width / 26.3,
		height: height / 12.5,
		backgroundColor: "#ececec",
		flexDirection: "row",
		justifyContent: "space-between"
	},
	filterModalInsideText: {
		fontSize: width / 30
	},
	filterModalReset: {
		position: "absolute",
		bottom: 0,
		left: 0,
		right: 0,
		height: height / 15.5,
		justifyContent: "space-around",
		alignItems: "flex-end",
		flexDirection: "row",
		backgroundColor: "#ffffff"
	},
	filterModalResetInner: {
		elevation: 5,
		backgroundColor: "#333333",
		width: width / 2,
		height: height / 15.5,
		borderRightWidth: 0.5,
		borderColor: "#ffffff",
		justifyContent: "center",
		alignItems: "center"
	},
	filterModalResetAllText: {
		color: "#ffffff",
		fontSize: height / 50.5
	},
	applyButton: {
		elevation: 5,
		backgroundColor: "#333333",
		width: width / 2,
		height: height / 15.5,
		borderLeftWidth: 0.5,
		borderColor: "#777777",
		justifyContent: "center",
		alignItems: "center"
	},
	applyText: {
		color: "#ffffff",
		fontSize: height / 50.5
	},
	separatorMargin: { marginTop: width / 150 }
});
