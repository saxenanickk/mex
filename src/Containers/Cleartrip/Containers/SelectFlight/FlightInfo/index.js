import React from "react";
import {
	View,
	FlatList,
	Dimensions,
	TouchableOpacity,
	ScrollView,
	Image
} from "react-native";
import Icon from "../../../../../Components/Icon";
import { Seperator } from "../../../../../Components";
import {
	CleartripTextRegular,
	CleartripTextMedium,
	CleartripTextBold
} from "../../../Components/CleartripText";
import styles from "./style";
import I18n from "../../../Assets/Strings/i18n";

const { width, height } = Dimensions.get("window");

class FlightInfo extends React.Component {
	constructor(props) {
		super(props);
		console.log(props);
		this.state = {
			fareBreakup: false,
			modalContent: false
		};
		this.previousFlightTime = null;
		this.showFareBreakup = this.showFareBreakup.bind(this);
		this.openCancellationModal = this.openCancellationModal.bind(this);
	}

	showFareBreakup() {
		this.setState({
			fareBreakup: !this.state.fareBreakup
		});
	}

	openCancellationModal() {
		this.props.navigation.navigate("CancelPolicy");
	}

	getDuration(duration) {
		let total = duration / 60;
		let hours = Math.floor(total / 60);
		let mins = total % 60;
		mins = mins < 10 ? "0" + mins : mins;
		return hours + "h " + mins + "m";
	}

	getDateInString(data, dateParam, timeParam) {
		let date = data[dateParam].split("/");
		date = date[2] + "-" + date[1] + "-" + date[0];
		let mins = data[timeParam];
		return new Date(date + "T" + mins);
	}
	renderTransitTime(data, index) {
		let prevArrival = this.getDateInString(
			data[index - 1],
			"arrivalDate",
			"arrivalTime"
		).getTime();
		let nextDeparture = this.getDateInString(
			data[index],
			"departureDate",
			"departureTime"
		).getTime();

		let duration = (nextDeparture - prevArrival) / 1000;

		return (
			<View style={styles.transitTimeView}>
				<Seperator width={width / 3} height={1} />
				<CleartripTextRegular style={styles.transitTimeText}>
					{I18n.t("transit_time") + ": " + this.getDuration(duration)}
				</CleartripTextRegular>
				<Seperator width={width / 3} height={1} />
			</View>
		);
	}

	getOriginCity = (data, key) => {
		return this.props.airports[data.segment[0].from].c;
	};

	getDestinationCity = (data, key) => {
		return this.props.airports[data.segment[data.segment.length - 1].to].c;
	};

	getFlightData = data => {
		if (
			this.props.flightDetail.isMultiCity &&
			this.props.flightDetail.isInternational
		) {
			let flightDetail = [];
			Object.keys(data[0].segment).map(key => {
				let obj = {};
				obj.segment = data[0].segment[key];
				obj.fare = data[0].fare;
				flightDetail.push(obj);
			});
			return flightDetail;
		}
		return data;
	};

	getDateToDisplay(date) {
		let dateArray = date.split("/");
		return I18n.strftime(
			new Date(dateArray[1] + "/" + dateArray[0] + "/" + dateArray[2]),
			"%a, %d %b"
		);
	}

	render() {
		let fareToDisplay = this.props.itineraryDetails
			? this.props.itineraryDetails.response.pricing_summary.total_fare
			: 0;
		let baseFare = this.props.itineraryDetails
			? this.props.itineraryDetails.response.pricing_summary.base_fare
			: 0;
		let taxes = this.props.itineraryDetails
			? this.props.itineraryDetails.response.pricing_summary.taxes
			: 0;
		console.log("props in flightInfo", this.props);
		return (
			<View style={styles.container}>
				<View style={styles.subHeaderBar}>
					<View style={styles.centeredRow}>
						<TouchableOpacity onPress={this.props.disposeModal}>
							<View style={styles.goBackButton}>
								<Icon
									iconType={"material"}
									iconSize={height / 24}
									iconName={"arrow-back"}
									iconColor={"#ffffff"}
								/>
							</View>
						</TouchableOpacity>
						<CleartripTextMedium style={styles.reviewItinererayText}>
							{I18n.t("review_itinerary")}
						</CleartripTextMedium>
					</View>
				</View>
				<ScrollView style={styles.fullFlex}>
					{this.getFlightData(this.props.flightDetail.data).map((data, key) => {
						return (
							<View key={key}>
								<View style={styles.flighDataView}>
									<View style={styles.centeredRow}>
										<Icon
											iconType={"font_awesome"}
											iconName={"plane"}
											iconSize={width / 25}
											iconColor={"rgba(0,0,0,0.5)"}
										/>
										<CleartripTextBold style={styles.originSourceText}>
											{this.getOriginCity(data, key) +
												" " +
												I18n.t("to_small") +
												" " +
												this.getDestinationCity(data, key)}
										</CleartripTextBold>
									</View>
								</View>
								<FlatList
									data={data.segment}
									renderItem={({ item, index }) => {
										return (
											<View style={styles.flightListView}>
												{index > 0 &&
													this.renderTransitTime(data.segment, index)}
												<View style={styles.flexStartRow}>
													<Image
														style={styles.flightImage}
														source={{
															uri:
																"https://www.cleartrip.com/images/logos/air-logos/" +
																item.airlineName +
																".png"
														}}
													/>
													<CleartripTextRegular style={styles.airlineInfoText}>
														{this.props.airlines[item.airlineName]}
													</CleartripTextRegular>
													<CleartripTextMedium style={styles.airlineInfoText}>
														{item.airlineName + " " + item.flightNumber}
													</CleartripTextMedium>
												</View>
												<View style={styles.spaceBetweenRow}>
													<View style={styles.fromFlightView}>
														<View style={styles.fromFlightInnerView}>
															<CleartripTextRegular
																style={styles.fromFlightText}>
																{item.from}
															</CleartripTextRegular>
															<CleartripTextBold
																style={styles.fromFlightDepartureText}>
																{" " + item.departureTime}
															</CleartripTextBold>
														</View>
														<CleartripTextRegular
															style={styles.departureDateText}>
															{this.getDateToDisplay(item.departureDate)}
														</CleartripTextRegular>
														<CleartripTextRegular
															numberOfLines={1}
															ellipsizeMode={"tail"}
															style={styles.fromAirportText}>
															{this.props.airports[item.from].n}
														</CleartripTextRegular>
														<CleartripTextRegular
															style={styles.fromAirportCityText}>
															{this.props.airports[item.from].c}
														</CleartripTextRegular>
													</View>
													<View style={styles.durationView}>
														<View style={styles.durationInnerView}>
															<Icon
																iconType={"ionicon"}
																iconName={"md-time"}
																iconSize={height / 34}
																iconColor={"rgba(0,0,0,0.4)"}
															/>
														</View>
														<CleartripTextRegular
															numberOfLines={1}
															ellipsizeMode={"tail"}
															style={styles.durationText}>
															{this.getDuration(item.duration)}
														</CleartripTextRegular>
													</View>
													<View style={styles.toFlightView}>
														<View style={styles.toFlightInnerView}>
															<CleartripTextBold
																style={styles.fromFlightDepartureText}>
																{item.arrivalTime + " "}
															</CleartripTextBold>
															<CleartripTextRegular
																style={styles.fromFlightText}>
																{item.to}
															</CleartripTextRegular>
														</View>
														<CleartripTextRegular
															style={styles.departureDateText}>
															{this.getDateToDisplay(item.arrivalDate)}
														</CleartripTextRegular>
														<CleartripTextRegular
															numberOfLines={1}
															ellipsizeMode={"tail"}
															style={styles.toAirportText}>
															{this.props.airports[item.to].n}
														</CleartripTextRegular>
														<CleartripTextRegular
															style={styles.toAirportCityText}>
															{this.props.airports[item.to].c}
														</CleartripTextRegular>
													</View>
												</View>
											</View>
										);
									}}
								/>
							</View>
						);
					})}
					<View style={styles.fareView}>
						<View style={styles.centeredRow}>
							<CleartripTextBold style={styles.fareForText}>
								{I18n.t("fare_for") +
									" " +
									this.props.searchInfo.adults +
									(this.props.searchInfo.adults < 2 ? " adult" : " adults") +
									(this.props.searchInfo.children === 0
										? ""
										: ", " +
										  this.props.searchInfo.children +
										  (this.props.searchInfo.children < 2
												? " " + I18n.t("child_single")
												: " " + I18n.t("children"))) +
									(this.props.searchInfo.infants === 0
										? ""
										: ", " +
										  this.props.searchInfo.infants +
										  (this.props.searchInfo.infants < 2
												? " " + I18n.t("infants")
												: " " + I18n.t("infant_single")))}
							</CleartripTextBold>
						</View>
					</View>
					<View style={styles.totalPayView}>
						<CleartripTextRegular style={styles.totalPayAmount}>
							{I18n.t("total_payable_amount")}
						</CleartripTextRegular>
						<CleartripTextMedium style={styles.fareToDisplayText}>
							{"₹ " + I18n.toNumber(fareToDisplay, { precision: 0 })}
						</CleartripTextMedium>
						{this.state.fareBreakup === true ? (
							<View style={styles.baseFareView}>
								<View style={styles.baseFareInnerView}>
									<CleartripTextRegular style={styles.baseFareText}>
										{I18n.t("base_fare")}
									</CleartripTextRegular>
									<CleartripTextRegular style={styles.baseFareInfoText}>
										{"₹ " + I18n.toNumber(baseFare, { precision: 0 })}
									</CleartripTextRegular>
								</View>
								<View style={styles.baseFareInnerView}>
									<CleartripTextRegular style={styles.baseFareText}>
										{I18n.t("taxes_and_fees")}
									</CleartripTextRegular>
									<CleartripTextRegular style={styles.baseFareInfoText}>
										{"₹ " + I18n.toNumber(taxes, { precision: 0 })}
									</CleartripTextRegular>
								</View>
							</View>
						) : null}
						<TouchableOpacity
							activeOpacity={1}
							onPress={() => this.showFareBreakup()}>
							<CleartripTextBold style={styles.showHideText}>
								{this.state.fareBreakup === false
									? I18n.t("show_fare_breakup")
									: I18n.t("hide_fare_breakup")}
							</CleartripTextBold>
						</TouchableOpacity>
						<TouchableOpacity onPress={() => this.openCancellationModal()}>
							<CleartripTextBold style={styles.cancelPolicyText}>
								{I18n.t("cancel_and_baggage_policy")}
							</CleartripTextBold>
						</TouchableOpacity>
						<TouchableOpacity
							activeOpacity={1}
							style={styles.continueBookingButton}
							onPress={() => {
								this.props.continueBooking();
							}}>
							<CleartripTextBold style={styles.continueBookingText}>
								{I18n.t("continue_booking_caps")}
							</CleartripTextBold>
						</TouchableOpacity>
					</View>
				</ScrollView>
			</View>
		);
	}
}

export default FlightInfo;
