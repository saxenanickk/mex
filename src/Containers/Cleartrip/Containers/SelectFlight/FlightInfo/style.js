import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

export default StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#ffffff"
	},
	headerBar: {
		flexDirection: "row",
		alignItems: "center",
		padding: 10,
		justifyContent: "flex-start",
		elevation: 3,
		borderBottomColor: "#000",
		backgroundColor: "#3366cc",
		height: height / 11
	},
	headerText: {
		paddingLeft: 50,
		paddingTop: 5,
		fontSize: 20
	},
	headerIcon: {
		paddingLeft: 10,
		color: "#FFF"
	},
	dateFlatList: {
		margin: 20
	},
	button: {
		borderColor: "#3A40FF",
		borderWidth: 2,
		borderLeftWidth: 0,
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: 30,
		paddingRight: 30
	},
	cancellationPolicyText: {
		color: "#ffffff",
		fontSize: width / 22,
		marginLeft: width / 35,
		marginRight: width / 35
	},
	subHeaderBar: {
		width: width,
		elevation: 5,
		backgroundColor: "#3366cc",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		justifyContent: "space-between",
		paddingRight: width / 26.3
	},
	centeredRow: {
		flexDirection: "row",
		alignItems: "center"
	},
	goBackButton: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	reviewItinererayText: {
		color: "#ffffff",
		fontSize: height / 38.4,
		marginLeft: width / 35,
		marginRight: width / 35
	},
	fullFlex: { flex: 1 },
	flighDataView: {
		paddingTop: height / 35,
		paddingHorizontal: width / 26.3,
		height: height / 12.5,
		backgroundColor: "#ececec",
		flexDirection: "row",
		justifyContent: "space-between"
	},
	originSourceText: {
		fontSize: width / 30,
		marginLeft: width / 35
	},
	flightListView: {
		width: width,
		height: height / 4.95,
		backgroundColor: "#ffffff",
		paddingHorizontal: width / 26.3,
		paddingTop: height / 41.7
	},
	flightImage: {
		width: height / 26.5,
		height: height / 26.5
	},
	flexStartRow: {
		flexDirection: "row",
		alignItems: "flex-start"
	},
	airlineInfoText: {
		justifyContent: "flex-start",
		alignItems: "flex-start",
		color: "#999999",
		fontSize: height / 65,
		marginLeft: width / 33.75
	},
	spaceBetweenRow: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	fromFlightView: {
		width: width / 2.5 - width / 25,
		alignItems: "flex-end"
	},
	fromFlightInnerView: {
		alignItems: "center",
		height: height / 28,
		flexDirection: "row"
	},
	fromFlightText: {
		fontSize: width / 20,
		color: "rgba(0,0,0,0.8)"
	},
	fromFlightDepartureText: {
		fontSize: width / 20,
		color: "rgba(0,0,0,1)"
	},
	departureDateText: {
		fontSize: height / 50
	},
	fromAirportText: {
		textAlign: "right",
		width: width / 2.5 - width / 35,
		fontSize: height / 68.5
	},
	fromAirportCityText: {
		textAlign: "right",
		fontSize: height / 68.5
	},
	durationView: {
		width: width - 2 * (width / 2.5) - 2 * (width / 40),
		alignItems: "center"
	},
	durationInnerView: {
		height: height / 28
	},
	durationText: {
		width: width - 2 * (width / 2.5) - 3 * (width / 35),
		textAlign: "center",
		fontSize: height / 68,
		color: "rgba(0,0,0,0.4)"
	},
	toFlightInnerView: {
		height: height / 28,
		flexDirection: "row"
	},
	toAirportText: {
		width: width / 2.5 - width / 35,
		fontSize: height / 68.5
	},
	toAirportCityText: {
		fontSize: height / 68.5
	},
	fareView: {
		paddingTop: height / 35,
		paddingHorizontal: width / 26.3,
		height: height / 12.5,
		backgroundColor: "#ececec",
		flexDirection: "row",
		justifyContent: "space-between"
	},
	fareForText: {
		fontSize: width / 30
	},
	totalPayView: {
		width: width,
		backgroundColor: "#fff",
		alignItems: "center"
	},
	totalPayAmount: {
		color: "#999999",
		fontSize: height / 60,
		marginTop: height / 16
	},
	fareToDisplayText: {
		fontSize: height / 31,
		color: "#000000",
		marginTop: height / 58
	},
	baseFareView: {
		height: height / 8.06,
		width: width / 1.42,
		justifyContent: "center",
		marginTop: height / 28.23
	},
	baseFareInnerView: {
		flexDirection: "row",
		justifyContent: "space-between",
		borderBottomWidth: height / 1500,
		borderBottomColor: "rgba(0,0,0,0.3)",
		height: height / 16.13,
		alignItems: "center"
	},
	baseFareText: {
		fontSize: height / 50.5,
		color: "#000"
	},
	baseFareInfoText: {
		fontSize: height / 50.5
	},
	showHideText: {
		fontSize: height / 56.5,
		marginTop: height / 29.1,
		color: "#3366cc"
	},
	cancelPolicyText: {
		fontSize: height / 56.5,
		marginTop: width / 20,
		color: "#3366cc"
	},
	continueBookingButton: {
		width: width / 1.08,
		margin: width / 26.3,
		elevation: 3,
		borderRadius: height / 192,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#f26822",
		height: height / 15.7
	},
	continueBookingText: {
		color: "#ffffff",
		fontSize: width / 25
	},
	transitTimeView: {
		flexDirection: "row",
		position: "absolute",
		top: 0,
		justifyContent: "center",
		alignItems: "center",
		width: width
	},
	transitTimeText: {
		fontSize: height / 70,
		marginHorizontal: width / 50
	},
	toFlightView: {
		width: width / 2.5 - width / 25,
		alignItems: "flex-start"
	}
});
