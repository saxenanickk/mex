import { takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api/";
import { NO_NETWORK_ERROR } from "../../Api/";
import { cleartripNetworkError } from "../../Saga";
/**
 *	Constants.
 */
export const SAVE_SELECTED_FLIGHTS = "SAVE_SELECTED_FLIGHTS";
export const GET_CANCEL_POLICY = "GET_CANCEL_POLICY";
export const STORE_CANCEL_POLICY = "STORE_CANCEL_POLICY";
export const CLEARTRIP_CANCEL_POLICY_ERROR = "CLEARTRIP_CANCEL_POLICY_ERROR";
/**
 *	Action Creators.
 */
export const saveSelectedFlights = payload => ({
	type: SAVE_SELECTED_FLIGHTS,
	payload
});
export const getCancelPolicy = payload => ({
	type: GET_CANCEL_POLICY,
	payload
});
export const storeCancelPolicy = payload => ({
	type: STORE_CANCEL_POLICY,
	payload
});
export const ctFlightCancelPolicyError = payload => ({
	type: CLEARTRIP_CANCEL_POLICY_ERROR,
	payload
});
export function* SelectFlightSaga(dispatch) {
	yield takeEvery(GET_CANCEL_POLICY, handleGetCancelPolicy);
}

function* handleGetCancelPolicy(action) {
	try {
		let cancelPolicy = yield call(Api.cancelPolicy, action.payload);
		console.log("cancelPolicy :", cancelPolicy);
		if (
			cancelPolicy.hasOwnProperty("error") &&
			cancelPolicy.errorCode === NO_NETWORK_ERROR
		) {
			yield put(
				cleartripNetworkError({
					actionType: action.type,
					payload: action.payload
				})
			);
			return;
		}
		yield put(storeCancelPolicy(cancelPolicy));
	} catch (error) {
		console.log("Cancel Policy saga error :", error);
		yield put(ctFlightCancelPolicyError(true));
	}
}
