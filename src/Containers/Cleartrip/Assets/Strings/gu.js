import * as I18n from "../../../../Assets/strings/i18n";

export default {
	/**
	 * Cleartrip App Strings
	 */
	...I18n.default.translations.gu,
	email_id: "ઇમેઇલ આઈડી",
	cleartrip_done: "પૂર્ણ થયું",
	class: "વર્ગ",
	adults: "પુખ્ત",
	one_way: "વન વે",
	economy: "ઇકોનોમી",
	infants: "શિશુઓ",
	business: "બિઝનેસ",
	children: "બાળકો",
	multicity: "મલ્ટીસિટી",
	depart_on: "પ્રસ્થાન સમય",
	return_on: "રીટર્ન સમય",
	round_trip: "રાઉન્ડ ટ્રીપ",
	travellers: "મુસાફરો",
	choose_date: "તારીખ પસંદ કરો",
	search_flights: "ફ્લાઇટ શોધો",
	premium_economy: "પ્રીમિયમ ઇકોનોમી",
	select_travellers: "મુસાફરો પસંદ કરો",
	search_city_airport: "શહેર અથવા એરપોર્ટ માટે શોધો",
	traveller_details: "ટ્રાવેલર વિગતો",
	contact_details: "સંપર્ક વિગતો",
	mobile_number: "મોબાઇલ નંબર",
	gst: "જીએસટી",
	gst_desc:
		"જો તમારી કંપની પાસે ગુડ એન્ડ સર્વિસ ટેક્સ નંબર છે, તો તે અહીં દાખલ કરો.",
	fill_gst_details: "આ બુકિંગ માટે GST નો ઉપયોગ કરો (વૈકલ્પિક)",
	continue_booking: "બુકિંગ ચાલુ રાખો",
	fill_details: "વિગતો ભરો",
	firstname: "પ્રથમ નામ",
	lastname: "છેલ્લું નામ",
	mr: "શ્રીમાન",
	ms: "મિસ",
	mrs: "શ્રીમતી",
	mstr: "મિસ્ટર",
	miss: "મિસ",
	error_message_while_booking:
		"માફ કરશો, અમે તમારી બુકિંગની પુષ્ટિ કરવામાં અસમર્થ છીએ",
	amount_return_message_on_booking_fail:
		"બાદ કરેલ રકમ તમારા બેંક એકાઉન્ટમાં પરત કરવામાં આવશે",
	please_wait_while_booking: "કૃપા કરીને તમારી ફ્લાઇટ બુકિંગ કરતી વખતે રાહ જુઓ",
	booking_failed: "બુકિંગ નિષ્ફળ થયું!",
	go_back_home: "ઘરે પાછા જાઓ",
	booking_fligh_complete: "બુકિંગ પૂર્ણ થયું",
	your_trip_from: "તમારી સફર",
	trip_confirmed: "પુષ્ટિ કરવામાં આવી છે",
	ct_price: "કિંમત",
	rate_your_exp: "તમારા અનુભવને રેટ કરો",
	how_is_your_exp: "તમે બુકિંગ અનુભવને કેવી રીતે રેટ કરશો?",
	view_trip_detail_cap: "ટ્રીપ વિગતો જુઓ",
	confirm_flight: "ફ્લાઇટની પુષ્ટિ કરો",
	sorry_for_error_while_booking:
		"માફ કરશો, અમે તમારી બુકિંગની પુષ્ટિ કરવામાં અસમર્થ છીએ",
	deducted_amount_will_be_return:
		"ઉપાડિત રકમ તમારા બેંક ખાતામાં પરત કરવામાં આવશે",
	to_caps: "માટે",
	pick_traveller: "મુસાફરો ચૂંટો",
	cancel_caps: "કેન્સલ",
	search_flights_capitalize: "શોધ ફ્લાઇટ્સ",
	add_one_more: "એક વધુ ઉમેરો",
	from_caps: "માંથી",
	popular_cities: "લોકપ્રિય શહેરો",
	flight_check_in: "તપાસો",
	flight_cabin: "કેબીન",
	cancel_policy_header: "રદીકરણ નીતિ",
	baggage_policy: "બેગેજ નીતિ",
	transit_time: "સંક્રમણ સમય",
	cancel_and_baggage_policy: "રદ્દીકરણ અને સામાનની નીતિ",
	review_itinerary: "સમીક્ષા માર્ગદર્શિકા",
	to_small: "થી",
	fare_for: "માટે ભાડું",
	child_single: "બાળક",
	infant_single: "શિશુ",
	total_payable_amount: "કુલ ચુકવવાપાત્ર રકમ",
	base_fare: "મૂળભૂત કરવું",
	taxes_and_fees: "કર અને ફી",
	cashback: "પાછા આવેલા પૈસા",
	discount: "ડિસ્કાઉન્ટ",
	show_fare_breakup: "ભાડું બ્રેકઅપ બતાવો",
	hide_fare_breakup: "ભાડું બ્રેકઅપ છુપાવો",
	continue_booking_caps: "બુકિંગ ચાલુ રાખો",
	multi_airlines: "મલ્ટી એલ્બમ",
	sort_by: "સૉર્ટ કરો",
	departure: "પ્રસ્થાન",
	duration: "અવધિ",
	non_stop: "નોન-સ્ટોપ",
	multi_airline_image: "મલ્ટી એરલાઇન",
	stop: "બંધ",
	searching_flights: "સર્ચ કરી રહ્યા છીએ",
	origin: "મૂળ",
	destination: "લક્ષ્યસ્થાન",
	error_while_fetching_flight_details:
		"ફ્લાઇટ્સની વિગતો લાવવામાં ભૂલ આવી હતી. મહેરબાની કરીને ફરીથી પ્રયતન કરો.",
	total_fare: "કુલ ભાડું",
	continue_caps: "ચાલુ રાખો",
	oops_too_many_filter: "અરે! લાગે છે કે તમે ઘણા બધા ફિલ્ટર્સ લાગુ કર્યા છે",
	ease_up_your_preferences: "તમારી પસંદગીઓને સરળ બનાવો અને ફરી પ્રયાસ કરો",
	back_to_filter_caps: "ફિલ્ટર્સ પર પાછા ફરો",
	filters_caps: "ફિલ્ટર્સ",
	quick_filters: "ઝડપી ગાળકો",
	early_morning: "વહેલી સવારે",
	evening: "સાંજ",
	reset_all: "બધા ફરીથી સેટ કરો",
	see_more_filters: "વધુ ફિલ્ટર્સ જુઓ",
	filters_norm: "ગાળકો",
	non_stop_flight_only: "ફક્ત નૉન-સ્ટોપ ફ્લાઇટ્સ",
	airline_details: "એરલાઇન વિગતો",
	hide_multi_airline: "મલ્ટિ-એરલાઇન માર્ગદર્શિકા છુપાવો",
	onward_deprture_time: "આગળ પ્રસ્થાન સમય",
	morning: "મોર્નિંગ",
	mid_day: "મધ્ય ડે",
	cl_night: "નાઇટ",
	preferred_airline: "પસંદગીના એરલાઇન્સ",
	reset_all_caps: "બધા ફરીથી કરો",
	apply_caps: "અરજી કરો",
	date_of_birth: "જન્મ તારીખ",
	additional_information: "વધારાની માહિતી",
	passport_number: "પાસપોર્ટ નથી.",
	passport_expiry_date: "પાસપોર્ટ સમાપ્તિ તારીખ",
	passport_issue_date: "પાસપોર્ટ ઇશ્યુ તારીખ",
	passport_country: "પાસપોર્ટ ઇસ્યુંગ દેશ",
	price_changed: "ભાવ બદલ્યો",
	markup: "માર્કઅપ",
	trip_details: "ટ્રીપ વિગતો",
	try_connecting_network:
		"મોબાઇલ અથવા Wi-Fi નેટવર્કથી કનેક્ટ કરવાનો પ્રયાસ કરો",
	no_net_no_candy: "કોઈ ઇન્ટરનેટ નથી, કોઈ કેન્ડી નથી",
	wait_while_retrieve_trip_detail:
		"કૃપા કરીને રાહ જુઓ જ્યારે અમે તમારી સહેલની વિગતો પ્રાપ્ત કર",
	adult_noun: {
		one: "પુખ્ત",
		other: "પુખ્ત"
	},
	child_noun: {
		one: "બાળક",
		other: "બાળકો",
		zero: ""
	},
	infants_noun: {
		one: "શિશુ",
		other: "શિશુઓ",
		zero: ""
	},
	hang_on: "રાહ જુઓ",
	creating_itinerary: "આ યાત્રા ચાલુ છે",
	continue: "ચાલુ રાખો",
	cancelled: "રદ",
	user_cancelled: "ચુકવણી વપરાશકર્તા દ્વારા રદ.",
	cancel: "રદ કરો",
	ok: "ઠીક છે",
	price_check_fail: "ભાવ તપાસ નિષ્ફળ",
	not_retrieve_cancel_policy: "રદ કરવાની નીતિ પાછું મેળવવામાં અસમર્થ",
	cant_process_request:
		"આ ક્ષણે તમારી વિનંતી પર પ્રક્રિયા કરી શકાતી નથી. ફરીથી પ્રયત્ન કરો.",
	fare_changed: "ભાડું બદલાયું",
	fare_change_query: "શું તમે બદલાયેલ ભાડું સાથે આગળ વધવા માંગો છો?",
	no: "ના",
	yes: "હા",
	no_flight_change_date:
		"પ્રદાન કરેલ વર્ણન માટે કોઈપણ ફ્લાઇટ્સ શોધવામાં અસમર્થ! કેટલાક અન્ય તારીખો પસંદ કરો.",
	no_location_found: "તમારું સ્થાન શોધવામાં અસમર"
};
