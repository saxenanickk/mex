import { combineReducers } from "redux";

import { reducer as searchFlight } from "./Containers/SearchFlight/Reducer";
import { reducer as travellerDetails } from "./Containers/TravellerDetails/Reducer";
import { reducer as confirmFlight } from "./Containers/ConfirmFlight/Reducer";
import { reducer as tripDetails } from "./Containers/TripDetails/Reducer";
import { reducer as selectFlight } from "./Containers/SelectFlight/Reducer";

import { CLEARTRIP_NETWORK_ERROR, SAVE_CLEARTRIP_DEEPLINK_STATE } from "./Saga";

const initialState = {
	cleartripNoNetworkError: null,
	deeplink: null
};

const cleartripRoot = (state = initialState, action) => {
	switch (action.type) {
		case CLEARTRIP_NETWORK_ERROR:
			return {
				...state,
				cleartripNoNetworkError: action.payload
			};
		case SAVE_CLEARTRIP_DEEPLINK_STATE:
			return {
				...state,
				deeplink: action.payload
			};
		default:
			return state;
	}
};

const ctReducer = combineReducers({
	searchFlight: searchFlight,
	travellerDetails: travellerDetails,
	confirmFlight: confirmFlight,
	tripDetails: tripDetails,
	selectFlight: selectFlight,
	cleartripRoot: cleartripRoot
});

export default ctReducer;
