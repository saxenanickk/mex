import Config from "react-native-config";

const { SERVER_BASE_URL_CLEARTRIP_FLIGHTS, SERVER_BASE_URL_IATA } = Config;

export const NO_NETWORK_ERROR = 1;
export const NO_NETWORK_MESSAGE = "network request failed";
export const RESPONSE_NOT_OK = "response status is not ok";
class CleartripApi {
	constructor() {
		console.log("CleartripApi Instantiated.");
	}

	fetchNearbyAirports(params) {
		console.log(params);
		let lat = params.lat;
		let lng = params.lng;
		let distance = params.distance;
		let APP_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_IATA +
						"/getnearby?lat=" +
						lat +
						"&lng=" +
						lng +
						"&distance=" +
						distance +
						"",
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": APP_TOKEN
						}
					}
				)
					.then(response => {
						if (response.ok) {
							response
								.json()
								.then(res => {
									console.log("response is", res);
									resolve(res.data);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						reject(error);
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	airportAutocomplete(params) {
		console.log(params);
		let query = params.query;
		let APP_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_IATA +
						"/autocomplete?query=" +
						query +
						"&location=" +
						JSON.stringify(params.location) +
						"",
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": APP_TOKEN
						}
					}
				)
					.then(response => {
						if (response.ok) {
							response
								.json()
								.then(res => {
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						reject(error);
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	cancelPolicy(params) {
		console.log(params);
		let query = params.query;
		let APP_TOKEN = params.appToken;
		params = params.params;
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_CLEARTRIP_FLIGHTS +
						"/domestic-fare-rules?" +
						"airline=" +
						params.airline +
						"&location=" +
						JSON.stringify(params.location) +
						"&fare_basis_code=" +
						params.fare_basis_code +
						"&booking_class=" +
						params.booking_class +
						"&ts=" +
						params.ts +
						"&domain=" +
						params.domain +
						"&air_booking_type=" +
						params.air_booking_type,
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": APP_TOKEN
						}
					}
				)
					.then(response => {
						console.log(response);
						if (response.ok) {
							response
								.text()
								.then(function(text) {
									resolve(text);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						// no network error
						resolve({
							error: NO_NETWORK_MESSAGE,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	searchFlight(params) {
		console.log("=> ", params);
		let APP_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_CLEARTRIP_FLIGHTS +
						"/search?" +
						"dateofdeparture=" +
						params.dateofdeparture +
						"&dateofreturn=" +
						params.dateofreturn +
						"&source=" +
						params.source +
						"&destination=" +
						params.destination +
						"&location=" +
						JSON.stringify(params.location) +
						"&adults=" +
						params.adults +
						"&children=" +
						params.children +
						"&infants=" +
						params.infants,
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": APP_TOKEN
						}
					}
				)
					.then(response => {
						console.log("response is", response);
						if (response.ok) {
							response
								.json()
								.then(res => {
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						resolve({
							error: NO_NETWORK_MESSAGE,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	searchFlightMultiCity(params) {
		console.log("IN SEARCH FLIGHT MULTI CITy");
		console.log(params);
		let APP_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_CLEARTRIP_FLIGHTS + "/mcsearch", {
					method: "POST",
					body: JSON.stringify({
						multicity: params.multicity,
						adults: params.adults,
						children: params.children,
						infants: params.infants,
						location: params.location
					}),
					headers: {
						"X-ACCESS-TOKEN": APP_TOKEN,
						"Content-Type": "application/json"
					}
				})
					.then(response => {
						console.log("====> ", response);
						if (response.ok) {
							response
								.json()
								.then(res => {
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						resolve({
							error: NO_NETWORK_MESSAGE,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	bookFlight(params) {
		console.log("request for booking flight", params);
		let APP_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_CLEARTRIP_FLIGHTS +
						"/book/" +
						params.itineraryid +
						"/" +
						params.payment_id +
						"?location=" +
						JSON.stringify(params.location),
					{
						method: "POST",
						headers: {
							"X-ACCESS-TOKEN": APP_TOKEN
						}
					}
				)
					.then(response => {
						console.log("book flight response : " + response);
						if (response.ok) {
							response
								.json()
								.then(res => {
									console.log("book flight response : " + res);
									resolve(res);
								})
								.catch(error => {
									//called when response is not json
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						//called when there is network error
						resolve({
							error: NO_NETWORK_MESSAGE,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	retrieveBooking(params) {
		console.log(params);
		let APP_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_CLEARTRIP_FLIGHTS +
						"/retrieveBooking/" +
						params.itineraryid +
						"?payment_id=" +
						params.transactionId +
						"&location=" +
						JSON.stringify(params.location),
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": APP_TOKEN
						}
					}
				)
					.then(response => {
						if (response.ok) {
							response
								.json()
								.then(res => {
									console.log("Response of retrieve booking: ", res);
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						resolve({
							error: NO_NETWORK_MESSAGE,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	createItinerary(params) {
		console.log("create itinerary", params);
		let APP_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			try {
				var fareObject = [];
				var flightsObject = [];
				for (var k = 0; k < params.FAREDETAILS.length; k++) {
					fareObject.push({
						amount: params.FAREDETAILS[k].AMOUNT,
						fare_key: params.FAREDETAILS[k].FAREKEY
					});
				}
				for (var j = 0; j < params.FLIGHTS.length; j++) {
					console.log(params.FLIGHTS[j]);
					var item = {};
					item.segments = {};
					var keys = Object.keys(params.FLIGHTS[j].SEGMENTS);
					for (var m = 0; m < keys.length; m++) {
						var indexCtr = keys[m];
						item.segments[indexCtr] = {
							departure_airport:
								params.FLIGHTS[j].SEGMENTS[indexCtr].DEPARTUREAIRPORT,
							arrival_airport:
								params.FLIGHTS[j].SEGMENTS[indexCtr].ARRIVALAIRPORT,
							flight_number: params.FLIGHTS[j].SEGMENTS[indexCtr].FLIGHTNUMBER,
							airline: params.FLIGHTS[j].SEGMENTS[indexCtr].AIRLINE,
							operating_airline:
								params.FLIGHTS[j].SEGMENTS[indexCtr].OPERATINGAIRLINE,
							departure_date: params.FLIGHTS[j].SEGMENTS[indexCtr].DEPARTUREDATE
						};
					}

					flightsObject.push(item);
				}
				let bodyMsg = {
					itinerary: {
						cabin_type: params.CABINTYPE,
						fare_details: fareObject,
						flights: flightsObject,
						is_multi_city: params.isMultiCity
					}
				};
				console.log("request of iteinerary creation", JSON.stringify(bodyMsg));
				fetch(
					SERVER_BASE_URL_CLEARTRIP_FLIGHTS +
						"/itinerary-json?adults=" +
						params.adults +
						"&children=" +
						params.children +
						"&infants=" +
						params.infants,
					{
						method: "POST",
						headers: {
							"Content-Type": "application/json",
							"X-ACCESS-TOKEN": APP_TOKEN
						},
						body: JSON.stringify({
							itinerary: {
								cabin_type: params.CABINTYPE,
								fare_details: fareObject,
								flights: flightsObject,
								is_multi_city: params.isMultiCity
							}
						})
					}
				)
					.then(response => {
						console.log("itinerary create response", response);
						if (response.ok) {
							response
								.json()
								.then(res => {
									console.log(
										"Response of itinerary creation: ",
										JSON.stringify(res)
									);
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						resolve({
							error: NO_NETWORK_MESSAGE,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	viewItinerary(params) {
		console.log("itenary", params);
		let APP_TOKEN = params.appToken;

		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_CLEARTRIP_FLIGHTS +
						"/view/" +
						params.itineraryid +
						"?location=" +
						JSON.stringify(params.location),
					{
						method: "GET",
						headers: {
							"Content-Type": "application/json",
							"X-ACCESS-TOKEN": APP_TOKEN
						}
					}
				)
					.then(response => {
						console.log("Response: ", response);
						if (response.ok) {
							response
								.json()
								.then(res => {
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						resolve({
							error: NO_NETWORK_MESSAGE,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	bookItinerary(params) {
		console.log(params);
		let APP_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			try {
				console.log("itinerary-id", params.itineraryid);
				fetch(
					SERVER_BASE_URL_CLEARTRIP_FLIGHTS + "/book/" + params.itineraryid,
					{
						method: "POST",
						headers: {
							"Content-Type": "application/json",
							"X-ACCESS-TOKEN": APP_TOKEN
						}
					}
				)
					.then(response => {
						if (response.ok) {
							response
								.json()
								.then(res => {
									console.log("Response for booking: ", JSON.stringify(res));
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						reject(error);
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	/**
	 * Check if price change or not
	 * Success message on no price change
	 *
	 * {
			"price-check": {
					"itinerary-id": "68e7faf10e-e027-4706-a806-6065e34d6689",
					"succes-message": "Price Check is success"
				}
			}

		* Error message in case seats are unavailable
			{
				"error": {
						"xmlns": "http://www.cleartrip.com/air/",
						"error-message": "The flight you were trying to book is no longer available. Please pick a different flight option.",
						"error-code": "532"
				}
			}

		*  Error msg in case Fare changed
			{
				"xmlns": "http://www.cleartrip.com/air/",
				"error-message": "The fare of the flight you were trying to book has changed from 8909.0 to 10617. You can book the new fare or please pick a different fare.",
				"error-code": "533"
			}
	  */
	priceCheck(params) {
		console.log(params);
		let APP_TOKEN = params.appToken;

		return new Promise(function(resolve, reject) {
			try {
				const pax_info_list_value = params.PAXINFOLIST.map(pax => ({
					title: pax.TITLE,
					first_name: pax.FIRSTNAME,
					last_name: pax.LASTNAME,
					type: pax.TYPE,
					date_of_birth: pax.DOB,
					pax_nationality: pax.PAXNATIONALITY,
					poi_details:
						pax.POIDETAILS !== null
							? {
									id_card_number: pax.POIDETAILS.IDCARDNUMBER,
									id_card_type: pax.POIDETAILS.IDCARDTYPE,
									visa_type: pax.POIDETAILS.VISATYPE
							  }
							: null,
					passport_detail:
						pax.PASSPORTDETAILS !== null
							? {
									passport_number: pax.PASSPORTDETAILS.passport_number,
									passport_exp_date: pax.PASSPORTDETAILS.passport_exp_date,
									passport_issuing_country:
										pax.PASSPORTDETAILS.passport_issuing_country,
									passport_issue_date: pax.PASSPORTDETAILS.passport_issue_date
							  }
							: null
				}));

				let bodyMsg = {
					price_check: {
						pax_info_list: pax_info_list_value,
						contact_detail: {
							title: params.CONTACTDETAILS.TITLE,
							first_name: params.CONTACTDETAILS.FIRSTNAME,
							last_name: params.CONTACTDETAILS.LASTNAME,
							email: params.CONTACTDETAILS.EMAIl,
							address: params.CONTACTDETAILS.ADDRESS,
							mobile: params.CONTACTDETAILS.MOBILE,
							city_name: params.CONTACTDETAILS.CITY,
							state_name: params.CONTACTDETAILS.STATE,
							country_name: params.CONTACTDETAILS.COUNTRY,
							pin_code: params.CONTACTDETAILS.PINCODE
						}
					}
				};
				console.log("request for price check", JSON.stringify(bodyMsg));
				fetch(
					SERVER_BASE_URL_CLEARTRIP_FLIGHTS +
						"/pricecheck/" +
						params.itineraryid +
						"?location=" +
						JSON.stringify(params.location),
					{
						method: "POST",
						headers: {
							"Content-Type": "application/json",
							"X-ACCESS-TOKEN": APP_TOKEN
						},
						body: JSON.stringify({
							price_check: {
								pax_info_list: pax_info_list_value,
								contact_detail: {
									title: params.CONTACTDETAILS.TITLE,
									first_name: params.CONTACTDETAILS.FIRSTNAME,
									last_name: params.CONTACTDETAILS.LASTNAME,
									email: params.CONTACTDETAILS.EMAIl,
									address: params.CONTACTDETAILS.ADDRESS,
									mobile: params.CONTACTDETAILS.MOBILE,
									city_name: params.CONTACTDETAILS.CITY,
									state_name: params.CONTACTDETAILS.STATE,
									country_name: params.CONTACTDETAILS.COUNTRY,
									pin_code: params.CONTACTDETAILS.PINCODE
								}
							}
						})
					}
				)
					.then(response => {
						console.log("Price check Response: ", response);
						response
							.json()
							.then(res => {
								console.log("Price check Response: ", JSON.stringify(res));
								resolve(res);
							})
							.catch(error => {
								reject(error);
							});
					})
					.catch(error => {
						resolve({
							error: NO_NETWORK_MESSAGE,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	fetchMiniRules(params) {
		const APP_TOKEN = params.appToken;

		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_CLEARTRIP_FLIGHTS +
						"/minirules/" +
						"location=" +
						JSON.stringify(params.location),
					{
						method: "POST",
						headers: {
							"Content-Type": "application/json",
							"X-ACCESS-TOKEN": APP_TOKEN
						}
					}
				).then(response => {});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	viewTrip(params) {
		console.log("view trip", params);
		let APP_TOKEN = params.appToken;

		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_CLEARTRIP_FLIGHTS +
						"/viewTrip/" +
						params.tripId +
						"?location=" +
						JSON.stringify(params.location),
					{
						method: "GET",
						headers: {
							"Content-Type": "application/json",
							"X-ACCESS-TOKEN": APP_TOKEN
						}
					}
				)
					.then(response => {
						console.log("Response: ", response);
						if (response.ok) {
							response
								.json()
								.then(res => {
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						resolve({
							error: NO_NETWORK_MESSAGE,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}
}

export default new CleartripApi();
