import React, { Fragment } from "react";
import { SafeAreaView, StatusBar } from "react-native";
import { connect } from "react-redux";
import RegisterScreen from "./RegisterScreen";
import { NoNetworkModal } from "./Components/NoNetworkModal";
import ApplicationToken from "../../CustomModules/ApplicationToken";
import {
	saveCleartripDeeplinkState,
	cleartripNetworkError,
	cleartripHandleRetry
} from "./Saga";
import GoAppAnalytics from "../../utils/GoAppAnalytics";

class App extends React.Component {
	constructor(props) {
		super(props);
		props.dispatch(
			saveCleartripDeeplinkState({
				params: props.params ? JSON.parse(props.params) : props.params
			})
		);
	}

	detectChangeOfScreen = (prevState, currState) => {
		let prevStateRouteLength = prevState.routes.length;
		let currStateRouteLength = currState.routes.length;
		if (prevStateRouteLength > 0 && currStateRouteLength > 0) {
			let prevRoute = prevState.routes[prevStateRouteLength - 1];
			let currRoute = currState.routes[currStateRouteLength - 1];
			if (prevRoute.routeName !== currRoute.routeName) {
				GoAppAnalytics.setPageView("cleartrip", currRoute.routeName);
			}
		}
	};

	handleRetry = () => {
		this.props.dispatch(
			cleartripHandleRetry({
				actionType: this.props.networkError.actionType,
				payload: this.props.networkError.payload
			})
		);
	};

	render() {
		return (
			<Fragment>
				<ApplicationToken />
				{/** For the Top Color (Under the Notch) */}
				<SafeAreaView style={{ flex: 0, backgroundColor: "#3366cc" }} />
				{/** For the Bottom Color (Below the home touch) */}
				<SafeAreaView style={{ flex: 1 }}>
					{this.props.networkError && (
						<NoNetworkModal
							close={() => this.props.dispatch(cleartripNetworkError(null))}
							onRetryPress={this.handleRetry}
						/>
					)}
					<StatusBar backgroundColor="#3366cc" />
					<RegisterScreen
						onNavigationStateChange={(prevState, currState) =>
							GoAppAnalytics.logChangeOfScreenInStack(
								"cleartrip",
								prevState,
								currState
							)
						}
					/>
				</SafeAreaView>
			</Fragment>
		);
	}
}

function mapStateToProps(state) {
	return {
		networkError:
			state.cleartrip && state.cleartrip.cleartripRoot
				? state.cleartrip.cleartripRoot.cleartripNoNetworkError
				: null
	};
}

export default connect(mapStateToProps)(App);
