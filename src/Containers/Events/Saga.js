import { all } from "redux-saga/effects";
// Import different Sagas here
import { eventsListSaga } from "./Containers/EventsList/Saga";

import { selectedEventSaga } from "./Containers/EventDetail/Saga";
import { eventRegistrationSaga } from "./Containers/EventRegistration/Saga";

export default function* eventsSaga() {
	yield all([eventsListSaga(), selectedEventSaga(), eventRegistrationSaga()]);
}
