// @ts-check
import React from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import reducer from "./Reducer";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";
import { getNewReducer, removeExistingReducer } from "../../../App";

export default class Events extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "events" });
		getNewReducer({ name: "events", reducer: reducer });
	}

	componentWillUnmount() {
		removeExistingReducer("events");
	}

	render() {
		// Deeplink Params
		return <App {...this.props} />;
	}
}

AppRegistry.registerComponent("Events", () => Events);
