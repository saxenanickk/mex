import I18n from "i18n-js";
import en from "./en";
import hi from "./hi";

I18n.translations = {
	en,
	hi
};

export default I18n;
