import CANCEL from "./cancel.png";
import DefaultEvent from "./events_default.png";
import FILTER from "./filter.png";
import LOCATION from "./location.png";
import LOGOUT from "./logout.png";
import SEARCH from "./search.png";
import SHARE from "./share.png";
import SUPPORT from "./support.png";
import WALLET from "./wallet.png";
import ORDERS from "./orders.png";
import CALENDAR from "./events.png";
import WALLET_CASH from "./wallet_cash.png";
export {
	CANCEL,
	DefaultEvent,
	FILTER,
	LOCATION,
	LOGOUT,
	SEARCH,
	SHARE,
	SUPPORT,
	WALLET,
	ORDERS,
	CALENDAR,
	WALLET_CASH
};
