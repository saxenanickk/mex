import Config from "react-native-config";

const SERVER_BASE_URL = Config.SERVER_BASE_URL_EVENTS;

class EventsAPI {
	constructor() {
		console.log("events api class initiated");
	}

	getSites(params) {
		let APP_TOKEN = params.app_token;
		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_URL + "/getSites", {
				method: "GET",
				headers: {
					"X-ACCESS-TOKEN": APP_TOKEN
				}
			})
				.then(response =>
					response
						.json()
						.then(res => (res.success ? resolve(res.data) : reject(res.data)))
				)
				.catch(error => reject(error));
		});
	}

	fetchAllEvents(params) {
		let APP_TOKEN = params.app_token;
		let args = params.args;
		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_URL + "/getEvents?" + args, {
				method: "GET",
				headers: {
					"X-ACCESS-TOKEN": APP_TOKEN
				}
			})
				.then(result => {
					result.json().then(res => {
						console.log(res, "events response");
						if (result.status.toString().charAt(0) !== "2") {
							reject(res);
						}
						resolve(res.data);
					});
				})
				.catch(error => {
					reject(error);
				});
		});
	}

	fetchRegisteredEvents(params) {
		let APP_TOKEN = params.app_token;

		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_URL + "/getRegisteredEvents", {
				method: "GET",
				headers: {
					"X-ACCESS-TOKEN": APP_TOKEN
				}
			})
				.then(result => {
					result.json().then(res => {
						if (res.success === 0) {
							reject(res);
						}
						resolve(res.data);
					});
				})
				.catch(error => {
					reject(error);
				});
		});
	}

	fetchSelectedEvent(params) {
		let APP_TOKEN = params.app_token;
		let event_id = params.event_id;

		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_URL + "/getEvent?event_id=" + event_id, {
				method: "GET",
				headers: {
					"X-ACCESS-TOKEN": APP_TOKEN
				}
			})
				.then(result => {
					result.json().then(res => {
						if (res.success === 0) {
							reject(res);
						}
						resolve(res.data);
					});
				})
				.catch(error => {
					reject(error);
				});
		});
	}

	fetchIsUserEligible(params) {
		let APP_TOKEN = params.app_token;
		let event_id = params.event_id;

		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_URL + "/isUserEligible?event_id=" + event_id, {
				method: "GET",
				headers: {
					"X-ACCESS-TOKEN": APP_TOKEN
				}
			})
				.then(result => {
					result.json().then(res => {
						if (res.success === 0) {
							reject(res);
						}
						resolve(res.isRegistered);
					});
				})
				.catch(error => {
					reject(error);
				});
		});
	}

	registerForEvent(params) {
		let APP_TOKEN = params.app_token;
		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_URL + "/registerEvent", {
				method: "POST",
				headers: {
					"X-ACCESS-TOKEN": APP_TOKEN
				},
				body: params.data
			})
				.then(result => {
					result.json().then(res => {
						if (res.success === 0) {
							reject(res);
						}
						resolve(res.registration_id);
					});
				})
				.catch(error => {
					reject(error);
				});
		});
	}

	getCategories(params) {
		let APP_TOKEN = params.app_token;
		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_URL + "/getCategories", {
				method: "GET",
				headers: {
					"X-ACCESS-TOKEN": APP_TOKEN
				}
			})
				.then(result => {
					result.json().then(res => {
						if (res.success === 0) {
							reject(res);
						}
						resolve(res.data);
					});
				})
				.catch(error => {
					reject(error);
				});
		});
	}
}

export default new EventsAPI();
