export const isExpired = event =>
	new Date().getTime() > new Date(event.end_time).getTime();
