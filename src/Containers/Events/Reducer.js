import { combineReducers } from "redux";
// Import Different Reducers Here
import { reducer as eventsListReducer } from "./Containers/EventsList/Reducer";
import { reducer as selectedEventReducer } from "./Containers/EventDetail/Reducer";
import { reducer as registerEventReducer } from "./Containers/EventRegistration/Reducer";

const eventsReducer = combineReducers({
	eventsList: eventsListReducer,
	selectedEvent: selectedEventReducer,
	registerEvent: registerEventReducer
});

export default eventsReducer;
