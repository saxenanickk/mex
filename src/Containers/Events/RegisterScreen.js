import React from "react";
import { Stack } from "../../utils/Navigators";
import Splash from "./Containers/Splash";
import EventDetail from "./Containers/EventDetail";
import PaymentSuccess from "./Containers/PaymentSuccess";
import Registration from "./Containers/EventRegistration/Registration";
import Info from "./Containers/EventRegistration/Info";
import ImageCarousel from "./Containers/ImageCarousel";
import EventsList from "./Containers/EventsList";

let params = null;

const Navigator = () => (
	<Stack.Navigator headerMode={"none"}>
		<Stack.Screen
			name={"Splash"}
			component={props => <Splash params={params} {...props} />}
		/>
		<Stack.Screen name={"Home"} component={EventsList} />
		<Stack.Screen name={"event_detail"} component={EventDetail} />
		<Stack.Screen name={"payment_success"} component={PaymentSuccess} />
		<Stack.Screen name={"registration"} component={Registration} />
		<Stack.Screen name={"info"} component={Info} />
		<Stack.Screen name={"ImageCarousel"} component={ImageCarousel} />
	</Stack.Navigator>
);

const RegisterScreen = props => {
	params = props.route.params;
	return (
		<Navigator
			onNavigationStateChange={(prevState, currState) =>
				props.onNavigationStateChange(prevState, currState)
			}
		/>
	);
};

export default RegisterScreen;
