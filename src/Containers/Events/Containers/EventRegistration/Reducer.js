import {
	EVENTS_INITIATE_EVENT_REGISTRATION,
	EVENTS_EVENT_REGISTRATION_RESP,
	EVENTS_EVENT_REGISTRATION_ERROR
} from "./Saga";

const initialState = {
	isLoading: false,
	isError: false,
	data: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case EVENTS_INITIATE_EVENT_REGISTRATION:
			return {
				...state,
				isLoading: true,
				isError: false,
				data: null
			};
		case EVENTS_EVENT_REGISTRATION_RESP:
			return {
				...state,
				isLoading: false,
				data: action.payload
			};

		case EVENTS_EVENT_REGISTRATION_ERROR:
			return {
				...state,
				isLoading: false,
				data: null,
				isError: true
			};
		default:
			return state;
	}
};
