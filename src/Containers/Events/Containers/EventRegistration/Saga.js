import { takeLatest } from "redux-saga/effects";
import EventsAPI from "../../Api";

/**
 * constants
 */

export const EVENTS_INITIATE_EVENT_REGISTRATION =
	"EVENTS_INITIATE_EVENT_REGISTRATION";
export const EVENTS_EVENT_REGISTRATION_RESP = "EVENTS_EVENT_REGISTRATION_RESP";
export const EVENTS_EVENT_REGISTRATION_ERROR =
	"EVENTS_EVENT_REGISTRATION_ERROR";

/**
 * action creators
 */

export const eventsInitiateEventRegistration = payload => ({
	type: EVENTS_INITIATE_EVENT_REGISTRATION,
	payload
});

export const eventsEventRegistrationResponse = payload => ({
	type: EVENTS_EVENT_REGISTRATION_RESP,
	payload
});

export const eventsEventRegistrationError = payload => ({
	type: EVENTS_EVENT_REGISTRATION_ERROR,
	payload
});

export function* eventRegistrationSaga(dispatch) {
	yield takeLatest(
		EVENTS_INITIATE_EVENT_REGISTRATION,
		handleInitiateEventRegistration
	);
}

/**
 * Handlers
 */

function* handleInitiateEventRegistration(action) {
	try {
	} catch (error) {}
}
