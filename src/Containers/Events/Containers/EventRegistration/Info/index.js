import React, { Component, Fragment } from "react";
import {
	View,
	StatusBar,
	ScrollView,
	Dimensions,
	TouchableOpacity,
	KeyboardAvoidingView,
	Platform,
	BackHandler
} from "react-native";
import { connect } from "react-redux";
import { capitalize } from "lodash";
import { DrawerHeader } from "../../../Components/DrawerHeader";
import { GoToast } from "../../../../../Components";
import {
	EventsTextMedium,
	EventsTextBold
} from "../../../Components/EventsText";
import SuperInput from "../../../Components/SuperInput";
import I18n from "../../../Assets/Strings/i18n";

const { width } = Dimensions.get("window");

class Info extends Component {
	constructor(props) {
		super(props);
		const { route } = this.props;
		const {
			additionalAttendeeInputs = [],
			handleAddInfoBack = () => {},
			numberOfTickets,
			updatedAttendeeData = () => {},
			inputModel = []
		} = route.params ? route.params : {};

		this.handleAddInfoBack = handleAddInfoBack;
		this.numberOfTickets = numberOfTickets;
		this.updatedAttendeeData = updatedAttendeeData;
		this.inputModel = inputModel;

		this.state = {
			inputs: additionalAttendeeInputs
		};
	}
	componentDidMount() {
		BackHandler.addEventListener(
			"hardwareBackPress",
			this.handleHardwareBack.bind(this)
		);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.handleHardwareBack.bind(this)
		);
	}

	handleHardwareBack = () => {
		this.handleAddInfoBack(this.state.inputs);
		this.props.navigation.goBack();
	};

	handleInputChange = (text, inputItem, inputIndex) => {
		let newInputState = [
			...this.state.inputs.slice(0, inputIndex),
			{ ...this.state.inputs[inputIndex], [inputItem.field_name]: text },
			...this.state.inputs.slice(inputIndex + 1)
		];
		console.log(newInputState);
		this.setState((state, props) => {
			return {
				inputs: [
					...state.inputs.slice(0, inputIndex),
					{ ...state.inputs[inputIndex], [inputItem.field_name]: text },
					...state.inputs.slice(inputIndex + 1)
				]
			};
		});
	};

	loopAllInputs = () => {
		for (let i = 0; i < this.numberOfTickets; i++) {
			let inputsLengthCheck = this.props.selectedEvent.mandatory_fields.filter(
				item => this.state.inputs[i][item].length > 0
			);
			if (
				inputsLengthCheck.length !==
				this.props.selectedEvent.mandatory_fields.length
			) {
				return false;
			}
		}
		return true;
	};

	validateMandatoryFields = () => {
		let isValid = this.loopAllInputs();
		console.log(isValid);
		if (isValid) {
			this.updatedAttendeeData(this.state.inputs);
			this.props.navigation.goBack();
		} else {
			GoToast.show(
				I18n.t("events_mandatory_fields"),
				I18n.t("events_info"),
				3000
			);
		}
	};

	render() {
		return (
			<View style={{ flex: 1, alignItems: "center" }}>
				<StatusBar backgroundColor={"#3d43dd"} />
				<DrawerHeader
					navigation={() => this.handleHardwareBack()}
					isHeaderName={true}
					headerName={I18n.t("events_registration_details")}
					isGoBack={true}
				/>
				<KeyboardAvoidingView
					behavior={Platform.select({
						ios: "padding",
						android: null
					})}
					keyboardVerticalOffset={Platform.select({
						ios: 50,
						android: 0
					})}
					style={{ flex: 1 }}>
					<ScrollView
						keyboardShouldPersistTaps={"always"}
						style={{
							flex: 1,
							width: width * 0.9,
							marginBottom: width / 40
						}}>
						<EventsTextBold
							style={{ color: "#000", paddingVertical: width / 25 }}>
							{" "}
							{I18n.t("events_attendees_info")}{" "}
						</EventsTextBold>
						<View
							style={{
								backgroundColor: "#F6F6F6",
								paddingHorizontal: width / 20,
								paddingVertical: width / 20
							}}>
							{[...Array(this.numberOfTickets)].map((item, index) => {
								return (
									<Fragment key={index}>
										<View
											style={{
												backgroundColor: "#505EFF",
												alignItems: "flex-start"
											}}>
											<EventsTextMedium
												style={{ color: "#fff", padding: width / 20 }}>
												{I18n.t("events_attendee")} #{index + 1}
											</EventsTextMedium>
										</View>
										{this.inputModel.map((inputItem, inputIndex) => {
											return (
												<View
													key={inputIndex}
													style={{
														flexDirection: "row",
														alignItems: "center",
														paddingHorizontal: width / 80,
														paddingVertical: width / 50
													}}>
													<EventsTextBold style={{ width: width / 3 }}>
														{capitalize(inputItem.field_name)}
														{this.props.selectedEvent.mandatory_fields.includes(
															inputItem.field_name
														) ? (
															<EventsTextBold>*</EventsTextBold>
														) : (
															""
														)}
													</EventsTextBold>
													<SuperInput
														fieldDetails={inputItem}
														value={
															this.state.inputs[index][inputItem.field_name]
														}
														onChangeValue={text =>
															this.handleInputChange(text, inputItem, index)
														}
													/>
												</View>
											);
										})}
									</Fragment>
								);
							})}
						</View>
					</ScrollView>
				</KeyboardAvoidingView>
				<TouchableOpacity
					onPress={this.validateMandatoryFields}
					style={{
						width: width * 0.9,
						alignItems: "center",
						justifyContent: "center",
						paddingVertical: width / 40,
						marginBottom: width / 40,
						backgroundColor: "#505EFF"
					}}>
					<EventsTextBold style={{ color: "#fff" }}>
						{I18n.t("events_done")}
					</EventsTextBold>
				</TouchableOpacity>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		token: state.appToken.token,
		selectedEvent: state.events && state.events.selectedEvent.event
	};
}

export default connect(mapStateToProps)(Info);
