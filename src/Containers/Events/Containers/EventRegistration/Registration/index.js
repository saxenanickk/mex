import React, { Component } from "react";
import {
	ActivityIndicator,
	View,
	StatusBar,
	Dimensions,
	TouchableOpacity,
	Image,
	ScrollView,
	Alert,
	KeyboardAvoidingView,
	Platform
} from "react-native";
import { HeaderHeightContext } from "@react-navigation/stack";
import { connect } from "react-redux";
import { DrawerHeader } from "../../../Components/DrawerHeader";
import {
	EventsTextMedium,
	EventsTextBold,
	EventsTextRegular
} from "../../../Components/EventsText";
import { GoToast } from "../../../../../Components";
import Dropdown from "../../../../../Components/Dropdown";
import { capitalize } from "../../../../../utils/capitalize";
import RazorPay from "../../../../../CustomModules/RazorPayModule";
import I18n from "../../../Assets/Strings/i18n";

import { styles } from "./style";
import SuperInput from "../../../Components/SuperInput";
import GoAppAnalytics from "../../../../../utils/GoAppAnalytics";
import { isExpired } from "../../../Utils";

import Config from "react-native-config";
import { CommonActions } from "@react-navigation/native";
import { ifIphoneX } from "../../../../../utils/iphonexHelper";

const { SERVER_BASE_URL_EVENTS } = Config;

const { width } = Dimensions.get("window");

class Registration extends Component {
	state = {
		isExpired: false,
		selectedTicketType: null,
		numberOfTickets: 1,
		numberOfPersons: 0,
		inputs: {},
		additionalAttendeeInputs: [],
		isAdditionalAttendeeInputsValid: false,
		inputModel: [],
		isPaymentLoading: false,
		isRegisterDisabled: false,
		isEventError: false
	};

	isUserEligibleAlertFirstTimeTriggered = false;

	componentDidMount() {
		if (this.props.selectedEvent !== null) {
			let isEventValid = this.checkMandatoryFieldsExistsInAdditionalOrDefaultFields(
				this.props.selectedEvent
			);
			if (!isEventValid) {
				this.setState({ isEventError: true });
			}
			this.checkExpiry(this.props.selectedEvent);
		}
	}

	handleBack = () => {
		this.props.navigation.goBack();
	};

	checkExpiry = event => {
		let expiryFlag = isExpired(event);
		if (expiryFlag) {
			this.setState({ isExpired: expiryFlag });
		}
	};

	shouldComponentUpdate(nextProps, nextState) {
		if (
			nextProps.isUserEligible === false ||
			this.props.isUserEligible === false
		) {
			if (!this.isUserEligibleAlertFirstTimeTriggered) {
				this.isUserEligibleAlertFirstTimeTriggered = true;
				Alert.alert(
					`${I18n.t("events_info")}`,
					`${I18n.t("events_already_registered")}`,
					[
						{
							text: `${I18n.t("events_cancel")}`,
							onPress: () => {
								GoAppAnalytics.trackWithProperties("events-registration", {
									isUserAlreadyRegistered: true
								});
								this.props.navigation.goBack();
							},
							style: "cancel"
						},
						{
							text: `${I18n.t("events_ok")}`,
							onPress: () => {
								GoAppAnalytics.trackWithProperties("events-registration", {
									isUserAlreadyRegistered: true
								});
								this.props.navigation.goBack();
							}
						}
					],
					{ cancelable: false }
				);
			}
		}
		if (
			(this.props.isUserEligible === null &&
				this.props.isUserEligibleError === true) ||
			(nextProps.isUserEligible === null &&
				nextProps.isUserEligibleError === true)
		) {
			if (!this.isUserEligibleAlertFirstTimeTriggered) {
				this.isUserEligibleAlertFirstTimeTriggered = true;
				Alert.alert(
					`${I18n.t("events_info")}`,
					`${I18n.t("events_something_went_wrong")}`,
					[
						{
							text: `${I18n.t("events_cancel")}`,
							onPress: () => this.props.navigation.goBack(),
							style: "cancel"
						},
						{
							text: `${I18n.t("events_ok")}`,
							onPress: () => this.props.navigation.goBack()
						}
					],
					{ cancelable: false }
				);
			}
		}
		if (
			this.props.selectedEvent === null &&
			nextProps.selectedEvent !== null &&
			typeof nextProps.selectedEvent === "object"
		) {
			let isEventValid = this.checkMandatoryFieldsExistsInAdditionalOrDefaultFields(
				nextProps.selectedEvent
			);
			if (!isEventValid) {
				this.setState({ isEventError: true });
			}
			this.checkExpiry(nextProps.selectedEvent);
		}
		return true;
	}

	checkMandatoryFieldsExistsInAdditionalOrDefaultFields = selectedEvent => {
		let mandatoryFields = selectedEvent.mandatory_fields;
		let additionalFields = selectedEvent.additional_fields.reduce(
			(acc, curVal) => [...acc, curVal.field_name],
			[]
		);
		let templateFields = Object.keys(selectedEvent.template_info.fields);
		let allFields = [...additionalFields, ...templateFields];
		if (mandatoryFields) {
			for (const mandatoryField of mandatoryFields) {
				if (!allFields.includes(mandatoryField)) {
					return false;
				}
				continue;
			}
		}
		return true;
	};

	validateMandatoryFields = () => {
		// Number of tickets or persons to consider
		let numberOfTickets =
			this.props.selectedEvent.event_type === "group"
				? this.state.numberOfPersons
				: this.state.numberOfTickets;

		// Check for main user inputs with mandatory fields that contain data or empty
		let isMandatoryFieldsFilled =
			this.props.selectedEvent.mandatory_fields &&
			this.props.selectedEvent.mandatory_fields.every(
				item =>
					this.state.inputs[item] && this.state.inputs[item].trim().length > 0
			);

		if (isMandatoryFieldsFilled === null || isMandatoryFieldsFilled === true) {
			// Check minimum number of tickets
			let isAdditionalMandatoryFieldsFilled =
				numberOfTickets > 1 ? this.state.isAdditionalAttendeeInputsValid : true;
			if (isAdditionalMandatoryFieldsFilled) {
				// Check For paid or free event
				if (this.props.selectedEvent.is_paid) {
					// Create Order
					this.createOrder();
				} else {
					this.registerFreeEvent();
				}
			} else {
				this.props.navigation.navigate("info", {
					inputModel: this.state.inputModel,
					// subtract one ticket as main user details are filled in register screen
					numberOfTickets: numberOfTickets - 1,
					additionalAttendeeInputs: this.state.additionalAttendeeInputs,
					mandatoryFields: this.props.selectedEvent.mandatory_fields,
					updatedAttendeeData: this.updatedAttendeeData,
					handleAddInfoBack: this.handleAddInfoBack
				});
				GoToast.show("Please fill mandatory fields", "Error", 3000);
			}
		} else {
			GoToast.show("Please fill mandatory fields", "Error", 3000);
		}
	};

	registerFreeEvent = async () => {
		let bookingObject = {
			amount: 0,
			product: {
				// TODO: change hardcoded name to username
				name: this.props.user.name,
				event_name: this.props.selectedEvent.name,
				booking_details: [
					this.state.inputs,
					...this.state.additionalAttendeeInputs
				],
				quantity: this.state.numberOfTickets,
				event_id: this.props.selectedEvent.event_id
			}
		};

		try {
			let registerForEvent = await fetch(
				SERVER_BASE_URL_EVENTS + "/registerEvent",
				{
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": this.props.token,
						"Content-Type": "application/json"
					},
					body: JSON.stringify(bookingObject)
				}
			);
			let registerResp = await registerForEvent.json();
			if (registerResp.success === 1) {
				GoAppAnalytics.trackWithProperties("events-registration", {
					isPaidEvent: false,
					event_id: this.props.selectedEvent.event_id,
					isRegistrationSuccess: true
				});
				const resetAction = CommonActions.reset({
					index: 0,
					routes: [{ name: "payment_success" }]
				});
				this.props.navigation.dispatch(resetAction);
			} else {
				Alert.alert(
					`${I18n.t("events_oops")}`,
					`${I18n.t("events_something_went_wrong")}`,
					[
						{
							text: `${I18n.t("events_cancel")}`,
							onPress: () => {
								GoAppAnalytics.trackWithProperties("events-registration", {
									isPaidEvent: false,
									event_id: this.props.selectedEvent.event_id,
									isRegistrationSuccess: false
								});
								console.log("Cancel Pressed");
							},
							style: "cancel"
						},
						{
							text: `${I18n.t("events_ok")}`,
							onPress: () => {
								GoAppAnalytics.trackWithProperties("events-registration", {
									isPaidEvent: false,
									event_id: this.props.selectedEvent.event_id,
									isRegistrationSuccess: false
								});
								console.log("OK Pressed");
							}
						}
					],
					{ cancelable: false }
				);
			}
		} catch (error) {
			GoAppAnalytics.trackWithProperties("events-registration", {
				isPaidEvent: false,
				event_id: this.props.selectedEvent.event_id,
				isRegistrationSuccess: false
			});
			Alert.alert(
				`${I18n.t("events_oops")}`,
				`${I18n.t("events_something_went_wrong")}`,
				[
					{
						text: `${I18n.t("events_cancel")}`,
						onPress: () => console.log("Cancel Pressed"),
						style: "cancel"
					},
					{
						text: `${I18n.t("events_ok")}`,
						onPress: () => console.log("OK Pressed")
					}
				],
				{ cancelable: false }
			);
			console.log(error);
		}
	};

	createOrder = async () => {
		this.setState({ isPaymentLoading: true, isRegisterDisabled: true });
		let amount =
			this.state.numberOfTickets * this.state.selectedTicketType.ticket_price;
		let orderObject = {
			amount,
			product: {
				registration_data: {
					event_id: this.props.selectedEvent.event_id,
					request: {
						product: {
							event_name: this.props.selectedEvent.name
						}
					},
					quantity: this.state.numberOfTickets,
					selected_ticket_type: this.state.selectedTicketType,
					name: this.props.user.name,
					booking_details: [
						this.state.inputs,
						...this.state.additionalAttendeeInputs
					]
				}
			}
		};
		try {
			GoAppAnalytics.trackWithProperties("events-registration", {
				orderStatus: "initiated",
				event_id: this.props.selectedEvent.event_id
			});
			let orderIdResp = await fetch(SERVER_BASE_URL_EVENTS + "/createOrder", {
				method: "POST",
				headers: {
					"X-ACCESS-TOKEN": this.props.token,
					"Content-Type": "application/json"
				},
				body: JSON.stringify(orderObject)
			});
			let orderData = await orderIdResp.json();
			console.log(orderData);
			if (
				"success" in orderData &&
				orderData.success === 1 &&
				"order_id" in orderData
			) {
				GoAppAnalytics.trackWithProperties("events-registration", {
					orderStatus: "payment-pending",
					event_id: this.props.selectedEvent.event_id
				});
				this.makePayment(orderData.order_id, amount);
			} else {
				GoAppAnalytics.trackWithProperties("events-registration", {
					orderStatus: "failed",
					event_id: this.props.selectedEvent.event_id
				});
				this.setState({ isPaymentLoading: false, isRegisterDisabled: false });
				Alert.alert(
					`${I18n.t("events_oops")}`,
					`${I18n.t("events_cannot_register")}`,
					[
						{
							text: `${I18n.t("events_cancel")}`,
							onPress: () => console.log("Cancel Pressed"),
							style: "cancel"
						},
						{
							text: `${I18n.t("events_ok")}`,
							onPress: () => console.log("OK Pressed")
						}
					],
					{ cancelable: false }
				);
			}
		} catch (error) {
			GoAppAnalytics.trackWithProperties("events-registration", {
				orderStatus: "failed",
				event_id: this.props.selectedEvent.event_id
			});
			this.setState({ isPaymentLoading: false, isRegisterDisabled: false });
			console.log(error);
		}
	};
	trimMobileNumber = contact => {
		try {
			if (contact.startsWith("+91-")) {
				return contact.slice(4);
			} else if (contact.startsWith("+91")) {
				return contact.slice(3);
			} else {
				return contact;
			}
		} catch (err) {
			return contact;
		}
	};

	makePayment = async (orderId, amount) => {
		let contactNumber = await this.trimMobileNumber(this.props.user.contact);
		RazorPay({
			app: "Events",
			description: `Payment for ${this.props.selectedEvent.name}`,
			amount: amount,
			name: this.props.user.name,
			contact: contactNumber,
			email: this.props.user.email,
			order_id: orderId
		})
			.then(response => {
				let orderObject = {
					order_id: orderId,
					payment_status: 1,
					payment_id: response.razorpay_payment_id,
					failure_reason: "None"
				};
				try {
					GoAppAnalytics.trackChargeWithProperties(
						parseInt(amount, 10),
						response.razorpay_payment_id
					);
				} catch (error) {
					console.log("Error GoAppAnalytics.trackChargeWithProperties", error);
				}
				GoAppAnalytics.trackWithProperties("events-registration", {
					orderStatus: "payment-success",
					orderId,
					event_id: this.props.selectedEvent.event_id
				});
				this.confirmOrder(orderObject);
			})
			.catch(error => {
				GoAppAnalytics.trackWithProperties("events-registration", {
					orderStatus: "payment-failed",
					event_id: this.props.selectedEvent.event_id
				});
				this.setState({ isPaymentLoading: false, isRegisterDisabled: false });
				if (error.code === 0 || error.code === 2) {
					Alert.alert(
						`${I18n.t("events_cancelled")}`,
						`${I18n.t("events_payment_cancelled_user")}`,
						[
							{
								text: `${I18n.t("events_cancel")}`,
								onPress: () => console.log("Cancel Pressed"),
								style: "cancel"
							},
							{
								text: `${I18n.t("events_ok")}`,
								onPress: () => console.log("OK Pressed")
							}
						],
						{ cancelable: false }
					);
				}
			});
	};

	confirmOrder = async orderData => {
		try {
			let confirmResp = await fetch(SERVER_BASE_URL_EVENTS + "/confirmOrder", {
				method: "POST",
				headers: {
					"X-ACCESS-TOKEN": this.props.token,
					"Content-Type": "application/json"
				},
				body: JSON.stringify(orderData)
			});

			let confirmData = await confirmResp.json();
			console.log(confirmData);
			if (confirmData.success === 1) {
				GoAppAnalytics.trackWithProperties("events-registration", {
					orderStatus: "booking-confirmed",
					event_id: this.props.selectedEvent.event_id
				});
				GoAppAnalytics.trackWithProperties("events-registration", {
					currentScreen: "Events Registration",
					nextScreen: "Events Registration Success"
				});
				const resetAction = CommonActions.reset({
					index: 0,
					routes: [{ name: "payment_success" }]
				});
				this.props.navigation.dispatch(resetAction);
			} else {
				GoAppAnalytics.trackWithProperties("events-registration", {
					orderStatus: "confirmation-failed",
					event_id: this.props.selectedEvent.event_id
				});
				this.setState({ isPaymentLoading: false, isRegisterDisabled: false });
				Alert.alert(
					`${I18n.t("events_unable_booking_confirm")}`,
					`${I18n.t("events_dont_panic")}`,
					[
						{
							text: `${I18n.t("events_cancel")}`,
							onPress: () => console.log("Cancel Pressed"),
							style: "cancel"
						},
						{
							text: `${I18n.t("events_ok")}`,
							onPress: () => console.log("OK Pressed")
						}
					],
					{ cancelable: false }
				);
			}
		} catch (error) {
			GoAppAnalytics.trackWithProperties("events-registration", {
				orderStatus: "confirmation-failed",
				event_id: this.props.selectedEvent.event_id
			});
			this.setState({ isPaymentLoading: false, isRegisterDisabled: false });
			Alert.alert(
				`${I18n.t("events_unable_booking_confirm")}`,
				`${I18n.t("events_dont_panic")}`,
				[
					{
						text: `${I18n.t("events_cancel")}`,
						onPress: () => console.log("Cancel Pressed"),
						style: "cancel"
					},
					{
						text: `${I18n.t("events_ok")}`,
						onPress: () => console.log("OK Pressed")
					}
				],
				{ cancelable: false }
			);
		}
	};

	createInputObj = (templateObj, additionalFieldsArr) => {
		let templateFields = Object.keys(templateObj).reduce((acc, curVal) => {
			return { ...acc, [curVal]: "" };
		}, []);
		let additionalFields = additionalFieldsArr.reduce((acc, curVal) => {
			// if (curVal.type === "picker")
			// return { ...acc, [curVal.field_name]: curVal.picker_options[0] }
			return { ...acc, [curVal.field_name]: "" };
		}, []);

		return { ...templateFields, ...additionalFields };
	};

	static getDerivedStateFromProps(nextProps, prevState) {
		let nextState = {};
		if (
			nextProps.selectedTicketType !== prevState.selectedTicketType &&
			prevState.selectedTicketType === null
		) {
			nextState = {
				...nextState,
				selectedTicketType: nextProps.selectedTicketType
			};
		}
		if (
			prevState.numberOfPersons === 0 &&
			nextProps.selectedEvent &&
			nextProps.selectedEvent.event_type === "group"
		) {
			let templateFields = Object.keys(
				nextProps.selectedEvent.template_info.fields
			).reduce((acc, curVal) => {
				return { ...acc, [curVal]: "" };
			}, []);
			let additionalFields = nextProps.selectedEvent.additional_fields.reduce(
				(acc, curVal) => {
					// if (curVal.type === "picker")
					// return { ...acc, [curVal.field_name]: curVal.picker_options[0] }
					return { ...acc, [curVal.field_name]: "" };
				},
				[]
			);
			let additionalAttendeeInput = { ...templateFields, ...additionalFields };
			let additionalAttendeeInputs = Array(
				nextProps.selectedEvent.template_info.criteria.min_members - 1
			).fill(additionalAttendeeInput);
			nextState = {
				...nextState,
				numberOfPersons:
					nextProps.selectedEvent.template_info.criteria.min_members,
				additionalAttendeeInputs: additionalAttendeeInputs
			};
		}

		if (
			Object.keys(prevState.inputs).length === 0 &&
			nextProps.selectedEvent !== null
		) {
			let templateFields = Object.keys(
				nextProps.selectedEvent.template_info.fields
			).reduce((acc, curVal) => {
				return { ...acc, [curVal]: "" };
			}, []);
			let additionalFields = nextProps.selectedEvent.additional_fields.reduce(
				(acc, curVal) => {
					// if (curVal.type === "picker")
					// return { ...acc, [curVal.field_name]: curVal.picker_options[0] }
					return { ...acc, [curVal.field_name]: "" };
				},
				[]
			);

			let inputModel = Object.keys(
				nextProps.selectedEvent.template_info.fields
			).reduce((acc, curVal) => {
				return [
					...acc,
					{
						field_name: curVal,
						type: nextProps.selectedEvent.template_info.fields[curVal]
					}
				];
			}, []);
			inputModel = [
				...inputModel,
				...nextProps.selectedEvent.additional_fields
			];
			nextState = {
				...nextState,
				inputs: { ...templateFields, ...additionalFields },
				inputModel
			};
		}
		return nextState;
	}

	updatedAttendeeData = inputData => {
		this.setState({
			additionalAttendeeInputs: inputData,
			isAdditionalAttendeeInputsValid: true
		});
	};

	handleAddInfoBack = inputData => {
		this.setState({
			additionalAttendeeInputs: inputData
		});
	};

	handlePersonOrTicketIncrement = (eventType = "ticket") => {
		if (eventType === "ticket") {
			if (this.state.numberOfTickets > 1) {
				this.setState((state, props) => {
					return {
						numberOfTickets: state.numberOfTickets - 1,
						additionalAttendeeInputs: state.additionalAttendeeInputs.splice(
							-1,
							1
						)
					};
				});
			} else {
				GoToast.show(
					`${I18n.t("events_one_ticket")}`,
					`${I18n.t("events_info")}`,
					1000
				);
			}
		} else {
			if (
				this.state.numberOfPersons >
				this.props.selectedEvent.template_info.criteria.min_members
			) {
				this.setState((state, props) => {
					return {
						numberOfPersons: state.numberOfPersons - 1,
						additionalAttendeeInputs: state.additionalAttendeeInputs.splice(
							-1,
							1
						)
					};
				});
			} else {
				GoToast.show(
					`Minimum of ${
						this.props.selectedEvent.template_info.criteria.min_members
					} required`,
					"Warning",
					1000
				);
			}
		}
	};

	handlePersonOrTicketDecrement = (eventType = "ticket") => {
		if (eventType === "ticket") {
			if (this.state.numberOfTickets < this.props.selectedEvent.max_tickets) {
				this.setState((state, props) => {
					let additionalAttendeeInputs = this.createInputObj(
						this.props.selectedEvent.template_info.fields,
						this.props.selectedEvent.additional_fields
					);
					return {
						numberOfTickets: state.numberOfTickets + 1,
						additionalAttendeeInputs: [
							...state.additionalAttendeeInputs,
							additionalAttendeeInputs
						]
					};
				});
			} else {
				GoToast.show(
					`${I18n.t("events_max_ticket_reached")}`,
					`${I18n.t("events_info")}`,
					1000
				);
			}
		} else {
			if (
				this.state.numberOfPersons <
				this.props.selectedEvent.template_info.criteria.max_members
			) {
				this.setState((state, props) => {
					let additionalAttendeeInputs = this.createInputObj(
						this.props.selectedEvent.template_info.fields,
						this.props.selectedEvent.additional_fields
					);
					return {
						numberOfPersons: state.numberOfPersons + 1,
						additionalAttendeeInputs: [
							...state.additionalAttendeeInputs,
							additionalAttendeeInputs
						]
					};
				});
			} else {
				GoToast.show(
					`${I18n.t("events_max_person_reached")}`,
					`${I18n.t("events_info")}`,
					1000
				);
			}
		}
	};

	renderPersonsOrTickets = (eventType = "ticket") => {
		return (
			<React.Fragment>
				<EventsTextMedium style={{ color: "#fff", paddingBottom: width / 40 }}>
					Number of {eventType === "ticket" ? "Tickets" : "Persons"}
				</EventsTextMedium>
				<View
					style={{
						flexDirection: "row",
						paddingBottom: width / 40
					}}>
					<TouchableOpacity
						onPress={() =>
							this.handlePersonOrTicketIncrement(
								this.props.selectedEvent.event_type === "group"
									? "group"
									: "ticket"
							)
						}
						style={{
							backgroundColor: "#6C78FF",
							paddingHorizontal: width / 30,
							paddingVertical: width / 70
						}}>
						<EventsTextBold style={{ color: "#fff", fontSize: width / 24 }}>
							-
						</EventsTextBold>
					</TouchableOpacity>
					<EventsTextRegular
						style={{
							backgroundColor: "#6C78FF",
							color: "#fff",
							paddingHorizontal: width / 30,
							paddingVertical: width / 70
						}}>
						{this.props.selectedEvent.event_type === "group"
							? this.state.numberOfPersons
							: this.state.numberOfTickets}
					</EventsTextRegular>
					<TouchableOpacity
						onPress={() =>
							this.handlePersonOrTicketDecrement(
								this.props.selectedEvent.event_type === "group"
									? "group"
									: "ticket"
							)
						}
						style={{
							backgroundColor: "#6C78FF",
							paddingHorizontal: width / 30,
							paddingVertical: width / 70
						}}>
						<EventsTextBold style={{ color: "#fff", fontSize: width / 24 }}>
							+
						</EventsTextBold>
					</TouchableOpacity>
				</View>
			</React.Fragment>
		);
	};
	ticketTypeHandler = selectedItem => {
		let selectedValue = this.props.selectedEvent.price_info.find(
			item => item.ticket_type === selectedItem
		);
		this.setState({
			selectedTicketType: selectedValue
		});
	};

	render() {
		const {
			selectedEvent,
			isLoading,
			isError,
			isUserEligibleLoading,
			isUserEligible,
			isUserEligibleError
		} = this.props;
		let newPriceInfo = [];

		selectedEvent &&
			selectedEvent.price_info &&
			selectedEvent.price_info.map(item =>
				newPriceInfo.push({ name: item.ticket_type })
			);

		const addInfo =
			this.props.selectedEvent &&
			this.props.selectedEvent.event_type === "group"
				? this.state.numberOfPersons > 1
				: this.state.numberOfTickets > 1;
		if (isLoading) {
			return (
				<View
					style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
					<ActivityIndicator size={"large"} />
				</View>
			);
		}
		if (isError) {
			return (
				<EventsTextBold>{I18n.t("events_something_went_wrong")}</EventsTextBold>
			);
		}
		if (isUserEligibleLoading) {
			return (
				<View
					style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
					<ActivityIndicator size={"large"} />
				</View>
			);
		}
		if (isUserEligible) {
			return (
				<View style={styles.container}>
					<StatusBar backgroundColor={"#3d43dd"} />
					<DrawerHeader
						navigation={() => this.props.navigation.goBack()}
						isHeaderName={true}
						headerName={"Registration Details"}
						isGoBack={true}
					/>
					<HeaderHeightContext.Consumer>
						{headerHeight => (
							<KeyboardAvoidingView
								{...(Platform.OS === "ios" ? { behavior: "padding" } : {})}
								keyboardVerticalOffset={Platform.select({
									ios: headerHeight + ifIphoneX(24, 0),
									android: 0
								})}
								style={{ flex: 1 }}>
								<ScrollView
									style={{ flex: 1, width: width * 0.9 }}
									keyboardShouldPersistTaps={"always"}
									showsVerticalScrollIndicator={false}>
									<EventsTextBold
										style={{
											alignSelf: "flex-start",
											paddingVertical: width / 25,
											fontSize: width / 22
										}}>
										{I18n.t("events_ticket_info")}
									</EventsTextBold>
									<View
										style={{
											backgroundColor: "#505EFF",
											paddingHorizontal: width / 30,
											paddingVertical: width / 20,
											borderRadius: width / 100,
											elevation: 3,
											shadowOffset: {
												width: 2,
												height: 2
											},
											shadowColor: "#000",
											shadowOpacity: 0.2,
											shadowRadius: 4
										}}>
										<EventsTextMedium style={{ color: "#fff" }}>
											{I18n.t("events_ticket_type")}
										</EventsTextMedium>
										<View
											style={{
												flexDirection: "row",
												justifyContent: "space-between"
											}}>
											{this.props.selectedEvent.is_paid ? (
												<Dropdown
													data={newPriceInfo}
													onPress={this.ticketTypeHandler}
													dataStyle={styles.chooseDateText}
												/>
											) : null}
											<View
												style={{
													backgroundColor: "#FF7234",
													alignItems: "center",
													justifyContent: "center",
													alignSelf: "center",
													paddingHorizontal: width / 20,
													paddingVertical: width / 60,
													elevation: 5,
													shadowOffset: {
														width: 2,
														height: 2
													},
													shadowColor: "#000",
													shadowOpacity: 0.2,
													shadowRadius: 4,
													borderRadius: width / 80
												}}>
												<EventsTextRegular style={{ color: "#fff" }}>
													{this.state.selectedTicketType === null
														? I18n.t("events_free")
														: this.state.selectedTicketType.ticket_currency +
														  " " +
														  this.state.selectedTicketType.ticket_price +
														  "/-"}
												</EventsTextRegular>
											</View>
										</View>

										{this.renderPersonsOrTickets(
											this.props.selectedEvent.event_type === "individual"
												? "ticket"
												: "people"
										)}
										{addInfo ? (
											<React.Fragment>
												<EventsTextBold
													style={{ color: "#fff", paddingBottom: width / 40 }}>
													{I18n.t("events_who_is_joining")}
												</EventsTextBold>
												<TouchableOpacity
													onPress={() => {
														let numberOfTickets =
															this.props.selectedEvent.event_type === "group"
																? this.state.numberOfPersons - 1
																: this.state.numberOfTickets - 1;
														this.props.navigation.navigate("info", {
															inputModel: this.state.inputModel,
															numberOfTickets: numberOfTickets,
															additionalAttendeeInputs: this.state
																.additionalAttendeeInputs,
															mandatoryFields: this.props.selectedEvent
																.mandatory_fields,
															updatedAttendeeData: this.updatedAttendeeData,
															handleAddInfoBack: this.handleAddInfoBack
														});
													}}
													style={{
														backgroundColor: "#6C78FF",
														paddingHorizontal: width / 30,
														paddingVertical: width / 70,
														alignSelf: "flex-start"
													}}>
													<EventsTextBold
														style={{ color: "#fff", fontSize: width / 24 }}>
														+ {I18n.t("events_add_info")}
													</EventsTextBold>
												</TouchableOpacity>
											</React.Fragment>
										) : null}
									</View>
									<View
										style={{
											flex: 1,
											width: width * 0.9,
											marginTop: width / 40
										}}>
										<EventsTextBold
											style={{
												alignSelf: "flex-start",
												fontSize: width / 22
											}}>
											{I18n.t("event_your_info")}
										</EventsTextBold>
										<View
											style={{
												backgroundColor: "#F6F6F6",
												paddingHorizontal: width / 20,
												paddingVertical: width / 20
											}}>
											{this.state.inputModel.map((inputItem, inputIndex) => {
												return (
													<View
														key={inputIndex}
														style={{
															flexDirection: "row",
															alignItems: "center",
															paddingHorizontal: width / 80,
															paddingVertical: width / 50
														}}>
														<EventsTextBold style={{ width: width / 3 }}>
															{capitalize(inputItem.field_name)}
															{this.props.selectedEvent.mandatory_fields ? (
																this.props.selectedEvent.mandatory_fields.includes(
																	inputItem.field_name
																) ? (
																	<EventsTextBold>*</EventsTextBold>
																) : (
																	""
																)
															) : (
																""
															)}
														</EventsTextBold>
														<SuperInput
															fieldDetails={inputItem}
															value={this.state.inputs[inputItem.field_name]}
															onChangeValue={text =>
																this.setState((state, props) => {
																	return {
																		inputs: {
																			...state.inputs,
																			[inputItem.field_name]: text
																		}
																	};
																})
															}
														/>
													</View>
												);
											})}
										</View>
									</View>
								</ScrollView>
							</KeyboardAvoidingView>
						)}
					</HeaderHeightContext.Consumer>
					{this.props.selectedEvent.is_paid ? (
						<EventsTextBold
							style={{
								paddingBottom: width / 60,
								paddingTop: width / 60,
								fontSize: width / 22,
								alignSelf: "flex-start",
								paddingHorizontal: width / 20
							}}>
							Total price for {this.state.numberOfTickets} ticket
							{this.state.numberOfTickets === 1 ? "" : "s"} is:
						</EventsTextBold>
					) : null}
					<View
						style={{
							// position: "absolute",
							// bottom: 0,
							width: width * 0.9,
							backgroundColor: "#F2F2F2",
							flexDirection: "row",
							alignItems: "center",
							justifyContent: "space-around",
							paddingVertical: width / 40,
							marginBottom: width / 60
						}}>
						<View style={{ flexDirection: "row", alignItems: "center" }}>
							<Image
								source={require("../../../Assets/Img/cash.png")}
								resizeMode={"contain"}
								style={{
									width: width / 8,
									height: width / 8
								}}
							/>
							<EventsTextBold style={{ paddingLeft: width / 20 }}>
								{this.state.selectedTicketType !== null
									? this.state.selectedTicketType.ticket_currency +
									  " " +
									  this.state.numberOfTickets *
											this.state.selectedTicketType.ticket_price
									: I18n.t("events_free")}
							</EventsTextBold>
						</View>
						<TouchableOpacity
							disabled={this.state.isExpired || this.state.isRegisterDisabled}
							onPress={this.validateMandatoryFields}
							style={{
								backgroundColor: this.state.isExpired ? "grey" : "#505EFF",
								paddingVertical: width / 35,
								paddingHorizontal: width / 10,
								shadowOffset: {
									width: 2,
									height: 2
								},
								shadowColor: "#000",
								shadowOpacity: 0.2,
								shadowRadius: 4
							}}>
							<EventsTextRegular style={{ color: "#fff" }}>
								{this.state.isExpired
									? "Expired"
									: this.props.selectedEvent.is_paid
									? "Pay"
									: "Register"}
							</EventsTextRegular>
						</TouchableOpacity>
					</View>
					{this.state.isPaymentLoading ? (
						<View
							style={{
								position: "absolute",
								flex: 1,
								alignItems: "center",
								justifyContent: "center",
								bottom: 0,
								top: 0,
								left: 0,
								right: 0,
								backgroundColor: "#fff"
							}}>
							<ActivityIndicator size={"large"} />
							<EventsTextBold>
								{I18n.t("events_payment_progress")}
							</EventsTextBold>
						</View>
					) : null}
				</View>
			);
		}
		if (isUserEligible !== null && isUserEligible === false) {
			return (
				<View
					style={{
						flex: 1,
						alignItems: "center",
						justifyContent: "center",
						paddingHorizontal: width / 30
					}}>
					<EventsTextBold
						style={{
							textAlign: "center"
						}}>
						{I18n.t("events_already_registered")}
					</EventsTextBold>
				</View>
			);
		}
		if (isUserEligibleError) {
			return (
				<View
					style={{
						flex: 1,
						alignItems: "center",
						justifyContent: "center",
						paddingHorizontal: width / 30
					}}>
					<EventsTextBold
						style={{
							textAlign: "center"
						}}>
						{I18n.t("events_already_registered")}
					</EventsTextBold>
				</View>
			);
		}
	}
}

function mapStateToProps(state) {
	return {
		token: state.appToken.token,
		user: state.appToken.user,
		selectedEvent: state.events && state.events.selectedEvent.event,
		isLoading: state.events && state.events.selectedEvent.isLoading,
		isError: state.events && state.events.selectedEvent.isError,
		isUserEligible: state.events && state.events.selectedEvent.isUserEligible,
		isUserEligibleLoading:
			state.events && state.events.selectedEvent.isUserEligibleLoading,
		isUserEligibleError:
			state.events && state.events.selectedEvent.isUserEligibleError,
		selectedTicketType:
			state.events && state.events.selectedEvent.selectedTicketType
	};
}

export default connect(mapStateToProps)(Registration);
