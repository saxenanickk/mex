import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	Platform,
	ActivityIndicator
} from "react-native";
import ImageViewer from "react-native-image-zoom-viewer";
import { connect } from "react-redux";
import { Icon } from "../../../../Components";

const { width } = Dimensions.get("window");

class ImageCarousel extends Component {
	state = {
		current: 0
	};

	changeSelectedIndex = index => {
		this.setState({
			current: index
		});
	};

	render() {
		const images = [
			...this.props.selectedEvent.image.map(image => ({
				url: image
			})),
			...this.props.selectedEvent.banners.map(image => ({
				url: image
			}))
		];
		return (
			<View
				style={{
					position: "absolute",
					top: 0,
					bottom: 0,
					left: 0,
					right: 0
				}}>
				<ImageViewer
					imageUrls={images}
					loadingRender={() => <ActivityIndicator size={"large"} />}
					renderIndicator={() => null}
					renderArrowLeft={() => (
						<Icon
							iconName={"ios-arrow-dropleft"}
							iconType={"icomoon"}
							iconSize={width / 12}
							iconColor={"#fff"}
						/>
					)}
					renderArrowRight={() => (
						<Icon
							iconName={"ios-arrow-dropright"}
							iconType={"icomoon"}
							iconSize={width / 12}
							iconColor={"#fff"}
						/>
					)}
				/>
				<TouchableOpacity
					style={{ position: "absolute", top: width / 20, right: width / 20 }}
					onPress={() => this.props.navigation.goBack()}>
					<Icon
						iconName={Platform.OS === "android" ? "md-close" : "ios-close"}
						iconType={"ionicon"}
						iconSize={width / 12}
						iconColor={"#fff"}
					/>
				</TouchableOpacity>
			</View>
		);
	}
}

const mapStateToProps = state => ({
	selectedEvent: state.events && state.events.selectedEvent.event
});

export default connect(mapStateToProps)(ImageCarousel);
