import React, { Component } from "react";
import { View } from "react-native";
import { connect } from "react-redux";

import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import { CommonActions } from "@react-navigation/native";

class Splash extends Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		GoAppAnalytics.setPageView("Events", "splash");
		console.log(this.props, "I am the splash");
		let eventIdFromDeepLink = this.handleDeepLink(this.props.params);

		if (this.props.appToken) {
			if (eventIdFromDeepLink !== null) {
				GoAppAnalytics.trackWithProperties("events-navigate", {
					isAppOpenedByDeepLink: true,
					eventId: eventIdFromDeepLink,
					nextScreen: "EventsDetail"
				});
				this.props.navigation.dispatch(
					CommonActions.reset({
						index: 0,
						routes: [
							{
								name: "event_detail",
								params: {
									eventId: eventIdFromDeepLink,
									isAppOpenedByDeepLink: true
								}
							}
						]
					})
				);
			} else {
				GoAppAnalytics.trackWithProperties("events-navigate", {
					isAppOpenedByDeepLink: false,
					eventId: null,
					nextScreen: "Home"
				});
				this.props.navigation.dispatch(
					CommonActions.reset({
						index: 0,
						routes: [{ name: "Home", params: { isAppOpenedByDeepLink: false } }]
					})
				);
			}
		}
	}

	handleDeepLink = params => {
		let deepLinkParams = params ? JSON.parse(params) : null;
		if (deepLinkParams !== null) {
			return deepLinkParams.event_id;
		}
		return null;
	};

	componentDidUpdate(prevProps) {
		if (
			prevProps.appToken === null &&
			this.props.appToken !== prevProps.appToken
		) {
			let eventIdFromDeepLink = this.handleDeepLink(this.props.params);
			if (eventIdFromDeepLink !== null) {
				GoAppAnalytics.trackWithProperties("events-navigate", {
					isAppOpenedByDeepLink: true,
					eventId: eventIdFromDeepLink,
					nextScreen: "event_detail"
				});
				this.props.navigation.dispatch(
					CommonActions.reset({
						index: 0,
						routes: [
							{
								name: "event_detail",
								params: {
									eventId: eventIdFromDeepLink,
									isAppOpenedByDeepLink: true
								}
							}
						]
					})
				);
			} else {
				GoAppAnalytics.trackWithProperties("events-navigate", {
					isAppOpenedByDeepLink: false,
					eventId: null,
					nextScreen: "Home"
				});
				this.props.navigation.dispatch(
					CommonActions.reset({
						index: 0,
						routes: [{ name: "Home", params: { isAppOpenedByDeepLink: false } }]
					})
				);
			}
		}
	}

	render() {
		return <View style={{ backgroundColor: "#000", flex: 1 }} />;
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token
	};
}

export default connect(mapStateToProps)(Splash);
