import React, { Component } from "react";
import {
	Text,
	View,
	Dimensions,
	Image,
	ScrollView,
	TouchableOpacity,
	StatusBar,
	ImageBackground,
	ActivityIndicator,
	Share
} from "react-native";
import { connect } from "react-redux";
import { Icon } from "../../../../Components";
import { DrawerHeader } from "../../Components/DrawerHeader";
import { WALLET, DefaultEvent } from "../../Assets/Img/Image";
import {
	EventsTextMedium,
	EventsTextRegular,
	EventsTextBold
} from "../../Components/EventsText";
import { styles } from "./style";
import {
	eventsGetSelectedEvent,
	eventsGetIsUserEligible,
	eventsSelectedTicketType
} from "./Saga";
import I18n from "../../Assets/Strings/i18n";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
const { width, height } = Dimensions.get("window");

class EventDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showEmptyPage: true
		};
	}

	componentDidMount() {
		const { route } = this.props;
		const { eventId } = route.params ? route.params : {};

		setTimeout(() => {
			this.setState({ showEmptyPage: false });
		}, 0);
		this.props.dispatch(
			eventsGetSelectedEvent({
				event_id: eventId,
				app_token: this.props.token
			})
		);
		this.props.dispatch(
			eventsGetIsUserEligible({
				app_token: this.props.token,
				event_id: eventId
			})
		);
	}

	onShare = async () => {
		try {
			const result = await Share.share({
				title: `Invite to join ${this.props.selectedEvent.name} event`,
				message: `Hey I am attending ${
					this.props.selectedEvent.name
				}, \nDo you wish to join with me? \nYou should check this out too!  \nhttps://apps.mexit.in/Events?event_id=${
					this.props.selectedEvent.event_id
				}`
			});

			if (result.action === Share.sharedAction) {
				if (result.activityType) {
					GoAppAnalytics.trackWithProperties("events-share", {
						isSharingSuccess: true,
						sharedWith: result.activityType
					});
					// shared with activity type of result.activityType
				} else {
					GoAppAnalytics.trackWithProperties("events-share", {
						isSharingSuccess: true,
						sharedWith: "unknown"
					});
					// shared
				}
			} else if (result.action === Share.dismissedAction) {
				// dismissed
				GoAppAnalytics.trackWithProperties("events-share", {
					isSharingSuccess: false
				});
			}
		} catch (error) {
			alert(error.message);
		}
	};

	handleBack = () => {
		this.props.navigation.goBack();
	};

	componentWillUnmount() {
		this.props.dispatch(eventsSelectedTicketType(null));
	}

	render() {
		if (this.state.showEmptyPage) {
			return null;
		}
		const { selectedEvent } = this.props;
		console.log(this.props);
		const images = this.props.selectedEvent
			? [...this.props.selectedEvent.image, ...this.props.selectedEvent.banners]
			: [];
		if (selectedEvent === null) {
			return (
				<View
					style={{
						flex: 1,
						alignItems: "center",
						justifyContent: "flex-start"
					}}>
					<StatusBar backgroundColor={"#3d43dd"} />
					<DrawerHeader
						navigation={this.handleBack}
						isHeaderName={true}
						headerName={"Event Details"}
						isGoBack={true}
					/>
					<View style={{ flex: 1, justifyContent: "center" }}>
						{this.props.isEventFetchError ? (
							<EventsTextBold>
								{I18n.t("events_fetch_failed_event_detail")}
							</EventsTextBold>
						) : (
							<ActivityIndicator size={"large"} />
						)}
					</View>
				</View>
			);
		}
		return (
			<View style={styles.container}>
				<StatusBar backgroundColor={"#3d43dd"} />
				<DrawerHeader
					navigation={this.handleBack}
					isHeaderName={true}
					headerName={"Event Details"}
					isGoBack={true}
				/>
				<ScrollView
					contentContainerStyle={styles.detailSection}
					style={{
						marginBottom: this.props.isUserEligible ? width / 8.5 : 0
					}}
					showsVerticalScrollIndicator={false}>
					{/* Main Image */}
					<TouchableOpacity
						disabled={!selectedEvent.image[0]}
						onPress={() => this.props.navigation.navigate("ImageCarousel")}>
						{selectedEvent.image[0] ? (
							<ImageBackground
								source={{ uri: selectedEvent.image[0] }}
								style={styles.imageDimension}
								imageStyle={{
									borderTopLeftRadius: width / 50,
									borderTopRightRadius: width / 50
								}}
								resizeMode={"cover"}>
								<View style={styles.tag}>
									<EventsTextMedium style={styles.eventType}>
										{selectedEvent.visibility === "generic"
											? I18n.t("events_open")
											: I18n.t("events_corporate")}
									</EventsTextMedium>
								</View>
								{/* <TouchableOpacity
									style={styles.shareButton}
									onPress={this.onShare}>
									<Image source={SHARE} style={styles.filterImage} />
								</TouchableOpacity> */}
							</ImageBackground>
						) : (
							<ImageBackground
								source={DefaultEvent}
								style={styles.imageDimension}
								imageStyle={{
									borderTopLeftRadius: width / 50,
									borderTopRightRadius: width / 50
								}}
								resizeMode={"cover"}>
								<View style={styles.tag}>
									<EventsTextMedium style={styles.eventType}>
										{selectedEvent.visibility === "generic"
											? I18n.t("events_open")
											: I18n.t("events_corporate")}
									</EventsTextMedium>
								</View>
								{/* <TouchableOpacity
									style={styles.shareButton}
									onPress={this.onShare}>
									<Image source={SHARE} style={styles.filterImage} />
								</TouchableOpacity> */}
							</ImageBackground>
						)}
					</TouchableOpacity>
					{/* Mini Images and Time */}
					<View
						style={{
							flexDirection: "row",
							alignItems: "center",
							justifyContent: "space-around",
							paddingTop: height / 80
						}}>
						<View
							style={{
								flex: 1,
								flexDirection: "row"
							}}>
							{/* Mini Images starts here */}
							{images.slice(1, 4).map((item, index) => {
								return (
									<TouchableOpacity
										key={index}
										onPress={() =>
											this.props.navigation.navigate("ImageCarousel")
										}>
										<ImageBackground
											source={{ uri: item }}
											style={{
												width: width / 8,
												height: width / 8,
												justifyContent: "center",
												marginHorizontal: height / 160,
												borderRadius: width / 16
											}}
											imageStyle={{ borderRadius: width / 50 }}
											resizeMode={"cover"}>
											{index === 2 ? (
												<View
													style={{
														flex: 1,
														backgroundColor: "rgba(0,0,0,0.7)",
														justifyContent: "center",
														alignItems: "center",
														borderRadius: width / 50
													}}>
													<EventsTextRegular
														style={{
															fontSize: height / 45,
															color: "#fff"
														}}>
														{"+"}
													</EventsTextRegular>
												</View>
											) : null}
										</ImageBackground>
									</TouchableOpacity>
								);
							})}
						</View>
						<EventsTextBold
							style={{
								marginRight: width / 25,
								backgroundColor: "#404040",
								color: "#fff",
								borderRadius: width / 20,
								paddingVertical: width / 180,
								paddingHorizontal: width / 50,
								fontSize: width / 36
							}}>
							{I18n.strftime(new Date(selectedEvent.start_time), "%d %b %Y")}
						</EventsTextBold>
					</View>
					<View style={styles.infoSection}>
						<View style={styles.innerInfoSection}>
							<EventsTextMedium style={styles.eventTitleText} numberOfLines={2}>
								{selectedEvent.name}
							</EventsTextMedium>
							{selectedEvent.venue_details ? (
								<View style={styles.locationView}>
									<Icon
										iconType={"ionicon"}
										iconSize={height / 45}
										iconName={"ios-pin"}
										iconColor={"#828282"}
										style={styles.picIconStyle}
									/>
									<EventsTextRegular
										style={styles.locationText}
										numberOfLines={2}>
										{selectedEvent.venue_details
											? selectedEvent.venue_details.address
											: ""}
									</EventsTextRegular>
								</View>
							) : null}
							<View
								style={[styles.locationView, { paddingVertical: width / 60 }]}>
								<Icon
									iconType={"ionicon"}
									iconSize={height / 45}
									iconName={"ios-time"}
									iconColor={"#828282"}
									style={styles.picIconStyle}
								/>
								<EventsTextRegular
									style={styles.locationText}
									numberOfLines={2}>
									{I18n.strftime(
										new Date(selectedEvent.start_time),
										"%I:%M %p"
									)}
								</EventsTextRegular>
							</View>
						</View>
					</View>
					{selectedEvent.is_paid ? (
						<View
							style={[
								styles.rateSection,
								{
									marginVertical: width / 50,
									alignItems: "center"
								}
							]}>
							<Image
								source={WALLET}
								style={styles.walletImage}
								resizeMode={"contain"}
							/>
							<View style={styles.rateTextView}>
								<EventsTextMedium style={styles.rateHeaderText}>
									{I18n.t("events_starting_from")}
								</EventsTextMedium>
								<EventsTextMedium style={styles.rateText}>
									{"₹ " +
										selectedEvent.min_ticket_price +
										` ${I18n.t("events_onward")}`}
								</EventsTextMedium>
							</View>
						</View>
					) : null}
					<View
						style={[
							styles.rateSection,
							{
								flexDirection: "column",
								alignItems: "flex-start",
								justifyContent: "center",
								paddingVertical: width / 30
							}
						]}>
						<EventsTextBold style={styles.rateHeaderText}>
							{I18n.t("events_eligibility_criteria")}
						</EventsTextBold>
						<EventsTextBold style={styles.rateText}>
							{selectedEvent.eligibility_criteria}
						</EventsTextBold>
					</View>
					<Text style={styles.descriptionTextView}>
						{selectedEvent.comments}
					</Text>
				</ScrollView>
				{this.props.isUserEligible ? (
					<View style={styles.eventRegisterContainer}>
						<TouchableOpacity
							style={styles.eventRegisterButton}
							onPress={() => {
								GoAppAnalytics.trackWithProperties("events-navigate", {
									currentScreen: "Event Detail",
									nextScreen: "Event Registration",
									eventId: selectedEvent.event_id
								});
								this.props.navigation.navigate("registration", {
									eventId: selectedEvent.event_id
								});
							}}>
							<Text style={styles.eventRegisterText}>Register</Text>
						</TouchableOpacity>
					</View>
				) : null}
				{this.props.isUserEligible === false ? (
					<View style={{ alignItems: "center", paddingHorizontal: width / 30 }}>
						<EventsTextBold style={{ textAlign: "center" }}>
							{I18n.t("events_already_registered")}
						</EventsTextBold>
					</View>
				) : null}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		selectedEvent: state.events && state.events.selectedEvent.event,
		token: state.appToken.token,
		isUserEligible: state.events && state.events.selectedEvent.isUserEligible,
		isEventFetchError: state.events && state.events.selectedEvent.isError
	};
}

export default connect(mapStateToProps)(EventDetail);
