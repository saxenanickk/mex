import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	container: { flex: 1, backgroundColor: "#f4f3f8", alignItems: "center" },
	detailSection: {
		width: 0.923 * width,
		alignSelf: "center",
		borderRadius: width / 50,
		backgroundColor: "#fff",
		paddingBottom: width / 50,
		paddingTop: width / 30,
		alignItems: "center"
	},
	imageDimension: {
		width: 0.923 * width,
		height: (0.923 * width) / 2
	},
	imageStyle: {
		borderTopLeftRadius: width / 50,
		borderTopRightRadius: width / 50
	},
	tag: {
		paddingVertical: height / 150,
		backgroundColor: "#e58e8e",
		width: width / 3,
		justifyContent: "center",
		alignItems: "center",
		borderBottomRightRadius: 5,
		borderTopLeftRadius: 5
	},
	eventType: {
		color: "#fff",
		fontSize: height / 65
	},
	infoSection: {
		width: 0.85 * width,
		paddingVertical: height / 60,
		flexDirection: "row",
		justifyContent: "space-between"
	},
	eventTitleText: {
		color: "#4a4a4a",
		fontSize: height / 45,
		paddingBottom: width / 80
	},
	innerInfoSection: {
		justifyContent: "space-around"
	},
	locationView: {
		flexDirection: "row",
		alignItems: "center"
	},
	locationText: {
		color: "#828282",
		fontSize: height / 55,
		paddingLeft: width / 60,
		width: "98%"
	},
	picIconStyle: {
		marginTop: height / 400
	},
	innerRightInfoSection: {
		height: height / 12,
		justifyContent: "space-between",
		alignItems: "flex-end"
	},
	dateView: {
		width: width / 5,
		paddingVertical: height / 130,
		borderRadius: width / 30,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#4b4b4b"
	},
	dateText: {
		color: "#fff",
		fontSize: height / 75
	},
	filterImage: {
		width: width / 25,
		height: width / 25
	},
	shareButton: {
		position: "absolute",
		right: 0,
		height: height / 20,
		width: width / 7,
		justifyContent: "center",
		alignItems: "center"
	},
	likeButton: {
		position: "absolute",
		right: 0,
		bottom: 0,
		height: height / 20,
		width: width / 7,
		justifyContent: "center",
		alignItems: "center"
	},
	eligibilityText: {
		color: "#000",
		fontSize: height / 50
	},
	rateSection: {
		width: 0.85 * width,
		borderRadius: width / 50,
		backgroundColor: "#f2f2f2",
		paddingHorizontal: width / 30,
		alignItems: "center",
		flexDirection: "row"
	},
	walletImage: {
		width: width / 12,
		height: width / 12
	},
	rateTextView: {
		marginLeft: width / 30,
		justifyContent: "center",
		paddingVertical: width / 30
	},
	rateText: {
		color: "#000"
	},
	rateHeaderText: {
		color: "#818181"
	},
	descriptionTextView: {
		paddingTop: width / 30,
		alignSelf: "flex-start",
		paddingHorizontal: width / 30
	},
	eventRegisterContainer: {
		position: "absolute",
		width: 0.923 * width,
		alignSelf: "center",
		borderRadius: width / 50,
		backgroundColor: "#fff",
		alignItems: "center",
		bottom: 0
	},
	eventRegisterButton: {
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "#505EFF",
		height: height / 20,
		width: width / 3.5,
		marginVertical: width / 60,
		borderRadius: width / 20
	},
	eventRegisterText: { color: "#fff", fontSize: width / 22 }
});
