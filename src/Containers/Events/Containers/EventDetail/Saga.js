import { takeLatest, put, call } from "redux-saga/effects";
import EventsAPI from "../../Api";

/**
 * constants
 */

export const EVENTS_GET_SELECTED_EVENT = "EVENTS_GET_SELECTED_EVENT";
export const EVENTS_SAVE_SELECTED_EVENT = "EVENTS_SAVE_SELECTED_EVENT";
export const EVENTS_SAVE_SELECTED_EVENT_ERROR =
	"EVENTS_SAVE_SELECTED_EVENT_ERROR";

export const EVENTS_GET_IS_USER_ELIGIBLE = "EVENTS_GET_IS_USER_ELIGIBLE";
export const EVENTS_SAVE_IS_USER_ELIGIBLE = "EVENTS_SAVE_IS_USER_ELIGIBLE";
export const EVENTS_SAVE_IS_USER_ELIGIBLE_ERROR =
	"EVENTS_SAVE_IS_USER_ELIGIBLE_ERROR";

export const EVENTS_SELECTED_TICKET_TYPE = "EVENTS_SELECTED_TICKET_TYPE";

/**
 * action creators
 */

export const eventsGetSelectedEvent = payload => ({
	type: EVENTS_GET_SELECTED_EVENT,
	payload
});

export const eventsSaveSelectedEvent = payload => ({
	type: EVENTS_SAVE_SELECTED_EVENT,
	payload
});

export const eventsSaveSelectedEventError = payload => ({
	type: EVENTS_SAVE_SELECTED_EVENT_ERROR,
	payload
});

export const eventsGetIsUserEligible = payload => ({
	type: EVENTS_GET_IS_USER_ELIGIBLE,
	payload
});

export const eventsSaveIsUserEligible = payload => ({
	type: EVENTS_SAVE_IS_USER_ELIGIBLE,
	payload
});

export const eventsSaveIsUserEligibleError = payload => ({
	type: EVENTS_SAVE_IS_USER_ELIGIBLE_ERROR,
	payload
});

export const eventsSelectedTicketType = payload => ({
	type: EVENTS_SELECTED_TICKET_TYPE,
	payload
});

export function* selectedEventSaga(dispatch) {
	yield takeLatest(EVENTS_GET_SELECTED_EVENT, handleFetchSelectedEvent);
	yield takeLatest(EVENTS_GET_IS_USER_ELIGIBLE, handleFetchIsUserEligible);
}

/**
 * Handlers
 */

function* handleFetchSelectedEvent(action) {
	try {
		let event = yield call(EventsAPI.fetchSelectedEvent, action.payload);
		if (event.image === null || event.image === undefined) {
			event.image = [];
		}
		if (event.banners === null || event.banners === undefined) {
			event.banners = [];
		}
		yield put(eventsSaveSelectedEvent(event));
		if (event.is_paid) {
			yield put(eventsSelectedTicketType(event.price_info[0]));
		} else {
			yield put(eventsSelectedTicketType(null));
		}
	} catch (error) {
		console.log(error);
		yield put(eventsSaveSelectedEventError(true));
	}
}

function* handleFetchIsUserEligible(action) {
	try {
		let isUserEligible = yield call(
			EventsAPI.fetchIsUserEligible,
			action.payload
		);
		yield put(eventsSaveIsUserEligible(!isUserEligible));
	} catch (error) {
		console.log(error);
		yield put(eventsSaveIsUserEligibleError(true));
	}
}
