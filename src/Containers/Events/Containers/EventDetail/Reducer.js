import {
	EVENTS_SAVE_SELECTED_EVENT,
	EVENTS_SAVE_SELECTED_EVENT_ERROR,
	EVENTS_GET_SELECTED_EVENT,
	EVENTS_GET_IS_USER_ELIGIBLE,
	EVENTS_SAVE_IS_USER_ELIGIBLE,
	EVENTS_SAVE_IS_USER_ELIGIBLE_ERROR,
	EVENTS_SELECTED_TICKET_TYPE
} from "./Saga";

const initialState = {
	event: null,
	isLoading: false,
	isError: false,
	isUserEligible: null,
	isUserEligibleLoading: false,
	isUserEligibleError: false,
	selectedTicketType: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case EVENTS_GET_SELECTED_EVENT:
			return {
				...state,
				event: null,
				isLoading: true,
				isError: false
			};

		case EVENTS_SAVE_SELECTED_EVENT:
			return {
				...state,
				event: action.payload,
				isLoading: false
			};

		case EVENTS_SAVE_SELECTED_EVENT_ERROR:
			return {
				...state,
				isError: true,
				event: null,
				isLoading: false
			};
		case EVENTS_GET_IS_USER_ELIGIBLE:
			return {
				...state,
				isUserEligible: null,
				isUserEligibleLoading: true,
				isUserEligibleError: false
			};

		case EVENTS_SAVE_IS_USER_ELIGIBLE:
			return {
				...state,
				isUserEligible: action.payload,
				isUserEligibleLoading: false
			};

		case EVENTS_SAVE_IS_USER_ELIGIBLE_ERROR:
			return {
				...state,
				isUserEligibleError: true,
				isUserEligibleLoading: false,
				isUserEligible: null
			};

		case EVENTS_SELECTED_TICKET_TYPE:
			return {
				...state,
				selectedTicketType: action.payload
			};

		default:
			return state;
	}
};
