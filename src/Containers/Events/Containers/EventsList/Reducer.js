import {
	EVENTS_SAVE_EVENT_LIST,
	EVENTS_GET_EVENT_LIST,
	EVENTS_SAVE_EVENT_LIST_ERROR,
	EVENTS_GET_ONGOING_EVENT_LIST,
	EVENTS_SAVE_ONGOING_EVENT_LIST,
	EVENTS_SAVE_ONGOING_EVENTS_LIST_ERROR,
	EVENTS_GET_CATEGORIES,
	EVENTS_SAVE_CATEGORIES,
	EVENTS_SAVE_CATEGORIES_ERROR,
	EVENTS_SAVE_SITES
} from "./Saga";
const initialState = {
	isLoading: false,
	eventsList: null,
	isError: false,
	isLoadingOngoing: false,
	eventsListOngoing: null,
	isErrorOngoing: false,
	isLoadingCategories: false,
	categories: [],
	isCategoriesError: false,
	sites: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case EVENTS_GET_EVENT_LIST:
			return {
				...state,
				isLoading: true,
				isError: false
			};
		case EVENTS_SAVE_EVENT_LIST:
			return {
				...state,
				eventsList: action.payload,
				isLoading: false
			};
		case EVENTS_SAVE_EVENT_LIST_ERROR:
			return {
				...state,
				isLoading: false,
				isError: true
			};
		case EVENTS_GET_ONGOING_EVENT_LIST:
			return {
				...state,
				isLoadingOngoing: true,
				isErrorOngoing: false
			};
		case EVENTS_SAVE_ONGOING_EVENT_LIST:
			return {
				...state,
				eventsListOngoing: action.payload,
				isLoadingOngoing: false
			};
		case EVENTS_SAVE_ONGOING_EVENTS_LIST_ERROR:
			return {
				...state,
				isLoadingOngoing: false,
				isErrorOngoing: true
			};

		case EVENTS_GET_CATEGORIES:
			return {
				...state,
				isLoadingCategories: true,
				isCategoriesError: false
			};

		case EVENTS_SAVE_CATEGORIES:
			return {
				...state,
				isLoadingCategories: false,
				categories: action.payload
			};
		case EVENTS_SAVE_CATEGORIES_ERROR:
			return {
				...state,
				isLoadingCategories: false,
				isCategoriesError: true
			};
		case EVENTS_SAVE_SITES:
			return {
				...state,
				sites: action.payload
			};
		default:
			return state;
	}
};
