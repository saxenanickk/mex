import React, { Component } from "react";
import {
	View,
	StyleSheet,
	Dimensions,
	FlatList,
	Image,
	ScrollView,
	TouchableOpacity,
	StatusBar,
	ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import I18n from "../../Assets/Strings/i18n";
import { EventsTextBold, EventsTextMedium } from "../../Components/EventsText";
import {
	eventsGetAllEventList,
	eventsGetAllOngoingEventsList,
	eventsGetSites
} from "./Saga";
import { DefaultEvent } from "../../Assets/Img/Image";
import { getMonthShortText } from "../../../../utils/dateUtils";
import {
	eventsGetIsUserEligible,
	eventsGetSelectedEvent
} from "../EventDetail/Saga";

import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import { DrawerHeader } from "../../Components/DrawerHeader";
import { CommonActions } from "@react-navigation/native";

const { height, width } = Dimensions.get("screen");

class EventsList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			events: [],
			ongoingEvents: [],
			selectedCategories: [],
			siteSelection: false,
			selectedSite: props.selectedSite
		};
		this.filterFlag = true;
		this.tempSiteId = null;
	}

	componentDidMount() {
		this.makeNeteworkCalls();
	}

	handleBackButtonClick = () => {
		this.props.navigation.goBack();
	};

	makeNeteworkCalls = () => {
		let date = new Date().toISOString();
		this.props.dispatch(
			eventsGetSites({
				app_token: this.props.token
			})
		);
		// upcoming
		this.props.dispatch(
			eventsGetAllEventList({
				app_token: this.props.token,
				args: "dateType=upcoming&limit=20&date=" + date
			})
		);
		// ongoing
		this.props.dispatch(
			eventsGetAllOngoingEventsList({
				app_token: this.props.token,
				args: "dateType=ongoing&limit=20&date=" + date
			})
		);
	};

	shouldComponentUpdate(nextProps, nextState) {
		if (nextProps.events && nextProps.events !== this.props.events) {
			this.eventFilter(nextProps.events, "events");
		}
		if (
			nextProps.ongoingEvents &&
			nextProps.ongoingEvents !== this.props.ongoingEvents
		) {
			this.eventFilter(nextProps.ongoingEvents, "ongoingEvents");
		}
		return true;
	}

	navigateToEventDetail = async event => {
		GoAppAnalytics.trackWithProperties("events-navigate", {
			currentScreen: "EventsHome",
			nextScreen: "EventDetail",
			eventId: event.id
		});
		this.navigateToScreen("event_detail", { eventId: event.id });
	};

	navigateToScreen = (routeName, params = {}) => {
		this.props.navigation.dispatch(
			CommonActions.navigate({
				name: routeName,
				params: params
			})
		);
	};

	getFormattedDate = dateString => {
		let date = new Date(dateString);
		let day = date.getDate();
		let month = getMonthShortText(date.getMonth());
		let year = date.getFullYear();
		return `${day} ${month} ${year}`;
	};

	eventFilter = (events, state) => {
		let EVENTS = events.slice();

		// Filter out the events based on selected site
		EVENTS = EVENTS.filter(
			item => item.site_details.site_id === this.props.selectedSite.id
		);

		// Setting new states of events & ongoing events
		this.setState({
			siteSelection: false,
			[state]: EVENTS
		});
	};

	renderEventCard = ({ item }) => (
		<TouchableOpacity
			activeOpacity={0.9}
			onPress={() => this.navigateToEventDetail(item)}>
			<View style={styles.eventCard}>
				<View style={{ flex: 0.2 }} />
				<View style={styles.infoContainer}>
					<View style={{ flex: 1 }} />
					<View style={styles.infoPanel}>
						<EventsTextMedium
							style={[styles.infoText]}
							numberOfLines={1}
							ellipsizeMode={"tail"}>
							{item.name}
						</EventsTextMedium>
						<View style={styles.textContainer}>
							<EventsTextMedium
								style={[styles.infoText, styles.locationAndTimeText]}
								numberOfLines={1}
								ellipsizeMode={"tail"}>
								{item.site_details.site_name.toUpperCase()}
							</EventsTextMedium>
							<EventsTextMedium
								style={[
									styles.infoText,
									styles.locationAndTimeText,
									{ textAlign: "right" }
								]}
								numberOfLines={1}
								ellipsizeMode={"tail"}>
								{I18n.strftime(new Date(item.start_time), "%a, %d %b")}
							</EventsTextMedium>
						</View>
						{item.canRegister ? (
							<TouchableOpacity
								style={styles.registerButton}
								onPress={() => {
									this.props.dispatch(
										eventsGetIsUserEligible({
											app_token: this.props.token,
											event_id: item.id
										})
									);
									this.props.dispatch(
										eventsGetSelectedEvent({
											app_token: this.props.token,
											event_id: item.id
										})
									);
									GoAppAnalytics.trackWithProperties("events-navigate", {
										currentScreen: "EventsHome",
										nextScreen: "Events Registration",
										eventId: item.id
									});
									this.navigateToScreen("registration", {
										eventId: item.id
									});
								}}>
								<EventsTextMedium style={styles.registerText} numberOfLines={1}>
									{I18n.t("events_register")}
								</EventsTextMedium>
							</TouchableOpacity>
						) : (
							<TouchableOpacity
								disabled
								style={[
									styles.registerButton,
									{ backgroundColor: "transparent" }
								]}>
								<EventsTextMedium
									style={[styles.registerText, { color: "green" }]}
									numberOfLines={1}>
									{I18n.t("events_in_progress")}
								</EventsTextMedium>
							</TouchableOpacity>
						)}
					</View>
				</View>
				<View style={styles.imageContainer}>
					{item.event_icon ? (
						<Image
							source={{ uri: item.event_icon }}
							style={styles.imageStyle}
						/>
					) : (
						<Image source={DefaultEvent} style={styles.imageStyle} />
					)}
				</View>
			</View>
		</TouchableOpacity>
	);

	handleCategoryPress = category => {
		let isCategoryInState = this.state.selectedCategories.some(element => {
			return category.id === element.id;
		});
		if (isCategoryInState) {
			let updatedSelectedCategories = this.state.selectedCategories.filter(
				item => item.id !== category.id
			);
			this.setState({
				selectedCategories: updatedSelectedCategories
			});
		} else {
			this.setState({
				selectedCategories: [...this.state.selectedCategories, category]
			});
		}
	};

	render() {
		return (
			<View style={styles.container}>
				<StatusBar backgroundColor={"#3d43dd"} />
				<DrawerHeader
					navigation={this.handleBackButtonClick}
					isHeaderName={true}
					headerName={"Events"}
					isGoBack={true}
					selectedSiteName={
						this.state.selectedSite && this.state.selectedSite.name
							? this.state.selectedSite.name
							: null
					}
				/>
				<ScrollView>
					<View style={styles.listHeader}>
						<EventsTextMedium style={styles.listHeaderText}>
							{I18n.t("events_upcoming")}
						</EventsTextMedium>
					</View>
					<FlatList
						contentContainerStyle={{}}
						showsHorizontalScrollIndicator={false}
						horizontal
						data={this.state.events}
						extraData={this.state.events}
						renderItem={this.renderEventCard}
						ListEmptyComponent={
							this.props.isLoading ? (
								<View
									style={{
										width: width,
										height: (3.5 * height) / 16,
										alignItems: "center",
										justifyContent: "center"
									}}>
									<ActivityIndicator />
								</View>
							) : (
								<View
									style={{
										width: width,
										height: (3.5 * height) / 16,
										alignItems: "center",
										justifyContent: "center"
									}}>
									<EventsTextBold>{I18n.t("events_no_events")}</EventsTextBold>
								</View>
							)
						}
						keyExtractor={item => item.id}
					/>
					{this.props.ongoingEvents && this.props.ongoingEvents.length ? (
						<View style={styles.listHeader}>
							<EventsTextMedium style={styles.listHeaderText}>
								{I18n.t("events_ongoing")}
							</EventsTextMedium>
						</View>
					) : null}
					{this.props.ongoingEvents && this.props.ongoingEvents.length ? (
						<FlatList
							contentContainerStyle={{}}
							horizontal
							showsHorizontalScrollIndicator={false}
							data={this.state.ongoingEvents}
							extraData={this.state.ongoingEvents}
							renderItem={this.renderEventCard}
							ListEmptyComponent={
								this.props.isLoading ? (
									<View
										style={{
											width: width,
											height: (3.5 * height) / 16,
											alignItems: "center",
											justifyContent: "center"
										}}>
										<ActivityIndicator />
									</View>
								) : (
									<View
										style={{
											width: width,
											height: (3.5 * height) / 16,
											alignItems: "center",
											justifyContent: "center"
										}}>
										<EventsTextBold>
											{I18n.t("events_no_events")}
										</EventsTextBold>
									</View>
								)
							}
							keyExtractor={item => item.id}
						/>
					) : null}
				</ScrollView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: { flex: 1, backgroundColor: "#f7f7f7" },
	dialogModalBackgroundStyle: {
		backgroundColor: "#000000",
		opacity: 0.6
	},
	modalWrapper: {
		flex: 1,
		position: "absolute",
		width: width / 1.1,
		height: height / 1.8,
		alignSelf: "center",
		borderRadius: height / 60,
		backgroundColor: "#ffffff",
		top: height / 5,
		justifyContent: "center",
		paddingHorizontal: width / 26.3
	},
	modalContainer: {
		flex: 1,
		alignItems: "flex-start"
	},
	appName: {
		flex: 1,
		flexDirection: "row",
		alignItems: "center",
		paddingVertical: width / 35
	},
	header: {
		width: width,
		backgroundColor: "#505eff",
		paddingHorizontal: width / 27.3,
		paddingVertical: height / 70,
		justifyContent: "space-between"
	},
	infoContainer: {
		flex: 0.8,
		backgroundColor: "#ffffff",
		borderRadius: 10,
		elevation: 5,
		shadowColor: "grey",
		shadowOpacity: 0.3,
		shadowRadius: 2,
		shadowOffset: {
			height: 1,
			width: 1
		},
		borderColor: "#ffffff"
	},
	infoPanel: {
		flex: 1.5,
		paddingHorizontal: 10,
		paddingBottom: 10,
		// alignItems: "center",
		justifyContent: "space-around"
	},
	textContainer: {
		paddingLeft: 2,
		paddingRight: 2,
		flexDirection: "row",
		alignItems: "center",
		alignSelf: "stretch",
		justifyContent: "space-between"
	},
	imageContainer: {
		backgroundColor: "#000000",
		position: "absolute",
		width: width / 2,
		height: width / 4,
		alignSelf: "center",
		top: 10,
		borderRadius: 10,
		elevation: 10,
		shadowColor: "grey",
		shadowOpacity: 0.3,
		shadowRadius: 2,
		shadowOffset: {
			height: 3,
			width: 3
		},
		borderColor: "#000000"
	},
	registerText: {
		fontSize: width / 35,
		color: "#ffffff"
	},
	infoText: {
		color: "#4d4d4d",
		fontSize: width / 28,
		alignItems: "center",
		justifyContent: "center"
		// textAlign: "center",
	},
	locationAndTimeText: {
		flex: 0.9,
		fontSize: width / 35,
		color: "#848484"
	},
	innerHeader: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	headerText: {
		color: "#fff",
		fontSize: height / 40
	},
	listHeader: {
		flexDirection: "row",
		paddingHorizontal: width / 27.3,
		justifyContent: "space-between",
		paddingVertical: height / 40
	},
	listHeaderText: {
		fontSize: height / 60,
		color: "#565658"
	},
	filterView: {
		alignItems: "center"
	},
	eventCard: {
		flex: 1,
		backgroundColor: "transparent",
		width: width / 1.65,
		height: width / 1.75,
		padding: 10
	},
	imageStyle: {
		width: width / 2,
		height: width / 4,
		borderRadius: 10,
		borderColor: "#000000"
	},
	registerButton: {
		alignSelf: "stretch",
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#4d6cee",
		borderRadius: 10,
		padding: 8
	},
	filterImage: {
		width: width / 30,
		height: width / 30
	}
});

function mapStateToProps(state) {
	return {
		isLoading: state.events && state.events.eventsList.isLoading,
		events: state.events && state.events.eventsList.eventsList,
		ongoingEvents: state.events && state.events.eventsList.eventsListOngoing,
		categories: state.events && state.events.eventsList.categories,
		sites: state.events && state.events.eventsList.sites,
		user: state.appToken.user,
		token: state.appToken.token,
		selectedSite:
			state.appToken && state.appToken.selectedSite
				? state.appToken.selectedSite
				: null
	};
}

export default connect(mapStateToProps)(EventsList);
