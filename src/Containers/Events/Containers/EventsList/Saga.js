import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import EventsAPI from "../../Api";

/**
 * constants
 */
// Below are for upcoming events
export const EVENTS_GET_EVENT_LIST = "EVENTS_GET_EVENT_LIST";
export const EVENTS_SAVE_EVENT_LIST = "EVENTS_SAVE_EVENT_LIST";
export const EVENTS_SAVE_EVENT_LIST_ERROR = "EVENTS_SAVE_EVENT_LIST_ERROR";

export const EVENTS_GET_ONGOING_EVENT_LIST = "EVENTS_GET_ONGOING_EVENT_LIST";
export const EVENTS_SAVE_ONGOING_EVENT_LIST = "EVENTS_SAVE_ONGOING_EVENT_LIST";
export const EVENTS_SAVE_ONGOING_EVENTS_LIST_ERROR =
	"EVENTS_SAVE_ONGOING_EVENTS_LIST_ERROR";

export const EVENTS_GET_CATEGORIES = "EVENTS_GET_CATEGORIES";
export const EVENTS_SAVE_CATEGORIES = "EVENTS_SAVE_CATEGORIES";
export const EVENTS_SAVE_CATEGORIES_ERROR = "EVENTS_SAVE_CATEGORIES_ERROR";

export const EVENTS_GET_SITES = "EVENTS_GET_SITES";
export const EVENTS_SAVE_SITES = "EVENTS_SAVE_SITES";

/**
 * action creators
 */
export const eventsGetAllEventList = payload => ({
	type: EVENTS_GET_EVENT_LIST,
	payload
});

export const eventsSaveAllEventList = payload => ({
	type: EVENTS_SAVE_EVENT_LIST,
	payload
});

export const eventsSaveEventListError = payload => ({
	type: EVENTS_SAVE_EVENT_LIST_ERROR,
	payload
});

export const eventsGetAllOngoingEventsList = payload => ({
	type: EVENTS_GET_ONGOING_EVENT_LIST,
	payload
});

export const eventsSaveAllOngoingEventsList = payload => ({
	type: EVENTS_SAVE_ONGOING_EVENT_LIST,
	payload
});

export const eventsSaveOngoingEventListError = payload => ({
	type: EVENTS_SAVE_ONGOING_EVENTS_LIST_ERROR,
	payload
});

export const eventsGetCategories = payload => ({
	type: EVENTS_GET_CATEGORIES,
	payload
});

export const eventsSaveCategories = payload => ({
	type: EVENTS_SAVE_CATEGORIES,
	payload
});

export const eventsSaveCategoriesError = payload => ({
	type: EVENTS_SAVE_CATEGORIES_ERROR,
	payload
});

export const eventsGetSites = payload => ({
	type: EVENTS_GET_SITES,
	payload
});

export const eventsSaveSites = payload => ({
	type: EVENTS_SAVE_SITES,
	payload
});

export function* eventsListSaga(dispatch) {
	yield takeLatest(EVENTS_GET_EVENT_LIST, handleFetchEvents);
	yield takeLatest(EVENTS_GET_ONGOING_EVENT_LIST, handleFetchEvents);
	yield takeLatest(EVENTS_GET_CATEGORIES, handleFetchCategories);
	yield takeLatest(EVENTS_GET_SITES, handleEventsGetSites);
}

/**
 * Handlers
 */

function* handleFetchEvents(action) {
	try {
		let events = yield call(EventsAPI.fetchAllEvents, action.payload);
		if (action.payload.args.includes("upcoming")) {
			let upcomingEvents = events.map(event => ({
				...event,
				...{ canRegister: true }
			}));
			let upcomingEventsSorted = upcomingEvents.sort((a, b) => {
				return new Date(a.start_time) - new Date(b.start_time);
			});
			yield put(eventsSaveAllEventList(upcomingEventsSorted));
		}
		if (action.payload.args.includes("ongoing")) {
			let ongoingEvents = events.map(event => ({
				...event,
				...{ canRegister: false }
			}));
			let ongoingEventsSorted = ongoingEvents.sort((a, b) => {
				return new Date(a.start_time) - new Date(b.start_time);
			});
			yield put(eventsSaveAllOngoingEventsList(ongoingEventsSorted));
		}
	} catch (error) {
		console.log(error);
		if (action.payload.args.includes("upcoming")) {
			yield put(eventsSaveEventListError(true));
		}
		if (action.payload.args.includes("ongoing")) {
			yield put(eventsSaveOngoingEventListError(true));
		}
	}
}

function* handleFetchCategories(action) {
	try {
		let categories = yield call(EventsAPI.getCategories, action.payload);
		yield put(eventsSaveCategories(categories));
	} catch (error) {
		yield put(eventsSaveCategoriesError(true));
	}
}

function* handleEventsGetSites(action) {
	try {
		let sites = yield call(EventsAPI.getSites, action.payload);
		sites.sort((site1, site2) => site1.name.localeCompare(site2.name));
		yield put(eventsSaveSites(sites));
	} catch (error) {
		yield put(eventsSaveSites([]));
	}
}
