import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#505EFF",
		alignItems: "center",
		justifyContent: "center"
	},
	thankYouText: {
		color: "#fff",
		opacity: 0.4,
		fontSize: width / 6,
		textAlign: "center"
	},
	paymentSuccessfulText: {
		color: "#fff",
		fontSize: width / 14,
		marginTop: width / 30
	},
	eventRegisterSuccessText: {
		color: "#fff",
		marginTop: width / 15,
		paddingHorizontal: width / 8,
		textAlign: "center"
	},
	homeButton: {
		backgroundColor: "#fff",
		paddingHorizontal: width / 15,
		paddingVertical: width / 40
	},
	homeText: {
		color: "#505EFF"
	}
});
