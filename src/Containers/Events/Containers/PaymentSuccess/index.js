import React, { Component } from "react";
import {
	Text,
	View,
	TouchableOpacity,
	Dimensions,
	Image,
	BackHandler
} from "react-native";
import { connect } from "react-redux";

import { WALLET_CASH } from "../../Assets/Img/Image";
import I18n from "../../Assets/Strings/i18n";
import { styles } from "./style";
import { CommonActions } from "@react-navigation/native";

const { width, height } = Dimensions.get("window");

class PaymentSuccess extends Component {
	static defaultProps = {
		isPaid: true
	};

	constructor(props) {
		super(props);
		this.goBack = this.goBack.bind(this);
		this.backHandler = BackHandler.addEventListener(
			"hardwareBackPress",
			this.goBack
		);
	}

	async goBack() {
		if (!this.props.navigation.goBack()) {
			this.props.navigation.reset([
				CommonActions.navigate({
					name: "Home"
				})
			]);
		}
	}

	componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.goBack);
	}

	render() {
		return (
			<View style={styles.container}>
				<View style={{ height: height / 3 }} />
				<View
					style={{
						alignItems: "center",
						justifyContent: "center"
					}}>
					<Text style={styles.thankYouText} numberOfLines={2}>
						{I18n.t("events_thank_you")}
					</Text>
					<View
						style={{
							position: "absolute",
							alignItems: "center",
							justifyContent: "center"
						}}>
						<Image
							source={WALLET_CASH}
							style={{
								height: width / 3,
								width: width / 3
							}}
						/>
					</View>
				</View>
				{this.props.isPaid ? (
					<Text style={styles.paymentSuccessfulText}>Payment Successful</Text>
				) : null}
				<Text style={styles.eventRegisterSuccessText}>
					{I18n.t("events_successful_registration")}
				</Text>
				<View
					style={{
						flex: 1,
						justifyContent: "flex-end",
						paddingBottom: width / 20
					}}>
					<TouchableOpacity
						style={styles.homeButton}
						onPress={() =>
							this.props.navigation.dispatch(
								CommonActions.reset({
									index: 0,
									routes: [
										{
											name: "Home",
											params: {
												isDeepLinkUsed: true
											}
										}
									]
								})
							)
						}>
						<Text style={styles.homeText}>{I18n.t("events_home")}</Text>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		isPaid:
			state.events &&
			state.events.selectedEvent &&
			state.events.selectedEvent.event
				? state.events.selectedEvent.event.is_paid
				: false
	};
};

export default connect(mapStateToProps)(PaymentSuccess);
