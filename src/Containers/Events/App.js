import React, { Fragment } from "react";
import { SafeAreaView, StatusBar } from "react-native";
import RegisterScreen from "./RegisterScreen";
import { DrawerOpeningHack } from "../../Components";
import GoAppAnalytics from "../../utils/GoAppAnalytics";

export default class App extends React.Component {
	render() {
		return (
			<Fragment>
				{/** For the Top Color (Under the Notch) */}
				<SafeAreaView style={{ flex: 0, backgroundColor: "#505eff" }} />
				{/** For the Bottom Color (Below the home touch) */}
				<SafeAreaView style={{ flex: 1 }}>
					<StatusBar backgroundColor={"#505eff"} barStyle={"light-content"} />
					<RegisterScreen
						{...this.props}
						onNavigationStateChange={(prevState, currState) =>
							GoAppAnalytics.logChangeOfScreenInStack(
								"events",
								prevState,
								currState
							)
						}
					/>
					<DrawerOpeningHack />
				</SafeAreaView>
			</Fragment>
		);
	}
}
