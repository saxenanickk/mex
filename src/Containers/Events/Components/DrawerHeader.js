import React from "react";
import { StyleSheet, Dimensions, View, TouchableOpacity } from "react-native";
import { Icon } from "../../../Components";
import { EventsTextMedium, EventsTextRegular } from "./EventsText";

const { width, height } = Dimensions.get("window");

export const DrawerHeader = props => (
	<View style={styles.header}>
		<View style={styles.innerHeader}>
			<View style={styles.headerTitleSection}>
				<TouchableOpacity
					style={styles.headerButton}
					onPress={() => (props.isGoBack ? props.navigation() : null)}>
					<Icon
						iconType={"ionicon"}
						iconSize={height / 30}
						iconName={props.isGoBack ? "md-arrow-round-back" : "md-menu"}
						iconColor={"#fff"}
					/>
				</TouchableOpacity>
				{props.isHeaderName && (
					<EventsTextMedium style={styles.headerText}>
						{props.headerName}
					</EventsTextMedium>
				)}
			</View>
			{props.selectedSiteName ? (
				<View style={[styles.innerHeader, styles.headerDistance]}>
					<View
						activeOpacity={0.8}
						style={{
							flexDirection: "row",
							alignItems: "center"
						}}>
						<EventsTextRegular style={styles.siteText}>
							{props.selectedSiteName}
						</EventsTextRegular>
						<Icon
							iconType={"ionicon"}
							iconSize={height / 55}
							iconName={"ios-pin"}
							iconColor={"#ffffff"}
						/>
					</View>
				</View>
			) : null}

			{props.isNotificationEnabled ? (
				<TouchableOpacity style={[styles.headerButton, styles.rowAlignEnd]}>
					<Icon
						iconType={"ionicon"}
						iconSize={height / 30}
						iconName={"ios-notifications-outline"}
						iconColor={"#fff"}
					/>
				</TouchableOpacity>
			) : null}
		</View>
	</View>
);

DrawerHeader.defaultProps = {
	isNotificationEnabled: false,
	isHeaderName: false,
	isGoBack: false
};

const styles = StyleSheet.create({
	header: {
		width: width,
		backgroundColor: "#505eff",
		paddingHorizontal: width / 27.3,
		paddingVertical: height / 50
	},
	innerHeader: {
		flexDirection: "row",
		justifyContent: "space-between",
		borderWidth: 0
	},
	headerButton: {
		borderColor: "#fff",
		width: width / 10,
		borderWidth: 0
	},
	headerText: {
		color: "#fff",
		fontSize: height / 40
	},
	rowAlignEnd: {
		alignItems: "flex-end"
	},
	searchBar: {
		backgroundColor: "#838aff",
		height: height / 15,
		borderRadius: 3,
		marginTop: height / 40
	},
	headerDistance: {
		marginTop: height / 120
	},
	headerTitleSection: {
		borderWidth: 0,
		flexDirection: "row"
	},
	siteText: {
		color: "#fff",
		marginRight: width / 50
	}
});
