import React, { Component } from "react";
import { Text, View, TextInput, Dimensions } from "react-native";
import { GoPicker } from "../../../Components";
import Dropdown from "../../../Components/Dropdown";
// let x = { field_name, type, picker_options }

const { width, height } = Dimensions.get("window");
export default class SuperInput extends Component {
	state = {
		selectedValue: this.props.value
	};
	onSelectedValueHandler = selectedValue => {
		this.props.onChangeValue(selectedValue);
		this.setState({
			selectedValue: selectedValue
		});
	};
	render() {
		if (this.props.fieldDetails.type.toLowerCase() === "picker") {
			let newPickerOptions = [];
			this.props.fieldDetails.picker_options.map(item =>
				newPickerOptions.push({ name: item })
			);
			return (
				<Dropdown
					data={newPickerOptions}
					onPress={this.onSelectedValueHandler}
				/>
			);
		}
		return (
			<TextInput
				onChangeText={text => this.props.onChangeValue(text)}
				defaultValue={this.props.value}
				style={{ flex: 1, borderWidth: 1, padding: width / 200, color: "#000" }}
				underlineColorAndroid={"transparent"}
				keyboardType={this.props.fieldDetails.type}
				returnKeyType={"done"}
			/>
		);
	}
}
