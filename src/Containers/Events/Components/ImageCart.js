import React, { Component } from "react";
import {
	Text,
	View,
	Dimensions,
	TouchableOpacity,
	ImageBackground
} from "react-native";
import { EventsTextRegular } from "./EventsText";

const { width, height } = Dimensions.get("window");

export default class ImageCart extends Component {
	render() {
		return (
			<View
				style={{
					flexDirection: "row",
					flexWrap: "wrap",
					justifyContent: "space-between",
					marginTop: width / 30
				}}>
				{this.props.image.slice(0, 5).map((item, index) => {
					return (
						<View key={index}>
							<TouchableOpacity>
								<ImageBackground
									source={{ uri: item }}
									style={{
										marginBottom: index === 0 ? width / 80 : 0,
										width: index === 0 ? width : width / 4.05,
										height: index === 0 ? height / 3.7 : height / 8,
										justifyContent: "center"
									}}
									resizeMode={"cover"}>
									{index === 4 && (
										<View
											style={{
												width: width / 4.05,
												height: height / 8,
												backgroundColor: "rgba(0,0,0,0.7)",
												justifyContent: "center",
												alignItems: "center"
											}}>
											<EventsTextRegular
												style={{
													fontSize: height / 45,
													color: "#fff"
												}}>
												{"+" + (this.props.image.length - 5)}
											</EventsTextRegular>
										</View>
									)}
								</ImageBackground>
							</TouchableOpacity>
						</View>
					);
				})}
				<Text> textInComponent </Text>
			</View>
		);
	}
}
