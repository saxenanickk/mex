import React, { Component, Fragment } from "react";
import {
	SafeAreaView,
	StatusBar,
	Dimensions,
	TouchableOpacity,
	View,
	StyleSheet,
	Platform
} from "react-native";
import GoAppAnalytics from "../../utils/GoAppAnalytics";
import {
	GoappTextRegular,
	GoappTextInputRegular
} from "../../Components/GoappText";
import { Icon, OtpValidation, GoToast, ProgressScreen } from "../../Components";
import { validatePhoneNumberDigit } from "../../utils/validation";
import { connect } from "react-redux";
import PhoneVerifyApi from "./Api";
import DialogContext from "../../DialogContext";
import { ERRORCLOUD } from "../../Assets/Img/Image";
import MiniApp from "../../CustomModules/MiniApp";
import { mexGetProfile } from "../../CustomModules/ApplicationToken/Saga";

const { width, height } = Dimensions.get("window");

// Todo: handle the case when there is no user profile
class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isOtpVerify: false,
			phoneNumber: "",
			countryCode: "+91",
			isVerified: false,
			isLoading: false,
			message: ""
		};
		this.closeOtpScreen = this.closeOtpScreen.bind(this);
		this.verifyPhone = this.verifyPhone.bind(this);
	}

	componentDidMount() {
		this.props.userProfile &&
			this.setNumberAndCode(this.props.userProfile.phonenum);
	}

	setNumberAndCode(phoneNum) {
		try {
			if (phoneNum) {
				let number = phoneNum.split("-");
				if (number.length === 2) {
					let countryCode = number[0].split("+")[1];
					this.setState({ countryCode: countryCode, phoneNumber: number[1] });
				} else {
					this.setState({ phoneNumber: number[0] });
				}
			}
		} catch (error) {
			this.setState({ phoneNumber: phoneNum });
		}
	}

	verifyPhone = () => {
		let number = "";
		if (this.state.countryCode[0] === "+") {
			number = `${this.state.countryCode}-${this.state.phoneNumber}`;
		} else {
			number = `+${this.state.countryCode}-${this.state.phoneNumber}`;
		}
		if (
			validatePhoneNumberDigit(number) &&
			this.state.countryCode.length > 0 &&
			this.state.phoneNumber.length > 0
		) {
			if (this.optSendCount == 0) {
				this.setAnalyticsVerify(
					"verify_phone",
					"otp_sent",
					this.state.phoneNumber
				);
			} else {
				this.setAnalyticsVerify(
					"verify_phone",
					"otp_resent",
					this.state.phoneNumber
				);
			}
			this.sendOtp(number);
			this.optSendCount = this.optSendCount + 1;
		} else {
			if (
				this.state.countryCode.length === 0 &&
				this.state.phoneNumber.length === 0
			) {
				GoToast.show("Phone number is mandatory.", "Error");
			} else {
				GoToast.show("Invalid phone number.", "Error");
			}
		}
	};

	validatePhoneNumber = text => {};

	closeOtpScreen = () => {
		this.setState({ isOtpVerify: false });
	};

	sendOtp = async number => {
		let message = "Sorry, we are unable to send OTP.";
		try {
			this.setState({ isLoading: true, message: "Sending OTP" });
			const resp = await PhoneVerifyApi.sendOtpToTheUser({
				appToken: this.props.appToken,
				phoneNum: number
			});
			if (resp.success === 1) {
				this.setState({ isOtpVerify: true });
			} else {
				message = resp.message;
				throw new Error("Unable to send Otp");
			}
		} catch (error) {
			this.context.current.openDialog({
				type: "Information",
				title: "Error",
				message: message
			});
			console.log("error is ", error);
		} finally {
			this.setState({ isLoading: false });
		}
	};

	verifyOtp = async otp => {
		let message = "Sorry, we are unable to verify OTP.";
		try {
			this.setState({
				isLoading: true,
				isOtpVerify: false,
				message: "Verifying OTP"
			});
			let number = "";
			if (this.state.countryCode[0] === "+") {
				number = `${this.state.countryCode}-${this.state.phoneNumber}`;
			} else {
				number = `+${this.state.countryCode}-${this.state.phoneNumber}`;
			}
			const resp = await PhoneVerifyApi.verifyOtp({
				appToken: this.props.appToken,
				phoneNum: number,
				verificationCode: otp
			});
			if (resp.success === 1) {
				this.setAnalyticsVerify(
					"verify_phone",
					"verify_success",
					this.state.phoneNumber
				);
				this.setState({ isVerified: true, isOtpVerify: false });
				MiniApp.setUserInfo(
					this.props.userProfile.name,
					this.props.userProfile.email,
					number
				);
				this.props.dispatch(mexGetProfile({ appToken: this.props.appToken }));
				const navigateTo = this.props.route.params.navigateTo || null;
				if (navigateTo !== null) {
					if (navigateTo.toLowerCase() === "bbinstant") {
						MiniApp.launchNativeApp("bbinstant");
					} else {
						MiniApp.launchWebApp(navigateTo, "");
					}
				} else {
					this.context.current.openDialog({
						type: "Action",
						title: "Information",
						message: "Phone number verified.",
						rightPress: () => this.props.navigation.navigate("Goapp")
					});
				}
			} else {
				if (resp.message === "Invalid verification code") {
					this.context.current.openDialog({
						type: "Confirmation",
						title: "Error!",
						message: "Incorrect OTP. Please try again.",
						leftPress: () => this.setState({ isOtpVerify: false }),
						rightPress: () => this.setState({ isOtpVerify: true }),
						icon: ERRORCLOUD
					});
				} else {
					message = resp.message;
					throw new Error("Unable to verify Otp");
				}
			}
		} catch (error) {
			this.setAnalyticsVerify(
				"verify_phone",
				"verify_fail",
				this.state.phoneNumber
			);
			console.log("error is", error);
			this.context.current.openDialog({
				type: "Information",
				title: "Error",
				message: message,
				icon: ERRORCLOUD
			});
		} finally {
			this.setState({ isLoading: false });
		}
	};

	setAnalyticsVerify = (type, status, number) => {
		GoAppAnalytics.trackWithProperties(type, { status, number });
	};

	handlePhoneNumberChange = (type, text) => {
		this.setState({ [type]: text });
	};
	render() {
		return (
			<Fragment>
				<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
				<View style={styles.bodyView}>
					<View style={styles.details}>
						<Icon
							iconType={"feather"}
							iconName={"check-circle"}
							iconSize={height / 25}
							iconColor={"#359DF1"}
							style={styles.checkCircle}
						/>
						<GoappTextRegular style={styles.detailsText}>
							{
								"Get your phone number verified, to get the best experience of MEX."
							}
						</GoappTextRegular>
					</View>
					<View style={styles.phoneNumberView}>
						<GoappTextRegular style={styles.phoneNumberText}>
							{"Phone Number"}
						</GoappTextRegular>
						<View
							style={{
								flexDirection: "row",
								alignItems: "center",
								justifyContent: "space-between",
								backgroundColor: "#EFF1F4",
								paddingVertical: 10,
								paddingLeft: 15,
								paddingRight: 10,
								borderRadius: 25
							}}>
							<View
								style={{
									flexDirection: "row",
									alignItems: "center"
								}}>
								<GoappTextInputRegular
									color={"rgba(0,0,0,1)"}
									placeholder="XXX"
									returnKeyType={"done"}
									maxLength={3}
									keyboardType="numeric"
									style={{
										height: height / 20,
										width: width / 12,
										fontSize: height / 60
									}}
									value={this.state.countryCode}
									onChangeText={text =>
										this.handlePhoneNumberChange("countryCode", text)
									}
								/>
							</View>
							<GoappTextRegular color={"rgba(0,0,0,0.5)"}>
								{"-"}
							</GoappTextRegular>
							<GoappTextInputRegular
								color={"rgba(0,0,0,1)"}
								maxLength={10}
								placeholder="XXXXXXXXXX"
								returnKeyType={"done"}
								keyboardType="numeric"
								style={{
									height: height / 20,
									width: width / 2.3,
									fontSize: height / 60
								}}
								value={this.state.phoneNumber}
								onChangeText={text =>
									this.handlePhoneNumberChange("phoneNumber", text)
								}
							/>
							{this.props.userProfile &&
							this.props.userProfile.phoneverified &&
							this.props.userProfile.phonenum ? (
								<Icon
									iconType={"feather"}
									iconName={"check-circle"}
									iconSize={height / 35}
									iconColor={"#58af61"}
								/>
							) : (
								<TouchableOpacity
									style={styles.PIverifyButton}
									onPress={this.verifyPhone}>
									<GoappTextRegular style={styles.PIverifyText} color={"#fff"}>
										{"Verify"}
									</GoappTextRegular>
								</TouchableOpacity>
							)}
						</View>
					</View>
					{this.state.isOtpVerify ? (
						<OtpValidation
							open={this.state.isOtpVerify}
							handleClose={this.closeOtpScreen}
							heading={"Enter OTP"}
							message={`We've sent a text message to ${
								this.state.countryCode[0] === "+"
									? `${this.state.countryCode}-${this.state.phoneNumber}`
									: `+${this.state.countryCode}-${this.state.phoneNumber}`
							}`}
							verificationMessage={"Phone number verification successful!"}
							isVerified={this.state.isVerified}
							onPressConfirm={this.verifyOtp}
							onPressResend={this.verifyPhone}
						/>
					) : null}
					{this.state.isLoading ? (
						<ProgressScreen
							indicatorColor={"#2c98f0"}
							indicatorSize={height / 45}
							isMessage={true}
							primaryMessage={"Hang On.."}
							message={this.state.message}
						/>
					) : null}
				</View>
			</Fragment>
		);
	}
}

const styles = StyleSheet.create({
	checkCircle: { alignSelf: "center" },
	detailsText: {
		textAlign: "center",
		marginTop: height / 40,
		width: width - width / 13.5,
		fontSize: height / 45,
		color: "#2a2a2a"
	},
	header: {
		flexDirection: "row",
		height: height / 12,
		width: width,
		backgroundColor: "#fff",
		alignItems: "center",
		paddingHorizontal: width / 27,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.2
			}
		})
	},
	headerTopic: {
		marginLeft: width / 25,
		fontSize: height / 45
	},
	bodyView: { flex: 1 },
	details: {
		alignSelf: "center",
		marginTop: width / 8
	},
	phoneNumberText: {
		marginLeft: width / 25,
		color: "#5c6170",
		fontSize: height / 60
	},
	inputView: { marginTop: width / 37 },
	textInput: {
		height: width / 9,
		backgroundColor: "#EFF1F4",
		borderRadius: width / 15,
		paddingLeft: width / 25
	},
	verifyButton: {
		position: "absolute",
		right: 0,
		backgroundColor: "#2c98f0",
		height: width / 10.8,
		paddingHorizontal: width / 30,
		alignItems: "center",
		justifyContent: "center",
		borderRadius: width / 15,
		margin: width / 125
	},
	verifyText: { fontSize: height / 50 },
	phoneNoMsg: {
		color: "#EB617B",
		textAlign: "right",
		fontSize: width / 35,
		marginTop: width / 100
	},
	phoneNumberView: {
		width: width - width / 13.5,
		alignSelf: "center",
		marginTop: height / 10
	},
	PIverifyButton: {
		backgroundColor: "#2c98f0",
		borderRadius: width / 15,
		height: height / 20,
		paddingHorizontal: width / 30,
		justifyContent: "center",
		alignSelf: "flex-end"
	},
	PIverifyText: { paddingHorizontal: 10 }
});

App.contextType = DialogContext;

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		user: state.appToken.user,
		userProfile: state.appToken.userProfile
	};
}
export default connect(mapStateToProps)(App);
