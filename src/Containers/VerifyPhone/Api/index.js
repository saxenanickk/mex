import Config from "react-native-config";

const { MEX_LOGIN_BASE_URL } = Config;
export const MOBILE_NUMBER_VERIFICATION = "MOBILE NUMBER VERIFICATION";
/**
 * @typedef {Object} sendOtpParam
 * @property {string} appToken
 * @property {string} userId
 * @property {string} phoneNum
 */

/**
 * @typedef {Object} verifyOtpParam
 * @property {string} verificationCode
 */

class PhoneVerifyApi {
	/**
	 * function to send otp to user
	 * @param {sendOtpParam} params
	 */
	sendOtpToTheUser(params) {
		console.log("params to send the otp is", params);
		return new Promise((resolve, reject) => {
			try {
				fetch(`${MEX_LOGIN_BASE_URL}sendVerificationCode`, {
					method: "POST",
					headers: {
						"x-access-token": params.appToken,
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						otpType: MOBILE_NUMBER_VERIFICATION,
						phoneNum: params.phoneNum
					})
				})
					.then(response => {
						response
							.json()
							.then(res => resolve(res))
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * function to verify the otp
	 * @param {sendOtpParam & verifyOtpParam} params
	 */
	verifyOtp(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${MEX_LOGIN_BASE_URL}verifyVerificationCode`, {
					method: "POST",
					headers: {
						"x-access-token": params.appToken,
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						phoneNum: params.phoneNum,
						verificationCode: params.verificationCode,
						otpType: MOBILE_NUMBER_VERIFICATION
					})
				})
					.then(response => {
						response
							.json()
							.then(res => resolve(res))
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				reject(error);
			}
		});
	}
}

export default new PhoneVerifyApi();
