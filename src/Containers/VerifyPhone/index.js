import React from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";

export default class VerifyPhone extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "verify_phone" });
	}

	render() {
		return <App {...this.props} />;
	}
}

AppRegistry.registerComponent("VerifyPhone", () => VerifyPhone);
