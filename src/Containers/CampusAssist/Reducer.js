import { combineReducers } from "redux";
// Import Reducers here
import ticketVenue from "./Containers/Ticket/TicketIncidentVenue/Reducer";
import ticketDetails from "./Containers/TicketDetails/Reducer";
import getTickets from "./Containers/Home/Reducer";
import getCategory from "./Containers/Ticket/Category/Reducer";
import getSubCategory from "./Containers/Ticket/SubCategory/Reducer";
import createTicket from "./Containers/Ticket/AdditionalDetails/Reducer";
import addComment from "./Containers/CommentsTicket/Reducer";

const reducer = combineReducers({
	ticketVenue,
	getTickets,
	getCategory,
	getSubCategory,
	ticketDetails,
	createTicket,
	addComment
});

export default reducer;
