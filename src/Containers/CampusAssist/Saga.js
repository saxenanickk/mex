import { all } from "redux-saga/effects";
import { campusAssistTicketVenueSaga } from "./Containers/Ticket/TicketIncidentVenue/Saga";
import { campusAssistTicketDetailsSaga } from "./Containers/TicketDetails/Saga";
import { campusAssistGetTicketsSaga } from "./Containers/Home/Saga";
import { campusAssistGetCategoriesSaga } from "./Containers/Ticket/Category/Saga";
import { campusAssistGetSubCategoriesSaga } from "./Containers/Ticket/SubCategory/Saga";
import { campusAssistCreateTicketsSaga } from "./Containers/Ticket/AdditionalDetails/Saga";
import { campusAssistAddCommentSaga } from "./Containers/CommentsTicket/Saga";

export default function* campusAssistSaga() {
	yield all([
		campusAssistTicketVenueSaga(),
		campusAssistGetTicketsSaga(),
		campusAssistGetCategoriesSaga(),
		campusAssistGetSubCategoriesSaga(),
		campusAssistTicketDetailsSaga(),
		campusAssistCreateTicketsSaga(),
		campusAssistAddCommentSaga()
	]);
}
