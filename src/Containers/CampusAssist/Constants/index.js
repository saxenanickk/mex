export const ARE_YOU_SURE_YOU_WANT_TO_CANCEL =
	"Are you sure you want to cancel?";
export const PLEASE_SELECT_AN_ISSUE = "Please select an issue";
export const YOUR_ISSUE_HAS_BEEN_REPORTED_WE_WILL_CONTACT_YOU_SHORTLY =
	"Your issue has been reported, we will contact you shortly";
export const THANK_YOU = "Thank you!";
