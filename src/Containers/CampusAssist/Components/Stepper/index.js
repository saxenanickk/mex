import React, { Component } from "react";
import { Text, View, StyleSheet } from "react-native";

class Stepper extends Component {
	static defaultProps = {
		activeColor: "#2c98f0",
		inActiveColor: "#D8D8D8",
		activeTextColor: "#2c98f0",
		inActiveTextColor: "#828282",
		labels: ["New", "In Progress", "Resolved", "Closed"],
		currentPosition: 0,
		circleSize: 18,
		seperatorHeight: 6
	};

	render() {
		const {
			currentPosition,
			circleSize,
			seperatorHeight,
			inActiveColor,
			activeColor,
			activeTextColor,
			inActiveTextColor
		} = this.props;
		return (
			<View style={styles.root}>
				<View style={[styles.container, styles.stepContainer]}>
					{this.props.labels.map((label, index) => {
						const color =
							currentPosition >= index ? activeColor : inActiveColor;
						if (index === 0) {
							return (
								<View key={index} style={[styles.circle(circleSize, color)]} />
							);
						} else {
							return (
								<React.Fragment key={index}>
									<View style={[styles.line(seperatorHeight, color)]} />
									<View style={[styles.circle(circleSize, color)]} />
								</React.Fragment>
							);
						}
					})}
				</View>
				<View style={styles.container}>
					{this.props.labels.map((label, index) => {
						const textColor =
							currentPosition >= index ? activeTextColor : inActiveTextColor;
						if (index === 0) {
							return (
								<Text key={index} style={[styles.textColor(textColor)]}>
									{label}
								</Text>
							);
						} else {
							return (
								<React.Fragment key={index}>
									<View style={[styles.transparentLine]} />
									<Text style={[styles.textColor(textColor)]}>{label}</Text>
								</React.Fragment>
							);
						}
					})}
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	root: {
		alignItems: "center"
	},
	container: {
		width: "100%",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center"
	},
	stepContainer: {
		width: "96%"
	},
	circle: (size, color) => ({
		width: size,
		height: size,
		borderRadius: size / 2,
		backgroundColor: color
	}),
	iconStyle: {
		borderWidth: 1,
		margin: 0,
		padding: 0
	},
	line: (height, color) => ({
		flex: 1,
		borderBottomWidth: height,
		borderColor: color
	}),
	transparentLine: {
		flex: 1
	},
	activeLineColor: {
		borderColor: "#2c98f0"
	},
	textColor: color => ({
		color: color
	}),
	activeTextColor: {
		color: "#2c98f0"
	}
});

export default Stepper;
