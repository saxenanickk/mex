import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import FastImage from "react-native-fast-image";
import I18n from "../../Assests/Strings/i18n";
import NewMexIcon from "../../../../Components/Icons/NewMexIcon";
import SupportIcon from "../../../../Components/Icons/SupportIcon";
import UserIcon from "../../../../Components/Icons/UserIcon";
import {
	GoappTextLight,
	GoappTextRegular
} from "../../../../Components/GoappText";

const ChatItem = ({ details, onPress }) => {
	if (details.name === "System" && details.comment !== null) {
		return (
			<View style={styles.container}>
				<NewMexIcon />
				<View style={styles.commentTextContainer}>
					<GoappTextRegular>{details.comment}</GoappTextRegular>
					<GoappTextLight color={"#8F96A3"} size={10}>
						{I18n.strftime(new Date(details.createdAt), "%I:%M %p")}
					</GoappTextLight>
				</View>
			</View>
		);
	}
	if (
		details.name === "Admin" &&
		(details.comment !== null || details.imageUrl !== null)
	) {
		return (
			<View style={styles.container}>
				<SupportIcon />
				<View
					style={[
						styles.commentTextContainer,
						styles.adminTextContainer(details.imageUrl)
					]}>
					{details.imageUrl !== null && details.imageUrl.startsWith("http") ? (
						<TouchableOpacity
							onPress={onPress}
							style={styles.commentImageContainer}>
							<FastImage
								source={{ uri: details.imageUrl }}
								style={styles.commentImage}
							/>
						</TouchableOpacity>
					) : null}
					<GoappTextRegular>{details.comment}</GoappTextRegular>
					<GoappTextLight color={"#8F96A3"} size={10}>
						{I18n.strftime(new Date(details.createdAt), "%I:%M %p")}
					</GoappTextLight>
				</View>
			</View>
		);
	}
	if (details.name === "Me") {
		return (
			<View style={[styles.container, styles.meContainer]}>
				<UserIcon />
				<View style={[styles.commentTextContainer, styles.meTextContainer]}>
					<GoappTextRegular color={"#fff"}>{details.comment}</GoappTextRegular>
					<GoappTextLight color={"#fff"} size={10}>
						{I18n.strftime(new Date(details.createdAt), "%I:%M %p")}
					</GoappTextLight>
				</View>
			</View>
		);
	}
	return null;
};

export default ChatItem;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: "row",
		marginLeft: "2%",
		marginRight: "12%",
		alignItems: "flex-end",
		marginBottom: 12
	},
	meContainer: {
		flexDirection: "row-reverse"
	},
	meTextContainer: {
		backgroundColor: "#2C98F0",
		marginLeft: 0,
		marginRight: 12,
		borderBottomRightRadius: 6,
		borderBottomLeftRadius: 24,
		alignItems: "flex-end"
	},
	commentTextContainer: {
		minWidth: 100,
		maxWidth: "88%",
		marginLeft: 12,
		backgroundColor: "#e4eaf4",
		paddingHorizontal: 12,
		paddingVertical: 5,
		borderRadius: 24,
		borderBottomLeftRadius: 6
	},
	adminTextContainer: imageUrl => ({
		flex: imageUrl !== null ? 1 : 0,
		backgroundColor: "#fff",
		borderWidth: 1,
		borderColor: "#e4eaf4"
	}),
	icon: {
		width: 25,
		height: 25
	},
	commentImageContainer: {
		aspectRatio: 1,
		marginBottom: 6
	},
	commentImage: {
		...StyleSheet.absoluteFill,
		borderRadius: 12
	}
});
