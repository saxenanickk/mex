import * as I18n from "../../../../Assets/strings/i18n";

export default {
	...I18n.default.translations.en
};
