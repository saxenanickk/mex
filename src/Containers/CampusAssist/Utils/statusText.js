export const statusTextChangeHandler = currentState => {
	switch (currentState) {
		case "NEW":
			return "New";
		case "ON_HOLD":
			return "On Hold";
		case "IN_PROGRESS":
			return "In Progress";
		case "RESOLVED":
			return "Resolved";
		default:
			return currentState;
	}
};
