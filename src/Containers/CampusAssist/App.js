import React, { Fragment } from "react";
import { StatusBar } from "react-native";
import RegisterScreen from "./RegisterScreen";
import GoAppAnalytics from "../../utils/GoAppAnalytics";

export default class App extends React.Component {
	render() {
		return (
			<Fragment>
				<StatusBar barStyle={"dark-content"} />
				<RegisterScreen
					{...this.props}
					onNavigationStateChange={(prevState, currState) =>
						GoAppAnalytics.logChangeOfScreenInStack(
							"campusassist",
							prevState,
							currState
						)
					}
				/>
			</Fragment>
		);
	}
}
