import React, { Component } from "react";
import { AppRegistry } from "react-native";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";
import { getNewReducer, removeExistingReducer } from "../../../App";
import App from "./App";
import reducer from "./Reducer";

export default class CampusAssist extends Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "CampusAssist" });
		getNewReducer({ name: "campusAssist", reducer: reducer });
	}

	componentWillUnmount() {
		removeExistingReducer("campusAssist");
	}
	render() {
		return <App {...this.props} />;
	}
}
AppRegistry.registerComponent("CampusAssist", () => CampusAssist);
