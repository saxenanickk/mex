import Config from "react-native-config";
import { Platform } from "react-native";
const {
	SERVER_BASE_URL_IMS,
	SERVER_BASE_URL_SITES,
	SERVER_BASE_URL_BUILDINGS
} = Config;

/**
 * @typedef {Object} CreateCommentByTicketIdParams
 * @property {string} appToken
 * @property {{ticketId: number, comment: string, site_id: string, priority: string, assigneeId: string, teamId: string, currentState: string}} body
 */

class CampusAssistApi {
	constructor() {
		console.log("Campus Assist Api instantiated");
	}

	reopenTickets(params) {
		return new Promise((resolve, reject) => {
			try {
				var payload = new FormData();
				payload.append("site_id", params.siteId);
				payload.append("currentState", "IN_PROGRESS");
				payload.append("assigneeId", params.assigneeId);
				payload.append("teamId", params.teamId);
				payload.append("ticketId", params.ticketId);
				payload.append("comment", params.comment);
				payload.append("priority", params.priority);
				fetch(`${SERVER_BASE_URL_IMS}createCommentByTicketId`, {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken
					},
					body: payload
				})
					.then(res =>
						res.json().then(response => {
							if (res.status === 200 && response.success === 1) {
								resolve(response);
							} else {
								reject(response);
							}
						})
					)
					.catch(err => reject(err))
					.catch(err => reject(err));
			} catch (e) {}
		});
	}

	createTicket(params) {
		return new Promise((resolve, reject) => {
			try {
				var body = new FormData();
				body.append("subCategoryId", params.subCategoryId);
				body.append("site_id", params.siteId);
				body.append("floorId", params.floorId);
				body.append("incidentOccuredAt", params.incidentOccuredAt);
				body.append("description", params.description);

				params.selectedImagesPath.forEach(imagePath => {
					body.append("files", {
						name: imagePath.fileName,
						type: imagePath.type,
						uri:
							Platform.OS === "android"
								? imagePath.uri
								: imagePath.uri.replace("file://", "")
					});
				});
				fetch(`${SERVER_BASE_URL_IMS}createTicket`, {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken
					},
					body: body
				})
					.then(res =>
						res
							.json()
							.then(response => {
								if (res.status === 200 && response.success === 1) {
									resolve(response);
								} else {
									reject(response);
								}
							})
							.catch(err => reject(err))
					)
					.catch(err => reject(err));
			} catch (e) {
				reject(e);
			}
		});
	}

	getFloors(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_BUILDINGS}getFloors`, {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						site_id: params.site_id,
						building_id: params.building_id
					})
				})
					.then(res =>
						res
							.json()
							.then(response => {
								if (res.status === 200 && response.success === 1) {
									resolve(response.data[Object.keys(response.data)[0]]);
								} else {
									reject(response);
								}
							})
							.catch(err => reject(err))
					)
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	getBuildings(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_BUILDINGS}getBuildings`, {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						site_id: params.site_id
					})
				})
					.then(res =>
						res
							.json()
							.then(response => {
								if (res.status === 200 && response.success === 1) {
									resolve(response.data);
								} else {
									reject(response);
								}
							})
							.catch(err => reject(err))
					)
					.catch(err => reject(err));
			} catch (err) {
				reject(err);
			}
		});
	}

	getSites(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_SITES}/getSites`, {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(res =>
						res
							.json()
							.then(response => {
								if (res.status === 200 && response.success === 1) {
									resolve(response.data);
								} else {
									reject(response);
								}
							})
							.catch(err => reject(err))
					)
					.catch(err => reject(err));
			} catch (err) {
				reject(err);
			}
		});
	}

	getSubCategories(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(
					`${SERVER_BASE_URL_IMS}getAllTicketSubCategory?site_id=${
						params.site_id
					}&category=${params.category}`,
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": params.appToken,
							"Content-Type": "application/json"
						}
					}
				)
					.then(res =>
						res
							.json()
							.then(response => {
								if (res.status === 200 && response.success === 1) {
									resolve(response.data);
								} else {
									reject(response);
								}
							})
							.catch(err => reject(err))
					)
					.catch(err => reject(err));
			} catch (err) {
				reject(err);
			}
		});
	}

	getCategories(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(
					`${SERVER_BASE_URL_IMS}getAllTicketCategory?site_id=${
						params.site_id
					}`,
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": params.appToken,
							"Content-Type": "application/json"
						}
					}
				)
					.then(res =>
						res
							.json()
							.then(response => {
								if (res.status === 200 && response.success === 1) {
									resolve(response.data);
								} else {
									reject(response);
								}
							})
							.catch(err => reject(err))
					)

					.catch(err => reject(err));
			} catch (err) {
				reject(err);
			}
		});
	}

	getTickets(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_IMS}getTickets?site_id=${params.siteId}`, {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"Content-Type": "application/json"
					}
				})
					.then(res =>
						res
							.json()
							.then(response => {
								if (res.status === 200 && response.success === 1) {
									if (Array.isArray(response.data)) {
										resolve(response.data);
									} else {
										resolve(response.data.list);
									}
								} else {
									reject(response);
								}
							})
							.catch(err => reject(err))
					)

					.catch(err => reject(err));
			} catch (err) {
				reject(err);
			}
		});
	}

	getTicketById(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(
					`${SERVER_BASE_URL_IMS}getTicketById?ticketId=${
						params.ticket_id
					}&site_id=${params.site_id}`,
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": params.appToken
						}
					}
				)
					.then(response => {
						response
							.json()
							.then(res => {
								if (response.status === 200 && res.success === 1) {
									resolve(res.data);
								} else {
									reject(res);
								}
							})
							.catch(error => reject(error));
					})
					.catch(error => {
						reject(error);
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * Add or create comment for a ticket
	 * @param {CreateCommentByTicketIdParams} params
	 */
	createCommentByTicketId(params) {
		return new Promise((resolve, reject) => {
			try {
				let data = new FormData();
				Object.keys(params.body).forEach((item, i) => {
					data.append(item, params.body[item]);
				});
				fetch(`${SERVER_BASE_URL_IMS}createCommentByTicketId`, {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken
					},
					body: data
				})
					.then(response => {
						response
							.json()
							.then(res => {
								if (res.success) {
									resolve(res);
								} else {
									reject(res.message);
								}
							})
							.catch(err => {
								reject(err);
							});
					})
					.catch(err => {
						reject(err);
					});
			} catch (e) {
				reject(e);
			}
		});
	}
}

export default new CampusAssistApi();
