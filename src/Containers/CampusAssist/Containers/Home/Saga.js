import { put, call, takeLatest } from "redux-saga/effects";
import CampusAssistApi from "../../Api";
import {
	createRequestTypes,
	action,
	REQUEST,
	SUCCESS,
	FAILURE
} from "../../../../utils/reduxUtils";

export const CAMPUS_ASSIST_GET_TICKETS = createRequestTypes(
	"CAMPUS_ASSIST_GET_TICKETS"
);
export const CAMPUS_ASSIST_REOPEN_TICKETS = createRequestTypes(
	"CAMPUS_ASSIST_REOPEN_ISSUES"
);

export const CAMPUS_ASSIST_UPDATE_TICKET_BY_ID =
	"CAMPUS_ASSIST_UPDATE_TICKET_BY_ID";

export const campusAssistReopenTickets = {
	request: (
		appToken,
		ticketId,
		siteId,
		comment,
		priority,
		assigneeId,
		teamId
	) =>
		action(CAMPUS_ASSIST_REOPEN_TICKETS[REQUEST], {
			appToken,
			ticketId,
			siteId,
			comment,
			priority,
			assigneeId,
			teamId
		}),
	success: response =>
		action(CAMPUS_ASSIST_REOPEN_TICKETS[SUCCESS], { response }),
	failure: error => action(CAMPUS_ASSIST_REOPEN_TICKETS[FAILURE], error)
};

export const campusAssistGetTickets = {
	request: (appToken, siteId) =>
		action(CAMPUS_ASSIST_GET_TICKETS[REQUEST], { appToken, siteId }),
	success: response => action(CAMPUS_ASSIST_GET_TICKETS[SUCCESS], { response }),
	failure: error => action(CAMPUS_ASSIST_GET_TICKETS[FAILURE], error)
};

/**
 * Action to update ticket by id
 * @param {number} payload TicketId
 */
export const campusAssistUpdateTicketById = payload =>
	action(CAMPUS_ASSIST_UPDATE_TICKET_BY_ID, payload);

export function* campusAssistGetTicketsSaga(dispatch) {
	yield takeLatest(CAMPUS_ASSIST_GET_TICKETS[REQUEST], handleGetTickets);
	yield takeLatest(
		CAMPUS_ASSIST_REOPEN_TICKETS[REQUEST],
		onReopenSubmitHandler
	);
}

function* onReopenSubmitHandler(params) {
	try {
		let response = yield call(CampusAssistApi.reopenTickets, params.payload);
		yield put(campusAssistReopenTickets.success(response));
		yield put(campusAssistUpdateTicketById(params.payload.ticketId));
	} catch (err) {
		yield put(campusAssistReopenTickets.failure(err));
	}
}

function* handleGetTickets(params) {
	try {
		let ticketsResponse = yield call(
			CampusAssistApi.getTickets,
			params.payload
		);
		yield put(campusAssistGetTickets.success(ticketsResponse));
	} catch (error) {
		yield put(campusAssistGetTickets.failure(error));
	}
}
