import {
	CAMPUS_ASSIST_GET_TICKETS,
	CAMPUS_ASSIST_REOPEN_TICKETS,
	CAMPUS_ASSIST_UPDATE_TICKET_BY_ID
} from "./Saga";
import { REQUEST, SUCCESS, FAILURE } from "../../../../utils/reduxUtils";

export const CAMPUS_ASSIST_REFRESH_TICKETS = "CAMPUS_ASSIST_REFRESH_TICKETS";

const initialState = {
	isTicketsLoading: false,
	ticketsResponse: [],
	isTicketsError: false,
	isReopenTicketsLoading: false,
	reopenTicketResponse: {},
	isReopenTicketsError: false
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case CAMPUS_ASSIST_GET_TICKETS[REQUEST]:
			return { ...state, isTicketsLoading: true, isTicketsError: false };
		case CAMPUS_ASSIST_GET_TICKETS[SUCCESS]:
			return {
				...state,
				isTicketsLoading: false,
				ticketsResponse: payload.response
			};
		case CAMPUS_ASSIST_GET_TICKETS[FAILURE]:
			return { ...state, isTicketsLoading: false, isTicketsError: true };
		case CAMPUS_ASSIST_REOPEN_TICKETS[REQUEST]:
			return {
				...state,
				isReopenTicketsLoading: true,
				isReopenTicketsError: false
			};
		case CAMPUS_ASSIST_REOPEN_TICKETS[SUCCESS]:
			return {
				...state,
				isReopenTicketsLoading: false,
				reopenTicketResponse: payload.response
			};
		case CAMPUS_ASSIST_REOPEN_TICKETS[FAILURE]:
			return {
				...state,
				isReopenTicketsLoading: false,
				isReopenTicketsError: true
			};
		case CAMPUS_ASSIST_REFRESH_TICKETS:
			return {
				...state,
				ticketsResponse: payload
			};
		case CAMPUS_ASSIST_UPDATE_TICKET_BY_ID:
			const updatedTickets = state.ticketsResponse.map(ticket => {
				if (ticket.ticketId === payload) {
					return { ...ticket, currentState: "IN_PROGRESS" };
				}
				return ticket;
			});
			return {
				...state,
				ticketsResponse: [...updatedTickets]
			};
		default:
			return state;
	}
};
