import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#ffffff"
	},
	seprator: {
		height: height / 100,
		backgroundColor: "#EFF1F4"
	},
	ticketContainer: {
		backgroundColor: "#FFFFFF",
		padding: height / 50
	},
	ticketBox: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	commentBox: {
		paddingVertical: height * 0.01,
		marginLeft: height / 50
	},
	ticketStatus: { alignItems: "flex-end" },
	reopenTxt: { paddingBottom: 7 },
	noRecordImg: {
		height: height / 7,
		width: width / 7,
		paddingLeft: width / 10
	},
	noRecordContainer: {
		alignSelf: "center",
		height: height / 1.3,
		width: width / 2,
		justifyContent: "center",
		alignItems: "center"
	},
	reportIssueContainer: {
		padding: height / 60,
		backgroundColor: "#2C98F0",
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 10,
		marginVertical: height / 50,
		marginHorizontal: width / 7
	},
	tickets: {
		paddingVertical: height / 80
	},
	loaderContainer: {
		flex: 1
	},
	loaderBox: {
		...StyleSheet.absoluteFill,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#00000099"
	}
});
