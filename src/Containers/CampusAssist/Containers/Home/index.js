import React, { Component } from "react";
import {
	ScrollView,
	View,
	TouchableOpacity,
	Dimensions,
	RefreshControl
} from "react-native";
import i18n from "../../../../Assets/strings/i18n";
import FastImage from "react-native-fast-image";
import { connect } from "react-redux";
import {
	GoappTextRegular,
	GoappTextBold,
	GoappTextMedium
} from "../../../../Components/GoappText";
import CommunityLoader from "../../../Community/Components/CommunityLoader";
import { VERIFIED_CORPORATE, WARNING } from "../../../../Assets/Img/Image";
import { ProgressScreen } from "../../../../Components";
import { styles } from "./style";
import { NO_ISSUE_REPORTED } from "../../Assests/Img/Image";
import { SKY_BLUE, WHITE } from "../../../../Constants/color";
import DialogContext from "../../../../DialogContext";
import { campusAssistGetTickets, campusAssistReopenTickets } from "./Saga";
import { statusTextChangeHandler } from "../../Utils/statusText";

const { height } = Dimensions.get("window");
class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			refreshing: false,
			reopenInputText: "",
			reopenAlert: false
		};
	}

	componentDidMount() {
		this.getTicketsHandler();
	}

	getTicketsHandler = () => {
		this.props.dispatch(
			campusAssistGetTickets.request(this.props.appToken, this.props.siteId)
		);
	};

	_openAlert = ticket => {
		this.context.current.openDialog({
			type: "RequiredInput",
			title: "Reopen Ticket",
			message: "Please specify the reason to reopen the ticket",
			rightTitle: "Submit",
			rightPress: text => this.onReopenTicketSubmit(text, ticket),
			error: "Please enter your reason to reopen the ticket"
		});
	};

	componentDidUpdate(prevProps) {
		if (
			this.props.reopenTicketResponse !== null &&
			this.props.reopenTicketResponse !== prevProps.reopenTicketResponse
		) {
			this.props.reopenTicketResponse.success === 1
				? this.onSuccessAlertPopUp()
				: this.onFailureAlertPopUp();
		}
		if (
			!this.props.isTicketsLoading &&
			this.props.isTicketsLoading !== prevProps.isTicketsLoading
		) {
			// eslint-disable-next-line react/no-did-update-set-state
			this.setState({ refreshing: false });
		}
	}

	onReopenTicketSubmit = (text, ticket) => {
		this.setState({ reopenInputText: text });
		ticket && ticket.assigneeDetail
			? this.props.dispatch(
					campusAssistReopenTickets.request(
						this.props.appToken,
						ticket.ticketId,
						this.props.siteId,
						text || "",
						ticket.priority,
						ticket.assigneeDetail.id,
						ticket.teamId
					)
			  )
			: this.setState({
					reopenAlert: true
			  });
	};

	onFailureAlertPopUp = () => {
		this.context.current.openDialog({
			type: "Action",
			title: "Error",
			message: "Unable to update ticket!",
			rightPress: () => this.props.navigation.navigate("Home"),
			icon: WARNING
		});
	};

	onSuccessAlertPopUp = () => {
		this.context.current.openDialog({
			type: "Action",
			title: "Thank You",
			message:
				"Your issue has been reopen and reassigned to our respective team",
			rightPress: () => this.props.navigation.navigate("Home"),
			icon: VERIFIED_CORPORATE
		});
	};

	reopenTicketDateDuration = time => {
		const newDate = new Date(time).setDate(new Date().getDate() + 2);
		return new Date(newDate).toDateString();
	};

	_renderLoaderOrNoTicketWithReportIssue = () => {
		const { isTicketsLoading } = this.props;
		if (isTicketsLoading) {
			return this._renderLoader();
		} else {
			return (
				<>
					{this._renderNoTickets()}
					{this._renderReportIssue()}
				</>
			);
		}
	};

	_renderTicketWithReportIssue() {
		return (
			<View style={{ flex: 1, justifyContent: "space-between" }}>
				<ScrollView
					style={styles.tickets}
					refreshControl={
						<RefreshControl
							refreshing={this.state.refreshing}
							onRefresh={this._onRefresh}
						/>
					}>
					{this._renderTickets()}
				</ScrollView>
				{this._renderReportIssue()}
			</View>
		);
	}
	_onRefresh = () => {
		this.setState({ refreshing: true }, () => this.getTicketsHandler());
	};

	render() {
		const { ticketsResponse, isReopenTicketsLoading } = this.props;

		return (
			<View style={styles.container}>
				{this.state.reopenAlert && this.onFailureAlertPopUp()}
				{ticketsResponse && ticketsResponse.length !== 0
					? this._renderTicketWithReportIssue()
					: this._renderLoaderOrNoTicketWithReportIssue()}
				{isReopenTicketsLoading ? (
					<View style={styles.loaderBox}>
						<CommunityLoader />
						<GoappTextBold color="white">{`Reopening ticket`}</GoappTextBold>
					</View>
				) : null}
			</View>
		);
	}

	_renderRetry = () => {
		this.context.current.openDialog({
			type: "Action",
			message: "Failed to get tickets!",
			rightTitle: "Retry",
			rightPress: () => this.getTicketsHandler(),
			icon: WARNING
		});
	};

	_renderReportIssue = () => {
		return (
			<TouchableOpacity
				style={styles.reportIssueContainer}
				onPress={() => {
					this.props.navigation.navigate("TicketCategory");
				}}>
				<GoappTextBold color={WHITE} size={15}>
					{i18n.t("report_issue")}
				</GoappTextBold>
			</TouchableOpacity>
		);
	};

	_renderLoader() {
		return (
			<View style={styles.loaderContainer}>
				<ProgressScreen
					isMessage={true}
					primaryMessage={"Hang On..."}
					message={"Getting Tickets"}
					indicatorColor={"#2C98F0"}
					indicatorSize={height / 50}
				/>
			</View>
		);
	}

	_renderTickets() {
		const { ticketsResponse } = this.props;
		return (
			<>
				{ticketsResponse &&
					ticketsResponse.map(item => (
						<React.Fragment key={item.ticketId}>
							<TouchableOpacity
								style={styles.ticketContainer}
								onPress={() =>
									this.props.navigation.navigate("TicketDetails", {
										ticketId: item.ticketId,
										isHomePage: true
									})
								}>
								<View style={styles.ticketBox}>
									<GoappTextRegular size={15}>
										{`Ticket #`}
										{item.ticketId}
									</GoappTextRegular>
									<View style={styles.ticketStatus}>
										<GoappTextBold size={14}>
											{statusTextChangeHandler(item.currentState)}
										</GoappTextBold>
										{item.currentState === "RESOLVED" ? (
											<GoappTextRegular size={10.5} color={"#9C9C9C"}>
												{`Reopen Valid till ${i18n.strftime(
													new Date(item.modifiedAt + 172800000),
													"%d/%m/%Y"
												)} `}
											</GoappTextRegular>
										) : (
											<GoappTextRegular />
										)}
									</View>
								</View>
								<GoappTextRegular size={15} color={"#313131"}>
									{item.subCategory}
								</GoappTextRegular>
								<GoappTextRegular
									size={10.5}
									color={"#9C9C9C"}
									style={styles.reopenTxt}>
									{`Reported on ${i18n.strftime(
										new Date(item.raisedAt),
										"%d/%m/%Y"
									)}`}
								</GoappTextRegular>
								<View style={styles.ticketBox}>
									{item.currentState === "RESOLVED" &&
									Date.parse(this.reopenTicketDateDuration(item.modifiedAt)) >
										Date.now() ? (
										<TouchableOpacity onPress={() => this._openAlert(item)}>
											<GoappTextMedium color={SKY_BLUE}>
												{i18n.t("reopen")}
											</GoappTextMedium>
										</TouchableOpacity>
									) : (
										<GoappTextMedium />
									)}

									<TouchableOpacity
										style={[styles.ticketBox, styles.commentBox]}
										onPress={() =>
											this.props.navigation.navigate("CommentsTicket", {
												ticketId: item.ticketId
											})
										}>
										<GoappTextMedium color={SKY_BLUE}>
											{i18n.t("comment")}
										</GoappTextMedium>
									</TouchableOpacity>
								</View>
							</TouchableOpacity>
							<View style={styles.seprator} />
						</React.Fragment>
					))}
			</>
		);
	}

	_renderNoTickets() {
		return (
			<View style={styles.noRecordContainer}>
				<FastImage
					source={NO_ISSUE_REPORTED}
					style={styles.noRecordImg}
					resizeMode={"contain"}
				/>
				<GoappTextBold>{i18n.t("no_issues_reported")}</GoappTextBold>
			</View>
		);
	}
}

const mapStateToProps = state => ({
	appToken: state.appToken.token,
	siteId: state.appToken.selectedSite.id,
	isTicketsLoading:
		state.campusAssist && state.campusAssist.getTickets.isTicketsLoading,
	ticketsResponse:
		state.campusAssist && state.campusAssist.getTickets.ticketsResponse,
	reopenTicketResponse:
		state.campusAssist && state.campusAssist.getTickets.reopenTicketResponse,
	isReopenTicketsLoading:
		state.campusAssist && state.campusAssist.getTickets.isReopenTicketsLoading,
	isTicketsError:
		state.campusAssist && state.campusAssist.getTickets.isTicketsError
});

Home.contextType = DialogContext;

export default connect(mapStateToProps)(Home);
