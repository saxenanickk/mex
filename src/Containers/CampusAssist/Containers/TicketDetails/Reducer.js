import {
	CAMPUS_ASSIST_GET_TICKET_DETAILS,
	CAMPUS_ASSIST_APPEND_COMMENT_TO_CHAT
} from "./Saga";
import { REQUEST, SUCCESS, FAILURE } from "../../../../utils/reduxUtils";

const initialState = {
	isTicketLoading: false,
	ticket: {},
	isTicketError: false
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case CAMPUS_ASSIST_GET_TICKET_DETAILS[REQUEST]:
			return {
				...state,
				isTicketLoading: !payload.setLoading,
				isTicketError: false
			};
		case CAMPUS_ASSIST_GET_TICKET_DETAILS[SUCCESS]:
			return {
				...state,
				isTicketLoading: false,
				isTicketError: false,
				ticket: payload.response
			};
		case CAMPUS_ASSIST_GET_TICKET_DETAILS[FAILURE]:
			return { ...state, isTicketLoading: false, isTicketError: true };
		case CAMPUS_ASSIST_APPEND_COMMENT_TO_CHAT:
			let tempComment = state.ticket.comment.slice();
			return {
				...state,
				ticket: {
					...state.ticket,
					comment: [payload, ...tempComment]
				}
			};
		default:
			return state;
	}
};
