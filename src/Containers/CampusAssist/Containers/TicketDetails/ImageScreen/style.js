import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	container: {
		position: "absolute",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0
	},
	closeIcon: {
		position: "absolute",
		top: width / 20,
		right: width / 20,
		paddingHorizontal: width / 20,
		paddingVertical: height / 100
	}
});
