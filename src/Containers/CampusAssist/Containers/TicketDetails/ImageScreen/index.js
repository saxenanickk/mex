import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	Platform,
	ActivityIndicator
} from "react-native";
import ImageViewer from "react-native-image-zoom-viewer";
import { connect } from "react-redux";
import { Icon } from "../../../../../Components";
import { styles } from "./style";
import { get } from "lodash";

const { width } = Dimensions.get("window");

class ImageScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			imageIndex: this.props.route.params.imageIndex || ""
		};
	}
	changeImages = imageIndex => {
		this.setState({
			imageIndex: imageIndex
		});
	};
	render() {
		const { ticket } = this.props;
		let images = [];
		ticket &&
			ticket.image.length > 0 &&
			(images = [
				...this.props.ticket.image.map(url => ({
					url
				}))
			]);

		return (
			<View style={styles.container}>
				{this.props.ticket.image.length === 1 ? (
					<ImageViewer
						imageUrls={images}
						loadingRender={() => <ActivityIndicator size={"large"} />}
						renderIndicator={() => null}
						saveToLocalByLongPress={false}
					/>
				) : (
					<ImageViewer
						imageUrls={images}
						loadingRender={() => <ActivityIndicator size={"large"} />}
						renderIndicator={() => null}
						index={this.state.imageIndex}
						saveToLocalByLongPress={false}
						renderArrowLeft={() => (
							<TouchableOpacity onPress={() => this.changeImages(0)}>
								<Icon
									iconName={"ios-arrow-dropleft"}
									iconType={"icomoon"}
									iconSize={width / 12}
									iconColor={"#fff"}
								/>
							</TouchableOpacity>
						)}
						renderArrowRight={() => (
							<TouchableOpacity onPress={() => this.changeImages(1)}>
								<Icon
									iconName={"ios-arrow-dropright"}
									iconType={"icomoon"}
									iconSize={width / 12}
									iconColor={"#fff"}
								/>
							</TouchableOpacity>
						)}
					/>
				)}
				<TouchableOpacity
					style={styles.closeIcon}
					onPress={() => this.props.navigation.goBack()}>
					<Icon
						iconName={Platform.OS === "android" ? "md-close" : "ios-close"}
						iconType={"ionicon"}
						iconSize={width / 12}
						iconColor={"#fff"}
					/>
				</TouchableOpacity>
			</View>
		);
	}
}

const mapStateToProps = state => ({
	ticket: get(state, "campusAssist.ticketDetails.ticket", {})
});

export default connect(mapStateToProps)(ImageScreen);
