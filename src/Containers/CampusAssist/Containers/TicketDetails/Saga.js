import { put, call, takeLatest } from "redux-saga/effects";
import CampusAssistApi from "../../Api";
import {
	createRequestTypes,
	action,
	REQUEST,
	SUCCESS,
	FAILURE
} from "../../../../utils/reduxUtils";

export const CAMPUS_ASSIST_GET_TICKET_DETAILS = createRequestTypes(
	"CAMPUS_ASSIST_GET_TICKET_DETAILS"
);

export const CAMPUS_ASSIST_APPEND_COMMENT_TO_CHAT =
	"CAMPUS_ASSIST_APPEND_COMMENT_TO_CHAT";

export const campusAssistGetTicketDetails = {
	request: (appToken, site_id, ticket_id, setLoading) =>
		action(CAMPUS_ASSIST_GET_TICKET_DETAILS[REQUEST], {
			appToken,
			site_id,
			ticket_id,
			setLoading
		}),
	success: response =>
		action(CAMPUS_ASSIST_GET_TICKET_DETAILS[SUCCESS], {
			response
		}),
	failure: error =>
		action(CAMPUS_ASSIST_GET_TICKET_DETAILS[FAILURE], {
			error
		})
};

export const campusAssistAppendCommentToChat = payload =>
	action(CAMPUS_ASSIST_APPEND_COMMENT_TO_CHAT, payload);

export function* campusAssistTicketDetailsSaga(dispatch) {
	yield takeLatest(
		CAMPUS_ASSIST_GET_TICKET_DETAILS[REQUEST],
		handleGetTicketDetails
	);
}

function* handleGetTicketDetails(param) {
	try {
		let response = yield call(CampusAssistApi.getTicketById, param.payload);
		response && response.comment.reverse();
		yield put(campusAssistGetTicketDetails.success(response));
	} catch (error) {
		yield put(campusAssistGetTicketDetails.failure(error));
	}
}
