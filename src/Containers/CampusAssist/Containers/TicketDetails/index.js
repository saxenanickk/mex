import React, { Component } from "react";
import { View, Dimensions, ScrollView } from "react-native";
import { GoappTextBold } from "../../../../Components/GoappText";
import FastImage from "react-native-fast-image";
import CommentSvg from "../../../../Components/Icons/CommentIcon";
import styles from "./styles";
import { TouchableOpacity } from "react-native-gesture-handler";
import { connect } from "react-redux";
import { campusAssistGetTicketDetails } from "./Saga";
import CommunityLoader from "../../../Community/Components/CommunityLoader";
import DialogContext from "../../../../DialogContext";
import I18n from "../../Assests/Strings/i18n";
import { statusTextChangeHandler } from "../../Utils/statusText";

const { width } = Dimensions.get("window");

class TicketDetails extends Component {
	componentDidMount() {
		this.getTicketDetails();
	}

	componentDidUpdate(prevProps, prevState) {
		if (
			this.props.isTicketError &&
			this.props.isTicketError !== prevProps.isTicketError
		) {
			this.context.current.openDialog({
				type: "Action",
				title: "Error",
				message: "Failed to fetch ticket details",
				rightTitle: "Retry",
				rightPress: () => this.getTicketDetails()
			});
		}
	}

	getTicketDetails = () => {
		const { route } = this.props;
		const ticketId = route.params.ticketId || null;

		this.props.dispatch(
			campusAssistGetTicketDetails.request(
				this.props.appToken,
				this.props.site_id,
				ticketId
			)
		);
	};
	componentWillUnmount() {
		const { route } = this.props;
		const isHomePage = route.params.isHomePage || false;

		if (isHomePage) {
			this.props.dispatch(campusAssistGetTicketDetails.success({}));
		}
	}

	render() {
		if (this.props.isTicketLoading) {
			return (
				<View style={styles.loader}>
					<CommunityLoader size={width / 20} />
				</View>
			);
		}

		if (this.props.isTicketError) {
			return null;
		}

		if (this.props.ticket) {
			const { ticket } = this.props;
			// const labels = ["NEW", "IN_PROGRESS", "RESOLVED", "CLOSED"];
			// let currentPosition = labels.findIndex(
			// 	item => item === ticket.currentState
			// );
			let raisedAt =
				ticket.raisedAt &&
				I18n.strftime(new Date(ticket.raisedAt), "%a %b %d, %I:%M %P");
			return (
				<ScrollView style={styles.mainContainer}>
					{/* <View style={styles.stepperContainer}>
						<Stepper
							circleSize={18}
							seperatorHeight={3}
							labels={["New", "In Progress", "Resolved", "Closed"]}
							currentPosition={currentPosition}
							/>
					</View> */}
					<View style={[styles.problemTextView, styles.stepperContainer]}>
						<GoappTextBold style={styles.textColor}>{"Status"}</GoappTextBold>
						<View style={styles.problemTextView}>
							<GoappTextBold>
								{statusTextChangeHandler(this.props.ticket.currentState)}
							</GoappTextBold>
						</View>
					</View>
					<View style={styles.line} />
					<View style={styles.problemView}>
						<GoappTextBold style={styles.textColor}>
							{this.props.ticket.primaryCategory}
						</GoappTextBold>
						<View style={styles.problemTextView}>
							<GoappTextBold>{this.props.ticket.subCategory}</GoappTextBold>
							<View style={styles.line} />
						</View>
					</View>
					<View>
						<GoappTextBold style={styles.textColor}>{"When"}</GoappTextBold>
						<View style={styles.problemTextView}>
							<GoappTextBold>{raisedAt}</GoappTextBold>
							<View style={styles.problemTextView}>
								<GoappTextBold style={styles.place}>{"Where"}</GoappTextBold>
								<GoappTextBold>{this.props.ticket.zoneAddress}</GoappTextBold>
							</View>
							<View style={styles.line} />
						</View>
					</View>
					<View style={styles.problemView}>
						<GoappTextBold style={styles.textColor}>
							{"Description"}
						</GoappTextBold>
						<View style={styles.problemTextView}>
							<GoappTextBold style={styles.descriptionText}>
								{this.props.ticket.description &&
								this.props.ticket.description.length > 0
									? this.props.ticket.description
									: "No description"}
							</GoappTextBold>
							<View>
								<GoappTextBold style={styles.attachmentText}>
									{"Attachments"}
								</GoappTextBold>
								<View style={styles.imageView}>
									{this.props.ticket &&
									Array.isArray(this.props.ticket.image) &&
									this.props.ticket.image.length > 0 ? (
										this.props.ticket.image.map((uri, index) => {
											if (uri.startsWith("https://")) {
												return (
													<TouchableOpacity
														onPress={() =>
															this.props.navigation.navigate("ImageScreen", {
																imageIndex: index
															})
														}
														key={index}>
														<FastImage
															source={{ uri }}
															resizeMode={"contain"}
															style={styles.attachImages}
														/>
													</TouchableOpacity>
												);
											}
											return null;
										})
									) : (
										<GoappTextBold>No attachments</GoappTextBold>
									)}
								</View>
							</View>
							<View style={styles.line} />
						</View>
					</View>
					<TouchableOpacity
						style={styles.commentView}
						onPress={() =>
							this.props.navigation.navigate("CommentsTicket", {
								isTicketDetails: true,
								ticketId: this.props.ticket.ticketId
							})
						}>
						<CommentSvg />
						<GoappTextBold style={styles.commentText}>
							{"Comment"}
						</GoappTextBold>
					</TouchableOpacity>
				</ScrollView>
			);
		}
	}
}
const mapStateToProps = state => ({
	appToken: state.appToken.token,
	site_id: state.appToken.selectedSite.id,
	ticket: state.campusAssist && state.campusAssist.ticketDetails.ticket,
	isTicketLoading:
		state.campusAssist && state.campusAssist.ticketDetails.isTicketLoading,
	isTicketError:
		state.campusAssist && state.campusAssist.ticketDetails.isTicketError
});
TicketDetails.contextType = DialogContext;
export default connect(mapStateToProps)(TicketDetails);
