import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	mainContainer: {
		flex: 1,
		paddingHorizontal: width / 20,
		backgroundColor: "#ffffff"
	},
	stepperContainer: {
		marginTop: width / 18
	},
	loader: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	problemView: {
		color: "#000000",
		marginVertical: width / 18
	},
	problemTextView: {
		marginTop: width / 40
	},
	textColor: {
		color: "#8b8b8b"
	},
	line: {
		marginTop: width / 18,
		borderBottomWidth: 2,
		borderColor: "#e8e8e8"
	},
	place: {
		color: "#8b8b8b",
		marginVertical: width / 40
	},
	descriptionText: {
		marginRight: width / 18
	},
	attachmentText: {
		color: "#8b8b8b",
		marginVertical: width / 40
	},
	imageView: {
		flexDirection: "row"
	},
	attachImages: {
		height: 80,
		width: 80,
		borderWidth: 1,
		borderColor: "transparent",
		marginRight: width / 30
	},
	commentView: {
		flexDirection: "row",
		marginBottom: height / 20
	},
	commentText: {
		color: "#349cf1",
		marginLeft: width / 70
	}
});
export default styles;
