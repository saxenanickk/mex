import { put, call, takeLatest } from "redux-saga/effects";
import CampusAssistApi from "../../../Api";
import {
	createRequestTypes,
	action,
	REQUEST,
	SUCCESS,
	FAILURE
} from "../../../../../utils/reduxUtils";

export const CAMPUS_ASSIST_GET_SUB_CATEGORY = createRequestTypes(
	"CAMPUS_ASSIST_GET_SUB_CATEGORY"
);

export const campusAssistGetSubCategories = {
	request: (appToken, site_id, category) =>
		action(CAMPUS_ASSIST_GET_SUB_CATEGORY[REQUEST], {
			appToken,
			site_id,
			category
		}),
	success: response =>
		action(CAMPUS_ASSIST_GET_SUB_CATEGORY[SUCCESS], { response }),
	failure: error => action(CAMPUS_ASSIST_GET_SUB_CATEGORY[FAILURE], { error })
};
export function* campusAssistGetSubCategoriesSaga(dispatch) {
	yield takeLatest(
		CAMPUS_ASSIST_GET_SUB_CATEGORY[REQUEST],
		handleGetSubCategories
	);
}

function* handleGetSubCategories(params) {
	try {
		let SubCategories = yield call(
			CampusAssistApi.getSubCategories,
			params.payload
		);
		yield put(campusAssistGetSubCategories.success(SubCategories));
	} catch (error) {
		yield put(campusAssistGetSubCategories.failure(error));
	}
}
