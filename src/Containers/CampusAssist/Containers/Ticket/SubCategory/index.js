import React, { Component } from "react";
import { TouchableOpacity, View, ScrollView } from "react-native";
import {
	GoappTextRegular,
	GoappTextBold
} from "../../../../../Components/GoappText";
import { styles } from "./style";
import CommunityLoader from "../../../../Community/Components/CommunityLoader";
import DialogContext from "../../../../../DialogContext";
import { RED, WHITE, SKY_BLUE } from "../../../Color";
import { get } from "lodash";
import { connect } from "react-redux";
import { campusAssistGetSubCategories } from "./Saga";
import { PLEASE_SELECT_AN_ISSUE } from "../../../Constants";
import { CAMPUS_ASSIST_GET_SELECTED_SUB_CATEGORY } from "./Reducer";
import { CAMPUS_ASSIST_RESET_CATEGORY } from "../Category/Reducer";
import { WARNING, ERRORCLOUD } from "../../../../../Assets/Img/Image";

class SubCategory extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isSubCategorySelected: false
		};
	}

	componentDidMount() {
		if (this.props.internetStatus) {
			this.getSubCategory();
		} else {
			this.openNoInternetAlert(this.getSubCategory);
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (
			this.props.isSubCategoriesError &&
			this.props.isSubCategoriesError !== prevProps.isSubCategoriesError
		) {
			this.context.current.openDialog({
				type: "Confirmation",
				title: "Oops!",
				message: "Failed to get Sub Categories!",
				leftPress: () => this.props.navigation.goBack(),
				rightTitle: "Retry",
				rightPress: () => this.getSubCategory(),
				icon: WARNING
			});
		}
	}

	openNoInternetAlert = (retryFunc, funcParams) => {
		this.context.current.openDialog({
			type: "DismissableAction",
			title: "No Internet Connection",
			message: "Turn on Internet and retry",
			rightTitle: "Retry",
			rightPress: () => {
				setTimeout(() => {
					retryFunc(funcParams);
				}, 500);
			},
			icon: ERRORCLOUD
		});
	};

	getSubCategory = () => {
		this.props.dispatch(
			campusAssistGetSubCategories.request(
				this.props.appToken,
				this.props.siteId,
				this.props.selectedCategory
			)
		);
	};

	selectedSubCategoryHandler = subcategory => {
		this.props.dispatch({
			type: CAMPUS_ASSIST_GET_SELECTED_SUB_CATEGORY,
			payload: subcategory
		});
	};

	navigationHandler = () => {
		if (this.props.selectedSubCategory !== "") {
			this.props.navigation.navigate("TicketIncidentVenue");
		} else {
			this.setState({
				isSubCategorySelected: true
			});
		}
	};

	goBackHandler = () => {
		this.context.current.openDialog({
			type: "Confirmation",
			message: "Are you sure you want to cancel?",
			leftTitle: "No",
			rightTitle: "Yes",
			rightPress: () => this.resetSelectedCategory()
		});
	};

	resetSelectedCategory = () => {
		this.props.navigation.navigate("Home");
		this.props.dispatch({
			type: CAMPUS_ASSIST_RESET_CATEGORY
		});
	};

	render() {
		const {
			subCategory,
			isSubCategoriesLoading,
			isSubCategoriesError
		} = this.props;

		return (
			<View style={styles.container}>
				{isSubCategoriesLoading ? (
					<View style={styles.loaderContainer}>
						<CommunityLoader />
					</View>
				) : isSubCategoriesError ? (
					<View style={styles.errorContainer}>
						<GoappTextBold>{`Unable to load sub categories`}</GoappTextBold>
						<TouchableOpacity
							onPress={this.getSubCategory}
							style={styles.retryButton}>
							<GoappTextBold color={"#fff"}>{`Retry`}</GoappTextBold>
						</TouchableOpacity>
					</View>
				) : (
					<>
						<ScrollView style={styles.listContainer}>
							{subCategory.map(item => this.renderListItem(item))}
						</ScrollView>
						{this._renderEmptyValueAlert()}
						{this._renderFooter()}
					</>
				)}
			</View>
		);
	}

	renderListItem(item) {
		return (
			<TouchableOpacity
				key={item.id}
				style={[
					styles.categoryContainer,
					item.id === this.props.selectedSubCategory && styles.selectedCategory
				]}
				onPress={() => {
					this.selectedSubCategoryHandler(item.id);
				}}>
				<GoappTextRegular
					style={[
						item.id === this.props.selectedSubCategory &&
							styles.selectedIssueText
					]}>
					{item.name}
				</GoappTextRegular>
			</TouchableOpacity>
		);
	}

	_renderEmptyValueAlert() {
		return (
			<View style={styles.alertWarning}>
				{this.state.isSubCategorySelected &&
				this.props.selectedSubCategory.length < 1 ? (
					<GoappTextRegular color={RED} size={10}>
						{PLEASE_SELECT_AN_ISSUE}
					</GoappTextRegular>
				) : null}
			</View>
		);
	}

	_renderFooter() {
		return (
			<View style={styles.buttonContainer}>
				<TouchableOpacity
					style={[styles.footerButton, styles.cancelButton]}
					onPress={this.goBackHandler}>
					<GoappTextBold color={SKY_BLUE}>{`Cancel`}</GoappTextBold>
				</TouchableOpacity>

				<TouchableOpacity
					style={[styles.footerButton, styles.nextButton]}
					onPress={this.navigationHandler}>
					<GoappTextBold color={WHITE}>{`Next`}</GoappTextBold>
				</TouchableOpacity>
			</View>
		);
	}
}
const mapStateToProps = state => ({
	appToken: state.appToken.token,
	siteId: state.appToken.selectedSite.id,
	isSubCategoriesLoading: get(
		state,
		"campusAssist.getSubCategory.isSubCategoriesLoading",
		""
	),
	isSubCategoriesError:
		state.campusAssist &&
		state.campusAssist.getSubCategory.isSubCategoriesError,
	selectedCategory: get(state, "campusAssist.getCategory.selectedCategory", ""),
	subCategory: get(state, "campusAssist.getSubCategory.subCategories", []),
	selectedSubCategory: get(
		state,
		"campusAssist.getSubCategory.selectedSubCategory",
		""
	),
	internetStatus: state.netInfo.status
});

SubCategory.contextType = DialogContext;

export default connect(mapStateToProps)(SubCategory);
