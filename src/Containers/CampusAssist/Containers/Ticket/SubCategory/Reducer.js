import { CAMPUS_ASSIST_GET_SUB_CATEGORY } from "./Saga";
import { REQUEST, SUCCESS, FAILURE } from "../../../../../utils/reduxUtils";

export const CAMPUS_ASSIST_GET_SELECTED_SUB_CATEGORY =
	"CAMPUS_ASSIST_GET_SELECTED_SUB_CATEGORY";
const initialState = {
	isSubCategoriesLoading: false,
	subCategories: [],
	isSubCategoriesError: false,
	selectedSubCategory: ""
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case CAMPUS_ASSIST_GET_SUB_CATEGORY[REQUEST]:
			return {
				...state,
				isSubCategoriesLoading: true,
				isSubCategoriesError: false,
				selectedSubCategory: ""
			};
		case CAMPUS_ASSIST_GET_SUB_CATEGORY[SUCCESS]:
			return {
				...state,
				isSubCategoriesLoading: false,
				subCategories: payload.response,
				selectedSubCategory: ""
			};
		case CAMPUS_ASSIST_GET_SUB_CATEGORY[FAILURE]:
			return {
				...state,
				isSubCategoriesLoading: false,
				isSubCategoriesError: true
			};
		case CAMPUS_ASSIST_GET_SELECTED_SUB_CATEGORY:
			return { ...state, selectedSubCategory: payload };
		default:
			return state;
	}
};
