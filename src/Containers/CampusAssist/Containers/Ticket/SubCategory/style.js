import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	container: {
		paddingVertical: height / 40,
		backgroundColor: "#ffffff",
		flex: 1
	},
	errorContainer: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center"
	},
	categoryContainer: {
		borderRadius: height / 1,
		padding: height / 70,
		marginVertical: height / 95,
		marginHorizontal: width / 20,
		backgroundColor: "#F5F6F9"
	},
	selectedCategory: {
		backgroundColor: "#2C98F0"
	},
	selectedIssueText: {
		color: "white"
	},
	retryButton: {
		backgroundColor: "#2C98F0",
		borderRadius: height / 10,
		paddingVertical: height / 70,
		paddingHorizontal: width / 8,
		marginTop: width * 0.03
	},
	footerButton: {
		borderRadius: height / 10,
		paddingVertical: height / 70,
		paddingHorizontal: width / 8
	},
	nextButton: {
		backgroundColor: "#2C98F0"
	},
	buttonContainer: {
		flexDirection: "row",
		justifyContent: "space-around"
	},
	cancelButton: {
		borderWidth: 1,
		borderColor: "#2C98F0"
	},
	alertWarning: {
		alignItems: "flex-end",
		paddingRight: width / 7,
		justifyContent: "center",
		height: height / 30
	},
	listContainer: {},
	loaderContainer: { justifyContent: "center", flex: 1, alignItems: "center" }
});
