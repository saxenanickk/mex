import { put, call, takeLatest, select } from "redux-saga/effects";
import CampusAssistApi from "../../../Api";
import {
	createRequestTypes,
	action,
	REQUEST,
	SUCCESS,
	FAILURE
} from "../../../../../utils/reduxUtils";

export const CAMPUS_ASSIST_GET_SITES = createRequestTypes(
	"CAMPUS_ASSIST_GET_SITES"
);
export const CAMPUS_ASSIST_GET_BUILDINGS = createRequestTypes(
	"CAMPUS_ASSIST_GET_BUILDINGS"
);
export const CAMPUS_ASSIST_GET_FLOOR = createRequestTypes(
	"CAMPUS_ASSIST_GET_FLOOR"
);

export const campusAssistGetSites = {
	request: appToken => action(CAMPUS_ASSIST_GET_SITES[REQUEST], { appToken }),
	success: response => action(CAMPUS_ASSIST_GET_SITES[SUCCESS], { response }),
	failure: error => action(CAMPUS_ASSIST_GET_SITES[FAILURE], error)
};

export const campusAssistGetBuildings = {
	request: (appToken, site_id) =>
		action(CAMPUS_ASSIST_GET_BUILDINGS[REQUEST], { appToken, site_id }),
	success: response =>
		action(CAMPUS_ASSIST_GET_BUILDINGS[SUCCESS], { response }),
	failure: error => action(CAMPUS_ASSIST_GET_BUILDINGS[FAILURE], error)
};

export const campusAssistGetFloors = {
	request: (appToken, site_id, building_id) =>
		action(CAMPUS_ASSIST_GET_FLOOR[REQUEST], {
			appToken,
			site_id,
			building_id
		}),
	success: response => action(CAMPUS_ASSIST_GET_FLOOR[SUCCESS], { response }),
	failure: error => action(CAMPUS_ASSIST_GET_FLOOR[FAILURE], error)
};

export function* campusAssistTicketVenueSaga(dispatch) {
	yield takeLatest(CAMPUS_ASSIST_GET_SITES[REQUEST], handleGetSites);
	yield takeLatest(CAMPUS_ASSIST_GET_BUILDINGS[REQUEST], handleGetBuildings);
	yield takeLatest(CAMPUS_ASSIST_GET_FLOOR[REQUEST], handleGetFloors);
}

function* handleGetFloors(params) {
	try {
		let floors = yield call(CampusAssistApi.getFloors, params.payload);
		yield put(campusAssistGetFloors.success(floors));
	} catch (err) {
		yield put(campusAssistGetFloors.failure(err));
	}
}

function* handleGetBuildings(params) {
	try {
		let buildings = yield call(CampusAssistApi.getBuildings, params.payload);
		yield put(campusAssistGetBuildings.success(buildings));
	} catch (err) {
		yield put(campusAssistGetBuildings.failure(err));
	}
}

function* handleGetSites(params) {
	try {
		// let sites = yield call(CampusAssistApi.getSites, params.payload);
		let sites = yield select(state => [state.appToken.selectedSite]);
		yield put(campusAssistGetSites.success(sites));
	} catch (err) {
		yield put(campusAssistGetSites.failure(err));
	}
}
