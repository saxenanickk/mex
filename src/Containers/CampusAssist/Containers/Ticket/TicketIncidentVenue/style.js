import { StyleSheet, Dimensions, Platform } from "react-native";

const { height, width } = Dimensions.get("window");
export const styles = StyleSheet.create({
	container: {
		marginBottom: height / 100
	},
	containerBox: {
		paddingVertical: height / 30,
		paddingHorizontal: width / 20,
		flex: 1,
		backgroundColor: "#ffffff"
	},
	errorContainer: {
		justifyContent: "space-between",
		flexDirection: "row"
	},
	dateTimeInput: {
		flexDirection: "row",
		alignItems: "center",
		width: width / 2.4,
		justifyContent: "space-around",
		paddingVertical: height / 55,
		borderRadius: height / 10,
		backgroundColor: "#F5F6F9"
	},
	locationInput: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		paddingHorizontal: width / 17,
		paddingVertical: height / 55,
		borderRadius: height / 10,
		backgroundColor: "#F5F6F9"
	},
	dataTime: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	itemsHeading: {
		paddingLeft: width / 18,
		marginBottom: height / 100
	},
	buttonContainer: {
		flexDirection: "row",
		justifyContent: "space-around",
		paddingHorizontal: width / 20,
		paddingVertical: height / 40,
		backgroundColor: "#ffffff"
	},
	button: {
		borderRadius: height / 10,
		paddingVertical: height / 70,
		paddingHorizontal: width / 8
	},
	retryButton: {
		borderRadius: height / 10,
		paddingVertical: height / 90,
		paddingHorizontal: width / 18,
		alignItems: "center",
		alignSelf: "center"
	},
	align: { paddingHorizontal: width / 55 },
	nextColor: {
		backgroundColor: "#2C98F0"
	},
	nextColor2: { backgroundColor: "#c8c8c8" },
	cancelButton: {
		borderColor: "#2C98F0",
		borderWidth: 1
	},
	locationLoading: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		marginVertical: height * 0.01,
		paddingVertical: height * 0.015
	},
	picker: {
		backgroundColor: "#f5f6f9",
		borderRadius: width * 0.5,
		paddingVertical: height / 60,
		...Platform.select({
			android: {
				paddingLeft: width * 0.04
			},
			ios: {
				paddingHorizontal: 8
			}
		})
	},
	alertWarning: {
		alignItems: "flex-end",
		paddingRight: 30,
		backgroundColor: "#ffffff"
	},
	goPickerStyle: {
		width: "100%",
		justifyContent: "space-around",
		paddingLeft: 8
	},
	pickerContainer: {
		justifyContent: "space-between",
		paddingHorizontal: 12,
		paddingVertical: 12
	}
});
