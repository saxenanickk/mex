import React, { Component } from "react";
import { View, TouchableOpacity, Dimensions } from "react-native";
import {
	GoappTextRegular,
	GoappTextBold
} from "../../../../../Components/GoappText";
import { connect } from "react-redux";
import { styles } from "./style";
import { Icon, GoToast, Dropdown } from "../../../../../Components/index";
import GoDateTimePicker from "../../../../../Components/GoDateTimePicker";
import {
	campusAssistGetSites,
	campusAssistGetBuildings,
	campusAssistGetFloors
} from "./Saga";
import { _24to12 } from "../../../../../utils/dateUtils";
import DialogContext from "../../../../../DialogContext";
import {
	CAMPUS_ASSIST_SELECTED_SITE,
	CAMPUS_ASSIST_SELECTED_BUILDINGS,
	CAMPUS_ASSIST_SELECTED_FLOOR,
	CAMPUS_ASSIST_SELECTED_DATE,
	CAMPUS_ASSIST_RESET_FORM,
	CAMPUS_ASSIST_RESET_BUILDINGS,
	CAMPUS_ASSIST_RESET_FLOORS
} from "./Reducer";
import { CAMPUS_ASSIST_RESET_CATEGORY } from "../Category/Reducer";
import { WARNING, ERRORCLOUD } from "../../../../../Assets/Img/Image";
import i18n from "../../../../../Assets/strings/i18n";
const { width } = Dimensions.get("window");

const minimumDate = new Date().setMonth(new Date().getMonth() - 1);

class TicketIncidentVenue extends Component {
	static defaultProps = {
		date: new Date(),
		isLocationsLoading: true,
		isLocationsError: false
	};

	constructor(props) {
		super(props);
		this.state = {
			isDatePickerVisible: false,
			datePickerType: null,
			type: null,
			pickerDate: null,
			errorMessage: null,
			isAlertWarning: true
		};
	}

	componentDidMount() {
		this.getSites();
	}
	componentWillUnmount() {
		this.props.dispatch({
			type: CAMPUS_ASSIST_RESET_FORM
		});
	}

	componentDidUpdate(prevProps) {
		if (
			this.props.selectedSite !== "Select" &&
			this.props.selectedSite.id !== prevProps.selectedSite.id
		) {
			this.props.dispatch({
				type: CAMPUS_ASSIST_RESET_BUILDINGS
			});
			this.getBuildings(this.props.selectedSite.id);
		}
		if (
			this.props.selectedBuilding !== "Select" &&
			this.props.selectedBuilding.id !== prevProps.selectedBuilding.id
		) {
			this.props.dispatch({
				type: CAMPUS_ASSIST_RESET_FLOORS
			});
		}
		if (
			this.props.isBuildingsError &&
			this.props.isBuildingsError !== prevProps.isBuildingsError
		) {
			this.props.dispatch({
				type: CAMPUS_ASSIST_RESET_BUILDINGS
			});
		}

		if (
			this.props.isSitesError &&
			this.props.isSitesError !== prevProps.isSitesError
		) {
			this.handleOpenAlert("sites");
		}

		if (
			this.props.isBuildingsError &&
			this.props.isBuildingsError !== prevProps.isBuildingsError
		) {
			this.handleOpenAlert("buildings");
		}

		if (
			this.props.isFloorsError &&
			this.props.isFloorsError !== prevProps.isFloorsError
		) {
			this.handleOpenAlert("floors");
		}
	}

	handleOpenAlert = type => {
		this.context.current.openDialog({
			type: "Confirmation",
			title: "Oops!",
			message: `Failed to get ${type}!`,
			leftPress: () => this.props.navigation.goBack(),
			rightTitle: "Retry",
			rightPress: () => this.handleRetryPress(type),

			icon: WARNING
		});
	};

	handleRetryPress = type => {
		switch (type) {
			case "sites":
				this.getSites();
				break;

			case "buildings":
				this.getBuildings(this.props.selectedSite.id);
				break;

			case "floors":
				this.getFloors([this.props.selectedBuilding.id]);
				break;

			default:
				break;
		}
	};

	getSites = () => {
		if (this.props.internetStatus) {
			this.props.dispatch(campusAssistGetSites.request(this.props.appToken));
		} else {
			this.openNoInternetAlert(this.getSites);
		}
	};

	getBuildings = selectedSiteId => {
		if (this.props.internetStatus) {
			this.props.dispatch(
				campusAssistGetBuildings.request(this.props.appToken, selectedSiteId)
			);
		} else {
			this.openNoInternetAlert(this.getBuildings, selectedSiteId);
		}
	};

	getFloors = selectedFloor => {
		if (this.props.internetStatus) {
			this.props.dispatch(
				campusAssistGetFloors.request(
					this.props.appToken,
					this.props.selectedSite.id,
					selectedFloor
				)
			);
			this.setState({
				isAlertWarning: false
			});
		} else {
			this.openNoInternetAlert(this.getFloors, selectedFloor);
		}
	};

	showDatePicker = (datePickerType, type, pickerDate) => {
		if (!this.state.isDatePickerVisible) {
			this.setState({
				isDatePickerVisible: true,
				datePickerType,
				type,
				pickerDate,
				errorMessage: null
			});
		} else {
			this.hideDatePicker();
		}
	};

	hideDatePicker = errorMessage => {
		this.setState(
			{
				isDatePickerVisible: false,
				datePickerType: null,
				type: null,
				pickerDate: null,
				errorMessage
			},
			() => {
				if (this.state.errorMessage) {
					setTimeout(() => {
						GoToast.show(errorMessage, "Error", 2000);
					}, 400);
				}
			}
		);
	};
	handleDateChange = date => {
		// If user selects minimum date set time to current time
		if (
			new Date().getDate() === new Date(date).getDate() &&
			new Date().getMonth() === new Date(date).getMonth()
		) {
			this.props.dispatch({
				type: CAMPUS_ASSIST_SELECTED_DATE,
				payload: Date.parse(new Date())
			});
		}
		// else preserve start time and end time
		else {
			let startTime = new Date(this.props.seletedDateTime);
			this.props.dispatch({
				type: CAMPUS_ASSIST_SELECTED_DATE,
				payload: new Date(date).setHours(
					startTime.getHours(),
					startTime.getMinutes()
				)
			});
		}
	};
	handleStartTimeChange = date => {
		let startTime = new Date(date);
		let incidentTime =
			new Date(this.props.seletedDateTime).toDateString() +
			" " +
			new Date(startTime).toTimeString();
		this.props.dispatch({
			type: CAMPUS_ASSIST_SELECTED_DATE,
			payload: Date.parse(incidentTime)
		});
	};
	handleConfirm = date => {
		let errorMessage = null;
		this.setState({ isDatePickerVisible: false });
		if (this.state.type === "date") {
			this.handleDateChange(Date.parse(date));
		}
		if (this.state.type === "Time") {
			const currentDate = new Date();
			if (
				new Date(this.props.seletedDateTime).toDateString() ===
				currentDate.toDateString()
			) {
				if (
					date.toTimeString() === currentDate.toTimeString() ||
					date.toTimeString() >= currentDate.toTimeString()
				) {
					errorMessage = "Start Time cannot be greater than current time";
				} else {
					this.handleStartTimeChange(Date.parse(date));
				}
			} else {
				this.handleStartTimeChange(Date.parse(date));
			}
		}
		this.hideDatePicker(errorMessage);
	};

	handleLocationChange = (selectedItemName, type) => {
		if (type === "Site") {
			this.selectedSiteHandler(selectedItemName);
		} else if (type === "Building") {
			this.selectedBuildingHandler(selectedItemName);
		} else if (type === "Floor") {
			this.selectedFloorHandler(selectedItemName);
		}
	};

	selectedFloorHandler = selectedFloorName => {
		let selectedFloor = this.props.floors.find(
			floor => floor.name === selectedFloorName
		);
		this.props.dispatch({
			type: CAMPUS_ASSIST_SELECTED_FLOOR,
			payload: selectedFloor
		});
	};

	selectedBuildingHandler = selectedBuildingName => {
		let selectedBuilding = this.props.buildings.find(
			building => building.name === selectedBuildingName
		);
		this.props.dispatch({
			type: CAMPUS_ASSIST_SELECTED_BUILDINGS,
			payload: selectedBuilding
		});
		this.getFloors([selectedBuilding.id]);
	};

	selectedSiteHandler = selectedSiteName => {
		let selectedSite = this.props.sites.find(
			site => site.name === selectedSiteName
		);
		this.props.dispatch({
			type: CAMPUS_ASSIST_SELECTED_SITE,
			payload: selectedSite
		});
		this.getBuildings(selectedSite.id);
	};

	openNoInternetAlert = (retryFunc, funcParams) => {
		this.context.current.openDialog({
			type: "DismissableAction",
			title: "No Internet Connection",
			message: "Turn on Internet and retry",
			rightTitle: "Retry",
			rightPress: () => {
				setTimeout(() => {
					retryFunc(funcParams);
				}, 500);
			},
			icon: ERRORCLOUD
		});
	};

	goBack = () => {
		this.context.current.openDialog({
			type: "Confirmation",
			message: "Are you sure you want to cancel?",
			leftTitle: "No",
			rightTitle: "Yes",
			rightPress: () => this.resetFormData()
		});
	};

	resetFormData = () => {
		this.props.navigation.navigate("Home");
		this.props.dispatch({
			type: CAMPUS_ASSIST_RESET_CATEGORY
		});
		this.props.dispatch({
			type: CAMPUS_ASSIST_RESET_FORM
		});
	};

	navigationHandler = () => {
		this.props.navigation.navigate("TicketAdditionalDetails");
	};

	render() {
		return (
			<>
				<View style={styles.containerBox}>
					<View style={styles.dataTime}>
						{this._renderIncidentDate()}
						{this._renderIncidentTime()}
					</View>
					{this._renderSelectLocation()}
				</View>
				{this.state.isAlertWarning && (
					<View style={styles.alertWarning}>
						<GoappTextRegular color={"red"} size={10}>
							{`Please fill the above information`}
						</GoappTextRegular>
					</View>
				)}
				{this._renderFooter()}
				{this._renderGoDetTimePicker()}
			</>
		);
	}

	_renderIncidentDate() {
		return (
			<View style={styles.container}>
				<GoappTextRegular
					style={styles.itemsHeading}
					size={12}
					color={"#5c6170"}>
					{`Date`}
				</GoappTextRegular>
				<TouchableOpacity
					style={styles.dateTimeInput}
					onPress={() =>
						this.showDatePicker(
							"date",
							"date",
							new Date(this.props.seletedDateTime)
						)
					}>
					<GoappTextRegular>
						{i18n.strftime(new Date(this.props.seletedDateTime), "%d/%m/%Y")}
					</GoappTextRegular>
					{this._renderCaretIcon()}
				</TouchableOpacity>
			</View>
		);
	}

	_renderIncidentTime() {
		return (
			<View style={styles.container}>
				<GoappTextRegular
					style={styles.itemsHeading}
					size={12}
					color={"#5c6170"}>
					{`Time`}
				</GoappTextRegular>
				<TouchableOpacity
					style={styles.dateTimeInput}
					onPress={() => {
						const { seletedDateTime } = this.props;
						this.showDatePicker("time", "Time", new Date(seletedDateTime));
					}}>
					<GoappTextRegular>
						{_24to12(this.props.seletedDateTime)}
					</GoappTextRegular>
					{this._renderCaretIcon()}
				</TouchableOpacity>
			</View>
		);
	}

	_renderSelectLocation() {
		return (
			<>
				<View style={styles.container}>
					{this.props.isSitesLoading ? (
						<>
							<GoappTextRegular
								style={styles.itemsHeading}
								size={12}
								color={"#5c6170"}>
								{`Select Site`}
							</GoappTextRegular>
							<View style={styles.locationLoading}>
								<GoappTextRegular>{`Loading...`}</GoappTextRegular>
							</View>
						</>
					) : this.props.sites.length > 0 ? (
						this._renderSites(this.props.sites, "Site")
					) : this.props.isSitesError ? (
						<View>
							<GoappTextRegular
								style={styles.itemsHeading}
								size={12}
								color={"#5c6170"}>
								{`Select Site`}
							</GoappTextRegular>
							<View style={styles.locationLoading}>
								<GoappTextRegular>{`Error`}</GoappTextRegular>
							</View>
						</View>
					) : (
						this.props.sites.length === 0 && (
							<GoappTextRegular
								style={{ paddingHorizontal: width / 30 }}
								size={12}>
								{"No sites available"}
							</GoappTextRegular>
						)
					)}
				</View>
				<View style={styles.container}>
					{this.props.isBuildingsLoading ? (
						<>
							<GoappTextRegular
								style={styles.itemsHeading}
								size={12}
								color={"#5c6170"}>
								{`Select Building`}
							</GoappTextRegular>
							<View style={styles.locationLoading}>
								<GoappTextRegular>{`Loading...`}</GoappTextRegular>
							</View>
						</>
					) : this.props.buildings.length > 0 ? (
						this._renderSites(this.props.buildings, "Building")
					) : this.props.isBuildingsError ? (
						<View style={{ paddingHorizontal: width / 30 }}>
							<GoappTextRegular
								size={10}
								color={"#302c30"}
								style={styles.align}>
								{`Something went wrong!`}
							</GoappTextRegular>
							<TouchableOpacity
								style={[
									styles.retryButton,
									styles.nextColor,
									{ width: width / 3 }
								]}
								onPress={() => this.getBuildings(this.props.selectedSite.id)}>
								<GoappTextBold color={"#ffffff"}>{`Retry`}</GoappTextBold>
							</TouchableOpacity>
						</View>
					) : (
						this.props.selectedSite !== "Select" &&
						this.props.buildings.length === 0 && (
							<GoappTextRegular
								style={{ paddingHorizontal: width / 30 }}
								size={12}>
								{"No buildings available"}
							</GoappTextRegular>
						)
					)}
				</View>
				<View style={styles.container}>
					{this.props.buildings.length > 0 ? (
						this.props.isFloorLoading ? (
							<>
								<GoappTextRegular
									style={styles.itemsHeading}
									size={12}
									color={"#5c6170"}>
									{`Select Floor`}
								</GoappTextRegular>
								<View style={styles.locationLoading}>
									<GoappTextRegular>{`Loading...`}</GoappTextRegular>
								</View>
							</>
						) : this.props.floors.length > 0 ? (
							this._renderSites(this.props.floors, "Floor")
						) : this.props.isFloorsError ? (
							<View style={{ paddingHorizontal: width / 30 }}>
								<View style={styles.errorContainer}>
									<GoappTextRegular
										style={styles.itemsHeading}
										size={12}
										color={"#5c6170"}>
										{`Select Floor`}
									</GoappTextRegular>
									<GoappTextRegular
										size={10}
										color={"red"}
										style={styles.align}>
										{`Something went wrong!`}
									</GoappTextRegular>
								</View>
								<TouchableOpacity
									style={[
										styles.retryButton,
										styles.nextColor,
										{ width: width / 3 }
									]}
									onPress={() =>
										this.getFloors([this.props.selectedBuilding.id])
									}>
									<GoappTextBold color={"#ffffff"}>{`Retry`}</GoappTextBold>
								</TouchableOpacity>
							</View>
						) : (
							this.props.selectedBuilding !== "Select" &&
							this.props.floors.length === 0 && (
								<GoappTextRegular
									style={{ paddingHorizontal: width / 30 }}
									size={12}>
									{"No floors available"}
								</GoappTextRegular>
							)
						)
					) : null}
				</View>
			</>
		);
	}

	_renderCaretIcon() {
		return (
			<Icon
				iconType={"font_awesome"}
				iconName={"caret-down"}
				iconColor={"grey"}
				iconSize={18}
			/>
		);
	}

	_renderSites(data, type) {
		return (
			<>
				<GoappTextRegular
					style={styles.itemsHeading}
					size={12}
					color={"#5c6170"}>
					{`Select ${type}`}
				</GoappTextRegular>
				<View style={styles.picker}>
					<Dropdown
						data={data}
						onPress={item => this.handleLocationChange(item, type)}
					/>
				</View>
			</>
		);
	}

	_renderFooter() {
		return (
			<View style={styles.buttonContainer}>
				<TouchableOpacity
					style={[styles.button, styles.cancelButton]}
					onPress={() => this.goBack()}>
					<GoappTextBold color={"#2c98f0"}>{`Cancel`}</GoappTextBold>
				</TouchableOpacity>
				<TouchableOpacity
					disabled={this.props.selectedFloor !== "Select" ? false : true}
					style={[
						styles.button,
						this.props.selectedFloor === "Select"
							? styles.nextColor2
							: styles.nextColor
					]}
					onPress={this.navigationHandler}>
					<GoappTextBold color={"white"}>{`Next`}</GoappTextBold>
				</TouchableOpacity>
			</View>
		);
	}

	_renderGoDetTimePicker() {
		return (
			<GoDateTimePicker
				isVisible={this.state.isDatePickerVisible}
				mode={this.state.datePickerType}
				onConfirm={this.handleConfirm}
				onCancel={this.hideDatePicker}
				date={this.state.pickerDate}
				maximumDate={new Date()}
				minimumDate={minimumDate}
				headerTextIOS={
					this.state.datePickerType === "date" ? "Pick a date" : "Pick time"
				}
			/>
		);
	}
}

const mapStateToProps = state => {
	const regexAlphabet = /[^a-zA-Z]/g;
	const regexNumeric = /[^0-9]/g;
	const compareAlphaNumberic = ({ name: a }, { name: b }) => {
		const aAlpha = a.replace(regexAlphabet, "");
		const bAlpha = a.replace(regexAlphabet, "");
		if (aAlpha === bAlpha) {
			const aNumeric = parseInt(a.replace(regexNumeric, ""), 10);
			const bNumeric = parseInt(b.replace(regexNumeric, ""), 10);
			return aNumeric === bNumeric ? 0 : aNumeric > bNumeric ? 1 : -1;
		} else {
			return aAlpha > bAlpha ? 1 : -1;
		}
	};
	const compare = (a, b) => {
		var nameA = a.name.toUpperCase(); // ignore upper and lowercase
		var nameB = b.name.toUpperCase(); // ignore upper and lowercase
		if (nameA < nameB) {
			return -1;
		}
		if (nameA > nameB) {
			return 1;
		}
	};
	const sites =
		state.campusAssist && state.campusAssist.ticketVenue.sites.sort(compare);
	const buildings =
		state.campusAssist &&
		state.campusAssist.ticketVenue.buildings.sort(compareAlphaNumberic);
	const floors =
		state.campusAssist &&
		state.campusAssist.ticketVenue.floors.sort(compareAlphaNumberic);
	return {
		appToken: state.appToken.token,
		sites: sites,
		isSitesLoading:
			state.campusAssist && state.campusAssist.ticketVenue.isSitesLoading,
		isSitesError:
			state.campusAssist && state.campusAssist.ticketVenue.isSitesError,
		selectedSite:
			state.campusAssist && state.campusAssist.ticketVenue.selectedSite,
		buildings: buildings,
		isBuildingsLoading:
			state.campusAssist && state.campusAssist.ticketVenue.isBuildingsLoading,
		isBuildingsError:
			state.campusAssist && state.campusAssist.ticketVenue.isBuildingsError,
		selectedBuilding:
			state.campusAssist && state.campusAssist.ticketVenue.selectedBuilding,
		floors: floors,
		isFloorLoading:
			state.campusAssist && state.campusAssist.ticketVenue.isFloorLoading,
		isFloorsError:
			state.campusAssist && state.campusAssist.ticketVenue.isFloorsError,
		selectedFloor:
			state.campusAssist && state.campusAssist.ticketVenue.selectedFloor,
		seletedDateTime:
			state.campusAssist && state.campusAssist.ticketVenue.seletedDateTime,
		internetStatus: state.netInfo.status
	};
};

TicketIncidentVenue.contextType = DialogContext;
export default connect(mapStateToProps)(TicketIncidentVenue);
