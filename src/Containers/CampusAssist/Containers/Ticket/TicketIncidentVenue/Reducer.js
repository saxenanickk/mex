import {
	CAMPUS_ASSIST_GET_SITES,
	CAMPUS_ASSIST_GET_BUILDINGS,
	CAMPUS_ASSIST_GET_FLOOR
} from "./Saga";
import { REQUEST, SUCCESS, FAILURE } from "../../../../../utils/reduxUtils";
export const CAMPUS_ASSIST_SELECTED_SITE = "CAMPUS_ASSIST_SELECTED_SITE";
export const CAMPUS_ASSIST_SELECTED_BUILDINGS =
	"CAMPUS_ASSIST_SELECTED_BUILDINGS";
export const CAMPUS_ASSIST_SELECTED_FLOOR = "CAMPUS_ASSIST_SELECTED_FLOOR";
export const CAMPUS_ASSIST_SELECTED_DATE = "CAMPUS_ASSIST_SELECTED_DATE";
export const CAMPUS_ASSIST_RESET_FORM = "CAMPUS_ASSIST_RESET_FORM";
export const CAMPUS_ASSIST_RESET_BUILDINGS = "CAMPUS_ASSIST_RESET_BUILDINGS";
export const CAMPUS_ASSIST_RESET_FLOORS = "CAMPUS_ASSIST_RESET_FLOORS";

const initialState = {
	isSitesLoading: false,
	sites: [],
	isSitesError: false,
	selectedSite: "Select",
	isBuildingsLoading: false,
	buildings: [],
	isBuildingsError: false,
	selectedBuilding: "Select",
	isFloorLoading: false,
	floors: [],
	isFloorsError: false,
	selectedFloor: "Select",
	seletedDateTime: Date.now()
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case CAMPUS_ASSIST_GET_SITES[REQUEST]:
			return { ...state, isSitesLoading: true, isSitesError: false };
		case CAMPUS_ASSIST_GET_SITES[SUCCESS]:
			return {
				...state,
				isSitesLoading: false,
				sites: payload.response
			};
		case CAMPUS_ASSIST_GET_SITES[FAILURE]:
			return {
				...state,
				isSitesLoading: false,
				isSitesError: true
			};
		case CAMPUS_ASSIST_SELECTED_SITE:
			return {
				...state,
				selectedSite: payload
			};
		case CAMPUS_ASSIST_GET_BUILDINGS[REQUEST]:
			return { ...state, isBuildingsLoading: true, isBuildingsError: false };
		case CAMPUS_ASSIST_GET_BUILDINGS[SUCCESS]:
			return {
				...state,
				isBuildingsLoading: false,
				buildings: payload.response
			};
		case CAMPUS_ASSIST_GET_BUILDINGS[FAILURE]:
			return {
				...state,
				isBuildingsLoading: false,
				isBuildingsError: true
			};

		case CAMPUS_ASSIST_SELECTED_BUILDINGS:
			return {
				...state,
				selectedBuilding: payload
			};
		case CAMPUS_ASSIST_GET_FLOOR[REQUEST]:
			return { ...state, isFloorLoading: true, isFloorsError: false };
		case CAMPUS_ASSIST_GET_FLOOR[SUCCESS]:
			return {
				...state,
				isFloorLoading: false,
				floors: payload.response
			};
		case CAMPUS_ASSIST_GET_FLOOR[FAILURE]:
			return {
				...state,
				isFloorLoading: false,
				isFloorsError: true
			};

		case CAMPUS_ASSIST_SELECTED_FLOOR:
			return {
				...state,
				selectedFloor: payload
			};
		case CAMPUS_ASSIST_SELECTED_DATE:
			return {
				...state,
				seletedDateTime: payload
			};
		case CAMPUS_ASSIST_RESET_FORM:
			return {
				...state,
				isSitesLoading: false,
				sites: [],
				isSitesError: false,
				selectedSite: "Select",
				isBuildingsLoading: false,
				buildings: [],
				isBuildingsError: false,
				selectedBuilding: "Select",
				isFloorLoading: false,
				floors: [],
				isFloorsError: false,
				selectedFloor: "Select",
				seletedDateTime: Date.now()
			};
		case CAMPUS_ASSIST_RESET_BUILDINGS:
			return {
				...state,
				buildings: [],
				selectedBuilding: "Select",
				floors: [],
				selectedFloor: "Select"
			};
		case CAMPUS_ASSIST_RESET_FLOORS:
			return {
				...state,
				floors: [],
				selectedFloor: "Select"
			};
		default:
			return state;
	}
};
