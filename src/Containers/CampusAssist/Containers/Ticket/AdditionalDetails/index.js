import React, { Component } from "react";
import { connect } from "react-redux";
import {
	View,
	Dimensions,
	TouchableOpacity,
	ImageBackground,
	KeyboardAvoidingView,
	Platform,
	Keyboard
} from "react-native";
import { HeaderHeightContext } from "@react-navigation/stack";
import { styles } from "./style";
import { Icon, GoappCamera } from "../../../../../Components";
import CommunityLoader from "../../../../Community/Components/CommunityLoader";
import {
	GoappTextLight,
	GoappTextBold,
	GoappTextInputRegular
} from "../../../../../Components/GoappText";
import DialogContext from "../../../../../DialogContext";
import {
	VERIFIED_CORPORATE,
	ERRORCLOUD,
	WARNING
} from "../../../../../Assets/Img/Image";
import { WHITE, SKY_BLUE, RED } from "../../../Color";
import { campusAssistCreateTickets } from "./Saga";
import {
	THANK_YOU,
	ARE_YOU_SURE_YOU_WANT_TO_CANCEL,
	YOUR_ISSUE_HAS_BEEN_REPORTED_WE_WILL_CONTACT_YOU_SHORTLY
} from "../../../Constants";
import { CAMPUS_ASSIST_RESET_FORM } from "../TicketIncidentVenue/Reducer";
import { CAMPUS_ASSIST_RESET_CATEGORY } from "../Category/Reducer";
import { CAMPUS_ASSIST_REFRESH_TICKETS } from "../../Home/Reducer";

const options = {
	storageOptions: {
		skipBackup: true,
		path: "images",
		cameraRoll: true,
		waitUntilSaved: true
	}
};
const MAXIMUM_PHOTOS = 2;
const { height } = Dimensions.get("window");
class TicketAdditionalDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedImagesPath: [],
			description: ""
		};
		this.SelectedImages = [];
	}

	cameraRef = React.createRef();

	onDescriptionChange = description => {
		this.setState({
			description
		});
	};

	componentDidUpdate(prevProps) {
		if (
			this.props.createTicketResponse !== null &&
			this.props.createTicketResponse !== prevProps.createTicketResponse
		) {
			this.props.createTicketResponse.success === 1
				? this.onSuccessAlertPopUp()
				: this.onFailureAlertPopUp();
		}
		if (
			this.props.isCreateTicketError &&
			this.props.isCreateTicketError !== prevProps.isCreateTicketError
		) {
			this.onFailureAlertPopUp();
		}
	}

	openNoInternetAlert = (retryFunc, funcParams) => {
		this.context.current.openDialog({
			type: "DismissableAction",
			title: "No Internet Connection",
			message: "Turn on Internet and retry",
			rightTitle: "Retry",
			rightPress: () => {
				setTimeout(() => {
					retryFunc(funcParams);
				}, 500);
			},
			icon: ERRORCLOUD
		});
	};

	openImagePicker = async () => {
		if (this.cameraRef.current) {
			this.cameraRef.current.handleCameraPermission();
		}
	};

	validateFileName = image => {
		try {
			let response = image;
			let fileName = response.fileName;
			if (!fileName) {
				fileName = "mexims.jpg";
				response = {
					...response,
					fileName: fileName
				};
				return response;
			}
			if (
				Platform.OS === "ios" &&
				(fileName.endsWith(".heic") || fileName.endsWith(".HEIC"))
			) {
				fileName = `${fileName.split(".")[0]}.JPG`;
				response = {
					...response,
					fileName: fileName
				};
			}
			return response;
		} catch (error) {
			return image;
		}
	};

	validateTypeOfImage = image => {
		try {
			let response = image;
			if (image.type === null || image.type === undefined) {
				let type = image.fileName.split(".");
				type = type[type.length - 1].toLowerCase();
				response = {
					...response,
					// eslint-disable-next-line no-useless-escape
					type: `image\/${type}`
				};
			}
			return response;
		} catch (error) {
			return image;
		}
	};

	componentWillUnmount() {
		this.SelectedImages = [];
		this.setState({
			selectedImagesPath: [],
			description: ""
		});
	}

	removeImagehandler = fileName => {
		let temp = this.state.selectedImagesPath;
		let newSelectedImages = temp.filter(item => item.fileName !== fileName);
		this.SelectedImages = newSelectedImages;
		this.setState({ selectedImagesPath: newSelectedImages });
	};
	goBack = () => {
		this.context.current.openDialog({
			type: "Confirmation",
			message: ARE_YOU_SURE_YOU_WANT_TO_CANCEL,
			leftTitle: "No",
			rightTitle: "Yes",
			rightPress: () => this.resetFormDataOnCancel()
		});
	};
	resetFormDataOnCancel = () => {
		this.props.navigation.navigate("Home");
		this.props.dispatch({
			type: CAMPUS_ASSIST_RESET_FORM
		});
		this.props.dispatch({
			type: CAMPUS_ASSIST_RESET_CATEGORY
		});
	};

	onSubmit = () => {
		Keyboard.dismiss();
		const description = this.state.description.trim();
		if (description.length <= 200) {
			if (this.props.internetStatus) {
				this.props.dispatch(
					campusAssistCreateTickets.request(
						this.props.appToken,
						this.props.subCategoryId,
						this.props.siteId,
						this.props.floorId,
						this.state.description,
						this.props.incidentOccuredAt,
						this.state.selectedImagesPath
					)
				);
			} else {
				this.openNoInternetAlert(this.onSubmit);
			}
		} else {
			this.context.current.openDialog({
				type: "Information",
				title: "Error",
				message: "Description length is too long"
			});
		}
	};

	onFailureAlertPopUp = () => {
		this.context.current.openDialog({
			type: "Information",
			title: "Warning",
			message: "Something went wrong!",
			icon: WARNING
		});
	};

	onSuccessAlertPopUp = () => {
		this.context.current.openDialog({
			type: "Action",
			title: THANK_YOU,
			message: YOUR_ISSUE_HAS_BEEN_REPORTED_WE_WILL_CONTACT_YOU_SHORTLY,
			rightPress: () => this.props.navigation.navigate("Home"),
			icon: VERIFIED_CORPORATE
		});
		this.resetFormData();
	};

	resetFormData = () => {
		const { createTicketResponse } = this.props;
		let ticketsArray = this.props.tickets;
		ticketsArray.unshift(createTicketResponse.data);
		this.props.dispatch({
			type: CAMPUS_ASSIST_RESET_FORM
		});
		this.props.dispatch({
			type: CAMPUS_ASSIST_RESET_CATEGORY
		});
		this.props.dispatch({
			type: CAMPUS_ASSIST_REFRESH_TICKETS,
			payload: ticketsArray
		});
	};

	render() {
		const { selectedImagesPath } = this.state;
		return (
			<HeaderHeightContext>
				{headerHeight => (
					<KeyboardAvoidingView
						contentContainerStyle={styles.container}
						keyboardVerticalOffset={headerHeight + 64}
						style={styles.container}
						{...(Platform.OS === "ios" ? { behavior: "padding" } : {})}>
						<TouchableOpacity
							activeOpacity={1}
							onPress={Keyboard.dismiss}
							style={styles.flex}>
							<View>
								{this._renderHeading(
									"Description",
									`(${200 -
										this.state.description.length} characters remaining)`
								)}
								<View
									style={styles.descriptionBox(this.state.description.length)}>
									<GoappTextInputRegular
										style={styles.descriptionInput}
										autofocus={true}
										multiline={true}
										numberOfLines={4}
										placeholder="Please describe your issue"
										onChangeText={e => this.onDescriptionChange(e)}
										blurOnSubmit={false}
										textAlignVertical={"top"}
										keyboardType={"default"}
									/>
								</View>
								{this._renderHeading("Attachments", "(Upload max. 2 images)")}
								{this._renderImageButton(selectedImagesPath)}
								{this._renderSelectedImages(selectedImagesPath)}
							</View>
							{this._renderFooter()}
							{this.props.isCreateTicketLoading && (
								<View style={styles.loaderContainer}>
									<CommunityLoader />
									<GoappTextBold color="white">{`Creating ticket`}</GoappTextBold>
								</View>
							)}
						</TouchableOpacity>
						{this._renderGoCamera()}
					</KeyboardAvoidingView>
				)}
			</HeaderHeightContext>
		);
	}
	_renderHeading(title, subTitle) {
		return (
			<View style={styles.heading}>
				<GoappTextBold>{title}</GoappTextBold>
				<GoappTextLight
					color={
						title === "Description" && this.state.description.length > 200
							? "red"
							: "#B1B1B1"
					}>
					{`  ` + subTitle}
				</GoappTextLight>
			</View>
		);
	}

	_renderImageButton(selectedImagesPath) {
		return (
			<>
				{selectedImagesPath.length < MAXIMUM_PHOTOS && (
					<TouchableOpacity
						style={styles.imageButton}
						onPress={this.openImagePicker}>
						<GoappTextBold color={SKY_BLUE}>Image</GoappTextBold>
						<Icon
							iconType={"material_community_icon"}
							iconSize={height / 45}
							iconName={"image"}
							iconColor={SKY_BLUE}
						/>
					</TouchableOpacity>
				)}
			</>
		);
	}

	_renderSelectedImages(selectedImagesPath) {
		return (
			<View style={styles.selectedImagesContainer}>
				{selectedImagesPath.length > 0
					? selectedImagesPath.map((img, index) => (
							<ImageBackground
								source={{
									uri: img.uri
								}}
								key={index}
								style={[styles.imageToUpload, styles.imageShape]}
								imageStyle={styles.imageBorder}
								resizeMode={"cover"}>
								<View
									style={[
										styles.imageToUpload,
										styles.lessOpacityBackground,
										styles.imageBorder
									]}>
									<TouchableOpacity
										style={styles.imageRemoveIcon}
										onPress={() => {
											this.removeImagehandler(img.fileName);
										}}>
										<View style={styles.imageRemoveBackground} />
										<Icon
											iconType={"ionicon"}
											iconName={"ios-close-circle-outline"}
											iconSize={24}
											iconColor={RED}
										/>
									</TouchableOpacity>
								</View>
							</ImageBackground>
					  ))
					: null}
			</View>
		);
	}

	_renderFooter() {
		return (
			<View style={styles.buttonContainer}>
				<TouchableOpacity
					style={[styles.button, styles.cancelButton]}
					onPress={() => this.goBack()}>
					<GoappTextBold color={SKY_BLUE}>{`Cancel`}</GoappTextBold>
				</TouchableOpacity>
				<TouchableOpacity
					disabled={this.props.isCreateTicketLoading}
					style={[styles.button, styles.nextColor]}
					onPress={this.onSubmit}>
					<GoappTextBold color={WHITE}>{`Submit`}</GoappTextBold>
				</TouchableOpacity>
			</View>
		);
	}

	_renderGoCamera() {
		return (
			<GoappCamera
				ref={this.cameraRef}
				options={options}
				onImageSelect={response => {
					response = this.validateFileName(response);
					response = this.validateTypeOfImage(response);
					this.SelectedImages.push(response);
					this.setState({ selectedImagesPath: this.SelectedImages });
				}}
			/>
		);
	}
}
const mapStateToProps = state => ({
	appToken: state.appToken.token,
	subCategoryId:
		state.campusAssist && state.campusAssist.getSubCategory.selectedSubCategory,
	siteId: state.campusAssist && state.campusAssist.ticketVenue.selectedSite.id,
	floorId:
		state.campusAssist && state.campusAssist.ticketVenue.selectedFloor.id,
	incidentOccuredAt:
		state.campusAssist && state.campusAssist.ticketVenue.seletedDateTime,
	createTicketResponse:
		state.campusAssist && state.campusAssist.createTicket.createTicketResponse,
	tickets: state.campusAssist && state.campusAssist.getTickets.ticketsResponse,
	isCreateTicketLoading:
		state.campusAssist && state.campusAssist.createTicket.isCreateTicketLoading,
	isCreateTicketError:
		state.campusAssist && state.campusAssist.createTicket.isCreateTicketError,
	internetStatus: state.netInfo.status
});
TicketAdditionalDetails.contextType = DialogContext;

export default connect(mapStateToProps)(TicketAdditionalDetails);
