import { put, call, takeLatest } from "redux-saga/effects";
import CampusAssistApi from "../../../Api";
import {
	createRequestTypes,
	action,
	REQUEST,
	SUCCESS,
	FAILURE
} from "../../../../../utils/reduxUtils";

export const CAMPUS_ASSIST_CREATE_TICKET = createRequestTypes(
	"CAMPUS_ASSIST_CREATE_TICKET"
);

export const campusAssistCreateTickets = {
	request: (
		appToken,
		subCategoryId,
		siteId,
		floorId,
		description,
		incidentOccuredAt,
		selectedImagesPath
	) =>
		action(CAMPUS_ASSIST_CREATE_TICKET[REQUEST], {
			appToken,
			subCategoryId,
			siteId,
			floorId,
			description,
			incidentOccuredAt,
			selectedImagesPath
		}),
	success: response =>
		action(CAMPUS_ASSIST_CREATE_TICKET[SUCCESS], { response }),
	failure: error => action(CAMPUS_ASSIST_CREATE_TICKET[FAILURE], { error })
};

export function* campusAssistCreateTicketsSaga(dispatch) {
	yield takeLatest(CAMPUS_ASSIST_CREATE_TICKET[REQUEST], handleCreateTicket);
}

function* handleCreateTicket(params) {
	try {
		let createTicketResponse = yield call(
			CampusAssistApi.createTicket,
			params.payload
		);
		yield put(campusAssistCreateTickets.success(createTicketResponse));
	} catch (err) {
		yield put(campusAssistCreateTickets.failure(err));
	}
}
