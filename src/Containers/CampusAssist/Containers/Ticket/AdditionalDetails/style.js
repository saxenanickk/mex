import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#ffffff"
	},
	flex: {
		flex: 1,
		justifyContent: "space-between"
	},
	descriptionBox: descriptionLength => ({
		marginHorizontal: height / 30,
		borderRadius: height / 50,
		backgroundColor: "#F8F9FC",
		height: height / 6,
		marginTop: height / 50,
		paddingHorizontal: width / 30,
		borderWidth: 1,
		borderColor: descriptionLength > 200 ? "red" : "#F8F9FC"
	}),
	descriptionInput: {
		flex: 1,
		paddingHorizontal: 4,
		marginVertical: 4
	},
	imageButton: {
		marginHorizontal: height / 30,
		borderRadius: height / 10,
		backgroundColor: "#EAF4FC",
		flexDirection: "row",
		width: width / 4,
		paddingHorizontal: width / 40,
		paddingVertical: height / 100,
		justifyContent: "space-around",
		marginTop: height / 50,
		alignItems: "center"
	},
	heading: {
		marginHorizontal: height / 30,
		flexDirection: "row",
		marginTop: height / 50
	},
	imageToUpload: {
		width: 0.3 * width,
		height: 0.2 * width,
		alignItems: "flex-end",
		backgroundColor: "#000",
		borderRadius: width / 10
	},
	imageBorder: {
		borderRadius: 10
	},
	lessOpacityBackground: {
		backgroundColor: "rgba(0,0,0,0.2)"
	},
	imageRemoveIcon: {
		width: width / 10,
		height: height / 25,
		alignItems: "center",
		justifyContent: "center"
	},
	imageRemoveBackground: {
		position: "absolute",
		backgroundColor: "#fff",
		width: 18,
		height: 18,
		borderRadius: 9
	},
	imageShape: { marginTop: height / 90, marginRight: width / 20 },
	button: {
		borderRadius: height / 10,
		paddingVertical: height / 70,
		paddingHorizontal: width / 8
	},
	nextColor: {
		backgroundColor: "#2C98F0"
	},
	cancelButton: {
		borderColor: "#2C98F0",
		borderWidth: 1
	},
	buttonContainer: {
		alignSelf: "center",
		width: width / 1.1,
		flexDirection: "row",
		justifyContent: "space-around",
		marginVertical: height / 40,
		marginHorizontal: height / 30
	},
	selectedImagesContainer: {
		flexDirection: "row",
		marginHorizontal: height / 30
	},
	loaderContainer: {
		...StyleSheet.absoluteFill,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#00000099"
	}
});
