import { REQUEST, SUCCESS, FAILURE } from "../../../../../utils/reduxUtils";
import { CAMPUS_ASSIST_CREATE_TICKET } from "./Saga";

const initialState = {
	isCreateTicketLoading: false,
	createTicketResponse: null,
	isCreateTicketError: false
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case CAMPUS_ASSIST_CREATE_TICKET[REQUEST]:
			return {
				...state,
				isCreateTicketLoading: true,
				isCreateTicketError: false
			};
		case CAMPUS_ASSIST_CREATE_TICKET[SUCCESS]:
			return {
				...state,
				isCreateTicketLoading: false,
				createTicketResponse: payload.response
			};
		case CAMPUS_ASSIST_CREATE_TICKET[FAILURE]:
			return {
				...state,
				isCreateTicketLoading: false,
				isCreateTicketError: true
			};
		default:
			return state;
	}
};
