import { put, call, takeLatest } from "redux-saga/effects";
import CampusAssistApi from "../../../Api";
import {
	createRequestTypes,
	action,
	REQUEST,
	SUCCESS,
	FAILURE
} from "../../../../../utils/reduxUtils";

export const CAMPUS_ASSIST_GET_CATEGORY = createRequestTypes(
	"CAMPUS_ASSIST_GET_CATEGORY"
);

export const campusAssistGetCategories = {
	request: (appToken, site_id) =>
		action(CAMPUS_ASSIST_GET_CATEGORY[REQUEST], { appToken, site_id }),
	success: response =>
		action(CAMPUS_ASSIST_GET_CATEGORY[SUCCESS], { response }),
	failure: error => action(CAMPUS_ASSIST_GET_CATEGORY[FAILURE], { error })
};
export function* campusAssistGetCategoriesSaga(dispatch) {
	yield takeLatest(CAMPUS_ASSIST_GET_CATEGORY[REQUEST], handleGetCategories);
}

function* handleGetCategories(params) {
	try {
		let categories = yield call(CampusAssistApi.getCategories, params.payload);
		yield put(campusAssistGetCategories.success(categories));
	} catch (error) {
		yield put(campusAssistGetCategories.failure(error));
	}
}
