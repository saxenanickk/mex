import React, { Component } from "react";
import { TouchableOpacity, View, ScrollView } from "react-native";
import { connect } from "react-redux";
import {
	GoappTextRegular,
	GoappTextBold
} from "../../../../../Components/GoappText";
import CommunityLoader from "../../../../Community/Components/CommunityLoader";
import { styles } from "./style";
import DialogContext from "../../../../../DialogContext";
import { RED, WHITE, SKY_BLUE } from "../../../Color";
import { campusAssistGetCategories } from "./Saga";
import { CAMPUS_ASSIST_GET_SELECTED_CATEGORY } from "./Reducer";
import { get } from "lodash";
import {
	ARE_YOU_SURE_YOU_WANT_TO_CANCEL,
	PLEASE_SELECT_AN_ISSUE
} from "../../../Constants";
import { CAMPUS_ASSIST_RESET_CATEGORY } from "./Reducer";
import { WARNING, ERRORCLOUD } from "../../../../../Assets/Img/Image";

class TicketCategory extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isSelectedCategory: false
		};
	}

	componentDidMount() {
		if (this.props.categories.length === 0) {
			this.getCategories();
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (
			this.props.isCategoriesError &&
			this.props.isCategoriesError !== prevProps.isCategoriesError
		) {
			this.context.current.openDialog({
				type: "Action",
				title: "Oops!",
				message: "Failed to get Categories!",
				rightTitle: "Retry",
				rightPress: () => this.getCategories(),
				icon: WARNING
			});
		}
	}

	componentWillUnmount() {
		this.props.dispatch({
			type: CAMPUS_ASSIST_RESET_CATEGORY
		});
	}

	openNoInternetAlert = (retryFunc, funcParams) => {
		this.context.current.openDialog({
			type: "DismissableAction",
			title: "No Internet Connection",
			message: "Turn on Internet and retry",
			rightTitle: "Retry",
			rightPress: () => {
				setTimeout(() => {
					retryFunc(funcParams);
				}, 500);
			},
			icon: ERRORCLOUD
		});
	};

	getCategories = () => {
		if (this.props.internetStatus) {
			this.props.dispatch(
				campusAssistGetCategories.request(
					this.props.appToken,
					this.props.siteId
				)
			);
		} else {
			this.openNoInternetAlert(this.getCategories);
		}
	};

	selectedCategoryHandle = selectedCategory => {
		this.props.dispatch({
			type: CAMPUS_ASSIST_GET_SELECTED_CATEGORY,
			payload: selectedCategory
		});
	};

	navigationHandler = () => {
		if (this.props.selectedCategory !== "") {
			this.props.navigation.navigate("SubCategory");
		} else {
			this.setState({
				isSelectedCategory: true
			});
		}
	};

	goBackHandler = () => {
		this.context.current.openDialog({
			type: "Confirmation",
			title: ARE_YOU_SURE_YOU_WANT_TO_CANCEL,
			rightTitle: "Yes",
			rightPress: () => this.resetSelectedCategory(),
			leftTitle: "No"
		});
	};

	resetSelectedCategory = () => {
		this.props.navigation.navigate("Home");
		this.props.dispatch({
			type: CAMPUS_ASSIST_RESET_CATEGORY
		});
	};

	render() {
		const { categories, isCategoriesLoading, isCategoriesError } = this.props;
		return (
			<View style={styles.container}>
				{isCategoriesLoading ? (
					<View style={styles.loaderContainer}>
						<CommunityLoader />
					</View>
				) : isCategoriesError ? (
					<View style={styles.errorContainer}>
						<GoappTextBold>{`Unable to load categories`}</GoappTextBold>
						<TouchableOpacity
							onPress={this.getCategories}
							style={styles.retryButton}>
							<GoappTextRegular color={"#fff"}>{`Retry`}</GoappTextRegular>
						</TouchableOpacity>
					</View>
				) : (
					<>
						<ScrollView style={styles.itemContainer}>
							{categories.map(item => this._renderListItem(item))}
						</ScrollView>
						{this._renderEmptyValueAlert()}
						{this._renderFooter()}
					</>
				)}
			</View>
		);
	}

	_renderListItem(item) {
		return (
			<TouchableOpacity
				key={item.id}
				style={[
					styles.categoryContainer,
					item.id === this.props.selectedCategory && styles.selectedCategory
				]}
				onPress={() => {
					this.selectedCategoryHandle(item.id);
				}}>
				<GoappTextRegular
					style={[
						item.id === this.props.selectedCategory &&
							styles.selectedCategoryText
					]}>
					{item.name}
				</GoappTextRegular>
			</TouchableOpacity>
		);
	}

	_renderEmptyValueAlert() {
		return (
			<View style={styles.alertWarning}>
				{this.state.isSelectedCategory &&
				this.props.selectedCategory.length <= 0 ? (
					<GoappTextRegular color={RED} size={10}>
						{PLEASE_SELECT_AN_ISSUE}
					</GoappTextRegular>
				) : null}
			</View>
		);
	}

	_renderFooter() {
		return (
			<View style={styles.buttonContainer}>
				<TouchableOpacity
					style={[styles.footerButton, styles.cancelButton]}
					onPress={this.goBackHandler}>
					<GoappTextBold color={SKY_BLUE}>{`Cancel`}</GoappTextBold>
				</TouchableOpacity>

				<TouchableOpacity
					style={[styles.footerButton, styles.nextButton]}
					onPress={this.navigationHandler}>
					<GoappTextBold color={WHITE}>{`Next`}</GoappTextBold>
				</TouchableOpacity>
			</View>
		);
	}
}

const mapStateToProps = state => ({
	appToken: state.appToken.token,
	siteId: state.appToken.selectedSite.id,
	isCategoriesLoading: get(
		state,
		"campusAssist.getCategory.isCategoriesLoading",
		false
	),
	categories: get(state, "campusAssist.getCategory.categories", []),
	isCategoriesError:
		state.campusAssist && state.campusAssist.getCategory.isCategoriesError,
	selectedCategory: get(state, "campusAssist.getCategory.selectedCategory", ""),
	internetStatus: state.netInfo.status
});
TicketCategory.contextType = DialogContext;
export default connect(mapStateToProps)(TicketCategory);
