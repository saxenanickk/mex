import { CAMPUS_ASSIST_GET_CATEGORY } from "./Saga";
import { REQUEST, SUCCESS, FAILURE } from "../../../../../utils/reduxUtils";
export const CAMPUS_ASSIST_GET_SELECTED_CATEGORY =
	"CAMPUS_ASSIST_GET_SELECTED_CATEGORY";
const initialState = {
	isCategoriesLoading: false,
	categories: [],
	isCategoriesError: false,
	selectedCategory: ""
};
export const CAMPUS_ASSIST_RESET_CATEGORY = "CAMPUS_ASSIST_RESET_CATEGORY";

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case CAMPUS_ASSIST_GET_CATEGORY[REQUEST]:
			return {
				...state,
				isCategoriesLoading: true,
				isCategoriesError: false,
				selectedCategory: ""
			};
		case CAMPUS_ASSIST_GET_CATEGORY[SUCCESS]:
			return {
				...state,
				isCategoriesLoading: false,
				categories: payload.response,
				selectedCategory: ""
			};
		case CAMPUS_ASSIST_GET_CATEGORY[FAILURE]:
			return { ...state, isCategoriesLoading: false, isCategoriesError: true };
		case CAMPUS_ASSIST_GET_SELECTED_CATEGORY:
			return { ...state, selectedCategory: payload };
		case CAMPUS_ASSIST_RESET_CATEGORY:
			return {
				...state,
				selectedCategory: ""
			};
		default:
			return state;
	}
};
