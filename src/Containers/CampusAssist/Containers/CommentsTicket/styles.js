import { StyleSheet, Dimensions } from "react-native";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#ffffff"
	},
	loader: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	h2text: {
		fontSize: width / 23,
		color: "#8f96a3"
	},
	flatview: {
		paddingVertical: width / 50,
		borderRadius: width / 35,
		backgroundColor: "#ffffff",
		marginHorizontal: width / 25,
		marginVertical: width / 30,
		marginRight: width / 10,
		borderWidth: 0.5,
		borderColor: "#e7e7e7"
	},
	message: {
		marginHorizontal: width / 40
	},
	time: {
		color: "#a1a6b1",
		marginHorizontal: width / 40
	},
	userFlatview: {
		paddingVertical: width / 50,
		borderRadius: width / 35,
		backgroundColor: "#2c98f0",
		marginHorizontal: width / 25,
		marginVertical: width / 30
	},
	userMessage: {
		marginHorizontal: width / 40,
		color: "white"
	},
	userTime: {
		color: "white",
		marginHorizontal: width / 40
	},
	commentField: {
		width,
		borderTopWidth: width / 40,
		borderTopColor: "#eff1f4",
		paddingHorizontal: width / 25,
		paddingVertical: width / 50
	},
	commentTextInput: {
		width: width / 1.2,
		paddingVertical: width / 30,
		marginHorizontal: width / 30,
		flex: 1
	},
	rowView: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		display: "flex"
	},
	adminChat: {
		marginLeft: width / 30,
		marginRight: width / 10
	},
	userChat: {
		marginLeft: width / 5
	},
	postButton: {
		borderRadius: width / 50,
		backgroundColor: "#2c98f0",
		paddingVertical: width / 55,
		paddingHorizontal: width / 20
	},
	postButtonText: {
		color: "#ffffff"
	},
	headerTextStyle: {
		textAlign: "center",
		paddingVertical: 8,
		color: "#8F96A3"
	}
});
export default styles;
