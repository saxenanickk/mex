import React, { Component, createRef } from "react";
import {
	View,
	FlatList,
	Dimensions,
	TouchableOpacity,
	KeyboardAvoidingView,
	Platform
} from "react-native";
import styles from "./styles";
import {
	GoappTextRegular,
	GoappTextInputRegular
} from "../../../../Components/GoappText";
import { HeaderHeightContext } from "@react-navigation/stack";
import { campusAssistGetTicketDetails } from "../TicketDetails/Saga";
import { connect } from "react-redux";
import _ from "lodash";
import CommunityLoader from "../../../Community/Components/CommunityLoader";
import { campusAssistAddComment } from "./Saga";
import ChatItem from "../../Components/ChatItem";
import I18n from "../../Assests/Strings/i18n";
import { ifIphoneX } from "../../../../utils/iphonexHelper";

const { width } = Dimensions.get("window");
class CommentsTicket extends Component {
	flatlistRef = createRef();

	state = {
		commentText: ""
	};

	componentDidMount() {
		this.getTicketDetailsComment();
	}

	componentDidUpdate(prevProps, prevState) {
		if (
			prevProps.ticket.hasOwnProperty("comment") &&
			this.props.ticket.hasOwnProperty("comment") &&
			this.props.ticket.comment.length > prevProps.ticket.comment.length
		) {
			// eslint-disable-next-line react/no-did-update-set-state
			this.setState({ commentText: "" });
			this.flatlistRef.current.scrollToIndex({ animated: true, index: 0 });
		}
	}

	getTicketDetailsComment = () => {
		const ticketId = this.props.route.params.ticketId || null;
		this.props.dispatch(
			campusAssistGetTicketDetails.request(
				this.props.appToken,
				this.props.site_id,
				ticketId,
				true
			)
		);
	};

	componentWillUnmount() {
		if (!this.props.route.params.isTicketDetails || false) {
			this.props.dispatch(campusAssistGetTicketDetails.success({}));
		}
	}

	_commentOnChangeText = text => {
		this.setState((state, props) => {
			return { commentText: text };
		});
	};

	handlePost = () => {
		this.props.dispatch(
			campusAssistAddComment.request({
				appToken: this.props.appToken,
				body: {
					comment: this.state.commentText,
					assigneeId: this.props.ticket.assigneeId,
					currentState: this.props.ticket.currentState,
					priority: this.props.ticket.priority,
					site_id: this.props.site_id,
					teamId: this.props.ticket.teamId,
					ticketId: this.props.ticket.ticketId
				}
			})
		);
	};

	render() {
		return (
			<HeaderHeightContext.Consumer>
				{headerHeight => (
					<KeyboardAvoidingView
						style={styles.container}
						contentContainerStyle={styles.container}
						{...(Platform.OS === "ios" ? { behavior: "padding" } : {})}
						keyboardVerticalOffset={Platform.select({
							ios: headerHeight + ifIphoneX(24, 0),
							android: 0
						})}>
						{_.isEmpty(this.props.ticket) ? (
							<View style={styles.loader}>
								<CommunityLoader size={width / 20} />
							</View>
						) : (
							<FlatList
								ref={this.flatlistRef}
								data={this.props.ticket.comment}
								extraData={this.props.ticket.comment}
								showsVerticalScrollIndicator={false}
								renderItem={({ item }) => (
									<ChatItem
										details={item}
										onPress={() =>
											this.props.navigation.navigate("CommentsImage", {
												image: item.imageUrl
											})
										}
									/>
								)}
								ListFooterComponent={() => (
									<GoappTextRegular style={styles.headerTextStyle}>
										{I18n.strftime(
											new Date(this.props.ticket.comment[0].createdAt),
											"%b %d, %Y"
										)}
									</GoappTextRegular>
								)}
								inverted
								keyExtractor={item => item.id.toString()}
							/>
						)}
						{/* Comment Input Box */}
						{/* Move To Components */}
						<View style={styles.commentField}>
							<View style={styles.rowView}>
								<GoappTextInputRegular
									placeholder={"Add a comment"}
									style={styles.commentTextInput}
									value={this.state.commentText}
									onChangeText={this._commentOnChangeText}
								/>
								<TouchableOpacity
									style={styles.postButton}
									onPress={this.handlePost}
									disabled={
										this.props.isCommentAddLoading ||
										this.state.commentText.trim().length === 0
									}>
									{this.props.isCommentAddLoading ? (
										<CommunityLoader
											containerStyle={{
												width: width * 0.06,
												height: width * 0.06
											}}
										/>
									) : (
										<GoappTextRegular style={styles.postButtonText}>
											Post
										</GoappTextRegular>
									)}
								</TouchableOpacity>
							</View>
						</View>
					</KeyboardAvoidingView>
				)}
			</HeaderHeightContext.Consumer>
		);
	}
}

const mapStateToProps = state => ({
	appToken: state.appToken.token,
	site_id: state.appToken.selectedSite.id,
	ticket: state.campusAssist && state.campusAssist.ticketDetails.ticket,
	isCommentAddLoading:
		state.campusAssist && state.campusAssist.addComment.isCommentAddLoading
});
export default connect(mapStateToProps)(CommentsTicket);
