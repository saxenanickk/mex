import { takeLatest, put, call } from "redux-saga/effects";
import {
	createRequestTypes,
	action as createAction,
	REQUEST,
	SUCCESS,
	FAILURE
} from "../../../../utils/reduxUtils";
import CampusAssistApi from "../../Api";
import { campusAssistAppendCommentToChat } from "../TicketDetails/Saga";

/**
 * Constants
 */

export const CAMPUS_ASSIST_ADD_COMMENT = createRequestTypes(
	"CAMPUS_ASSIST_ADD_COMMENT"
);

/**
 * Action Creators
 */

export const campusAssistAddComment = {
	/**
	 * @param {import("../../Api").CreateCommentByTicketIdParams} payload
	 */
	request: payload => createAction(CAMPUS_ASSIST_ADD_COMMENT[REQUEST], payload),
	success: response =>
		createAction(CAMPUS_ASSIST_ADD_COMMENT[SUCCESS], { response }),
	failure: error => createAction(CAMPUS_ASSIST_ADD_COMMENT[FAILURE], { error })
};

export function* campusAssistAddCommentSaga(dispatch) {
	yield takeLatest(CAMPUS_ASSIST_ADD_COMMENT[REQUEST], handleAddComment);
}

/**
 *
 * @param {Object} action
 * @param {import("../../Api").CreateCommentByTicketIdParams} action.payload
 */
function* handleAddComment(action) {
	try {
		yield call(CampusAssistApi.createCommentByTicketId, action.payload);
		yield put(campusAssistAddComment.success(true));
		yield put(
			campusAssistAppendCommentToChat({
				id: Math.random(),
				name: "Me",
				comment: action.payload.body.comment,
				createdAt: Date.parse(new Date()),
				imageUrl: null
			})
		);
		// name: 'Me',
		// comment: 'From app',
		// createdAt: 1582094661197,
		// imageUrl: null,
		// id: 2294,
		// memberId: 173
	} catch (error) {
		yield put(campusAssistAddComment.failure(error));
	}
}
