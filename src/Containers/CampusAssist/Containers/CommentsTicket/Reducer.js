import { CAMPUS_ASSIST_ADD_COMMENT } from "./Saga";
import { REQUEST, SUCCESS, FAILURE } from "../../../../utils/reduxUtils";

const initialState = {
	isCommentAddLoading: false,
	isCommentAddSuccess: false,
	isCommentAddError: false
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case CAMPUS_ASSIST_ADD_COMMENT[REQUEST]:
			return {
				...state,
				isCommentAddLoading: true,
				isCommentAddSuccess: false,
				isCommentAddError: false
			};

		case CAMPUS_ASSIST_ADD_COMMENT[SUCCESS]:
			return {
				...state,
				isCommentAddLoading: false,
				isCommentAddSuccess: true
			};

		case CAMPUS_ASSIST_ADD_COMMENT[FAILURE]:
			return {
				...state,
				isCommentAddLoading: false,
				isCommentAddError: true
			};

		default:
			return state;
	}
};
