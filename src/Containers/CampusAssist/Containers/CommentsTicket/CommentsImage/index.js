import React, { Component } from "react";
import { View, Dimensions, TouchableOpacity, Platform } from "react-native";
import FastImage from "react-native-fast-image";

import ImageZoom from "react-native-image-pan-zoom";
import { styles } from "./style";
import { Icon } from "../../../../../Components";

const { width, height } = Dimensions.get("window");

class CommentsImage extends Component {
	constructor(props) {
		super(props);
		this.image = "";
	}

	_renderCommentImage = () => {
		let image = this.props.route.params.image || "";

		try {
			if (image.startsWith("http")) {
				if (
					image.startsWith("http://res.cloudinary.com/") ||
					image.startsWith("https://res.cloudinary.com/")
				) {
					let imageArray = image.split("/upload/");
					image = `${imageArray[0]}/upload/q_50/${imageArray[1]}`;
				}
				return (
					<ImageZoom
						cropWidth={width}
						cropHeight={height}
						imageWidth={width}
						imageHeight={height - height / 13}>
						<FastImage
							source={{ uri: image }}
							style={{ width: width, height: height - height / 13 }}
							resizeMode={Platform.OS === "ios" ? "contain" : "center"}
						/>
					</ImageZoom>
				);
			}
			return null;
		} catch (error) {
			return null;
		}
	};

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<TouchableOpacity
						style={styles.backButton}
						onPress={() => this.props.navigation.goBack()}>
						<Icon
							iconType={"material"}
							iconSize={width / 15}
							iconName={"close"}
							iconColor={"#fff"}
						/>
					</TouchableOpacity>
				</View>
				{this._renderCommentImage()}
			</View>
		);
	}
}

export default CommentsImage;
