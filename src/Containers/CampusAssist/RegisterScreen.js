import React from "react";
import { TouchableOpacity, StyleSheet, Text, Dimensions } from "react-native";
import Home from "./Containers/Home";
import TicketCategory from "./Containers/Ticket/Category";
import SubCategory from "./Containers/Ticket/SubCategory";
import TicketIncidentVenue from "./Containers/Ticket/TicketIncidentVenue";
import TicketAdditionalDetails from "./Containers/Ticket/AdditionalDetails";
import { Icon } from "../../Components";
import HeaderTitle from "../../Components/HeaderTitle";
import { GoappTextLight, GoappTextBold } from "../../Components/GoappText";
import TicketDetails from "./Containers/TicketDetails";
import CommentsTicket from "./Containers/CommentsTicket";
import ImageScreen from "./Containers/TicketDetails/ImageScreen";
import CommentsImage from "./Containers/CommentsTicket/CommentsImage";
import { Stack } from "../../utils/Navigators";

const { height } = Dimensions.get("window");

const Styles = StyleSheet.create({
	container: {
		alignItems: "center",
		padding: 6,
		marginLeft: 8,
		backgroundColor: "transparent"
	},
	categoryRightHeader: {
		paddingHorizontal: 20,
		fontSize: 16
	}
});

const CategoryRightHeader = props => (
	<Text style={Styles.categoryRightHeader}>{props.rightHeaderText}</Text>
);
const BackButton = ({ onPress }) => (
	<TouchableOpacity style={Styles.container} onPress={onPress}>
		<Icon iconType={"material"} iconName={"arrow-back"} iconSize={24} />
	</TouchableOpacity>
);

const ConnectedBackButton = ({ navigation }) => (
	<BackButton onPress={() => navigation.navigate("Goapp")} />
);

let data = null;

const Navigator = () => (
	<Stack.Navigator
		headerMode="float"
		screenOptions={({ navigation }) => ({
			headerLeft: () => <BackButton onPress={() => navigation.goBack()} />
		})}>
		<Stack.Screen
			options={({ navigation }) => ({
				title: "Campus Assist",
				headerLeft: () => <ConnectedBackButton navigation={navigation} />
			})}
			name={"Home"}
			component={props => <Home {...data} {...props} />}
		/>
		<Stack.Screen
			options={{
				title: "Select a Category",
				headerRight: () => <CategoryRightHeader rightHeaderText={"1/4"} />
			}}
			name={"TicketCategory"}
			component={TicketCategory}
		/>
		<Stack.Screen
			options={{
				title: "What happened?",
				headerRight: () => <CategoryRightHeader rightHeaderText={"2/4"} />
			}}
			name={"SubCategory"}
			component={SubCategory}
		/>
		<Stack.Screen
			options={{
				headerShown: false
			}}
			name={"CommentsImage"}
			component={CommentsImage}
		/>
		<Stack.Screen
			options={{
				title: "When & Where?",
				headerRight: () => <CategoryRightHeader rightHeaderText={"3/4"} />
			}}
			name={"TicketIncidentVenue"}
			component={TicketIncidentVenue}
		/>
		<Stack.Screen
			options={{
				headerTitle: (
					<HeaderTitle
						title={
							<GoappTextLight size={height / 70} color={"#959595"}>
								<GoappTextBold size={height / 50} color={"black"}>
									{"Additional Details"}
								</GoappTextBold>
								{" (Optional)"}
							</GoappTextLight>
						}
					/>
				),
				headerRight: () => <CategoryRightHeader rightHeaderText={"4/4"} />
			}}
			name={"TicketAdditionalDetails"}
			component={TicketAdditionalDetails}
		/>
		<Stack.Screen
			options={({ route }) => ({
				headerTitle: () => {
					const ticketId = route.params.ticketId || "";

					return (
						<HeaderTitle
							title={
								<GoappTextBold
									size={
										height / 50
									}>{`Details - Ticket #${ticketId}`}</GoappTextBold>
							}
						/>
					);
				}
			})}
			name={"TicketDetails"}
			component={TicketDetails}
		/>
		<Stack.Screen
			options={({ route }) => ({
				headerTitle: () => {
					const ticketId = route.params.ticketId || "";
					return (
						<HeaderTitle
							title={
								<GoappTextBold
									size={
										height / 50
									}>{`Comment - Ticket #${ticketId}`}</GoappTextBold>
							}
						/>
					);
				}
			})}
			name={"CommentsTicket"}
			component={CommentsTicket}
		/>
		<Stack.Screen
			options={{ headerShown: false }}
			name={"ImageScreen"}
			component={ImageScreen}
		/>
	</Stack.Navigator>
);

const RegisterScreen = props => {
	data = props;
	return (
		<Navigator
			onNavigationStateChange={(prevState, currState) =>
				props.onNavigationStateChange(prevState, currState)
			}
		/>
	);
};

export default RegisterScreen;
