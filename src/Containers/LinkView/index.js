import React, { Component } from "react";
import { View, Dimensions } from "react-native";
import { WebView } from "react-native-webview";
import { connect } from "react-redux";
import { ProgressBar } from "../../Components";
import styles from "./style";
import GoAppAnalytics from "../../utils/GoAppAnalytics";

const { width, height } = Dimensions.get("window");

class LinkView extends Component {
	constructor(props) {
		super(props);
		this.state = {
			barProgress: 100
		};
		const { url } = props.route.params;
		this.url = url || "http://www.mexit.in/";
		GoAppAnalytics.setPageView("link_view", this.url);
	}

	render() {
		return (
			<View style={styles.container}>
				{this.state.barProgress !== 100 ? (
					<ProgressBar
						width={width / 5}
						height={height / 200}
						value={this.state.barProgress}
						backgroundColor="#000"
						barEasing={"linear"}
					/>
				) : null}
				<WebView
					source={{
						uri: this.url
					}}
					onLoad={event => console.log("on load", event.nativeEvent)}
					onLoadProgress={event =>
						console.log("on load progress", event.nativeEvent)
					}
					ref={ref => (this.webViewRef = ref)}
				/>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {};
}

// eslint-disable-next-line prettier/prettier
export default connect(
	mapStateToProps,
	null,
	null
)(LinkView);
