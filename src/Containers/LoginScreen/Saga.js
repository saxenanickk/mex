import { takeLatest, put, call } from "redux-saga/effects";
import Api from "./Api";
import {
	saveAppToken,
	mexSaveUserProfile
} from "../../CustomModules/ApplicationToken/Saga";
import AsyncStorage from "@react-native-community/async-storage";
import MiniApp from "../../CustomModules/MiniApp";
import GoAppAnalytics from "../../utils/GoAppAnalytics";
import {
	clearData,
	USER_PROFILE,
	HOME_PAGE_CONTEXT,
	HOME_PAGE_BANNERS,
	HOME_PAGE_TRENDING_APPS,
	SERVICES,
	PARTNERS
} from "../../CustomModules/Offline";
/**
 * Constants
 */
export const MEX_LOGIN = "MEX_LOGIN";
export const MEX_SOCIAL_LOGIN = "MEX_SOCIAL_LOGIN";
export const MEX_SIGNUP = "MEX_SIGNUP";
export const MEX_LOGIN_SUCCESS = "MEX_LOGIN_SUCCESS";
export const MEX_LOGOUT = "MEX_LOGOUT";
export const MEX_FORGOT_PASSWORD = "MEX_FORGOT_PASSWORD";

/**
 * Action Creators
 */
export const mexLogin = payload => ({
	type: MEX_LOGIN,
	payload
});

export const mexSocialLogin = payload => ({
	type: MEX_SOCIAL_LOGIN,
	payload
});

export const mexSignUp = payload => ({
	type: MEX_SIGNUP,
	payload
});

export const mexLoginSuccess = payload => ({
	type: MEX_LOGIN_SUCCESS,
	payload
});

export const mexLogout = payload => ({
	type: MEX_LOGOUT,
	payload
});

export const mexForgotPassword = payload => ({
	type: MEX_FORGOT_PASSWORD,
	payload
});

/**
 * Root Saga
 */
export default function* loginSaga(dispatch) {
	yield takeLatest(MEX_LOGIN, handleMexLogin);
	yield takeLatest(MEX_SOCIAL_LOGIN, handleMexSocialLogin);
	yield takeLatest(MEX_SIGNUP, handleMexSignUp);
	yield takeLatest(MEX_LOGIN_SUCCESS, handleMexLoginSuccess);
	yield takeLatest(MEX_LOGOUT, handleMexLogout);
	yield takeLatest(MEX_FORGOT_PASSWORD, handleMexForgotPassword);
}

/**
 * Handlers
 */
function* handleMexLogin(action) {
	try {
		const data = yield call(Api.login, action.payload);
		console.log("Mex Login Saga Data ", data);

		if (data.success) {
			GoAppAnalytics.trackWithProperties("login", {
				type: "email",
				status: "success"
			});
			yield put(mexLoginSuccess(data.token));
		} else if (
			!data.success &&
			data.message ===
				"Activation Link has been sent to your email ID. Kindly Activate!"
		) {
			action.payload.activationError();
		} else if (!data.success && data.message === "Invalid credentials") {
			action.payload.invalidCredentials();
		} else if (
			!data.success &&
			data.message ===
				"There is no account associated with this email ID. Please signup for a new account"
		) {
			action.payload.pleaseRegister(data.message);
		} else {
			throw new Error();
		}
	} catch (error) {
		console.log("Mex Login Saga Error ", error);
		action.payload.genericErrorMessage(
			"Something went wrong. Try again later."
		);
	}
}

function* handleMexSocialLogin(action) {
	try {
		const data = yield call(Api.socialLogin, action.payload);
		console.log("Mex Social Login Saga Data ", data);
		if (data.success) {
			yield put(mexLoginSuccess(data.token));
		} else {
			throw new Error();
		}
	} catch (error) {
		console.log("Mex Social Login Saga Error ", error);
		action.payload.genericErrorMessage(
			"Something went wrong. Try again later."
		);
	}
}

function* handleMexSignUp(action) {
	try {
		const data = yield call(Api.signup, action.payload);
		console.log("Mex Signup Saga Data ", data);

		if (data.success && data.message) {
			switch (data.message) {
				case "User already exists. Please login":
					action.payload.userExists();
					break;
				case "Name is mandatory":
				case "Password is mandatory":
				case "Email is mandatory":
				case "Password must contain at least one letter, one number,one special character and be longer than six characters.":
					action.payload.genericErrorMessage(data.message);
					break;
				case "Activation Link has been sent to your email ID. Kindly Activate!":
					action.payload.signupSuccess();
					break;
				default:
					throw new Error();
			}
		} else {
			action.payload.genericErrorMessage(data.message);
		}
	} catch (error) {
		action.payload.genericErrorMessage(
			"Something went wrong. Try again later."
		);
	}
}

function* handleMexLoginSuccess(action) {
	AsyncStorage.setItem(
		"mex_app_token",
		JSON.stringify({ token: action.payload, user: null })
	);
	MiniApp.setSessionToken(action.payload);
	yield put(saveAppToken({ token: action.payload, user: null }));
}

function* handleMexLogout(action) {
	try {
		yield call(Api.logout, action.payload);
	} catch (error) {
		console.log(error);
	} finally {
		AsyncStorage.setItem("mex_app_token", "");
		clearData(USER_PROFILE);
		clearData(HOME_PAGE_CONTEXT);
		clearData(HOME_PAGE_BANNERS);
		clearData(HOME_PAGE_TRENDING_APPS);
		clearData(SERVICES);
		clearData(PARTNERS);
		yield put(saveAppToken({ token: null, user: null }));
		yield put(mexSaveUserProfile({ user: null, userProfile: null }));
	}
}

function* handleMexForgotPassword(action) {
	try {
		const data = yield call(Api.forgotPassword, action.payload);
		console.log("Mex Forgot Password Saga Data ", data);
		if (data.success) {
			action.payload.emailSent("Password reset link is sent to your email");
		} else {
			action.payload.genericErrorMessage(data.message);
		}
	} catch (error) {
		console.log("Mex Social Login Saga Error ", error);
		action.payload.genericErrorMessage(
			"Something went wrong. Try again later."
		);
	}
}
