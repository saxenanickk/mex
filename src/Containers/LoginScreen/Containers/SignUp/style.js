import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	backgroundImage: {
		flex: 0.5,
		height: undefined,
		width: undefined
	},
	loginTextHeader: {
		color: "#FFFFFF",
		fontSize: height / 30,
		marginLeft: width / 40
	},
	notAMember: {
		alignSelf: "center"
	},
	textInputTitle: { paddingLeft: width / 25 },
	inputBox: {
		height: height / 15,
		marginTop: height / 50,
		borderRadius: height / 20,
		backgroundColor: "#F8F9FC",
		paddingHorizontal: width / 25,
		paddingVertical: height / 50,
		position: "relative",
		// alignSelf: "center",
		flex: 1
	},
	headingContainer: {
		flexDirection: "row",
		alignItems: "center",
		marginTop: height / 13,
		marginLeft: width / 25
	},
	loginButton: {
		backgroundColor: "#2C98F0",
		paddingHorizontal: width / 40,
		paddingVertical: height / 50,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 10
	},
	loginText: {
		color: "#FFFFFF",
		alignSelf: "center",
		fontSize: height / 54
	},
	emailContainer: {},
	passwordContainer: {},
	passwordInput: {
		flexDirection: "row",
		alignItems: "center"
	},
	eyeIcon: {
		flex: 0.3,
		backgroundColor: "#F8F9FC",
		justifyContent: "center",
		alignItems: "center",
		marginTop: height / 50,
		height: height / 15,
		borderTopRightRadius: height / 20,
		borderBottomRightRadius: height / 20
	},
	contentContainer: {
		flex: 1,
		backgroundColor: "#ffffff",
		justifyContent: "flex-start",
		// marginTop: width / 20,
		paddingHorizontal: width / 20
	}
});
