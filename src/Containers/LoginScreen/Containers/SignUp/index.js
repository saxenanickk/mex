import React, { Component } from "react";
import {
	View,
	ScrollView,
	StatusBar,
	TouchableOpacity,
	Keyboard
} from "react-native";
import { connect } from "react-redux";
import { styles } from "./style";
import Icon from "../../../../Components/Icon";
import { Dimensions } from "react-native";
import {
	GoappTextInputRegular,
	GoappTextRegular
} from "../../../../Components/GoappText";
import { mexSignUp } from "../../Saga";
import DialogContext from "../../../../DialogContext";
import { ProgressScreen } from "../../../../Components";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import { ERRORCLOUD } from "../../../../Assets/Img/Image";

const { width, height } = Dimensions.get("window");

class SignUp extends Component {
	constructor() {
		super();
		this.state = {
			name: "",
			email: "",
			password: "",
			errorField: "",
			isLoading: false,
			showPassword: false,
			uri: "",
			headerText: ""
		};
	}

	handleFullNameInput = name => this.setState({ name: name, errorField: "" });
	handleEmailInput = email =>
		this.setState({ email: email.trim(), errorField: "" });
	handlePasswordInput = password =>
		this.setState({ password: password.trim(), errorField: "" });

	setAnalytics = (type, status) => {
		GoAppAnalytics.trackWithProperties("signup", {
			type,
			status
		});
	};

	signUp = () => {
		const { name, email, password } = this.state;

		if (!name) {
			this.setState({ errorField: "name" });
		} else if (!email) {
			this.setState({ errorField: "email" });
		} else if (!password) {
			this.setState({ errorField: "password" });
		} else {
			Keyboard.dismiss();
			this.setAnalytics("email", "initiate");
			this.setState({ isLoading: true }, () => {
				this.props.dispatch(
					mexSignUp({
						name,
						email,
						password,
						navigation: this.props.navigation,
						genericErrorMessage: message => {
							this.setAnalytics("email", "fail");
							this.setState({ isLoading: false }, () =>
								this.context.current.openDialog({
									type: "Information",
									title: "Error",
									message: message,
									icon: ERRORCLOUD
								})
							);
						},
						userExists: () => {
							this.setAnalytics("email", "fail");
							this.setState({ isLoading: false }, () =>
								this.context.current.openDialog({
									type: "Confirmation",
									title: "Information",
									message: "User already exists. Please login",
									rightPress: () => {
										this.props.navigation.navigate("EmailLogin");
									}
								})
							);
						},
						signupSuccess: () => {
							this.setAnalytics("email", "success");
							this.props.navigation.replace("Verify", {
								name: this.state.name,
								email: this.state.email
							});
							this.setState({ isLoading: false });
						}
					})
				);
			});
		}
	};

	togglePassword = () =>
		this.setState(({ showPassword }) => ({ showPassword: !showPassword }));

	render() {
		const { isLoading, showPassword } = this.state;

		return (
			<ScrollView
				contentContainerStyle={{ flex: 1 }}
				keyboardShouldPersistTaps="never">
				<View style={{ flex: 1, backgroundColor: "#eff1f4" }}>
					<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
					<View style={styles.contentContainer}>
						<View
							style={{
								height: height * 0.7,
								backgroundColor: "#ffffff",
								justifyContent: "space-around"
							}}>
							<View style={styles.emailContainer}>
								<GoappTextRegular
									color={
										this.state.errorField === "name" ? "#ff0000" : "#5C6170"
									}
									style={styles.textInputTitle}
									size={height / 60}>
									{"Full Name"}
								</GoappTextRegular>
								<View style={styles.passwordInput}>
									<GoappTextInputRegular
										onSubmitEditing={event => {
											this.emailField.focus();
										}}
										value={this.state.name}
										onChangeText={this.handleFullNameInput}
										color={
											this.state.errorField === "name" ? "#ff0000" : "#000000"
										}
										size={height / 60}
										style={styles.inputBox}
										placeholder="Enter Full Name"
										placeholderTextColor={
											this.state.errorField === "name" ? "#ff0000" : null
										}
										autoFocus={true}
										keyboardType={"default"}
										textContentType={"name"}
										returnKeyType={"next"} //	enum ["done", "go", "next"]
									/>
								</View>
							</View>
							<View style={styles.emailContainer}>
								<GoappTextRegular
									color={
										this.state.errorField === "email" ? "#ff0000" : "#5C6170"
									}
									style={styles.textInputTitle}
									size={height / 60}>
									{"Email"}
								</GoappTextRegular>
								<View style={styles.passwordInput}>
									<GoappTextInputRegular
										inputRef={ref => {
											this.emailField = ref;
										}}
										onSubmitEditing={event => {
											this.passwordField.focus();
										}}
										value={this.state.email}
										onChangeText={this.handleEmailInput}
										color={
											this.state.errorField === "email" ? "#ff0000" : "#000000"
										}
										size={height / 60}
										style={styles.inputBox}
										placeholder="Enter Email"
										placeholderTextColor={
											this.state.errorField === "email" ? "#ff0000" : null
										}
										keyboardType={"email-address"}
										textContentType={"emailAddress"}
										returnKeyType={"next"}
									/>
								</View>
							</View>
							<View style={styles.passwordContainer}>
								<GoappTextRegular
									color={
										this.state.errorField === "password" ? "#ff0000" : "#5C6170"
									}
									style={styles.textInputTitle}
									size={height / 54}>
									{"Password"}
								</GoappTextRegular>
								<View style={styles.passwordInput}>
									<GoappTextInputRegular
										inputRef={ref => {
											this.passwordField = ref;
										}}
										value={this.state.password}
										onChangeText={this.handlePasswordInput}
										color={
											this.state.errorField === "password"
												? "#ff0000"
												: "#000000"
										}
										size={height / 54}
										style={[
											styles.inputBox,
											{
												borderTopLeftRadius: height / 20,
												borderBottomLeftRadius: height / 20,
												borderTopRightRadius: 0,
												borderBottomRightRadius: 0
											}
										]}
										secureTextEntry={!showPassword}
										placeholder="Enter Password"
										placeholderTextColor={
											this.state.errorField === "password" ? "#ff0000" : null
										}
										keyboardType={"default"}
										textContentType={"password"}
										returnKeyType={"done"}
									/>
									<View style={styles.eyeIcon}>
										<TouchableOpacity onPress={this.togglePassword}>
											<Icon
												iconType={"material_community_icon"}
												iconName={"eye"}
												iconSize={height / 30}
												iconColor={showPassword ? "#ff0000" : "#2C98F0"}
											/>
										</TouchableOpacity>
									</View>
								</View>
								<GoappTextRegular
									size={height / 70}
									color={"grey"}
									style={{
										marginTop: height / 100,
										paddingHorizontal: width / 25
									}}>
									{
										"Password must be 8 characters long and must contain at least one alphabet, one number and one symbol."
									}
								</GoappTextRegular>
							</View>
							<GoappTextRegular
								size={height / 60}
								color={"#000000"}
								style={{
									paddingHorizontal: width / 25
								}}>
								{"By signing up you agree to our "}
								<GoappTextRegular
									onPress={() =>
										this.props.navigation.navigate("LinkView", {
											url: "https://www.mexit.in/service-terms",
											title: "Terms & Conditions"
										})
									}
									color={"#2c98f0"}>
									{"Terms & Conditions"}
								</GoappTextRegular>
								{" and "}
								<GoappTextRegular
									onPress={() =>
										this.props.navigation.navigate("LinkView", {
											url: "https://www.mexit.in/privacy-policy",
											title: "Privacy Policy"
										})
									}
									color={"#2c98f0"}>
									{"Privacy Policy"}
								</GoappTextRegular>
								{"."}
							</GoappTextRegular>
							<TouchableOpacity onPress={this.signUp}>
								<View style={styles.loginButton}>
									<GoappTextRegular color={"#FFFFFF"} size={height / 54}>
										{"Sign Up"}
									</GoappTextRegular>
								</View>
							</TouchableOpacity>
							<TouchableOpacity
								style={styles.notAMember}
								onPress={() => {
									this.props.navigation.navigate("EmailLogin");
								}}>
								<GoappTextRegular size={height / 60} color={"#5C6170"}>
									{"Already a user? "}
									<GoappTextRegular color={"#2C98F0"} size={height / 60}>
										{"Login"}
									</GoappTextRegular>
								</GoappTextRegular>
							</TouchableOpacity>
						</View>
					</View>
					{isLoading ? (
						<ProgressScreen
							isMessage={true}
							primaryMessage={"Hang On..."}
							message={"Signing In"}
							indicatorColor={"#2C98F0"}
							indicatorSize={height / 50}
						/>
					) : null}
				</View>
			</ScrollView>
		);
	}
}

const mapStateToProps = state => {
	return {};
};

SignUp.contextType = DialogContext;
export default connect(mapStateToProps)(SignUp);
