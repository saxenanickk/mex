import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#ffffff"
	},
	header: {
		flexDirection: "row",
		padding: height / 40
	},
	heading: {
		marginLeft: width / 28
	},
	emailInput: {
		flexDirection: "row",
		alignItems: "center"
	},
	emailContainer: {
		// marginTop: height / 30,
	},
	inputHeaderText: {
		paddingLeft: width / 25
	},
	inputBox: {
		height: height / 15,
		marginTop: height / 50,
		borderRadius: height / 20,
		backgroundColor: "#F8F9FC",
		paddingHorizontal: width / 25,
		paddingVertical: height / 50,
		position: "relative",
		// alignSelf: "center",
		flex: 1
	},
	loginButton: {
		backgroundColor: "#2C98F0",
		paddingHorizontal: width / 40,
		paddingVertical: height / 50,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 10
	},
	loginText: {
		alignSelf: "center"
	},
	hr: {
		height: height / 50,
		backgroundColor: "#eff2f7"
	}
});
