import React, { Component } from "react";
import { View, Dimensions, TouchableOpacity, StatusBar } from "react-native";
import { connect } from "react-redux";
import { styles } from "./style";
import {
	GoappTextRegular,
	GoappTextInputRegular
} from "../../../../Components/GoappText";
import DialogContext from "../../../../DialogContext";
import { mexForgotPassword } from "../../Saga";
import { ProgressScreen } from "../../../../Components";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import { ERRORCLOUD } from "../../../../Assets/Img/Image";

const { width, height } = Dimensions.get("window");

class ForgotPassword extends Component {
	constructor() {
		super();
		this.state = { errorField: false, email: "", isLoading: false };
	}

	setAnalytics = (type, status) => {
		GoAppAnalytics.trackWithProperties("reset_password", {
			status
		});
	};

	handleReset = () => {
		const { email } = this.state;
		if (!email) {
			this.setState({ errorField: true });
		} else {
			this.setState({ isLoading: true }, () => {
				this.setAnalytics("initiate");
				this.props.dispatch(
					mexForgotPassword({
						email,
						genericErrorMessage: message => {
							this.setAnalytics("fail");
							this.setState({ isLoading: false }, () =>
								this.context.current.openDialog({
									type: "Information",
									title: "Error!",
									message: message,
									icon: ERRORCLOUD
								})
							);
						},
						emailSent: message => {
							this.setState({ isLoading: false });
							this.setAnalytics("link_sent");
							this.context.current.openDialog({
								type: "Action",
								title: "Info Alert!",
								message: message,
								rightPress: () => {
									this.props.navigation.navigate("EmailLogin");
								}
							});
						}
					})
				);
			});
		}
	};

	handleEmailInput = email => this.setState({ email, errorField: "" });

	render() {
		const { isLoading } = this.state;

		return (
			<View style={styles.container}>
				<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
				<View
					style={{
						height: height * 0.3,
						paddingHorizontal: width / 20,
						justifyContent: "space-around"
					}}>
					<View style={styles.emailContainer}>
						<GoappTextRegular
							color={this.state.errorField ? "#ff0000" : "#5C6170"}
							size={height / 60}
							style={styles.inputHeaderText}>
							{"Email Address"}
						</GoappTextRegular>
						<View style={styles.emailInput}>
							<GoappTextInputRegular
								onSubmitEditing={event => {
									this.handleReset();
								}}
								value={this.state.email}
								onChangeText={this.handleEmailInput}
								color={this.state.errorField ? "#ff0000" : "#000000"}
								placeholderTextColor={this.state.errorField ? "#ff0000" : null}
								size={height / 54}
								style={styles.inputBox}
								placeholder="abc@xyz.com"
								autoFocus={true}
								keyboardType={"email-address"}
								textContentType={"emailAddress"}
								returnKeyType={"done"}
							/>
						</View>
					</View>
					<TouchableOpacity onPress={this.handleReset}>
						<View style={styles.loginButton}>
							<GoappTextRegular color={"#FFFFFF"} size={height / 54}>
								{"Reset my password"}
							</GoappTextRegular>
						</View>
					</TouchableOpacity>
				</View>
				{isLoading ? (
					<ProgressScreen
						isMessage={true}
						primaryMessage={"Hang On..."}
						message={"Processing your request"}
						indicatorColor={"#2C98F0"}
						indicatorSize={height / 50}
					/>
				) : null}
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {};
};

ForgotPassword.contextType = DialogContext;
export default connect(mapStateToProps)(ForgotPassword);
