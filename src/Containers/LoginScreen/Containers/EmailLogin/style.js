import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	backgroundImage: {
		flex: 0.5,
		height: undefined,
		width: undefined
	},
	textInputTitle: { paddingLeft: width / 25 },
	inputBox: {
		height: height / 15,
		marginTop: height / 50,
		borderRadius: height / 20,
		backgroundColor: "#F8F9FC",
		paddingHorizontal: width / 25,
		paddingVertical: height / 50,
		position: "relative",
		// alignSelf: "center",
		flex: 1
	},
	notAMember: {
		alignSelf: "center",
		marginTop: width / 10
	},
	headingContainer: {
		flexDirection: "row",
		alignItems: "center",
		marginTop: height / 13,
		marginLeft: width / 25
	},
	loginButton: {
		backgroundColor: "#2C98F0",
		paddingHorizontal: width / 40,
		paddingVertical: height / 50,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 10
	},
	emailContainer: {
		marginVertical: 10
	},
	passwordContainer: {
		marginTop: 15
	},
	forgotPasswordText: {
		alignSelf: "center"
	},
	forgotPasswordTouchableOpacity: { marginTop: width / 15 },
	emailInput: {
		flexDirection: "row",
		alignItems: "center"
	},
	passwordInput: {
		flexDirection: "row",
		alignItems: "center"
	},
	eyeIcon: {
		flex: 0.3,
		backgroundColor: "#F8F9FC",
		justifyContent: "center",
		alignItems: "center",
		marginTop: height / 50,
		height: height / 15,
		borderTopRightRadius: height / 20,
		borderBottomRightRadius: height / 20
	},
	loginView: { marginTop: width / 5 }
});
