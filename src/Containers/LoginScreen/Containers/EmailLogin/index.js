import React, { Component } from "react";
import {
	View,
	TouchableOpacity,
	Dimensions,
	StatusBar,
	Keyboard
} from "react-native";
import { connect } from "react-redux";
import { styles } from "./style";
import Icon from "../../../../Components/Icon";
import {
	GoappTextBold,
	GoappTextRegular,
	GoappTextInputRegular
} from "../../../../Components/GoappText";

import { mexLogin } from "../../Saga";
import DialogContext from "../../../../DialogContext";
import { ProgressScreen } from "../../../../Components";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import { DeviceId } from "../../../../CustomModules/GoDeviceInfo";
import { ERRORCLOUD } from "../../../../Assets/Img/Image";
import { CommonActions } from "@react-navigation/native";

const { width, height } = Dimensions.get("window");
export class EmailLogin extends Component {
	constructor() {
		super();
		this.state = {
			email: "",
			password: "",
			isLoading: false,
			errorField: "",
			showPassword: false
		};
		this.navigateTo = this.navigateTo.bind(this);
	}

	setAnalytics = (type, status) => {
		GoAppAnalytics.trackWithProperties("login", {
			type,
			status
		});
	};
	componentDidUpdate(prevProps, prevState) {
		if (this.props.appToken && prevProps.appToken !== this.props.appToken) {
			this.navigateTo();
		}
	}

	navigateTo = async () => {
		this.props.navigation.dispatch(
			CommonActions.reset({
				index: 0,
				routes: [{ name: "Goapp" }]
			})
		);
	};

	handleEmailInput = email =>
		this.setState({ email: email.trim(), errorField: "" });

	handlePasswordInput = password =>
		this.setState({ password: password.trim(), errorField: "" });

	login = async () => {
		this.setAnalytics("email", "initiate");
		const { email, password } = this.state;
		if (!email) {
			this.setState({ errorField: "email" });
		} else if (!password) {
			this.setState({ errorField: "password" });
		} else {
			Keyboard.dismiss();
			let deviceId = await DeviceId();
			this.setState({ isLoading: true }, () =>
				this.props.dispatch(
					mexLogin({
						email,
						password,
						deviceId,
						genericErrorMessage: message => {
							this.setAnalytics("email", "fail");
							this.setState({ isLoading: false }, () =>
								this.context.current.openDialog({
									type: "Information",
									title: "Error",
									message: message,
									icon: ERRORCLOUD
								})
							);
						},
						pleaseRegister: message => {
							this.setState({ isLoading: false });
							this.context.current.openDialog({
								type: "Confirmation",
								title: "Info!",
								message: message,
								rightPress: () => {
									this.props.navigation.navigate("SignUp");
								}
							});
						},
						invalidCredentials: () => {
							this.setAnalytics("email", "invalid_credential");
							this.setState({ isLoading: false });
							this.context.current.openDialog({
								type: "Information",
								title: "Error!",
								message: "Invalid Credentials",
								icon: ERRORCLOUD
							});
						},
						activationError: () => {
							this.setAnalytics("email", "INACTIVE_ACCOUNT");
							this.setState({ isLoading: false });
							this.context.current.openDialog({
								type: "Information",
								title: "Info!",
								message:
									"Activation Link has been sent to your email ID. Kindly Activate!"
							});
						}
					})
				)
			);
		}
	};

	togglePassword = () =>
		this.setState(({ showPassword }) => ({ showPassword: !showPassword }));

	render() {
		const { isLoading, showPassword } = this.state;

		return (
			<View style={{ flex: 1, backgroundColor: "#ffffff" }}>
				<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
				<View
					style={{
						height: height / 2,
						backgroundColor: "#ffffff",
						marginTop: width / 20,
						paddingHorizontal: width / 20,
						justifyContent: "flex-start"
					}}>
					<View
						style={{
							justifyContent: "space-around"
						}}>
						<View style={styles.emailContainer}>
							<GoappTextRegular
								color={
									this.state.errorField === "email" ? "#ff0000" : "#5C6170"
								}
								size={height / 60}
								style={styles.textInputTitle}>
								{"Email Address"}
							</GoappTextRegular>
							<View style={styles.emailInput}>
								<GoappTextInputRegular
									onSubmitEditing={event => {
										this.passwordField.focus();
									}}
									value={this.state.email}
									onChangeText={this.handleEmailInput}
									color={
										this.state.errorField === "email" ? "#ff0000" : "#000000"
									}
									placeholderTextColor={
										this.state.errorField === "email" ? "#ff0000" : null
									}
									size={height / 54}
									style={styles.inputBox}
									placeholder="abc@xyz.com"
									autoFocus={true}
									keyboardType={"email-address"}
									textContentType={"emailAddress"}
									returnKeyType={"next"} //	enum ["done", "go", "next"]
								/>
							</View>
						</View>
						<View style={styles.passwordContainer}>
							<GoappTextRegular
								color={
									this.state.errorField === "password" ? "#ff0000" : "#5C6170"
								}
								size={height / 60}
								style={styles.textInputTitle}>
								{"Password"}
							</GoappTextRegular>
							<View style={styles.passwordInput}>
								<GoappTextInputRegular
									inputRef={ref => {
										this.passwordField = ref;
									}}
									value={this.state.password}
									onChangeText={this.handlePasswordInput}
									color={
										this.state.errorField === "password" ? "#ff0000" : "#000000"
									}
									placeholderTextColor={
										this.state.errorField === "password" ? "#ff0000" : null
									}
									size={height / 54}
									style={[
										styles.inputBox,
										{
											borderTopLeftRadius: height / 20,
											borderBottomLeftRadius: height / 20,
											borderTopRightRadius: 0,
											borderBottomRightRadius: 0
										}
									]}
									secureTextEntry={!showPassword}
									placeholder="********"
									keyboardType={"default"}
									textContentType={"password"}
									returnKeyType={"done"}
								/>
								<View style={styles.eyeIcon}>
									<TouchableOpacity onPress={this.togglePassword}>
										<Icon
											iconType={"material_community_icon"}
											iconName={"eye"}
											iconSize={height / 30}
											iconColor={showPassword ? "#ff0000" : "#2C98F0"}
										/>
									</TouchableOpacity>
								</View>
							</View>
						</View>
						<View style={styles.loginView}>
							<TouchableOpacity onPress={() => this.login()}>
								<View style={styles.loginButton}>
									<GoappTextRegular color={"#FFFFFF"} size={height / 54}>
										{"Login"}
									</GoappTextRegular>
								</View>
							</TouchableOpacity>

							<TouchableOpacity
								style={styles.forgotPasswordTouchableOpacity}
								onPress={() => {
									this.props.navigation.navigate("ForgotPassword");
								}}>
								<GoappTextBold
									size={height / 54}
									color={"#2C98F0"}
									style={styles.forgotPasswordText}>
									{"Forgot Password?"}
								</GoappTextBold>
							</TouchableOpacity>
							<TouchableOpacity
								style={styles.notAMember}
								onPress={() => {
									this.props.navigation.navigate("SignUp");
								}}>
								<GoappTextRegular size={height / 60} color={"#5C6170"}>
									{"New to MEX.? "}
									<GoappTextRegular color={"#2C98F0"} size={height / 60}>
										{"Sign Up via Email!"}
									</GoappTextRegular>
								</GoappTextRegular>
							</TouchableOpacity>
						</View>
					</View>
				</View>
				{isLoading ? (
					<ProgressScreen
						isMessage={true}
						primaryMessage={"Hang On..."}
						message={"Signing In"}
						indicatorColor={"#2C98F0"}
						indicatorSize={height / 50}
					/>
				) : null}
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		appToken: state.appToken ? state.appToken.token : null
	};
};

EmailLogin.contextType = DialogContext;
export default connect(mapStateToProps)(EmailLogin);
