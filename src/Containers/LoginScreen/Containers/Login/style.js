import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	backgroundImage: {
		flex: 0.4,
		height: undefined,
		width: undefined
	},
	header: {
		flexDirection: "row",
		justifyContent: "space-between",
		marginTop: height / 10,
		alignItems: "center"
	},
	mexLogo: {
		height: height / 10.2,
		width: height / 10.2
	},
	googleLogin: {
		padding: width / 30,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#2c98f0",
		borderRadius: height / 10,
		...Platform.select({
			android: { elevation: 3 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.2
			}
		})
	},
	facebookLogin: {
		padding: width / 30,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#3956a0",
		borderRadius: height / 10,
		...Platform.select({
			android: { elevation: 3 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.2
			}
		})
	},
	emailLogin: {
		padding: width / 30,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#ffffff",
		borderRadius: height / 10,
		...Platform.select({
			android: { elevation: 3 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.2
			}
		})
	},
	signupMemberText: {
		color: "#5C6170"
	},
	registerSignupText: {
		color: "#2C98F0"
	},
	notAMember: {
		alignSelf: "center"
	},
	buttonOuterContainer: {
		flex: 0.6,
		paddingVertical: height / 30,
		paddingHorizontal: width / 20,
		justifyContent: "center"
	},
	buttonInnerContainer: {
		flex: 0.6,
		justifyContent: "space-between"
	},
	boxSize: {
		paddingVertical: width / 16,
		paddingHorizontal: width / 8
	}
});
