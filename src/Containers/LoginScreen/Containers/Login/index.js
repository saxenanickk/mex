import React, { Component } from "react";
import {
	View,
	ImageBackground,
	Dimensions,
	TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import { LOGINBACKGROUND } from "../../Assets/Img/Image";
import {
	GoappTextRegular,
	GoappTextBold
} from "../../../../Components/GoappText";
import GoogleLogin from "../../../../CustomModules/GoogleLogin";
import FacebookLogin from "../../../../CustomModules/FacebookLogin";

import { styles } from "./style";

import { mexSocialLogin } from "../../Saga";
import { ProgressScreen, Icon } from "../../../../Components";
import DialogContext from "../../../../DialogContext";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import { DeviceId } from "../../../../CustomModules/GoDeviceInfo";
import { ERRORCLOUD } from "../../../../Assets/Img/Image";
import { CommonActions } from "@react-navigation/native";
const { height } = Dimensions.get("window");

class Login extends Component {
	constructor() {
		super();
		this.state = {
			isLoading: false
		};
		this.navigateTo = this.navigateTo.bind(this);
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.props.appToken && prevProps.appToken !== this.props.appToken) {
			this.navigateTo();
		}
	}

	navigateTo = () => {
		this.props.navigation.dispatch(
			CommonActions.reset({
				index: 0,
				routes: [{ name: "Goapp" }]
			})
		);
	};

	setAnalytics = (type, status) => {
		GoAppAnalytics.trackWithProperties("login", {
			type,
			status
		});
	};

	googleSuccess = async response => {
		try {
			this.setAnalytics("google", "success");
			const {
				user: { name, email }
			} = response;
			const deviceId = await DeviceId();
			this.setState({ isLoading: true }, () =>
				this.props.dispatch(
					mexSocialLogin({
						email,
						name,
						deviceId,
						fcmToken: "",
						genericErrorMessage: message => {
							this.setState({ isLoading: false });
							this.context.current.openDialog({
								type: "Information",
								title: "Error",
								message: message,
								icon: ERRORCLOUD
							});
						}
					})
				)
			);
		} catch (error) {
			this.setAnalytics("google", "fail");
			console.log(error);
		}
	};

	facebookSuccess = async response => {
		try {
			this.setAnalytics("facebook", "success");
			const { name, email } = response;
			const deviceId = await DeviceId();
			this.setState({ isLoading: true }, () =>
				this.props.dispatch(
					mexSocialLogin({
						email,
						name,
						deviceId,
						fcmToken: "",
						genericErrorMessage: message => {
							this.setState({ isLoading: false });
							this.context.current.openDialog({
								type: "Information",
								title: "Error",
								message: message,
								icon: ERRORCLOUD
							});
						}
					})
				)
			);
		} catch (error) {
			this.setAnalytics("facebook", "fail");
			console.log(error);
		}
	};

	render() {
		const { isLoading } = this.state;

		return (
			<View style={styles.container}>
				<ImageBackground
					source={LOGINBACKGROUND}
					style={styles.backgroundImage}
					resizeMode="stretch">
					<View style={[styles.header, styles.boxSize]}>
						<GoappTextBold color={"#FFFFFF"} size={height / 27}>
							{""}
						</GoappTextBold>
						<Icon
							iconType={"icomoon"}
							iconName={"mex_new"}
							iconColor={"#fff"}
							iconSize={height / 12}
						/>
					</View>
				</ImageBackground>
				<View style={styles.buttonOuterContainer}>
					<View style={styles.buttonInnerContainer}>
						<GoogleLogin
							onSuccess={this.googleSuccess}
							onError={error => {
								console.log(error);
							}}
						/>
						<FacebookLogin
							onSuccess={this.facebookSuccess}
							onError={error => {
								console.log(error);
							}}
						/>
						<TouchableOpacity
							onPress={() => {
								this.props.navigation.navigate("EmailLogin");
							}}>
							<View style={styles.emailLogin}>
								<GoappTextRegular size={height / 50} color={"#000000"}>
									{"Login via Email"}
								</GoappTextRegular>
							</View>
						</TouchableOpacity>
						<TouchableOpacity
							style={styles.notAMember}
							onPress={() => {
								this.props.navigation.navigate("SignUp");
							}}>
							<GoappTextRegular size={height / 60} color={"#5C6170"}>
								{"New to MEX.? "}
								<GoappTextRegular color={"#2C98F0"} size={height / 60}>
									{"Sign Up via Email!"}
								</GoappTextRegular>
							</GoappTextRegular>
						</TouchableOpacity>
					</View>
				</View>
				{isLoading ? (
					<ProgressScreen
						isMessage={true}
						primaryMessage={"Hang On..."}
						message={"Signing In"}
						indicatorColor={"#2C98F0"}
						indicatorSize={height / 50}
					/>
				) : null}
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		appToken: state.appToken ? state.appToken.token : null
	};
};

Login.contextType = DialogContext;
export default connect(mapStateToProps)(Login);
