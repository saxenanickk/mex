import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	safeareaview: {
		flex: 1,
		justifyContent: "space-around",
		marginHorizontal: width * 0.1
	},
	topText: {
		marginTop: height * 0.01
	},
	usernameText: {
		marginVertical: height * 0.01
	},
	imageView: {
		alignItems: "center"
	},
	image: {
		width: 250,
		height: 125,
		aspectRatio: 1.77
	},
	verifiedText: {
		alignItems: "center"
	},
	loginButton: {
		marginTop: height * 0.01,
		marginBottom: height * 0.05
	},
	bottomView: {
		alignItems: "center"
	},
	resendButton: {
		flexDirection: "row",
		marginTop: height / 60
	}
});
