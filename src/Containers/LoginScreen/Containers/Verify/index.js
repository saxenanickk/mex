import React, { Component } from "react";
import { View, TouchableOpacity, SafeAreaView, Dimensions } from "react-native";
import FastImage from "react-native-fast-image";
import LoginApi from "../../Api";
import { styles } from "./style";
import {
	GoappTextBold,
	GoappTextRegular
} from "../../../../Components/GoappText";
import { VERIFIED_CORPORATE } from "../../../../Assets/Img/Image";
import FreshChat from "../../../../CustomModules/Freshchat";
import ProgressScreen from "../../../../Components/ProgressScreen";
import GoToast from "../../../../Components/GoToast";

const { width, height } = Dimensions.get("window");

/**
 * @typedef {Object} Props
 * @property {string} name
 * @property {string} email
 * @property {NavigationProp} navigation
 */

/**
 * @typedef {Object} State
 */

/**
 * @augments {Component<Props, State>}
 */
class Verify extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isResendMailLoading: false,
			resendCount: 0,
			time: 59
		};
		this.startClock = this.startClock.bind(this);
	}

	componentDidMount() {
		this.interval = setInterval(this.startClock, 1000);
	}

	componentWillUnmount() {
		if (this.interval) {
			clearInterval(this.interval);
		}
	}

	startClock = () => {
		if (this.state.time > 0) {
			this.setState({ time: this.state.time - 1 });
		} else {
			clearInterval(this.interval);
		}
	};

	handleResendMail = async () => {
		try {
			this.setState({ isResendMailLoading: true });
			const email = this.props.route.params.email || "";
			let resendMailResp = await LoginApi.resendMail({ email });
			if (resendMailResp.success === 1) {
				GoToast.show("Verification link sent successfully", "Yay!");
				this.setState({ time: 59 });
				this.interval = setInterval(this.startClock, 1000);
			} else {
			}
		} catch (error) {
			console.log(error);
			GoToast.show("Failed to send verification link", "Oops!");
		} finally {
			this.setState({ isResendMailLoading: false });
		}
	};
	render() {
		const name = this.props.route.params.name || "";
		const email = this.props.route.params.email || "";

		return (
			<SafeAreaView style={styles.safeareaview}>
				<View style={styles.topText}>
					<GoappTextBold size={32}>Hello</GoappTextBold>
					<GoappTextBold
						size={42}
						color={"#2c98f0"}
						style={styles.usernameText}>
						{name}!
					</GoappTextBold>
					<GoappTextRegular>
						We have sent a verification email to
						<GoappTextBold>{` ${email}`}</GoappTextBold>. Kindly verify your
						account and login to MEX.
					</GoappTextRegular>
				</View>
				<View style={styles.imageView}>
					<FastImage source={VERIFIED_CORPORATE} style={styles.image} />
				</View>
				<View style={styles.verifiedText}>
					<GoappTextBold>Already verified? Proceed to</GoappTextBold>
					<TouchableOpacity
						style={styles.loginButton}
						onPress={() => this.props.navigation.replace("EmailLogin")}>
						<GoappTextRegular color={"#2c98f0"} size={22}>
							Login!
						</GoappTextRegular>
					</TouchableOpacity>
					<TouchableOpacity
						style={styles.resendButton}
						onPress={this.handleResendMail}
						disabled={this.state.time > 0}>
						{this.state.time > 0 ? (
							<GoappTextRegular color={"#2C98F0"} size={height / 50}>
								{`00:${
									this.state.time > 10 ? this.state.time : "0" + this.state.time
								}`}
							</GoappTextRegular>
						) : null}
						<GoappTextRegular
							style={{ marginLeft: width / 60 }}
							color={this.state.time > 0 ? "#cbcbcb" : "#2C98F0"}
							size={height / 50}>
							{"Resend Mail"}
						</GoappTextRegular>
					</TouchableOpacity>
				</View>
				<View style={styles.bottomView}>
					<GoappTextBold>
						Trouble Signing Up?
						<GoappTextBold
							color={"#2c98f0"}
							onPress={() => FreshChat.launchSupportChat()}>
							{" "}
							Contact support!
						</GoappTextBold>
					</GoappTextBold>
				</View>
				{this.state.isResendMailLoading ? (
					<ProgressScreen
						message={"Sending Verification Link"}
						isMessage={true}
					/>
				) : null}
			</SafeAreaView>
		);
	}
}

export default Verify;
