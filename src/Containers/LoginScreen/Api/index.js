import Config from "react-native-config";

const SERVER_BASE_URL = Config.MEX_LOGIN_BASE_URL;

class LoginApi {
	login(params) {
		const { email, password, deviceId } = params;

		return new Promise((resolve, reject) => {
			fetch(`${SERVER_BASE_URL}login`, {
				method: "POST",
				headers: {
					"content-type": "application/json"
				},
				body: JSON.stringify({
					deviceId,
					email,
					password
				})
			})
				.then(res => res.json())
				.then(response => resolve(response))
				.catch(error => reject(error));
		});
	}

	socialLogin(params) {
		const { email, name, deviceId, fcmToken } = params;

		return new Promise((resolve, reject) => {
			fetch(`${SERVER_BASE_URL}login/social`, {
				method: "POST",
				headers: {
					"content-type": "application/json"
				},
				body: JSON.stringify({
					email,
					name,
					deviceId,
					fcmToken
				})
			})
				.then(res => res.json())
				.then(response => resolve(response))
				.catch(error => reject(error));
		});
	}

	signup(params) {
		const { email, name, password, phoneNum } = params;

		return new Promise((resolve, reject) => {
			fetch(`${SERVER_BASE_URL}register`, {
				method: "POST",
				headers: {
					"content-type": "application/json"
				},
				body: JSON.stringify({
					deviceId: "",
					email,
					name,
					password,
					phoneNum
				})
			})
				.then(res => res.json())
				.then(response => resolve(response))
				.catch(error => reject(error));
		});
	}

	logout(params) {
		const { token } = params;
		return new Promise((resolve, reject) => {
			fetch(`${SERVER_BASE_URL}logout`, {
				method: "POST",
				headers: {
					"x-access-token": token
				}
			})
				.then(res => res.json())
				.then(response => resolve(response))
				.catch(error => reject(error));
		});
	}

	forgotPassword(params) {
		const { email } = params;
		return new Promise((resolve, reject) => {
			fetch(`${SERVER_BASE_URL}forgotPassword`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({ email })
			})
				.then(res => res.json())
				.then(response => resolve(response))
				.catch(error => reject(error));
		});
	}

	resendMail(params) {
		const { email } = params;
		return new Promise((resolve, reject) => {
			fetch(`${SERVER_BASE_URL}resendActivationLink`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({ email, otpType: "SIGNUP" })
			})
				.then(res => res.json())
				.then(response => resolve(response))
				.catch(error => reject(error));
		});
	}
}

export default new LoginApi();
