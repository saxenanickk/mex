import React from "react";
import { Stack } from "../../utils/Navigators";
import EmailLogin from "./Containers/EmailLogin";
import SignUp from "./Containers/SignUp";
import Login from "./Containers/Login";
import ForgotPassword from "./Containers/ForgotPassword";
import Verify from "./Containers/Verify";

const Navigator = () => (
	<Stack.Navigator headerMode={"float"}>
		<Stack.Screen
			options={{ headerShown: false }}
			name={"Login"}
			component={Login}
		/>
		<Stack.Screen
			options={{ title: "Login" }}
			name={"EmailLogin"}
			component={EmailLogin}
		/>
		<Stack.Screen
			options={{ title: "Sign Up" }}
			name={"SignUp"}
			component={SignUp}
		/>
		<Stack.Screen
			options={{ title: "Forgot Password" }}
			name={"ForgotPassword"}
			component={ForgotPassword}
		/>
		<Stack.Screen name={"Verify"} component={Verify} />
	</Stack.Navigator>
);

export default Navigator;
