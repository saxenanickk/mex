import React, { Component } from "react";
import { Text, View, StatusBar } from "react-native";
import Navigator from "./RegisterScreen";
import { GoappAlertContext } from "../../GoappAlertContext";
import { CustomAlert } from "../../Components";
import GoAppAnalytics from "../../utils/GoAppAnalytics";
export class App extends Component {
	alertRef = React.createRef();

	componentDidMount() {
		setTimeout(() => {
			StatusBar.setBackgroundColor("#1f3053");
			StatusBar.setBarStyle("light-content");
		}, 200);
	}

	render() {
		return (
			<View style={{ flex: 1 }}>
				<GoappAlertContext.Provider value={this.alertRef}>
					<Navigator
						onNavigationStateChange={(prevState, currState) =>
							GoAppAnalytics.logChangeOfScreenInStack(
								"login_signup_screen",
								prevState,
								currState
							)
						}
					/>
				</GoappAlertContext.Provider>
				<CustomAlert ref={this.alertRef} />
			</View>
		);
	}
}

export default App;
