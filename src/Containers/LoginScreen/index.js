import React from "react";
import { AppRegistry } from "react-native";
import { connect } from "react-redux";
import App from "./App";
import reducer from "./Reducer";
import { getNewReducer } from "../../../App";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";

class Login extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "login" });
		getNewReducer({ name: "login", reducer: reducer });
	}

	componentWillUnmount() {
		// removeExistingReducer("login");
	}

	render() {
		return <App {...this.props} />;
	}
}

export default connect()(Login);
AppRegistry.registerComponent("Login", () => Login);
