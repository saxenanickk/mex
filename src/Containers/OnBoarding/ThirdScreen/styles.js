import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#ffffff",
		alignItems: "center",
		paddingVertical: height / 10
	},
	screenContainer: {
		flex: 1,
		padding: width / 10
	},
	titleContainer: {
		flex: 0.65,
		marginLeft: width / 30,
		width: width / 1.25,
		alignSelf: "flex-start"
	},
	titleBold: {
		color: "#2B3136",
		fontSize: height / 30
	},
	titleRegular: {
		color: "#2B3136",
		fontSize: height / 30
	},
	content: {
		flex: 1.5
	},
	image: { flex: 1, width: undefined, height: undefined }
});
