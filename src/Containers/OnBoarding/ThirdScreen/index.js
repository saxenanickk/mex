import React, { Component } from "react";
import { View } from "react-native";
import FastImage from "react-native-fast-image";
import { GoappTextBold, GoappTextRegular } from "../../../Components/GoappText";
import styles from "./styles";
import { ONBOARDINGFIRST } from "../Assets/Img";

export class ThirdScreen extends Component {
	render() {
		return (
			<View style={styles.screenContainer}>
				<View style={styles.titleContainer}>
					<GoappTextRegular style={styles.titleBold}>
						<GoappTextBold style={styles.titleBold}>{"Connect "}</GoappTextBold>
						{"with the "}
						<GoappTextBold style={styles.titleBold}>
							{"like minded "}
						</GoappTextBold>
						<GoappTextRegular style={styles.titleBold}>
							{"members in your campus"}
						</GoappTextRegular>
					</GoappTextRegular>
				</View>
				<View style={styles.content}>
					<FastImage
						style={styles.image}
						source={ONBOARDINGFIRST}
						resizeMode={"contain"}
					/>
				</View>
			</View>
		);
	}
}

export default ThirdScreen;
