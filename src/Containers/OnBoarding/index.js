import React, { Component, Fragment } from "react";
import {
	View,
	TouchableOpacity,
	Dimensions,
	StatusBar,
	SafeAreaView
} from "react-native";
import { connect } from "react-redux";
import { GoappTextMedium } from "../../Components/GoappText";
import styles from "./styles";
import FirstScreen from "./FirstScreen";
import Carousel from "react-native-snap-carousel";
import SecondScreen from "./SecondScreen";
import ThirdScreen from "./ThirdScreen";

import { mexSaveGlobalNavigationState } from "../../Saga";
import { CommonActions } from "@react-navigation/native";

const { width, height } = Dimensions.get("window");

class OnBoarding extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currentSectionIndex: 0
		};
		this.props.dispatch(mexSaveGlobalNavigationState(props.navigation));
	}

	componentDidMount() {
		setTimeout(() => {
			StatusBar.setBackgroundColor("#fff");
			StatusBar.setBarStyle("dark-content");
		}, 200);
	}

	render() {
		return (
			<Fragment>
				{/** For the Top Color (Under the Notch) */}
				<SafeAreaView style={{ flex: 0, backgroundColor: "#ffffff" }} />
				{/** For the Bottom Color (Below the home touch) */}
				<SafeAreaView style={{ flex: 1 }}>
					<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
					<View style={styles.container}>
						<Carousel
							bounces={false}
							data={[1, 2, 3]}
							renderItem={({ item }) => {
								return item === 1 ? (
									<FirstScreen />
								) : item === 2 ? (
									<SecondScreen />
								) : (
									<ThirdScreen />
								);
							}}
							sliderWidth={width}
							itemWidth={width}
							hasParallaxImages={true}
							inactiveSlideScale={1}
							inactiveSlideOpacity={1}
							loopClonesPerSide={2}
							autoplay={false}
							onSnapToItem={index =>
								this.setState({ currentSectionIndex: index })
							}
							ref={ref => (this.corouselRef = ref)}
						/>
						<View
							style={{
								alignSelf: "center",
								marginVertical: height / 60
							}}>
							<View
								style={{
									flexDirection: "row",
									justifyContent: "center",
									paddingVertical: height / 90
								}}>
								{[1, 2, 3].map((item, index) => {
									if (this.state.currentSectionIndex === index) {
										return (
											<View
												key={index}
												style={{
													width: 20,
													height: 10,
													borderRadius: 5,
													backgroundColor: "#2c98f0",
													marginHorizontal: width / 90
												}}
											/>
										);
									} else {
										return (
											<View
												key={index}
												style={{
													width: 10,
													height: 10,
													borderRadius: 5,
													backgroundColor: "#D8D8D8",
													marginHorizontal: width / 90
												}}
											/>
										);
									}
								})}
							</View>
						</View>
						<TouchableOpacity
							onPress={() => {
								let index =
									this.state.currentSectionIndex < 2
										? this.state.currentSectionIndex + 1
										: this.props.navigation.dispatch(
												CommonActions.reset({
													index: 0,
													routes: [{ name: "Login" }]
												})
										  );
								this.corouselRef.snapToItem(index);
							}}
							style={styles.button}>
							<GoappTextMedium style={styles.buttonText}>
								{this.state.currentSectionIndex < 2 ? "Next" : "Done"}
							</GoappTextMedium>
						</TouchableOpacity>
					</View>
				</SafeAreaView>
			</Fragment>
		);
	}
}

const mapStateToProps = state => {
	return {};
};

export default connect(mapStateToProps)(OnBoarding);
