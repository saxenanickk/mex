import React, { Component } from "react";
import { View } from "react-native";
import FastImage from "react-native-fast-image";
import { GoappTextBold, GoappTextRegular } from "../../../Components/GoappText";
import styles from "./styles";
import { ONBOARDINGTHIRD } from "../Assets/Img";

export class FirstScreen extends Component {
	render() {
		return (
			<View style={styles.screenContainer}>
				<View style={styles.titleContainer}>
					<GoappTextRegular style={styles.titleBold}>
						{"Experience the world of "}
						<GoappTextBold style={styles.titleBold}>
							{"smart buildings "}
						</GoappTextBold>
						<GoappTextRegular style={styles.titleBold}>
							{"and "}
						</GoappTextRegular>
						<GoappTextBold style={styles.titleBold}>
							{"smart features."}
						</GoappTextBold>
					</GoappTextRegular>
				</View>
				<View style={styles.content}>
					<FastImage
						style={styles.image}
						source={ONBOARDINGTHIRD}
						resizeMode={"contain"}
					/>
				</View>
			</View>
		);
	}
}

export default FirstScreen;
