import React, { Component } from "react";
import { View } from "react-native";
import FastImage from "react-native-fast-image";
import { GoappTextBold, GoappTextRegular } from "../../../Components/GoappText";
import styles from "./styles";
import { ONBOARDINGSECOND } from "../Assets/Img";

export class SecondScreen extends Component {
	render() {
		return (
			<View style={styles.screenContainer}>
				<View style={styles.titleContainer}>
					<GoappTextRegular style={styles.titleBold}>
						{"Access "}
						<GoappTextBold style={styles.titleBold}>
							{"best brands "}
						</GoappTextBold>
						<GoappTextRegular style={styles.titleBold}>
							{"and curated offers "}
						</GoappTextRegular>
						<GoappTextBold style={styles.titleBold}>
							{"all at one place"}
						</GoappTextBold>
					</GoappTextRegular>
				</View>
				<View style={styles.content}>
					<FastImage
						style={styles.image}
						source={ONBOARDINGSECOND}
						resizeMode={"contain"}
					/>
				</View>
			</View>
		);
	}
}

export default SecondScreen;
