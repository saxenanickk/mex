import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#ffffff",
		alignItems: "center",
		paddingBottom: height / 20
	},
	button: {
		backgroundColor: "#2C98F0",
		width: width - width / 15,
		height: height / 17,
		borderRadius: 30,
		justifyContent: "center",
		alignItems: "center"
	},
	buttonText: {
		color: "#ffffff",
		fontSize: height / 50
	}
});
