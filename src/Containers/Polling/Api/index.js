import Config from "react-native-config";

const { SERVER_BASE_URL_POLLING } = Config;

function createDetailResponseForPoll(pollDetail, response) {
	return new Promise(function(resolve, reject) {
		let errorObj = { success: 0, data: null };
		try {
			let data = [];
			data = pollDetail;
			let myAnswer = null;
			let pollResult = null;
			if (response.length === 2) {
				pollResult = response[0];
				myAnswer = response[1];
			} else if (response.length === 1) {
				myAnswer = response[0];
			}
			pollDetail.questions.forEach((item, index) => {
				if (myAnswer && myAnswer.data) {
					let aindex = myAnswer.data.findIndex(
						answer => answer.questionId === item.id
					);
					if (aindex >= 0) {
						data.questions[index].selectedOptions =
							myAnswer.data[aindex].answerIds;
					}
				}

				if (pollResult && pollResult.data && pollResult.data.questions) {
					let prIndex = pollResult.data.questions.findIndex(
						prQuestion => prQuestion.questionId === item.id
					);
					if (prIndex >= 0) {
						let answer = pollResult.data.questions[prIndex].answers;
						item.choice &&
							item.choice.forEach((choice, cindex) => {
								const percentage = answer[cindex].percentage;
								data.questions[prIndex].choice[cindex] = {
									...choice,
									isShowPercentage: true,
									percentage
								};
							});
					}
				}
			});
			resolve({ success: 1, data: data });
		} catch (error) {
			console.log("err", error);
			resolve(errorObj);
		}
	});
}

class PollingApi {
	constructor() {
		console.log("polling api instantiated");
	}

	getAllPolls(params) {
		return new Promise(function(resolve, reject) {
			let url = SERVER_BASE_URL_POLLING + "getPollsByStatus";
			url = url + `?ongoingStatus=ONGOING&site_id=${params.site_id}`;
			try {
				fetch(url, {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"x-access-token": params.appToken
					}
				})
					.then(response => {
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(err => reject(err));
					})
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	getPollDetail(params) {
		let url = "getPollsById";
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					`${SERVER_BASE_URL_POLLING}${url}?id=${params.pollId}&site_id=${
						params.site_id
					}`,
					{
						method: "GET",
						headers: {
							"Content-type": "application/json",
							"x-access-token": params.appToken
						}
					}
				)
					.then(response => {
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(err => {
								reject(err);
							});
					})
					.catch(err => {
						reject(err);
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	getPollResult(params) {
		let urls = params.pollDetail.publishResult
			? ["getPollsResultByid", "getMyAnswer"]
			: ["getMyAnswer"];
		let promise = [];
		const pollDetail = params.pollDetail;
		urls.forEach(url => {
			promise.push(
				new Promise(function(resolve, reject) {
					try {
						fetch(
							`${SERVER_BASE_URL_POLLING}${url}?id=${params.pollId}&site_id=${
								params.site_id
							}`,
							{
								method: "GET",
								headers: {
									"Content-type": "application/json",
									"x-access-token": params.appToken
								}
							}
						)
							.then(response => {
								response
									.json()
									.then(res => {
										resolve(res);
									})
									.catch(err => {
										reject(err);
									});
							})
							.catch(err => {
								reject(err);
							});
					} catch (error) {
						reject(error);
					}
				})
			);
		});
		return new Promise((resolve, reject) => {
			Promise.all(promise)
				.then(async response => {
					response = await createDetailResponseForPoll(pollDetail, response);

					resolve(response);
				})
				.catch(error => {
					reject(error);
				});
		});
	}

	getPollResults(params) {
		return new Promise(function(resolve, reject) {
			let url = SERVER_BASE_URL_POLLING + "getPollsResultByid";
			url = url + `?id=${params.pollId}&site_id=${params.site_id}`;
			try {
				fetch(url, {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"x-access-token": params.appToken
					}
				})
					.then(response => {
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(err => {
								reject(err);
							});
					})
					.catch(err => {
						reject(err);
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	submitPollAnswer(params) {
		return new Promise(function(resolve, reject) {
			let url = SERVER_BASE_URL_POLLING + "answer";
			url = url + `?site_id=${params.site_id}`;
			try {
				fetch(url, {
					method: "POST",
					headers: {
						"Content-type": "application/json",
						"x-access-token": params.appToken
					},
					body: JSON.stringify(params.body)
				})
					.then(response => {
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(err => {
								reject(err);
							});
					})
					.catch(err => reject(err));
			} catch (err) {
				reject(err);
			}
		});
	}
}

export default new PollingApi();
