import { all } from "redux-saga/effects";
import { pollingHomeSaga } from "./Containers/Home/Saga";
import { pollingDetailSaga } from "./Containers/PollDetail/Saga";
// Import different Sagas here

export const POLLING_SAVE_ERROR = "POLLING_SAVE_ERROR";

/**
 * @typedef {Object} ErrorType
 * @property {String} errorScreen
 * @property {String} errorMessage
 *
 * @param {ErrorType} payload
 */
export const pollingSaveError = payload => ({
	type: POLLING_SAVE_ERROR,
	payload
});

export default function* pollingSaga() {
	yield all([pollingHomeSaga(), pollingDetailSaga()]);
}
