import React, { Component } from "react";
import {
	View,
	BackHandler,
	Dimensions,
	FlatList,
	StatusBar
} from "react-native";
import { connect } from "react-redux";
import { PollListItem, PollEmptyView, PollHeader } from "../../Components";
import { styles, SKY_BLUE, WHITE } from "./style";
import { ProgressScreen } from "../../../../Components";
import { pollGetAllPolls, pollSaveAllPolls } from "./Saga";
import { pollingSaveError } from "../../Saga";
import {
	POLL_HOME_SCREEN,
	NO_POLLS_YET,
	HANG_ON,
	POLL,
	LOADING_POLLS,
	ALWAYS,
	POLLING
} from "../../constant";
import { GoappAlertContext } from "../../../../GoappAlertContext";

const { height } = Dimensions.get("window");

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isFocused: true,
			isError: false,
			isLoading: false,
			message: ""
		};
		this.fetchAllPolls = this.fetchAllPolls.bind(this);
		BackHandler.addEventListener("hardwareBackPress", this.handleBackPressed);
	}

	componentDidMount() {
		this._unsubscribeFocus = this.props.navigation.addListener("focus", () => {
			this.setState({ isFocused: true });
		});

		this._unsubscribeBlur = this.props.navigation.addListener("blur", () => {
			this.setState({ isFocused: false });
		});

		this.fetchAllPolls();
	}
	handleBackPressed = () => {
		if (this.state.isFocused) {
			const parent = this.props.navigation.dangerouslyGetParent();
			let route = "Goapp";
			if (parent && parent.state && parent.state.routes) {
				let length = parent.state.routes.length;
				if (length >= 2) {
					route = parent.state.routes[length - 2].routeName;
				}
			}
			this.props.navigation.navigate(route);
			return true;
		}
	};

	shouldComponentUpdate(props, state) {
		if (
			props.error &&
			props.error.errorScreen &&
			props.error.errorScreen === POLL_HOME_SCREEN &&
			props.error !== this.props.error
		) {
			this.setState({ isError: true, isLoading: false });
		}
		if (props.polls && props.polls !== this.props.polls) {
			this.setState({ isLoading: false });
		}
		return true;
	}

	componentWillUnmount() {
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.handleBackPressed
		);
		this._unsubscribeFocus();
		this._unsubscribeBlur();
		this.props.dispatch(pollSaveAllPolls(null));
		this.props.dispatch(pollingSaveError(null));
	}

	fetchAllPolls() {
		this.setState({
			isError: false,
			isLoading: true,
			message: LOADING_POLLS
		});
		this.props.dispatch(pollSaveAllPolls(null));
		this.props.dispatch(pollingSaveError(null));
		this.props.dispatch(
			pollGetAllPolls({
				appToken: this.props.appToken,
				site_id: this.props.selectedSite.id
			})
		);
	}

	navigateToPollDetail = item => {
		let navigationPayload = {
			pollId: item.pollDetail.id
		};
		this.setState({ isFocused: false });
		this.props.navigation.navigate("PollDetail", navigationPayload);
	};

	renderLoader = () => {
		try {
			if (this.state.isLoading) {
				return (
					<ProgressScreen
						isMessage={true}
						indicatorSize={height / 35}
						indicatorColor={SKY_BLUE}
						primaryMessage={HANG_ON}
						message={this.state.message}
					/>
				);
			} else return null;
		} catch (error) {
			return null;
		}
	};

	renderPollListItems = ({ item }) => (
		<PollListItem
			title={item.pollDetail.name}
			isPoll={item.pollDetail.type === POLL}
			style={{
				marginTop: height / 60
			}}
			onPress={() => this.navigateToPollDetail(item)}
			questions={item.questionIds ? item.questionIds.length : 0}
			isCompleted={item.userCompletionStatus}
		/>
	);

	renderErrorView = () => {
		try {
			if (this.state.isError) {
				return (
					<PollEmptyView textMessage={NO_POLLS_YET} style={styles.emptyView} />
				);
			} else {
				return null;
			}
		} catch (error) {
			return null;
		}
	};

	render() {
		return (
			<View style={styles.container}>
				<StatusBar backgroundColor={WHITE} barStyle={"dark-content"} />
				<PollHeader onPress={this.handleBackPressed} title={POLLING} />
				{this.props.polls ? (
					<FlatList
						data={this.props.polls}
						keyExtractor={(item, index) => index.toString()}
						showsVerticalScrollIndicator={false}
						keyboardShouldPersistTaps={ALWAYS}
						refreshing={false}
						onRefresh={this.fetchAllPolls}
						renderItem={this.renderPollListItems}
						ListEmptyComponent={() => (
							<PollEmptyView
								textMessage={NO_POLLS_YET}
								style={styles.emptyView}
							/>
						)}
					/>
				) : null}
				{this.renderLoader()}
				{this.renderErrorView()}
			</View>
		);
	}
}
Home.contextType = GoappAlertContext;

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		user: state.appToken.user,
		userProfile: state.appToken.userProfile,
		selectedSite: state.appToken.selectedSite,
		polls: state.polling && state.polling.homeReducer.polls,
		error: state.polling && state.polling.mainReducer.error,
		pollsResult: state.polling && state.polling.homeReducer.pollsResult
	};
}

export default connect(mapStateToProps)(Home);
