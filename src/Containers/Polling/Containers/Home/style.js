import { Dimensions, StyleSheet } from "react-native";
const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	question: {
		width: width,
		flexDirection: "row",
		alignItems: "center",
		paddingHorizontal: width / 30,
		paddingVertical: height / 90
	},
	container: {
		flex: 1,
		backgroundColor: "#eff1f4"
	},
	emptyView: {
		height: height - height / 15,
		backgroundColor: "#fff"
	}
});

export const SKY_BLUE = "#2C98F0";
export const WHITE = "#fff";
