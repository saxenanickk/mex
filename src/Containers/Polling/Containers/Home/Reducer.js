import { POLL_SAVE_ALL_POLLS } from "./Saga";

const initialState = {
	polls: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case POLL_SAVE_ALL_POLLS:
			return {
				...state,
				polls: action.payload
			};
		default:
			return state;
	}
};
