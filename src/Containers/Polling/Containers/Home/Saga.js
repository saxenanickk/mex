import { takeLatest, put, call } from "redux-saga/effects";
import Api from "../../Api";
import { pollingSaveError } from "../../Saga";
import { POLL_HOME_SCREEN } from "../../constant";

export const POLL_GET_ALL_POLLS = "POLL_GET_ALL_POLLS";
export const POLL_SAVE_ALL_POLLS = "POLL_SAVE_ALL_POLLS";

/**
 * @typedef {Object} GetPoll
 * @property {String} appToken
 * @param {GetPoll} payload
 */
export const pollGetAllPolls = payload => ({
	type: POLL_GET_ALL_POLLS,
	payload
});

export const pollSaveAllPolls = payload => ({
	type: POLL_SAVE_ALL_POLLS,
	payload
});

export function* pollingHomeSaga() {
	yield takeLatest(POLL_GET_ALL_POLLS, handlePollGetAllPolls);
}

function* handlePollGetAllPolls(action) {
	try {
		const resp = yield call(Api.getAllPolls, action.payload);
		if (resp && resp.data && resp.data.length > 0) {
			yield put(pollSaveAllPolls(resp.data));
		} else {
			throw new Error("Unable to get Polls");
		}
	} catch (error) {
		yield put(
			pollingSaveError({
				errorScreen: POLL_HOME_SCREEN,
				errorMessage: "No Poll Found"
			})
		);
	}
}
