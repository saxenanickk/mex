import React, { Component } from "react";
import {
	View,
	Dimensions,
	FlatList,
	TouchableOpacity,
	StatusBar,
	Image
} from "react-native";
import { connect } from "react-redux";
import {
	GoappTextBold,
	GoappTextRegular
} from "../../../../Components/GoappText";
import { PollQuestionSection } from "../../Components";
import { ProgressScreen, Icon } from "../../../../Components";
import {
	pollGetDetail,
	pollSaveDetail,
	pollSaveResults,
	pollPostAnswer
} from "./Saga";
import { pollingSaveError } from "../../Saga";
import DialogContext from "../../../../DialogContext";
import {
	POLL_DETAIL_SCREEN,
	NO_POLL_DETAIL,
	POLL_ANSWER_NOT_SUBMIT
} from "../../constant";
import { styles } from "./style";
import {
	ERRORCLOUD,
	VERIFIED_CORPORATE,
	POLLING_CREATIVE
} from "../../../../Assets/Img/Image";
import LinearGradient from "react-native-linear-gradient";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import I18n from "../../../../Assets/strings/i18n";

const { height } = Dimensions.get("window");

class PollDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			answer: {},
			isLoading: true,
			message: "",
			isSubmitedAllAnswer: false
		};
	}

	componentDidMount() {
		this.getPollDetail();
	}

	componentWillUnmount() {
		this.props.dispatch(pollSaveDetail(null));
		this.props.dispatch(pollingSaveError(null));
	}

	shouldComponentUpdate(props, state) {
		if (
			props.error &&
			props.error.errorScreen === POLL_DETAIL_SCREEN &&
			props.error.errorMessage === NO_POLL_DETAIL &&
			props.error !== this.props.error
		) {
			this.showAlert("Error", "unable to get poll details.", true);
		}
		if (props.pollDetail && props.pollDetail !== this.props.pollDetail) {
			this.setState({ isLoading: false, message: "" });
		}
		if (
			props.error &&
			props.error.errorScreen === POLL_DETAIL_SCREEN &&
			props.error.errorMessage === POLL_ANSWER_NOT_SUBMIT &&
			props.error !== this.props.error
		) {
			this.setState({ isLoading: false, message: "" });
			this.showAlert("Error", "Answer not submitted.", false, false);
		}
		if (
			props.pollAnswer &&
			props.pollAnswer !== this.props.pollAnswer &&
			props.pollAnswer.success === 1
		) {
			this.setState({ isLoading: false, message: "" });
			this.showAlert(
				"Success!",
				"Your response has been registered. Thanks for participating.",
				false,
				true
			);
		}
		return true;
	}

	showAlert = (title, message, type = false, backOnPress = true) => {
		let icon = VERIFIED_CORPORATE;
		let rightTitle;
		let rightPress = () =>
			backOnPress ? this.props.navigation.goBack() : null;
		if (type) {
			icon = ERRORCLOUD;
			rightTitle = "Retry";
			rightPress = () => this.getPollDetail();
		}
		this.context.current.openDialog({
			type: "Action",
			title: title,
			message: message,
			rightTitle: rightTitle,
			rightPress: rightPress,
			icon: icon
		});
	};

	setQuestionAnswer = (questionsId, selectedOption) => {
		let temp = this.state.answer;
		if (selectedOption.length > 0) {
			temp[questionsId] = selectedOption;
		} else {
			delete temp[questionsId];
		}
		this.setState({ answer: temp });
	};

	getPollDetail() {
		const { selectedSite, appToken } = this.props;
		const pollId = this.props.route.params.pollId || null;
		// Poll Open
		GoAppAnalytics.trackWithProperties("Poll Open", {
			id: pollId
		});
		this.setState({ isLoading: true, message: "Loading poll detail" });
		this.props.dispatch(pollSaveDetail(null));
		this.props.dispatch(pollingSaveError(null));
		this.props.dispatch(pollSaveResults(null));
		this.props.dispatch(
			pollGetDetail({
				pollId: pollId,
				site_id: selectedSite.id,
				appToken: appToken
			})
		);
	}

	onSubmitButton = () => {
		let temp = this.state.answer;
		const { selectedSite, appToken } = this.props;
		let body = {
			pollId: this.props.pollDetail.id,
			questionAnswers: temp
		};
		if (
			this.props.pollDetail.questions.length ===
			Object.keys(body.questionAnswers).length
		) {
			GoAppAnalytics.trackWithProperties("Poll Submit", {
				id: this.props.pollDetail.id
			});
			this.setState({ isLoading: true, message: "Submitting your response" });
			this.props.dispatch(
				pollPostAnswer({
					body: body,
					site_id: selectedSite.id,
					appToken: appToken
				})
			);
		} else {
			this.setState({ isSubmitedAllAnswer: true });
		}
	};

	renderPollQuestions = ({ item, index }) => {
		let isDisabled = this.props.pollDetail.userCompletionStatus ? true : false;
		if (item.choice && item.choice.length > 0) {
			return (
				<PollQuestionSection
					questionNumber={index + 1}
					isDisabled={isDisabled}
					data={item}
					style={styles.pollingQuestionSection}
					onAnswerSelect={answer => this.setQuestionAnswer(item.id, answer)}
				/>
			);
		} else {
			return null;
		}
	};
	pollHeader = () => {
		let answerSubmit = Object.keys(this.state.answer).length;
		return (
			<LinearGradient
				start={{ x: 0.0, y: 1.0 }}
				end={{ x: 0.0, y: 0.0 }}
				style={styles.headerContainer}
				colors={["#294d77", "#5c97c9"]}>
				<Image
					source={POLLING_CREATIVE}
					style={styles.imageBackground}
					resizeMode={"contain"}
				/>
				<View style={styles.absoluteHeader}>
					<TouchableOpacity
						style={styles.touchableOpacityBack}
						onPress={() => {
							this.props.navigation.goBack();
						}}>
						<Icon
							iconType={"ionicon"}
							iconName={"ios-arrow-round-back"}
							iconSize={height / 20}
							iconColor={"#fff"}
						/>
					</TouchableOpacity>
					<View style={styles.titleQuestionView}>
						<GoappTextRegular style={styles.headerTitle}>
							{this.props.pollDetail.name}
						</GoappTextRegular>
						{this.props.pollDetail.userCompletionStatus !== true ? (
							<GoappTextRegular style={styles.headerQuestion} numberOfLines={1}>
								{`${answerSubmit}/${
									this.props.pollDetail.questions.length
								} ${I18n.t("question", {
									count: this.props.pollDetail.questions.length
								})}`}
							</GoappTextRegular>
						) : null}
					</View>
				</View>
			</LinearGradient>
		);
	};

	renderLoader = () => {
		try {
			if (this.state.isLoading) {
				return (
					<ProgressScreen
						isMessage={true}
						indicatorSize={height / 35}
						indicatorColor={"#2C98F0"}
						primaryMessage={"Hang On..."}
						message={this.state.message}
					/>
				);
			} else return null;
		} catch (error) {
			return null;
		}
	};

	render() {
		let isDisabled =
			this.props.pollDetail && this.props.pollDetail.userCompletionStatus
				? true
				: false;
		return (
			<View style={styles.container}>
				<StatusBar backgroundColor={"#6692b5"} barStyle={"dark-content"} />
				{this.props.pollDetail ? (
					<View style={styles.fullFlex}>
						{this.pollHeader()}
						<FlatList
							showsVerticalScrollIndicator={false}
							keyboardShouldPersistTaps={"always"}
							style={styles.FlatList}
							data={this.props.pollDetail.questions}
							keyExtractor={(item, index) => index.toString()}
							renderItem={this.renderPollQuestions}
						/>
						{!isDisabled ? (
							<View style={styles.submitbutton}>
								{this.state.isSubmitedAllAnswer ? (
									<View style={styles.submitAllQuestionView}>
										<GoappTextBold style={styles.submitAllQuestion}>
											{"Kindly submit your response for all questions"}
										</GoappTextBold>
									</View>
								) : null}
								<TouchableOpacity
									onPress={() => this.onSubmitButton()}
									style={styles.submitTouchableOpacity}>
									<GoappTextBold style={styles.submitText}>
										{"Submit"}
									</GoappTextBold>
								</TouchableOpacity>
							</View>
						) : null}
					</View>
				) : null}
				{this.renderLoader()}
			</View>
		);
	}
}

PollDetail.contextType = DialogContext;
function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		user: state.appToken.user,
		userProfile: state.appToken.userProfile,
		selectedSite: state.appToken.selectedSite,
		pollDetail: state.polling && state.polling.pollDetailReducer.pollDetail,
		error: state.polling && state.polling.mainReducer.error,
		pollAnswer: state.polling && state.polling.pollDetailReducer.pollAnswer
	};
}

export default connect(mapStateToProps)(PollDetail);
