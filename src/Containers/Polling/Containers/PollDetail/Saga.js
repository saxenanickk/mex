import { takeLatest, put, call } from "redux-saga/effects";
import Api from "../../Api";
import { pollingSaveError } from "../../Saga";
import {
	NO_POLL_DETAIL,
	POLL_DETAIL_SCREEN,
	POLL_ANSWER_NOT_SUBMIT
} from "../../constant";
import { pollGetAllPolls } from "../Home/Saga";

export const POLL_GET_DETAILS = "POLL_GET_DETAILS";
export const POLL_SAVE_DETAILS = "POLL_SAVE_DETAILS";
export const POLL_GET_RESULTS = "POLL_GET_RESULTS";
export const POLL_SAVE_RESULTS = "POLL_SAVE_RESULTS";
export const POLL_POST_ANSWER = "POLL_POST_ANSWER";
export const POLL_SAVE_ANSWER_RESP = "POLL_SAVE_ANSWER_RESP";

export const pollGetDetail = payload => ({
	type: POLL_GET_DETAILS,
	payload
});

export const pollSaveDetail = payload => ({
	type: POLL_SAVE_DETAILS,
	payload
});

export const pollGetResults = payload => ({
	type: POLL_GET_RESULTS,
	payload
});

export const pollSaveResults = payload => ({
	type: POLL_SAVE_RESULTS,
	payload
});

export const pollPostAnswer = payload => ({
	type: POLL_POST_ANSWER,
	payload
});

export const pollSaveAnswerResp = payload => ({
	type: POLL_SAVE_ANSWER_RESP,
	payload
});

export function* pollingDetailSaga(dispatch) {
	yield takeLatest(POLL_GET_DETAILS, handleGetPollDetails);
	yield takeLatest(POLL_GET_RESULTS, handleGetPollResults);
	yield takeLatest(POLL_POST_ANSWER, handlePostPollAnswer);
}

function* handleGetPollDetails(action) {
	try {
		const pollDetailResp = yield call(Api.getPollDetail, action.payload);
		if (pollDetailResp.success > 0) {
			if (pollDetailResp.data.userCompletionStatus === true) {
				const pollResultResp = yield call(Api.getPollResult, {
					...action.payload,
					pollDetail: pollDetailResp.data
				});
				if (pollResultResp.success > 0)
					yield put(pollSaveDetail(pollResultResp.data));
				else throw new Error("unable to get poll detail");
			} else {
				yield put(pollSaveDetail(pollDetailResp.data));
			}
		} else {
			throw new Error("unable to get poll detail");
		}
	} catch (error) {
		yield put(
			pollingSaveError({
				errorScreen: POLL_DETAIL_SCREEN,
				errorMessage: NO_POLL_DETAIL
			})
		);
	}
}

function* handleGetPollResults(action) {
	try {
		const resp = yield call(Api.getPollResults, action.payload);
		if (resp && resp.data && resp.data.questions.length > 0)
			yield put(pollSaveResults(resp.data));
		else throw new Error("unable to get poll result");
	} catch (error) {
		yield put(
			pollingSaveError({
				errorScreen: POLL_DETAIL_SCREEN,
				errorMessage: NO_POLL_DETAIL
			})
		);
	}
}

function* handlePostPollAnswer(action) {
	try {
		const resp = yield call(Api.submitPollAnswer, action.payload);
		if (resp && resp.success === 1) {
			let params = {
				site_id: action.payload.site_id,
				appToken: action.payload.appToken,
				pollId: action.payload.body.pollId
			};
			yield put(pollSaveAnswerResp({ success: resp.success }));
			yield put(pollGetDetail(params));
			yield put(
				pollGetAllPolls({ site_id: params.site_id, appToken: params.appToken })
			);
		} else throw new Error("unable to submit answer");
	} catch (error) {
		yield put(
			pollingSaveError({
				errorScreen: POLL_DETAIL_SCREEN,
				errorMessage: POLL_ANSWER_NOT_SUBMIT
			})
		);
	}
}
