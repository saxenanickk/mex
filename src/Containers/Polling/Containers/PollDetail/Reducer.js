import {
	POLL_SAVE_DETAILS,
	POLL_SAVE_RESULTS,
	POLL_SAVE_ANSWER_RESP
} from "./Saga";

const initialState = {
	pollDetail: null,
	pollsResult: null,
	pollAnswer: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case POLL_SAVE_DETAILS:
			return {
				...state,
				pollDetail: action.payload
			};
		case POLL_SAVE_RESULTS:
			return {
				...state,
				pollsResult: action.payload
			};
		case POLL_SAVE_ANSWER_RESP:
			return {
				...state,
				pollAnswer: action.payload
			};
		default:
			return state;
	}
};
