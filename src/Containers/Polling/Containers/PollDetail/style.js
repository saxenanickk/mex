import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	fullFlex: { flex: 1 },
	container: { flex: 1, backgroundColor: "#eff1f4" },
	FlatList: { height: height },
	pollingQuestionSection: { marginTop: height / 80 },
	submitbutton: {
		width: width,
		paddingVertical: height / 60,
		marginTop: height / 90,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#fff"
	},
	submitTouchableOpacity: {
		width: width - width / 15,
		alignSelf: "center",
		backgroundColor: "#2C98F0",
		borderRadius: width / 10,
		justifyContent: "center",
		alignItems: "center",
		paddingVertical: height / 50
	},
	submitText: { color: "#fff", fontSize: height / 45 },
	submitAllQuestion: {
		color: "#e64a58",
		fontSize: height / 72
	},
	submitAllQuestionView: {
		alignSelf: "flex-end",
		paddingBottom: 4,
		marginRight: height / 28
	},
	headerContainer: {
		justifyContent: "center",
		position: "relative",
		alignItems: "flex-end",
		height: height / 4,
		width: width
	},
	absoluteHeader: {
		position: "absolute",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		height: height / 4,
		width: width,
		justifyContent: "space-between",
		paddingVertical: height / 60,
		paddingHorizontal: width / 50
	},
	touchableOpacityBack: {
		width: width / 10,
		height: height / 16,
		// justifyContent: "flex-start",
		paddingHorizontal: width / 30
	},
	title: {
		width: width / 1.3,
		fontSize: width / 25,
		marginLeft: width / 30,
		justifyContent: "center",
		color: "#fff"
	},
	imageBackground: {
		width: width / 2,
		height: height / 5,
		marginRight: width / 20
	},
	AnsByQues: {
		// paddingRight: 0,
		color: "#fff",
		fontSize: width / 35
	},
	titleQuestionView: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "flex-end",
		paddingHorizontal: width / 40
	},
	headerTitle: {
		color: "#fff",
		fontSize: height / 40,
		width: width / 1.7
	},
	headerQuestion: {
		fontSize: height / 50,
		color: "#fff",
		width: width / 3,
		textAlign: "right"
	}
});
