import React from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import reducer from "./Reducer";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";
import { getNewReducer, removeExistingReducer } from "../../../App";

export default class Polling extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "polling" });
		getNewReducer({ name: "polling", reducer: reducer });
	}

	componentWillUnmount() {
		removeExistingReducer("polling");
	}

	render() {
		//Deeplink Params
		return <App params={this.props.params} />;
	}
}

AppRegistry.registerComponent("Polling", () => Polling);
