import React from "react";
import { View, Dimensions, StyleSheet, TouchableOpacity } from "react-native";
import { Icon } from "../../../Components";
import { GoappTextRegular } from "../../../Components/GoappText";
import PropTypes from "prop-types";

const { width, height } = Dimensions.get("window");

const PollEmptyView = props => (
	<View style={[styles.container, props.style]}>
		<View style={styles.icon}>
			<Icon
				iconType={"icomoon"}
				iconName={"poll"}
				iconColor={"#2C98F0"}
				iconSize={height / 10}
			/>
		</View>
		<GoappTextRegular style={styles.textMsg}>
			{props.textMessage}
		</GoappTextRegular>
		{props.isRefresh ? (
			<TouchableOpacity
				style={styles.refresh}
				onPress={() => props.onRefresh()}>
				<GoappTextRegular style={styles.refreshText}>
					{"REFERESH"}
				</GoappTextRegular>
			</TouchableOpacity>
		) : null}
	</View>
);

PollEmptyView.propTypes = {
	/**
	 * text message displayed on empty view
	 */
	textMessage: PropTypes.string,
	/**
	 * whether refresh button shown or not
	 */
	isRefresh: PropTypes.bool,
	/**
	 * action performed when clicked on refresh button
	 */
	onRefresh: PropTypes.func,
	/**
	 * style applied to the parent component
	 */
	style: PropTypes.object
};

const styles = StyleSheet.create({
	container: {
		height: height,
		width: width,
		backgroundColor: "#fff",
		justifyContent: "center",
		alignItems: "center"
	},
	refresh: {
		backgroundColor: "#2C98F0",
		width: "60%",
		marginVertical: height / 18,
		alignItems: "center"
	},
	textMsg: {
		fontSize: 16,
		marginTop: "5%"
	},
	refreshText: {
		color: "#fff",
		padding: "3%"
	}
});

export default PollEmptyView;
