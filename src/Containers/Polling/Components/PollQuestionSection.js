import React, { Component } from "react";
import { View, StyleSheet, Dimensions, TextInput } from "react-native";
import { PollQuestion, PollOption } from "./index";
import { remove } from "lodash";
import {
	OPTION_SINGLE_SELECTION,
	OPTION_TEXT_BOX,
	OPTION_MULTIPLE_SELECTION
} from "../constant";

const { width, height } = Dimensions.get("window");

/**
 * @typedef {Object} Props
 * @property {Object} style
 * @property {Number} questionNumber
 * @property {Boolean} isDisabled
 * @property {Array} data
 * @property {Array} onAnswerSelect
 */

/**
 * @typedef {Object} State
 * @property {Boolean} isSelected
 * @property {Array} selectedOptions
 * @property {String} answerText
 */

/**
 * @augments {Component<Props, State>}
 */
class PollQuestionSection extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isSelected: false,
			selectedOptions: this.props.data.selectedOptions
				? this.props.data.selectedOptions
				: [],
			answerText: ""
		};
	}

	onChangeText = text => {
		this.setState({ answerText: text });
		this.props.onAnswerSelect(text);
	};
	selectedOptions = id => {
		const { type } = this.props.data;
		let tempId = this.state.selectedOptions.slice();
		if (tempId.includes(id)) {
			remove(tempId, item => item === id);
		} else {
			if (type === OPTION_SINGLE_SELECTION) {
				tempId = [id];
			} else {
				tempId.push(id);
			}
		}

		this.setState({ selectedOptions: tempId });
		this.props.onAnswerSelect(tempId);
	};
	render() {
		return (
			<View style={[styles.container, this.props.style]}>
				<PollQuestion
					number={this.props.questionNumber}
					question={this.props.data.question}
					style={styles.pollQuestion}
					isMultiple={this.props.data.type === OPTION_MULTIPLE_SELECTION}
				/>
				{this.props.data.type === OPTION_TEXT_BOX ? (
					<View style={styles.textInputView}>
						<TextInput
							style={styles.textInput}
							onChangeText={text => this.onChangeText(text)}
							value={this.state.answerText}
							multiline={true}
							placeholderTextColor={"#4a4a4a"}
							placeholder="Write message"
						/>
					</View>
				) : (
					this.props.data.choice.map((option, index) => {
						return (
							<PollOption
								key={index}
								option={option.choice}
								isSelected={this.state.selectedOptions.includes(option.id)}
								style={styles.pollOption}
								onPress={() => this.selectedOptions(option.id)}
								isDisabled={this.props.isDisabled}
								isShowPercentage={option.isShowPercentage}
								percentage={option.percentage}
							/>
						);
					})
				)}
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: { paddingVertical: height / 60, backgroundColor: "#fff" },
	pollOption: { marginTop: height / 60 },
	pollQuestion: {
		width: width,
		flexDirection: "row",
		alignItems: "center",
		paddingHorizontal: width / 30,
		paddingVertical: height / 90
	},
	textInput: {
		height: height / 6.5,
		borderColor: "#2C98F0",
		borderWidth: 0.5,
		borderRadius: width / 30,
		padding: 0,
		paddingHorizontal: width / 30,
		paddingVertical: width / 30,
		textAlignVertical: "top"
	},
	textInputView: { marginHorizontal: width / 25 }
});

export default PollQuestionSection;
