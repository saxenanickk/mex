import React from "react";
import { View, Dimensions, StyleSheet } from "react-native";
import { GoappTextBold, GoappTextLight } from "../../../Components/GoappText";
import PropTypes from "prop-types";

const { width, height } = Dimensions.get("window");
const PollQuestion = props => (
	<View style={props.style}>
		<View style={styles.circle}>
			<GoappTextBold color={"#fff"} size={height / 45}>
				{props.number}
			</GoappTextBold>
		</View>
		<GoappTextBold style={styles.textStyle} size={height / 45}>
			{props.question}
			{props.isMultiple ? (
				<GoappTextLight color={"rgba(0,0,0,0.5)"} size={height / 90}>
					{" (Multiple Choice)"}
				</GoappTextLight>
			) : null}
			{props.isMandatory ? (
				<GoappTextBold color={"#ff0000"} size={height / 50}>
					{" * "}
				</GoappTextBold>
			) : null}
		</GoappTextBold>
	</View>
);

const styles = StyleSheet.create({
	container: {
		flexDirection: "row",
		width: width,
		paddingHorizontal: width / 30,
		paddingVertical: height / 90
	},
	textStyle: {
		marginLeft: width / 30,
		width: "75%"
	},
	circle: {
		width: width / 14,
		height: width / 14,
		borderRadius: width / 28,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#2C98F0"
	}
});

PollQuestion.propTypes = {
	/**
	 * serial number of the question
	 */
	number: PropTypes.number.isRequired,
	/**
	 * question to be displayed
	 */
	question: PropTypes.string.isRequired,
	/**
	 * whether question is mandatory or not
	 */
	isMandatory: PropTypes.bool,
	/**
	 * style of question area
	 */
	style: PropTypes.object,
	/**
	 * whether question is multiple selection or not
	 */
	isMultiple: PropTypes.bool
};

PollQuestion.defaultProps = {
	isMandatory: false,
	width: width,
	style: styles.container
};
export default PollQuestion;
