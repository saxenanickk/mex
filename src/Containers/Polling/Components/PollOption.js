import React from "react";
import { View, Dimensions, StyleSheet, TouchableOpacity } from "react-native";
import { GoappTextRegular } from "../../../Components/GoappText";
import PropTypes from "prop-types";

const { width, height } = Dimensions.get("window");

const PollOption = props => {
	const getSelectedPercentageBackground = () => {
		let obj = {
			position: "absolute",
			width: "0%",
			height: "100%",
			borderTopLeftRadius: width / 30,
			borderBottomLeftRadius: width / 30,
			backgroundColor: "rgba(0,0,0,0.2)"
		};
		try {
			obj = { ...obj, width: `${props.percentage}%` };
			if (props.percentage > 97)
				obj = {
					...obj,
					borderTopRightRadius: width / 30,
					borderBottomRightRadius: width / 30
				};
			return obj;
		} catch (error) {
			return obj;
		}
	};
	return (
		<TouchableOpacity
			disabled={props.isDisabled}
			onPress={props.onPress}
			style={[
				styles.container,
				props.style,
				props.isSelected ? styles.selectedContainer : styles.unSelectedContainer
			]}>
			<View style={getSelectedPercentageBackground()} />
			<View style={styles.innerContainer}>
				{props.isShowPercentage ? (
					<GoappTextRegular
						numberOfLines={1}
						style={[
							props.isSelected
								? styles.selectedFirstText
								: styles.unselectedFirstText
						]}>
						{`${props.percentage}%`}
					</GoappTextRegular>
				) : null}
				<GoappTextRegular
					style={[
						props.isSelected
							? styles.selectedSecondText
							: styles.unselectedSecondText,
						props.isShowPercentage
							? styles.emptyMarginLeft
							: styles.unEmptyMarginLeft
					]}>
					{props.option}
				</GoappTextRegular>
			</View>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	container: {
		width: width - width / 15,
		alignSelf: "center",
		flexDirection: "row",
		justifyContent: "space-between",
		borderWidth: 0.5,
		borderRadius: width / 30,
		borderColor: "#2C98F0"
	},
	selectedContainer: {
		backgroundColor: "#2C98F0"
	},
	unSelectedContainer: {
		backgroundColor: "#fff"
	},
	emptyMarginLeft: {
		marginLeft: 0
	},
	unEmptyMarginLeft: {
		marginLeft: width / 30
	},
	innerContainer: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingVertical: height / 60
	},
	selectedFirstText: {
		width: "15%",
		color: "#fff",
		fontSize: height / 55,
		marginLeft: width / 30
	},
	unselectedFirstText: {
		width: "15%",
		color: "#2C98F0",
		fontSize: height / 55,
		marginLeft: width / 30
	},
	selectedSecondText: {
		width: "80%",
		color: "#fff",
		fontSize: height / 55,
		marginRight: width / 30
	},
	unselectedSecondText: {
		width: "80%",
		color: "#2C98F0",
		fontSize: height / 55,
		marginHorizontal: width / 30
	}
});

PollOption.propTypes = {
	/**
	 * style of question area
	 */
	style: PropTypes.object,
	/**
	 * check whether this option is selected or not
	 */
	isSelected: PropTypes.bool,
	/**
	 * if need to show percentage
	 */
	isShowPercentage: PropTypes.bool,
	/**
	 * percentage polled on a particular option
	 */
	percentage: PropTypes.number,
	/**
	 * option information
	 */
	option: PropTypes.string.isRequired,
	/**
	 * action performed when pressed the option
	 */
	onPress: PropTypes.func,
	/**
	 * disable the option
	 */
	isDisabled: PropTypes.bool
};

PollOption.defaultProps = {
	isSelected: false,
	isShowPercentage: false,
	percentage: 0,
	style: null,
	onPress: () => console.log("option pressed"),
	isDisabled: false
};
export default PollOption;
