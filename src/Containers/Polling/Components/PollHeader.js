import React from "react";
import { View, Dimensions, TouchableOpacity, StyleSheet } from "react-native";
import { Icon } from "../../../Components";
import { GoappTextMedium } from "../../../Components/GoappText";
const { width, height } = Dimensions.get("window");

const PollHeader = props => (
	<View style={styles.container}>
		<TouchableOpacity
			style={styles.touchableOpacityBack}
			onPress={props.onPress}>
			<Icon
				iconType={"ionicon"}
				iconName={"ios-arrow-round-back"}
				iconSize={height / 20}
				iconColor={"#000000"}
			/>
		</TouchableOpacity>
		<GoappTextMedium style={styles.title}>{props.title}</GoappTextMedium>
	</View>
);

const styles = StyleSheet.create({
	container: {
		flexDirection: "row",
		backgroundColor: "#fff",
		alignItems: "center",
		paddingVertical: width / 50,
		width: width,
		paddingHorizontal: width / 30
	},
	touchableOpacityBack: {
		width: width / 10,
		height: height / 16,
		justifyContent: "center"
	},
	title: {
		width: width / 1.3,
		fontSize: height / 40,
		marginLeft: width / 30,
		justifyContent: "center"
	}
});
export default PollHeader;
