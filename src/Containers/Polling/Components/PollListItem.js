import React from "react";
import { View, Dimensions, StyleSheet, TouchableOpacity } from "react-native";
import { GoappTextLight, GoappTextMedium } from "../../../Components/GoappText";
import { Icon } from "../../../Components";
import I18n from "../../../Assets/strings/i18n";
import PropTypes from "prop-types";

const { width, height } = Dimensions.get("window");
const PollListItem = props => (
	<TouchableOpacity
		style={[styles.container, props.style]}
		onPress={props.onPress}>
		<View style={styles.innerTopContainer}>
			<View style={styles.iconBox}>
				<Icon
					iconType={"icomoon"}
					iconName={"poll"}
					iconColor={"#2C98F0"}
					iconSize={height / 26}
				/>
			</View>
			<GoappTextMedium style={styles.titleText}>{props.title}</GoappTextMedium>
		</View>
		<View style={styles.innerBottomContainer}>
			<GoappTextLight style={styles.completedText}>
				{props.isCompleted ? "Completed" : ""}
			</GoappTextLight>
			<View style={styles.bottomLeft}>
				{props.questions > 0 ? (
					<GoappTextLight style={styles.questionText}>
						{`${props.questions} ${I18n.t("question", {
							count: props.questions
						})}`}
					</GoappTextLight>
				) : null}
				<Icon
					iconType={"ionicon"}
					iconName={"ios-arrow-round-forward"}
					iconColor={"#2C98F0"}
					iconSize={height / 27}
				/>
			</View>
		</View>
	</TouchableOpacity>
);

const styles = StyleSheet.create({
	container: {
		width: width,
		backgroundColor: "#fff",
		paddingHorizontal: width / 30,
		paddingVertical: height / 60,
		alignSelf: "center",
		alignItems: "center",
		borderWidth: 1,
		borderColor: "#e9e9e9"
	},
	innerTopContainer: {
		color: "#fff",
		flexDirection: "row",
		alignSelf: "center",
		alignItems: "center"
	},
	iconBox: {
		width: width / 8.5,
		paddingVertical: 7,
		borderWidth: 1,
		borderRadius: width / 90,
		borderColor: "#e9e9e9",
		justifyContent: "center",
		alignItems: "center"
	},
	titleText: {
		width: "80%",
		fontSize: height / 50,
		marginLeft: width / 30
	},
	innerBottomContainer: {
		width: "98%",
		color: "#fff",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between"
	},
	completedText: {
		fontSize: height / 70,
		color: "rgba(0,0,0,0.5)",
		marginLeft: width / 6
	},
	bottomLeft: {
		flexDirection: "row",
		alignItems: "center"
	},
	questionText: {
		fontSize: height / 70,
		color: "#2C98F0",
		marginRight: width / 30
	}
});

PollListItem.propTypes = {
	/**
	 * whether this is a poll or quiz
	 */
	isPoll: PropTypes.bool,
	/**
	 * title for the list section
	 */
	title: PropTypes.string.isRequired,
	/**
	 * whether poll or quiz is completed or not
	 */
	isCompleted: PropTypes.bool,
	/**
	 * style given to the root component
	 */
	style: PropTypes.object,
	/**
	 * action performed when clicked on the list item
	 */
	onPress: PropTypes.func,
	/**
	 * number of qustions in a poll
	 */
	questions: PropTypes.number
};

PollListItem.defaultProps = {
	isPoll: true,
	isCompleted: false,
	style: null,
	questions: 0,
	onPress: () => console.log("item pressed")
};
export default PollListItem;
