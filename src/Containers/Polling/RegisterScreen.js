import React from "react";
import { Stack } from "../../utils/Navigators";
import Home from "./Containers/Home";
import PollDetail from "./Containers/PollDetail";

const RegisterScreen = () => (
	<Stack.Navigator headerMode={"none"}>
		<Stack.Screen name={"Home"} component={Home} />
		<Stack.Screen name={"PollDetail"} component={PollDetail} />
	</Stack.Navigator>
);

export default RegisterScreen;
