import React, { Fragment } from "react";
import { SafeAreaView, StatusBar, StyleSheet } from "react-native";
import RegisterScreen from "./RegisterScreen";
import GoAppAnalytics from "../../utils/GoAppAnalytics";

export default class App extends React.Component {
	render() {
		return (
			<Fragment>
				{/** For the Top Color (Under the Notch) */}
				<SafeAreaView style={styles.container} />
				{/** For the Bottom Color (Below the home touch) */}
				<SafeAreaView style={styles.fullFlex}>
					<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
					<RegisterScreen
						nav={this.props.nav}
						onNavigationStateChange={(prevState, currState) =>
							GoAppAnalytics.logChangeOfScreenInStack(
								"polling",
								prevState,
								currState
							)
						}
					/>
				</SafeAreaView>
			</Fragment>
		);
	}
}

const styles = StyleSheet.create({
	container: { flex: 0, backgroundColor: "#ffffff" },
	fullFlex: { flex: 1 }
});
