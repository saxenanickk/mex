import { combineReducers } from "redux";
import { reducer as homeReducer } from "./Containers/Home/Reducer";
import { reducer as pollDetailReducer } from "./Containers/PollDetail/Reducer";
import { POLLING_SAVE_ERROR } from "./Saga";

const initialState = {
	error: null
};

const mainReducer = (state = initialState, action) => {
	switch (action.type) {
		case POLLING_SAVE_ERROR:
			return {
				...state,
				error: action.payload
			};
		default:
			return state;
	}
};

const pollingReducer = combineReducers({
	mainReducer,
	homeReducer,
	pollDetailReducer
});

export default pollingReducer;
