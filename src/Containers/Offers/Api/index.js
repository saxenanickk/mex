// @ts-check
import Config from "react-native-config";

const { SERVER_BASE_URL_OFFERS, SERVER_BASE_URL_SITES } = Config;

/**
 * API methods for offers
 */
class OffersApi {
	constructor() {
		console.log("Offers Api Instantiated.");
	}

	/**
	 *
	 * @param {Object} params
	 * @param {String} params.appToken
	 * @param {String} params.siteId
	 */
	getSections(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(
					`${SERVER_BASE_URL_OFFERS}/getSections?site_id=${params.siteId}`,
					{
						method: "GET",
						headers: {
							"Content-Type": "application/json",
							"X-ACCESS-TOKEN": params.appToken
						}
					}
				)
					.then(response => {
						response
							.json()
							.then(res => {
								if (res.success === 1) {
									resolve(res.data);
								} else {
									reject("err");
								}
							})
							.catch(err => reject(err));
					})
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * @param {{ siteId: String; appToken: string; }} params
	 */
	getAllCategories(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(
					`${SERVER_BASE_URL_OFFERS}/getCategoriesBySite?site_id=${
						params.siteId
					}`,
					{
						method: "GET",
						headers: {
							"Content-Type": "application/json",
							"X-ACCESS-TOKEN": params.appToken
						}
					}
				)
					.then(response => {
						response
							.json()
							.then(res => {
								if (res.success === 1) {
									resolve(res.data);
								} else {
									reject(res.message);
								}
							})
							.catch(err => reject(err));
					})
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * Get All Offers For a Site
	 * @param {Object} params
	 * @param {String} params.appToken X-ACCESS-TOKEN
	 * @param {String} params.siteId User Site Id from Get All Sites
	 * @param {String} params.categories Specific category name
	 * @param {String} params.brand_name Speacific brand name
	 */
	getOffers(params) {
		let url = `${SERVER_BASE_URL_OFFERS}/getOffers?site_id=${params.siteId}`;
		if (params.categories) {
			url = `${url}&categories=${encodeURIComponent(params.categories)}`;
		}
		if (params.brand_name) {
			url = `${url}&brand_name=${encodeURIComponent(params.brand_name)}`;
		}
		return new Promise((resolve, reject) => {
			try {
				fetch(url, {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						response
							.json()
							.then(res => {
								console.log(res);
								if (res.success === 1) {
									resolve(res.data);
								} else {
									reject(res.message);
								}
							})
							.catch(err => reject(err));
					})
					.catch(err => reject(err));
			} catch (error) {
				console.log("error", error);
				reject(error);
			}
		});
	}

	/**
	 * Get An Offer by Offer ID
	 * @param {Object} params
	 * @param {String} params.appToken X-ACCESS-TOKEN
	 * @param {String} params.id Offer Id
	 */
	getOfferById(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_OFFERS}/getOfferById?id=${params.id}`, {
					method: "GET",
					headers: {
						"Content-Type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						response
							.json()
							.then(res => {
								if (res.success === 1) {
									resolve(res.data);
								} else {
									reject("error");
								}
							})
							.catch(err => reject(err));
					})
					.catch(err => reject(err));
			} catch (error) {
				console.log("error", error);
				reject(error);
			}
		});
	}

	/**
	 * @param {Object} params
	 * @param {String} params.appToken
	 * @param {String} params.siteId
	 * @param {String} params.offerId
	 */
	getOfferCode(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(
					`${SERVER_BASE_URL_OFFERS}/getCode?site_id=${
						params.siteId
					}&offer_id=${params.offerId}`,
					{
						method: "GET",
						headers: {
							"Content-Type": "application/json",
							"X-ACCESS-TOKEN": params.appToken
						}
					}
				)
					.then(response => {
						response
							.json()
							.then(res => {
								if (res.success === 1) {
									resolve(res.data);
								} else {
									reject(res.message);
								}
							})
							.catch(e => reject(e));
					})
					.catch(e => reject(e));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * @param {Object} params
	 * @param {String} params.appToken X-ACCESS-TOKEN
	 * @param {{site_id: String; keyword: String;}} params.data Site Id and Search String
	 */
	searchOffers(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_OFFERS}/searchOffers`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					},
					body: JSON.stringify(params.data)
				})
					.then(response => {
						response
							.json()
							.then(res => {
								console.log(res);
								if (res.success === 1) {
									resolve(res.data);
								} else {
									reject("error");
								}
							})
							.catch(err => reject(err));
					})
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 *
	 * @param {Object} params
	 * @param {String} params.appToken X-ACCESS-TOKEN
	 * @param {String} params.redemptionId
	 */
	getSms(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(
					`${SERVER_BASE_URL_OFFERS}/getSms?redemption_id=${
						params.redemptionId
					}`,
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": params.appToken
						}
					}
				)
					.then(response => {
						response
							.json()
							.then(res => {
								if (res.success === 1) {
									resolve(res);
								} else {
									reject(res);
								}
							})
							.catch(err => reject(err));
					})
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * @param {{ appToken: string; }} params
	 */
	getAllSites(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_SITES + "/getSites", {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response", response);
						response
							.json()
							.then(res => {
								console.log("response of get sites is", res);
								if (res.success === 1) {
									resolve(res.data);
								} else {
									reject("Error");
								}
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}
}

export default new OffersApi();
