import React from "react";
import { Stack } from "../../utils/Navigators";
import Splash from "./Containers/Splash";
import CategorizedOffers from "./Containers/CategorizedOffers";
import Main from "./Containers/Main";
import OfferDetail from "./Containers/OfferDetail";

let params = null;

const AppNavigator = () => (
	<Stack.Navigator headerMode={"none"}>
		<Stack.Screen
			name={"Splash"}
			component={props => <Splash params={params} {...props} />}
		/>
		<Stack.Screen name={"CategorizedOffers"} component={CategorizedOffers} />
		<Stack.Screen name={"Main"} component={Main} />
		<Stack.Screen name={"OfferDetail"} component={OfferDetail} />
	</Stack.Navigator>
);

const RegisterScreen = props => {
	params = props.route.params;
	return (
		<AppNavigator
			onNavigationStateChange={(prevState, currState) =>
				props.onNavigationStateChange(prevState, currState)
			}
		/>
	);
};

export default RegisterScreen;
