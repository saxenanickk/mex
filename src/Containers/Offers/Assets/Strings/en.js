import * as I18n from "../../../../Assets/strings/i18n";

export default {
	...I18n.default.translations.en,
	offers: "Offers",
	offer: "Offer",
	no_offers_found: "No offers found",
	offers_search_message:
		"Type above to search partners, exclusive offers & brands(Minimum 3 characters).",
	no_offers: "No offers",
	oops: "Oops!",
	offer_error_message: "Something went wrong, please try again.",
	Oops: "Oops!",
	maximum_offer_exceed:
		"You have exceeded the maximum number of redemption available for this offer.",
	something_wrong: "Something went wrong, please try again.",
	no_code_required: "NO CODE REQUIRED",
	copy_code: "Copy Code",
	launch_app: "Launch App",
	brand_link: "Brand Link",
	get_sms: "Get SMS",
	brand_link_to_avail_this_offer:
		"Please go to the Brand Link to avail this offer.",
	mex_partner_app_please_avail_inside_mex:
		"This is MEX. partner app. Please avail inside the MEX. marketplace.",
	code_at_partner_outlet_to_avail_offer:
		"Show this code at Partners Outlet to avail this offer.",
	go_to_partner_outlet_to_avail_offer:
		"Go to Partners Outlet to avail this offer.",
	unable_to_get_trending_offers: "Unable to get Trending Offers",
	unable_to_get_offers: "Unable to get Offers",
	hang_on: "Hang On...",
	loading_offers_for_you: "Loading Offers for you",
	user_redemption_limit_exceeded:
		"You have claimed the maximum number of offers already.",
	total_redemption_limit_exceeded:
		"Total number of offer redemption is exceeded.",
	offer_expired: "Offer Expired.",
	searching_for_offers: "Searching for offers",
	search_for_partners_offers: "Search for partners, offers..."
};
