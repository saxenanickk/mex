const NOOFFER = require("./group.png");
const ARROWDOWN = require("./arrow_down.png");
const ARROWUP = require("./arrow_up.png");
const ERRORCLOUD = require("./error_cloud.png");
const NONUMBER = require("./no_number.png");
export { NOOFFER, ARROWDOWN, ARROWUP, ERRORCLOUD, NONUMBER };
