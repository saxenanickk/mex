import { all } from "redux-saga/effects";
// Import different Sagas here
import { offersDetailSaga } from "./Containers/OfferDetail/Saga";
import { offersMainSaga } from "./Containers/Main/Saga";
import { offersListSaga } from "./Containers/CategorizedOffers/Saga";
import { offersSearchSaga } from "./Containers/SearchOffer/Saga";

export const OFFERS_SAVE_ERROR = "OFFERS_SAVE_ERROR";

export const offersSaveError = payload => ({
	type: OFFERS_SAVE_ERROR,
	payload
});

export default function* offersSaga() {
	yield all([
		offersMainSaga(),
		offersDetailSaga(),
		offersListSaga(),
		offersSearchSaga()
	]);
}
