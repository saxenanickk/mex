import React, { Component, Fragment } from "react";
import { StatusBar } from "react-native";
import RegisterScreen from "./RegisterScreen";
import GoAppAnalytics from "../../utils/GoAppAnalytics";
import { OffersAlert } from "./Components";
import { AlertContext } from "./AlertContext";
import { SafeAreaView } from "react-native-safe-area-context";

class App extends Component {
	alertRef = React.createRef();
	render() {
		return (
			<Fragment>
				<SafeAreaView style={{ flex: 1, backgroundColor: "#ffffff" }}>
					<AlertContext.Provider value={this.alertRef}>
						<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
						<RegisterScreen
							{...this.props}
							nav={this.props.nav}
							onNavigationStateChange={(prevState, currState) =>
								GoAppAnalytics.logChangeOfScreenInStack(
									"offers",
									prevState,
									currState
								)
							}
						/>
					</AlertContext.Provider>
					<OffersAlert ref={this.alertRef} />
				</SafeAreaView>
			</Fragment>
		);
	}
}

export default App;
