import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
	flatListImage: {
		width: width / 2.4,
		marginTop: height / 40,
		justifyContent: "flex-end",
		aspectRatio: 76 / 100,
		borderRadius: 10
	},
	flatListBrandName: {
		color: "#FFFFFF",
		margin: width / 45,
		fontSize: height / 65,
		position: "absolute",
		bottom: width / 14
	},
	flatListText: {
		color: "#FFFFFF",
		marginHorizontal: width / 45,
		marginBottom: width / 45,
		fontSize: height / 60,
		position: "absolute",
		bottom: width / 45
	},
	flatList: { marginBottom: height / 40 },
	imageIcon: {
		width: width / 16,
		height: width / 16,
		margin: width / 73
	},
	flatListLinearGradient: {
		position: "absolute",
		bottom: 0,
		width: "100%",
		color: "#fff",
		height: width / 3.5,
		borderRadius: 10
	},
	bottomText: {
		position: "absolute",
		bottom: 0,
		paddingBottom: height / 100,
		paddingHorizontal: width / 50
	}
});
