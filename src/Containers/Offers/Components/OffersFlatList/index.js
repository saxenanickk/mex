import React from "react";
import {
	View,
	FlatList,
	ImageBackground,
	TouchableOpacity,
	Dimensions
} from "react-native";
import { connect } from "react-redux";
import { OffersTextRegular } from "../../Components";
import styles from "./styles";
const { width, height } = Dimensions.get("window");
import LinearGradient from "react-native-linear-gradient";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";

class OffersFlatList extends React.Component {
	render() {
		return (
			<FlatList
				style={styles.flatList}
				data={this.props.data}
				keyExtractor={(item, index) => index.toString()}
				numColumns={2}
				showsVerticalScrollIndicator={false}
				extraData={this.props.data}
				renderItem={({ item, index }) => (
					<View style={{ flex: 1, flexDirection: "row" }}>
						<TouchableOpacity
							onPress={() => {
								this.props.navigation.navigate("OfferDetail", {
									id: item.id
								});
								GoAppAnalytics.trackWithProperties("offer_open", {
									id: item.id,
									name: item.brand_name,
									src: "category_screen"
								});
							}}>
							<ImageBackground
								source={{ uri: item.listing_banner_image }}
								style={[
									styles.flatListImage,
									{
										marginLeft: index % 2 ? width / 34 : 0
									}
								]}
								imageStyle={{
									borderRadius: 10
								}}>
								<LinearGradient
									colors={["rgba(0,0,0,0)", "#000000"]}
									style={styles.flatListLinearGradient}>
									<View style={styles.bottomText}>
										<OffersTextRegular
											size={height / 60}
											color={"#fff"}
											numberOfLines={1}>
											{item.brand_name}
										</OffersTextRegular>
										<OffersTextRegular
											size={height / 55}
											color={"#fff"}
											numberOfLines={1}>
											{item.text_1}
										</OffersTextRegular>
									</View>
								</LinearGradient>
							</ImageBackground>
						</TouchableOpacity>
					</View>
				)}
			/>
		);
	}
}

export default connect()(OffersFlatList);
