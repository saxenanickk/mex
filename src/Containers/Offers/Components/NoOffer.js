import React from "react";
import { View, Image, Dimensions, TouchableOpacity } from "react-native";
import { NOOFFER } from "../Assets/Img";
import { OffersTextBold, OffersTextRegular } from "./OffersText";
const { width, height } = Dimensions.get("window");

export const NoOffer = props => (
	<React.Fragment>
		<Image source={NOOFFER} style={props.imageStyle} resizeMode={"contain"} />
		<View>
			<OffersTextBold
				style={{
					fontSize: height / 40,
					marginTop: 25,
					color: "#808080"
				}}>
				{"Oops... No Offers Found"}
			</OffersTextBold>
		</View>
		<View>
			<OffersTextRegular
				style={{
					fontSize: height / 45,
					color: "#b5b5b4",
					marginTop: 15,
					marginLeft: "15%",
					marginRight: "15%",
					textAlign: "center"
				}}>
				{"The offers you're looking for does not seem to exist."}
			</OffersTextRegular>
		</View>
		{props.onRefresh && (
			<TouchableOpacity
				onPress={() => props.onRefresh()}
				style={{
					marginTop: height / 60,
					backgroundColor: "#14314c",
					width: width / 2,
					paddingVertical: height / 90,
					borderRadius: width / 60
				}}>
				<OffersTextRegular
					style={{
						fontSize: height / 45,
						color: "#fff",
						textAlign: "center"
					}}>
					{"REFRESH"}
				</OffersTextRegular>
			</TouchableOpacity>
		)}
	</React.Fragment>
);
