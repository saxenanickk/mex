import React, { Component } from "react";
import {
	Modal,
	View,
	Dimensions,
	TouchableOpacity,
	Image,
	Platform
} from "react-native";
import { Icon } from "../../../Components";
import { OffersTextBold, OffersTextMedium } from "../Components";
import { connect } from "react-redux";
import { ERRORCLOUD } from "../Assets/Img";

const INFORMATION = "Information";
const { width, height } = Dimensions.get("window");
class OffersAlert extends Component {
	constructor(props) {
		super(props);
		this.state = {
			open: false,
			title: "",
			type: "",
			message: "",
			icon: null,
			cancelAction: null,
			okAction: null
		};
	}

	setOpen = value => this.setState({ open: value });
	setTitle = value => this.setState({ title: value });
	setType = value => this.setState({ type: value });
	setIcon = value => this.setState({ icon: value });
	setMessage = value => this.setState({ message: value });
	setCancelAction = value => this.setState({ cancelAction: value });
	setOkAction = value => this.setState({ okAction: value });

	handleClose = () => {
		this.setCancelAction(null);
		this.setOkAction(null);
		this.setOpen(false);
		this.setIcon(null);
	};

	handleCancelClick = () => {
		const { cancelAction } = this.state;
		if (cancelAction && cancelAction.onPress) {
			cancelAction.onPress();
		}
		this.handleClose();
	};

	handleOkClick = () => {
		const { okAction } = this.state;
		if (okAction && okAction.onPress) {
			okAction.onPress();
		}
		this.handleClose();
	};

	openAlert(title = INFORMATION, message = "", actions = [], icon = null) {
		if (actions.length >= 2) {
			actions.map((item, index) => {
				if (index === 0) {
					this.setCancelAction(item);
				} else if (index === 1) {
					this.setOkAction(item);
				}
			});
		} else if (actions.length === 1) {
			this.setOkAction(actions[0]);
		}
		this.setTitle(title);
		this.setIcon(icon);
		this.setMessage(message);
		this.setOpen(true);
	}

	render() {
		const { open, title, message, cancelAction, okAction, icon } = this.state;
		const { handleClose } = this;
		console.log("icon is", icon);
		return (
			<Modal visible={open} onRequestClose={handleClose} transparent={true}>
				<View
					style={{
						flex: 1,
						justifyContent: "center",
						alignItems: "center",
						backgroundColor: "rgba(0,0,0,0.5)"
					}}>
					<View
						style={{
							width: width / 1.29,
							paddingVertical: height / 49,
							paddingHorizontal: width / 25,
							borderRadius: width / 50,
							backgroundColor: "#fff"
						}}>
						<TouchableOpacity
							style={{
								alignSelf: "flex-end",
								padding: width / 30
							}}
							onPress={handleClose}>
							<Icon
								iconType={"ionicon"}
								iconName={"ios-close"}
								iconSize={height / 24}
								iconColor={"#000000"}
							/>
						</TouchableOpacity>
						<Image
							source={icon !== null ? icon : ERRORCLOUD}
							style={{
								width: width / 2.95,
								height: height / 15.68,
								alignSelf: "center",
								marginBottom: height / 90
							}}
							resizeMode={"contain"}
						/>
						<OffersTextBold
							size={height / 55}
							numberOfLines={1}
							style={{
								width: width / 1.83,
								alignSelf: "center",
								textAlign: "center"
							}}>
							{title}
						</OffersTextBold>
						<OffersTextMedium
							size={height / 55}
							style={{
								width: width / 1.83,
								alignSelf: "center",
								textAlign: "center",
								color: "#4A4A4A"
							}}>
							{message}
						</OffersTextMedium>
						<View
							style={{
								width: width / 1.83,
								marginTop: height / 32,
								flexDirection: "row",
								alignSelf: "center",
								justifyContent: cancelAction ? "space-between" : "center"
							}}>
							{cancelAction && (
								<TouchableOpacity
									onPress={this.handleCancelClick}
									style={{
										backgroundColor: "#15314C",
										width: width / 4.1,
										height: height / 23.7,
										justifyContent: "center",
										alignItems: "center",
										borderRadius: width / 30,
										...Platform.select({
											android: { elevation: 5 },
											ios: {
												shadowOffset: { width: 1, height: 1 },
												shadowColor: "grey",
												shadowOpacity: 0.2
											}
										})
									}}>
									<OffersTextMedium
										size={height / 60}
										style={{
											color: "#fff"
										}}
										numberOfLines={1}>
										{cancelAction.text}
									</OffersTextMedium>
								</TouchableOpacity>
							)}
							<TouchableOpacity
								onPress={this.handleOkClick}
								style={{
									backgroundColor: "#15314C",
									width: cancelAction ? width / 4.1 : width / 2,
									height: height / 23.7,
									justifyContent: "center",
									alignItems: "center",
									borderRadius: width / 30,
									...Platform.select({
										android: { elevation: 5 },
										ios: {
											shadowOffset: { width: 1, height: 1 },
											shadowColor: "grey",
											shadowOpacity: 0.2
										}
									})
								}}>
								<OffersTextMedium
									size={height / 60}
									style={{
										color: "#fff"
									}}
									numberOfLines={1}>
									{okAction ? okAction.text : "OK"}
								</OffersTextMedium>
							</TouchableOpacity>
						</View>
					</View>
				</View>
			</Modal>
		);
	}
}

export default connect(
	null,
	null,
	null,
	{ forwardRef: true }
)(OffersAlert);
