import React, { FunctionComponent } from "react";
import {
	Text,
	Platform,
	TextInput,
	TextProps,
	StyleProp,
	TextStyle
} from "react-native";
import {
	font_one,
	font_two,
	font_three,
	font_four
} from "../../../Assets/styles";

/**
 * @typedef {Object} OffersTextProps
 * @property {StyleProp<TextStyle>} [style]
 * @property {Number} [size]
 * @property {String} [color]
 */

/**
 * @type {FunctionComponent<OffersTextProps & TextProps>}
 */
export const OffersTextLight = ({
	size,
	color,
	style,
	children,
	...restProps
}) => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_one,
				fontWeight: Platform.OS === "ios" ? "300" : undefined,
				fontSize: size,
				color: color
			},
			style
		]}>
		{children}
	</Text>
);

/**
 * @type {FunctionComponent<TextProps & {size?: Number, color?: String, style?: StyleProp<TextStyle>}>}
 */
export const OffersTextRegular = props => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_two,
				fontWeight: Platform.OS === "ios" ? "400" : undefined,
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}>
		{props.children}
	</Text>
);

/**
 * @type {FunctionComponent<OffersTextProps & TextProps>}
 */
export const OffersTextMedium = props => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_three,
				fontWeight: Platform.OS === "ios" ? "500" : undefined,
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}>
		{props.children}
	</Text>
);

/**
 * @type {FunctionComponent<OffersTextProps & TextProps>} props
 */
export const OffersTextBold = props => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_four,
				fontWeight: Platform.OS === "ios" ? "700" : undefined,
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

export const OffersTextInputRegular = ({
	size,
	color,
	style,
	placeholder,
	value,
	onChangeText,
	multiline,
	numberOfLines,
	ellipsizeMode,
	editable,
	...restProps
}) => (
	<TextInput
		{...restProps}
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_two,
				fontWeight: Platform.OS === "ios" ? "400" : undefined,
				fontSize: size,
				color: color,
				margin: 0,
				padding: 0
			},
			style
		]}
		underlineColorAndroid={"transparent"}
		placeholder={placeholder}
		value={value}
		onChangeText={onChangeText}
		multiline={multiline}
		numberOfLines={numberOfLines}
		ellipsizeMode={ellipsizeMode}
		editable={editable}
	/>
);

export const OffersTextInputMedium = ({
	size,
	color,
	style,
	placeholder,
	value,
	onChangeText,
	multiline,
	numberOfLines,
	ellipsizeMode,
	editable,
	...restProps
}) => (
	<TextInput
		{...restProps}
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_three,
				fontWeight: Platform.OS === "ios" ? "500" : undefined,
				fontSize: size,
				color: color,
				margin: 0,
				padding: 0
			},
			style
		]}
		underlineColorAndroid={"transparent"}
		placeholder={placeholder}
		value={value}
		onChangeText={onChangeText}
		multiline={multiline}
		numberOfLines={numberOfLines}
		ellipsizeMode={ellipsizeMode}
		editable={editable}
	/>
);

export const OffersTextInputBold = ({
	size,
	color,
	style,
	placeholder,
	value,
	onChangeText,
	multiline,
	numberOfLines,
	ellipsizeMode,
	editable,
	...restProps
}) => (
	<TextInput
		{...restProps}
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_four,
				fontWeight: Platform.OS === "ios" ? "700" : undefined,
				fontSize: size,
				color: color,
				margin: 0,
				padding: 0
			},
			style
		]}
		underlineColorAndroid={"transparent"}
		placeholder={placeholder}
		value={value}
		onChangeText={onChangeText}
		multiline={multiline}
		numberOfLines={numberOfLines}
		ellipsizeMode={ellipsizeMode}
		editable={editable}
	/>
);

const defaultProps = {
	size: 14,
	color: "#000",
	editable: true
};

OffersTextBold.defaultProps = defaultProps;
OffersTextLight.defaultProps = defaultProps;
OffersTextMedium.defaultProps = defaultProps;
OffersTextRegular.defaultProps = defaultProps;
