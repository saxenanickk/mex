import { takeLatest, put, call } from "redux-saga/effects";
import Api from "../../Api";

export const OFFERS_GET_OFFER_DETAIL = "GET_OFFER_DETAIL";
export const OFFERS_SAVE_OFFER_DETAIL_SUCCESS = "GET_OFFER_DETAIL_SUCCESS";
export const OFFERS_SAVE_OFFER_DETAIL_ERROR = "GET_OFFER_DETAIL_ERROR";
export const OFFERS_GET_OFFER_CODE = "OFFERS_GET_OFFER_CODE";
export const OFFERS_SAVE_OFFER_CODE_SUCCESS = "OFFERS_SAVE_OFFER_CODE_SUCCESS";
export const OFFERS_SAVE_OFFER_CODE_ERROR = "OFFERS_SAVE_OFFER_CODE_ERROR";

export const offersGetOfferDetail = payload => ({
	type: OFFERS_GET_OFFER_DETAIL,
	payload
});

export const offersSaveOfferDetailSuccess = payload => ({
	type: OFFERS_SAVE_OFFER_DETAIL_SUCCESS,
	payload
});

export const offersSaveOfferDetailError = payload => ({
	type: OFFERS_SAVE_OFFER_DETAIL_ERROR,
	payload
});

export const offersGetOfferCode = payload => ({
	type: OFFERS_GET_OFFER_CODE,
	payload
});

export const offersSaveOfferCodeSuccess = payload => ({
	type: OFFERS_SAVE_OFFER_CODE_SUCCESS,
	payload
});

export const offersSaveOfferCodeError = payload => ({
	type: OFFERS_SAVE_OFFER_CODE_ERROR,
	payload
});

export function* offersDetailSaga(dispatch) {
	yield takeLatest(OFFERS_GET_OFFER_DETAIL, handleGetOfferDetail);
	yield takeLatest(OFFERS_GET_OFFER_CODE, handleGetOfferCode);
}

function* handleGetOfferDetail(action) {
	try {
		const data = yield call(Api.getOfferById, action.payload);
		yield put(offersSaveOfferDetailSuccess(data));
	} catch (error) {
		yield put(offersSaveOfferDetailError(true));
	}
}

function* handleGetOfferCode(action) {
	try {
		const data = yield call(Api.getOfferCode, action.payload);
		console.log("data is", data);
		yield put(offersSaveOfferCodeSuccess(data));
	} catch (error) {
		console.log("error is", error);
		yield put(offersSaveOfferCodeError(true));
	}
}
