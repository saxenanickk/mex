import { StyleSheet, Dimensions, Platform } from "react-native";
const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	loadingContainer: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center"
	},
	container: {
		flex: 1
	},
	header: {
		width: width,
		paddingHorizontal: width / 18.75
	},
	backButton: {
		width: width / 4,
		paddingVertical: height / 60
	},
	mainContainer: {
		justifyContent: "center",
		alignItems: "center",
		height: height - height / 10,
		width: width
	},
	backButtonStyle: {
		width: 24 * 2.5,
		alignItems: "center",
		justifyContent: "center"
	}
});
