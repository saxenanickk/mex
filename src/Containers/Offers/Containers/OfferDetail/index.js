import React, { Component } from "react";
import {
	View,
	Image,
	Dimensions,
	TouchableOpacity,
	Animated,
	TouchableHighlight,
	Linking,
	StyleSheet,
	Platform,
	BackHandler,
	ImageBackground,
	SafeAreaView
} from "react-native";
import { connect } from "react-redux";
import { memoize } from "lodash";
import getRNDraftJSBlocks from "react-native-draftjs-render";
import { Icon, ProgressScreen } from "../../../../Components";
import {
	OffersTextBold,
	OffersTextMedium,
	OffersTextRegular
} from "../../Components";
import { scale } from "../../../../utils/scaling";
import {
	offersGetOfferDetail,
	offersSaveOfferDetailSuccess,
	offersSaveOfferDetailError
} from "./Saga";
import BottomOffer from "../BottomOffer";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import { AlertContext } from "../../AlertContext";
import { styles } from "./style";
import { ARROWUP, ARROWDOWN } from "../../Assets/Img/index";
import { CommonActions } from "@react-navigation/native";

const TouchableOpacityAnimated = Animated.createAnimatedComponent(
	TouchableOpacity
);

const TouchableHighlightAnimated = Animated.createAnimatedComponent(
	TouchableHighlight
);

const ImageBackgroundAnimated = Animated.createAnimatedComponent(
	ImageBackground
);

const { width, height } = Dimensions.get("window");

const IMAGE_MAX_HEIGHT = (width * 100) / 63;
const IMAGE_MIN_HEIGHT = IMAGE_MAX_HEIGHT - IMAGE_MAX_HEIGHT / 3.59;

/**
 * @typedef {Object} Props
 * @property {string} brandName
 * @property {string} text1
 * @property {string} text2
 * @property {string} expiryDate
 * @property {string} description
 * @property {string} appToken
 * @property {{isOfferDetailLoading: Boolean; offerDetails: (Object | null); isOfferDetailError: Boolean}} offerDetails
 * @property {NavigationScreenProp} navigation
 * @property {Dispatch<Action>} dispatch
 */

/**
 * @export
 * @class OfferDetail
 * @extends {Component<Props>}
 */
class OfferDetail extends Component {
	constructor(props) {
		super(props);
		this._swipeY = new Animated.Value(0);
		this._swipeDirection = null;
		this._scrollOffset = 0;
		this.imageMaxHeight = 0;
		this.prevY = 0;
		this.scrollRef = React.createRef();
		this.state = {
			showOffer: false,
			scrollY: new Animated.Value(0)
		};
		this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
		this.backHandler = BackHandler.addEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
	}

	componentDidMount() {
		const { route } = this.props;
		const { id } = route.params ? route.params : {};

		let offerId = id;

		if (typeof offerId === "string") {
			this.props.dispatch(offersSaveOfferDetailSuccess(null));
			this.props.dispatch(
				offersGetOfferDetail({
					id: offerId,
					appToken: this.props.appToken
				})
			);
		}
	}

	/**
	 * Back Button Handler
	 */
	handleBackButtonClick() {
		if (this._scrollOffset + 50 >= IMAGE_MIN_HEIGHT) {
			this.scrollToTop();
		} else {
			this.goBack();
		}
		return true;
	}

	/**
	 *
	 * @param {Props} prevProps
	 */
	componentDidUpdate(prevProps) {
		if (
			this.props.offerDetails.offerDetails !== null &&
			prevProps.offerDetails.offerDetails === null
		) {
			GoAppAnalytics.trackWithProperties("view_offer", {
				name: this.props.offerDetails.offerDetails.brand_name
			});
		}
	}

	getExpiresIn = memoize((endDate, textOpacity) => {
		let now = new Date(new Date().toDateString()).getTime();
		let endTime =
			new Date(new Date(endDate).toDateString()).getTime() + 86400000;
		let diff = Math.floor((endTime - now) / 86400000);
		if (diff === 1) {
			return this.getAnimatedMixedText("Expires", "Today", textOpacity);
		}

		if (diff === 0) {
			return this.getAnimatedMixedText("Expires in", "few hours", textOpacity);
		}

		if (diff < 0) {
			return this.getAnimatedMixedText("Expired", "", textOpacity);
		}

		return this.getAnimatedMixedText("Expires in", `${diff} Days`, textOpacity);
	});

	getAnimatedMixedText = (text1, text2, textOpacity) => {
		return (
			<Animated.Text
				style={{
					color: "#fff",
					position: "absolute",
					top: IMAGE_MAX_HEIGHT / 1.16,
					opacity: textOpacity,
					fontSize: height / 55,
					paddingLeft: scale(30),
					width: "58%"
				}}>
				{`${text1} `}
				<Animated.Text
					style={{
						color: "#fff",
						position: "absolute",
						top: IMAGE_MAX_HEIGHT / 1.16,
						opacity: textOpacity,
						fontSize: height / 55,
						fontWeight: Platform.OS === "android" ? "bold" : "700",
						paddingLeft: scale(30),
						width: "58%"
					}}>
					{text2}
				</Animated.Text>
			</Animated.Text>
		);
	};
	getMixedText = (text1, text2) => {
		return (
			<OffersTextMedium
				style={{
					marginTop: scale(5),
					color: "#15314C",
					fontSize: height / 55
				}}>
				{`${text1} `}
				<OffersTextBold
					style={{
						marginTop: scale(5),
						color: "#15314C",
						fontSize: height / 55
					}}>
					{text2}
				</OffersTextBold>
			</OffersTextMedium>
		);
	};
	getExpiresBottom = memoize(endDate => {
		let now = new Date(new Date().toDateString()).getTime();
		let endTime =
			new Date(new Date(endDate).toDateString()).getTime() + 86400000;
		let diff = Math.floor((endTime - now) / 86400000);
		if (diff === 1) {
			return this.getMixedText("Expires", "Today");
		}

		if (diff === 0) {
			return this.getMixedText("Expires in", "few hours");
		}

		if (diff < 0) {
			return this.getMixedText("Expired", "");
		}

		return this.getMixedText("Expires in", `${diff} Days`);
	});
	componentWillUnmount() {
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
		this.props.dispatch(offersSaveOfferDetailSuccess(null));
	}
	shouldComponentUpdate(props, state) {
		if (
			props.offerDetails.isOfferDetailError &&
			props.offerDetails.isOfferDetailError !==
				this.props.offerDetails.isOfferDetailError
		) {
			this.context.current.openAlert("Oops", "Unable to get Offer Detail");
			this.props.dispatch(offersSaveOfferDetailError(false));
			this.goBack();
		}
		return true;
	}

	openUrl = url => {
		if (url.startsWith("http://") || url.startsWith("https://")) {
			Linking.openURL(url);
		} else {
			Linking.openURL(`http://${url}`);
		}
	};

	scrollToTop = () => {
		this._scrollOffset = 0;
		this.scrollRef.getNode().scrollToOffset({ offset: 0, animated: true });
	};

	scrollToBottom = () => {
		this._scrollOffset = IMAGE_MIN_HEIGHT;
		this.scrollRef
			.getNode()
			.scrollToOffset({ offset: IMAGE_MIN_HEIGHT, animated: true });
	};

	autoScrollOnScrollDrag = event => {
		this._scrollOffset = event.nativeEvent.contentOffset.y;
		if (Platform.OS === "android") {
			if (
				event.nativeEvent.velocity.y > 0 &&
				event.nativeEvent.contentOffset.y < IMAGE_MAX_HEIGHT - height / 5
			) {
				this.scrollToTop();
			}
			if (
				event.nativeEvent.velocity.y < 0 &&
				event.nativeEvent.contentOffset.y > 0 &&
				event.nativeEvent.contentOffset.y < IMAGE_MIN_HEIGHT
			) {
				this.scrollToBottom();
			}
			this.prevY = event.nativeEvent.contentOffset.y;
		} else {
			if (
				this.prevY - event.nativeEvent.contentOffset.y > 0 &&
				event.nativeEvent.contentOffset.y < IMAGE_MAX_HEIGHT - height / 5
			) {
				this.scrollToTop();
			}
			if (
				this.prevY - event.nativeEvent.contentOffset.y < 0 &&
				event.nativeEvent.contentOffset.y > 0 &&
				event.nativeEvent.contentOffset.y < IMAGE_MIN_HEIGHT
			) {
				this.scrollToBottom();
			}
			this.prevY = event.nativeEvent.contentOffset.y;
		}
	};

	convertToDraftJSText(text) {
		try {
			return getRNDraftJSBlocks({
				contentState: JSON.parse(text),
				navigate: url => this.openUrl(url),
				customStyles: draftStyles
			});
		} catch (error) {
			console.log("erorr is", error);
			return null;
		}
	}

	async goBack() {
		// Todo explain and handle this scenario
		this.props.navigation.goBack();
		// if (!this.props.navigation.goBack()) {
		// 	this.props.navigation.dispatch(
		// 		CommonActions.reset({
		// 			index: 0,
		// 			routes: [{ name: "Main" }]
		// 		})
		// 	);
		// }
	}
	render() {
		const howToRedeemBlocks =
			this.props.offerDetails.offerDetails &&
			this.props.offerDetails.offerDetails.how_to_redeem
				? this.convertToDraftJSText(
						this.props.offerDetails.offerDetails.how_to_redeem
				  )
				: null;

		const termsAndConditionsBlocks =
			this.props.offerDetails.offerDetails &&
			this.props.offerDetails.offerDetails.terms_and_conditions
				? this.convertToDraftJSText(
						this.props.offerDetails.offerDetails.terms_and_conditions
				  )
				: null;

		var headMov = this.state.scrollY.interpolate({
			inputRange: [0, IMAGE_MIN_HEIGHT, IMAGE_MIN_HEIGHT + 1],
			outputRange: [0, -IMAGE_MIN_HEIGHT, -IMAGE_MIN_HEIGHT],
			extrapolate: "clamp",
			useNativeDriver: "true"
		});

		const bottomGetThis = this.state.scrollY.interpolate({
			inputRange: [0, height / 8, height / 8 + 1],
			outputRange: [height / 8, 0, 0],
			extrapolate: "clamp",
			useNativeDriver: "true"
		});

		const imageOpacity = this.state.scrollY.interpolate({
			inputRange: [0, IMAGE_MIN_HEIGHT, IMAGE_MIN_HEIGHT + 1],
			outputRange: [1, 0, 0],
			extrapolate: "clamp",
			useNativeDriver: "true"
		});

		const viewOpacity = this.state.scrollY.interpolate({
			inputRange: [0, IMAGE_MIN_HEIGHT, IMAGE_MIN_HEIGHT + 1],
			outputRange: [0, 1, 1],
			extrapolate: "clamp",
			useNativeDriver: "true"
		});

		const textOpacity = viewOpacity.interpolate({
			inputRange: [0, 0.1, 1],
			outputRange: [1, 0, 0],
			extrapolate: "clamp",
			useNativeDriver: "true"
		});

		const backButton = this.state.scrollY.interpolate({
			inputRange: [0, IMAGE_MIN_HEIGHT, IMAGE_MIN_HEIGHT + 1],
			outputRange: [0, -height / 20, -height / 20],
			extrapolate: "clamp",
			useNativeDriver: "true"
		});

		const bottomUpArrowButton = this.state.scrollY.interpolate({
			inputRange: [0, IMAGE_MIN_HEIGHT / 5, IMAGE_MIN_HEIGHT + 1],
			outputRange: [0, -height / 10, -2 * height],
			extrapolate: "clamp",
			useNativeDriver: "true"
		});

		const topArrowButton = this.state.scrollY.interpolate({
			inputRange: [0, IMAGE_MIN_HEIGHT / 2, IMAGE_MIN_HEIGHT],
			outputRange: [-height / 10, 0, 0],
			extrapolate: "clamp",
			useNativeDriver: "true"
		});

		const blackBrandColorText = this.state.scrollY.interpolate({
			inputRange: [0, IMAGE_MIN_HEIGHT, IMAGE_MIN_HEIGHT + 1],
			outputRange: [
				0,
				IMAGE_MAX_HEIGHT - IMAGE_MIN_HEIGHT + IMAGE_MIN_HEIGHT / 2.1,
				IMAGE_MAX_HEIGHT - IMAGE_MIN_HEIGHT + IMAGE_MIN_HEIGHT / 2.1
			],
			extrapolate: "clamp",
			useNativeDriver: "true"
		});

		const textTwoTransform = this.state.scrollY.interpolate({
			inputRange: [0, IMAGE_MIN_HEIGHT, IMAGE_MIN_HEIGHT + 1],
			outputRange: [height / 30, 0, 0],
			extrapolate: "clamp",
			useNativeDriver: "true"
		});

		const textThreeTransform = this.state.scrollY.interpolate({
			inputRange: [0, IMAGE_MIN_HEIGHT, IMAGE_MIN_HEIGHT + 1],
			outputRange: [height / 30, 0, 0],
			extrapolate: "clamp",
			useNativeDriver: "true"
		});

		const animatedTopView = this.state.scrollY.interpolate({
			inputRange: [0, IMAGE_MIN_HEIGHT, IMAGE_MIN_HEIGHT + 1],
			outputRange: [-height / 4.5, 0, 0],
			extrapolate: "clamp",
			useNativeDriver: "true"
		});

		if (
			this.props.offerDetails.isOfferDetailLoading ||
			this.props.offerDetails.offerDetails === null
		) {
			return (
				<SafeAreaView>
					<View style={styles.header}>
						<TouchableOpacity
							style={styles.backButton}
							onPress={() => this.goBack()}>
							<Icon
								iconType={"ionicon"}
								iconName={"md-arrow-back"}
								iconSize={25}
								iconColor={"#606060"}
							/>
						</TouchableOpacity>
					</View>
					<View style={styles.mainContainer}>
						<ProgressScreen
							isMessage={true}
							indicatorSize={height / 30}
							indicatorColor={"#14314c"}
							primaryMessage={"Hang On..."}
							message={"Loading"}
						/>
					</View>
				</SafeAreaView>
			);
		} else {
			return (
				<View style={{ flex: 1 }}>
					<Animated.FlatList
						ref={ref => (this.scrollRef = ref)}
						data={[0, 1]}
						showsVerticalScrollIndicator={false}
						scrollEventThrottle={16}
						renderItem={({ item, index }) => {
							if (index === 0) {
								return (
									<ImageBackgroundAnimated
										source={{
											uri: this.props.offerDetails.offerDetails
												.offer_detailed_image
										}}
										onLayout={event => {
											if (this.imageMaxHeight === 0) {
												this.imageMaxHeight = event.nativeEvent.layout.height;
											}
										}}
										resizeMode={"cover"}
										style={{
											zIndex: 2,
											position: "relative",
											width,
											height: IMAGE_MAX_HEIGHT,
											opacity: imageOpacity,
											backgroundColor: "#fff",
											transform: [{ translateY: headMov }]
										}}>
										<TouchableOpacityAnimated
											onPress={() => this.goBack()}
											style={[
												{
													transform: [
														{
															translateY: backButton
														}
													]
												},
												styles.backButtonStyle
											]}>
											<Icon
												iconType={"ionicon"}
												iconName={"md-arrow-back"}
												iconSize={24}
												iconColor={"#fff"}
												style={{
													padding: height / 60
												}}
											/>
										</TouchableOpacityAnimated>

										<Animated.Text
											numberOfLines={1}
											style={{
												color: "#fff",
												position: "absolute",
												top: IMAGE_MAX_HEIGHT / 5.49,
												opacity: textOpacity,
												fontSize: height / 35,
												fontWeight: Platform.OS === "ios" ? "bold" : "700",
												paddingLeft: scale(30),
												width: "58%"
											}}>
											{this.props.offerDetails.offerDetails.brand_name}
										</Animated.Text>
										<Animated.View
											style={{
												color: "#fff",
												position: "absolute",
												top: IMAGE_MAX_HEIGHT / 2.7,
												paddingLeft: scale(30),
												width: "58%"
											}}>
											<Animated.Text
												numberOfLines={2}
												style={{
													color: "#fff",
													opacity: textOpacity,
													width: "100%",
													fontWeight: Platform.OS === "ios" ? "bold" : "700",
													fontSize: height / 40
												}}>
												{`${this.props.offerDetails.offerDetails.text_2}`}
											</Animated.Text>
											<Animated.Text
												color={"#fff"}
												numberOfLines={2}
												style={{
													color: "#fff",
													marginTop: IMAGE_MAX_HEIGHT / 16,
													width: "100%",
													opacity: textOpacity,
													fontSize: height / 55
												}}>
												{this.props.offerDetails.offerDetails.text_3}
											</Animated.Text>
										</Animated.View>
										<TouchableOpacityAnimated
											onPress={() => {
												this.scrollToBottom();
											}}
											style={{
												position: "absolute",
												bottom: 0,
												alignSelf: "center",
												opacity: textOpacity,
												transform: [
													{
														translateY: bottomUpArrowButton
													}
												]
											}}>
											<Image
												source={ARROWUP}
												resizeMode={"contain"}
												style={{
													width: width / 5,
													height: width / 11
												}}
											/>
										</TouchableOpacityAnimated>
									</ImageBackgroundAnimated>
								);
							} else {
								return (
									<View
										style={{
											marginTop: height / 20,
											paddingHorizontal: scale(30),
											paddingBottom: height / 6,
											minHeight: IMAGE_MAX_HEIGHT
										}}>
										<OffersTextBold style={{ color: "#6c6c6c" }}>
											How to redeem
										</OffersTextBold>
										<View
											style={{
												marginTop: height / 90
											}}>
											{howToRedeemBlocks !== null ? howToRedeemBlocks : null}
										</View>
										{this.props.offerDetails.offerDetails.special_note &&
										this.props.offerDetails.offerDetails.special_note.length >
											0 ? (
											<View style={{ paddingTop: scale(35) }}>
												<OffersTextBold style={{ color: "#6c6c6c" }}>
													Special note
												</OffersTextBold>
												<OffersTextRegular style={{ color: "#6c6c6c" }}>
													{this.props.offerDetails.offerDetails.special_note}
												</OffersTextRegular>
											</View>
										) : null}
										{this.props.offerDetails.offerDetails
											.terms_and_conditions &&
										this.props.offerDetails.offerDetails.terms_and_conditions
											.length > 0 ? (
											<View
												style={{
													paddingTop: scale(35)
												}}>
												<OffersTextBold style={{ color: "#6c6c6c" }}>
													Terms and conditions
												</OffersTextBold>
												{termsAndConditionsBlocks !== null
													? termsAndConditionsBlocks
													: null}
											</View>
										) : null}
									</View>
								);
							}
						}}
						onScroll={Animated.event([
							{
								nativeEvent: { contentOffset: { y: this.state.scrollY } }
							}
						])}
						onScrollEndDrag={this.autoScrollOnScrollDrag.bind(this)}
					/>

					<Animated.View
						pointerEvents={"none"}
						style={{
							width: width,
							position: "absolute",
							top: animatedTopView,
							height: height / 4.5,
							borderBottomWidth: 0.3,
							borderBottomColor: "#ececec",
							backgroundColor: "#fff",
							opacity: viewOpacity,
							justifyContent: "center"
						}}>
						<Animated.Text
							style={{
								color: "#000",
								opacity: viewOpacity,
								fontSize: height / 35,
								fontWeight: Platform.OS === "ios" ? "bold" : "700",
								paddingLeft: scale(30)
							}}>
							{this.props.offerDetails.offerDetails.brand_name}
						</Animated.Text>
						<Animated.Text
							style={{
								color: "rgba(0, 0, 0, 0.5)",
								paddingLeft: scale(30),
								width: "58%",
								fontWeight: Platform.OS === "ios" ? "bold" : "700",
								fontSize: height / 50
							}}>
							{this.props.offerDetails.offerDetails.text_2}
						</Animated.Text>
						<Animated.Text
							color={"#fff"}
							style={{
								color: "rgba(0, 0, 0, 0.5)",
								fontSize: height / 55,
								paddingLeft: scale(30),
								width: "58%"
							}}>
							{this.props.offerDetails.offerDetails.text_3}
						</Animated.Text>
					</Animated.View>
					<TouchableOpacityAnimated
						onPress={() => {
							GoAppAnalytics.trackWithProperties("offer_redeem", {
								id: this.props.offerDetails.offerDetails.id,
								name: this.props.offerDetails.offerDetails.brand_name
							});
							this.setState({ showOffer: true });
						}}
						style={{
							position: "absolute",
							top: IMAGE_MAX_HEIGHT / 1.36,
							backgroundColor: "#fff",
							paddingHorizontal: scale(12),
							marginLeft: scale(30),
							paddingVertical: scale(10),
							borderRadius: 4,
							width: "30%",
							alignItems: "center",
							justifyContent: "center",
							opacity: textOpacity,
							transform: [
								{
									scale: textOpacity
								}
							]
						}}>
						<Animated.Text
							style={{
								color: "#B89586"
							}}>
							{"Get this"}
						</Animated.Text>
					</TouchableOpacityAnimated>
					{this.getExpiresIn(
						this.props.offerDetails.offerDetails.end_date,
						textOpacity
					)}
					<TouchableOpacityAnimated
						onPress={() => this.scrollToTop()}
						style={{
							position: "absolute",
							top: 0,
							opacity: viewOpacity,
							transform: [{ translateY: topArrowButton }],
							alignSelf: "center"
						}}>
						<Image
							source={ARROWDOWN}
							resizeMode={"contain"}
							style={{ width: width / 5, height: width / 11 }}
						/>
					</TouchableOpacityAnimated>
					<Animated.View
						style={{
							position: "absolute",
							bottom: 0,
							height: height / 8,
							width: width,
							paddingHorizontal: scale(30),
							justifyContent: "center",
							backgroundColor: "#fff",
							transform: [{ translateY: bottomGetThis }]
						}}>
						<TouchableOpacity
							onPress={() => {
								GoAppAnalytics.trackWithProperties("offer_redeem", {
									id: this.props.offerDetails.offerDetails.id,
									name: this.props.offerDetails.offerDetails.brand_name
								});
								this.setState({ showOffer: true });
							}}
							style={{
								backgroundColor: "#15314C",
								paddingVertical: scale(10),
								paddingHorizontal: scale(12),
								borderRadius: 4,
								width: "40%",
								alignItems: "center",
								justifyContent: "center"
							}}>
							<OffersTextRegular color={"#fff"}>{"Get this"}</OffersTextRegular>
						</TouchableOpacity>
						{this.getExpiresBottom(
							this.props.offerDetails.offerDetails.end_date
						)}
					</Animated.View>
					{this.state.showOffer && (
						<BottomOffer
							open={this.state.showOffer}
							handleClose={() => this.setState({ showOffer: false })}
							offer={this.props.offerDetails.offerDetails}
						/>
					)}
				</View>
			);
		}
	}
}

OfferDetail.contextType = AlertContext;
const draftStyles = StyleSheet.flatten({
	"unordered-list-item": {
		color: "#757475",
		fontSize: height / 50,
		marginTop: 3
	},
	unorderedListItemBullet: {
		color: "#757475",
		fontSize: height / 50
	},
	"ordered-list-item": {
		color: "#757475",
		fontSize: height / 50,
		marginTop: 3
	},
	orderedListItemNumber: {
		color: "#757475",
		fontSize: height / 50,
		marginRight: 3
	},
	unstyled: {
		color: "#757475",
		fontSize: height / 50
	}
});

const mapStateToProps = state => ({
	offerDetails: state.offers && state.offers.offerDetails,
	appToken: state.appToken.token
});

export default connect(mapStateToProps)(OfferDetail);
