import {
	OFFERS_GET_OFFER_DETAIL,
	OFFERS_SAVE_OFFER_DETAIL_SUCCESS,
	OFFERS_SAVE_OFFER_DETAIL_ERROR,
	OFFERS_GET_OFFER_CODE,
	OFFERS_SAVE_OFFER_CODE_SUCCESS,
	OFFERS_SAVE_OFFER_CODE_ERROR
} from "./Saga";

const initialState = {
	isOfferDetailLoading: false,
	offerDetails: null,
	isOfferDetailError: false,
	isOfferCodeLoading: false,
	offerCode: null,
	isOfferCodeError: false
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case OFFERS_GET_OFFER_DETAIL:
			return {
				...state,
				isOfferDetailLoading: true,
				offerDetails: null,
				isOfferDetailError: false
			};

		case OFFERS_SAVE_OFFER_DETAIL_SUCCESS:
			return {
				...state,
				isOfferDetailLoading: false,
				offerDetails: action.payload
			};

		case OFFERS_SAVE_OFFER_DETAIL_ERROR:
			return {
				...state,
				isOfferDetailLoading: false,
				isOfferDetailError: action.payload
			};
		case OFFERS_GET_OFFER_CODE:
			return {
				...state,
				isOfferCodeLoading: true,
				isOfferCodeError: false
			};
		case OFFERS_SAVE_OFFER_CODE_SUCCESS:
			return {
				...state,
				isOfferCodeLoading: false,
				offerCode: action.payload
			};
		case OFFERS_SAVE_OFFER_CODE_ERROR:
			return {
				...state,
				isOfferCodeLoading: false,
				isOfferCodeError: true
			};
		default:
			return state;
	}
};
