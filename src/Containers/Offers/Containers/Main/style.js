import { StyleSheet, Dimensions, Platform } from "react-native";
import { font_two } from "../../../../Assets/styles.android";

const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#FCFCFC"
	},
	header: {
		width: width,
		paddingVertical: height / 32.54,
		paddingHorizontal: width / 18.75
	},
	inputSection: {
		height: height / 18,
		paddingHorizontal: width / 28,
		flexDirection: "row",
		alignItems: "center",
		backgroundColor: "#EBEBEB",
		borderRadius: width / 50
	},
	textInput: {
		width: width / 1.8,
		padding: 0,
		height: height / 21.16,
		marginLeft: width / 30,
		fontSize: height / 50,
		color: "#424446",
		fontFamily: Platform.OS === "ios" ? "Avenir" : font_two,
		fontWeight: Platform.OS === "ios" ? "400" : undefined
	},
	inputRow: {
		marginTop: height / 29,
		width: width / 1.12,
		height: height / 18,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between"
	},
	headerBar: {
		flexDirection: "row",
		alignItems: "center"
	},
	backArrow: {
		marginRight: width * 0.01
	}
});
