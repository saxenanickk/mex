import { takeLatest, put, call } from "redux-saga/effects";
import Api from "../../Api";

export const OFFERS_GET_ALL_SITES = "OFFERS_GET_ALL_SITES";
export const OFFERS_SAVE_ALL_SITES_SUCCESS = "OFFERS_GET_ALL_SITES_SUCCESS";
export const OFFERS_SAVE_ALL_SITES_ERROR = "OFFERS_GET_ALL_SITES_ERROR";
export const OFFERS_SAVE_USER_SITE_ID = "OFFERS_SAVE_USER_SITE_ID";
export const OFFERS_GET_SECTIONS = "OFFERS_GET_SECTIONS";
export const OFFERS_SAVE_SECTIONS_SUCCESS = "OFFERS_SAVE_SECTIONS_SUCCESS";
export const OFFERS_SAVE_SECTIONS_ERROR = "OFFERS_SAVE_SECTIONS_ERROR";
export const OFFERS_GET_ALL_OFFERS = "OFFERS_GET_ALL_OFFERS";
export const OFFERS_SAVE_ALL_OFFERS_SUCCESS = "OFFERS_SAVE_ALL_OFFERS_SUCCESS";
export const OFFERS_SAVE_ALL_OFFERS_ERROR = "OFFERS_SAVE_ALL_OFFERS_ERROR";
export const OFFERS_GET_ALL_CATEGORIES = "OFFERS_GET_ALL_CATEGORIES";
export const OFFERS_SAVE_ALL_CATEGORIES = "OFFERS_SAVE_ALL_CATEGORIES";
export const OFFERS_SAVE_ALL_CATEGORIES_ERROR =
	"OFFERS_SAVE_ALL_CATEGORIES_ERROR";

export const offersGetAllSites = payload => ({
	type: OFFERS_GET_ALL_SITES,
	payload
});

export const offersSaveAllSitesSuccess = payload => ({
	type: OFFERS_SAVE_ALL_SITES_SUCCESS,
	payload
});

export const offersSaveAllSitesError = payload => ({
	type: OFFERS_SAVE_ALL_SITES_ERROR,
	payload
});

export const offersSaveUserSiteId = payload => ({
	type: OFFERS_SAVE_USER_SITE_ID,
	payload
});

export const offersGetSections = payload => ({
	type: OFFERS_GET_SECTIONS,
	payload
});

export const offersSaveSectionsSuccess = payload => ({
	type: OFFERS_SAVE_SECTIONS_SUCCESS,
	payload
});

export const offersSaveSectionsError = payload => ({
	type: OFFERS_SAVE_SECTIONS_ERROR,
	payload
});

export const offersGetAllOffers = payload => ({
	type: OFFERS_GET_ALL_OFFERS,
	payload
});

export const offersSaveAllOffersSuccess = payload => ({
	type: OFFERS_SAVE_ALL_OFFERS_SUCCESS,
	payload
});

export const offersSaveAllOffersError = payload => ({
	type: OFFERS_SAVE_ALL_OFFERS_ERROR,
	payload
});

export const offersGetAllCategories = payload => ({
	type: OFFERS_GET_ALL_CATEGORIES,
	payload
});

export const offersSaveAllCategories = payload => ({
	type: OFFERS_SAVE_ALL_CATEGORIES,
	payload
});

export const offersSaveAllCategoriesError = payload => ({
	type: OFFERS_SAVE_ALL_CATEGORIES_ERROR,
	payload
});

export function* offersMainSaga(dispatch) {
	yield takeLatest(OFFERS_GET_ALL_SITES, handleGetAllSites);
	yield takeLatest(OFFERS_GET_SECTIONS, handleGetSections);
	yield takeLatest(OFFERS_GET_ALL_OFFERS, handleGetAllOffers);
	yield takeLatest(OFFERS_GET_ALL_CATEGORIES, handleGetAllCategories);
}

function* handleGetAllSites(action) {
	try {
		const data = yield call(Api.getAllSites, action.payload);
		yield put(offersSaveAllSitesSuccess(data));
		const userSite = data.find(siteObject => {
			return action.payload.siteId === siteObject.ref_site_id;
		});
		yield put(offersSaveUserSiteId(userSite.id));
	} catch (error) {
		yield put(offersSaveAllSitesError(true));
	}
}

function* handleGetSections(action) {
	try {
		const data = yield call(Api.getSections, action.payload);
		yield put(offersSaveSectionsSuccess(data));
	} catch (error) {
		yield put(offersSaveSectionsError(true));
	}
}

function* handleGetAllOffers(action) {
	try {
		const data = yield call(Api.getOffers, action.payload);
		yield put(offersSaveAllOffersSuccess(data));
	} catch (error) {
		yield put(offersSaveAllOffersError(true));
	}
}

function* handleGetAllCategories(action) {
	try {
		const data = yield call(Api.getAllCategories, action.payload);
		yield put(offersSaveAllCategories(data));
	} catch (error) {
		yield put(offersSaveAllCategoriesError(true));
	}
}
