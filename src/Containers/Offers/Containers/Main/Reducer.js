import {
	OFFERS_GET_ALL_SITES,
	OFFERS_SAVE_ALL_SITES_SUCCESS,
	OFFERS_SAVE_ALL_SITES_ERROR,
	OFFERS_SAVE_USER_SITE_ID,
	OFFERS_GET_SECTIONS,
	OFFERS_SAVE_SECTIONS_SUCCESS,
	OFFERS_SAVE_SECTIONS_ERROR,
	OFFERS_GET_ALL_OFFERS,
	OFFERS_SAVE_ALL_OFFERS_SUCCESS,
	OFFERS_SAVE_ALL_OFFERS_ERROR,
	OFFERS_SAVE_ALL_CATEGORIES,
	OFFERS_GET_ALL_CATEGORIES,
	OFFERS_SAVE_ALL_CATEGORIES_ERROR
} from "./Saga";

const initialState = {
	isSitesLoading: false,
	sites: [],
	isSitesError: false,
	userSiteId: null,
	isSectionsLoading: true,
	sections: [],
	isSectionsError: false,
	isOffersLoading: true,
	offers: null,
	isOffersError: false,
	isCategoriesLoading: false,
	categories: [],
	isCategoriesError: false
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case OFFERS_GET_ALL_SITES:
			return {
				...state,
				isSitesLoading: true,
				sites: [],
				isSitesError: false
			};

		case OFFERS_SAVE_ALL_SITES_SUCCESS:
			return {
				...state,
				isSitesLoading: false,
				sites: action.payload
			};
		case OFFERS_SAVE_ALL_SITES_ERROR:
			return {
				...state,
				isSitesLoading: false,
				isSitesError: true
			};
		case OFFERS_SAVE_USER_SITE_ID:
			return {
				...state,
				userSiteId: action.payload
			};
		case OFFERS_GET_SECTIONS:
			return {
				...state,
				isSectionsLoading: true,
				sections: [],
				isSectionsError: false
			};
		case OFFERS_SAVE_SECTIONS_SUCCESS:
			return {
				...state,
				isSectionsLoading: action.payload === null ? true : false,
				sections: action.payload === null ? [] : action.payload
			};
		case OFFERS_SAVE_SECTIONS_ERROR:
			return {
				...state,
				isSectionsLoading: false,
				isSectionsError: true
			};
		case OFFERS_GET_ALL_OFFERS:
			return {
				...state,
				isOffersLoading: true,
				isOffersError: false
			};
		case OFFERS_SAVE_ALL_OFFERS_SUCCESS:
			return {
				...state,
				isOffersLoading: action.payload === null ? true : false,
				offers: action.payload === null ? [] : action.payload
			};
		case OFFERS_SAVE_ALL_OFFERS_ERROR:
			return {
				...state,
				isOffersLoading: false,
				isOffersError: true
			};
		case OFFERS_GET_ALL_CATEGORIES:
			return {
				...state,
				isCategoriesLoading: true,
				isCategoriesError: false
			};
		case OFFERS_SAVE_ALL_CATEGORIES:
			return {
				...state,
				isCategoriesLoading: false,
				categories: action.payload
			};
		case OFFERS_SAVE_ALL_CATEGORIES_ERROR:
			return {
				...state,
				isCategoriesLoading: false,
				isCategoriesError: true
			};
		default:
			return state;
	}
};
