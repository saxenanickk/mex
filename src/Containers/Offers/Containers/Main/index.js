import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	StatusBar,
	NativeModules,
	LayoutAnimation,
	Platform,
	TextInput,
	Keyboard,
	Animated,
	BackHandler
} from "react-native";
import { connect } from "react-redux";
import {
	OffersAlert,
	OffersTextBold,
	OffersTextRegular
} from "../../Components";
import styles from "./style";
import { Icon } from "../../../../Components";
import SearchOffer from "../SearchOffer";
import Home from "../Home";
import {
	offersGetSearchOffer,
	offersSaveSearchOffer
} from "../SearchOffer/Saga";
import { offersSaveSectionsSuccess, offersSaveAllOffersSuccess } from "./Saga";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import I18n from "../../Assets/Strings/i18n";
const { width, height } = Dimensions.get("window");

const { UIManager } = NativeModules;

if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showOffer: false,
			isEditing: false,
			searchText: ""
		};
		this.transition = new Animated.Value(1);
		this.toggleEditing = this.toggleEditing.bind(this);
		this.changeSearchText = this.changeSearchText.bind(this);
		this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
		BackHandler.addEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
	}

	componentWillUnmount() {
		this.props.dispatch(offersSaveSectionsSuccess(null));
		this.props.dispatch(offersSaveAllOffersSuccess(null));
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
	}

	/**
	 * Back Button Handler
	 */
	handleBackButtonClick() {
		if (this.state.isEditing) {
			this.toggleEditing();
		}
		return false;
	}

	toggleEditing = () => {
		if (this.state.isEditing) {
			this.inputRef && this.inputRef.blur();
			this.props.dispatch(
				offersSaveSearchOffer({
					data: null,
					keyword: ""
				})
			);
			Keyboard.dismiss();
		}
		setTimeout(() => {
			LayoutAnimation.easeInEaseOut();
			this.setState({ isEditing: !this.state.isEditing, searchText: "" });
		}, 100);
	};

	changeSearchText = text => {
		if (
			this.props.userSiteId &&
			text.length > 2 &&
			this.props.keyword != text
		) {
			this.props.dispatch(
				offersGetSearchOffer({
					appToken: this.props.token,
					data: { site_id: this.props.userSiteId, keyword: text }
				})
			);
			GoAppAnalytics.trackWithProperties("offer_search", {
				query: text
			});
		}
		this.setState({ searchText: text });
	};

	handleBackPress = () => {
		// go back
		this.props.navigation.navigate("Goapp");
	};

	render() {
		return (
			<View style={styles.container}>
				<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
				<View style={styles.header}>
					<View style={styles.headerBar}>
						<TouchableOpacity
							style={styles.backArrow}
							onPress={this.handleBackPress}>
							<Icon
								iconType={"feather"}
								iconName={"arrow-left"}
								iconSize={width / 18}
							/>
						</TouchableOpacity>
						<OffersTextBold
							color={"#273D52"}
							numberOfLines={1}
							size={height / 35}>
							{"Offers"}
						</OffersTextBold>
					</View>
					<View style={styles.inputRow}>
						<View
							style={[
								styles.inputSection,
								{ width: this.state.isEditing ? width / 1.3 : width / 1.12 }
							]}>
							<Icon
								iconName={"ios-search"}
								iconType={"ionicon"}
								iconColor={"#424446"}
								iconSize={height / 28}
							/>
							<TextInput
								ref={ref => (this.inputRef = ref)}
								value={this.state.searchText}
								style={styles.textInput}
								onChangeText={this.changeSearchText}
								placeholder={I18n.t("search_for_partners_offers")}
								onFocus={this.toggleEditing}
								underlineColorAndroid={"transparent"}
							/>
							{this.state.isEditing && (
								<TouchableOpacity
									onPress={() => this.setState({ searchText: "" })}>
									<Icon
										iconName={"ios-close"}
										iconType={"ionicon"}
										iconColor={"#424446"}
										iconSize={height / 28}
									/>
								</TouchableOpacity>
							)}
						</View>
						{this.state.isEditing && (
							<TouchableOpacity onPress={this.toggleEditing}>
								<OffersTextRegular size={height / 60}>
									{"Close"}
								</OffersTextRegular>
							</TouchableOpacity>
						)}
					</View>
				</View>
				<View style={!this.state.isEditing ? { flex: 0 } : { flex: 1 }}>
					<SearchOffer
						searchText={
							this.state.searchText.length > 2 ? this.state.searchText : ""
						}
						open={this.state.isEditing}
						navigation={this.props.navigation}
						isEditing={this.state.isEditing}
					/>
				</View>
				<Animated.View
					style={
						this.state.isEditing
							? { position: "absolute", top: height }
							: { flex: 1 }
					}>
					<Home
						open={!this.state.isEditing}
						navigation={this.props.navigation}
					/>
				</Animated.View>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.appToken.user,
		token: state.appToken.token,
		userSiteId: state.offers && state.offers.home.userSiteId,
		keyword: state.offers && state.offers.offerSearch.keyword
	};
}

export default connect(mapStateToProps)(Main);
