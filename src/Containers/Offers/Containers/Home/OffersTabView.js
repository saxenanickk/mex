import React, { Component } from "react";
import {
	Dimensions,
	View,
	ScrollView,
	TouchableOpacity,
	StyleSheet
} from "react-native";
import { connect } from "react-redux";
import FastImage from "react-native-fast-image";
import LinearGradient from "react-native-linear-gradient";
import { Icon } from "../../../../Components";
import { OffersTextLight, OffersTextRegular } from "../../Components";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";

const { width, height } = Dimensions.get("window");

const Offer = props => (
	<TouchableOpacity
		activeOpacity={1}
		onPress={() => props.handleOfferPress()}
		style={{
			width: "42%",
			marginVertical: width / 40,
			alignSelf: "flex-start",
			marginLeft: props.index % 2 === 0 ? "5%" : "2%",
			marginRight: props.index % 2 === 0 ? "3%" : "2%"
		}}>
		<FastImage
			source={{ uri: props.offer.listing_banner_image }}
			style={{
				borderRadius: 10,
				aspectRatio: 76 / 100
			}}>
			<LinearGradient
				colors={["rgba(0,0,0,0)", "rgba(0,0,0,1)"]}
				style={{
					position: "absolute",
					bottom: 0,
					borderRadius: 10,
					justifyContent: "flex-end",
					paddingLeft: "5%",
					paddingBottom: "7%",
					width: "100%",
					color: "#fff",
					height: "45%"
				}}>
				<OffersTextRegular size={12} color={"#fff"}>
					{props.offer.brand_name}
				</OffersTextRegular>
				<OffersTextRegular size={14} color={"#fff"}>
					{props.offer.text_1}
				</OffersTextRegular>
			</LinearGradient>
		</FastImage>
	</TouchableOpacity>
);

class OffersTabView extends Component {
	state = {
		index: 0,
		routes: [{ key: "all", title: "All offers" }]
	};

	static getDerivedStateFromProps(nextProps, prevState) {
		if (prevState.routes.length !== nextProps.categories.length + 1) {
			const newRoutes = nextProps.categories.map(elem => {
				return { key: elem.id, title: elem.name, icon: elem.category_icon };
			});
			return {
				routes: [...prevState.routes, ...newRoutes]
			};
		}
		return {};
	}

	_handleIndexChange = index => {
		this.setState({ index });
		GoAppAnalytics.trackWithProperties("view_category", {
			category: this.state.routes[index].title
		});
	};

	_renderIcon = (route, index) => {
		const focused = this.state.index === index;
		if (route.icon) {
			return (
				<FastImage
					source={{ uri: route.icon }}
					style={{
						width: 20,
						height: 20,
						margin: 4,
						opacity: focused ? 1 : 0.6
					}}
					resizeMode={"contain"}
				/>
			);
		}
		switch (route.key) {
			case "all":
				return (
					<Icon
						iconType={"feather"}
						iconName={"shopping-cart"}
						iconSize={20}
						iconColor={focused ? "#273D52" : "#545454"}
					/>
				);
			default:
				return null;
		}
	};

	_renderTabBar = () => (
		<ScrollView
			contentContainerStyle={{
				flexGrow: 1,
				flexDirection: "row",
				flexWrap: "nowrap",
				alignItems: "center",
				justifyContent: "space-around",
				backgroundColor: "#FCFCFC",
				paddingHorizontal: width / 30
			}}
			horizontal
			keyboardShouldPersistTaps={"handled"}
			showsHorizontalScrollIndicator={false}
			automaticallyAdjustContentInsets={false}
			overScrollMode={"never"}>
			{this.props.offers !== null
				? this.state.routes.map((route, index) => (
						<TouchableOpacity
							key={index}
							style={[styles.tabItemContainer]}
							onPress={() => this._handleIndexChange(index)}>
							<View style={[styles.tabItem]}>
								{this._renderIcon(route, index)}
								<OffersTextLight
									style={{
										fontSize: 14,
										color: index === this.state.index ? "#273D52" : "#545454"
									}}>
									{route.title}
								</OffersTextLight>
								<View
									style={{
										height: 2,
										borderRadius: 10,
										backgroundColor:
											index === this.state.index ? "#36597A" : "#fff",
										width: "100%",
										flexDirection: "row"
									}}
								/>
							</View>
						</TouchableOpacity>
				  ))
				: null}
		</ScrollView>
	);

	render() {
		return (
			<View style={{ flex: 1 }}>
				{this.props.offers && this.props.offers.length > 0
					? this._renderTabBar()
					: null}
				<View
					style={{
						flex: 1,
						flexDirection: "row",
						flexWrap: "wrap",
						backgroundColor: "#F2F2F2",
						paddingTop: 15,
						paddingBottom: height / 90
					}}>
					{this.props.offers !== null && this.props.offers.length > 0
						? this.props.offers
								.filter(offer => {
									let selectedCategory = this.state.routes[this.state.index]
										.title;
									if (selectedCategory === "All offers") {
										return true;
									}
									const isSelectedOffer = offer.categories.findIndex(
										category => category === selectedCategory
									);
									if (isSelectedOffer !== -1) {
										return true;
									}
									return false;
								})
								.map((offer, index) => {
									return (
										<Offer
											key={offer.id}
											offer={offer}
											handleOfferPress={() => {
												this.props.navigation.navigate("OfferDetail", {
													id: offer.id
												});
												GoAppAnalytics.trackWithProperties("offer_open", {
													id: offer.id,
													name: offer.brand_name,
													src: "tab"
												});
											}}
											index={index}
										/>
									);
								})
						: null}
				</View>
			</View>
		);
	}
}

export default connect()(OffersTabView);

const styles = StyleSheet.create({
	tabItemContainer: {
		flex: 1,
		marginHorizontal: width / 40,
		marginVertical: width / 20
	},
	activeTabItemContainer: {
		borderBottomWidth: 3
	},
	tabItem: {
		flex: 1,
		alignItems: "center",
		justifyContent: "flex-end"
	}
});
