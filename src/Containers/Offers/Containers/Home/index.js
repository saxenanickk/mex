import React, { Component } from "react";
import {
	View,
	Dimensions,
	ScrollView,
	TouchableOpacity,
	Platform,
	UIManager
} from "react-native";
import { connect } from "react-redux";
import FastImage from "react-native-fast-image";
import Carousel from "react-native-snap-carousel";
import { ProgressScreen } from "../../../../Components";
import OffersTabView from "./OffersTabView";
import { OffersTextBold, OffersTextMedium, NoOffer } from "../../Components";
import {
	offersGetSections,
	offersGetAllOffers,
	offersGetAllCategories,
	offersSaveSectionsError,
	offersSaveAllOffersError
} from "../Main/Saga";

import BottomOffer from "../BottomOffer";
import { AlertContext } from "../../AlertContext";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import I18n from "../../Assets/Strings/i18n";
const { width, height } = Dimensions.get("window");

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showOffer: false,
			selectedOffer: null,
			currentSectionIndex: 0
		};
		if (Platform.OS === "android") {
			UIManager.setLayoutAnimationEnabledExperimental(true);
		}
	}

	componentDidMount() {
		this.fetchAllOffers(this.props);
	}

	fetchAllOffers = props => {
		if (
			props.userSiteId !== null &&
			(props.sections === null || props.sections.length === 0) &&
			!props.isSectionsError
		) {
			props.dispatch(
				offersGetSections({
					appToken: props.appToken,
					siteId: props.userSiteId
				})
			);
		}
		if (
			props.userSiteId !== null &&
			(props.offers === null || props.offers.length === 0) &&
			!props.isOffersError
		) {
			props.dispatch(
				offersGetAllOffers({
					appToken: props.appToken,
					siteId: props.userSiteId
				})
			);
			props.dispatch(
				offersGetAllCategories({
					appToken: props.appToken,
					siteId: props.userSiteId
				})
			);
		}
	};

	shouldComponentUpdate(props, state) {
		if (
			props.isSectionsError &&
			props.isSectionsError !== this.props.isSectionsError
		) {
			this.context.current.openAlert(
				I18n.t("Oops"),
				I18n.t("unable_to_get_trending_offers")
			);
			this.props.dispatch(offersSaveSectionsError(false));
		}
		if (
			props.isOffersError &&
			props.isOffersError !== this.props.isOffersError
		) {
			this.context.current.openAlert(
				I18n.t("Oops"),
				I18n.t("unable_to_get_offers")
			);
			this.props.dispatch(offersSaveAllOffersError(false));
		}
		return true;
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevProps.userSiteId !== this.props.userSiteId) {
			this.props.dispatch(
				offersGetSections({
					appToken: this.props.appToken,
					siteId: this.props.userSiteId
				})
			);
			this.props.dispatch(
				offersGetAllCategories({
					appToken: this.props.appToken,
					siteId: this.props.userSiteId
				})
			);
			this.props.dispatch(
				offersGetAllOffers({
					appToken: this.props.appToken,
					siteId: this.props.userSiteId
				})
			);
		}
	}

	handleOnTrendingOfferPress = offerDetails => {
		this.props.navigation.navigate("OfferDetail", { id: offerDetails.id });
		GoAppAnalytics.trackWithProperties("offer_open", {
			name: offerDetails.brand_name,
			id: offerDetails.id,
			src: "trending"
		});
	};

	handleGetThis = offer => {
		this.setState({ showOffer: true, selectedOffer: offer });
		GoAppAnalytics.trackWithProperties("offer_redeem", {
			id: offer.offer_details.id,
			name: offer.offer_details.brand_name
		});
	};

	render() {
		return (
			<React.Fragment>
				<ScrollView
					style={{ flex: 1, backgroundColor: "#F2F2F2" }}
					keyboardShouldPersistTaps={"always"}
					showsVerticalScrollIndicator={false}>
					{this.props.isSectionsLoading ? (
						<View
							style={{
								height: height - height / 4,
								backgroundColor: "#fff",
								justifyContent: "center",
								alignItems: "center"
							}}>
							<ProgressScreen
								isMessage={true}
								indicatorSize={height / 30}
								indicatorColor={"#14314c"}
								primaryMessage={I18n.t("hang_on")}
								message={I18n.t("searching_for_offers")}
							/>
						</View>
					) : null}
					{/* Carousel */}
					{this.props.sections.length > 0 ? (
						<View
							style={{
								flex: 1,
								backgroundColor: "#fcfcfc",
								paddingBottom: width / 50
							}}>
							<OffersTextBold
								style={{
									fontSize: 18,
									color: "#252525",
									marginBottom: width / 50,
									paddingHorizontal: width / 20
								}}>
								Trending
							</OffersTextBold>
							<Carousel
								data={this.props.sections}
								containerCustomStyle={{ flex: 1 }}
								renderItem={({ item, index }) => (
									<TouchableOpacity
										activeOpacity={1}
										onPress={() =>
											this.handleOnTrendingOfferPress(item.offer_details)
										}>
										<FastImage
											key={index}
											style={{
												flex: 1,
												borderRadius: 10,
												aspectRatio: 1.76
											}}
											source={{ uri: item.offer_details.icon }}
											resizeMode={"cover"}
										/>
										<View
											style={{
												paddingLeft: "4.5%",
												position: "absolute",
												width: "100%",
												height: "100%",
												paddingTop: "8%",
												justifyContent: "space-evenly"
											}}>
											<OffersTextBold
												style={{
													color: "#fff",
													// height: "20%",
													alignItems: "flex-end",
													fontSize: 20
												}}>
												{item.offer_details.brand_name}
											</OffersTextBold>
											<OffersTextMedium
												style={{
													color: "#fff",
													// height: "25%",
													width: "55%",
													fontSize: 16
												}}>
												{item.offer_details.text_1}
											</OffersTextMedium>
											<TouchableOpacity
												onPress={() => this.handleGetThis(item)}
												style={{
													backgroundColor: "#000",
													width: "30%",
													alignItems: "center",
													alignSelf: "flex-start",
													paddingVertical: "3%",
													marginBottom: "4%",
													borderRadius: 7
												}}>
												<OffersTextMedium style={{ color: "#fff" }}>
													{"Get this"}
												</OffersTextMedium>
											</TouchableOpacity>
										</View>
									</TouchableOpacity>
								)}
								hasParallaxImages={true}
								inactiveSlideScale={0.95}
								inactiveSlideOpacity={0.8}
								loopClonesPerSide={2}
								sliderWidth={width}
								itemWidth={width * 0.9}
								autoplay={false}
								autoplayDelay={500}
								autoplayInterval={3000}
								onSnapToItem={index =>
									this.setState({ currentSectionIndex: index })
								}
								ref={ref => (this.corouselRef = ref)}
							/>
							<View style={{ alignSelf: "center", flex: 1 }}>
								<View
									style={{
										flexDirection: "row",
										justifyContent: "center",
										paddingVertical: height / 90
									}}>
									{this.props.sections &&
										this.props.sections.map((item, index) => {
											if (
												this.corouselRef &&
												this.corouselRef.currentIndex === index
											) {
												return (
													<View
														key={index}
														style={{
															width: 15,
															height: 10,
															borderRadius: 5,
															backgroundColor: "#D8D8D8",
															marginLeft: width / 90
														}}
													/>
												);
											} else {
												return (
													<View
														key={index}
														style={{
															width: 10,
															height: 10,
															borderRadius: 5,
															backgroundColor: "#D8D8D8",
															marginLeft: width / 90
														}}
													/>
												);
											}
										})}
								</View>
							</View>
						</View>
					) : null}
					{this.props.isOffersLoading ? null : (
						<OffersTabView
							offers={this.props.offers}
							sections={this.props.sections}
							categories={this.props.categories}
							navigation={this.props.navigation}
							onRefresh={() => this.fetchAllOffers(this.props)}
						/>
					)}
				</ScrollView>
				{!this.props.isOffersLoading &&
				(this.props.offers === null || this.props.offers.length === 0) ? (
					<View
						style={{
							height: "100%",
							alignItems: "center",
							justifyContent: "center",
							backgroundColor: "#fff"
						}}>
						<NoOffer
							imageStyle={{
								width: width / 2,
								height: width / 2
							}}
							onRefresh={() => this.fetchAllOffers(this.props)}
						/>
					</View>
				) : null}
				{this.state.showOffer && (
					<BottomOffer
						open={this.state.showOffer}
						handleClose={() =>
							this.setState({ showOffer: false, selectedOffer: null })
						}
						offer={this.state.selectedOffer.offer_details}
					/>
				)}
			</React.Fragment>
		);
	}
}

Home.contextType = AlertContext;
const mapStateToProps = state => ({
	appToken: state.appToken.token,
	userSiteId: state.offers && state.offers.home.userSiteId,
	sections: state.offers && state.offers.home.sections,
	isSectionsLoading: state.offers && state.offers.home.isSectionsLoading,
	isSectionsError: state.offers && state.offers.home.isSectionsError,
	isOffersLoading: state.offers && state.offers.home.isOffersLoading,
	offers: state.offers && state.offers.home.offers,
	isOffersError: state.offers && state.offers.home.isOffersError,
	categories: state.offers && state.offers.home.categories
});

export default connect(mapStateToProps)(Home);
