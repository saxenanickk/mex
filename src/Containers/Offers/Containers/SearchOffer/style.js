import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#FCFCFC"
	},
	noSearchText: {
		width: width / 1.21,
		textAlign: "center",
		alignSelf: "center",
		marginTop: height / 3.5,
		fontSize: height / 50,
		color: "rgba(0, 0, 0, 0.5)"
	},
	offerSearchList: {
		height: height,
		paddingBottom: height / 90
	},
	offerSearchCard: {
		width: width,
		paddingVertical: 15,
		paddingHorizontal: width / 18.75,
		backgroundColor: "#F2F2F2",
		flexDirection: "row",
		alignItems: "center"
	},
	itemSeparator: {
		width: width,
		height: 0.4,
		backgroundColor: "#979797"
	},
	offerIcon: {
		width: 30,
		height: 30,
		aspectRatio: 1
	},
	offerSearchTextSection: {
		height: width / 8,
		justifyContent: "center"
	},
	offerSearchText: {
		marginLeft: width / 30,
		width: width / 1.8,
		color: "#6D7278"
	},
	offerCenter: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center"
	}
});

export default styles;
