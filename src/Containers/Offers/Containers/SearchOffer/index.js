import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	NativeModules,
	Platform,
	FlatList,
	ActivityIndicator
} from "react-native";
import FastImage from "react-native-fast-image";
import { connect } from "react-redux";
import { OffersTextBold, OffersTextRegular, NoOffer } from "../../Components";
import styles from "./style";
import { AlertContext } from "../../AlertContext";
import I18n from "../../Assets/Strings/i18n";
import { NOOFFER } from "../../Assets/Img";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import { ProgressScreen } from "../../../../Components";

const { width, height } = Dimensions.get("window");
const { UIManager } = NativeModules;

if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}

class SearchOffer extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.renderSearchCard = this.renderSearchCard.bind(this);
	}

	renderSearchCard = () => {
		return <View style={styles.offerSearchCard} />;
	};

	shouldComponentUpdate(props, state) {
		if (props.err && props.err !== this.props.err) {
			this.context.current.openAlert(
				I18n.t("oops"),
				I18n.t("offer_error_message")
			);
		}
		return true;
	}
	render() {
		const { searchText } = this.props;
		if (this.props.loading) {
			return (
				<View style={styles.offerCenter}>
					<ProgressScreen
						isMessage={true}
						indicatorSize={height / 30}
						indicatorColor={"#14314c"}
						primaryMessage={I18n.t("hang_on")}
						message={I18n.t("searching_for_offers")}
					/>
				</View>
			);
		} else {
			return (
				<View style={styles.container}>
					{searchText.trim() === "" ? (
						<OffersTextRegular style={styles.noSearchText}>
							{I18n.t("offers_search_message")}
						</OffersTextRegular>
					) : this.props.data ? (
						<FlatList
							style={styles.offerSearchList}
							data={this.props.data}
							extraData={this.props.data}
							keyExtractor={(item, index) => index.toString()}
							keyboardShouldPersistTaps={"always"}
							renderItem={({ item }) => (
								<TouchableOpacity
									activeOpacity={0.6}
									onPress={() => {
										let payload =
											item._source.item_type === "BRAND"
												? { brand_name: item._source.item_name }
												: { category: item._source.item_name };
										this.props.navigation.navigate(
											"CategorizedOffers",
											payload
										);
									}}>
									<View style={styles.offerSearchCard}>
										<View
											style={{
												padding: 5,
												backgroundColor: "#fff",
												borderRadius: 10,
												...Platform.select({
													android: { elevation: 2 },
													ios: {
														shadowOffset: { width: 2, height: 2 },
														shadowColor: "black",
														shadowOpacity: 0.3
													}
												})
											}}>
											<FastImage
												resizeMode={"contain"}
												source={{
													uri:
														item._source.item_type === "CATEGORY"
															? item._source.item_details.category_icon
															: item._source.item_details.brand_icon
												}}
												style={styles.offerIcon}
											/>
										</View>
										<View style={styles.offerSearchTextSection}>
											<OffersTextRegular
												style={styles.offerSearchText}
												size={height / 50}
												color={"#6D7277"}>
												{item._source.item_name}
											</OffersTextRegular>
											<OffersTextRegular
												style={styles.offerSearchText}
												size={height / 70}
												color={"#424446"}>
												{item._source.item_type}
											</OffersTextRegular>
										</View>
										<OffersTextRegular size={height / 60} color={"#424446"}>
											{`${item._source.offer_count} ${
												item._source.offer_count > 1
													? I18n.t("offers")
													: I18n.t("offer")
											} `}
										</OffersTextRegular>
									</View>
								</TouchableOpacity>
							)}
							ItemSeparatorComponent={() => (
								<View style={styles.itemSeparator} />
							)}
							ListEmptyComponent={() =>
								this.props.isEditing ? (
									<View
										style={{
											height: height - height / 2,
											alignItems: "center",
											justifyContent: "center"
										}}>
										<NoOffer />
									</View>
								) : null
							}
						/>
					) : null}
				</View>
			);
		}
	}
}

SearchOffer.contextType = AlertContext;
const mapStateToProps = state => {
	return {
		user: state.appToken.user,
		token: state.appToken.token,
		userSiteId: state.offers && state.offers.home.userSiteId,
		loading: state.offers && state.offers.offerSearch.isSearchOffersLoading,
		data: state.offers && state.offers.offerSearch.searchOffersData,
		err: state.offers && state.offers.offerSearch.isSearchOffersError
	};
};

export default connect(mapStateToProps)(SearchOffer);
