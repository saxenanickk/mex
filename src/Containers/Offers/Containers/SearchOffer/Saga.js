import { takeLatest, put, call } from "redux-saga/effects";
import OffersAPI from "../../Api";

export const OFFERS_GET_SEARCH_OFFERS = "OFFERS_GET_SEARCH_OFFERS";
export const OFFERS_SAVE_SEARCH_OFFERS = "OFFERS_SAVE_SEARCH_OFFERS";
export const OFFERS_SAVE_SEARCH_OFFERS_ERROR =
	"OFFERS_SAVE_SEARCH_OFFERS_ERROR";

export const offersGetSearchOffer = payload => ({
	type: OFFERS_GET_SEARCH_OFFERS,
	payload
});
export const offersSaveSearchOffer = payload => ({
	type: OFFERS_SAVE_SEARCH_OFFERS,
	payload
});

export const offersSaveSearchOfferError = payload => ({
	type: OFFERS_SAVE_SEARCH_OFFERS_ERROR,
	payload
});

export function* offersSearchSaga(dispatch) {
	yield takeLatest(OFFERS_GET_SEARCH_OFFERS, handleFetchOffers);
}

function* handleFetchOffers(action) {
	try {
		let data = yield call(OffersAPI.searchOffers, action.payload);
		let finalData = {
			keyword: action.payload.data.keyword,
			data: data
		};
		yield put(offersSaveSearchOffer(finalData));
	} catch (err) {
		yield put(offersSaveSearchOfferError());
	}
}
