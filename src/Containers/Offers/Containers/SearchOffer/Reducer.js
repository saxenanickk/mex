import {
	OFFERS_GET_SEARCH_OFFERS,
	OFFERS_SAVE_SEARCH_OFFERS,
	OFFERS_SAVE_SEARCH_OFFERS_ERROR
} from "./Saga";

const initialState = {
	searchOffersData: null,
	isSearchOffersLoading: false,
	isSearchOffersError: false,
	keyword: ""
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case OFFERS_GET_SEARCH_OFFERS:
			return {
				...state,
				isSearchOffersLoading: true,
				isSearchOffersError: false
			};
		case OFFERS_SAVE_SEARCH_OFFERS:
			return {
				...state,
				searchOffersData: action.payload.data,
				isSearchOffersLoading: false,
				keyword: action.payload.keyword
			};
		case OFFERS_SAVE_SEARCH_OFFERS_ERROR:
			return {
				...state,
				isSearchOffersLoading: false,
				isSearchOffersError: true
			};
		default:
			return state;
	}
};
