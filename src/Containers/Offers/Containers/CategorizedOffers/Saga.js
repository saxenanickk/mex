import { takeLatest, put, call } from "redux-saga/effects";
import OffersAPI from "../../Api";

export const OFFERS_GET_CATEGORIZED_OFFER = "OFFERS_GET_CATEGORIZED_OFFER";
export const OFFERS_SAVE_CATEGORIZED_OFFERS = "OFFERS_SAVE_CATEGORIZED_OFFERS";
export const OFFERS_SAVE_CATEGORIZED_OFFERS_ERROR =
	"OFFERS_SAVE_CATEGORIZED_OFFERS_ERROR";
export const OFFERS_RESET_CATEGORIZED_OFFERS =
	"OFFERS_RESET_CATEGORIZED_OFFERS";

export const offersGetCatgorizedOffer = payload => ({
	type: OFFERS_GET_CATEGORIZED_OFFER,
	payload
});

export const offersSaveCatgorizedOffer = payload => ({
	type: OFFERS_SAVE_CATEGORIZED_OFFERS,
	payload
});

export const offersSaveCatgorizedOfferError = payload => ({
	type: OFFERS_SAVE_CATEGORIZED_OFFERS_ERROR,
	payload
});

export const offersResetCatgorizedOffer = payload => ({
	type: OFFERS_RESET_CATEGORIZED_OFFERS,
	payload
});
export function* offersListSaga(dispatch) {
	yield takeLatest(OFFERS_GET_CATEGORIZED_OFFER, handleFetchOffers);
}

function* handleFetchOffers(action) {
	try {
		let data = yield call(OffersAPI.getOffers, action.payload);
		yield put(offersSaveCatgorizedOffer(data));
	} catch (err) {
		yield put(offersSaveCatgorizedOfferError(true));
	}
}
