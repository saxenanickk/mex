import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
	categorizedHeader: {
		marginVertical: height / 35,
		flexDirection: "row",
		justifyContent: "space-between",
		flexWrap: "wrap"
	},
	categorizedHeaderRight: {
		color: "#424446",
		alignSelf: "center"
	},
	categorizedHeaderLeft: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center"
	},
	categorizedCenter: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center"
	},
	categorizedBackIcon: {
		marginTop: height / 232,
		color: "#777777"
	},
	categorizedTitle: {
		marginLeft: width / 19,
		fontSize: height / 30,
		color: "#3C5063"
	},
	categorizedBoundarySpace: {
		marginHorizontal: width / 18.75,
		flex: 1
	}
});
