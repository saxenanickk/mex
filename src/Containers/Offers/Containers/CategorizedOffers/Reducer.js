import {
	OFFERS_GET_CATEGORIZED_OFFER,
	OFFERS_SAVE_CATEGORIZED_OFFERS,
	OFFERS_SAVE_CATEGORIZED_OFFERS_ERROR,
	OFFERS_RESET_CATEGORIZED_OFFERS
} from "./Saga";

const initialState = {
	name: "",
	isCategorizedLoading: false,
	categorizedOffers: null,
	categorizedOffersError: false
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case OFFERS_GET_CATEGORIZED_OFFER:
			return {
				...state,
				name: action.payload.name,
				isCategorizedLoading: true,
				categorizedOffersError: false
			};
		case OFFERS_SAVE_CATEGORIZED_OFFERS:
			return {
				...state,
				categorizedOffers: action.payload,
				isCategorizedLoading: false
			};
		case OFFERS_SAVE_CATEGORIZED_OFFERS_ERROR:
			return {
				...state,
				categorizedOffersError: action.payload,
				isCategorizedLoading: false
			};
		case OFFERS_RESET_CATEGORIZED_OFFERS:
			return {
				...state,
				categorizedOffers: null,
				name: "",
				isCategorizedLoading: false,
				categorizedOffersError: false
			};
		default:
			return state;
	}
};
