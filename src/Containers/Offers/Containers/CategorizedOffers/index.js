import React from "react";
import {
	View,
	ActivityIndicator,
	TouchableOpacity,
	Dimensions
} from "react-native";
import { connect } from "react-redux";
import { OffersTextBold, OffersTextRegular, NoOffer } from "../../Components";
import { Icon, ProgressScreen } from "../../../../Components";
import { OffersFlatList } from "../../Components";
import {
	offersGetCatgorizedOffer,
	offersResetCatgorizedOffer,
	offersSaveCatgorizedOfferError
} from "./Saga";
import { AlertContext } from "../../AlertContext";
import styles from "./style";
import I18n from "../../Assets/Strings/i18n";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";

const { width, height } = Dimensions.get("window");
const HeaderBar = props => {
	return (
		<View>
			<View style={styles.categorizedHeader}>
				<View style={styles.categorizedHeaderLeft}>
					<TouchableOpacity onPress={props.onBackPress}>
						<Icon
							iconType={"ionicon"}
							iconName={"md-arrow-back"}
							iconSize={25}
							style={styles.categorizedBackIcon}
						/>
					</TouchableOpacity>
					<OffersTextBold style={styles.categorizedTitle}>{`${
						props.name
					}`}</OffersTextBold>
				</View>
				<OffersTextRegular style={styles.categorizedHeaderRight}>
					{props.length > 0
						? `${props.length} ${
								props.length > 1 ? I18n.t("offers") : I18n.t("offer")
						  }`
						: null}
				</OffersTextRegular>
			</View>
		</View>
	);
};

class CategorizedOffers extends React.Component {
	componentDidMount() {
		const { route } = this.props;
		const { category, brand_name } = route.params ? route.params : {};

		const categories = category;

		if (this.props.userSiteId) {
			let action;
			if (categories) {
				action = offersGetCatgorizedOffer({
					appToken: this.props.token,
					siteId: this.props.userSiteId,
					categories: categories,
					name: categories
				});
				GoAppAnalytics.trackWithProperties("category_offers_list", {
					name: categories
				});
			}
			if (brand_name) {
				action = offersGetCatgorizedOffer({
					appToken: this.props.token,
					siteId: this.props.userSiteId,
					brand_name: brand_name,
					name: brand_name
				});
				GoAppAnalytics.trackWithProperties("brand_offers_list", {
					name: brand_name
				});
			}
			this.props.dispatch(action);
		}
	}

	shouldComponentUpdate(props, state) {
		if (props.error && props.error !== this.props.error) {
			this.context.current.openAlert(
				I18n.t("oops"),
				I18n.t("offer_error_message")
			);
			this.props.dispatch(offersSaveCatgorizedOfferError(false));
		}
		return true;
	}
	componentWillUnmount() {
		this.props.dispatch(offersResetCatgorizedOffer());
	}
	render() {
		return (
			<View style={styles.categorizedBoundarySpace}>
				<HeaderBar
					onBackPress={() => {
						this.props.navigation.goBack();
					}}
					name={this.props.name}
					length={this.props.data ? this.props.data.length : null}
				/>
				{this.props.loading || this.props.data === null ? (
					<View style={styles.categorizedCenter}>
						<ProgressScreen
							isMessage={true}
							indicatorSize={height / 30}
							indicatorColor={"#14314c"}
							primaryMessage={I18n.t("hang_on")}
							message={I18n.t("loading_offers_for_you")}
						/>
					</View>
				) : this.props.data.length > 0 ? (
					<OffersFlatList
						navigation={this.props.navigation}
						data={this.props.data}
					/>
				) : (
					<View style={styles.categorizedCenter}>
						<NoOffer />
					</View>
				)}
			</View>
		);
	}
}
CategorizedOffers.contextType = AlertContext;

const mapStateToProps = state => ({
	user: state.appToken.user,
	token: state.appToken.token,
	userSiteId: state.offers.home.userSiteId,
	data: state.offers.categorizedOffer.categorizedOffers,
	loading: state.offers.categorizedOffer.isCategorizedLoading,
	name: state.offers.categorizedOffer.name,
	error: state.offers.categorizedOffer.categorizedOffersError
});
export default connect(mapStateToProps)(CategorizedOffers);
