import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");
const theme_color = "#15314C";
export const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "rgba(0,0,0,0.5)"
	},
	couponMainView: {
		position: "absolute",
		bottom: 0,
		width: width,
		paddingVertical: height / 33.6,
		borderTopLeftRadius: width / 30,
		borderTopRightRadius: width / 30,
		backgroundColor: "#fff",
		alignItems: "center"
	},
	couponBottomView: {
		width: width,
		paddingVertical: height / 33.6,
		paddingHorizontal: width / 10.58,
		backgroundColor: "#fff",
		alignItems: "center"
	},
	closeButton: {
		height: width / 10,
		width: width / 10,
		borderRadius: width / 20,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#fff"
	},
	closeButtonBackground: {
		backgroundColor: "#ececec",
		height: width / 10,
		width: width / 10,
		marginRight: width / 20,
		borderRadius: width / 20,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "flex-end"
	},
	couponHeaderText: {
		marginTop: height / 60,
		textAlign: "center",
		fontSize: height / 55,
		lineHeight: height / 35,
		width: width / 1.21,
		color: "#rgba(0, 0, 0, 0.5)"
	},
	offerCodeView: {
		borderWidth: 1,
		borderStyle: "solid",
		borderColor: theme_color,
		marginTop: height / 28,
		width: width / 1.21,
		height: height / 17.4,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 60
	},
	offerCodeText: {
		fontSize: height / 28,
		color: theme_color,
		width: width / 1.25,
		textAlign: "center"
	},
	bottomButtonSection: {
		marginTop: height / 19.57,
		width: width / 1.21,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between"
	},
	bottomMiniButton: {
		width: width / 3,
		height: height / 17.4,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 50,
		borderColor: theme_color,
		borderWidth: 1
	},
	bottomButtonText: {
		width: width / 4,
		color: theme_color,
		fontSize: height / 55,
		textAlign: "center"
	},
	getSMSText: {
		marginTop: height / 31.32,
		width: width / 2,
		textAlign: "right",
		fontSize: height / 55,
		textDecorationLine: "underline",
		color: "rgba(0,0,0,0.5)"
	},
	smsButton: {
		width: width / 3,
		alignSelf: "flex-end",
		alignItems: "flex-end"
	},
	smsLoadingStyle: {
		padding: 10
	},
	fullSizeButton: {
		width: width / 1.21,
		flexDirection: "row",
		alignItems: "center",
		height: height / 17.4,
		borderRadius: width / 50,
		backgroundColor: theme_color
	},
	fullSizeText: {
		width: width / 1.21,
		color: "#fff",
		fontSize: height / 55,
		textAlign: "center"
	}
});
