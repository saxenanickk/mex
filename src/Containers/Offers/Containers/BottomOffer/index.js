import React, { Component } from "react";
import {
	Modal,
	View,
	Dimensions,
	TouchableOpacity,
	Clipboard,
	ActivityIndicator,
	Linking,
	Platform
} from "react-native";
import { Icon, GoToast } from "../../../../Components";
import {
	OffersTextBold,
	OffersTextMedium,
	OffersTextRegular
} from "../../Components";
import { connect } from "react-redux";
import {
	offersSaveOfferCodeSuccess,
	offersGetOfferCode,
	offersSaveOfferCodeError
} from "../OfferDetail/Saga";
import { styles } from "./style";
import { AlertContext } from "../../AlertContext";
import { AppList } from "../../../Goapp/AppList";
import Api from "../../Api";
import I18n from "../../Assets/Strings/i18n";
import MiniApp from "../../../../CustomModules/MiniApp";
import { NONUMBER } from "../../Assets/Img";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
const { height } = Dimensions.get("window");

const COUPON_WITH_APP = 1;
const COUPON_WITH_LINK = 2;
const COUPON_WITH_NOTHING = 3;
const NOCOUPON_WITH_APP = 4;
const NOCOUPON_WITH_LINK = 5;
const NOCOUPON = 6;

class BottomOffer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			uiViewType: null,
			isLoadingOffer: true,
			isSmsSending: false
		};
	}

	componentDidMount() {
		const { offer } = this.props;
		this.props.dispatch(
			offersGetOfferCode({
				appToken: this.props.appToken,
				siteId: this.props.userSiteId,
				offerId: offer.id
			})
		);
	}

	handleCopyCode = () => {
		try {
			Clipboard.setString(this.props.offerCode.code);
			GoAppAnalytics.trackWithProperties("offer_redeem_code_copy", {
				id: this.props.offer.id,
				name: this.props.offer.brand_name
			});
			GoToast.show("Copied Code", "Info", "LONG");
		} catch (error) {
			console.log("error", error);
		}
	};

	handleGetSms = async () => {
		try {
			this.setState({ isSmsSending: true });
			GoAppAnalytics.trackWithProperties("offer_get_sms", {
				id: this.props.offer.id,
				name: this.props.offer.brand_name
			});
			let smsStatus = await Api.getSms({
				appToken: this.props.appToken,
				redemptionId: this.props.offerCode.redemption_id
			});
			if (smsStatus.success === 1) {
				GoToast.show("Sms sent", "Info", "LONG");
			} else {
				if (smsStatus.status_message === "USER_PHONE_NUMBER_NOT_FOUND") {
					this.context.current.openAlert(
						"Mobile Number Update!",
						"Please update your mobile number in the Profile section to get the offer SMS.",
						[],
						NONUMBER
					);
				} else {
					GoToast.show("Failed to send sms, please try again", "Info", "LONG");
				}
			}
		} catch (error) {
			GoToast.show("Failed to send sms, please try again", "Info", "LONG");
		} finally {
			this.setState({ isSmsSending: false });
		}
	};

	shouldComponentUpdate(props, state) {
		if (
			props.offerCode &&
			props.offerCode !== this.props.offerCode &&
			props.offerCode.status === 1
		) {
			this.checkAndMakeCondition(props);
		}

		if (
			props.offerCode &&
			props.offerCode !== this.props.offerCode &&
			props.offerCode.status_message === "USER_REDEMPTION_LIMIT_EXCEEDED"
		) {
			props.handleClose();
			setTimeout(() => {
				this.context.current.openAlert(
					I18n.t("Oops"),
					I18n.t("user_redemption_limit_exceeded")
				);
			}, 500);
		}

		if (
			props.offerCode &&
			props.offerCode !== this.props.offerCode &&
			props.offerCode.status_message === "TOTAL_REDEMPTION_LIMIT_EXCEEDED"
		) {
			props.handleClose();
			setTimeout(() => {
				this.context.current.openAlert(
					I18n.t("Oops"),
					I18n.t("total_redemption_limit_exceeded")
				);
			}, 500);
		}

		if (
			props.offerCode &&
			props.offerCode !== this.props.offerCode &&
			props.offerCode.status_message === "OFFER_EXPIRED"
		) {
			props.handleClose();
			setTimeout(() => {
				this.context.current.openAlert(I18n.t("Oops"), I18n.t("offer_expired"));
			}, 500);
		}

		if (
			props.isOfferCodeError &&
			props.isOfferCodeError !== this.props.isOfferCodeError
		) {
			props.handleClose();
			setTimeout(() => {
				this.context.current.openAlert(
					I18n.t("Oops"),
					I18n.t("something_wrong")
				);
			}, 500);
		}
		return true;
	}

	openUrl = url => {
		GoAppAnalytics.trackWithProperties("offer_redeem", {
			id: this.props.offer.id,
			name: this.props.offer.brand_name,
			url: url
		});
		if (url.startsWith("http://") || url.startsWith("https://")) {
			Linking.openURL(url);
		} else {
			Linking.openURL(`http://${url}`);
		}
	};

	onAppClick = app_name => {
		try {
			let item = AppList.filter(item => item.name === app_name);
			GoAppAnalytics.trackWithProperties("offer_redeem_app_launch", {
				id: this.props.offer.id,
				app: app_name,
				name: this.props.offer.brand_name
			});
			if (item.length > 0) {
				switch (item[0].type) {
					case "PWA":
						//uncomment this code when trying to launch web app
						if (app_name === "Azgo" || app_name === "Hungerbox") {
							MiniApp.launchPhoneVerifiedApp(
								app_name,
								this.props.userProfile,
								this.props.navigation
							);
						} else {
							MiniApp.launchWebApp(app_name, item[0].url);
						}

						break;
					case "SWIGGY":
						MiniApp.launchNativeApp("swiggy");
						break;
					case "BBInstant":
						if (Platform.OS === "android") {
							MiniApp.launchPhoneVerifiedApp(
								"bbinstant",
								this.props.userProfile,
								this.props.navigation
							);
						} else {
							this.context.current.openAlert(
								"Info!",
								"BBInstant coming soon on iOS. Currently available only on Android."
							);
						}
						break;
					case "NATIVE":
						if (
							app_name === "Community" ||
							app_name === "Vms" ||
							app_name === "Ignite"
						) {
							MiniApp.launchCorporateVerifyApp(
								app_name,
								this.props.userProfile,
								this.props.navigation
							);
						} else {
							MiniApp.launchApp(this.props.navigation, app_name);
						}
						break;
					case "SERVICE":
						MiniApp.launchApp(this.props.navigation, app_name);
					default:
						console.log("do nothing");
				}
			}
		} catch (error) {
			console.log("unable to open app", error);
		} finally {
			this.props.handleClose();
		}
	};

	renderOfferCoupon(coupon, app_name, brand_link, message) {
		return (
			<React.Fragment>
				<OffersTextMedium style={styles.couponHeaderText} numberOfLines={3}>
					{message}
				</OffersTextMedium>
				{coupon ? (
					<View style={styles.offerCodeView}>
						<OffersTextBold style={styles.offerCodeText} numberOfLines={1}>
							{coupon}
						</OffersTextBold>
					</View>
				) : (
					<View style={styles.offerCodeView}>
						<OffersTextBold style={styles.offerCodeText} numberOfLines={1}>
							{I18n.t("no_code_required")}
						</OffersTextBold>
					</View>
				)}
				<View style={styles.bottomButtonSection}>
					{coupon && (
						<TouchableOpacity
							onPress={this.handleCopyCode}
							style={styles.bottomMiniButton}>
							<OffersTextMedium
								style={styles.bottomButtonText}
								numberOfLines={1}>
								{I18n.t("copy_code")}
							</OffersTextMedium>
						</TouchableOpacity>
					)}
					{app_name ? (
						<TouchableOpacity
							onPress={() => this.onAppClick(app_name)}
							style={coupon ? styles.bottomMiniButton : styles.fullSizeButton}>
							<OffersTextMedium
								style={coupon ? styles.bottomButtonText : styles.fullSizeText}
								numberOfLines={1}>
								{I18n.t("launch_app")}
							</OffersTextMedium>
						</TouchableOpacity>
					) : (
						<TouchableOpacity
							onPress={() => this.openUrl(brand_link)}
							style={coupon ? styles.bottomMiniButton : styles.fullSizeButton}>
							<OffersTextMedium
								style={coupon ? styles.bottomButtonText : styles.fullSizeText}
								numberOfLines={1}>
								{I18n.t("brand_link")}
							</OffersTextMedium>
						</TouchableOpacity>
					)}
				</View>
				{coupon &&
					(this.state.isSmsSending ? (
						<View style={styles.smsButton}>
							<ActivityIndicator
								style={styles.smsLoadingStyle}
								size={height / 75}
							/>
						</View>
					) : (
						<TouchableOpacity
							style={styles.smsButton}
							onPress={this.handleGetSms}>
							<OffersTextRegular numberOfLines={1} style={styles.getSMSText}>
								{I18n.t("get_sms")}
							</OffersTextRegular>
						</TouchableOpacity>
					))}
			</React.Fragment>
		);
	}

	renderEmptyCoupons = (coupon, message) => {
		return (
			<React.Fragment>
				<OffersTextMedium style={styles.couponHeaderText} numberOfLines={3}>
					{message}
				</OffersTextMedium>
				{coupon ? (
					<View style={styles.offerCodeView}>
						<OffersTextBold style={styles.offerCodeText} numberOfLines={1}>
							{coupon}
						</OffersTextBold>
					</View>
				) : (
					<View style={styles.offerCodeView}>
						<OffersTextBold style={styles.offerCodeText} numberOfLines={1}>
							{I18n.t("no_code_required")}
						</OffersTextBold>
					</View>
				)}
				{coupon &&
					(this.state.isSmsSending ? (
						<View style={styles.smsButton}>
							<ActivityIndicator style={styles.smsLoadingStyle} />
						</View>
					) : (
						<TouchableOpacity
							style={styles.smsButton}
							onPress={this.handleGetSms}>
							<OffersTextRegular numberOfLines={1} style={styles.getSMSText}>
								{I18n.t("get_sms")}
							</OffersTextRegular>
						</TouchableOpacity>
					))}
			</React.Fragment>
		);
	};
	renderOfferCodeSection() {
		const { uiViewType } = this.state;
		const { offerCode, offer } = this.props;
		switch (uiViewType) {
			case COUPON_WITH_LINK:
				return this.renderOfferCoupon(
					offerCode.code,
					null,
					offer.url,
					I18n.t("brand_link_to_avail_this_offer")
				);
			case COUPON_WITH_APP:
				return this.renderOfferCoupon(
					offerCode.code,
					offer.marketplace_app_name,
					null,
					I18n.t("mex_partner_app_please_avail_inside_mex")
				);
			case COUPON_WITH_NOTHING:
				return this.renderEmptyCoupons(
					offerCode.code,
					I18n.t("code_at_partner_outlet_to_avail_offer")
				);
			case NOCOUPON_WITH_LINK:
				return this.renderOfferCoupon(
					null,
					null,
					offer.url,
					I18n.t("brand_link_to_avail_this_offer")
				);
			case NOCOUPON_WITH_APP:
				return this.renderOfferCoupon(
					null,
					offer.marketplace_app_name,
					null,
					I18n.t("mex_partner_app_please_avail_inside_mex")
				);
			case NOCOUPON:
				return this.renderEmptyCoupons(
					null,
					I18n.t("go_to_partner_outlet_to_avail_offer")
				);
			default:
				return null;
		}
	}
	checkAndMakeCondition(props) {
		const { offer, offerCode } = props;
		let uiViewType = null;
		if (offerCode.code && offerCode.code.trim() !== "") {
			if (offer.url) {
				uiViewType = COUPON_WITH_LINK;
			} else if (offer.marketplace_app_name) {
				uiViewType = COUPON_WITH_APP;
			} else {
				uiViewType = COUPON_WITH_NOTHING;
			}
		} else {
			if (offer.url) {
				uiViewType = NOCOUPON_WITH_LINK;
			} else if (offer.marketplace_app_name) {
				uiViewType = NOCOUPON_WITH_APP;
			} else {
				uiViewType = NOCOUPON;
			}
		}
		this.setState({ uiViewType, isLoadingOffer: false });
	}

	componentWillUnmount() {
		this.props.dispatch(offersSaveOfferCodeSuccess(null));
		this.props.dispatch(offersSaveOfferCodeError(false));
	}

	render() {
		const { open, handleClose } = this.props;
		return (
			<Modal
				visible={open}
				onRequestClose={handleClose}
				transparent={true}
				animationType={"slide"}
				onDismiss={handleClose}>
				<TouchableOpacity onPress={handleClose} style={styles.container}>
					<TouchableOpacity style={styles.couponMainView} activeOpacity={1}>
						<View style={styles.closeButtonBackground}>
							<TouchableOpacity
								style={styles.closeButton}
								onPress={handleClose}>
								<Icon
									iconType={"ionicon"}
									iconName={"ios-close"}
									iconSize={height / 24}
									iconColor={"#000000"}
								/>
							</TouchableOpacity>
						</View>
						<View style={styles.couponBottomView}>
							{this.state.isLoadingOffer ? (
								<ActivityIndicator size={"large"} />
							) : (
								this.renderOfferCodeSection()
							)}
						</View>
					</TouchableOpacity>
				</TouchableOpacity>
			</Modal>
		);
	}
}

BottomOffer.contextType = AlertContext;
const mapStateToProps = state => ({
	isOfferCodeLoading:
		state.offers && state.offers.offerDetails.isOfferCodeLoading,
	offerCode: state.offers && state.offers.offerDetails.offerCode,
	isOfferCodeError: state.offers && state.offers.offerDetails.isOfferCodeError,
	appToken: state.appToken.token,
	userSiteId: state.offers && state.offers.home.userSiteId,
	userProfile: state.appToken.userProfile
});

export default connect(mapStateToProps)(BottomOffer);
