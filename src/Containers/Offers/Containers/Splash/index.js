import React, { Component } from "react";
import { View } from "react-native";
import { connect } from "react-redux";
import ApplicationToken from "../../../../CustomModules/ApplicationToken";
import { offersSaveUserSiteId } from "../Main/Saga";
import { CommonActions } from "@react-navigation/native";

class Splash extends Component {
	componentDidMount() {
		let offerId = this.handleDeepLink(this.props.params);
		if (this.props.appToken !== null) {
			this.props.dispatch(offersSaveUserSiteId(this.props.siteId));
			this.navigateToScreen(offerId);
		}
	}

	navigateToScreen = offerId => {
		if (offerId) {
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "OfferDetail", params: { id: offerId } }]
				})
			);
		} else {
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "Main" }]
				})
			);
		}
	};
	handleDeepLink = params => {
		let deepLinkParams = params ? JSON.parse(params) : null;
		if (deepLinkParams !== null) {
			return deepLinkParams.offer_id;
		}
		return null;
	};
	/**
	 * @param {{ appToken: String; }} prevProps
	 * @param {any} prevState
	 */
	componentDidUpdate(prevProps, prevState) {
		let offerId = this.handleDeepLink(this.props.params);
		if (
			prevProps.appToken === null &&
			this.props.appToken !== prevProps.appToken
		) {
			this.props.dispatch(offersSaveUserSiteId(this.props.userProfile.site_id));
			this.navigateToScreen(offerId);
		}
	}

	render() {
		return (
			<View
				style={{
					flex: 1,
					backgroundColor: "#ffffff",
					justifyContent: "center",
					alignItems: "center"
				}}>
				<ApplicationToken />
			</View>
		);
	}
}

/**
 * @param {{ appToken: { token: String;userProfile: String }; }} state
 */
function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		userProfile: state.appToken.userProfile,
		user: state.appToken.user,
		siteId:
			state.appToken && state.appToken.selectedSite
				? state.appToken.selectedSite.id
				: null
	};
}

export default connect(mapStateToProps)(Splash);
