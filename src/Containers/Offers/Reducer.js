import { combineReducers } from "redux";

import { reducer as offersHomeReducer } from "./Containers/Main/Reducer";
import { reducer as offerDetailsReducer } from "./Containers/OfferDetail/Reducer";
import { reducer as offerCategorizedReducer } from "./Containers/CategorizedOffers/Reducer";
import { reducer as offerSearchReducer } from "./Containers/SearchOffer/Reducer";

const initialState = {
	error: null
};

const mainReducer = (state = initialState, action) => {
	switch (action.type) {
		default:
			return state;
	}
};

const offersReducer = combineReducers({
	mainReducer,
	offerDetails: offerDetailsReducer,
	home: offersHomeReducer,
	categorizedOffer: offerCategorizedReducer,
	offerSearch: offerSearchReducer
});

export default offersReducer;
