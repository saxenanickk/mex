import React from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import reducer from "./Reducer";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";
import { getNewReducer, removeExistingReducer } from "../../../App";

export default class Offers extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "offers" });
		getNewReducer({ name: "offers", reducer: reducer });
	}

	componentWillUnmount() {
		removeExistingReducer("offers");
	}

	render() {
		return <App {...this.props} />;
	}
}

AppRegistry.registerComponent("Offers", () => Offers);
