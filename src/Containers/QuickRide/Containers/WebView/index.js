import React, { Component } from "react";
import { View, Dimensions } from "react-native";
import WebView from "react-native-webview";
import { connect } from "react-redux";
import { ProgressScreen, DrawerOpeningHack } from "../../../../Components";
import Config from "react-native-config";

const { SERVER_BASE_URL_SESSION, SERVER_BASE_URL_QUICKRIDE_LOGIN } = Config;
const { width } = Dimensions.get("window");

class QuickRide extends Component {
	constructor() {
		super();
		this.state = {
			dataLaoded: false,
			uri: SERVER_BASE_URL_QUICKRIDE_LOGIN
		};
	}

	componentDidMount() {
		fetch(SERVER_BASE_URL_SESSION, {
			method: "POST",
			headers: {
				"x-access-token": this.props.appToken
			}
		})
			.then(response => response.json())
			.then(res => {
				if (res.success) {
					let URL = this.state.uri.slice();
					URL = `${URL}?qrUserId=${res.data.qrUserId}&appToken=${
						res.data.appToken
					}&authorization=${res.data.authorization}&rmzReferenceId=${
						res.data.rmzReferenceId
					}`;

					this.setState({ uri: URL, dataLaoded: true });
				} else {
					this.setState({ dataLaoded: true });
				}
			})
			.catch(error => {
				console.log(error);
				this.setState({ dataLaoded: true });
			});
	}

	render() {
		return (
			<View style={{ flex: 1 }}>
				{!this.state.dataLaoded ? (
					<ProgressScreen
						isMessage={true}
						message={"We are loading..."}
						indicatorColor={"#3398f0"}
						indicatorSize={width / 15}
						primaryMessage={"Hang On"}
						messageBar={{ width: width / 1.5 }}
					/>
				) : (
					<WebView
						source={{ uri: this.state.uri }}
						cacheEnabled={false}
						javaScriptEnabled={true}
						style={{ flex: 1 }}
					/>
				)}
				<DrawerOpeningHack />
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null
	};
}

export default connect(
	mapStateToProps,
	null,
	null
)(QuickRide);
