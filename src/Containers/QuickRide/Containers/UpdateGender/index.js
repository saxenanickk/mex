import React, { Component } from "react";
import { View, Dimensions, TouchableOpacity, Image } from "react-native";
import { connect } from "react-redux";
import {
	MALESELECTED,
	MALEUNSELECTED,
	FEMALESELECTED,
	FEMALEUNSELECTED,
	ANOYMOUSSELECTED,
	ANOYMOUSUNSELECTED
} from "../../../../Assets/Img/Image";
import { CustomAlert, ProgressScreen } from "../../../../Components";
import {
	GoappTextRegular,
	GoappTextMedium
} from "../../../../Components/GoappText";

import Config from "react-native-config";

const SERVER_BASE_URL_CUSTOMER = Config.SERVER_BASE_URL_CUSTOMER;

const { width, height } = Dimensions.get("window");

class UpdateGender extends Component {
	constructor(props) {
		super(props);
		this.state = {
			open: false,
			gender: null,
			userInfo: null
		};
		this.alertRef = React.createRef();
		this.gender = [
			{
				value: "MALE",
				type: "Male",
				selectedImage: MALESELECTED,
				unselectedImage: MALEUNSELECTED
			},
			{
				value: "FEMALE",
				type: "Female",
				selectedImage: FEMALESELECTED,
				unselectedImage: FEMALEUNSELECTED
			},
			{
				value: "UNKNOWN",
				type: "Not to say",
				selectedImage: ANOYMOUSSELECTED,
				unselectedImage: ANOYMOUSUNSELECTED
			}
		];
		this.checkUserProfile = this.checkUserProfile.bind(this);
	}

	componentDidMount() {
		this.props.appToken && this.checkUserProfile(this.props.appToken);
	}

	shouldComponentUpdate(props, state) {
		if (props.appToken && this.props.appToken === null) {
			this.checkUserProfile(props.appToken);
		}
		return true;
	}

	async checkUserProfile(token) {
		try {
			const response = await this.getUserProfile(token);
			if (response.success === 1 && response.data) {
				let temp = this.gender.filter(
					item => item.value === response.data.gender
				);
				if (temp.length <= 0) {
					this.setState({ open: true, gender: response.data.gender });
				} else {
					this.props.navigation.reset([
						NavigationActions.navigate({
							routeName: "WebView"
						})
					]);
				}
			} else {
				throw new Error("Gender not found");
			}
		} catch (error) {
			console.log("error is", error);
			this.setState({ open: true });
		}
	}

	getUserProfile(token) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(`${SERVER_BASE_URL_CUSTOMER}/getUserProfile`, {
					method: "GET",
					headers: {
						"Content-Type": "application/json",
						"X-ACCESS-TOKEN": token
					}
				}).then(response => {
					response
						.json()
						.then(res => {
							resolve(res);
						})
						.catch(err => reject(err));
				});
			} catch (error) {
				reject(error);
			}
		});
	}

	updateGender() {
		const { appToken } = this.props;
		const { gender } = this.state;
		this.setState({ isLoading: true });
		return new Promise(function(resolve, reject) {
			try {
				fetch(`${SERVER_BASE_URL_CUSTOMER}/updateGender`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						"X-ACCESS-TOKEN": appToken
					},
					body: JSON.stringify({ gender })
				}).then(response => {
					response
						.json()
						.then(res => {
							resolve(res);
						})
						.catch(err => reject(err));
				});
			} catch (error) {
				reject(error);
			}
		});
	}

	async updateUserGender() {
		try {
			const resp = await this.updateGender();
			if (resp.success === 1) {
				this.setState({ open: false, isLoading: false });
				this.props.navigation.reset([
					NavigationActions.navigate({
						routeName: "WebView"
					})
				]);
			} else {
				throw new Error("Unable to update gender");
			}
		} catch (error) {
			this.setState({ isLoading: false });
			this.alertRef.current.openAlert(
				"Oops!",
				"Unable to update gender,Try after some time."
			);
		}
	}

	setOpen = value => this.setState({ open: value });

	handleClose = () => {
		this.setOpen(false);
	};

	handleCancelClick = () => {
		this.handleClose();
	};

	render() {
		const { open } = this.state;
		if (open) {
			return (
				<View style={{ flex: 1 }}>
					<CustomAlert ref={this.alertRef} />

					{this.state.isLoading && (
						<ProgressScreen
							isMessage={true}
							indicatorSize={height / 30}
							indicatorColor={"#3398f0"}
							primaryMessage={"Hang On..."}
							message={"Updating Gender"}
						/>
					)}
					<View
						style={{
							flex: 1,
							justifyContent: "center",
							backgroundColor: "#fff",
							paddingHorizontal: width / 20
						}}>
						<View style={{ paddingHorizontal: width / 20 }}>
							<GoappTextRegular
								style={{ color: "#9a9ea7", fontSize: height / 50 }}>
								{"Gender"}
							</GoappTextRegular>
							<View
								style={{
									flexDirection: "row",
									marginTop: height / 60,
									width: width - width / 5,

									justifyContent: "space-between"
								}}>
								{this.gender.map(item => (
									<TouchableOpacity
										onPress={() => this.setState({ gender: item.value })}
										key={item.value}
										style={[
											{
												width: (4 * width) / 18,
												borderRadius: width / 50,
												backgroundColor: "#f8f9fc",
												justifyContent: "center",
												alignItems: "center",
												paddingVertical: height / 60,
												paddingHorizontal: width / 200
											},
											this.state.gender === item.value
												? { borderWidth: 1, borderColor: "#3398f0" }
												: null
										]}>
										<Image
											source={
												this.state.gender === item.value
													? item.selectedImage
													: item.unselectedImage
											}
											style={{ width: width / 15, height: width / 15 }}
											resizeMode={"contain"}
										/>
										<GoappTextRegular
											style={{
												color:
													this.state.gender === item.value ? "#3398f0" : "#000",
												fontSize: height / 60
											}}>
											{item.type}
										</GoappTextRegular>
									</TouchableOpacity>
								))}
							</View>
						</View>
						{this.state.gender && (
							<TouchableOpacity
								onPress={() => this.updateUserGender()}
								style={{
									position: "absolute",
									bottom: height / 10,
									backgroundColor: "#3398f0",
									justifyContent: "center",
									alignItems: "center",
									alignSelf: "center",
									width: width - width / 5,
									paddingVertical: height / 70,
									borderRadius: width / 20
								}}>
								<GoappTextMedium
									style={{ color: "#fff", fontSize: height / 50 }}>
									{"Next"}
								</GoappTextMedium>
							</TouchableOpacity>
						)}
					</View>
				</View>
			);
		} else {
			return (
				<ProgressScreen
					isMessage={true}
					indicatorSize={height / 30}
					indicatorColor={"#3398f0"}
					primaryMessage={"Hang On..."}
					message={"Fetching Details"}
				/>
			);
		}
	}
}

function mapStateToProps(state) {
	return {
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null
	};
}

export default connect(mapStateToProps, null, null)(UpdateGender);
