import UpdateGender from "./Containers/UpdateGender";
import WebView from "./Containers/WebView";

const RegisterScreen = createStackNavigator(
	{
		UpdateGender: {
			screen: UpdateGender
		},
		WebView: {
			screen: WebView
		}
	},
	{
		headerMode: "none"
	}
);

export default createAppContainer(RegisterScreen);
