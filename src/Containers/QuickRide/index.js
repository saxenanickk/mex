import App from "./App";
import React from "react";
import { AppRegistry } from "react-native";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";

export default class QuickRide extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "quickride" });
	}

	render() {
		return <App />;
	}
}

AppRegistry.registerComponent("QuickRide", () => QuickRide);
