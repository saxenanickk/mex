import React, { Fragment } from "react";
import { SafeAreaView, StatusBar } from "react-native";
import RegisterScreen from "./RegisterScreen";
import GoAppAnalytics from "../../utils/GoAppAnalytics";
import ApplicationToken from "../../CustomModules/ApplicationToken";

export default class App extends React.Component {
	render() {
		return (
			<Fragment>
				<SafeAreaView style={{ flex: 0, backgroundColor: "green" }} />
				<SafeAreaView style={{ flex: 1 }}>
					<StatusBar backgroundColor={"green"} barStyle={"light-content"} />
					<ApplicationToken />
					<RegisterScreen
						navigation={this.props.navigation}
						onNavigationStateChange={(prevState, currState) =>
							GoAppAnalytics.logChangeOfScreenInStack(
								"quickride",
								prevState,
								currState
							)
						}
					/>
				</SafeAreaView>
			</Fragment>
		);
	}
}
