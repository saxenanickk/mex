import { combineReducers } from "redux";
import { reducer as searchHotel } from "./Containers/SearchHotel/Reducer";
import { reducer as splash } from "./Containers/Splash/Reducer";
import { reducer as selectHotel } from "./Containers/SelectHotel/Reducer";
import { reducer as selectRoom } from "./Containers/SelectRoom/Reducer";
import { reducer as bookHotel } from "./Containers/BookHotel/Reducer";
import { reducer as tripDetail } from "./Containers/TripDetail/Reducer";

const ctHotelReducer = combineReducers({
	searchHotel: searchHotel,
	splash: splash,
	selectHotel: selectHotel,
	selectRoom: selectRoom,
	bookHotel: bookHotel,
	tripDetail: tripDetail
});

export default ctHotelReducer;
