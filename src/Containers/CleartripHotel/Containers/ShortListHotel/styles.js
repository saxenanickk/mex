import { StyleSheet, Dimensions, Platform } from "react-native";

const { height, width } = Dimensions.get("window");

const styles = StyleSheet.create({
	rowView: { flexDirection: "row" },
	mapView: {
		height: height,
		width: width
	},
	cityText: {
		fontSize: height / 60
	},
	hotelNameText: {
		color: "#000",
		fontSize: height / 50,
		width: width * 0.65
	},
	flexOne: { flex: 1 },
	viewPager: {
		flex: 1,
		position: "absolute",
		bottom: 0,
		backgroundColor: "#fff"
	},
	flexEndJustified: { justifyContent: "flex-end" },
	backButtonContainer: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	shortListHotelIcon: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	viewPagerView: {
		width: width,
		backgroundColor: "#fff"
	},
	shortListBar: {
		width: width / 4,
		backgroundColor: "rgba(0,0,0,0.5)",
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		paddingVertical: width / 70
	},
	hotelListLengthText: {
		color: "#fff",
		fontSize: height / 70
	},
	image: {
		width: width / 4,
		height: height / 6,
		borderRadius: width / 20,
		justifyContent: "flex-end"
	},
	listMap: {
		backgroundColor: "#fff",
		flexDirection: "row",
		borderWidth: 0.5,
		borderColor: "#DFDDE0",
		paddingHorizontal: width / 26.3,
		paddingVertical: height / 75
	},
	cancellationChargeText: {
		fontSize: height / 60
	},
	shortListText: {
		color: "#fff",
		fontSize: height / 70,
		marginLeft: width / 40
	},
	flatListContainer: { height: height },
	mapSection: {
		flex: 3,
		paddingVertical: width / 30,
		backgroundColor: "#fff",
		alignItems: "center",
		justifyContent: "center"
	},
	mapContainer: { flex: 1, height: height },
	topButton: {
		width: width,
		backgroundColor: "#DFDDE0",
		elevation: 5,
		alignItems: "center"
	},
	topFillBar: {
		width: width / 1.1,
		paddingVertical: width / 40,
		paddingHorizontal: width / 30,
		borderRadius: 5,
		marginTop: width / 50,
		backgroundColor: "#fff",
		flexDirection: "row",
		justifyContent: "flex-start",
		alignItems: "center"
	},
	featureButton: {
		position: "absolute",
		right: width / 30,
		width: width / 4.5,
		height: width / 9,
		borderRadius: width / 15,
		flexDirection: "row",
		elevation: 10,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#3366cc"
	},
	featureText: {
		marginLeft: width / 40,
		color: "#fff"
	},
	header: {
		width: width,
		elevation: 5,
		backgroundColor: "#3366cc",
		justifyContent: "space-between",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		...Platform.select({
			android: { elevation: 2 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "black",
				shadowOpacity: 0.1
			}
		})
	},
	headerText: {
		color: "#ffffff",
		fontSize: height / 45.71,
		marginLeft: width / 26.3
	},
	textStyle: {
		color: "#fff",
		fontSize: height / 48
	},
	dateTime: {
		flexDirection: "row",
		paddingHorizontal: width / 27,
		justifyContent: "space-between",
		alignItems: "center",
		height: height / 13,
		borderBottomWidth: 0.3,
		borderBottomColor: "#DFDDE0"
	},
	bar: {
		width: width,
		paddingVertical: height / 35,
		paddingHorizontal: width / 27,
		borderBottomWidth: 0.3,
		borderBottomColor: "#DFDDE0"
	},
	search: {
		width: width / 1.1,
		borderRadius: 5,
		paddingVertical: width / 40,
		backgroundColor: "#EC6733",
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center",
		marginTop: width / 40
	},
	cross: {
		width: width / 20,
		height: width / 20,
		borderRadius: width / 40,
		backgroundColor: "#777777",
		justifyContent: "center",
		alignItems: "center"
	},
	primaryHeader: {
		color: "#fff",
		fontSize: height / 50
	},
	secondaryHeader: {
		color: "rgba(255,255,255,0.5)",
		fontSize: height / 70
	},
	listRow: {
		backgroundColor: "#fff",
		flexDirection: "row",
		borderBottomWidth: 0.5,
		borderColor: "#DFDDE0",
		paddingHorizontal: width / 26.3,
		paddingVertical: height / 75
	},
	ratingSection: {
		flexDirection: "row",
		width: width / 1.52,
		height: height / 10,
		justifyContent: "space-between"
	},
	shortlistNumber: {
		position: "absolute",
		top: height / 70,
		right: width / 18,
		width: width / 25,
		height: width / 25,
		borderRadius: width / 50,
		backgroundColor: "#000",
		justifyContent: "center",
		alignItems: "center",
		elevation: 10
	},
	priceSection: {
		justifyContent: "space-between",
		width: width / 5,
		alignItems: "flex-end"
	},
	baseFareText: {
		fontSize: height / 70,
		textDecorationLine: "line-through",
		textDecorationColor: "rgba(0,0,0,0.5)"
	},
	taxText: {
		fontSize: height / 70
	},
	discountText: {
		fontSize: height / 40,
		color: "#000"
	},
	headerContainer: { flexDirection: "row", alignItems: "center" },
	hotelInfoView: {
		justifyContent: "space-around",
		paddingHorizontal: width / 30
	}
});

export default styles;
