import React from "react";
import {
	View,
	Dimensions,
	StatusBar,
	TouchableOpacity,
	FlatList,
	ImageBackground,
	NativeModules,
	Platform
} from "react-native";
import { connect } from "react-redux";
import { Icon, MapView } from "../../../../Components";
import { Marker } from "react-native-maps";
import {
	saveStayDate,
	savePersonalInfo,
	getSearchHotel
} from "../SearchHotel/Saga";
import { removeShortListHotel, saveShortListHotel } from "../SelectHotel/Saga";
import Util from "../../Util";
import I18n from "../../Assets/Strings/i18n";
import {
	StarReview,
	CTProgressScreen,
	CtHotelTextRegular,
	CtHotelTextMedium,
	CtHotelTextBold
} from "../../Components";
import styles from "./styles";
import { EMPTYIMAGE } from "../../Assets/Img/Image";
import { TopTab } from "../../../../utils/Navigators";

const { width, height } = Dimensions.get("window");
const { UIManager } = NativeModules;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.015;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}

class ShortListHotel extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isExpand: false,
			addTraveller: false,
			mapVisible: false,
			region: null,
			isProgress: false
		};
		this.hotelNavigator = null;
		this.renderHotelNavigator();
	}

	componentDidMount() {
		let selectedHotel = this.props.shortListHotel[0];
		this.setState({
			hotel: selectedHotel,
			region: {
				latitude: parseFloat(
					selectedHotel["basic-info"]["hotel-info:locality-latitude"]
				),
				longitude: parseFloat(
					selectedHotel["basic-info"]["hotel-info:locality-longitude"]
				)
			}
		});
	}
	shouldComponentUpdate(props, state) {
		if (
			props.shortListHotel !== null &&
			props.shortListHotel !== this.props.shortListHotel
		) {
			this.setState({ isProgress: false, isExpand: false });
		}
		return true;
	}

	onDateSelected = (checkIn, checkOut) => {
		this.props.dispatch(
			saveStayDate({
				checkInDate: checkIn,
				checkOutDate: checkOut
			})
		);
	};

	getDateToDisplay = () => {
		let checkInDate = this.props.stayDate.checkInDate;
		let checkOutDate = this.props.stayDate.checkOutDate;
		let dateToDisplay =
			I18n.l("date.formats.normal_day", checkInDate) +
			" to " +
			I18n.l("date.formats.normal_day", checkOutDate);
		return dateToDisplay;
	};

	getTravellerToDisplay = () => {
		let detail =
			this.props.personalInfo.length +
			" " +
			I18n.t("room", { count: this.props.personalInfo.length }) +
			", ";
		let adultCount = 0;
		let childCount = 0;
		this.props.personalInfo.map(item => {
			adultCount += item.adult;
			childCount += item.child.length;
		});
		detail += adultCount + " " + I18n.t("adult", { count: adultCount });
		if (childCount > 0) {
			detail +=
				", " + childCount + "  " + I18n.t("child", { count: childCount });
		}
		return detail;
	};

	closeModal = () => {
		this.setState({ addTraveller: false });
	};

	saveTravellerDetail = personalInfo => {
		this.props.dispatch(savePersonalInfo(personalInfo));
		this.setState({ addTraveller: false });
	};

	onPageSelected = position => {
		let selectedHotel = this.props.shortListHotel[position];
		let regionSelected = {
			latitude: parseFloat(
				selectedHotel["basic-info"]["hotel-info:locality-latitude"]
			),
			longitude: parseFloat(
				selectedHotel["basic-info"]["hotel-info:locality-longitude"]
			)
		};
		this.mapRef.animateRegionChange({
			latitude: regionSelected.latitude,
			longitude: regionSelected.longitude,
			latitudeDelta: LATITUDE_DELTA,
			longitudeDelta: LONGITUDE_DELTA
		});
		this.setState({
			hotel: selectedHotel,
			region: regionSelected
		});
	};

	renderHotels = ({ item }) => {
		let hotel = item;
		const IMAGE_URL = `https://www.cleartrip.com/places/hotels${
			item["basic-info"]["hotel-info:thumb-nail-image"]
		}`;
		if (hotel !== undefined) {
			return (
				<TouchableOpacity
					onPress={() => {
						this.props.navigation.navigate("HotelDetail", {
							hotelId: hotel["hotel-id"]
						});
					}}
					style={this.state.mapVisible ? styles.listMap : styles.listRow}>
					<View style={styles.rowView}>
						<ImageBackground
							source={
								hotel["basic-info"]["hotel-info:thumb-nail-image"] !== ""
									? {
											uri: IMAGE_URL
									  }
									: EMPTYIMAGE
							}
							style={styles.image}>
							<TouchableOpacity
								onPress={() => {
									if (
										this.props.shortListHotel.filter(
											obj => obj["hotel-id"] === item["hotel-id"]
										).length === 1
									) {
										this.props.dispatch(removeShortListHotel(item));
									} else {
										this.props.dispatch(saveShortListHotel(item));
									}
								}}
								style={styles.shortListBar}>
								{this.props.shortListHotel.filter(
									obj => obj["hotel-id"] === item["hotel-id"]
								).length === 1 ? (
									<View style={styles.rowView}>
										<Icon
											iconType={"ionicon"}
											iconSize={height / 46}
											iconName={"ios-heart"}
											iconColor={"#ff0000"}
										/>
										<CtHotelTextRegular style={styles.shortListText}>
											{I18n.t("shortlisted")}
										</CtHotelTextRegular>
									</View>
								) : (
									<View style={styles.rowView}>
										<Icon
											iconType={"icomoon"}
											iconSize={height / 46}
											iconName={"ios-heart-empty"}
											iconColor={"#ffffff"}
										/>
										<CtHotelTextRegular style={styles.shortListText}>
											{I18n.t("shortlist")}
										</CtHotelTextRegular>
									</View>
								)}
							</TouchableOpacity>
						</ImageBackground>
					</View>
					<View style={styles.hotelInfoView}>
						<View>
							<CtHotelTextMedium style={styles.hotelNameText} numberOfLines={1}>
								{hotel["basic-info"]["hotel-info:hotel-name"]}
							</CtHotelTextMedium>
							<CtHotelTextRegular style={styles.cityText}>
								{hotel["basic-info"]["hotel-info:city"]}
							</CtHotelTextRegular>
						</View>
						<View style={styles.ratingSection}>
							<View style={styles.flexEndJustified}>
								<View>
									<StarReview
										rating={hotel["basic-info"]["hotel-info:star-rating"]}
									/>
									<CtHotelTextRegular>
										{I18n.t("user_rating")}
									</CtHotelTextRegular>
								</View>
								<View>
									<CtHotelTextRegular style={styles.cancellationChargeText}>
										{I18n.t("cancellation_charges_apply")}
									</CtHotelTextRegular>
								</View>
							</View>
							{!this.props.isAnyTime && (
								<View style={styles.priceSection}>
									<CtHotelTextRegular style={styles.baseFareText}>
										{item.discount !== 0
											? " ₹" + I18n.toNumber(item.baseFare, { precision: 0 })
											: ""}
									</CtHotelTextRegular>
									<CtHotelTextRegular style={styles.discountText}>
										{"" +
											I18n.toNumber(item.baseFare - item.discount, {
												precision: 0
											})}
									</CtHotelTextRegular>
									<CtHotelTextRegular style={styles.taxText}>
										{item.tax !== 0
											? "+₹" +
											  I18n.toNumber(item.tax, { precision: 0 }) +
											  " " +
											  I18n.t("taxes")
											: ""}
									</CtHotelTextRegular>
									<CtHotelTextRegular style={styles.taxText}>
										{I18n.t("total") +
											" ₹" +
											I18n.toNumber(item.price, { precision: 0 })}
									</CtHotelTextRegular>
								</View>
							)}
						</View>
					</View>
				</TouchableOpacity>
			);
		} else {
			return null;
		}
	};

	renderHotelNavigator = () => {
		try {
			let mapScreen = {};
			this.props.shortListHotel.map((item, index) => {
				mapScreen[`screen${index}`] = () => this.renderHotels({ item });
			});

			const TabNav = () => (
				<TopTab.Navigator
					lazy={true}
					tabBarOptions={{
						safeAreaInset: "bottom",
						showLabel: false,
						indicatorStyle: {
							backgroundColor: "transparent"
						},
						style: {
							backgroundColor: "transparent"
						}
					}}>
					{this.props.shortListHotel.map((item, index) => (
						<TopTab.Screen
							name={`screen${index}`}
							component={() => this.renderHotels({ item })}
						/>
					))}
				</TopTab.Navigator>
			);

			this.hotelNavigator = () => (
				<TabNav
					onNavigationStateChange={(prevState, currState) =>
						this.onPageSelected(currState.index)
					}
				/>
			);
		} catch (error) {
			this.hotelNavigator = null;
		}
	};
	renderMaps = () => {
		return (
			<View style={styles.mapContainer}>
				<View style={styles.mapSection}>
					<MapView
						ref={ref => (this.mapRef = ref)}
						onLayout={true}
						initialRegion={{
							latitude: this.state.region.latitude,
							longitude: this.state.region.longitude,
							latitudeDelta: LATITUDE_DELTA,
							longitudeDelta: LONGITUDE_DELTA
						}}
						showsUserLocation={true}
						showsMyLocationButton={true}>
						<Marker
							coordinate={this.state.region}
							title={""}
							pinColor={"#EC6733"}
						/>
					</MapView>
				</View>
				{this.hotelNavigator ? <this.hotelNavigator /> : null}
			</View>
		);
	};

	getDateAsParameter = date => {
		let month = date.getMonth() + 1;
		month = month < 10 ? "0" + month : month;
		return date.getYear() + 1900 + "-" + month + "-" + date.getDate();
	};
	modifyHotelSearch = () => {
		this.setState({ isProgress: true });
		let params = {
			checkInDate: this.getDateAsParameter(this.props.stayDate.checkInDate),
			checkOutDate: this.getDateAsParameter(this.props.stayDate.checkOutDate),
			appToken: this.props.appToken,
			noOfRooms: this.props.personalInfo.length,
			PERSONALINFO: this.props.personalInfo,
			city: this.props.searchType.value.city,
			state: this.props.searchType.value.state,
			country: "IN"
		};
		this.props.dispatch(getSearchHotel(params));
	};
	render() {
		return (
			<View style={styles.flexOne}>
				<StatusBar backgroundColor="#1f3d7a" barStyle="light-content" />
				<View style={styles.header}>
					<View style={styles.headerContainer}>
						<TouchableOpacity
							onPress={() => {
								this.props.navigation.goBack();
							}}>
							<View style={styles.backButtonContainer}>
								<Icon
									iconType={"material"}
									iconSize={height / 26}
									iconName={"arrow-back"}
									iconColor={"#ffffff"}
								/>
							</View>
						</TouchableOpacity>
						<View>
							<View style={styles.rowView}>
								<CtHotelTextBold style={styles.primaryHeader}>
									{I18n.t("shortlisted") + " " + I18n.t("hotels")}
								</CtHotelTextBold>
							</View>
						</View>
					</View>
					<View>
						<View style={styles.shortListHotelIcon}>
							{this.props.shortListHotel.length === 0 ? (
								<Icon
									iconType={"icomoon"}
									iconSize={height / 26}
									iconName={"ios-heart-empty"}
									iconColor={"#ffffff"}
								/>
							) : (
								<Icon
									iconType={"ionicon"}
									iconSize={height / 26}
									iconName={"ios-heart"}
									iconColor={"#ff0000"}
								/>
							)}
						</View>
					</View>
					{/* number of shortlisted hotels at absolute position */}
					{this.props.shortListHotel.length > 0 && (
						<View style={styles.shortlistNumber}>
							<CtHotelTextRegular style={styles.hotelListLengthText}>
								{this.props.shortListHotel.length}
							</CtHotelTextRegular>
						</View>
					)}
				</View>
				{/* body section */}
				{!this.state.mapVisible ? (
					<View style={styles.flexOne}>
						{/* <View
							style={[
								styles.topButton,
								{
									height:
										this.state.isExpand === true ? height / 4.5 : height / 13.5,
								},
							]}>
							<TouchableOpacity
								onPress={() => {
									if (!this.state.isExpand) {
										this.setState({ isExpand: true })
									} else {
										Util.openCheckInDatePicker(this.onDateSelected)
									}
								}}
								style={styles.topFillBar}>
								<Icon
									iconType={"ionicon"}
									iconSize={height / 35}
									iconName={"md-calendar"}
									iconColor={"rgba(0,0,0,0.5)"}
								/>
								<CtHotelTextRegular
									style={[
										styles.textStyle,
										{ color: "rgba(0,0,0,0.5)", marginLeft: width / 30 },
									]}>
									{this.getDateToDisplay()}
								</CtHotelTextRegular>
							</TouchableOpacity>
							<TouchableOpacity
								style={styles.topFillBar}
								onPress={() => this.setState({ addTraveller: true })}>
								<Icon
									iconType={"ionicon"}
									iconSize={height / 35}
									iconName={"md-person"}
									iconColor={"rgba(0,0,0,0.5)"}
								/>
								<CtHotelTextRegular
									style={[
										styles.textStyle,
										{ color: "rgba(0,0,0,0.5)", marginLeft: width / 30 },
									]}>
									{this.getTravellerToDisplay()}
								</CtHotelTextRegular>
							</TouchableOpacity>
							<TouchableOpacity
								style={styles.search}
								onPress={() => this.modifyHotelSearch()}>
								<CtHotelTextRegular
									style={[styles.textStyle, { fontSize: height / 55 }]}>
									{I18n.t("modify_search")}
								</CtHotelTextRegular>
							</TouchableOpacity>
						</View> */}
						<FlatList
							style={styles.flatListContainer}
							data={this.props.shortListHotel}
							keyExtractor={(item, index) => index.toString()}
							renderItem={this.renderHotels}
						/>
					</View>
				) : (
					<View style={styles.flexOne}>{this.renderMaps()}</View>
				)}
				{this.state.isProgress && <CTProgressScreen indicatorSize={"large"} />}
				<TouchableOpacity
					onPress={() => this.setState({ mapVisible: !this.state.mapVisible })}
					style={[
						styles.featureButton,
						{
							bottom: this.state.mapVisible ? height / 5.7 : height / 30
						}
					]}>
					<Icon
						iconType={"ionicon"}
						iconSize={width / 18}
						iconName={this.state.mapVisible ? "md-menu" : "md-map"}
						iconColor={"#ffffff"}
					/>
					<CtHotelTextBold style={styles.featureText}>
						{this.state.mapVisible ? I18n.t("cl_list") : I18n.t("cl_map")}
					</CtHotelTextBold>
				</TouchableOpacity>
				{this.state.addTraveller &&
					Util.displayTraveller(
						this.closeModal,
						this.saveTravellerDetail,
						this.state.addTraveller
					)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		stayDate: state.cleartripHotel && state.cleartripHotel.searchHotel.stayDate,
		personalInfo:
			state.cleartripHotel && state.cleartripHotel.searchHotel.personalInfo,
		searchType:
			state.cleartripHotel && state.cleartripHotel.searchHotel.searchType,
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		hotels:
			state.cleartripHotel &&
			state.cleartripHotel.searchHotel &&
			state.cleartripHotel.searchHotel.hotels
				? state.cleartripHotel.searchHotel.hotels
				: null,
		error: state.cleartripHotel && state.cleartripHotel.searchHotel.error,
		shortListHotel:
			state.cleartripHotel && state.cleartripHotel.selectHotel.shortListHotel
	};
}

export default connect(
	mapStateToProps,
	null,
	null
)(ShortListHotel);
