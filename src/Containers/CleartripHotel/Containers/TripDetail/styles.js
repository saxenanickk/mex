import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: { flex: 1, backgroundColor: "#e9e9e9" },
	header: {
		width: width,
		elevation: 5,
		backgroundColor: "#3366cc",
		justifyContent: "space-between",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33
	},
	flexOne: { flex: 1 },
	flexEndAligned: { alignItems: "flex-end" },
	subHeader: {
		paddingHorizontal: width / 30,
		height: height / 13,
		justifyContent: "center"
	},
	centerAligned: { alignItems: "center" },
	subHeaderText: {
		fontSize: height / 50,
		color: "rgba(0,0,0,0.5)",
		marginTop: height / 50
	},
	infoBar: {
		width: width,
		backgroundColor: "#fff",
		paddingVertical: height / 40,
		paddingHorizontal: width / 30,
		borderBottomColor: "#DFDDE0",
		borderBottomWidth: 1,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	infoText: {
		fontSize: height / 43,
		color: "#000"
	},
	errorImage: {
		width: width / 3,
		height: width / 3,
		marginBottom: height / 20
	},
	backButton: {
		width: width / 7,
		height: height / 15,
		justifyContent: "center",
		alignItems: "center"
	},
	checkType: {
		color: "rgba(0,0,0,0.4)",
		fontSize: height / 65
	},
	checkDate: {
		color: "#000",
		fontSize: height / 45
	},
	checkTime: {
		color: "rgba(0,0,0,0.4)",
		fontSize: height / 55
	},
	timeInfo: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: width / 4,
		alignItems: "center",
		height: height / 6,
		backgroundColor: "#fff",
		borderBottomColor: "#DFDDE0",
		borderBottomWidth: 1
	},
	textStyle: {
		color: "#fff",
		fontSize: height / 45
	},
	lowerTextStyle: {
		color: "#fff",
		fontSize: height / 55
	},
	imageStyle: {
		width: width,
		height: height / 3.5
	},
	hotelInfo: {
		width: width,
		height: height / 5,
		justifyContent: "center",
		alignItems: "center"
	},
	goBack: {
		width: width / 1.2,
		marginTop: height / 20,
		paddingVertical: width / 30,
		backgroundColor: "#EC6733",
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center"
	},
	errorText: {
		fontSize: height / 55,
		color: "#000"
	},
	buttonText: {
		fontSize: height / 55,
		fontWeight: "bold",
		color: "#fff"
	},
	errorScreen: {
		justifyContent: "center",
		alignItems: "center",
		flex: 1
	}
});

export default styles;
