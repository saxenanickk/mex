/* eslint-disable radix */
import React, { Component } from "react";
import {
	View,
	Animated,
	ScrollView,
	Dimensions,
	TouchableOpacity,
	ImageBackground,
	Alert,
	Image,
	BackHandler
} from "react-native";
import { connect } from "react-redux";
import Util from "../../Util";
import { getTripDetail, saveTripDetail, saveTripError } from "./Saga";
import {
	CTProgressScreen,
	CtHotelTextRegular,
	CtHotelTextBold
} from "../../Components";
import { Icon } from "../../../../Components";

import I18n from "../../Assets/Strings/i18n";
import { NORESULT } from "../../Assets/Img/Image";
import styles from "./styles";
import Config from "react-native-config";
import { CommonActions } from "@react-navigation/native";

const { CLEARTRIP_SERVER_IMAGE_URL } = Config;
const { height, width } = Dimensions.get("window");

const CATEGORY = {
	BF: "Room Rate",
	DIS: "Discount",
	TAX: "Taxes"
};
class TripDetail extends Component {
	constructor(props) {
		super(props);

		this.tripId =
			this.props.route.params && this.props.route.params.tripId
				? this.props.route.params.tripId
				: null;

		this.state = {
			scrollY: new Animated.Value(0)
		};
		this.totalRate = 0;
	}

	componentDidMount() {
		this.props.dispatch(
			getTripDetail({
				appToken: this.props.appToken,
				tripId: this.tripId
			})
		);
		this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
			this.goBack();
			return true;
		});
	}

	getDate = date => {
		let month = Util.getMonthName(date.getMonth()).toUpperCase();
		let day = date.getDate();
		day = day < 10 ? "0" + day : day;
		return month + " " + day;
	};

	getTime = date => {
		let time = date.toDateString().split(" ");
		return time[0] + ", 12pm";
	};

	calculatePrice = pricingElement => {
		let price = 0;
		if (pricingElement instanceof Array) {
			pricingElement.map(cost => {
				price += cost["common:amount"];
			});
		} else {
			price = pricingElement["common:amount"];
		}
		return price;
	};

	calculateRate = () => {
		let rateElement = this.room["rate-breakdown"]["common:rate"];
		let price = 0;
		if (rateElement instanceof Array) {
			rateElement.map(rates => {
				let pricingElement =
					rates["common:pricing-elements"]["common:pricing-element"];
				price += this.calculatePrice(pricingElement);
			});
		} else {
			let pricingElement =
				rateElement["common:pricing-elements"]["common:pricing-element"];
			price = this.calculatePrice(pricingElement);
		}
		return parseInt(price);
	};
	shouldComponentUpdate(props, state) {
		return true;
	}

	componentWillUnmount() {
		this.props.dispatch(saveTripDetail(null));
		this.props.dispatch(saveTripError(false));
		this.backHandler.remove();
	}

	getRoomType = () => {
		let rooms = this.props.tripDetail.rooms.room;
		if (rooms instanceof Array) {
			return rooms[0].type;
		} else {
			return rooms.type;
		}
	};

	getTravellerName = () => {
		let contact = this.props.tripDetail["contact-detail"];
		return (
			contact.title + " " + contact["first-name"] + " " + contact["last-name"]
		);
	};

	getOccupancy = () => {
		let singularType = ["adult", "child"];
		let pluralType = ["adults", "children"];

		let guests = this.props.tripDetail.guests.split("\n");
		console.log(guests);
		let occupancy = "";
		guests.map((item, index) => {
			let data = item.split(":");
			if (index >= 1) {
				occupancy += data[1] + " ";
				occupancy +=
					parseInt(data[1]) > 1
						? I18n.t(pluralType[index - 1])
						: I18n.t(singularType[index - 1]);
			}
		});
		console.log("occupancy is", occupancy);
		return occupancy;
	};

	getVoucherNumber = () => {
		let bookingInfo = this.props.tripDetail["booking-info-list"][
			"booking-info"
		];
		if (bookingInfo instanceof Array) {
			return bookingInfo[0]["voucher-number"];
		} else {
			return bookingInfo["voucher-number"];
		}
	};

	getPayments = () => {
		let rooms = this.props.tripDetail["room-rates"]["room-rate"];
		if (rooms instanceof Array) {
			return rooms[0]["pricing-elements"]["pricing-element"];
		} else {
			return rooms["pricing-elements"]["pricing-element"];
		}
	};

	calculatePrice = pricingElement => {
		let price = 0;
		if (pricingElement instanceof Array) {
			pricingElement.map(cost => {
				price += cost.amount;
			});
		} else {
			price = pricingElement.amount;
		}
		return parseInt(price);
	};

	calculateRate = () => {
		let rateElement = this.props.tripDetail["room-rates"]["room-rate"];
		let price = 0;
		if (rateElement instanceof Array) {
			rateElement.map(rates => {
				let pricingElement = rates["pricing-elements"]["pricing-element"];
				price += this.calculatePrice(pricingElement);
			});
		} else {
			let pricingElement = rateElement["pricing-elements"]["pricing-element"];
			price = this.calculatePrice(pricingElement);
		}
		return price;
	};
	fetchTripDetail = () => {
		this.props.dispatch(saveTripError(false));
		this.props.dispatch(
			getTripDetail({
				appToken: this.props.appToken,
				tripId: this.tripId
			})
		);
	};

	backOnError = () => {
		Alert.alert(
			I18n.t("cancelled"),
			I18n.t("go_back_without_trip_details"),
			[
				{
					text: I18n.t("cancel"),
					onPress: () => {
						console.log("cancel pressed");
					},
					style: "cancel"
				},
				{
					text: I18n.t("ok"),
					onPress: () => {
						this.props.navigation.dispatch(
							CommonActions.reset({
								index: 0,
								routes: [{ name: "SearchHotel" }]
							})
						);
					}
				}
			],
			{ cancelable: false }
		);
	};

	goBack = () => {
		if (this.props.tripDetail !== null) {
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "SearchHotel" }]
				})
			);
		} else {
			this.backOnError();
		}
	};
	render() {
		return (
			<View style={styles.container}>
				{!this.props.tripError ? (
					<View style={styles.flexOne}>
						{this.props.tripDetail !== null ? (
							<View style={styles.flexOne}>
								<ScrollView
									scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
									onScroll={() => {
										Animated.event([
											{
												nativeEvent: {
													contentOffset: { y: this.state.scrollY }
												}
											}
										]);
									}}
									style={styles.flexOne}
									stickyHeaderIndices={[0]}>
									<ImageBackground
										style={[styles.imageStyle]}
										source={{
											uri:
												CLEARTRIP_SERVER_IMAGE_URL +
												this.props.hotelDetail["basic-info"]["thumb-nail-image"]
										}}>
										<TouchableOpacity
											style={styles.backButton}
											onPress={() => this.goBack()}>
											<Icon
												iconType={"material"}
												iconSize={height / 26}
												iconName={"arrow-back"}
												iconColor={"#fff"}
											/>
										</TouchableOpacity>
										<View style={styles.hotelInfo}>
											<CtHotelTextBold style={styles.textStyle}>
												{this.props.hotelDetail["basic-info"]["hotel-name"]}
											</CtHotelTextBold>
											<CtHotelTextRegular style={styles.lowerTextStyle}>
												{I18n.t("trip_id") + this.props.tripDetail["trip-ref"]}
											</CtHotelTextRegular>
										</View>
									</ImageBackground>
									<View style={styles.subHeader}>
										<CtHotelTextBold style={styles.subHeaderText}>
											{I18n.t("hotel_address")}
										</CtHotelTextBold>
									</View>
									<View style={styles.infoBar}>
										<CtHotelTextRegular style={styles.infoText}>
											{this.props.tripDetail["hotel-detail"].address}
										</CtHotelTextRegular>
									</View>
									<View style={styles.subHeader}>
										<CtHotelTextBold style={styles.subHeaderText}>
											{I18n.t("booking_details")}
										</CtHotelTextBold>
									</View>
									<View style={styles.timeInfo}>
										<View style={styles.flexEndAligned}>
											<CtHotelTextRegular style={styles.checkType}>
												{I18n.t("check_in")}
											</CtHotelTextRegular>
											<CtHotelTextBold style={styles.checkDate}>
												{this.getDate(this.props.stayDate.checkInDate)}
											</CtHotelTextBold>
											<CtHotelTextRegular style={styles.checkTime}>
												{this.getTime(this.props.stayDate.checkInDate)}
											</CtHotelTextRegular>
										</View>
										<View style={styles.centerAligned}>
											<Icon
												iconType={"ionicon"}
												iconSize={width / 20}
												iconName={"md-time"}
												iconColor={"rgba(0,0,0,0.4)"}
											/>
											<CtHotelTextRegular style={styles.checkType}>
												{this.props.searchCriteria["number-of-nights"] +
													" night"}
											</CtHotelTextRegular>
										</View>
										<View>
											<CtHotelTextRegular style={styles.checkType}>
												{I18n.t("check_out")}
											</CtHotelTextRegular>
											<CtHotelTextBold style={styles.checkDate}>
												{this.getDate(this.props.stayDate.checkOutDate)}
											</CtHotelTextBold>
											<CtHotelTextRegular style={styles.checkTime}>
												{this.getTime(this.props.stayDate.checkOutDate)}
											</CtHotelTextRegular>
										</View>
									</View>
									<View style={styles.infoBar}>
										<CtHotelTextRegular style={styles.infoText}>
											{this.getRoomType()}
										</CtHotelTextRegular>
									</View>
									<View style={styles.infoBar}>
										<CtHotelTextRegular style={styles.infoText}>
											{I18n.t("traveller")}
										</CtHotelTextRegular>
										<CtHotelTextRegular style={styles.infoText}>
											{this.getTravellerName()}
										</CtHotelTextRegular>
									</View>
									<View style={styles.infoBar}>
										<CtHotelTextRegular style={styles.infoText}>
											{I18n.t("total_rooms")}
										</CtHotelTextRegular>
										<CtHotelTextRegular style={styles.infoText}>
											{this.props.tripDetail["room-count"] + " Room"}
										</CtHotelTextRegular>
									</View>
									<View style={styles.infoBar}>
										<CtHotelTextRegular style={styles.infoText}>
											{I18n.t("occupancy")}
										</CtHotelTextRegular>
										<CtHotelTextRegular style={styles.infoText}>
											{this.getOccupancy()}
										</CtHotelTextRegular>
									</View>
									<View style={styles.subHeader}>
										<CtHotelTextBold style={styles.subHeaderText}>
											{I18n.t("voucher_no")}
										</CtHotelTextBold>
									</View>
									<View style={styles.infoBar}>
										<CtHotelTextRegular style={styles.infoText}>
											{this.getVoucherNumber()}
										</CtHotelTextRegular>
									</View>
									<View style={styles.subHeader}>
										<CtHotelTextBold style={styles.subHeaderText}>
											{I18n.t("payment_details")}
										</CtHotelTextBold>
									</View>
									<View>
										{this.getPayments().map((cost, index) => {
											return (
												<View key={index} style={styles.infoBar}>
													<CtHotelTextRegular style={styles.infoText}>
														{CATEGORY[cost.category]}
													</CtHotelTextRegular>
													<CtHotelTextRegular style={styles.infoText}>
														{"₹"}
														{Math.abs(parseInt(cost.amount))}
													</CtHotelTextRegular>
												</View>
											);
										})}
									</View>
									<View style={styles.infoBar}>
										<CtHotelTextRegular style={styles.infoText}>
											{I18n.t("total_fare")}
										</CtHotelTextRegular>
										<CtHotelTextRegular style={styles.infoText}>
											{"₹"}
											{this.calculateRate()}
										</CtHotelTextRegular>
									</View>
								</ScrollView>
							</View>
						) : (
							<CTProgressScreen
								indicatorSize={"large"}
								message={"fetching trip details"}
							/>
						)}
					</View>
				) : (
					<View style={styles.flexOne}>
						<View style={styles.header}>
							<TouchableOpacity
								style={styles.backButton}
								onPress={() => this.backOnError()}>
								<Icon
									iconType={"material"}
									iconSize={height / 26}
									iconName={"arrow-back"}
									iconColor={"#fff"}
								/>
							</TouchableOpacity>
						</View>
						<View style={styles.errorScreen}>
							<Image
								resizeMode={"contain"}
								source={NORESULT}
								style={styles.errorImage}
							/>
							<CtHotelTextBold style={styles.errorText}>
								{I18n.t("unable_to_fetch_detail")}
							</CtHotelTextBold>
							<CtHotelTextBold style={styles.errorText}>
								{I18n.t("try_again")}
							</CtHotelTextBold>
							<TouchableOpacity
								style={styles.goBack}
								onPress={() => this.fetchTripDetail()}>
								<CtHotelTextRegular style={styles.buttonText}>
									{I18n.t("refresh")}
								</CtHotelTextRegular>
							</TouchableOpacity>
						</View>
					</View>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		stayDate: state.cleartripHotel && state.cleartripHotel.searchHotel.stayDate,
		personalInfo:
			state.cleartripHotel && state.cleartripHotel.searchHotel.personalInfo,
		searchType:
			state.cleartripHotel && state.cleartripHotel.searchHotel.searchType,
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		hotels:
			state.cleartripHotel &&
			state.cleartripHotel.searchHotel &&
			state.cleartripHotel.searchHotel.hotels
				? state.cleartripHotel.searchHotel.hotels
				: null,
		hotelDetail:
			state.cleartripHotel && state.cleartripHotel.selectHotel.hotelInfo,
		searchCriteria:
			state.cleartripHotel &&
			state.cleartripHotel.selectHotel &&
			state.cleartripHotel.selectHotel.hotelRateInfo &&
			state.cleartripHotel.selectHotel.hotelRateInfo["search-criteria"],
		provisionalId:
			state.cleartripHotel && state.cleartripHotel.bookHotel.provisionalBooking,
		bookResponse:
			state.cleartripHotel && state.cleartripHotel.bookHotel.bookResponse,
		tripDetail:
			state.cleartripHotel && state.cleartripHotel.tripDetail.tripDetail,
		tripError: state.cleartripHotel && state.cleartripHotel.tripDetail.tripError
	};
}

export default connect(
	mapStateToProps,
	null,
	null
)(TripDetail);
