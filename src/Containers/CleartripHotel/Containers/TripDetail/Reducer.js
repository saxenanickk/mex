import { CTHOTEL_SAVE_TRIP_DETAIL, CTHOTEL_SAVE_TRIP_ERROR } from "./Saga";
const initialState = {
	tripDetail: null,
	tripError: false
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case CTHOTEL_SAVE_TRIP_DETAIL:
			return {
				...state,
				tripDetail: action.payload
			};
		case CTHOTEL_SAVE_TRIP_ERROR:
			return {
				...state,
				tripError: action.payload
			};
		default:
			return state;
	}
};
