import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api/";

export const CTHOTEL_GET_TRIP_DETAIL = "CTHOTEL_GET_TRIP_DETAIL";
export const CTHOTEL_SAVE_TRIP_DETAIL = "CTHOTEL_SAVE_TRIP_DETAIL";
export const CTHOTEL_SAVE_TRIP_ERROR = "CTHOTEL_SAVE_TRIP_ERROR";
export const getTripDetail = payload => ({
	type: CTHOTEL_GET_TRIP_DETAIL,
	payload
});

export const saveTripDetail = payload => ({
	type: CTHOTEL_SAVE_TRIP_DETAIL,
	payload
});

export const saveTripError = payload => ({
	type: CTHOTEL_SAVE_TRIP_ERROR,
	payload
});

export function* tripDetailSaga(dispatch) {
	yield takeLatest(CTHOTEL_GET_TRIP_DETAIL, handleGetTripDetail);
}

function* handleGetTripDetail(action) {
	try {
		let tripResponse = yield call(Api.getTrip, action.payload);
		console.log("trip response is==>", tripResponse);
		if (tripResponse.hasOwnProperty("itinerary")) {
			yield put(saveTripDetail(tripResponse.itinerary));
		} else {
			yield put(saveTripError(true));
		}
	} catch (error) {
		console.log("trip detail error==>", error);
		yield put(saveTripError(true));
	}
}
