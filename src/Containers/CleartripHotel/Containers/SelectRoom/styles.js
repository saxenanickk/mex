import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	amenitySection: {
		width: width / 1.2,
		paddingVertical: height / 50,
		justifyContent: "center",
		backgroundColor: "#fff",
		borderRadius: width / 30,
		marginTop: height / 5
	},
	amenityList: {
		flexDirection: "row",
		paddingHorizontal: width / 30,
		paddingVertical: height / 100
	},
	amenityText: {
		fontSize: height / 55,
		color: "#000"
	},
	pointStyle: {
		flexDirection: "row",
		paddingHorizontal: width / 26.3,
		paddingVertical: width / 30,
		alignItems: "center",
		borderBottomWidth: 1,
		borderBottomColor: "#F3F4F2",
		backgroundColor: "#fff"
	},
	pointText: {
		fontSize: height / 60,
		width: width / 1.5,
		marginLeft: width / 30,
		color: "#000"
	},
	listText: {
		color: "rgba(0,0,0,0.5)",
		fontSize: height / 55
	},
	roomRate: {
		fontSize: height / 45,
		color: "#000",
		marginLeft: width / 30
	},
	separator: {
		width: width,
		height: height / 20,
		borderColor: "#DFDDE0"
	},
	listButton: {
		width: width,
		height: height / 7,
		paddingVertical: width / 30,
		paddingHorizontal: width / 26.3,
		borderBottomWidth: 1,
		borderBottomColor: "#F3F4F2"
	},
	bottomButton: {
		width: width / 1.1,
		paddingVertical: width / 30,
		backgroundColor: "#EC6733",
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center",
		elevation: 5
	},
	infoBar: {
		paddingHorizontal: width / 70,
		paddingVertical: width / 260,
		backgroundColor: "#232323",
		borderRadius: width / 40,
		justifyContent: "center",
		alignItems: "center"
	},
	infoText: {
		color: "#fff",
		fontSize: height / 70
	},
	roomText: {
		color: "#fff",
		fontSize: height / 47
	},
	imageStyle: {
		width: width,
		height: height / 4,
		borderBottomColor: "#DFDDE0",
		borderBottomWidth: 1
	},
	imageBottomView: {
		position: "absolute",
		bottom: height / 50,
		paddingHorizontal: width / 26.3,
		width: width
	},
	bottomBarRow: {
		flexDirection: "row",
		justifyContent: "center",
		paddingHorizontal: width / 30,
		paddingVertical: width / 40,
		backgroundColor: "#fff",
		width: width,
		position: "absolute",
		bottom: 0,
		borderTopWidth: 1,
		borderTopColor: "#DFDDE0",
		elevation: 5
	},
	bodyStyle: {
		backgroundColor: "#e9e9e9",
		flex: 1,
		overflow: "visible"
	},
	header: {
		width: width,
		backgroundColor: "#fff",
		justifyContent: "flex-start",
		flexDirection: "row",
		paddingHorizontal: width / 26.3,
		alignItems: "center",
		height: height / 13.33,
		elevation: 5
	},
	textStyle: {
		color: "#000"
	},
	listItem: {
		width: width,
		backgroundColor: "#fff",
		borderBottomWidth: 1,
		borderBottomColor: "#DFDDE0"
	},
	search: {
		width: width / 1.2,
		paddingVertical: width / 30,
		backgroundColor: "#EC6733",
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center"
	},
	cross: {
		width: width / 20,
		height: width / 20,
		borderRadius: width / 40,
		backgroundColor: "#777777",
		justifyContent: "center",
		alignItems: "center"
	},
	primaryHeader: {
		color: "#000",
		fontSize: height / 50,
		width: width / 1.5
	},
	secondaryHeader: {
		color: "rgba(0,0,0,0.5)",
		fontSize: height / 70
	},
	listRow: {
		flexDirection: "row",
		borderBottomWidth: 0.5,
		borderColor: "#000",
		paddingHorizontal: width / 26.3,
		paddingVertical: height / 75
	},
	ratingSection: {
		flexDirection: "row",
		width: width / 1.52,
		height: height / 10,
		justifyContent: "space-between"
	},
	shortlistNumber: {
		position: "absolute",
		top: height / 70,
		right: width / 18,
		width: width / 25,
		height: width / 25,
		borderRadius: width / 50,
		backgroundColor: "#000",
		justifyContent: "center",
		alignItems: "center",
		elevation: 10
	},
	bottomBar: {
		position: "absolute",
		bottom: 0,
		elevation: 5,
		backgroundColor: "#fff",
		width: width,
		height: height / 9,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingHorizontal: width / 26.3,
		borderTopWidth: 1,
		borderTopColor: "#F3F4F2"
	},
	button: {
		width: width / 2.25,
		paddingVertical: width / 30,
		borderColor: "#656765",
		borderWidth: 1,
		borderRadius: 3,
		justifyContent: "center",
		alignItems: "center",
		flexDirection: "row"
	},
	baseFareText: {
		fontSize: height / 70,
		textDecorationLine: "line-through",
		textDecorationColor: "rgba(0,0,0,0.5)",
		marginLeft: width / 30
	},
	backButton: { width: width / 7 },
	offerImg: {
		width: width / 14,
		height: width / 14
	},
	flatListContainer: { marginBottom: height / 12 },
	infoContainer: {
		flexDirection: "row",
		marginTop: width / 60
	},
	roomContainer: {
		flexDirection: "row",
		alignItems: "center"
	},
	marginLeftView: {
		marginLeft: width / 15
	},
	rowView: { flexDirection: "row" },
	detailsText: {
		marginLeft: width / 30,
		color: "#3366cc"
	},
	amenityContainer: {
		flex: 1,
		backgroundColor: "rgba(0,0,0,0.5)",
		justifyContent: "center",
		alignItems: "center"
	},
	closeButton: {
		alignSelf: "center"
	},
	closeText: {
		alignSelf: "center"
	},
	unmatchedView: {
		width: width,
		backgroundColor: "#fff",
		paddingVertical: height / 50
	},
	unmatchedInnerView: {
		flexDirection: "row",
		paddingHorizontal: width / 30,
		alignItems: "center"
	},
	unmatchedText: {
		width: width / 1.2,
		marginLeft: width / 11,
		color: "#000000"
	},
	unmatchedTextDescription: {
		width: width / 1.2,
		marginLeft: width / 55,
		color: "#000000"
	},
	listTextView: { flexDirection: "row", marginLeft: width / 11 }
});

export default styles;
