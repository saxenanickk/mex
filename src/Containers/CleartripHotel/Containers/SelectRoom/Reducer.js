import { CTHOTEL_SAVE_HOTEL_CANCEL_POLICY, CTHOTEL_SAVE_OTP } from "./Saga";

const initialState = {
	cancelPolicy: null,
	Otp: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case CTHOTEL_SAVE_HOTEL_CANCEL_POLICY:
			return {
				...state,
				cancelPolicy: action.payload
			};
		case CTHOTEL_SAVE_OTP:
			return {
				...state,
				Otp: action.payload
			};
		default:
			return state;
	}
};
