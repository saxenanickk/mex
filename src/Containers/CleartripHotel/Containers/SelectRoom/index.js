/* eslint-disable radix */
import React from "react";
import {
	View,
	Image,
	Dimensions,
	TouchableOpacity,
	Modal,
	FlatList,
	ImageBackground,
	ScrollView
} from "react-native";
import { connect } from "react-redux";
import Util from "../../Util";
import I18n from "../../Assets/Strings/i18n";
import { EMPTYIMAGE, OFFER } from "../../Assets/Img/Image";
import {
	ToolTip,
	CtHotelTextRegular,
	CtHotelTextBold,
	CheckBox
} from "../../Components";
import { Icon } from "../../../../Components";
import { getHotelCancelPolicy, saveHotelCancelPolicy, getOtp } from "./Saga";
import styles from "./styles";
import { GoToast } from "../../../../Components";
import Config from "react-native-config";

const { CLEARTRIP_SERVER_IMAGE_URL } = Config;
const { width, height } = Dimensions.get("window");
const COUNT = 1;
const VALUE = 2;
class SelectRoom extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedRoomId: null,
			openHelp: false,
			selectedRoom: null,
			amenityModal: false,
			amenityRoom: null
		};
		this.posX = null;
		this.posY = null;
	}

	shouldComponentUpdate(props, state) {
		if (
			props.cancelPolicy !== null &&
			props.cancelPolicy !== this.props.cancelPolicy
		) {
			if (props.cancelPolicy.sucess && props.cancelPolicy.sucess === 0) {
				GoToast.show(props.cancelPolicy.message, I18n.t("information"), "LONG");
				this.setState({ openHelp: false });
				this.props.dispatch(saveHotelCancelPolicy(null));
			}
			return true;
		}
		return true;
	}

	getSecondaryHeaderText = () => {
		let checkInDate = this.props.stayDate.checkInDate;
		let checkOutDate = this.props.stayDate.checkOutDate;
		let rooms =
			this.props.personalInfo.length +
			" " +
			I18n.t("room", { count: this.props.personalInfo.length });
		let totalCount = 0;
		for (let item of this.props.personalInfo) {
			totalCount += item.adult + item.child.length;
		}
		let date =
			checkInDate.getDate() +
			" " +
			Util.getMonthName(checkInDate.getMonth()) +
			" - " +
			checkOutDate.getDate() +
			" " +
			Util.getMonthName(checkOutDate.getMonth());
		return date + " | " + totalCount + I18n.t("guests") + ", " + rooms;
	};

	callGetCancelHotelPolicy = (bookId, roomCode) => {
		let adult = [];
		let child = [];
		this.props.personalInfo.map(row => {
			adult.push(row.adult);
			child.push(row.child.length);
		});
		let params = {
			hotelId: this.props.hotelDetail["hotel-id"],
			checkInDate: this.props.stayDate.checkInDate,
			checkOutDate: this.props.stayDate.checkOutDate,
			rooms: this.props.personalInfo.length,
			adults: adult,
			children: child,
			bookId: bookId,
			roomType: roomCode,
			appToken: this.props.appToken
		};
		this.props.dispatch(getHotelCancelPolicy(params));
	};
	header = () => {
		return (
			<View style={styles.header}>
				<TouchableOpacity
					style={styles.backButton}
					onPress={() => {
						this.props.navigation.goBack();
					}}>
					<Icon
						iconType={"material"}
						iconSize={height / 26}
						iconName={"arrow-back"}
						iconColor={"rgba(0,0,0,0.5)"}
					/>
				</TouchableOpacity>
				<View>
					<CtHotelTextBold style={styles.primaryHeader}>
						{I18n.t("select_your_room")}
					</CtHotelTextBold>
					<CtHotelTextRegular style={styles.secondaryHeader}>
						{this.getSecondaryHeaderText()}
					</CtHotelTextRegular>
				</View>
			</View>
		);
	};

	getRooms = () => {
		let room = [];
		if (
			this.props.hotelDetail["other-info"] &&
			this.props.hotelDetail["other-info"]["rooms-info"] &&
			this.props.hotelDetail["other-info"]["rooms-info"]["room-info"]
		) {
			if (
				this.props.hotelDetail["other-info"]["rooms-info"][
					"room-info"
				] instanceof Array
			) {
				if (
					this.props.hotelDetail["other-info"]["rooms-info"][
						"room-info"
					][0] instanceof Array
				) {
					return this.props.hotelDetail["other-info"]["rooms-info"][
						"room-info"
					][0];
				} else {
					room.push(
						this.props.hotelDetail["other-info"]["rooms-info"]["room-info"][0]
					);
					return room;
				}
			} else if (
				this.props.hotelDetail["other-info"]["rooms-info"][
					"room-info"
				] instanceof Object
			) {
				room.push(
					this.props.hotelDetail["other-info"]["rooms-info"]["room-info"]
				);
				return room;
			}
		}
		return room;
	};

	getImage = room => {
		let subRoom = [];
		if (room["sub-rooms"] && room["sub-rooms"]["sub-room"]) {
			if (room["sub-rooms"]["sub-room"] instanceof Array) {
				return room["sub-rooms"]["sub-room"];
			} else if (room["sub-rooms"]["sub-room"] instanceof Object) {
				subRoom.push(room["sub-rooms"]["sub-room"]);
			}
		}
		return subRoom;
	};

	getRoomRates = roomTypeCode => {
		let hotel = this.props.hotelRates;
		let roomRate = [];
		if (hotel["room-rates"]["room-rate"] instanceof Array) {
			roomRate = hotel["room-rates"]["room-rate"];
		} else {
			roomRate.push(hotel["room-rates"]["room-rate"]);
		}
		return roomRate.filter(
			row => row["room-type"]["room-type-id"] === roomTypeCode
		);
	};

	isRoomAvailable = roomTypeCode => {
		let hotel = this.props.hotelRates;
		let roomRate = [];
		if (hotel["room-rates"]["room-rate"] instanceof Array) {
			roomRate = hotel["room-rates"]["room-rate"];
		} else {
			roomRate.push(hotel["room-rates"]["room-rate"]);
		}
		return (
			roomRate.filter(row => row["room-type"]["room-type-id"] === roomTypeCode)
				.length > 0
		);
	};
	/**
	 * function to calculate discounted price
	 */
	calculatePrice = pricingElement => {
		let price = 0;
		if (pricingElement instanceof Array) {
			pricingElement.map(cost => {
				price += cost["common:amount"];
			});
		} else {
			price = pricingElement["common:amount"];
		}
		return price;
	};
	calculateRate = rateElement => {
		let price = 0;
		if (rateElement instanceof Array) {
			rateElement.map(rates => {
				let pricingElement =
					rates["common:pricing-elements"]["common:pricing-element"];
				price += this.calculatePrice(pricingElement);
			});
		} else {
			let pricingElement =
				rateElement["common:pricing-elements"]["common:pricing-element"];
			price = this.calculatePrice(pricingElement);
		}
		return parseInt(Math.round(price));
	};
	/**
	 * function to calculate un discounted price
	 */
	calculateTotalPrice = pricingElement => {
		let price = 0;
		if (pricingElement instanceof Array) {
			pricingElement.map(cost => {
				if (cost["common:category"] !== "DIS") {
					price += cost["common:amount"];
				}
			});
		} else {
			if (pricingElement["common:category"] !== "DIS") {
				price = pricingElement["common:amount"];
			}
		}
		return price;
	};
	calculateTotalRate = rateElement => {
		let price = 0;
		if (rateElement instanceof Array) {
			rateElement.map(rates => {
				let pricingElement =
					rates["common:pricing-elements"]["common:pricing-element"];
				price += this.calculateTotalPrice(pricingElement);
			});
		} else {
			let pricingElement =
				rateElement["common:pricing-elements"]["common:pricing-element"];
			price = this.calculateTotalPrice(pricingElement);
		}
		return parseInt(Math.round(price));
	};
	getInclusionList = item => {
		let facilities = "";
		if (item.inclusions.inclusion instanceof Array) {
			if (item.inclusions.inclusion.indexOf("Free Wi-Fi") > -1) {
				facilities = facilities + "Free Wi-Fi";
			}
			if (item.inclusions.inclusion.indexOf("Breakfast") > -1) {
				facilities += facilities !== "" ? ", " : "";
				facilities = facilities + "Breakfast";
			}
		} else {
			if (item.inclusions.inclusion !== "All Applicable Taxes") {
				return item.inclusions.inclusion;
			}
		}
		return facilities;
	};

	/**
	 * function to find all offers provided by a hotel
	 */
	isOffers = item => {
		let ar = [];
		if (
			item.hasOwnProperty("promo-offers") &&
			item["promo-offers"].hasOwnProperty("promo-offer")
		) {
			if (item["promo-offers"]["promo-offer"] instanceof Array) {
				return item["promo-offers"]["promo-offer"];
			} else {
				ar.push(item["promo-offers"]["promo-offer"]);
				return ar;
			}
		}
		return ar;
	};

	/**
	 * function to display the type of bed in each room
	 */
	isBedType = subRoom => {
		if (subRoom["bed-types"] && subRoom["bed-types"]["bed-type"]) {
			return (
				<View style={[styles.infoBar, { marginLeft: width / 30 }]}>
					<CtHotelTextRegular style={styles.infoText}>
						{subRoom["bed-types"]["bed-type"].name}
					</CtHotelTextRegular>
				</View>
			);
		}
	};
	/**
	 * function to display the show more option
	 */
	isShowMore = room => {
		let amenity = this.getRoomAmenity(room, COUNT);
		console.log("amenity is", amenity);
		if (amenity > 0) {
			return (
				<TouchableOpacity
					onPress={() =>
						this.setState({
							amenityModal: true,
							amenityRoom: room
						})
					}
					style={[styles.infoBar, { marginLeft: width / 30 }]}>
					<CtHotelTextRegular style={styles.infoText}>
						{"+"}
						{amenity}
						{I18n.t("more")}
					</CtHotelTextRegular>
				</TouchableOpacity>
			);
		}
	};
	/**
	 * function to get all the amenities  of a room
	 */
	getRoomAmenity = (room, type) => {
		if (room["room-amenities"] && room["room-amenities"]["room-amenity"]) {
			let amenities =
				room["room-amenities"]["room-amenity"] instanceof Array
					? room["room-amenities"]["room-amenity"]
					: new Array(room["room-amenities"]["room-amenity"]);
			let amenity = amenities.filter(row => row.category === "General");
			if (type === COUNT && amenity.length > 0) {
				if (amenity[0].amenities && amenity[0].amenities.amenity) {
					return amenity[0].amenities.amenity.length;
				}
			}
			if (type === VALUE && amenity.length > 0) {
				if (
					amenity[0].amenities &&
					amenity[0].amenities.amenity &&
					amenity[0].amenities.amenity instanceof Array
				) {
					return amenity[0].amenities.amenity;
				} else {
					return new Array(amenity[0].amenities.amenity);
				}
			}
		}
	};

	/* Rooms which are not available in hotel info*/
	getUnMatchedRoom = () => {
		let roomInfo = this.getRooms();
		let hotel = this.props.hotelRates;
		let roomRate = [];
		if (hotel["room-rates"]["room-rate"] instanceof Array) {
			roomRate = hotel["room-rates"]["room-rate"];
		} else {
			roomRate.push(hotel["room-rates"]["room-rate"]);
		}
		let unmatchedRoom = [];
		roomRate.map(rate => {
			if (
				roomInfo.filter(
					room => rate["room-type"]["room-type-id"] === room["room-type-code"]
				).length < 1
			) {
				unmatchedRoom.push(rate);
			}
		});
		console.log("unmatchedRoom", unmatchedRoom);
		return unmatchedRoom;
	};
	render() {
		return (
			<View style={styles.bodyStyle}>
				<this.header />
				{this.isOffers(this.props.hotelRates).map((item, index) => {
					return (
						<View style={styles.pointStyle} key={index}>
							<Image
								resizeMode={"contain"}
								source={OFFER}
								style={styles.offerImg}
							/>
							<CtHotelTextRegular style={styles.pointText}>
								{item.desc}
							</CtHotelTextRegular>
						</View>
					);
				})}
				<FlatList
					data={this.getUnMatchedRoom()}
					keyExtractor={(item, index) => index.toString()}
					renderItem={({ item }) => {
						return (
							<TouchableOpacity
								style={styles.unmatchedView}
								onPress={() => {
									this.setState({
										selectedRoomId: item["room-type"]["room-type-code"],
										selectedRoom: item
									});
								}}>
								<View style={styles.unmatchedInnerView}>
									<CheckBox
										width={width / 30}
										height={width / 30}
										borderRadius={width / 60}
										checkColor={"#3366cc"}
										value={
											this.state.selectedRoomId ===
											item["room-type"]["room-type-code"]
										}
									/>
									<CtHotelTextRegular style={styles.unmatchedTextDescription}>
										{item["room-type"]["room-description"]}
									</CtHotelTextRegular>
								</View>
								<CtHotelTextRegular style={styles.unmatchedText}>
									{I18n.toCurrency(item.totalChargeableRate, {
										precision: 0,
										unit: "₹"
									})}
								</CtHotelTextRegular>
								<View style={styles.listTextView}>
									<CtHotelTextRegular
										style={[
											styles.listText,
											{
												color:
													item.hasOwnProperty("special-categories") &&
													item["special-categories"] === "FREE_CANCEL_POLICY"
														? "#A6DD7F"
														: "#000"
											}
										]}>
										{item.hasOwnProperty("special-categories") &&
										item["special-categories"] === "FREE_CANCEL_POLICY"
											? "Free Cancellation"
											: item.hasOwnProperty("cancel-policy")
											? I18n.t("cancellation_policy")
											: I18n.t("non_refundable")}
									</CtHotelTextRegular>
									{item.hasOwnProperty("cancel-policy") && (
										<TouchableOpacity
											style={styles.rowView}
											onPress={evt => {
												this.posX = evt.nativeEvent.pageX;
												this.posY = evt.nativeEvent.pageY;
												this.setState({
													openHelp: true
												});
											}}>
											<CtHotelTextRegular style={styles.detailsText}>
												{I18n.t("details")}
											</CtHotelTextRegular>
											{this.state.openHelp && (
												<ToolTip
													visible={this.state.openHelp}
													message={
														item.hasOwnProperty("cancel-policy")
															? item["cancel-policy"]
															: item.refundable
													}
													left={this.posX}
													right={this.posY}
													onRequestClose={() => {
														this.setState({
															openHelp: false
														});
													}}
												/>
											)}
										</TouchableOpacity>
									)}
								</View>
							</TouchableOpacity>
						);
					}}
				/>
				<FlatList
					style={styles.flatListContainer}
					data={this.getRooms()}
					extraData={this.state}
					keyExtractor={(item, index) => index.toString()}
					renderItem={({ item, index }) => {
						let room = item;
						let subRoom = this.getImage(room);
						return (
							<View style={styles.listItem}>
								{subRoom.length > 0 && (
									<View>
										{this.isRoomAvailable(subRoom[0]["room-type-code"]) && (
											<View
												style={[
													styles.imageStyle,
													{
														height:
															subRoom[0]["image-info"] !== undefined
																? height / 4
																: height / 8
													}
												]}>
												<FlatList
													extraData={this.state}
													horizontal={true}
													data={subRoom}
													keyExtractor={(item, index) => index.toString()}
													renderItem={({ item, index }) => {
														return (
															<View>
																{item["image-info"] ? (
																	<ImageBackground
																		source={{
																			uri:
																				CLEARTRIP_SERVER_IMAGE_URL +
																				item["image-info"].image[
																					"wide-angle-image-url"
																				]
																		}}
																		style={styles.imageStyle}
																	/>
																) : (
																	<ImageBackground
																		source={EMPTYIMAGE}
																		style={styles.imageStyle}
																	/>
																)}
															</View>
														);
													}}
												/>
												{/* fixed section which shows area of room*/}
												<View style={styles.imageBottomView}>
													<CtHotelTextBold
														style={[
															styles.roomText,
															{
																color:
																	subRoom[0]["image-info"] !== undefined
																		? "#fff"
																		: "#000"
															}
														]}>
														{room["room-type"]}
													</CtHotelTextBold>
													<View style={styles.infoContainer}>
														<View style={styles.infoBar}>
															<CtHotelTextRegular style={styles.infoText}>
																{subRoom[0].sqft + " sq ft"}
															</CtHotelTextRegular>
														</View>
														{this.isBedType(subRoom[0])}
														{this.isShowMore(room)}
													</View>
												</View>
											</View>
										)}
										{/* this section will displays different rates available of a room*/}
										<FlatList
											extraData={this.state}
											data={this.getRoomRates(subRoom[0]["room-type-code"])}
											keyExtractor={(item, index) => index.toString()}
											renderItem={({ item, index }) => {
												return (
													<TouchableOpacity
														style={styles.listButton}
														onPress={() => {
															console.log("room =>", subRoom[0]);
															this.setState({
																selectedRoomId:
																	item["room-type"]["room-type-code"],
																selectedRoom: item
															});
														}}>
														<View style={styles.roomContainer}>
															<CheckBox
																width={width / 30}
																height={width / 30}
																borderRadius={width / 60}
																checkColor={"#3366cc"}
																value={
																	this.state.selectedRoomId ===
																	item["room-type"]["room-type-code"]
																}
															/>
															<CtHotelTextBold style={styles.roomRate}>
																{"₹"}
																{I18n.toNumber(
																	this.calculateRate(
																		item["rate-breakdown"]["common:rate"]
																	),
																	{ precision: 0 }
																)}
															</CtHotelTextBold>
															<CtHotelTextRegular style={styles.baseFareText}>
																{this.calculateRate(
																	item["rate-breakdown"]["common:rate"]
																) !==
																	this.calculateTotalRate(
																		item["rate-breakdown"]["common:rate"]
																	) &&
																	"₹" +
																		I18n.toNumber(
																			this.calculateTotalRate(
																				item["rate-breakdown"]["common:rate"]
																			),
																			{ precision: 0 }
																		)}
															</CtHotelTextRegular>
														</View>
														<View style={styles.marginLeftView}>
															<CtHotelTextRegular style={styles.listText}>
																{this.getInclusionList(item)}
															</CtHotelTextRegular>
															<View style={styles.rowView}>
																<CtHotelTextRegular
																	style={[
																		styles.listText,
																		{
																			color:
																				item.hasOwnProperty(
																					"special-categories"
																				) &&
																				item["special-categories"] ===
																					"FREE_CANCEL_POLICY"
																					? "#A6DD7F"
																					: "#000"
																		}
																	]}>
																	{item.hasOwnProperty("special-categories") &&
																	item["special-categories"] ===
																		"FREE_CANCEL_POLICY"
																		? "Free Cancellation"
																		: item.hasOwnProperty("cancel-policy")
																		? I18n.t("cancellation_policy")
																		: I18n.t("non_refundable")}
																</CtHotelTextRegular>
																{item.hasOwnProperty("cancel-policy") && (
																	<TouchableOpacity
																		style={styles.rowView}
																		onPress={evt => {
																			this.posX = evt.nativeEvent.pageX;
																			this.posY = evt.nativeEvent.pageY;
																			console.log(this.state);
																			this.setState({
																				openHelp: true
																			});
																		}}>
																		<CtHotelTextRegular
																			style={styles.detailsText}>
																			{I18n.t("details")}
																		</CtHotelTextRegular>
																		{this.state.openHelp && (
																			<ToolTip
																				visible={this.state.openHelp}
																				message={
																					item.hasOwnProperty("cancel-policy")
																						? item["cancel-policy"]
																						: item.refundable
																				}
																				left={this.posX}
																				right={this.posY}
																				onRequestClose={() => {
																					this.setState({
																						openHelp: false
																					});
																				}}
																			/>
																		)}
																	</TouchableOpacity>
																)}
															</View>
														</View>
													</TouchableOpacity>
												);
											}}
										/>
										<Modal
											transparent={true}
											onRequestClose={() => {
												console.log("amenmity room is", this.state.amenityRoom);
												this.setState({ amenityModal: false });
											}}
											visible={this.state.amenityModal}>
											<TouchableOpacity
												onPress={() => this.setState({ amenityModal: false })}
												style={styles.amenityContainer}>
												{this.state.amenityRoom && (
													<ScrollView
														contentContainerStyle={styles.amenitySection}>
														{this.getRoomAmenity(
															this.state.amenityRoom,
															VALUE
														).map((amenity, index) => {
															return (
																<TouchableOpacity
																	key={index}
																	style={styles.amenityList}>
																	<CtHotelTextRegular
																		style={styles.amenityText}>
																		{amenity}
																	</CtHotelTextRegular>
																</TouchableOpacity>
															);
														})}
														<TouchableOpacity
															onPress={() =>
																this.setState({ amenityModal: false })
															}
															style={styles.closeButton}>
															<CtHotelTextBold style={styles.closeText}>
																{I18n.t("close_C")}
															</CtHotelTextBold>
														</TouchableOpacity>
													</ScrollView>
												)}
											</TouchableOpacity>
										</Modal>
									</View>
								)}
							</View>
						);
					}}
				/>

				<View style={styles.bottomBarRow}>
					<TouchableOpacity
						style={styles.bottomButton}
						onPress={() => {
							if (this.state.selectedRoomId !== null) {
								this.props.dispatch(
									getOtp({
										appToken: this.props.appToken
									})
								);
								this.props.navigation.navigate("BookHotel", {
									room: this.state.selectedRoom
								});
							} else {
								GoToast.show(
									I18n.t("please_select_a_room"),
									I18n.t("information"),
									"LONG"
								);
							}
						}}>
						<CtHotelTextBold style={styles.roomText}>
							{I18n.t("continue_booking")}
						</CtHotelTextBold>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		stayDate: state.cleartripHotel && state.cleartripHotel.searchHotel.stayDate,
		personalInfo:
			state.cleartripHotel && state.cleartripHotel.searchHotel.personalInfo,
		searchType:
			state.cleartripHotel && state.cleartripHotel.searchHotel.searchType,
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		hotels:
			state.cleartripHotel &&
			state.cleartripHotel.searchHotel &&
			state.cleartripHotel.searchHotel.hotels
				? state.cleartripHotel.searchHotel.hotels
				: null,
		error: state.cleartripHotel && state.cleartripHotel.searchHotel.error,
		shortListHotel:
			state.cleartripHotel && state.cleartripHotel.selectHotel.shortListHotel,
		hotelDetail:
			state.cleartripHotel && state.cleartripHotel.selectHotel.hotelInfo,
		hotelRates:
			state.cleartripHotel &&
			state.cleartripHotel.selectHotel.hotelRateInfo &&
			state.cleartripHotel.selectHotel.hotelRateInfo.hotels &&
			state.cleartripHotel.selectHotel.hotelRateInfo.hotels.hotel &&
			state.cleartripHotel.selectHotel.hotelRateInfo.hotels.hotel,
		cancelPolicy:
			state.cleartripHotel && state.cleartripHotel.selectRoom.cancelPolicy
	};
}

export default connect(mapStateToProps)(SelectRoom);
