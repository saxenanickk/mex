import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api/";
import { saveServerError } from "../SearchHotel/Saga";
export const CTHOTEL_GET_HOTEL_CANCEL_POLICY =
	"CTHOTEL_GET_HOTEL_CANCEL_POLICY";
export const CTHOTEL_SAVE_HOTEL_CANCEL_POLICY =
	"CTHOTEL_SAVE_HOTEL_CANCEL_POLICY";
export const CTHOTEL_GET_OTP = "CTHOTEL_GET_OTP";
export const CTHOTEL_SAVE_OTP = "CTHOTEL_SAVE_OTP";
export const getHotelCancelPolicy = payload => ({
	type: CTHOTEL_GET_HOTEL_CANCEL_POLICY,
	payload
});

export const saveHotelCancelPolicy = payload => ({
	type: CTHOTEL_SAVE_HOTEL_CANCEL_POLICY,
	payload
});

export const getOtp = payload => ({
	type: CTHOTEL_GET_OTP,
	payload
});

export const saveOtp = payload => ({
	type: CTHOTEL_SAVE_OTP,
	payload
});
export function* selectRoomSaga(dispatch) {
	yield takeEvery(CTHOTEL_GET_HOTEL_CANCEL_POLICY, handleGetHotelCancelPolicy);
	yield takeEvery(CTHOTEL_GET_OTP, handleGetOtp);
}

function* handleGetHotelCancelPolicy(action) {
	try {
		let cancelPolicy = yield call(Api.getHotelCancelPolicy, action.payload);
		if (cancelPolicy.hasOwnProperty("rate-rules-response")) {
			yield put(saveHotelCancelPolicy(cancelPolicy["rate-rules-response"]));
		} else {
			yield put(saveHotelCancelPolicy(cancelPolicy));
		}
	} catch (error) {
		console.log("Hotel Cancel Policy error:", error);
		yield put(
			saveServerError({
				error: true,
				message: "Unable to get cancellation policy, try again later"
			})
		);
	}
}

function* handleGetOtp(action) {
	try {
		let otpResponse = yield call(Api.getOTP, action.payload);
		console.log("otp response is:", otpResponse);
		if (otpResponse.hasOwnProperty("otp-response")) {
			yield put(saveOtp(otpResponse["otp-response"]));
		}
	} catch (error) {
		console.log("Hotel get Otp Error:", error);
	}
}
