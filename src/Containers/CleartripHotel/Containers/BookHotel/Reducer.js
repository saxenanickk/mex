import {
	CTHOTEL_SAVE_PROVISIONAL_BOOKING,
	CTHOTEL_SAVE_BOOKING,
	CTHOTEL_SAVE_PROVISIONAL_BOOK_ERROR,
	CTHOTEL_SAVE_BOOKING_ERROR
} from "./Saga";

const initialState = {
	provisionalBooking: null,
	bookResponse: null,
	provisionalBookError: false,
	bookingError: false
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case CTHOTEL_SAVE_PROVISIONAL_BOOKING:
			return {
				...state,
				provisionalBooking: action.payload
			};
		case CTHOTEL_SAVE_BOOKING:
			return {
				...state,
				bookResponse: action.payload
			};
		case CTHOTEL_SAVE_PROVISIONAL_BOOK_ERROR:
			return {
				...state,
				provisionalBookError: action.payload
			};
		case CTHOTEL_SAVE_BOOKING_ERROR:
			return {
				...state,
				bookingError: action.payload
			};
		default:
			return state;
	}
};
