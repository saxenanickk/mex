import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api/";
import { saveServerError } from "../SearchHotel/Saga";
export const CTHOTEL_GET_PROVISIONAL_BOOKING =
	"CTHOTEL_GET_PROVISIONAL_BOOKING";
export const CTHOTEL_SAVE_PROVISIONAL_BOOKING =
	"CTHOTEL_SAVE_PROVISIONAL_BOOKING";
export const CTHOTEL_MAKE_BOOKING = "CTHOTEL_MAKE_BOOKING";
export const CTHOTEL_SAVE_BOOKING = "CTHOTEL_SAVE_BOOKING";
export const CTHOTEL_SAVE_PROVISIONAL_BOOK_ERROR =
	"CTHOTEL_SAVE_PROVISIONAL_BOOK_ERROR";
export const CTHOTEL_SAVE_BOOKING_ERROR = "CTHOTEL_SAVE_BOOKING_ERROR";
export const getProvisionalBooking = payload => ({
	type: CTHOTEL_GET_PROVISIONAL_BOOKING,
	payload
});

export const saveProvisionalBooking = payload => ({
	type: CTHOTEL_SAVE_PROVISIONAL_BOOKING,
	payload
});

export const makeBooking = payload => ({
	type: CTHOTEL_MAKE_BOOKING,
	payload
});
export const saveBooking = payload => ({
	type: CTHOTEL_SAVE_BOOKING,
	payload
});
export const saveProvisionalBookingError = payload => ({
	type: CTHOTEL_SAVE_PROVISIONAL_BOOK_ERROR,
	payload
});
export const saveBookingError = payload => ({
	type: CTHOTEL_SAVE_BOOKING_ERROR,
	payload
});
export function* bookHotelSaga(dispatch) {
	yield takeEvery(CTHOTEL_GET_PROVISIONAL_BOOKING, handleGetProvisionalBooking);
	yield takeLatest(CTHOTEL_MAKE_BOOKING, handleMakeBooking);
}

function* handleGetProvisionalBooking(action) {
	try {
		let response = yield call(Api.getProvisionalBooking, action.payload);
		console.log("response of provisional booking:", response);
		if (response.hasOwnProperty("provisional-book-response")) {
			yield put(saveProvisionalBooking(response["provisional-book-response"]));
		} else {
			yield put(saveProvisionalBookingError(true));
		}
	} catch (error) {
		console.log("Error of provisional booking:", error);
		yield put(saveProvisionalBookingError(true));
	}
}

function* handleMakeBooking(action) {
	try {
		let response = yield call(Api.getHotelBooking, action.payload);
		console.log("response of  booking:", response);
		if (response.hasOwnProperty("book-response")) {
			yield put(saveBooking(response["book-response"]));
		} else {
			yield put(saveBookingError(true));
		}
	} catch (error) {
		console.log("Error of provisional booking:", error);
		yield put(saveBookingError(true));
	}
}
