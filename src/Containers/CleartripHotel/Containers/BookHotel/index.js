/* eslint-disable radix */
import React, { Component } from "react";
import {
	View,
	ScrollView,
	Dimensions,
	TextInput,
	TouchableOpacity,
	FlatList,
	ImageBackground,
	Alert,
	Image,
	BackHandler,
	Platform,
	LayoutAnimation,
	UIManager,
	Keyboard
} from "react-native";
import { connect } from "react-redux";
import Util, { ONE_DAY_EPOCH_DURATION } from "../../Util";
import I18n from "../../Assets/Strings/i18n";
import Razorpay from "../../../../CustomModules/RazorPayModule";
import styles from "./styles";
import {
	StarReview,
	CTProgressScreen,
	CtHotelTextRegular,
	CtHotelTextBold
} from "../../Components";
import { Seperator, Icon } from "../../../../Components";
import {
	getProvisionalBooking,
	saveProvisionalBooking,
	makeBooking,
	saveBooking,
	saveProvisionalBookingError,
	saveBookingError
} from "./Saga";

import { NORESULT } from "../../Assets/Img/Image";
import { GoToast } from "../../../../Components";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import Config from "react-native-config";
import { CommonActions } from "@react-navigation/native";

const { CLEARTRIP_SERVER_IMAGE_URL } = Config;
const { height, width } = Dimensions.get("window");
const PROVISIONAL_BOOK = 0;
const CONFIRM_BOOK = 1;
if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}
const ErrorScreen = props => {
	return (
		<View style={styles.errorScreen}>
			<Image
				resizeMode={"contain"}
				source={NORESULT}
				style={styles.noResultImage}
			/>
			<CtHotelTextBold style={styles.errorText}>
				{props.primaryMessage}
			</CtHotelTextBold>
			<CtHotelTextBold style={styles.errorText}>
				{props.secondaryMessage}
			</CtHotelTextBold>
			<TouchableOpacity style={styles.goBack} onPress={() => props.callBack()}>
				<CtHotelTextRegular style={styles.buttonText}>
					{I18n.t("go_home")}
				</CtHotelTextRegular>
			</TouchableOpacity>
		</View>
	);
};
class BookHotel extends Component {
	constructor(props) {
		super(props);
		this.room =
			this.props.route.params && this.props.route.params.room
				? this.props.route.params.room
				: null;
		this.state = {
			type: "Mr",
			firstName: "",
			lastName: "",
			emailId: "",
			phone: "",
			rate: this.calculateRate(),
			provisionalBook: false,
			bookCode: PROVISIONAL_BOOK,
			isError: false,
			postPay: false,
			isHeaderVisible: false
		};
		this.isAnimated = true;
		this.scrollViewHeight = 0;
		this.textInputTouchIndex = 0;
		this.scrollRef = null;
		this.textInputYOffset = [];
		this._keyboardDidShow = this._keyboardDidShow.bind(this);
		this._keyboardDidHide = this._keyboardDidHide.bind(this);
		this.keyboardDidShowListener = Keyboard.addListener(
			"keyboardDidShow",
			this._keyboardDidShow
		);
		this.keyboardDidHideListener = Keyboard.addListener(
			"keyboardDidHide",
			this._keyboardDidHide
		);
	}

	_keyboardDidShow = event => {
		try {
			if (Platform.OS === "ios") {
				if (this.scrollRef !== null && this.scrollRef !== undefined) {
					let index = this.textInputTouchIndex;
					this.keyBoardHeight = event.endCoordinates.height;
					if (
						this.scrollViewHeight + this.textInputYOffset[index] >
						event.endCoordinates.height
					) {
						this.scrollRef.scrollTo({
							x: 0,
							y:
								this.scrollViewHeight +
								this.textInputYOffset[index] -
								this.keyBoardHeight,
							animated: true
						});
					}
				}
			}
		} catch (error) {
			console.log("do nothing");
		}
	};

	_keyboardDidHide = () => {
		try {
			if (Platform.OS === "ios") {
				if (this.scrollRef !== null && this.scrollRef !== undefined) {
					this.scrollRef.scrollTo({
						x: 0,
						y: this.keyBoardHeight,
						animated: true
					});
				}
			}
		} catch (error) {
			console.log("do nothing", error);
		}
	};
	getDate = date => {
		let month = Util.getMonthName(date.getMonth()).toUpperCase();
		let day = date.getDate();
		day = day < 10 ? "0" + day : day;
		return month + " " + day;
	};

	getTime = date => {
		let time = date.toDateString().split(" ");
		return time[0] + ", 12pm";
	};

	calculatePrice = pricingElement => {
		let price = 0;
		if (pricingElement instanceof Array) {
			pricingElement.map(cost => {
				price += cost["common:amount"];
			});
		} else {
			price = pricingElement["common:amount"];
		}
		return price;
	};

	calculateRate = () => {
		let rateElement = this.room["rate-breakdown"]["common:rate"];
		let price = 0;
		if (rateElement instanceof Array) {
			rateElement.map(rates => {
				let pricingElement =
					rates["common:pricing-elements"]["common:pricing-element"];
				price += this.calculatePrice(pricingElement);
			});
		} else {
			let pricingElement =
				rateElement["common:pricing-elements"]["common:pricing-element"];
			price = this.calculatePrice(pricingElement);
		}
		return parseInt(Math.round(price));
	};
	getOffers = () => {
		let offers = [];
		if (this.room.offers && this.room.offers.offer) {
			if (this.room.offers.offer instanceof Array) {
				return this.room.offers.offer;
			} else {
				offers.push(this.room.offers.offer);
				return offers;
			}
		}
		return offers;
	};

	validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}

	validateContactDetail = () => {
		if (!this.state.firstName) {
			return "First Name is required";
		}
		if (!this.state.lastName) {
			return "Last Name is required";
		}
		if (!this.state.phone) {
			return "Mobile number is required.";
		}
		if (this.state.phone.length < 10) {
			return "Mobile number must be atleast 10 digit";
		}
		if (!this.state.emailId) {
			return "Email is required.";
		}
		if (!this.validateEmail(this.state.emailId)) {
			return "Email address is not valid.";
		}
		return null;
	};

	getSecondaryStickyHeader = () => {
		let msg = "";
		let room = this.props.personalInfo.length;
		msg = room + " " + I18n.t("room", { count: room });
		let night =
			new Date(this.props.stayDate.checkOutDate).getTime() -
			new Date(this.props.stayDate.checkInDate).getTime();

		night = parseInt(night) / ONE_DAY_EPOCH_DURATION;
		msg +=
			" " +
			I18n.t("for") +
			" " +
			night +
			" " +
			I18n.t("night", { count: night });
		return msg;
	};
	getBookingParameter = () => {
		let adult = [];
		let child = [];
		let childrenAge = [];
		this.props.personalInfo.map(row => {
			let childAge = [];
			row.child.map(obj => childAge.push(obj.age));
			childrenAge.push(childAge);
			adult.push(row.adult);
			child.push(row.child.length);
		});
		let params = {
			HotelId: this.props.hotelDetail["hotel-id"],
			CheckInDate: this.props.stayDate.checkInDate,
			CheckOutDate: this.props.stayDate.checkOutDate,
			NoOfRooms: this.props.personalInfo.length,
			Adults: adult,
			Children: child,
			ChildAge: childrenAge,
			BookId: this.room["booking-code"],
			RoomType: this.room["room-type"]["room-type-code"],
			Country: "India",
			Currency: "INR",
			_title: this.state.type,
			firstName: this.state.firstName,
			lastName: this.state.lastName,
			mobileNo: this.state.phone,
			Email: this.state.emailId
		};
		return params;
	};
	bookHotel = () => {
		var validationErrorMsg = this.validateContactDetail();
		if (validationErrorMsg !== null) {
			GoToast.show(validationErrorMsg, I18n.t("error"));
		} else {
			let params = this.getBookingParameter();
			if (this.state.postPay === true) {
				params = {
					...params,
					post_pay: this.state.postPay,
					ItineraryId: this.props.Otp["itinerary-id"],
					Otp: this.props.Otp.otp
				};
			}
			this.setState({ provisionalBook: true, bookCode: PROVISIONAL_BOOK });
			this.props.dispatch(getProvisionalBooking(params));
		}
	};

	shouldComponentUpdate(props, state) {
		if (
			props.provisionalId !== null &&
			props.provisionalId !== this.props.provisionalId
		) {
			this.setState({ provisionalBook: false });
			if (props.provisionalId.hasOwnProperty("provisional-book-id")) {
				this.sendToPayment();
			} else {
				this.props.navigation.dispatch(
					CommonActions.reset({
						index: 0,
						routes: [{ name: "SearchHotel" }]
					})
				);
				GoToast.show(
					I18n.t("unable_to_book_hotel"),
					I18n.t("information"),
					"LONG"
				);
			}
			return true;
		}
		if (
			props.bookResponse !== null &&
			props.bookResponse !== this.props.bookResponse
		) {
			this.setState({ provisionalBook: false });
			this.props.navigation.navigate("TripDetail", {
				tripId: props.bookResponse["booking-id"]
			});
		}
		if (
			props.provisionalBookError === true &&
			props.provisionalBookError !== this.props.provisionalBookError
		) {
			this.setState({ isError: true, provisionalBook: false });
		}
		if (
			props.bookingError === true &&
			props.bookingError !== this.props.bookingError
		) {
			this.setState({ isError: true, provisionalBook: false });
		}
		return true;
	}

	componentDidMount() {
		this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
			this.closeBooking();
			return true;
		});
	}
	componentWillUnmount() {
		this.props.dispatch(saveProvisionalBooking(null));
		this.props.dispatch(saveBooking(null));
		this.props.dispatch(saveBookingError(false));
		this.props.dispatch(saveProvisionalBookingError(false));
		this.backHandler.remove();
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	}
	getDescription = () => {
		return "Book hotel " + this.props.hotelDetail["basic-info"]["hotel-name"];
	};
	sendToPayment = () => {
		Razorpay({
			app: "Cleartrip",
			color: "#f26822",
			description: this.getDescription(),
			amount: parseInt(this.state.rate)
		})
			.then(response => {
				let params = this.getBookingParameter();
				params = {
					...params,
					ProvisionalId: this.props.provisionalId["provisional-book-id"],
					Amount: parseInt(this.state.rate),
					paymentId: response.razorpay_payment_id
				};
				try {
					GoAppAnalytics.trackChargeWithProperties(
						parseInt(this.state.rate, 10),
						response.razorpay_payment_id
					);
				} catch (error) {
					console.log("Error GoAppAnalytics.trackChargeWithProperties", error);
				}
				this.props.dispatch(makeBooking(params));
				this.setState({ provisionalBook: true, bookCode: CONFIRM_BOOK });
			})
			.catch(error => {
				console.log("RazorPay Error: ", error);
				GoToast.show(error.description, I18n.t("error"));
				this.props.navigation.goBack();
				if (error.code === 0) {
					Alert.alert(
						I18n.t("cancelled"),
						I18n.t("payment_cancelled_by_user"),
						[
							{
								text: I18n.t("cancel"),
								onPress: () => {
									console.log("cancel pressed");
								},
								style: "cancel"
							},
							{
								text: I18n.t("ok"),
								onPress: () => {
									this.props.navigation.dispatch(
										CommonActions.reset({
											index: 0,
											routes: [{ name: "SearchHotel" }]
										})
									);
								}
							}
						],
						{ cancelable: false }
					);
				}
			});
	};

	closeBooking = () => {
		Alert.alert(
			I18n.t("warning"),
			this.props.provisionalId !== null
				? I18n.t("cancel_after_payment")
				: I18n.t("cancel_before_payment"),
			[
				{
					text: I18n.t("cancel"),
					onPress: () => {
						console.log("cancel pressed");
					},
					style: "cancel"
				},
				{
					text: I18n.t("ok"),
					onPress: () => {
						this.props.provisionalId !== null
							? this.props.navigation.dispatch(
									CommonActions.reset({
										index: 0,
										routes: [{ name: "SearchHotel" }]
									})
							  )
							: this.props.navigation.goBack();
					}
				}
			],
			{ cancelable: false }
		);
	};

	render() {
		return (
			<View style={styles.container}>
				{this.state.provisionalBook && (
					<CTProgressScreen
						indicatorSize={"large"}
						indicatorColor={"#3366cc"}
						message={
							this.state.bookCode === PROVISIONAL_BOOK
								? I18n.t("getting_provisional_book")
								: I18n.t("booking_hotel")
						}
					/>
				)}
				{this.state.isError === true ? (
					<ErrorScreen
						callBack={() => {
							this.props.navigation.dispatch(
								CommonActions.reset({
									index: 0,
									routes: [{ name: "SearchHotel" }]
								})
							);
						}}
						primaryMessage={I18n.t("error_message_while_booking")}
						secondaryMessage={
							this.state.bookCode === PROVISIONAL_BOOK
								? I18n.t("try_again")
								: I18n.t("amount_return_message_on_booking_fail")
						}
					/>
				) : (
					<View style={{ flex: 1 }}>
						{this.state.isHeaderVisible ? (
							<View style={styles.stickyHeaderContainer}>
								<TouchableOpacity
									style={styles.backButton}
									onPress={() => {
										this.props.navigation.goBack();
									}}>
									<Icon
										iconType={"material"}
										iconSize={height / 26}
										iconName={"arrow-back"}
										iconColor={"#000"}
									/>
								</TouchableOpacity>
								<View
									style={{
										height: height / 15,
										justifyContent: "center"
									}}>
									<CtHotelTextRegular style={styles.hotelNameText}>
										{this.props.hotelDetail["basic-info"]["hotel-name"]}
									</CtHotelTextRegular>
									<CtHotelTextRegular style={styles.stickyHeaderText}>
										{this.getSecondaryStickyHeader()}
									</CtHotelTextRegular>
								</View>
							</View>
						) : null}
						<ScrollView
							style={styles.container}
							ref={ref => (this.scrollRef = ref)}
							onScroll={event => {
								if (
									event.nativeEvent.contentOffset.y > height / 3.5 &&
									this.isAnimated === true
								) {
									this.isAnimated = false;
									LayoutAnimation.easeInEaseOut();
									this.setState({ isHeaderVisible: true });
								} else if (
									event.nativeEvent.contentOffset.y < height / 3.5 &&
									this.isAnimated === false
								) {
									this.isAnimated = true;
									LayoutAnimation.easeInEaseOut();
									this.setState({ isHeaderVisible: false });
								}
							}}
							scrollEventThrottle={16}
							keyboardShouldPersistTaps={"always"}>
							<View key="parallax-header" style={styles.parallaxHeader}>
								<ImageBackground
									style={styles.imageStyle}
									source={{
										uri:
											CLEARTRIP_SERVER_IMAGE_URL +
											this.props.hotelDetail["basic-info"]["thumb-nail-image"]
									}}>
									<View style={styles.hotelInfo}>
										<CtHotelTextBold style={styles.textStyle} numberOfLines={2}>
											{this.props.hotelDetail["basic-info"]["hotel-name"]}
										</CtHotelTextBold>
										<CtHotelTextRegular style={styles.lowerTextStyle}>
											{this.props.hotelDetail["basic-info"].locality +
												", " +
												this.props.hotelDetail["basic-info"].city}
										</CtHotelTextRegular>
										<StarReview
											rating={
												this.props.hotelDetail["basic-info"]["star-rating"]
											}
											darkStar={"#fff"}
											lightStar={"rgba(255,255,255,0.5)"}
										/>
									</View>
									<TouchableOpacity
										style={styles.fixedBack}
										onPress={() => this.props.navigation.goBack()}>
										<Icon
											iconType={"material"}
											iconSize={height / 26}
											iconName={"arrow-back"}
											iconColor={"#fff"}
										/>
									</TouchableOpacity>
								</ImageBackground>
								<View style={styles.timeInfo}>
									<View style={styles.checkTypeContainer}>
										<CtHotelTextRegular style={styles.checkType}>
											{I18n.t("check_in")}
										</CtHotelTextRegular>
										<CtHotelTextBold style={styles.checkDate}>
											{this.getDate(this.props.stayDate.checkInDate)}
										</CtHotelTextBold>
										<CtHotelTextRegular style={styles.checkTime}>
											{this.getTime(this.props.stayDate.checkInDate)}
										</CtHotelTextRegular>
									</View>
									<View style={styles.checkTypeIconContainer}>
										<Icon
											iconType={"ionicon"}
											iconSize={width / 20}
											iconName={"md-time"}
											iconColor={"rgba(0,0,0,0.4)"}
										/>
										<CtHotelTextRegular style={styles.checkType}>
											{this.props.searchCriteria["number-of-nights"] +
												" " +
												I18n.t("night", {
													count: this.props.searchCriteria["number-of-nights"]
												})}
										</CtHotelTextRegular>
									</View>
									<View style={styles.checkOutContainer}>
										<CtHotelTextRegular style={styles.checkType}>
											{I18n.t("check_out")}
										</CtHotelTextRegular>
										<CtHotelTextBold style={styles.checkDate}>
											{this.getDate(this.props.stayDate.checkOutDate)}
										</CtHotelTextBold>
										<CtHotelTextRegular style={styles.checkTime}>
											{this.getTime(this.props.stayDate.checkOutDate)}
										</CtHotelTextRegular>
									</View>
								</View>
								<View style={styles.bottomBorder} />
							</View>
							<View style={styles.offerSection}>
								<CtHotelTextBold style={styles.price}>
									{"₹" +
										I18n.toNumber(this.state.rate, { precision: 0 }) +
										" " +
										I18n.t("for") +
										" " +
										this.props.personalInfo.length +
										" " +
										I18n.t("room", { count: this.props.personalInfo.length })}
								</CtHotelTextBold>
								<CtHotelTextRegular style={styles.roomType}>
									{this.room["room-type"]["room-type-name"]}
								</CtHotelTextRegular>
								<Seperator
									style={styles.seperator}
									width={width / 10}
									height={height / 1200}
									color={"rgba(0,0,0,0.5)"}
								/>
								<View style={styles.dealView}>
									<CtHotelTextRegular style={styles.dealtext}>
										{I18n.t("deal")}
									</CtHotelTextRegular>
								</View>
								<FlatList
									style={styles.flatListContainer}
									data={this.getOffers()}
									keyExtractor={(item, index) => index.toString()}
									renderItem={({ item }) => {
										return (
											<CtHotelTextRegular style={styles.offer}>
												{item["offer-message"]}
											</CtHotelTextRegular>
										);
									}}
								/>
							</View>
							<View style={styles.contactBox}>
								<CtHotelTextBold style={styles.contactText}>
									{I18n.t("contact_details")}
								</CtHotelTextBold>
							</View>
							<View
								style={styles.travellerDetail}
								onLayout={event =>
									(this.scrollViewHeight = event.nativeEvent.layout.y)
								}>
								<View style={styles.titleContainer}>
									<TouchableOpacity
										style={[
											styles.persontype,
											{ alignItems: "flex-start", width: width / 12 }
										]}
										onPress={() => this.setState({ type: "Mr" })}>
										<CtHotelTextRegular
											style={[
												styles.persontypeText,
												{
													color:
														this.state.type === "Mr"
															? "#000"
															: "rgba(0,0,0,0.5)"
												}
											]}>
											{I18n.t("mr")}
										</CtHotelTextRegular>
									</TouchableOpacity>
									<Seperator
										width={width / 300}
										height={height / 40}
										color={"#DFDDE0"}
									/>
									<TouchableOpacity
										style={styles.persontype}
										onPress={() => this.setState({ type: "Ms" })}>
										<CtHotelTextRegular
											style={[
												styles.persontypeText,
												{
													color:
														this.state.type === "Ms"
															? "#000"
															: "rgba(0,0,0,0.5)"
												}
											]}>
											{I18n.t("ms")}
										</CtHotelTextRegular>
									</TouchableOpacity>
									<Seperator
										width={width / 300}
										height={height / 40}
										color={"#DFDDE0"}
									/>
									<TouchableOpacity
										style={styles.persontype}
										onPress={() => this.setState({ type: "Mrs" })}>
										<CtHotelTextRegular
											style={[
												styles.persontypeText,
												{
													color:
														this.state.type === "Mrs"
															? "#000"
															: "rgba(0,0,0,0.5)"
												}
											]}>
											{I18n.t("mrs")}
										</CtHotelTextRegular>
									</TouchableOpacity>
								</View>
								<View
									style={styles.inputField}
									onLayout={event =>
										(this.textInputYOffset[0] = event.nativeEvent.layout.y)
									}
									accessible={true}
									onTouchStart={() => (this.textInputTouchIndex = 0)}>
									<TextInput
										style={styles.inputStyle}
										autoFocus={true}
										placeholder={I18n.t("first_name")}
										placeholderTextColor={"#D9D8D8"}
										value={this.state.firstName}
										onChangeText={text => this.setState({ firstName: text })}
										underlineColorAndroid={"transparent"}
										selectionColor={"#D9D8D8"}
									/>
								</View>
								<View
									style={styles.inputField}
									onLayout={event =>
										(this.textInputYOffset[1] = event.nativeEvent.layout.y)
									}
									accessible={true}
									onTouchStart={() => (this.textInputTouchIndex = 1)}>
									<TextInput
										style={styles.inputStyle}
										placeholder={I18n.t("last_name")}
										placeholderTextColor={"#D9D8D8"}
										value={this.state.lastName}
										onChangeText={text => this.setState({ lastName: text })}
										underlineColorAndroid={"transparent"}
										selectionColor={"#D9D8D8"}
									/>
								</View>
								<View
									style={styles.inputField}
									accessible={true}
									onLayout={event =>
										(this.textInputYOffset[2] = event.nativeEvent.layout.y)
									}
									onTouchStart={() => (this.textInputTouchIndex = 2)}>
									<TextInput
										style={styles.inputStyle}
										placeholder={I18n.t("email_id")}
										placeholderTextColor={"#D9D8D8"}
										value={this.state.emailId}
										onChangeText={text => this.setState({ emailId: text })}
										underlineColorAndroid={"transparent"}
										selectionColor={"#D9D8D8"}
									/>
								</View>
								<View
									style={styles.inputField}
									accessible={true}
									onLayout={event =>
										(this.textInputYOffset[3] = event.nativeEvent.layout.y)
									}
									onTouchStart={() => (this.textInputTouchIndex = 3)}>
									<TextInput
										style={styles.inputStyle}
										keyboardType={"phone-pad"}
										placeholder={I18n.t("ph_number")}
										returnKeyType={"done"}
										placeholderTextColor={"#D9D8D8"}
										value={this.state.phone}
										onChangeText={text => this.setState({ phone: text })}
										underlineColorAndroid={"transparent"}
										selectionColor={"#D9D8D8"}
									/>
								</View>
							</View>
							<View style={styles.paymentSection}>
								<CtHotelTextRegular style={styles.checkType}>
									{I18n.t("total_booking_amount")}
								</CtHotelTextRegular>
								<CtHotelTextRegular style={styles.totalPrice}>
									{"₹" +
										I18n.toNumber(parseInt(this.state.rate), { precision: 0 })}
								</CtHotelTextRegular>
								<TouchableOpacity
									style={styles.search}
									onPress={() => this.bookHotel()}>
									<CtHotelTextRegular style={styles.searchText}>
										{I18n.t("pay_securely")}
									</CtHotelTextRegular>
								</TouchableOpacity>
							</View>
						</ScrollView>
					</View>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		stayDate: state.cleartripHotel && state.cleartripHotel.searchHotel.stayDate,
		personalInfo:
			state.cleartripHotel && state.cleartripHotel.searchHotel.personalInfo,
		searchType:
			state.cleartripHotel && state.cleartripHotel.searchHotel.searchType,
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		hotels:
			state.cleartripHotel &&
			state.cleartripHotel.searchHotel &&
			state.cleartripHotel.searchHotel.hotels
				? state.cleartripHotel.searchHotel.hotels
				: null,
		error: state.cleartripHotel && state.cleartripHotel.searchHotel.error,
		shortListHotel:
			state.cleartripHotel && state.cleartripHotel.selectHotel.shortListHotel,
		hotelDetail:
			state.cleartripHotel && state.cleartripHotel.selectHotel.hotelInfo,
		hotelRates:
			state.cleartripHotel &&
			state.cleartripHotel.selectHotel &&
			state.cleartripHotel.selectHotel.hotelRateInfo &&
			state.cleartripHotel.selectHotel.hotelRateInfo.hotels &&
			state.cleartripHotel.selectHotel.hotelRateInfo.hotels.hotel
				? state.cleartripHotel.selectHotel.hotelRateInfo.hotels.hotel
				: null,
		searchCriteria:
			state.cleartripHotel &&
			state.cleartripHotel.selectHotel &&
			state.cleartripHotel.selectHotel.hotelRateInfo
				? state.cleartripHotel.selectHotel.hotelRateInfo["search-criteria"]
				: null,
		provisionalId:
			state.cleartripHotel && state.cleartripHotel.bookHotel.provisionalBooking,
		bookResponse:
			state.cleartripHotel && state.cleartripHotel.bookHotel.bookResponse,
		provisionalBookError:
			state.cleartripHotel &&
			state.cleartripHotel.bookHotel.provisionalBookError,
		bookingError:
			state.cleartripHotel && state.cleartripHotel.bookHotel.bookingError,
		Otp: state.cleartripHotel && state.cleartripHotel.selectRoom.Otp
	};
}

export default connect(
	mapStateToProps,
	null,
	null
)(BookHotel);
