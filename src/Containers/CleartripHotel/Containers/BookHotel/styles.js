import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

const styles = StyleSheet.create({
	checkTypeIconContainer: { alignItems: "center" },
	checkTypeContainer: {
		alignItems: "flex-end",
		marginRight: width / 30
	},
	checkOutContainer: {
		alignItems: "flex-start",
		marginLeft: width / 30
	},
	stickyHeaderContainer: {
		flexDirection: "row",
		alignItems: "center",
		borderBottomWidth: 1,
		borderBottomColor: "#DFDDE0",
		zIndex: 5,
		position: "absolute",
		width: width,
		height: height / 15,
		backgroundColor: "#fff",
		top: 0
	},
	hotelNameText: {
		color: "#3366cc",
		fontSize: height / 60
	},
	titleContainer: {
		flexDirection: "row",
		flex: 1,
		borderBottomColor: "#DFDDE0",
		borderBottomWidth: 1,
		alignItems: "center"
	},
	parallaxHeader: {
		zIndex: 5
	},
	backgroundContainer: { width: width, height: height / 2 },
	backgroundOpacityColor: {
		position: "absolute",
		top: 0,
		width: width,
		backgroundColor: "rgba(0,0,0,.4)",
		height: height / 2
	},
	backButton: {
		width: width / 7,
		height: height / 15,
		justifyContent: "center",
		alignItems: "center",
		borderWidth: 0,
		borderColor: "#fff"
	},
	fixedBack: {
		position: "absolute",
		top: height / 30,
		left: width / 30,
		width: width / 4,
		paddingVertical: width / 30,
		borderWidth: 0,
		borderColor: "#fff"
	},
	search: {
		marginTop: height / 50,
		width: width / 1.085,
		paddingVertical: width / 30,
		backgroundColor: "#EC6733",
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center"
	},
	searchText: {
		fontSize: height / 55,
		fontWeight: "bold",
		color: "#fff"
	},
	totalPrice: {
		fontSize: height / 35,
		color: "#000",
		marginVertical: height / 80
	},
	paymentSection: {
		borderTopColor: "#e9e9e9",
		borderTopWidth: height / 80,
		backgroundColor: "#fff",
		justifyContent: "center",
		alignItems: "center",
		paddingVertical: height / 40
	},
	inputStyle: {
		height: height / 15,
		fontSize: height / 50,
		color: "#000"
	},
	inputField: {
		marginTop: height / 70,
		borderBottomColor: "#DFDDE0",
		borderBottomWidth: 1
	},
	persontypeText: {
		fontSize: height / 50
	},
	travellerDetail: {
		paddingVertical: height / 30,
		paddingHorizontal: width / 20
	},
	persontype: {
		width: width / 8,
		height: width / 10,
		borderWidth: 0,
		borderColor: "#000",
		justifyContent: "center",
		alignItems: "center"
	},
	contactBox: {
		width: width,
		height: height / 12,
		justifyContent: "center",
		backgroundColor: "#e9e9e9",
		paddingHorizontal: width / 30,
		elevation: 0
	},
	contactText: {
		fontSize: height / 50,
		marginTop: height / 40
	},
	offer: {
		fontSize: height / 55,
		color: "#000"
	},
	dealView: {
		marginTop: height / 40,
		backgroundColor: "#15A515",
		paddingVertical: width / 200,
		paddingHorizontal: width / 40,
		borderRadius: width / 20,
		justifyContent: "center",
		alignItems: "center"
	},
	dealtext: {
		fontSize: height / 58,
		color: "#fff"
	},
	offerSection: {
		justifyContent: "center",
		alignItems: "center",
		paddingVertical: width / 30
	},
	price: {
		color: "#000",
		fontSize: height / 50
	},
	roomType: {
		marginTop: height / 150,
		color: "#000",
		fontSize: height / 50
	},
	checkType: {
		color: "rgba(0,0,0,0.4)",
		fontSize: height / 65
	},
	checkDate: {
		color: "#000",
		fontSize: height / 45,
		marginTop: height / 120
	},
	checkTime: {
		color: "rgba(0,0,0,0.4)",
		fontSize: height / 55,
		marginTop: height / 250
	},
	timeInfo: {
		flexDirection: "row",
		justifyContent: "center",
		paddingHorizontal: width / 30,
		alignItems: "center",
		height: height / 6,
		zIndex: 5
	},
	bottomBorder: {
		borderBottomColor: "#DFDDE0",
		borderBottomWidth: 1,
		width: width / 3,
		height: 2,
		alignSelf: "center"
	},
	textStyle: {
		color: "#fff",
		fontSize: height / 45,
		width: width / 1.3,
		textAlign: "center"
	},
	lowerTextStyle: {
		color: "#fff",
		fontSize: height / 55
	},
	imageStyle: {
		width: width,
		height: height / 3.5
	},
	hotelInfo: {
		width: width,
		height: height / 5,
		justifyContent: "center",
		alignItems: "center",
		borderWidth: 0,
		borderColor: "#fff"
	},
	container: {
		flex: 1,
		backgroundColor: "white"
	},
	placeholder: {
		backgroundColor: "yellow",
		flex: 1,
		height: 120,
		marginHorizontal: 20,
		marginTop: 20
	},
	goBack: {
		width: width / 1.2,
		marginTop: height / 20,
		paddingVertical: width / 30,
		backgroundColor: "#EC6733",
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center"
	},
	errorText: {
		fontSize: height / 55,
		color: "#000"
	},
	buttonText: {
		fontSize: height / 55,
		fontWeight: "bold",
		color: "#fff"
	},
	errorScreen: {
		position: "absolute",
		top: 0,
		width: width,
		height: height,
		justifyContent: "center",
		alignItems: "center"
	},
	stickyHeaderText: {
		fontSize: height / 70,
		color: "rgba(0,0,0,0.5)"
	},
	noResultImage: {
		width: width / 3,
		height: width / 3,
		marginBottom: height / 20
	},
	seperator: { marginTop: height / 30 },
	flatListContainer: { marginTop: height / 70 }
});

export default styles;
