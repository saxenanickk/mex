import {
	CTHOTEL_SAVE_CURRENT_LOCATION,
	CTHOTEL_API_ERROR,
	CTHOTEL_SAVE_FETCH_LOCATION
} from "./Saga";
const initialState = {
	currentLocation: null,
	error: null,
	isFetchingLocation: false
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case CTHOTEL_SAVE_CURRENT_LOCATION:
			return {
				...state,
				currentLocation: action.payload
			};
		case CTHOTEL_SAVE_FETCH_LOCATION:
			return {
				...state,
				isFetchingLocation: action.payload
			};
		case CTHOTEL_API_ERROR:
			return {
				...state,
				error: action.payload
			};
		default:
			return state;
	}
};
