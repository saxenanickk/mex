import React, { Component } from "react";
import { View, Image, StatusBar } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";

import {
	saveStayDate,
	savePersonalInfo,
	saveSearchType
} from "../SearchHotel/Saga";
import { SPLASH } from "../../Assets/Img/Image";
import { ONE_DAY_EPOCH_DURATION } from "../../Util";
import styles from "./styles";
import ApplicationToken from "../../../../CustomModules/ApplicationToken";
import { CommonActions } from "@react-navigation/native";

class Splash extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		this.getItemFromStore();
		setTimeout(() => {
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "SearchHotel" }]
				})
			);
		}, 2000);
	}

	/** function to save current date into store */
	storeCurrentDate = () => {
		let checkIn = new Date();
		let checkOut = new Date(checkIn.getTime() + ONE_DAY_EPOCH_DURATION);
		this.props.dispatch(
			saveStayDate({
				checkInDate: checkIn,
				checkOutDate: checkOut
			})
		);
	};

	/** retrieve items from store */
	async getItemFromStore() {
		try {
			let value = JSON.parse(await AsyncStorage.getItem("cleartripHotel"));
			if (value !== null) {
				let storedDate = new Date(value.stayDate.checkInDate);
				if (storedDate.getTime() > new Date().getTime()) {
					this.props.dispatch(
						saveStayDate({
							checkInDate: storedDate,
							checkOutDate: new Date(value.stayDate.checkOutDate)
						})
					);
				} else {
					this.storeCurrentDate();
				}
				this.props.dispatch(savePersonalInfo(value.personalInfo));
				this.props.dispatch(saveSearchType(value.searchType));
			} else {
				this.storeCurrentDate();
			}
		} catch (error) {
			console.log("error==>", error);
		}
	}

	render() {
		return (
			<View>
				<ApplicationToken />
				{/* <LocationModule
					onError={() => {
						GoToast.show(I18n.t("no_location_message"), I18n.t("information"))
					}}
				/> */}
				<StatusBar backgroundColor="#923e0b" barStyle="light-content" />
				<Image style={styles.img} source={SPLASH} />
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		stayDate: state.cleartripHotel && state.cleartripHotel.searchHotel.stayDate,
		location: state.location,
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		currentLocation:
			state.cleartripHotel && state.cleartripHotel.splash.currentLocation
	};
}

export default connect(mapStateToProps)(Splash);
