import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api/";
import { saveSearchType } from "../SearchHotel/Saga";
export const CTHOTEL_SAVE_CURRENT_LOCATION = "CTHOTEL_SAVE_CURRENT_LOCATION";
export const CTHOTEL_GET_CURRENT_LOCATION = "CTHOTEL_GET_CURRENT_LOCATION";
export const CTHOTEL_SAVE_FETCH_LOCATION = "CTHOTEL_SAVE_FETCH_LOCATION";
export const CTHOTEL_API_ERROR = "CTHOTEL_API_ERROR";
export const saveCurrentLocation = payload => ({
	type: CTHOTEL_SAVE_CURRENT_LOCATION,
	payload
});

export const getCurrentLocation = payload => ({
	type: CTHOTEL_GET_CURRENT_LOCATION,
	payload
});

export const apiFailure = payload => ({
	type: CTHOTEL_API_ERROR,
	payload
});

export const saveFetchLocation = payload => ({
	type: CTHOTEL_SAVE_FETCH_LOCATION,
	payload
});
export function* splashSaga(dispatch) {
	yield takeLatest(CTHOTEL_GET_CURRENT_LOCATION, handleGetCurrentLocation);
}

function* handleGetCurrentLocation(action) {
	try {
		let currentLocation = yield call(Api.getCurrentLocation, action.payload);
		console.log("current location is==>", currentLocation.formatted_address);
		yield put(saveCurrentLocation(currentLocation.formatted_address));
		let country = currentLocation.address_components.filter(
			item => item.types.indexOf("country") > -1
		);
		let state = currentLocation.address_components.filter(
			item => item.types.indexOf("administrative_area_level_1") > -1
		);
		let city = currentLocation.address_components.filter(
			item => item.types.indexOf("administrative_area_level_2") > -1
		);
		if (city.length < 1) {
			city = currentLocation.address_components.filter(
				item => item.types.indexOf("locality") > -1
			);
		}
		yield put(
			saveSearchType({
				type: true,
				value: {
					city: city.length ? city[0].long_name.split(" ")[0] : null,
					state: state[0].long_name.split(" ")[0],
					country: country[0].short_name.split(" ")[0]
				}
			})
		);
		yield put(saveFetchLocation(false));
		yield put(apiFailure(null));
	} catch (error) {
		console.log("get current location error:", error);
		yield put(
			apiFailure({
				error: true,
				error_message: "unable to fetch current Location"
			})
		);
		yield put(saveFetchLocation(false));
	}
}
