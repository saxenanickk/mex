import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	travellerText: {
		fontSize: height / 45,
		color: "#000"
	},
	shortListText: {
		fontSize: height / 50,
		color: "#000"
	},
	selectRoomText: {
		fontSize: height / 50,
		color: "#fff"
	},
	pickDatesAndRoomsText: {
		fontSize: height / 55
	},
	doneText: {
		fontSize: height / 55,
		color: "#3366cc"
	},
	backArrowContainer: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	backArrowButtonContainer: { flexDirection: "row", alignItems: "center" },
	secondaryHeaderContainer: { flexDirection: "row" },
	primaryHeaderContainer: { flexDirection: "row" },
	shortListHotelsLengthText: {
		color: "#fff",
		fontSize: height / 70
	},
	heartButton: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	datemsg: {
		fontSize: height / 65,
		color: "rgba(0,0,0,0.5)"
	},
	dot: {
		fontWeight: "bold",
		fontSize: height / 40,
		color: "#000"
	},
	bottomBarRow: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: width / 30,
		paddingVertical: width / 50
	},
	modalStyle: {
		width: width,
		height: height,
		backgroundColor: "rgba(0,0,0,0.3)"
	},
	modalBar: {
		position: "absolute",
		bottom: 0,
		height: height / 4,
		width: width,
		backgroundColor: "#fff"
	},
	bodyStyle: {
		backgroundColor: "#fff",
		flex: 1
	},
	header: {
		width: width,
		backgroundColor: "#fff",
		justifyContent: "space-between",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33
	},
	headerText: {
		color: "#ffffff",
		fontSize: height / 45.71,
		marginLeft: width / 26.3
	},
	textStyle: {
		color: "#000"
	},
	dateTime: {
		flexDirection: "row",
		paddingHorizontal: width / 27,
		justifyContent: "space-between",
		alignItems: "center",
		height: height / 13,
		borderBottomWidth: 0.3,
		borderBottomColor: "#DFDDE0"
	},
	bar: {
		width: width,
		paddingVertical: height / 35,
		paddingHorizontal: width / 27,
		borderBottomWidth: 0.3,
		borderBottomColor: "#DFDDE0"
	},
	search: {
		width: width / 1.2,
		paddingVertical: width / 30,
		backgroundColor: "#EC6733",
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center"
	},
	cross: {
		width: width / 20,
		height: width / 20,
		borderRadius: width / 40,
		backgroundColor: "#777777",
		justifyContent: "center",
		alignItems: "center"
	},
	primaryHeader: {
		color: "rgba(0,0,0,0.5)",
		fontSize: height / 50,
		width: width / 1.5
	},
	secondaryHeader: {
		color: "rgba(0,0,0,0.5)",
		fontSize: height / 70
	},
	listRow: {
		flexDirection: "row",
		borderBottomWidth: 0.5,
		borderColor: "#000",
		paddingHorizontal: width / 26.3,
		paddingVertical: height / 75
	},
	ratingSection: {
		flexDirection: "row",
		width: width / 1.52,
		height: height / 10,
		justifyContent: "space-between"
	},
	shortlistNumber: {
		position: "absolute",
		top: height / 70,
		right: width / 18,
		width: width / 25,
		height: width / 25,
		borderRadius: width / 50,
		backgroundColor: "#000",
		justifyContent: "center",
		alignItems: "center",
		elevation: 10
	},
	bottomBar: {
		position: "absolute",
		bottom: 0,
		elevation: 5,
		backgroundColor: "#fff",
		width: width,
		height: height / 9,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingHorizontal: width / 26.3,
		borderTopWidth: 1,
		borderTopColor: "#F3F4F2"
	},
	button: {
		width: width / 2.25,
		paddingVertical: width / 30,
		borderColor: "#656765",
		borderWidth: 1,
		borderRadius: 3,
		justifyContent: "center",
		alignItems: "center",
		flexDirection: "row"
	},
	container: { flex: 1 },
	imageCarouselContainer: {
		flex: 1,
		width: width,
		alignItems: "center",
		justifyContent: "center",
		paddingBottom: width / 10
	},
	imageCarouseText: {
		color: "#fff",
		fontSize: height / 60
	}
});

export default styles;
