import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	interestText: {
		marginLeft: width / 40,
		fontSize: height / 65
	},
	interestBar: {
		flexDirection: "row",
		paddingHorizontal: width / 26.3,
		alignItems: "center"
	},
	interestSection: {
		position: "absolute",
		bottom: height / 3.5,
		backgroundColor: "#F3F4F2",
		width: width,
		paddingVertical: width / 40
	},
	mapStyle: {
		height: height,
		width: width
	},
	mapSection: {
		paddingVertical: width / 30,
		backgroundColor: "#fff",
		alignItems: "center",
		justifyContent: "center"
	},
	infoSection: {
		flexDirection: "row",
		justifyContent: "space-between",
		borderBottomWidth: 1,
		borderBottomColor: "#F3F4F2"
	}
});

export default styles;
