import React from "react";
import { View, FlatList, Dimensions } from "react-native";
import MapView, { Marker } from "react-native-maps";
import { connect } from "react-redux";
import { Icon } from "../../../../../Components";
import { CtHotelTextRegular } from "../../../Components";
import styles from "./styles";
import I18n from "../../../Assets/Strings/i18n";
const { width, height } = Dimensions.get("window");

const MapSection = props => {
	return (
		<View style={styles.mapSection}>
			<MapView
				initialRegion={{
					latitude: parseFloat(props.hotel.latitude),
					longitude: parseFloat(props.hotel.longitude),
					latitudeDelta: 0.006,
					longitudeDelta: 0.002
				}}
				showsMyLocationButton={true}
				style={styles.mapStyle}>
				<Marker
					coordinate={{
						latitude: parseFloat(props.hotel.latitude),
						longitude: parseFloat(props.hotel.longitude)
					}}
					title={props.name}
					pinColor={"#EC6733"}
				/>
			</MapView>
			<FlatList
				style={styles.interestSection}
				data={
					props.hotel.hasOwnProperty("points-of-interest") === true
						? props.hotel["points-of-interest"]["point-of-interest"] instanceof
						  Array
							? props.hotel["points-of-interest"]["point-of-interest"]
							: []
						: []
				}
				renderItem={({ item }) => {
					return (
						<View>
							{!(item.distance instanceof Object) && (
								<View style={styles.interestBar}>
									<Icon
										iconType={"ionicon"}
										iconSize={height / 55}
										iconName={
											!(item.type instanceof Object)
												? item.type === "Railway Station"
													? "ios-train"
													: item.type === "Airport"
													? "ios-airplane"
													: "ios-pin"
												: "ios-pin"
										}
										iconColor={"#585A58"}
									/>
									<CtHotelTextRegular style={styles.interestText}>
										{item.distance + "km" + " " + I18n.t("from") + item.name}
									</CtHotelTextRegular>
								</View>
							)}
						</View>
					);
				}}
			/>
		</View>
	);
};
class Location extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<View>
				<MapSection
					hotel={this.props.hotelDetail["other-info"]["location-info"]}
					name={this.props.hotelDetail["basic-info"]["hotel-name"]}
					locality={this.props.hotelDetail["basic-info"].locality}
					address={this.props.hotelDetail["basic-info"].address}
				/>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		stayDate: state.cleartripHotel && state.cleartripHotel.searchHotel.stayDate,
		personalInfo:
			state.cleartripHotel && state.cleartripHotel.searchHotel.personalInfo,
		searchType:
			state.cleartripHotel && state.cleartripHotel.searchHotel.searchType,
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		hotels:
			state.cleartripHotel &&
			state.cleartripHotel.searchHotel &&
			state.cleartripHotel.searchHotel.hotels
				? state.cleartripHotel.searchHotel.hotels
				: null,
		error: state.cleartripHotel && state.cleartripHotel.searchHotel.error,
		shortListHotel:
			state.cleartripHotel && state.cleartripHotel.selectHotel.shortListHotel,
		hotelDetail:
			state.cleartripHotel && state.cleartripHotel.selectHotel.hotelInfo,
		isUpdate: state.cleartripHotel && state.cleartripHotel.selectHotel.isUpdate
	};
}

export default connect(
	mapStateToProps,
	null,
	null
)(Location);
