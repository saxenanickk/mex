import React, { Component } from "react";
import {
	Text,
	View,
	Dimensions,
	StatusBar,
	TouchableOpacity,
	Modal,
	BackHandler
} from "react-native";
import { connect } from "react-redux";
import { Icon, ProgressBar, GoToast } from "../../../../Components";
import { savePersonalInfo, saveStayDate } from "../SearchHotel/Saga";
import {
	saveHotelInfoById,
	getHotelRateInfo,
	getHotelInfoById,
	saveRateError
} from "../SelectHotel/Saga";
import Util, { ONE_DAY_EPOCH_DURATION } from "../../Util";
import I18n from "../../Assets/Strings/i18n";
import {
	CTProgressScreen,
	CtHotelTextRegular,
	CtHotelTextBold
} from "../../Components";
import Navigator from "./Navigator";
import styles from "./styles";
import ImageCarousel from "./ImageCarousel";
import Config from "react-native-config";

const { CLEARTRIP_SERVER_IMAGE_URL } = Config;

const { width, height } = Dimensions.get("window");

class HotelDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			progress: 5,
			modalVisible: false,
			addTraveller: false,
			progressBar: false,
			dateSelected: false,
			showImageCarousel: false
		};

		const { route } = props;
		const { hotelId } = route.params ? route.params : {};

		this.hotelId = hotelId;

		this.getSecondaryHeader = this.getSecondaryHeader.bind(this);
	}

	callGetHotelRateInfo = hotelId => {
		let now = new Date();
		let params = {
			checkInDate: this.props.isAnyTime
				? Util.getDateAsParameter(now)
				: Util.getDateAsParameter(this.props.stayDate.checkInDate),
			checkOutDate: this.props.isAnyTime
				? Util.getDateAsParameter(
						new Date(now.getTime() + ONE_DAY_EPOCH_DURATION)
				  )
				: Util.getDateAsParameter(this.props.stayDate.checkOutDate),
			appToken: this.props.appToken,
			noOfRooms: this.props.personalInfo.length,
			PERSONALINFO: this.props.personalInfo,
			hotelId: hotelId,
			city: this.props.searchType.value.city,
			state: this.props.searchType.value.state,
			country: "IN"
		};
		this.props.dispatch(
			getHotelInfoById({
				isAnyTime: this.props.isAnyTime,
				hotelInfo: {
					hotelId: hotelId,
					appToken: this.props.appToken
				},
				hotelRateInfo: params
			})
		);
	};

	componentDidMount() {
		BackHandler.addEventListener("hardwareBackPress", this.handleBackHandler);
		let self = this;
		this.interval = setInterval(function() {
			if (self.state.progress < 90) {
				self.setState({
					progress: self.state.progress + 3
				});
			}
		}, 1000);
		this.callGetHotelRateInfo(this.hotelId);
	}

	shouldComponentUpdate(props, state) {
		if (
			props.hotelDetail !== null &&
			props.hotelDetail !== this.props.hotelDetail
		) {
			clearInterval(this.interval);
			return true;
		}
		if (
			props.hotelRates !== null &&
			props.hotelRates !== this.props.hotelRates
		) {
			this.setState({ progressBar: false });
		}

		if (props.rateError === true) {
			GoToast.show(
				I18n.t("unable_fetch_rate_message"),
				I18n.t("information"),
				"LONG"
			);
			this.props.dispatch(saveRateError(false));
			this.setState({ progressBar: false });
			this.props.navigation.goBack();
		}
		return true;
	}

	handleBackHandler = () => {
		if (this.state.showImageCarousel) {
			this.setState({ showImageCarousel: false });
			return true;
		}
		return false;
	};

	getSecondaryHeader = () => {
		let checkInDate = this.props.stayDate.checkInDate;
		let checkOutDate = this.props.stayDate.checkOutDate;
		let rooms =
			this.props.personalInfo.length > 1
				? this.props.personalInfo.length + " rooms"
				: this.props.personalInfo.length + " room";
		let totalCount = 0;
		for (let item of this.props.personalInfo) {
			totalCount += item.adult + item.child.length;
		}
		let date =
			checkInDate.getDate() +
			" " +
			Util.getMonthName(checkInDate.getMonth()) +
			" - " +
			checkOutDate.getDate() +
			" " +
			Util.getMonthName(checkOutDate.getMonth());
		return date + ", " + totalCount + " guests, " + rooms;
	};

	onDateSelected = (checkIn, checkOut) => {
		this.props.dispatch(
			saveStayDate({
				checkInDate: checkIn,
				checkOutDate: checkOut
			})
		);
		this.setState({ dateSelected: true });
		this.setState({ modalVisible: true });
	};

	getTravellerToDisplay = () => {
		let detail = "";
		let adultCount = 0;
		let childCount = 0;
		this.props.personalInfo.map(item => {
			(adultCount += item.adult), (childCount += item.child.length);
		});
		adultCount > 1
			? (detail += adultCount + " adults")
			: (detail += adultCount + " adult");
		childCount > 0
			? childCount > 1
				? (detail += "," + childCount + " children ")
				: (detail += "," + childCount + " child ")
			: null;
		return detail;
	};

	getDateToDisplay = date => {
		let dateReturn = date.toDateString().split(" ");
		let dateToDisplay =
			dateReturn[0] + ", " + dateReturn[2] + " " + dateReturn[1];
		return dateToDisplay;
	};

	callGetHotelInfo = () => {
		let now = new Date();
		let params = {
			checkInDate: Util.getDateAsParameter(this.props.stayDate.checkInDate),
			checkOutDate: Util.getDateAsParameter(this.props.stayDate.checkOutDate),
			appToken: this.props.appToken,
			noOfRooms: this.props.personalInfo.length,
			PERSONALINFO: this.props.personalInfo,
			hotelId: this.hotelId,
			city: this.props.searchType.value.city,
			state: this.props.searchType.value.state,
			country: "IN"
		};
		this.setState({ modalVisible: false, progressBar: true });
		this.props.dispatch(getHotelRateInfo(params));
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.handleBackHandler
		);
	};
	componentWillUnmount() {
		clearInterval(this.interval);
		this.props.dispatch(
			saveHotelInfoById({
				hotelInfo: null,
				hotelRateInfo: null
			})
		);
	}

	closeModal = () => {
		this.setState({ addTraveller: false, modalVisible: true });
	};

	saveTravellerDetail = personalInfo => {
		this.props.dispatch(savePersonalInfo(personalInfo));
		this.setState({ addTraveller: false, modalVisible: true });
	};

	getButtonText = keyword => {
		return I18n.currentLocale() === "en-US"
			? I18n.t(keyword).toUpperCase()
			: I18n.t(keyword);
	};
	render() {
		return (
			<View style={styles.container}>
				<StatusBar backgroundColor="#1f3d7a" barStyle="light-content" />
				{this.state.progressBar && (
					<CTProgressScreen message={"checking availibilty"} />
				)}
				<View style={styles.header}>
					<View style={styles.backArrowButtonContainer}>
						<TouchableOpacity
							onPress={() => {
								if (this.state.showImageCarousel) {
									this.setState({ showImageCarousel: false });
								} else {
									this.props.navigation.goBack();
								}
							}}>
							<View style={styles.backArrowContainer}>
								<Icon
									iconType={"material"}
									iconSize={height / 26}
									iconName={"arrow-back"}
									iconColor={"rgba(0,0,0,0.5)"}
								/>
							</View>
						</TouchableOpacity>
						<View>
							<View style={styles.primaryHeaderContainer}>
								<CtHotelTextBold style={styles.primaryHeader} numberOfLines={1}>
									{this.props.hotelDetail &&
										this.props.hotelDetail["basic-info"]["hotel-name"]}
								</CtHotelTextBold>
							</View>
							<View style={styles.secondaryHeaderContainer}>
								<CtHotelTextRegular style={styles.secondaryHeader}>
									{this.props.hotelDetail && this.props.searchType.value.city}
								</CtHotelTextRegular>
							</View>
						</View>
					</View>
					<TouchableOpacity
						style={styles.heartButton}
						onPress={() => {
							if (this.props.shortListHotel.length > 0) {
								this.props.navigation.navigate("ShortListHotel");
							}
						}}>
						{this.props.shortListHotel.length === 0 ? (
							<Icon
								iconType={"icomoon"}
								iconSize={height / 26}
								iconName={"ios-heart-empty"}
								iconColor={"#rgba(0,0,0,0.5)"}
							/>
						) : (
							<Icon
								iconType={"ionicon"}
								iconSize={height / 26}
								iconName={"ios-heart"}
								iconColor={"#f44336"}
							/>
						)}
					</TouchableOpacity>
					{/* number of shortlisted hotels at absolute position */}
					{this.props.shortListHotel.length > 0 && (
						<View style={styles.shortlistNumber}>
							<CtHotelTextRegular style={styles.shortListHotelsLengthText}>
								{this.props.shortListHotel.length}
							</CtHotelTextRegular>
						</View>
					)}
				</View>
				{/* body section*/}
				<View style={styles.bodyStyle}>
					{this.props.hotelDetail === null ? (
						<ProgressBar
							width={width}
							height={height / 200}
							value={this.state.progress}
							backgroundColor="#EC6733"
							barEasing={"linear"}
						/>
					) : (
						<View style={styles.container}>
							<Navigator
								screenProps={{
									onImagePress: () => this.setState({ showImageCarousel: true })
								}}
							/>
							<View style={styles.bottomBar}>
								<TouchableOpacity style={styles.button}>
									<CtHotelTextRegular style={styles.shortListText}>
										{this.props.shortListHotel.filter(
											obj =>
												obj["hotel-id"] === this.props.hotelDetail["hotel-id"]
										).length === 1
											? this.getButtonText("shortlisted")
											: this.getButtonText("shortlist_searchHotel_C")}
									</CtHotelTextRegular>
								</TouchableOpacity>
								<TouchableOpacity
									onPress={() => {
										if (this.props.hotelRates === null) {
											this.props.navigation.goBack();
											// To Do Handle this case
											// this.setState({ modalVisible: true })
										} else {
											this.props.navigation.navigate("SelectRoom");
										}
									}}
									style={[
										styles.button,
										{
											backgroundColor:
												this.props.hotelRates !== null
													? "#EC6733"
													: "rgba(0,0,0,0.6)",
											borderWidth: 0
										}
									]}>
									<CtHotelTextRegular style={styles.selectRoomText}>
										{this.props.hotelRates !== null
											? this.getButtonText("select_room_C")
											: this.getButtonText("check_availability")}
									</CtHotelTextRegular>
								</TouchableOpacity>
							</View>
						</View>
					)}
				</View>
				<Modal
					visible={this.state.modalVisible}
					transparent={true}
					onRequestClose={() => this.setState({ modalVisible: false })}>
					<TouchableOpacity
						style={styles.modalStyle}
						onPress={() => this.setState({ modalVisible: false })}>
						<View style={styles.modalBar}>
							<View style={styles.bottomBarRow}>
								<CtHotelTextBold style={styles.pickDatesAndRoomsText}>
									{I18n.t("pick_dates_and_rooms")}
								</CtHotelTextBold>
								<TouchableOpacity
									onPress={() => {
										if (this.state.dateSelected === false) {
											GoToast.show(
												I18n.t("select_check_in_date"),
												I18n.t("information")
											);
										} else {
											this.callGetHotelInfo();
										}
									}}>
									<CtHotelTextBold style={styles.doneText}>
										{I18n.t("done")}
									</CtHotelTextBold>
								</TouchableOpacity>
							</View>
							<TouchableOpacity
								onPress={() => {
									this.setState({ modalVisible: false });
									Util.openCheckInDatePicker(this.onDateSelected);
								}}
								style={styles.bottomBarRow}>
								<View>
									<CtHotelTextRegular style={styles.datemsg}>
										{I18n.t("check_in")}
									</CtHotelTextRegular>
									<CtHotelTextBold style={styles.dot}>
										{this.state.dateSelected === false
											? "..."
											: this.getDateToDisplay(this.props.stayDate.checkInDate)}
									</CtHotelTextBold>
								</View>
								<View>
									<CtHotelTextRegular style={styles.datemsg}>
										{I18n.t("check_out")}
									</CtHotelTextRegular>
									<CtHotelTextBold
										style={[styles.dot, { alignSelf: "flex-end" }]}>
										{this.state.dateSelected === false
											? "..."
											: this.getDateToDisplay(this.props.stayDate.checkOutDate)}
									</CtHotelTextBold>
								</View>
							</TouchableOpacity>
							<TouchableOpacity
								onPress={() =>
									this.setState({ modalVisible: false, addTraveller: true })
								}
								style={[styles.bottomBarRow, { flexDirection: "column" }]}>
								<CtHotelTextRegular
									style={[styles.datemsg, { fontSize: height / 52 }]}>
									{this.props.personalInfo.length + I18n.t("room_C")}
								</CtHotelTextRegular>
								<CtHotelTextRegular style={styles.travellerText}>
									{this.getTravellerToDisplay()}
								</CtHotelTextRegular>
							</TouchableOpacity>
						</View>
					</TouchableOpacity>
				</Modal>
				{this.state.addTraveller &&
					Util.displayTraveller(
						this.closeModal,
						this.saveTravellerDetail,
						this.state.addTraveller
					)}
				{this.state.showImageCarousel ? (
					<ImageCarousel
						images={this.props.hotelDetail["other-info"]["image-info"].image}
						baseUrl={CLEARTRIP_SERVER_IMAGE_URL}
						imageAttribute={"wide-angle-image-url"}
						onClosePress={() => this.setState({ showImageCarousel: false })}
					/>
				) : null}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		stayDate: state.cleartripHotel && state.cleartripHotel.searchHotel.stayDate,
		isAnyTime:
			state.cleartripHotel && state.cleartripHotel.searchHotel.isAnyTime,
		personalInfo:
			state.cleartripHotel && state.cleartripHotel.searchHotel.personalInfo,
		searchType:
			state.cleartripHotel && state.cleartripHotel.searchHotel.searchType,
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		hotels:
			state.cleartripHotel &&
			state.cleartripHotel.searchHotel &&
			state.cleartripHotel.searchHotel.hotels
				? state.cleartripHotel.searchHotel.hotels
				: null,
		error: state.cleartripHotel && state.cleartripHotel.searchHotel.error,
		shortListHotel:
			state.cleartripHotel && state.cleartripHotel.selectHotel.shortListHotel,
		hotelDetail:
			state.cleartripHotel && state.cleartripHotel.selectHotel.hotelInfo,
		hotelRates:
			state.cleartripHotel &&
			state.cleartripHotel.selectHotel.hotelRateInfo &&
			state.cleartripHotel.selectHotel.hotelRateInfo.hotels &&
			state.cleartripHotel.selectHotel.hotelRateInfo.hotels.hotel,
		rateError:
			state.cleartripHotel && state.cleartripHotel.selectHotel.rateError
	};
}

export default connect(
	mapStateToProps,
	null,
	null
)(HotelDetail);
