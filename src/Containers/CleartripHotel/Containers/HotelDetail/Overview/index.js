import React from "react";
import {
	View,
	FlatList,
	ScrollView,
	Dimensions,
	Modal,
	TouchableOpacity,
	Image
} from "react-native";
import styles from "./styles";
import { CtHotelTextBold, CtHotelTextRegular } from "../../../Components";
import MapView, { Marker } from "react-native-maps";
import { connect } from "react-redux";
import { ImageCart } from "../../../Components/ImageCart";
import { Icon } from "../../../../../Components";
import StarReview from "../../../Components/StarReview";
import TripRating from "../../../Components/TripRating";
import I18n from "../../../Assets/Strings/i18n";
import {
	OFFER,
	WIFI,
	POOL,
	AC,
	ROOMSERVICE,
	COFEESHOP,
	BAR,
	GYM
} from "../../../Assets/Img/Image";
const { width, height } = Dimensions.get("window");

const MapSection = props => {
	return (
		<View style={styles.mapSection}>
			<CtHotelTextBold style={styles.localityTextStyle}>
				{props.locality}
			</CtHotelTextBold>
			<CtHotelTextRegular style={styles.addressText}>
				{props.address}
			</CtHotelTextRegular>
			<MapView
				initialRegion={{
					latitude: parseFloat(props.hotel.latitude),
					longitude: parseFloat(props.hotel.longitude),
					latitudeDelta: 0.006,
					longitudeDelta: 0.002
				}}
				showsMyLocationButton={true}
				style={styles.mapView}>
				<Marker
					coordinate={{
						latitude: parseFloat(props.hotel.latitude),
						longitude: parseFloat(props.hotel.longitude)
					}}
					title={props.name}
					pinColor={"#EC6733"}
				/>
			</MapView>
			<FlatList
				style={styles.flatList}
				data={
					props.hotel.hasOwnProperty("points-of-interest") === true
						? props.hotel["points-of-interest"]["point-of-interest"] instanceof
						  Array
							? props.hotel["points-of-interest"]["point-of-interest"]
							: []
						: []
				}
				renderItem={({ item }) => {
					return (
						<View>
							{!(item.distance instanceof Object) && (
								<View style={styles.hotelItemRow}>
									<Icon
										iconType={"ionicon"}
										iconSize={height / 55}
										iconName={
											!(item.type instanceof Object)
												? item.type === "Railway Station"
													? "ios-train"
													: item.type === "Airport"
													? "ios-airplane"
													: "ios-pin"
												: "ios-pin"
										}
										iconColor={"#585A58"}
									/>
									<CtHotelTextRegular style={styles.hotelItemName}>
										{item.distance + "km" + " " + I18n.t("from") + item.name}
									</CtHotelTextRegular>
								</View>
							)}
						</View>
					);
				}}
			/>
			<TouchableOpacity onPress={props.navigateToLocation}>
				<CtHotelTextBold style={styles.fullMapText}>
					{I18n.t("view_full_map")}
				</CtHotelTextBold>
			</TouchableOpacity>
		</View>
	);
};
class Overview extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			modalVisible: false,
			showMore: this.isShowMore(),
			amenityModal: false
		};
		this.importantAmenity = [
			{ type: "Internet", name: "Wifi", image: WIFI },
			{ type: "Swimming Pool", name: "Pool", image: POOL },
			{ type: "Air Conditioning", name: "AC", image: AC },
			{ type: "Room Service", name: "Room service", image: ROOMSERVICE },
			{ type: "Bar", name: "Bar", image: BAR },
			{ type: "Coffee Shop", name: "Cofee shop", image: COFEESHOP },
			{ type: "Gym", name: "Gym", image: GYM }
		];
	}

	componentDidMount() {
		console.log("props are", this.props);
	}
	onCloseModal = () => {
		this.setState({ modalVisible: false });
	};

	navigateToLocation = () => {
		this.props.navigation.navigate("Location");
	};
	/**
	 * function to find all important points about a hotel
	 */
	isPoints = item => {
		let ar = [];
		if (
			item.hasOwnProperty("what-we-like") &&
			item["what-we-like"].hasOwnProperty("points")
		) {
			if (item["what-we-like"].points.point instanceof Array) {
				return item["what-we-like"].points.point;
			} else {
				ar.push(item["what-we-like"].points.point);
				return ar;
			}
		}
		return ar;
	};
	/**
	 * function to find all offers provided by a hotel
	 */
	isOffers = item => {
		let ar = [];
		if (
			item &&
			item.hasOwnProperty("promo-offers") &&
			item["promo-offers"].hasOwnProperty("promo-offer")
		) {
			if (item["promo-offers"]["promo-offer"] instanceof Array) {
				return item["promo-offers"]["promo-offer"];
			} else {
				ar.push(item["promo-offers"]["promo-offer"]);
				return ar;
			}
		}
		return ar;
	};

	/**
	 * funtion to find out whether to show show more option or not
	 */
	isShowMore = () => {
		let point = this.isPoints(this.props.hotelDetail["other-info"]);
		let offer = this.isOffers(this.props.hotelRates);
		return point.length + offer.length > 2;
	};
	shouldComponentUpdate(props, state) {
		console.log("props in overview", props);
		return true;
	}

	getTripRating = hotelDetail => {
		if (
			hotelDetail["basic-info"]["hotel-ratings"]["hotel-rating"] instanceof
			Array
		) {
			return true;
		} else {
			return false;
		}
	};

	calculateWebViewHeight = () => {
		let length = this.props.hotelDetail["other-info"].description.length;
		let fontSize = height / 70;
		let numOfCharacterInLine = width / fontSize;
		let totalLine = length / numOfCharacterInLine;
		console.log("total line is", totalLine);
		console.log("number of character in one line", numOfCharacterInLine);
		return (height / 38) * totalLine;
	};

	getFamousAmenities = amenities => {
		let ar = [];
		if (amenities !== null) {
			amenities.map(amenity => {
				let index = this.importantAmenity.findIndex(
					row => row.type === amenity
				);
				if (index >= 0) {
					ar.push(this.importantAmenity[index]);
				}
			});
		}
		return ar;
	};

	renderMore = () => {
		let important = this.getFamousAmenities(this.props.hotelDetail.amenity);
		let leftAmenity = this.props.hotelDetail.amenity.length - important.length;
		if (leftAmenity > 0) {
			return (
				<TouchableOpacity
					style={styles.amenityInfoSection}
					onPress={() =>
						this.setState({ modalVisible: true, amenityModal: true })
					}>
					<View style={[styles.amenityIconSection, { height: height / 20 }]}>
						<Icon
							iconType={"ionicon"}
							iconSize={width / 15}
							iconName={"ios-add"}
							iconColor={"#3366cc"}
						/>
						<CtHotelTextBold
							style={[
								styles.amenityMoreTextSection,
								{ marginLeft: width / 60 }
							]}>
							{leftAmenity}
						</CtHotelTextBold>
					</View>
					<CtHotelTextBold style={styles.amenityMoreTextSection}>
						{I18n.t("more")}
					</CtHotelTextBold>
				</TouchableOpacity>
			);
		}
	};
	render() {
		return (
			<ScrollView style={styles.scrollView}>
				<ImageCart
					image={this.props.hotelDetail["other-info"]["image-info"].image}
					onImageSelect={this.props.screenProps.onImagePress}
				/>
				<View style={styles.rating}>
					<View style={styles.ratingSection}>
						<StarReview
							rating={this.props.hotelDetail["basic-info"]["star-rating"]}
						/>
						<CtHotelTextRegular style={styles.ratingText}>
							{this.props.hotelDetail["basic-info"]["star-rating"] +
								" " +
								I18n.t("star_hotel")}
						</CtHotelTextRegular>
					</View>
					<View style={styles.ratingSection}>
						<TripRating
							rating={
								this.getTripRating(this.props.hotelDetail)
									? this.props.hotelDetail["basic-info"]["hotel-ratings"][
											"hotel-rating"
									  ][1].rating
									: 0
							}
						/>
						<CtHotelTextRegular style={styles.ratingText}>
							{this.getTripRating(this.props.hotelDetail)
								? this.props.hotelDetail["basic-info"]["hotel-ratings"][
										"hotel-rating"
								  ][1]["total-ratings"] +
								  " " +
								  I18n.t("reviews")
								: "no reviews"}
						</CtHotelTextRegular>
					</View>
				</View>
				<View
					style={{
						height: this.state.showMore ? height / 6 : null
					}}>
					{this.isOffers(this.props.hotelRates).map((item, index) => {
						return (
							<View style={styles.pointStyle} key={index}>
								<Image
									resizeMode={"contain"}
									source={OFFER}
									style={styles.offerImage}
								/>
								<CtHotelTextRegular style={styles.pointText}>
									{item.desc}
								</CtHotelTextRegular>
							</View>
						);
					})}
					{this.isPoints(this.props.hotelDetail["other-info"]).map(
						(item, index) => {
							return (
								<View style={styles.pointStyle} key={index}>
									<Icon
										iconType={"ionicon"}
										iconSize={width / 12}
										iconName={"ios-checkmark-circle-outline"}
										iconColor={"#5fa600"}
									/>
									<CtHotelTextRegular style={styles.pointText}>
										{item}
									</CtHotelTextRegular>
								</View>
							);
						}
					)}
				</View>
				{this.state.showMore && (
					<TouchableOpacity
						style={[
							styles.pointStyle,
							{ justifyContent: "center", alignItems: "center" }
						]}
						onPress={() => this.setState({ showMore: false })}>
						<CtHotelTextBold style={styles.showMoreText}>
							{I18n.t("show_more")}
						</CtHotelTextBold>
					</TouchableOpacity>
				)}
				<View style={styles.separaterSection}>
					<CtHotelTextBold style={styles.separaterText}>
						{I18n.t("location")}
					</CtHotelTextBold>
				</View>
				<MapSection
					hotel={this.props.hotelDetail["other-info"]["location-info"]}
					name={this.props.hotelDetail["basic-info"]["hotel-name"]}
					locality={this.props.hotelDetail["basic-info"].locality}
					address={this.props.hotelDetail["basic-info"].address}
					navigateToLocation={this.navigateToLocation}
				/>
				<View style={styles.separaterSection}>
					<CtHotelTextBold style={styles.separaterText}>
						{I18n.t("amenities_and_information")}
					</CtHotelTextBold>
				</View>
				<View style={styles.amenityBar}>
					{this.getFamousAmenities(this.props.hotelDetail.amenity).map(
						(item, index) => {
							if (index < 4) {
								return (
									<View style={styles.amenityInfoSection} key={index}>
										<View style={styles.amenityIconSection}>
											<Image
												resizeMode={"contain"}
												source={item.image}
												style={styles.offerImage}
											/>
										</View>
										<CtHotelTextRegular style={styles.amenityTextSection}>
											{item.name}
										</CtHotelTextRegular>
									</View>
								);
							}
						}
					)}
					{this.renderMore()}
				</View>
				<View style={styles.infoSection}>
					<View style={styles.subInfoSection}>
						<CtHotelTextRegular style={styles.primaryInfo}>
							{this.props.hotelDetail["other-info"]["number-of-floors"]}
						</CtHotelTextRegular>
						<CtHotelTextRegular style={styles.secondaryInfo}>
							{I18n.t("floors")}
						</CtHotelTextRegular>
					</View>
					<View style={styles.subInfoSection}>
						<CtHotelTextRegular style={styles.primaryInfo}>
							{this.props.hotelDetail["other-info"]["number-of-rooms"]}
						</CtHotelTextRegular>
						<CtHotelTextRegular style={styles.secondaryInfo}>
							{I18n.t("room", {
								count: this.props.hotelDetail["other-info"]["number-of-rooms"]
							})}
						</CtHotelTextRegular>
					</View>
					<View style={styles.subInfoSection}>
						<CtHotelTextBold style={[styles.primaryInfo]}>
							{this.props.hotelDetail["other-info"]["policy-info"][
								"check-in-time"
							] /
								100 +
								":00"}
						</CtHotelTextBold>
						<CtHotelTextRegular style={styles.secondaryInfo}>
							{I18n.t("check_in")}
						</CtHotelTextRegular>
					</View>
					<View style={styles.subInfoSection}>
						<CtHotelTextBold style={[styles.primaryInfo]}>
							{this.props.hotelDetail["other-info"]["policy-info"][
								"check-out-time"
							] /
								100 +
								":00"}
						</CtHotelTextBold>
						<CtHotelTextRegular style={styles.secondaryInfo}>
							{I18n.t("check_out")}
						</CtHotelTextRegular>
					</View>
				</View>
				<View style={styles.descriptionSection}>
					<CtHotelTextRegular style={styles.description}>
						{this.props.hotelDetail["other-info"].description instanceof Object
							? null
							: this.props.hotelDetail["other-info"].description}
					</CtHotelTextRegular>
				</View>
				<Modal
					animationType={"slide"}
					onRequestClose={() =>
						this.setState({
							amenityModal: false,
							modalVisible: false
						})
					}
					transparent={this.state.amenityModal}
					visible={this.state.modalVisible && this.state.amenityModal}>
					<View style={styles.amenityContainer}>
						{this.props.hotelDetail.amenity.length > 0 && (
							<View style={styles.amenitySection}>
								<FlatList
									data={this.props.hotelDetail.amenity}
									renderItem={({ item, index }) => {
										return (
											<View key={index} style={styles.amenityList}>
												<Icon
													iconType={"ionicon"}
													iconSize={width / 17}
													iconName={"ios-checkmark-circle-outline"}
													iconColor={"#000"}
												/>
												<CtHotelTextRegular style={styles.amenityText}>
													{item}
												</CtHotelTextRegular>
											</View>
										);
									}}
								/>
								<TouchableOpacity
									onPress={() =>
										this.setState({
											amenityModal: false,
											modalVisible: false
										})
									}
									style={styles.closeButton}>
									<CtHotelTextBold style={styles.closeText}>
										{I18n.t("close_C")}
									</CtHotelTextBold>
								</TouchableOpacity>
							</View>
						)}
					</View>
				</Modal>
			</ScrollView>
		);
	}
}

function mapStateToProps(state) {
	return {
		stayDate: state.cleartripHotel && state.cleartripHotel.searchHotel.stayDate,
		personalInfo:
			state.cleartripHotel && state.cleartripHotel.searchHotel.personalInfo,
		searchType:
			state.cleartripHotel && state.cleartripHotel.searchHotel.searchType,
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		hotels:
			state.cleartripHotel &&
			state.cleartripHotel.searchHotel &&
			state.cleartripHotel.searchHotel.hotels
				? state.cleartripHotel.searchHotel.hotels
				: null,
		error: state.cleartripHotel && state.cleartripHotel.searchHotel.error,
		shortListHotel:
			state.cleartripHotel && state.cleartripHotel.selectHotel.shortListHotel,
		hotelDetail:
			state.cleartripHotel && state.cleartripHotel.selectHotel.hotelInfo,
		hotelRates:
			state.cleartripHotel &&
			state.cleartripHotel.selectHotel.hotelRateInfo &&
			state.cleartripHotel.selectHotel.hotelRateInfo.hotels &&
			state.cleartripHotel.selectHotel.hotelRateInfo.hotels.hotel,
		isUpdate: state.cleartripHotel && state.cleartripHotel.selectHotel.isUpdate
	};
}

export default connect(
	mapStateToProps,
	null,
	null
)(Overview);
