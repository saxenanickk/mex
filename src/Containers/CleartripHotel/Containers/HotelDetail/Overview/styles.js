import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	localityTextStyle: {
		fontSize: height / 55,
		color: "#000"
	},
	mapView: {
		height: height / 4,
		width: width
	},
	hotelItemRow: {
		flexDirection: "row",
		paddingHorizontal: width / 26.3,
		alignItems: "center"
	},
	hotelItemName: {
		marginLeft: width / 40,
		fontSize: height / 65
	},
	flatList: {
		backgroundColor: "#F3F4F2",
		width: width,
		paddingVertical: width / 40
	},
	scrollView: { flex: 1, marginBottom: height / 10.5 },
	addressText: {
		fontSize: height / 60,
		marginBottom: width / 30,
		alignSelf: "center",
		paddingHorizontal: width / 26.3
	},
	showMoreText: { color: "#3366cc" },
	imageContainer: { flex: 1, backgroundColor: "#000" },
	backArrowContainer: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	fullMapText: {
		marginTop: width / 30,
		fontSize: height / 55,
		color: "#3366cc"
	},
	offerImage: {
		width: width / 20,
		height: width / 20
	},
	hotelHeader: {
		flexDirection: "row",
		paddingHorizontal: width / 26.3,
		backgroundColor: "#000"
	},
	hotelHeaderAddressContainer: { justifyContent: "center" },
	primaryHeader: {
		color: "#fff",
		fontSize: height / 50,
		width: width / 1.5
	},
	secondaryHeader: {
		color: "#fff",
		fontSize: height / 70
	},
	rating: {
		flexDirection: "row",
		justifyContent: "space-between",
		borderBottomWidth: 1,
		borderBottomColor: "#F3F4F2"
	},
	ratingText: {
		fontSize: height / 60
	},
	ratingSection: {
		width: width / 2,
		paddingVertical: height / 50,
		justifyContent: "center",
		alignItems: "center"
	},
	pointStyle: {
		flexDirection: "row",
		paddingHorizontal: width / 26.3,
		paddingVertical: width / 30,
		alignItems: "center",
		borderBottomWidth: 1,
		borderBottomColor: "#F3F4F2"
	},
	pointText: {
		fontSize: height / 50,
		width: width / 1.5,
		marginLeft: width / 30
	},
	circle: {
		width: width / 15,
		height: width / 15,
		borderRadius: width / 30,
		borderWidth: width / 200,
		borderColor: "#5fa600",
		justifyContent: "center",
		alignItems: "center"
	},
	separaterSection: {
		width: width,
		height: height / 14,
		backgroundColor: "#F3F4F2",
		paddingHorizontal: width / 25,
		justifyContent: "center"
	},
	separaterText: {
		fontSize: height / 55
	},
	mapSection: {
		paddingVertical: width / 30,
		backgroundColor: "#fff",
		alignItems: "center",
		justifyContent: "center"
	},
	interestSection: {
		width: width,
		paddingVertical: width / 40,
		backgroundColor: "#F3F4F2"
	},
	infoSection: {
		flexDirection: "row",
		justifyContent: "space-between",
		borderBottomWidth: 1,
		borderBottomColor: "#F3F4F2"
	},
	subInfoSection: {
		width: width / 4,
		justifyContent: "center",
		alignItems: "center",
		paddingVertical: width / 20
	},
	primaryInfo: {
		fontSize: height / 40,
		color: "#000"
	},
	secondaryInfo: {
		fontSize: height / 50
	},
	descriptionSection: {
		paddingHorizontal: width / 26.3,
		paddingVertical: width / 30,
		alignItems: "center"
	},
	description: {
		fontSize: height / 50,
		color: "rgba(0,0,0,0.5)",
		lineHeight: width / 20
	},
	amenityInfoSection: {
		width: width / 5,
		paddingVertical: height / 50,
		alignItems: "center"
	},
	amenityTextSection: {
		fontSize: height / 50,
		textAlign: "center",
		color: "rgba(0,0,0,0.5)"
	},
	amenityIconSection: {
		height: height / 18,
		justifyContent: "center",
		alignItems: "center",
		flexDirection: "row"
	},
	amenityBar: {
		flexDirection: "row"
	},
	amenityMoreTextSection: {
		fontSize: height / 40,
		textAlign: "center",
		color: "#3366cc"
	},
	amenitySection: {
		width: width / 1.2,
		paddingVertical: height / 50,
		justifyContent: "center",
		backgroundColor: "#fff",
		borderRadius: width / 30,
		height: height / 1.5
	},
	amenityList: {
		flexDirection: "row",
		paddingHorizontal: width / 30,
		paddingVertical: height / 100,
		alignItems: "center"
	},
	amenityText: {
		fontSize: height / 55,
		color: "#000",
		marginLeft: width / 30
	},
	closeButton: {
		alignSelf: "center"
	},
	closeText: {
		alignSelf: "center",
		color: "#3366cc"
	},
	amenityContainer: {
		flex: 1,
		backgroundColor: "rgba(0,0,0,0.5)",
		justifyContent: "center",
		alignItems: "center"
	}
});

export default styles;
