import React from "react";
import { Dimensions } from "react-native";
import { fontFamily_semi_bold } from "../../../../Assets/styles";
import Overview from "./Overview";
import Location from "./Location";
import { TopTab } from "../../../../utils/Navigators";

const { width, height } = Dimensions.get("window");

const Navigator = () => (
	<TopTab.Navigator
		tabBarOptions={{
			labelStyle: {
				fontSize: height / 46,
				color: "#3366cc",
				fontFamily: fontFamily_semi_bold
			},
			tabStyle: {
				width: width / 2
			},
			style: {
				backgroundColor: "#fff",
				elevation: 5
			},
			indicatorStyle: {
				backgroundColor: "#3366cc",
				height: height / 250
			}
		}}>
		<TopTab.Screen name={"Overview"} component={Overview} />
		<TopTab.Screen name={"Location"} component={Location} />
	</TopTab.Navigator>
);

export default Navigator;
