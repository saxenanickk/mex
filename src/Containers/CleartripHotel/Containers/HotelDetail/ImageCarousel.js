import React, { Component } from "react";
import {
	Text,
	View,
	ActivityIndicator,
	Dimensions,
	Platform,
	TouchableOpacity
} from "react-native";
import ImageViewer from "react-native-image-zoom-viewer";
import { CtHotelTextBold } from "../../Components";
import { Icon } from "../../../../Components";
import styles from "./styles";

const { width, height } = Dimensions.get("window");

export default class ImageCarousel extends Component {
	state = {
		images: []
	};

	buildImageArray = () => {
		let images = this.props.images.map(image => ({
			url: `${this.props.baseUrl}${image[this.props.imageAttribute]}`,
			category: image["image-category"]
		}));
		return images;
	};

	static getDerivedStateFromProps(nextProps, prevState) {
		let nextState = {};
		if (nextProps.images.length !== prevState.images.length) {
			nextState = {
				...nextState,
				images: [
					...nextProps.images.map(image => ({
						url: `${nextProps.baseUrl}${image[nextProps.imageAttribute]}`,
						category: image["image-category"]
					}))
				]
			};
		}
		return nextState;
	}

	_renderFooter = currentIndex => {
		if (currentIndex === -1) {
			return (
				<View style={styles.imageCarouselContainer}>
					<CtHotelTextBold style={styles.imageCarouseText}>
						{this.state.images[0].category}
					</CtHotelTextBold>
					<CtHotelTextBold style={styles.imageCarouseText}>
						{`1 of ${this.state.images.length}`}
					</CtHotelTextBold>
				</View>
			);
		}
		return (
			<View style={styles.imageCarouselContainer}>
				<CtHotelTextBold style={styles.imageCarouseText}>
					{this.state.images[currentIndex].category}
				</CtHotelTextBold>
				<CtHotelTextBold style={styles.imageCarouseText}>
					{`${currentIndex + 1} of ${this.state.images.length}`}
				</CtHotelTextBold>
			</View>
		);
	};

	render() {
		return (
			<View
				style={{ position: "absolute", top: 0, bottom: 0, left: 0, right: 0 }}>
				<ImageViewer
					imageUrls={this.state.images}
					loadingRender={() => <ActivityIndicator size={"large"} />}
					renderIndicator={() => null}
					renderFooter={this._renderFooter}
					renderArrowLeft={() => (
						<Icon
							iconName={"ios-arrow-dropleft"}
							iconType={"icomoon"}
							iconSize={width / 12}
							iconColor={"#fff"}
						/>
					)}
					renderArrowRight={() => (
						<Icon
							iconName={"ios-arrow-dropright"}
							iconType={"icomoon"}
							iconSize={width / 12}
							iconColor={"#fff"}
						/>
					)}
					saveToLocalByLongPress={false}
				/>
				<TouchableOpacity
					onPress={this.props.onClosePress}
					style={{
						position: "absolute",
						backgroundColor: "#000",
						justifyContent: "flex-end",
						alignItems: "flex-end",
						padding: width / 30,
						top: width / 40,
						right: width / 40
					}}>
					<Icon
						iconType={"ionicon"}
						iconSize={height / 26}
						iconName={Platform.OS === "android" ? "md-close" : "ios-close"}
						iconColor={"#fff"}
					/>
				</TouchableOpacity>
			</View>
		);
	}
}
