import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api/";
import { saveServerError } from "../SearchHotel/Saga";
export const CTHOTEL_GET_ROOM_INFO = "CTHOTEL_GET_ROOM_INFO";
export const CTHOTEL_SAVE_ROOM_INFO = "CTHOTEL_SAVE_ROOM_INFO";

export const saveRoomInfo = payload => ({
	type: CTHOTEL_SAVE_ROOM_INFO,
	payload
});

export const getRoomInfo = payload => ({
	type: CTHOTEL_GET_ROOM_INFO,
	payload
});

export function* hotelDetailSaga(dispatch) {
	yield takeEvery(CTHOTEL_GET_ROOM_INFO, handleGetRoomInfo);
}

function* handleGetRoomInfo(action) {
	try {
		let response = yield call(Api.getRoomInfoOfHotel, action.payload);
		if (response.hasOwnProperty("hotel-search-response")) {
			yield put(saveRoomInfo(response["hotel-search-response"].hotels.hotel));
		} else {
			yield put(saveRoomInfo(response));
		}
	} catch (error) {
		console.log("error in room info:", error);
		if (error) {
			yield put(
				saveServerError({
					error: true,
					message: "unable to connect the server, try after sometime"
				})
			);
		}
	}
}
