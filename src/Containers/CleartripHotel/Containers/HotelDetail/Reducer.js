import { CTHOTEL_SAVE_ROOM_INFO } from "./Saga";
const initialState = {
	rooms: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case CTHOTEL_SAVE_ROOM_INFO:
			return {
				...state,
				rooms: action.payload
			};
		default:
			return state;
	}
};
