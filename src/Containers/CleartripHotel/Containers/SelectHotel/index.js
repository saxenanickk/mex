/* eslint-disable radix */
import React, { Component } from "react";
import {
	View,
	Image,
	Dimensions,
	StatusBar,
	TouchableOpacity,
	Modal,
	ImageBackground,
	NativeModules,
	TextInput,
	Platform
} from "react-native";
import { connect } from "react-redux";
import { Icon, ProgressBar } from "../../../../Components";
import {
	saveSearchHotel,
	saveServerError,
	noHotelError,
	filterHotel,
	getSearchHotel
} from "../SearchHotel/Saga";
import {
	StarReview,
	CtHotelTextRegular,
	CtHotelTextMedium,
	CtHotelTextBold
} from "../../Components";
import { saveShortListHotel, removeShortListHotel } from "./Saga";
import HotelList from "./HotelList";
import { NORESULT, SEARCHHOTEL, EMPTYIMAGE } from "../../Assets/Img/Image";
import I18n from "../../Assets/Strings/i18n";
import styles from "./styles";
import { GoToast } from "../../../../Components";
import withCancellableSaga from "../../../../utils/withCancellableSaga";
import Config from "react-native-config";

const { CLEARTRIP_SERVER_IMAGE_URL } = Config;
const { width, height } = Dimensions.get("window");
const { UIManager } = NativeModules;
if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}

const Featured = props => {
	return (
		<TouchableOpacity
			style={styles.featuredButtonContainer}
			onPress={() => props.closeModal("feature")}>
			<View style={styles.featuredContainer}>
				<View style={styles.sortByContainer}>
					<CtHotelTextRegular>{I18n.t("sort_by")}</CtHotelTextRegular>
				</View>
				<TouchableOpacity
					style={styles.sort}
					onPress={() => props.applySort(0)}>
					<CtHotelTextRegular
						style={[
							styles.sortText,
							{ color: props.sortType === 0 ? "#3366cc" : "#000" }
						]}>
						{I18n.t("featured")}
					</CtHotelTextRegular>
					{props.sortType === 0 && (
						<Icon
							iconType={"ionicon"}
							iconSize={height / 30}
							iconName={"ios-checkmark"}
							iconColor={"#3366cc"}
						/>
					)}
				</TouchableOpacity>
				<TouchableOpacity
					style={styles.sort}
					onPress={() => props.applySort(1)}>
					<CtHotelTextRegular
						style={[
							styles.sortText,
							{ color: props.sortType === 1 ? "#3366cc" : "#000" }
						]}>
						{I18n.t("price_low_to_high")}
					</CtHotelTextRegular>
					{props.sortType === 1 && (
						<Icon
							iconType={"ionicon"}
							iconSize={height / 30}
							iconName={"ios-checkmark"}
							iconColor={"#3366cc"}
						/>
					)}
				</TouchableOpacity>
				<TouchableOpacity
					style={styles.sort}
					onPress={() => props.applySort(2)}>
					<CtHotelTextRegular
						style={[
							styles.sortText,
							{ color: props.sortType === 2 ? "#3366cc" : "#000" }
						]}>
						{I18n.t("price_high_to_low")}
					</CtHotelTextRegular>
					{props.sortType === 2 && (
						<Icon
							iconType={"ionicon"}
							iconSize={height / 30}
							iconName={"ios-checkmark"}
							iconColor={"#3366cc"}
						/>
					)}
				</TouchableOpacity>
				<TouchableOpacity
					style={styles.sort}
					onPress={() => props.applySort(3)}>
					<CtHotelTextRegular
						style={[
							styles.sortText,
							{ color: props.sortType === 3 ? "#3366cc" : "#000" }
						]}>
						{I18n.t("ratings")}
					</CtHotelTextRegular>
					{props.sortType === 3 && (
						<Icon
							iconType={"ionicon"}
							iconSize={height / 30}
							iconName={"ios-checkmark"}
							iconColor={"#3366cc"}
						/>
					)}
				</TouchableOpacity>
				<TouchableOpacity
					style={styles.sort}
					onPress={() => props.applySort(4)}>
					<CtHotelTextRegular
						style={[
							styles.sortText,
							{ color: props.sortType === 4 ? "#3366cc" : "#000" }
						]}>
						{I18n.t("savings")}
					</CtHotelTextRegular>
					{props.sortType === 4 && (
						<Icon
							iconType={"ionicon"}
							iconSize={height / 30}
							iconName={"ios-checkmark"}
							iconColor={"#3366cc"}
						/>
					)}
				</TouchableOpacity>
			</View>
		</TouchableOpacity>
	);
};

class SelectHotel extends Component {
	constructor(props) {
		super(props);
		this.state = {
			progress: 5,
			modalVisible: false,
			filterModal: false,
			featureModal: false,
			filterIconVisible: true,
			filterParameters: {
				hotelPay: false,
				freeCancel: false,
				minPrice: Number.MIN_VALUE,
				maxPrice: Number.MAX_VALUE,
				starRating: [],
				taRating: 0,
				facilities: [
					{ type: "Air Conditioning", isChecked: false },
					{ type: "Bar", isChecked: false },
					{ type: "Business Center", isChecked: false },
					{ type: "Cofee Shop", isChecked: false },
					{ type: "Swimming Pool", isChecked: false },
					{ type: "Restaurant", isChecked: false },
					{ type: "Room Service", isChecked: false }
				]
			},
			isFiltered: false,
			sortType: 0,
			isError: false,
			searchQuery: ""
		};
		this.priceRange = {
			minPrice: Number.MIN_VALUE,
			maxPrice: Number.MAX_VALUE
		};
		this.getSecondaryHeader = this.getSecondaryHeader.bind(this);
	}

	componentDidMount() {
		let self = this;
		this.interval = setInterval(function() {
			if (self.state.progress < 90) {
				self.setState({
					progress: self.state.progress + 3
				});
			}
		}, 1000);
	}

	getSecondaryHeader = () => {
		let checkInDate = this.props.stayDate.checkInDate;
		let checkOutDate = this.props.stayDate.checkOutDate;
		let rooms =
			this.props.personalInfo.length +
			" " +
			I18n.t("room", { count: this.props.personalInfo.length });
		let totalCount = 0;
		for (let item of this.props.personalInfo) {
			totalCount += item.adult + item.child.length;
		}
		let date =
			I18n.l("date.formats.header_day", checkInDate) +
			" - " +
			I18n.l("date.formats.header_day", checkOutDate);
		return !this.props.isAnyTime
			? date + ", " + totalCount + " " + I18n.t("guests") + ", " + rooms
			: totalCount + " " + I18n.t("guests") + ", " + rooms;
	};

	shouldComponentUpdate(props, state) {
		if (props.error !== null) {
			clearInterval(this.interval);
			GoToast.show(props.error.message, I18n.t("error"));
			this.props.dispatch(saveServerError(null));
			this.props.navigation.goBack();
		}
		if (props.noHotelError === true) {
			clearInterval(this.interval);
			this.setState({ isError: true });
			this.props.dispatch(noHotelError(false));
		}
		if (props.hotels !== null && props.hotels !== this.props.hotels) {
			clearInterval(this.interval);
			if (
				props.hotels.hasOwnProperty("success") &&
				props.hotels.success === 0
			) {
				GoToast.show(props.hotels.message, I18n.t("information"));
				this.props.dispatch(saveSearchHotel(null));
				this.props.navigation.goBack();
			}
			if (
				props.hotelsDefault !== null &&
				props.hotelsDefault !== this.props.hotelsDefault
			) {
				this.priceRange = this.getPriceRange(props.hotelsDefault);
			}
			return true;
		}
		return true;
	}

	closeModal = type => {
		this.setState({ modalVisible: false, featureModal: false });
	};
	componentWillUnmount() {
		clearInterval(this.interval);
		this.props.dispatch(saveSearchHotel(null));
	}

	getPriceRange = hotel => {
		let min = Number.MAX_VALUE;
		let max = Number.MIN_VALUE;
		hotel.map(row => {
			if (row.price < min) {
				min = row.price;
			}
			if (row.price > max) {
				max = row.price;
			}
		});
		return { minPrice: min, maxPrice: max };
	};

	getHotel = () => {
		let tempHotel = this.props.hotels.slice();
		if (this.state.searchQuery.trim().length >= 3) {
			tempHotel = tempHotel.filter(
				row =>
					row["basic-info"]["hotel-info:hotel-name"]
						.toLowerCase()
						.search(this.state.searchQuery.toLowerCase().trim()) >= 0
			);
		}
		return tempHotel;
	};

	applyFilter = (item, resetApplied) => {
		this.setState({
			filterParameters: item,
			isFiltered: !resetApplied
		});
		this.props.dispatch(
			filterHotel({
				filterParameters: item,
				isFiltered: !resetApplied,
				sortType: this.state.sortType
			})
		);
	};

	applySort = value => {
		this.setState({
			sortType: value,
			modalVisible: false,
			featureModal: false
		});
		this.props.dispatch(
			filterHotel({
				filterParameters: this.state.filterParameters,
				isFiltered: this.state.isFiltered,
				sortType: value
			})
		);
	};
	getSortMessage = () => {
		switch (this.state.sortType) {
			case 0:
				return I18n.t("featured");
			case 1:
			case 2:
				return I18n.t("price");
			case 3:
				return I18n.t("rating");
			case 4:
				return I18n.t("saving");
			default:
				return I18n.t("featured");
		}
	};

	callGetHotelInfo = item => {
		this.props.navigation.navigate("HotelDetail", {
			hotelId: item["hotel-id"]
		});
	};

	handleHotelShortlist = item => {
		if (
			this.props.shortListHotel.filter(
				obj => obj["hotel-id"] === item["hotel-id"]
			).length === 1
		) {
			this.props.dispatch(removeShortListHotel(item));
		} else {
			this.props.dispatch(saveShortListHotel(item));
		}
	};
	renderHotels = ({ item }) => {
		return (
			<TouchableOpacity
				onPress={() => this.callGetHotelInfo(item)}
				style={styles.listRow}>
				<View style={styles.rowView}>
					<ImageBackground
						loadingIndicatorSource={EMPTYIMAGE}
						source={
							item["basic-info"]["hotel-info:thumb-nail-image"] !== ""
								? {
										uri:
											CLEARTRIP_SERVER_IMAGE_URL +
											item["basic-info"]["hotel-info:thumb-nail-image"]
								  }
								: EMPTYIMAGE
						}
						resizeMode={"contain"}
						style={styles.image}>
						<TouchableOpacity
							onPress={() => {
								if (
									this.props.shortListHotel.filter(
										obj => obj["hotel-id"] === item["hotel-id"]
									).length === 1
								) {
									this.props.dispatch(removeShortListHotel(item));
								} else {
									this.props.dispatch(saveShortListHotel(item));
								}
							}}
							style={styles.shortListBar}>
							{this.props.shortListHotel.filter(
								obj => obj["hotel-id"] === item["hotel-id"]
							).length === 1 ? (
								<View style={styles.rowView}>
									<Icon
										iconType={"ionicon"}
										iconSize={height / 46}
										iconName={"ios-heart"}
										iconColor={"#ff0000"}
									/>
									<CtHotelTextRegular style={styles.shortListText}>
										{I18n.t("shortlisted")}
									</CtHotelTextRegular>
								</View>
							) : (
								<View style={styles.rowView}>
									<Icon
										iconType={"icomoon"}
										iconSize={height / 46}
										iconName={"ios-heart-empty"}
										iconColor={"#ffffff"}
									/>
									<CtHotelTextRegular style={styles.shortListText}>
										{I18n.t("shortlist")}
									</CtHotelTextRegular>
								</View>
							)}
						</TouchableOpacity>
					</ImageBackground>
				</View>
				<View style={styles.hotelNameAndRatingContainer}>
					<View>
						<CtHotelTextMedium style={styles.nameText}>
							{item["basic-info"]["hotel-info:hotel-name"]}
						</CtHotelTextMedium>
						<CtHotelTextRegular style={styles.hotelLocalityText}>
							{item["basic-info"]["hotel-info:locality"]}
						</CtHotelTextRegular>
					</View>
					<View style={styles.ratingSection}>
						<View style={styles.ratingContainer}>
							<View>
								<StarReview
									rating={item["basic-info"]["hotel-info:star-rating"]}
								/>
							</View>
							<View>
								<CtHotelTextRegular style={styles.cancellationChargesText}>
									{I18n.t("cancellation_charges_apply")}
								</CtHotelTextRegular>
							</View>
						</View>
						{!this.props.isAnyTime && (
							<View style={styles.priceSection}>
								<CtHotelTextRegular style={styles.baseFareText}>
									{item.discount !== 0
										? " ₹" + I18n.toNumber(item.baseFare, { precision: 0 })
										: ""}
								</CtHotelTextRegular>
								<CtHotelTextRegular style={styles.discountText}>
									{"₹" +
										I18n.toNumber(
											parseInt(item.baseFare) - parseInt(item.discount),
											{ precision: 0 }
										)}
								</CtHotelTextRegular>
								<CtHotelTextRegular style={styles.taxText}>
									{item.tax !== 0
										? "+₹" +
										  I18n.toNumber(item.tax, { precision: 0 }) +
										  " " +
										  I18n.t("taxes")
										: ""}
								</CtHotelTextRegular>
								<CtHotelTextRegular style={styles.priceText}>
									{I18n.t("total") +
										" ₹" +
										I18n.toNumber(item.price, { precision: 0 })}
								</CtHotelTextRegular>
							</View>
						)}
					</View>
				</View>
			</TouchableOpacity>
		);
	};

	render() {
		return (
			<View style={styles.container}>
				<StatusBar backgroundColor="#1f3d7a" barStyle="light-content" />
				<View style={styles.header}>
					<View style={styles.backButtonAndTextContainer}>
						<TouchableOpacity
							style={styles.backButton}
							onPress={() => {
								this.props.navigation.goBack();
							}}>
							<Icon
								iconType={"material"}
								iconSize={height / 26}
								iconName={"arrow-back"}
								iconColor={"#ffffff"}
							/>
						</TouchableOpacity>
						<View>
							<View style={styles.rowView}>
								<CtHotelTextBold style={styles.primaryHeader}>
									{this.props.searchType.value.city}
								</CtHotelTextBold>
								{this.props.hotels && (
									<CtHotelTextBold style={styles.primaryHeader}>
										{" | " + this.props.hotels.length + I18n.t("hotels")}
									</CtHotelTextBold>
								)}
							</View>
							<View style={styles.rowView}>
								<CtHotelTextRegular style={styles.secondaryHeader}>
									{this.getSecondaryHeader()}
								</CtHotelTextRegular>
							</View>
						</View>
					</View>
					<TouchableOpacity
						style={styles.heartButton}
						onPress={() => {
							//method to navigate shortlisted hotel page
							if (this.props.shortListHotel.length > 0) {
								this.props.navigation.navigate("ShortListHotel");
							}
						}}>
						{this.props.shortListHotel.length === 0 ? (
							<Icon
								iconType={"icomoon"}
								iconSize={height / 26}
								iconName={"ios-heart-empty"}
								iconColor={"#ffffff"}
							/>
						) : (
							<Icon
								iconType={"ionicon"}
								iconSize={height / 26}
								iconName={"ios-heart"}
								iconColor={"#ff0000"}
							/>
						)}
					</TouchableOpacity>
					{/* number of shortlisted hotels at absolute position */}
					{this.props.shortListHotel.length > 0 && (
						<View style={styles.shortlistNumber}>
							<CtHotelTextRegular style={styles.shortlistNumberText}>
								{this.props.shortListHotel.length}
							</CtHotelTextRegular>
						</View>
					)}
				</View>
				{/* body section*/}
				{this.state.isError === true ? (
					<View style={styles.errorContainer}>
						<Image
							resizeMode={"contain"}
							source={NORESULT}
							style={styles.noReseltImage}
						/>
						<CtHotelTextBold style={styles.errorText}>
							{I18n.t("error_mesage_while_searching")}
						</CtHotelTextBold>
						<CtHotelTextBold style={styles.errorText}>
							{I18n.t("try_different_search")}
						</CtHotelTextBold>
						<TouchableOpacity
							style={styles.goBack}
							onPress={() => this.props.navigation.goBack()}>
							<CtHotelTextRegular style={styles.buttonText}>
								{I18n.t("modify_search")}
							</CtHotelTextRegular>
						</TouchableOpacity>
					</View>
				) : (
					<View style={styles.container}>
						{this.props.hotels === null ? (
							<ProgressBar
								width={width}
								height={height / 200}
								value={this.state.progress}
								backgroundColor="#EC6733"
								barEasing={"linear"}
							/>
						) : (
							<View style={styles.container}>
								<ImageBackground
									source={SEARCHHOTEL}
									style={styles.imageSearchBar}>
									<View style={styles.searchBar}>
										<Icon
											iconType={"material"}
											iconSize={height / 32}
											iconName={"search"}
											iconColor={"#rgba(0,0,0,0.5)"}
										/>
										<TextInput
											style={styles.textInput}
											placeholder={I18n.t("search_loc_hotel")}
											placeholderTextColor={"rgba(0,0,0,0.5)"}
											value={this.state.text}
											onChangeText={text => {
												this.setState({ searchQuery: text });
											}}
											underlineColorAndroid={"transparent"}
											selectionColor={"#3366cc"}
										/>
									</View>
								</ImageBackground>
								<HotelList
									hotels={this.props.hotels}
									callGetHotelInfo={this.callGetHotelInfo}
									handleHotelShortlist={this.handleHotelShortlist}
									searchQuery={this.state.searchQuery}
									shortListHotel={this.props.shortListHotel}
									sortType={this.state.sortType}
									isAnyTime={this.props.isAnyTime}
								/>
								<View
									style={[
										styles.fixedBottom,
										{
											bottom:
												this.state.filterIconVisible === true
													? height / 35
													: -height / 15
										}
									]}>
									<TouchableOpacity
										onPress={() =>
											this.setState({ modalVisible: true, featureModal: true })
										}
										style={styles.featureButton}>
										<Icon
											iconType={"material"}
											iconSize={width / 18}
											iconName={"swap-vert"}
											iconColor={"#ffffff"}
										/>
										<CtHotelTextBold style={styles.featureText}>
											{I18n.t("by") + " " + this.getSortMessage()}
										</CtHotelTextBold>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() => {
											this.props.navigation.navigate("Filter", {
												priceRange: this.priceRange,
												filterParameter: this.state.filterParameters,
												applyFilter: this.applyFilter
											});
										}}
										style={styles.filterButton}>
										<Icon
											iconType={"ionicon"}
											iconSize={width / 20}
											iconName={"ios-funnel"}
											iconColor={"#ffffff"}
										/>
									</TouchableOpacity>
								</View>
							</View>
						)}
					</View>
				)}
				<Modal
					transparent={this.state.featureModal}
					animationType={"slide"}
					visible={this.state.modalVisible}
					onRequestClose={() => this.setState({ modalVisible: false })}
					hardwareAccelerated={true}>
					<Featured
						closeModal={this.closeModal}
						applySort={this.applySort}
						sortType={this.state.sortType}
					/>
				</Modal>
			</View>
			// 	<Filter
			// 	priceRange={this.priceRange}
			// 	filterParameter={this.state.filterParameters}
			// 	applyFilter={this.applyFilter}
			// />
		);
	}
}

function mapStateToProps(state) {
	return {
		stayDate: state.cleartripHotel && state.cleartripHotel.searchHotel.stayDate,
		personalInfo:
			state.cleartripHotel && state.cleartripHotel.searchHotel.personalInfo,
		searchType:
			state.cleartripHotel && state.cleartripHotel.searchHotel.searchType,
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		hotels:
			state.cleartripHotel &&
			state.cleartripHotel.searchHotel &&
			state.cleartripHotel.searchHotel.hotels
				? state.cleartripHotel.searchHotel.hotels
				: null,
		hotelsDefault:
			state.cleartripHotel &&
			state.cleartripHotel.searchHotel &&
			state.cleartripHotel.searchHotel.hotelsDefault
				? state.cleartripHotel.searchHotel.hotelsDefault
				: null,
		error: state.cleartripHotel && state.cleartripHotel.searchHotel.error,
		noHotelError:
			state.cleartripHotel && state.cleartripHotel.searchHotel.noHotelError,
		shortListHotel:
			state.cleartripHotel && state.cleartripHotel.selectHotel.shortListHotel,
		isAnyTime:
			state.cleartripHotel && state.cleartripHotel.searchHotel.isAnyTime
	};
}

export default connect(
	mapStateToProps,
	null,
	null
)(withCancellableSaga(SelectHotel, { actions: [getSearchHotel] }));
