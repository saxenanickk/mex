import { takeLatest, takeEvery, put, call, all } from "redux-saga/effects";
import Api from "../../Api/";
import { saveServerError } from "../SearchHotel/Saga";
export const CTHOTEL_SAVE_SHORTLIST_HOTEL = "CTHOTEL_SAVE_SHORTLIST_HOTEL";
export const CTHOTEL_REMOVE_SHORTLIST_HOTEL = "CTHOTEL_REMOVE_SHORTLIST_HOTEL";
export const CTHOTEL_GET_HOTEL_INFO_BY_ID = "CTHOTEL_GET_HOTEL_INFO_BY_ID";
export const CTHOTEL_SAVE_HOTEL_INFO_BY_ID = "CTHOTEL_SAVE_HOTEL_INFO_BY_ID";
export const CTHOTEL_GET_RATE_INFO_OF_HOTEL = "CTHOTEL_GET_RATE_INFO_OF_HOTEL";
export const CTHOTEL_SAVE_RATE_INFO_OF_HOTEL =
	"CTHOTEL_SAVE_RATE_INFO_OF_HOTEL";
export const CTHOTEL_SAVE_RATE_ERROR = "CTHOTEL_SAVE_RATE_ERROR";
export const CTHOTEL_CHANGE_SHORT_LIST_HOTEL =
	"CTHOTEL_CHANGE_SHORT_LIST_HOTEL";
export const saveShortListHotel = payload => ({
	type: CTHOTEL_SAVE_SHORTLIST_HOTEL,
	payload
});
export const removeShortListHotel = payload => ({
	type: CTHOTEL_REMOVE_SHORTLIST_HOTEL,
	payload
});
export const getHotelInfoById = payload => ({
	type: CTHOTEL_GET_HOTEL_INFO_BY_ID,
	payload
});
export const saveHotelInfoById = payload => ({
	type: CTHOTEL_SAVE_HOTEL_INFO_BY_ID,
	payload
});
export const getHotelRateInfo = payload => ({
	type: CTHOTEL_GET_RATE_INFO_OF_HOTEL,
	payload
});
export const saveHotelRateInfo = payload => ({
	type: CTHOTEL_SAVE_RATE_INFO_OF_HOTEL,
	payload
});

export const saveRateError = payload => ({
	type: CTHOTEL_SAVE_RATE_ERROR,
	payload
});

export const changeShortListHotel = payload => ({
	type: CTHOTEL_CHANGE_SHORT_LIST_HOTEL,
	payload
});
export function* selectHotelSaga(dispatch) {
	yield takeEvery(CTHOTEL_GET_HOTEL_INFO_BY_ID, handleGetHotelInfoById);
	yield takeEvery(CTHOTEL_GET_RATE_INFO_OF_HOTEL, handleGetHotelRateInfo);
}

function* handleGetHotelInfoById(action) {
	console.log("Hotel Info handler", action);
	try {
		const [hotelInfo, rateInfo] = yield all([
			call(Api.getHotelInfoById, action.payload.hotelInfo),
			call(Api.getRateInfoOfHotel, action.payload.hotelRateInfo)
		]);
		console.log("Hotel Info", hotelInfo, rateInfo);
		if (
			hotelInfo.hasOwnProperty("hotel-info") &&
			rateInfo.hasOwnProperty("hotel-search-response") &&
			rateInfo["hotel-search-response"].hotels !== ""
		) {
			yield put(
				saveHotelInfoById({
					hotelInfo: hotelInfo["hotel-info"],
					hotelRateInfo: !action.payload.isAnyTime
						? rateInfo["hotel-search-response"]
						: null
				})
			);
		} else {
			yield put(saveRateError(true));
		}
	} catch (error) {
		console.log("hotel info error", error);
		yield put(saveRateError(true));
	}
}

function* handleGetHotelRateInfo(action) {
	try {
		console.log("Rate Info:", action.payload);
		let rateInfo = yield call(Api.getRateInfoOfHotel, action.payload);
		console.log("Rate Info:", rateInfo);
		if (rateInfo.hasOwnProperty("hotel-search-response")) {
			yield put(saveHotelRateInfo(rateInfo["hotel-search-response"]));
		} else {
			yield put(saveRateError(true));
		}
	} catch (error) {
		console.log("hotel info error", error);
	}
}
