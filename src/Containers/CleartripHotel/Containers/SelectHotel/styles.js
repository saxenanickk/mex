import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: { flex: 1 },
	noReseltImage: {
		width: width / 3,
		height: width / 3,
		marginBottom: height / 20
	},
	backButtonAndTextContainer: { flexDirection: "row", alignItems: "center" },
	featuredButtonContainer: { flex: 1, backgroundColor: "rgba(0,0,0,0.7)" },
	featuredContainer: {
		position: "absolute",
		bottom: 0,
		backgroundColor: "#fff"
	},
	sortByContainer: {
		width: width,
		paddingHorizontal: width / 20,
		paddingVertical: width / 30,
		backgroundColor: "#e9e9e9"
	},
	rowView: { flexDirection: "row" },
	shortlistNumberText: {
		color: "#fff",
		fontSize: height / 70
	},
	shortListText: {
		color: "#fff",
		fontSize: height / 70,
		marginLeft: width / 40
	},
	hotelLocalityText: {
		fontSize: height / 60
	},
	cancellationChargesText: {
		fontSize: height / 60
	},
	errorContainer: { flex: 1, justifyContent: "center", alignItems: "center" },
	heartButton: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	backButton: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	priceText: {
		fontSize: height / 60,
		color: "#000"
	},
	taxText: {
		fontSize: height / 70
	},
	discountText: {
		fontSize: height / 40,
		color: "#000"
	},
	baseFareText: {
		fontSize: height / 70,
		textDecorationLine: "line-through",
		textDecorationColor: "rgba(0,0,0,0.5)"
	},
	priceSection: {
		justifyContent: "space-between",
		width: width / 4,
		alignItems: "flex-end"
	},
	nameText: {
		color: "#000",
		fontSize: height / 50,
		width: width / 2
	},
	shortListBar: {
		width: width / 4,
		backgroundColor: "rgba(0,0,0,0.5)",
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		paddingVertical: width / 70
	},
	image: {
		width: width / 4,
		height: height / 6,
		borderRadius: width / 20,
		justifyContent: "flex-end"
	},
	sortText: {
		color: "#000",
		fontSize: height / 55
	},
	sort: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingRight: width / 20,
		marginLeft: width / 20,
		paddingVertical: width / 40,
		backgroundColor: "#fff",
		borderBottomWidth: 1,
		borderBottomColor: "#DFDDE0"
	},
	fixedBottom: {
		flexDirection: "row",
		justifyContent: "space-around",
		position: "absolute",
		bottom: height / 35,
		right: 0,
		width: width / 1.9
	},
	filterButton: {
		width: width / 9,
		height: width / 9,
		borderRadius: width / 18,
		elevation: 5,
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#3366cc"
	},
	featureButton: {
		width: width / 3,
		height: width / 9,
		borderRadius: width / 15,
		flexDirection: "row",
		paddingHorizontal: width / 30,
		elevation: 10,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#3366cc"
	},
	featureText: {
		color: "#fff"
	},
	header: {
		width: width,
		elevation: 5,
		backgroundColor: "#3366cc",
		justifyContent: "space-between",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33
	},
	headerText: {
		color: "#ffffff",
		fontSize: height / 45.71,
		marginLeft: width / 26.3
	},
	textStyle: {
		color: "#000"
	},
	dateTime: {
		flexDirection: "row",
		paddingHorizontal: width / 27,
		justifyContent: "space-between",
		alignItems: "center",
		height: height / 13,
		borderBottomWidth: 0.3,
		borderBottomColor: "#DFDDE0"
	},
	bar: {
		width: width,
		paddingVertical: height / 35,
		paddingHorizontal: width / 27,
		borderBottomWidth: 0.3,
		borderBottomColor: "#DFDDE0"
	},
	search: {
		width: width / 1.2,
		paddingVertical: width / 30,
		backgroundColor: "#EC6733",
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center"
	},
	cross: {
		width: width / 20,
		height: width / 20,
		borderRadius: width / 40,
		backgroundColor: "#777777",
		justifyContent: "center",
		alignItems: "center"
	},
	primaryHeader: {
		color: "#fff",
		fontSize: height / 50
	},
	secondaryHeader: {
		color: "rgba(255,255,255,0.5)",
		fontSize: height / 70
	},
	listRow: {
		flexDirection: "row",
		borderBottomWidth: 0.5,
		borderColor: "#000",
		paddingHorizontal: width / 26.3,
		paddingVertical: height / 75
	},
	ratingSection: {
		flexDirection: "row",
		width: width / 1.52,
		height: height / 10,
		justifyContent: "space-between"
	},
	shortlistNumber: {
		position: "absolute",
		top: height / 70,
		right: width / 18,
		width: width / 25,
		height: width / 25,
		borderRadius: width / 50,
		backgroundColor: "#000",
		justifyContent: "center",
		alignItems: "center",
		elevation: 10
	},
	goBack: {
		width: width / 1.2,
		marginTop: height / 20,
		paddingVertical: width / 30,
		backgroundColor: "#EC6733",
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center"
	},
	errorText: {
		fontSize: height / 55,
		color: "#000"
	},
	buttonText: {
		fontSize: height / 55,
		fontWeight: "bold",
		color: "#fff"
	},
	textInput: {
		marginLeft: width / 40,
		width: width / 1.2,
		height: height / 15
	},
	imageSearchBar: {
		flexDirection: "row",
		width: width,
		height: height / 12,
		alignItems: "center",
		elevation: 5,
		paddingHorizontal: width / 30
	},
	searchBar: {
		width: width / 1.06,
		height: height / 15,
		borderRadius: 5,
		flexDirection: "row",
		backgroundColor: "#fff",
		alignItems: "center",
		paddingHorizontal: width / 30
	},
	hotelNameAndRatingContainer: {
		justifyContent: "space-around",
		paddingLeft: width / 30
	},
	ratingContainer: { justifyContent: "flex-end" }
});

export default styles;
