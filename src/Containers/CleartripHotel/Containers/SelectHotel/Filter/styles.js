import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: { flex: 1 },
	priceTextContainer: {
		flexDirection: "row",
		justifyContent: "space-between",
		marginTop: width / 50
	},
	filterView: {
		flex: 1,
		backgroundColor: "#F0F0F0",
		marginBottom: height / 12
	},
	bottomBar: {
		position: "absolute",
		bottom: 0,
		flexDirection: "row",
		alignItems: "center",
		width: width,
		height: height / 13,
		backgroundColor: "#777777"
	},
	bottomButton: {
		width: width / 2,
		justifyContent: "center",
		alignItems: "center",
		height: height / 13
	},
	buttonText: {
		fontSize: height / 46.3,
		color: "#fff"
	},
	star: {
		width: width / 5.5,
		justifyContent: "center",
		alignItems: "center"
	},
	price: {
		fontSize: height / 65,
		color: "rgba(0,0,0,0.5)"
	},
	slider: {
		width: width,
		paddingVertical: width / 30,
		paddingHorizontal: width / 26.3,
		backgroundColor: "#fff"
	},
	subHeader: {
		flexDirection: "row",
		width: width,
		justifyContent: "space-between",
		paddingHorizontal: width / 26.3,
		height: height / 13,
		alignItems: "center"
	},
	header: {
		width: width,
		elevation: 8,
		backgroundColor: "#777777",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33
	},
	backButton: {
		width: width / 5,
		height: height / 13.33,
		justifyContent: "center",
		alignItems: "center"
	},
	headerText: {
		color: "#fff",
		fontWeight: "bold",
		fontSize: height / 45.71
	},
	horizontalBar: {
		width: width,
		paddingVertical: width / 30,
		paddingHorizontal: width / 26.3,
		flexDirection: "row",
		alignItems: "center",
		borderBottomWidth: 1,
		borderBottomColor: "#DFDDE0",
		backgroundColor: "#fff"
	},
	barText: {
		marginLeft: width / 30,
		color: "#000",
		fontSize: height / 45.71
	}
});

export default styles;
