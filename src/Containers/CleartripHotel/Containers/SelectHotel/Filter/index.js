import React, { Component } from "react";
import {
	View,
	Dimensions,
	StatusBar,
	TouchableOpacity,
	ScrollView,
	FlatList,
	Slider
} from "react-native";
import { Icon, Seperator } from "../../../../../Components";
import {
	CtHotelTextRegular,
	CtHotelTextBold,
	CheckBox
} from "../../../Components";
import I18n from "../../../Assets/Strings/i18n";
import styles from "./styles";
const { width, height } = Dimensions.get("window");

class FilterScreen extends Component {
	constructor(props) {
		super(props);
		console.log("props are", props);
		this.state = {
			hotelPay: props.filterParameter.hotelPay,
			freeCancel: props.filterParameter.freeCancel,
			minPrice:
				props.filterParameter.minPrice === Number.MIN_VALUE
					? props.priceRange.minPrice
					: props.filterParameter.minPrice,
			maxPrice:
				props.filterParameter.maxPrice === Number.MAX_VALUE
					? props.priceRange.maxPrice
					: props.filterParameter.maxPrice,
			starRating: props.filterParameter.starRating,
			taRating: props.filterParameter.taRating,
			isReset: false,
			facilities: props.filterParameter.facilities
		};
		this.star = [1, 2, 3, 4, 5];
		this.ta = [4.5, 4.0, 3.5, 3.0];
	}

	handleStarRating = rating => {
		if (this.state.starRating.indexOf(rating) === -1) {
			this.setState({
				starRating: [...this.state.starRating, rating]
			});
		} else {
			let index = this.state.starRating.indexOf(rating);
			let temp = this.state.starRating
				.slice(0, index)
				.concat(this.state.starRating.slice(index + 1, 5));
			this.setState({ starRating: temp });
		}
	};

	handleTARating = rating => {
		switch (rating) {
			case 3.0:
				if (this.state.taRating === 3.0) {
					this.setState({ taRating: 3.5 });
				} else {
					this.setState({ taRating: 3.0 });
				}
				break;
			case 3.5:
				if (this.state.taRating !== 0 && this.state.taRating <= 3.5) {
					this.setState({ taRating: 4.0 });
				} else {
					this.setState({ taRating: 3.5 });
				}
				break;
			case 4.0:
				if (this.state.taRating !== 0 && this.state.taRating <= 4.0) {
					this.setState({ taRating: 4.5 });
				} else {
					this.setState({ taRating: 4.0 });
				}
				break;
			case 4.5:
				if (this.state.taRating !== 0 && this.state.taRating <= 4.5) {
					this.setState({ taRating: 0 });
				} else {
					this.setState({ taRating: 4.5 });
				}
				break;
			default:
				return;
		}
	};

	printStar = () => {
		switch (this.state.starRating.length) {
			case 0:
				return I18n.t("all_star");
			case 1:
				return this.state.starRating[0] + " " + I18n.t("star");
			case 2:
				return (
					this.state.starRating[0] +
					" " +
					I18n.t("and") +
					" " +
					this.state.starRating[1] +
					" " +
					I18n.t("stars")
				);
			default:
				return I18n.t("multiple_rating");
		}
	};

	handleFacilities = (item, index) => {
		let temp = this.state.facilities.slice();
		temp[index].isChecked = !temp[index].isChecked;
		this.setState({ facilities: temp });
	};

	reset = () => {
		this.setState({
			hotelPay: false,
			freeCancel: false,
			minPrice: this.props.priceRange.minPrice,
			maxPrice: this.props.priceRange.maxPrice,
			starRating: [],
			taRating: 0,
			facilities: [
				{ type: "Air Conditioning", isChecked: false },
				{ type: "Bar", isChecked: false },
				{ type: "Business Center", isChecked: false },
				{ type: "Cofee Shop", isChecked: false },
				{ type: "Swimming Pool", isChecked: false },
				{ type: "Restaurant", isChecked: false },
				{ type: "Room Service", isChecked: false }
			],
			isReset: true
		});
	};

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<TouchableOpacity
						style={styles.backButton}
						onPress={() => this.props.navigation.goBack()}>
						<Icon
							iconType={"material"}
							iconSize={height / 26}
							iconName={"arrow-back"}
							iconColor={"#ffffff"}
						/>
					</TouchableOpacity>
					<CtHotelTextRegular style={styles.headerText}>
						{I18n.t("filter_results")}
					</CtHotelTextRegular>
				</View>
				<ScrollView style={styles.filterView}>
					<StatusBar backgroundColor="#535353" barStyle="light-content" />
					<View>
						<TouchableOpacity
							onPress={() => this.setState({ hotelPay: !this.state.hotelPay })}
							style={styles.horizontalBar}>
							<CheckBox
								width={width / 25}
								height={width / 25}
								borderRadius={width / 150}
								checkColor={"#3366cc"}
								value={this.state.hotelPay}
							/>
							<CtHotelTextRegular style={styles.barText}>
								{I18n.t("show_pay_at_hotel")}
							</CtHotelTextRegular>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() =>
								this.setState({ freeCancel: !this.state.freeCancel })
							}
							style={styles.horizontalBar}>
							<CheckBox
								width={width / 25}
								height={width / 25}
								borderRadius={width / 150}
								checkColor={"#3366cc"}
								value={this.state.freeCancel}
							/>
							<CtHotelTextRegular style={styles.barText}>
								{I18n.t("free_cancel_hotel")}
							</CtHotelTextRegular>
						</TouchableOpacity>
						<View style={styles.subHeader}>
							<CtHotelTextRegular>{I18n.t("total_price")}</CtHotelTextRegular>
							<CtHotelTextRegular>
								{I18n.toCurrency(this.state.minPrice, {
									precision: 0,
									unit: "₹"
								}) +
									" " +
									I18n.t("to") +
									" " +
									I18n.toCurrency(this.state.maxPrice, {
										precision: 0,
										unit: "₹"
									})}
							</CtHotelTextRegular>
						</View>
						<View style={styles.slider}>
							<Slider
								maximumTrackTintColor={"#777777"}
								minimumTrackTintColor={"#3366cc"}
								minimumValue={this.props.priceRange.minPrice}
								maximumValue={this.props.priceRange.maxPrice}
								thumbTintColor={"#3366cc"}
								value={this.state.maxPrice}
								onValueChange={value =>
									this.setState({ maxPrice: Math.round(value) })
								}
							/>
							<View style={styles.priceTextContainer}>
								<CtHotelTextRegular style={styles.price}>
									{I18n.toCurrency(this.props.priceRange.minPrice, {
										precision: 0,
										unit: "₹"
									})}
								</CtHotelTextRegular>
								<CtHotelTextRegular style={styles.price}>
									{I18n.toCurrency(this.props.priceRange.maxPrice, {
										precision: 0,
										unit: "₹"
									})}
								</CtHotelTextRegular>
							</View>
						</View>
						<View style={styles.subHeader}>
							<CtHotelTextRegular>{I18n.t("star_rating")}</CtHotelTextRegular>
							<CtHotelTextRegular>{this.printStar()}</CtHotelTextRegular>
						</View>
						<View style={styles.horizontalBar}>
							<FlatList
								extraData={this.state}
								horizontal={true}
								data={this.star}
								keyExtractor={(item, index) => index.toString()}
								renderItem={({ item, index }) => {
									return (
										<TouchableOpacity
											onPress={() => this.handleStarRating(item)}
											style={styles.star}>
											<Icon
												iconType={"ionicon"}
												iconSize={height / 20}
												iconName={"ios-star"}
												iconColor={
													this.state.starRating.indexOf(item) > -1
														? "#EC9715"
														: "rgba(0,0,0,0.2)"
												}
											/>
											<CtHotelTextRegular
												style={[styles.barText, { marginLeft: 0 }]}>
												{item}
											</CtHotelTextRegular>
										</TouchableOpacity>
									);
								}}
							/>
						</View>
						<View style={styles.subHeader}>
							<CtHotelTextRegular>
								{I18n.t("tripadvisor_rating")}
							</CtHotelTextRegular>
						</View>
						<FlatList
							extraData={this.state}
							data={this.ta}
							keyExtractor={(item, index) => index.toString()}
							renderItem={({ item, index }) => {
								return (
									<TouchableOpacity
										onPress={() => this.handleTARating(item)}
										style={styles.horizontalBar}>
										<CheckBox
											width={width / 25}
											height={width / 25}
											borderRadius={width / 150}
											checkColor={"#3366cc"}
											value={
												this.state.taRating !== 0 && this.state.taRating <= item
											}
										/>
										<CtHotelTextRegular style={styles.barText}>
											{item + " " + I18n.t("and_above")}
										</CtHotelTextRegular>
									</TouchableOpacity>
								);
							}}
						/>
						<View style={styles.subHeader}>
							<CtHotelTextRegular>
								{I18n.t("hotel_facilities")}
							</CtHotelTextRegular>
						</View>
						<FlatList
							extraData={this.state}
							data={this.state.facilities}
							keyExtractor={(item, index) => index.toString()}
							renderItem={({ item, index }) => {
								return (
									<TouchableOpacity
										onPress={() => this.handleFacilities(item, index)}
										style={styles.horizontalBar}>
										<CheckBox
											width={width / 25}
											height={width / 25}
											borderRadius={width / 150}
											checkColor={"#3366cc"}
											value={item.isChecked === true}
										/>
										<CtHotelTextRegular style={styles.barText}>
											{item.type}
										</CtHotelTextRegular>
									</TouchableOpacity>
								);
							}}
						/>
					</View>
				</ScrollView>
				<View style={styles.bottomBar}>
					<TouchableOpacity
						style={styles.bottomButton}
						onPress={() => this.reset()}>
						<CtHotelTextBold style={styles.buttonText}>
							{I18n.t("reset_all")}
						</CtHotelTextBold>
					</TouchableOpacity>
					<Seperator width={width / 750} height={height / 13} color={"#fff"} />
					<TouchableOpacity
						style={styles.bottomButton}
						onPress={() => {
							this.props.applyFilter(
								{
									hotelPay: this.state.hotelPay,
									freeCancel: this.state.freeCancel,
									minPrice: this.state.minPrice,
									maxPrice: this.state.maxPrice,
									starRating: this.state.starRating,
									taRating: this.state.taRating,
									facilities: this.state.facilities
								},
								this.state.isReset
							);
							this.props.navigation.goBack();
						}}>
						<CtHotelTextBold style={styles.buttonText}>
							{I18n.t("apply")}
						</CtHotelTextBold>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

const Filter = props => <FilterScreen {...props} {...props.route.params} />;

export default Filter;
