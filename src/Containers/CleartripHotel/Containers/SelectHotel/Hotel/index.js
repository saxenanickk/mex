/* eslint-disable radix */
import React, { Component } from "react";
import {
	View,
	Dimensions,
	StyleSheet,
	TouchableOpacity,
	ImageBackground
} from "react-native";
import { Icon } from "../../../../../Components";
import {
	StarReview,
	CtHotelTextMedium,
	CtHotelTextRegular
} from "../../../Components";
import styles from "./styles";
import { EMPTYIMAGE } from "../../../Assets/Img/Image";
import I18n from "../../../Assets/Strings/i18n";

const { width, height } = Dimensions.get("window");

class Hotel extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	shouldComponentUpdate(props, state) {
		if (
			props.shortListHotel !== this.props.shortListHotel &&
			props.shortListHotel.filter(
				obj => obj["hotel-id"] === this.props.hotel["hotel-id"]
			).length ===
				this.props.shortListHotel.filter(
					obj => obj["hotel-id"] === this.props.hotel["hotel-id"]
				).length
		) {
			return false;
		}
		return true;
	}
	render() {
		const item = this.props.hotel;
		const IMAGE_URL = `https://www.cleartrip.com/places/hotels${
			item["basic-info"]["hotel-info:thumb-nail-image"]
		}`;

		return (
			<TouchableOpacity
				activeOpacity={1}
				onPress={() => this.props.callGetHotelInfo(item)}
				style={styles.listRow}>
				<View>
					<ImageBackground
						loadingIndicatorSource={EMPTYIMAGE}
						source={
							item["basic-info"]["hotel-info:thumb-nail-image"] !== ""
								? {
										uri: IMAGE_URL
								  }
								: EMPTYIMAGE
						}
						resizeMode={"cover"}
						imageStyle={{ borderRadius: width / 200 }}
						style={styles.image}>
						<TouchableOpacity
							onPress={() => {
								this.props.handleHotelShortlist(item);
							}}
							style={styles.shortListBar}>
							{this.props.shortListHotel.filter(
								obj => obj["hotel-id"] === item["hotel-id"]
							).length === 1 ? (
								<View style={styles.rowView}>
									<Icon
										iconType={"ionicon"}
										iconSize={height / 46}
										iconName={"ios-heart"}
										iconColor={"#ff0000"}
									/>
									<CtHotelTextRegular style={styles.shortListText}>
										{I18n.t("shortlisted")}
									</CtHotelTextRegular>
								</View>
							) : (
								<View style={styles.rowView}>
									<Icon
										iconType={"icomoon"}
										iconSize={height / 46}
										iconName={"ios-heart-empty"}
										iconColor={"#ffffff"}
									/>
									<CtHotelTextRegular style={styles.shortListText}>
										{I18n.t("shortlist")}
									</CtHotelTextRegular>
								</View>
							)}
						</TouchableOpacity>
					</ImageBackground>
				</View>
				<View style={styles.hotelInfoView}>
					<View>
						<CtHotelTextMedium numberOfLines={2} style={styles.nameText}>
							{item["basic-info"]["hotel-info:hotel-name"]}
						</CtHotelTextMedium>
						<CtHotelTextRegular style={styles.localityText}>
							{item["basic-info"]["hotel-info:locality"]}
						</CtHotelTextRegular>
					</View>
					<View style={styles.ratingSection}>
						<View style={styles.ratingContainer}>
							<View>
								<StarReview
									rating={item["basic-info"]["hotel-info:star-rating"]}
								/>
							</View>
							<View>
								<CtHotelTextRegular style={styles.cancellationChargeText}>
									{I18n.t("cancellation_charges_apply")}
								</CtHotelTextRegular>
							</View>
						</View>
						{!this.props.isAnyTime && (
							<View style={styles.priceSection}>
								<CtHotelTextRegular style={styles.baseFareText}>
									{item.discount !== 0
										? " ₹" + I18n.toNumber(item.baseFare, { precision: 0 })
										: ""}
								</CtHotelTextRegular>
								<CtHotelTextRegular style={styles.discountText}>
									{"₹" +
										I18n.toNumber(
											parseInt(item.baseFare) - parseInt(item.discount),
											{ precision: 0 }
										)}
								</CtHotelTextRegular>
								<CtHotelTextRegular style={styles.taxText}>
									{item.tax !== 0
										? "+₹" +
										  I18n.toNumber(item.tax, { precision: 0 }) +
										  " " +
										  I18n.t("taxes")
										: ""}
								</CtHotelTextRegular>
								<CtHotelTextRegular style={styles.priceText}>
									{I18n.t("total") +
										" ₹" +
										I18n.toNumber(item.price, { precision: 0 })}
								</CtHotelTextRegular>
							</View>
						)}
					</View>
				</View>
			</TouchableOpacity>
		);
	}
}

export default Hotel;
