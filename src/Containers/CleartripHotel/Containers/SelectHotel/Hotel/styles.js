import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

const styles = StyleSheet.create({
	rowView: {
		flex: 1,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center"
	},
	shortListText: {
		color: "#fff",
		fontSize: height / 70,
		marginLeft: width / 40
	},
	cancellationChargeText: {
		fontSize: height / 70
	},
	priceText: {
		fontSize: height / 70,
		color: "#000"
	},
	taxText: {
		fontSize: height / 70
	},
	discountText: {
		fontSize: height / 40,
		color: "#000"
	},
	baseFareText: {
		fontSize: height / 70,
		textDecorationLine: "line-through",
		textDecorationColor: "rgba(0,0,0,0.5)"
	},
	priceSection: {
		flex: 2,
		justifyContent: "space-between",
		// width: width / 4,
		alignItems: "flex-end"
	},
	nameText: {
		color: "#000",
		fontSize: height / 55,
		width: width / 1.6
	},
	shortListBar: {
		backgroundColor: "rgba(0,0,0,0.5)",
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		borderBottomLeftRadius: width / 200,
		borderBottomRightRadius: width / 200
	},
	image: {
		width: width / 3.5,
		height: height / 6,
		justifyContent: "flex-end"
	},
	listRow: {
		flexDirection: "row",
		borderBottomWidth: 0.5,
		padding: width / 50,
		borderColor: "#000000"
	},
	ratingSection: {
		flexDirection: "row",
		width: width / 1.52,
		height: height / 10,
		justifyContent: "space-between"
	},
	hotelInfoView: {
		justifyContent: "space-between",
		marginLeft: width / 50
	},
	localityText: {
		fontSize: height / 70,
		color: "grey"
	},
	ratingContainer: { flex: 3, justifyContent: "flex-end" }
});

export default styles;
