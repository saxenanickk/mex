import React, { Component } from "react";
import { View, Image, FlatList, Dimensions } from "react-native";
import { CtHotelTextBold } from "../../../Components";
import Hotel from "../Hotel";
import { NORESULT } from "../../../Assets/Img/Image";
import I18n from "../../../Assets/Strings/i18n";
import styles from "./styles";

const { height, width } = Dimensions.get("window");
class HotelList extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	shouldComponentUpdate(props, state) {
		if (props.searchQuery !== this.props.searchQuery) {
			return true;
		}
		if (props.shortListHotel !== this.props.shortListHotel) {
			return true;
		}
		if (props.sortType !== this.props.sortType) {
			return true;
		}
		if (props.hotels !== null && props.hotels === this.props.hotels) {
			return false;
		}
		return true;
	}

	getHotel = () => {
		let tempHotel = this.props.hotels;
		if (this.props.searchQuery.trim().length >= 3) {
			tempHotel = tempHotel.filter(
				row =>
					row["basic-info"]["hotel-info:hotel-name"]
						.toLowerCase()
						.search(this.props.searchQuery.toLowerCase().trim()) >= 0
			);
		}
		return tempHotel;
	};

	render() {
		return (
			<FlatList
				removeClippedSubviews={true} // unmount components that are off of the window.
				maxToRenderPerBatch={8} // trading off for performance here
				windowSize={11}
				// getItemLayout is boosting performance here
				getItemLayout={(data, index) => ({
					length: Math.round(height / 5),
					offset: Math.round(height / 5) * index,
					index
				})}
				keyboardShouldPersistTaps={"always"}
				keyboardDismissMode={"interactive"}
				style={styles.flatListContainer}
				data={this.getHotel()}
				keyExtractor={item => `${item["hotel-id"]}`}
				renderItem={({ item }) => {
					return (
						<Hotel
							hotel={item}
							hotels={this.props.hotels}
							callGetHotelInfo={this.props.callGetHotelInfo}
							handleHotelShortlist={this.props.handleHotelShortlist}
							shortListHotel={this.props.shortListHotel}
							isAnyTime={this.props.isAnyTime}
						/>
					);
				}}
				ListEmptyComponent={() => {
					return (
						<View style={styles.emptyListContainer}>
							<Image
								source={NORESULT}
								style={styles.image}
								resizeMode={"contain"}
							/>
							<CtHotelTextBold style={styles.errorText}>
								{I18n.t("error_message_while_filter")}
							</CtHotelTextBold>
							<CtHotelTextBold style={styles.errorText}>
								{I18n.t("try_different_filter")}
							</CtHotelTextBold>
						</View>
					);
				}}
			/>
		);
	}
}

export default HotelList;
