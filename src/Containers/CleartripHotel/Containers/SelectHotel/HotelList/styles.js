import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

const styles = StyleSheet.create({
	errorText: {
		fontSize: height / 55,
		color: "#000"
	},
	flatListContainer: { height: height, backgroundColor: "#fff" },
	emptyListContainer: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	image: { width: width / 2, height: height / 2 }
});

export default styles;
