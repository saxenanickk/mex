import {
	CTHOTEL_SAVE_SHORTLIST_HOTEL,
	CTHOTEL_REMOVE_SHORTLIST_HOTEL,
	CTHOTEL_SAVE_HOTEL_INFO_BY_ID,
	CTHOTEL_SAVE_RATE_INFO_OF_HOTEL,
	CTHOTEL_SAVE_RATE_ERROR,
	CTHOTEL_CHANGE_SHORT_LIST_HOTEL
} from "./Saga";

const initialState = {
	shortListHotel: [],
	hotelInfo: null,
	hotelRateInfo: null,
	rateError: false
};

// retrieve all amenities of a hotel
const getAmenities = hotel => {
	let amenitiesArray = [];
	let resultedArray = [];
	if (
		hotel["basic-info"] &&
		hotel["basic-info"]["hotel-amenities"] &&
		hotel["basic-info"]["hotel-amenities"]["hotel-amenity"]
	) {
		if (
			hotel["basic-info"]["hotel-amenities"]["hotel-amenity"] instanceof Array
		) {
			amenitiesArray = hotel["basic-info"]["hotel-amenities"]["hotel-amenity"];
		} else {
			amenitiesArray.push(
				hotel["basic-info"]["hotel-amenities"]["hotel-amenity"]
			);
		}
	}
	amenitiesArray.map(amenity => {
		if (amenity.amenities && amenity.amenities.amenity) {
			if (amenity.amenities.amenity instanceof Array) {
				resultedArray = resultedArray.concat(amenity.amenities.amenity);
			} else {
				resultedArray.push(amenity.amenities.amenity);
			}
		}
	});
	return resultedArray;
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case CTHOTEL_SAVE_SHORTLIST_HOTEL:
			return {
				...state,
				shortListHotel: [...state.shortListHotel, action.payload]
			};
		case CTHOTEL_REMOVE_SHORTLIST_HOTEL:
			const temp = state.shortListHotel.slice(
				0,
				state.shortListHotel.indexOf(action.payload)
			);
			return {
				...state,
				shortListHotel: temp.concat(
					state.shortListHotel.slice(
						state.shortListHotel.indexOf(action.payload) + 1,
						state.shortListHotel.length
					)
				)
			};
		case CTHOTEL_SAVE_HOTEL_INFO_BY_ID:
			if (action.payload.hotelInfo !== null) {
				action.payload.hotelInfo.amenity = getAmenities(
					action.payload.hotelInfo
				);
			}
			return {
				...state,
				hotelInfo: action.payload.hotelInfo,
				hotelRateInfo: action.payload.hotelRateInfo
			};
		case CTHOTEL_SAVE_RATE_INFO_OF_HOTEL:
			return {
				...state,
				hotelRateInfo: action.payload
			};
		case CTHOTEL_SAVE_RATE_ERROR:
			return {
				...state,
				rateError: action.payload
			};
		case CTHOTEL_CHANGE_SHORT_LIST_HOTEL: {
			if (state.shortListHotel.length > 0) {
				let tempShortList = [];
				state.shortListHotel.map(hotel => {
					let obj = action.payload.filter(
						row => row["hotel-id"] === hotel["hotel-id"]
					);
					if (obj.length > 0) {
						tempShortList.push(obj[0]);
					}
				});
				return {
					...state,
					shortListHotel: tempShortList
				};
			} else {
				return {
					...state
				};
			}
		}
		default:
			return state;
	}
};
