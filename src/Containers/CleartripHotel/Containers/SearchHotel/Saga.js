import { takeLatest, put, call } from "redux-saga/effects";
import { takeLatestCancellable } from "../../../../utils/sagaHelperAction";
import Api from "../../Api/";
import { changeShortListHotel } from "../SelectHotel/Saga";
export const CTHOTEL_SAVE_STAY_DATE = "CTHOTEL_SAVE_CHECKIN_CHECKOUT_DATE";
export const CTHOTEL_SAVE_PERSONAL_INFORMATION =
	"CTHOTEL_SAVE_PERSONAL_INFORMATION";
export const CTHOTEL_SAVE_SEARCH_TYPE = "CTHOTEL_SAVE_SEARCH_TYPE";
export const CTHOTEL_GET_SEARCH_HOTEL = "CTHOTEL_GET_SEARCH_HOTEL";
export const CTHOTEL_SAVE_SEARCH_HOTEL = "CTHOTEL_SAVE_SEARCH_HOTEL";
export const CTHOTEL_SAVE_SERVER_ERROR = "CTHOTEL_SAVE_SERVER_ERROR";
export const CTHOTEL_SAVE_IS_ANY_TIME = "CTHOTEL_SAVE_IS_ANY_TIME";
export const CTHOTEL_NO_HOTEL_ERROR = "CTHOTEL_NO_HOTEL_ERROR";
export const CTHOTEL_GET_AUTO_COMPLETE = "CTHOTEL_GET_AUTO_COMPLETE";
export const CTHOTEL_SAVE_AUTO_COMPLETE = "CTHOTEL_SAVE_AUTO_COMPLETE";
export const CTHOTEL_FILTER_HOTEL = "CTHOTEL_FILTER_HOTEL";
export const CTHOTEL_SORT_HOTEL = "CTHOTEL_SORT_HOTEL";

export const saveStayDate = payload => ({
	type: CTHOTEL_SAVE_STAY_DATE,
	payload
});

export const savePersonalInfo = payload => ({
	type: CTHOTEL_SAVE_PERSONAL_INFORMATION,
	payload
});

export const saveSearchType = payload => ({
	type: CTHOTEL_SAVE_SEARCH_TYPE,
	payload
});
export const saveSearchHotel = payload => ({
	type: CTHOTEL_SAVE_SEARCH_HOTEL,
	payload
});
export const getSearchHotel = payload => ({
	type: CTHOTEL_GET_SEARCH_HOTEL,
	payload
});
export const saveServerError = payload => ({
	type: CTHOTEL_SAVE_SERVER_ERROR,
	payload
});

export const saveIsAnyTime = payload => ({
	type: CTHOTEL_SAVE_IS_ANY_TIME,
	payload
});
export const noHotelError = payload => ({
	type: CTHOTEL_NO_HOTEL_ERROR,
	payload
});
export const getAutoComplete = payload => ({
	type: CTHOTEL_GET_AUTO_COMPLETE,
	payload
});
export const saveAutoComplete = payload => ({
	type: CTHOTEL_SAVE_AUTO_COMPLETE,
	payload
});
export const filterHotel = payload => ({
	type: CTHOTEL_FILTER_HOTEL,
	payload
});
export const sortHotel = payload => ({
	type: CTHOTEL_SORT_HOTEL,
	payload
});
export function* searchHotelSaga(dispatch) {
	yield takeLatestCancellable(CTHOTEL_GET_SEARCH_HOTEL, handleGetSearchHotel);
	yield takeLatest(CTHOTEL_GET_AUTO_COMPLETE, handleGetAutoComplete);
}

function* handleGetSearchHotel(action) {
	try {
		let response = yield call(Api.getHotels, action.payload);
		console.log("getSearchHotel response is=>", response);
		if (response.hasOwnProperty("hotel-search-response")) {
			if (response["hotel-search-response"].hotels !== "") {
				yield put(
					saveSearchHotel(response["hotel-search-response"].hotels.hotel)
				);
				yield put(
					changeShortListHotel(response["hotel-search-response"].hotels.hotel)
				);
			} else {
				yield put(noHotelError(true));
			}
		} else {
			yield put(
				saveServerError({
					error: true,
					message: "an error occured"
				})
			);
		}
	} catch (error) {
		console.log("Get search hotel error:", error);
		if (error) {
			yield put(
				saveServerError({
					error: true,
					message: "unable to connect the server, try after sometime"
				})
			);
		}
	}
}

function* handleGetAutoComplete(action) {
	try {
		let response = yield call(Api.getAutocompleteQuery, action.payload);
		console.log("getAutoComplete response is=>", response);
		if (response.success === 1) {
			yield put(saveAutoComplete(response.data));
		} else if (response.success === 0) {
			yield put(saveAutoComplete([]));
		}
	} catch (error) {
		console.log("Get AutoComplete error:", error);
		if (error) {
			yield put(
				saveServerError({
					error: true,
					message: "unable to connect the server, try after sometime"
				})
			);
		}
	}
}
