import React, { Component } from "react";
import {
	View,
	Dimensions,
	StatusBar,
	TouchableOpacity,
	FlatList,
	Modal,
	BackHandler
} from "react-native";
import { Icon, GoPicker } from "../../../../../Components";
import { connect } from "react-redux";
import { CtHotelTextMedium, CtHotelTextRegular } from "../../../Components";
import I18n from "../../../Assets/Strings/i18n";
import styles from "./styles";
const { width, height } = Dimensions.get("window");
import { Picker } from "@react-native-community/picker";

/** constants used for filling up traveller details */
const MAX_ADULT = 4;
const MIN_ADULT = 1;
const MAX_CHILD = 3;
const MIN_CHILD = 0;
const COMMON_CHILD_AGE = 4;
const COMMON_ADULT_NUM = 2;
const UNIT = 1;
const MAX_PERSON_COUNT = 4;

/** AddTraveller component */
class AddTraveller extends Component {
	constructor(props) {
		super(props);
		this.state = {
			personalInfo: this.props.personalInfo.slice()
		};

		const { route } = props;
		const { saveTravellerDetail = () => {} } = route.params ? route.params : {};

		this.saveTravellerDetail = saveTravellerDetail;

		this.age = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
		this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
	}

	componentWillMount() {
		BackHandler.addEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
	}

	/**
	 * Back Button Handler
	 */
	handleBackButtonClick() {
		this.saveTravellerDetail(this.state.personalInfo);
		this.props.navigation.goBack();
		return true;
	}

	removeRoom = index => {
		let temp = this.state.personalInfo
			.slice(0, index)
			.concat(
				this.state.personalInfo.slice(
					index + UNIT,
					this.state.personalInfo.length
				)
			);
		this.setState({ personalInfo: temp });
	};

	addRoom = () => {
		let room = { adult: COMMON_ADULT_NUM, child: [] };
		this.setState({
			personalInfo: [...this.state.personalInfo, room]
		});
	};

	addTraveller = (type, index) => {
		let temp = this.state.personalInfo.slice();
		if (type === true) {
			temp[index].child.push({ age: COMMON_CHILD_AGE });
		} else {
			temp[index].adult = temp[index].adult + UNIT;
		}
		this.setState({ personalInfo: temp.slice() });
	};

	removeTraveller = (type, index) => {
		let temp = this.state.personalInfo.slice();
		if (type === true) {
			temp[index].child.pop();
		} else {
			temp[index].adult = temp[index].adult - UNIT;
		}
		this.setState({ personalInfo: temp.slice() });
	};

	setChildAge = (index, cindex, itemValue) => {
		let temp = this.state.personalInfo.slice();
		temp[index].child[cindex].age = itemValue;
		this.setState({ personalInfo: temp });
	};

	isDisabled = (item, type) => {
		return type === true
			? item.adult === MAX_ADULT ||
					item.adult + item.child.length === MAX_PERSON_COUNT
			: item.adult === MIN_ADULT;
	};

	isDisabledChild = (item, type) => {
		return type === true
			? item.child.length === MAX_CHILD ||
					item.adult + item.child.length === MAX_PERSON_COUNT
			: item.child.length === MIN_CHILD;
	};

	render() {
		return (
			<View style={styles.container}>
				<StatusBar backgroundColor="#535353" barStyle="light-content" />
				<View style={styles.header}>
					<View style={styles.backButtonContainer}>
						<TouchableOpacity
							style={styles.backButton}
							onPress={() => {
								this.saveTravellerDetail(this.state.personalInfo);
								this.props.navigation.goBack();
							}}>
							<Icon
								iconType={"material"}
								iconSize={height / 26}
								iconName={"arrow-back"}
								iconColor={"#ffffff"}
							/>
						</TouchableOpacity>
						<CtHotelTextMedium style={styles.roomAndGuestsText}>
							{I18n.t("room_guests")}
						</CtHotelTextMedium>
					</View>
				</View>
				{/* body section */}
				<FlatList
					style={styles.flatList}
					data={this.state.personalInfo}
					extraData={this.state}
					renderItem={({ item, index }) => {
						return (
							<View key={index} style={styles.roomItemContainer}>
								<View style={styles.roomBar}>
									<CtHotelTextRegular style={styles.roomText}>
										{I18n.t("room", { count: 1 }) + " " + (index + 1)}
									</CtHotelTextRegular>
									{index > 0 && (
										<TouchableOpacity
											style={styles.button}
											onPress={() => {
												this.removeRoom(index);
											}}>
											<CtHotelTextRegular style={styles.removeText}>
												{I18n.t("remove")}
											</CtHotelTextRegular>
										</TouchableOpacity>
									)}
								</View>
								<View style={styles.bar}>
									<CtHotelTextRegular style={styles.text}>
										{I18n.t("no_of_adult")}
									</CtHotelTextRegular>
									<View style={styles.buttonBar}>
										<TouchableOpacity
											style={styles.button}
											disabled={this.isDisabled(item, false)}
											onPress={() => {
												this.removeTraveller(false, index);
											}}>
											<View
												style={[
													styles.increment,
													{
														backgroundColor: !this.isDisabled(item, false)
															? "#777777"
															: "#DFDDE0"
													}
												]}>
												<Icon
													iconType={"ionicon"}
													iconSize={width / 30}
													iconName={"md-remove"}
													iconColor={"#ffffff"}
												/>
											</View>
										</TouchableOpacity>
										<CtHotelTextRegular style={styles.text}>
											{item.adult}
										</CtHotelTextRegular>
										<TouchableOpacity
											style={styles.button}
											disabled={this.isDisabled(item, true)}
											onPress={() => {
												this.addTraveller(false, index);
											}}>
											<View
												style={[
													styles.increment,
													{
														backgroundColor: !this.isDisabled(item, true)
															? "#777777"
															: "#DFDDE0"
													}
												]}>
												<Icon
													iconType={"ionicon"}
													iconSize={width / 30}
													iconName={"md-add"}
													iconColor={"#ffffff"}
												/>
											</View>
										</TouchableOpacity>
									</View>
								</View>
								<View style={styles.bar}>
									<CtHotelTextRegular style={styles.text}>
										{I18n.t("no_of_child")}
									</CtHotelTextRegular>
									<View style={styles.buttonBar}>
										<TouchableOpacity
											style={styles.button}
											disabled={this.isDisabledChild(item, false)}
											onPress={() => {
												this.removeTraveller(true, index);
											}}>
											<View
												style={[
													styles.increment,
													{
														backgroundColor: !this.isDisabledChild(item, false)
															? "#777777"
															: "#DFDDE0"
													}
												]}>
												<Icon
													iconType={"ionicon"}
													iconSize={width / 30}
													iconName={"md-remove"}
													iconColor={"#ffffff"}
												/>
											</View>
										</TouchableOpacity>
										<CtHotelTextRegular style={styles.text}>
											{item.child.length}
										</CtHotelTextRegular>
										<TouchableOpacity
											style={styles.button}
											disabled={this.isDisabledChild(item, true)}
											onPress={() => {
												this.addTraveller(true, index);
											}}>
											<View
												style={[
													styles.increment,
													{
														backgroundColor: !this.isDisabledChild(item, true)
															? "#777777"
															: "#DFDDE0"
													}
												]}>
												<Icon
													iconType={"ionicon"}
													iconSize={width / 30}
													iconName={"md-add"}
													iconColor={"#ffffff"}
												/>
											</View>
										</TouchableOpacity>
									</View>
								</View>
								{item.child.map((child, cindex) => {
									return (
										<View
											key={cindex}
											style={[
												styles.bar,
												{ paddingVertical: width / 70, paddingRight: 0 }
											]}>
											<CtHotelTextRegular style={styles.text}>
												{I18n.t("child", { count: 1 }) +
													" " +
													(cindex + 1) +
													"'s " +
													I18n.t("age")}
											</CtHotelTextRegular>
											<GoPicker
												selectedValue={
													child.age !== null ? child.age : "Select"
												}
												data={this.age}
												style={styles.picker}
												updateSelectedValue={({ itemValue, itemIndex }) => {
													this.setChildAge(index, cindex, itemValue);
												}}
											/>
										</View>
									);
								})}
							</View>
						);
					}}
				/>
				{/* button to add more rooms */}
				{this.state.personalInfo.length < 4 && (
					<TouchableOpacity
						onPress={() => {
							this.addRoom();
						}}
						style={styles.addButton}>
						<Icon
							iconType={"ionicon"}
							iconSize={height / 20}
							iconName={"md-add"}
							iconColor={"#ffffff"}
						/>
					</TouchableOpacity>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		personalInfo:
			state.cleartripHotel && state.cleartripHotel.searchHotel.personalInfo
	};
}
export default connect(
	mapStateToProps,
	null,
	null
)(AddTraveller);
