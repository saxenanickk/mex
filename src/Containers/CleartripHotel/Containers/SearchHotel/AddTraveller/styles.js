import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	backButtonContainer: {
		flexDirection: "row",
		alignItems: "center"
	},
	flatList: { height: height },
	roomItemContainer: {
		backgroundColor: "#F0F0F0"
	},
	roomText: {
		fontSize: height / 45,
		color: "#000"
	},
	removeText: {
		fontSize: height / 50,
		color: "#3366cc",
		fontWeight: "bold"
	},
	roomAndGuestsText: {
		color: "#fff",
		fontSize: height / 40
	},
	picker: { width: width / 3.6 },
	buttonBar: {
		flexDirection: "row",
		width: width / 5.5,
		justifyContent: "space-between",
		alignItems: "center"
	},
	roomBar: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		height: height / 15,
		paddingHorizontal: width / 30
	},
	header: {
		width: width,
		elevation: 8,
		backgroundColor: "#777777",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		justifyContent: "space-between"
	},
	addButton: {
		position: "absolute",
		bottom: height / 20,
		right: width / 15,
		width: width / 8,
		height: width / 8,
		borderRadius: width / 16,
		backgroundColor: "#3366cc",
		justifyContent: "center",
		alignItems: "center",
		elevation: 8,
		shadowOffset: { width: 2.2, height: 2.2 },
		shadowColor: "black",
		shadowOpacity: 0.2
	},
	increment: {
		width: width / 25,
		height: width / 25,
		borderRadius: width / 50,
		backgroundColor: "#777777",
		justifyContent: "center",
		alignItems: "center"
	},
	bar: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingLeft: width / 30,
		paddingRight: width / 20,
		paddingVertical: width / 30,
		backgroundColor: "#fff",
		borderBottomColor: "#DFDDE0",
		borderBottomWidth: 0.3,
		alignItems: "center"
	},
	text: {
		fontSize: height / 45,
		color: "#000"
	},
	backButton: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	button: {
		borderWidth: 0,
		borderColor: "#000",
		paddingHorizontal: width / 30,
		paddingVertical: width / 50
	},
	container: { flex: 1 }
});

export default styles;
