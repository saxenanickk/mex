import React, { Component } from "react";
import {
	View,
	Dimensions,
	StatusBar,
	TouchableOpacity,
	ScrollView,
	FlatList,
	TextInput,
	Keyboard
} from "react-native";
import { Icon } from "../../../../../Components";
import { connect } from "react-redux";
import LocationModule from "../../../../../CustomModules/LocationModule";
import { getAutoComplete, saveAutoComplete, saveSearchType } from "../Saga";
import { getCurrentLocation, saveFetchLocation } from "../../Splash/Saga";
import I18n from "../../../Assets/Strings/i18n";
import { CtHotelTextRegular, CtHotelTextBold } from "../../../Components";
import { CTProgressScreen } from "../../../Components";
import styles from "./styles";
import { GoToast } from "../../../../../Components";

const { width, height } = Dimensions.get("window");

class SearchScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isSearching: false,
			locationModule: false
		};
	}

	callAutoComplete = text => {
		if (text.length > 2) {
			if (!this.state.isSearching) {
				this.setState({ isSearching: true });
			}
			this.props.dispatch(
				getAutoComplete({
					appToken: this.props.appToken,
					query: text
				})
			);
		} else {
			if (this.state.isSearching) {
				this.setState({ isSearching: false });
			}
		}
	};

	getCities = () => {
		if (this.props.searchQuery instanceof Array) {
			return this.props.searchQuery;
		} else {
			return this.props.searchQuery.ByCity;
		}
	};

	getHotels = () => {
		let hotel = [];
		if (this.props.searchQuery instanceof Array) {
			return hotel;
		} else {
			return this.props.searchQuery.ByHotels;
		}
	};

	fetchCurrentLocation = data => {
		if (data && data.latitude !== null) {
			this.props.dispatch(saveFetchLocation(true));
			this.props.dispatch(
				getCurrentLocation({
					latitude: data.latitude,
					longitude: data.longitude
				})
			);
		} else {
			this.setState({ locationModule: true });
		}
	};

	shouldComponentUpdate(props, state) {
		if (
			props.isFetchingLocation === false &&
			props.isFetchingLocation !== this.props.isFetchingLocation
		) {
			this.props.navigation.goBack();
		}
		return true;
	}
	componentWillUnmount() {
		this.props.dispatch(saveAutoComplete(null));
	}
	render() {
		return (
			<View style={styles.container}>
				<StatusBar backgroundColor="#535353" barStyle="light-content" />
				{this.state.locationModule && (
					<LocationModule
						onLocationReceived={this.fetchCurrentLocation}
						onError={() => {
							this.setState({ locationModule: false });
							GoToast.show(
								I18n.t("no_location_message"),
								I18n.t("information")
							);
						}}
					/>
				)}
				{this.props.isFetchingLocation && (
					<CTProgressScreen
						indicatorSize={"large"}
						message={"fetching current location"}
					/>
				)}
				<View style={styles.header}>
					<View style={styles.headerContainer}>
						<TouchableOpacity
							style={styles.backButton}
							onPress={() => this.props.navigation.goBack()}>
							<Icon
								iconType={"material"}
								iconSize={height / 26}
								iconName={"arrow-back"}
								iconColor={"#ffffff"}
							/>
						</TouchableOpacity>
						<TextInput
							style={styles.textInput}
							autoFocus={true}
							placeholder={I18n.t("search_city_hotel")}
							placeholderTextColor={"#D9D8D8"}
							value={this.state.text}
							onChangeText={text => this.callAutoComplete(text)}
							underlineColorAndroid={"transparent"}
							selectionColor={"#D9D8D8"}
						/>
					</View>
				</View>
				{!this.state.isSearching && (
					<TouchableOpacity
						style={styles.locationBar}
						onPress={() => this.fetchCurrentLocation()}>
						<Icon
							iconType={"ionicon"}
							iconSize={height / 26}
							iconName={"ios-locate"}
							iconColor={"#777777"}
						/>
						<CtHotelTextRegular style={styles.locationText}>
							{I18n.t("current_location")}
						</CtHotelTextRegular>
					</TouchableOpacity>
				)}
				{this.props.searchQuery !== null && (
					<ScrollView
						style={styles.scrollViewContainer}
						keyboardShouldPersistTaps={"always"}>
						{this.getCities().length > 0 && (
							<View>
								<View style={styles.listHeader}>
									<CtHotelTextRegular style={styles.listHeaderText}>
										{I18n.t("cities")}
									</CtHotelTextRegular>
								</View>
								<FlatList
									keyboardShouldPersistTaps={"always"}
									data={this.getCities()}
									extraData={this.state}
									renderItem={({ item, index }) => {
										return (
											<TouchableOpacity
												style={styles.touchBar}
												onPress={() => {
													Keyboard.dismiss();
													let place = item.split(",");
													this.props.dispatch(
														saveSearchType({
															type: true,
															value: {
																city: place[0],
																state: place[1],
																country: place[2]
															}
														})
													);
													this.props.navigation.goBack();
												}}>
												<Icon
													iconType={"material"}
													iconSize={height / 26}
													iconName={"location-city"}
													iconColor={"rgba(0,0,0,0.5)"}
												/>
												<CtHotelTextRegular style={styles.listText}>
													{item}
												</CtHotelTextRegular>
											</TouchableOpacity>
										);
									}}
								/>
							</View>
						)}
						{this.getHotels().length > 0 && (
							<View>
								<View style={styles.listHeader}>
									<CtHotelTextBold style={styles.listHeaderText}>
										{I18n.t("hotels")}
									</CtHotelTextBold>
								</View>
								<FlatList
									style={styles.flatListContainer}
									keyboardShouldPersistTaps={"always"}
									data={this.getHotels()}
									extraData={this.state}
									renderItem={({ item, index }) => {
										return (
											<TouchableOpacity
												style={styles.touchBar}
												onPress={() => {
													Keyboard.dismiss();
													this.props.dispatch(
														saveSearchType({
															type: false,
															value: item
														})
													);
													this.props.navigation.goBack();
												}}>
												<Icon
													iconType={"font_awesome"}
													iconSize={height / 26}
													iconName={"hotel"}
													iconColor={"rgba(0,0,0,0.5)"}
												/>
												<CtHotelTextRegular style={styles.listText}>
													{item.name + ", " + item.city}
												</CtHotelTextRegular>
											</TouchableOpacity>
										);
									}}
								/>
							</View>
						)}
					</ScrollView>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		currentLocaton:
			state.cleartripHotel && state.cleartripHotel.splash.currentLocaton,
		locationError: state.cleartripHotel && state.cleartripHotel.splash.error,
		isFetchingLocation:
			state.cleartripHotel && state.cleartripHotel.splash.isFetchingLocation,
		searchType:
			state.cleartripHotel && state.cleartripHotel.searchHotel.searchType,
		searchQuery:
			state.cleartripHotel && state.cleartripHotel.searchHotel.searchQuery,
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		location: state.location
	};
}

export default connect(mapStateToProps)(SearchScreen);
