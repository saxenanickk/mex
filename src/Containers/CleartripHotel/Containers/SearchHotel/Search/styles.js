import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: { flex: 1, backgroundColor: "#F0F0F0" },
	headerContainer: {
		flexDirection: "row",
		alignItems: "center"
	},
	flatListContainer: { height: height },
	scrollViewContainer: { flex: 1 },
	listText: {
		fontSize: height / 45,
		color: "#000",
		marginLeft: width / 30
	},
	listHeader: {
		width: width,
		height: height / 14,
		justifyContent: "center",
		paddingHorizontal: width / 30,
		marginTop: height / 80
	},
	listHeaderText: {
		fontSize: height / 50
	},
	touchBar: {
		width: width,
		height: height / 15,
		backgroundColor: "#fff",
		flexDirection: "row",
		alignItems: "center",
		paddingHorizontal: width / 30,
		justifyContent: "flex-start",
		borderBottomColor: "#DFDDE0",
		borderBottomWidth: 1
	},
	header: {
		width: width,
		elevation: 8,
		backgroundColor: "#777777",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		justifyContent: "space-between"
	},
	backButton: {
		justifyContent: "center",
		width: width / 6,
		paddingLeft: width / 26.3,
		height: height / 13.33
	},
	locationText: {
		fontSize: height / 45.7,
		color: "#000",
		marginLeft: width / 30
	},
	locationBar: {
		width: width,
		height: height / 15,
		flexDirection: "row",
		alignItems: "center",
		paddingHorizontal: width / 26.3,
		backgroundColor: "#fff"
	},
	textInput: {
		height: height / 13.33,
		width: (4.2 * width) / 6,
		backgroundColor: "transparent",
		color: "#D9D8D8",
		fontSize: height / 45.7
	},
	addButton: {
		position: "absolute",
		top: height / 1.15,
		right: width / 15,
		width: width / 8,
		height: width / 8,
		borderRadius: width / 16,
		backgroundColor: "#3366cc",
		justifyContent: "center",
		alignItems: "center",
		elevation: 8
	},
	increment: {
		width: width / 25,
		height: width / 25,
		borderRadius: width / 50,
		backgroundColor: "#777777",
		justifyContent: "center",
		alignItems: "center"
	},
	bar: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingVertical: width / 30,
		paddingHorizontal: width / 30,
		backgroundColor: "#fff",
		borderBottomColor: "#DFDDE0",
		borderBottomWidth: 0.3,
		alignItems: "center"
	},
	text: {
		fontSize: height / 45
	}
});

export default styles;
