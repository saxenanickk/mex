import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	header: {
		width: width,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		backgroundColor: "#3366cc",
		justifyContent: "space-between",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33
	},
	supportTextButton: {
		backgroundColor: "#ffffff",
		flexDirection: "row",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		paddingVertical: height / 90,
		borderRadius: width / 20,
		justifyContent: "flex-start",
		paddingHorizontal: width / 50,
		alignItems: "center",
		marginRight: width / 26.3
	},
	supportText: {
		color: "#3366cc",
		width: width / 7
	},
	headerText: {
		color: "#ffffff",
		fontSize: height / 45.71,
		marginLeft: width / 26.3
	},
	destinationButton: {
		width: width,
		paddingVertical: height / 35,
		paddingHorizontal: width / 27,
		borderBottomWidth: 0.3,
		borderBottomColor: "#DFDDE0"
	},
	textStyle: {
		color: "#000"
	},
	dateTime: {
		flexDirection: "row",
		paddingHorizontal: width / 27,
		justifyContent: "space-between",
		alignItems: "center",
		height: height / 13,
		borderBottomWidth: 0.3,
		borderBottomColor: "#DFDDE0"
	},
	bar: {
		width: width,
		paddingVertical: height / 35,
		paddingHorizontal: width / 27,
		borderBottomWidth: 0.3,
		borderBottomColor: "#DFDDE0"
	},
	search: {
		width: width / 1.08,
		paddingVertical: width / 30,
		backgroundColor: "#EC6733",
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center"
	},
	cross: {
		width: width / 20,
		height: width / 20,
		borderRadius: width / 40,
		backgroundColor: "#777777",
		justifyContent: "center",
		alignItems: "center"
	},
	secondaryTextStyle: {
		fontSize: height / 40,
		fontWeight: "bold"
	},
	calendar: {
		width: width / 1.17,
		borderRightColor: "#DFDDE0",
		borderRightWidth: 0.3,
		height: height / 13,
		justifyContent: "center"
	},
	buttonText: {
		fontSize: height / 55,
		fontWeight: "bold",
		color: "#fff",
		elevation: 10
	}
});

export default styles;
