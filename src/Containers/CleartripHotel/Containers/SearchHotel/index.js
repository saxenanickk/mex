import React, { Component } from "react";
import {
	View,
	Dimensions,
	StatusBar,
	TouchableOpacity,
	NativeModules,
	LayoutAnimation,
	Platform
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import {
	CtHotelTextRegular,
	CtHotelTextMedium,
	CtHotelTextBold
} from "../../Components";
import { connect } from "react-redux";
import { Icon, DatePicker, HomeBack } from "../../../../Components";
import {
	saveStayDate,
	savePersonalInfo,
	getSearchHotel,
	saveIsAnyTime
} from "./Saga";
import { ONE_DAY_EPOCH_DURATION, EPOCH_ONE_YEAR_DURATION } from "../../Util";
import Freshchat from "../../../../CustomModules/Freshchat";
import I18n from "../../Assets/Strings/i18n";
import styles from "./styles";
import { GoToast } from "../../../../Components";

const { width, height } = Dimensions.get("window");
const { UIManager } = NativeModules;

if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}
class SearchHotel extends Component {
	checkIn = null;
	checkOut = null;
	constructor(props) {
		super(props);
		this.state = {
			personal: props.personalInfo.slice(),
			showCheckinDatePicker: false,
			showCheckoutDatePicker: false
		};
	}

	componentWillUnmount() {
		console.log("props are==>", this.props);
		AsyncStorage.setItem(
			"cleartripHotel",
			JSON.stringify({
				stayDate: this.props.stayDate,
				personalInfo: this.props.personalInfo,
				searchType: this.props.searchType
			})
		);
	}

	onDateSelected = (checkIn, checkOut) => {
		this.props.dispatch(
			saveStayDate({
				checkInDate: checkIn,
				checkOutDate: checkOut
			})
		);
	};

	getDateToDisplay = (checkInDate, checkOutDate) => {
		console.log("=====> ", checkInDate, checkOutDate);
		let dateToDisplay =
			I18n.l(
				"date.formats.normal_day",
				checkInDate ? checkInDate : new Date()
			) +
			" to " +
			I18n.l(
				"date.formats.normal_day",
				checkOutDate
					? checkOutDate
					: new Date(new Date().getTime() + ONE_DAY_EPOCH_DURATION)
			);
		return dateToDisplay;
	};

	getTravellerToDisplay = () => {
		let detail =
			this.props.personalInfo.length +
			" " +
			I18n.t("room", { count: this.props.personalInfo.length }) +
			", ";
		let adultCount = 0;
		let childCount = 0;
		this.props.personalInfo.map(item => {
			adultCount += item.adult;
			childCount += item.child.length;
		});
		detail += adultCount + " " + I18n.t("adult", { count: adultCount });
		if (childCount > 0) {
			detail +=
				", " + childCount + "  " + I18n.t("child", { count: childCount });
		}
		return detail;
	};

	saveTravellerDetail = personalInfo => {
		this.props.dispatch(savePersonalInfo(personalInfo));
	};

	getDateAsParameter = date => {
		try {
			let month = date.getMonth() + 1;
			let day = date.getDate();
			month = month < 10 ? "0" + month : month;
			day = day < 10 ? "0" + day : day;
			return date.getYear() + 1900 + "-" + month + "-" + day;
		} catch (error) {
			date = new Date();
			let month = date.getMonth() + 1;
			let day = date.getDate();
			month = month < 10 ? "0" + month : month;
			day = day < 10 ? "0" + day : day;
			return date.getYear() + 1900 + "-" + month + "-" + day;
		}
	};

	callSearchHotel = () => {
		let now = new Date();
		let params = {
			checkInDate: this.props.isAnyTime
				? this.getDateAsParameter(now)
				: this.getDateAsParameter(this.props.stayDate.checkInDate),
			checkOutDate: this.props.isAnyTime
				? this.getDateAsParameter(
						new Date(now.getTime() + ONE_DAY_EPOCH_DURATION)
				  )
				: this.getDateAsParameter(this.props.stayDate.checkOutDate),
			appToken: this.props.appToken,
			noOfRooms: this.props.personalInfo.length,
			PERSONALINFO: this.props.personalInfo,
			city: this.props.searchType.value.city,
			state: this.props.searchType.value.state,
			country: "IN"
		};
		this.props.dispatch(getSearchHotel(params));
		this.props.navigation.navigate("SelectHotel");
	};

	searchHotels = () => {
		if (this.props.searchType === null) {
			GoToast.show(I18n.t("select_city_hotel"), I18n.t("information"));
		} else {
			if (this.props.searchType.type) {
				this.callSearchHotel();
			} else {
				this.props.navigation.navigate("HotelDetail", {
					hotelId: this.props.searchType.value.hotel_id
				});
			}
		}
	};
	getDestination = () => {
		if (this.props.searchType.type) {
			return this.props.searchType.value.city;
		} else {
			return this.props.searchType.value.name;
		}
	};

	render() {
		return (
			<View>
				<StatusBar backgroundColor="#1f3d7a" barStyle="light-content" />
				<View style={styles.header}>
					<HomeBack
						navigation={this.props.navigation}
						headerText={I18n.t("search_hotel")}
						headerTextStyle={styles.headerText}
					/>
					<TouchableOpacity
						style={styles.supportTextButton}
						onPress={() => Freshchat.launchSupportChat()}>
						<CtHotelTextMedium style={styles.supportText} numberOfLines={1}>
							{I18n.t("app_support")}
						</CtHotelTextMedium>
						<Icon
							iconType={"material"}
							iconSize={height / 40}
							iconName={"headset"}
							iconColor={"#3366cc"}
						/>
					</TouchableOpacity>
				</View>
				{/* body section*/}
				<View>
					<TouchableOpacity
						style={styles.destinationButton}
						onPress={() => this.props.navigation.navigate("SearchPlace")}>
						<CtHotelTextRegular
							style={[
								styles.textStyle,
								{ fontSize: height / 65, color: "#818081" }
							]}>
							{this.props.searchType === null
								? I18n.t("pick_city_area_hotel")
								: I18n.t("destination_hotel")}
						</CtHotelTextRegular>
						<CtHotelTextRegular
							style={[
								styles.textStyle,
								{ fontSize: height / 32, fontWeight: "bold" }
							]}>
							{this.props.searchType === null ? "..." : this.getDestination()}
						</CtHotelTextRegular>
					</TouchableOpacity>
					<View>
						{this.props.isAnyTime === false ? (
							<View style={styles.dateTime}>
								<TouchableOpacity
									style={styles.calendar}
									onPress={() =>
										!this.state.showCheckinDatePicker &&
										!this.state.showCheckoutDatePicker &&
										this.setState({ showCheckinDatePicker: true })
									}>
									<CtHotelTextRegular
										style={[
											styles.textStyle,
											{ fontSize: height / 65, color: "#818081" }
										]}>
										{I18n.t("travel_dates")}
									</CtHotelTextRegular>
									<CtHotelTextBold
										style={[styles.textStyle, styles.secondaryTextStyle]}>
										{this.getDateToDisplay(
											this.props.stayDate.checkInDate,
											this.props.stayDate.checkOutDate
										)}
									</CtHotelTextBold>
								</TouchableOpacity>
								<TouchableOpacity
									style={[styles.cross]}
									onPress={() => {
										LayoutAnimation.easeInEaseOut();
										this.setState({
											showCheckinDatePicker: false,
											showCheckoutDatePicker: false
										});
										this.props.dispatch(saveIsAnyTime(true));
									}}>
									<Icon
										iconType={"ionicon"}
										iconSize={width / 30}
										iconName={"md-close"}
										iconColor={"#ffffff"}
									/>
								</TouchableOpacity>
							</View>
						) : (
							<TouchableOpacity
								style={styles.dateTime}
								onPress={() => {
									LayoutAnimation.easeInEaseOut();
									this.props.dispatch(saveIsAnyTime(false));
								}}>
								<CtHotelTextBold
									style={[styles.textStyle, styles.secondaryTextStyle]}>
									{I18n.t("anytime")}
								</CtHotelTextBold>
							</TouchableOpacity>
						)}
					</View>
					<TouchableOpacity
						style={styles.bar}
						onPress={() =>
							this.props.navigation.navigate("AddTraveller", {
								saveTravellerDetail: this.saveTravellerDetail
							})
						}>
						<CtHotelTextRegular
							style={[
								styles.textStyle,
								{ fontSize: height / 65, color: "#818081" }
							]}>
							{I18n.t("travellers")}
						</CtHotelTextRegular>
						<CtHotelTextBold
							style={[styles.textStyle, styles.secondaryTextStyle]}>
							{this.getTravellerToDisplay()}
						</CtHotelTextBold>
					</TouchableOpacity>
					<View style={styles.bar}>
						<TouchableOpacity
							style={styles.search}
							onPress={() => this.searchHotels()}>
							<CtHotelTextRegular style={styles.buttonText}>
								{I18n.currentLocale() === "en-US"
									? I18n.t("search_hotel").toUpperCase()
									: I18n.t("search_hotel")}
							</CtHotelTextRegular>
						</TouchableOpacity>
					</View>
				</View>
				<View
					style={{
						position: "absolute",
						top: 0,
						bottom:
							this.state.showCheckinDatePicker ||
							this.state.showCheckoutDatePicker
								? 0
								: height,
						left: 0,
						right: 0,
						backgroundColor: "transparent"
					}}
				/>
				{this.state.showCheckinDatePicker ? (
					<DatePicker
						date={new Date()}
						minimumDate={new Date()}
						onDateChange={date => {
							console.log(date);
							if (date !== null) {
								let month = parseInt(date.month) + 1;
								// @ts-ignore
								month = month < 10 ? "0" + month : month;
								let day = date.day < 10 ? "0" + date.day : date.day;
								let dateString = date.year + "/" + month + "/" + day;
								this.CHECK_IN_DATE = new Date(dateString);
								this.setState({
									showCheckinDatePicker: false,
									showCheckoutDatePicker: true
								});
							} else {
								this.setState({
									showCheckinDatePicker: false
								});
							}
						}}
						maximumDate={
							new Date(
								new Date().getTime() +
									EPOCH_ONE_YEAR_DURATION -
									ONE_DAY_EPOCH_DURATION
							)
						}
					/>
				) : null}
				{this.state.showCheckoutDatePicker ? (
					<DatePicker
						date={
							new Date(this.CHECK_IN_DATE.getTime() + ONE_DAY_EPOCH_DURATION)
						}
						minimumDate={
							new Date(this.CHECK_IN_DATE.getTime() + ONE_DAY_EPOCH_DURATION)
						}
						onDateChange={date => {
							console.log(date);
							if (date !== null) {
								let month = parseInt(date.month) + 1;
								// @ts-ignore
								month = month < 10 ? "0" + month : month;
								let day = date.day < 10 ? "0" + date.day : date.day;
								let dateString = date.year + "/" + month + "/" + day;
								this.CHECK_OUT_DATE = new Date(dateString);
								this.setState({ showCheckoutDatePicker: false });
								this.onDateSelected(this.CHECK_IN_DATE, this.CHECK_OUT_DATE);
							} else {
								this.setState({
									showCheckinDatePicker: false,
									showCheckoutDatePicker: false
								});
							}
						}}
						maximumDate={
							new Date(new Date().getTime() + EPOCH_ONE_YEAR_DURATION)
						}
					/>
				) : null}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		stayDate: state.cleartripHotel && state.cleartripHotel.searchHotel.stayDate,
		personalInfo:
			state.cleartripHotel && state.cleartripHotel.searchHotel.personalInfo,
		searchType:
			state.cleartripHotel && state.cleartripHotel.searchHotel.searchType,
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		isAnyTime:
			state.cleartripHotel && state.cleartripHotel.searchHotel.isAnyTime
	};
}

export default connect(mapStateToProps)(SearchHotel);
