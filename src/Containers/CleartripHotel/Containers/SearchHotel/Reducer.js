/* eslint-disable radix */
import {
	CTHOTEL_SAVE_PERSONAL_INFORMATION,
	CTHOTEL_SAVE_STAY_DATE,
	CTHOTEL_SAVE_SEARCH_TYPE,
	CTHOTEL_SAVE_SEARCH_HOTEL,
	CTHOTEL_SAVE_SERVER_ERROR,
	CTHOTEL_SAVE_IS_ANY_TIME,
	CTHOTEL_NO_HOTEL_ERROR,
	CTHOTEL_SAVE_AUTO_COMPLETE,
	CTHOTEL_FILTER_HOTEL,
	CTHOTEL_SORT_HOTEL
} from "./Saga";

const initialState = {
	stayDate: {
		checkInDate: null,
		checkOutDate: null
	},
	personalInfo: [
		{
			adult: 2,
			child: []
		}
	],
	searchType: null,
	hotels: null,
	hotelsDefault: null,
	error: null,
	isAnyTime: false,
	noHotelError: false,
	searchQuery: null
};

const sortHotel = (hotels, sortType) => {
	switch (sortType) {
		case 0:
			return hotels;
		case 1:
			return hotels.sort((first, second) => first.price - second.price);
		case 2:
			return hotels.sort((first, second) => second.price - first.price);
		default:
			return hotels;
	}
};

// retrieve all amenities of a hotel
const getHotelAmenities = hotel => {
	let amenitiesArray = [];
	let resultedArray = [];
	if (
		hotel["basic-info"] &&
		hotel["basic-info"]["hotel-info:hotel-amenities"] &&
		hotel["basic-info"]["hotel-info:hotel-amenities"][
			"hotel-info:hotel-amenity"
		]
	) {
		if (
			hotel["basic-info"]["hotel-info:hotel-amenities"][
				"hotel-info:hotel-amenity"
			] instanceof Array
		) {
			amenitiesArray =
				hotel["basic-info"]["hotel-info:hotel-amenities"][
					"hotel-info:hotel-amenity"
				];
		} else {
			amenitiesArray.push(
				hotel["basic-info"]["hotel-info:hotel-amenities"][
					"hotel-info:hotel-amenity"
				]
			);
		}
	}
	amenitiesArray.map(amenity => {
		if (
			amenity["hotel-info:amenities"] &&
			amenity["hotel-info:amenities"]["hotel-info:amenity"]
		) {
			if (
				amenity["hotel-info:amenities"]["hotel-info:amenity"] instanceof Array
			) {
				resultedArray = resultedArray.concat(
					amenity["hotel-info:amenities"]["hotel-info:amenity"]
				);
			} else {
				resultedArray.push(
					amenity["hotel-info:amenities"]["hotel-info:amenity"]
				);
			}
		}
	});
	return resultedArray;
};
export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case CTHOTEL_SAVE_STAY_DATE:
			return {
				...state,
				stayDate: action.payload
			};
		case CTHOTEL_SAVE_PERSONAL_INFORMATION:
			return {
				...state,
				personalInfo: action.payload
			};
		case CTHOTEL_SAVE_SEARCH_TYPE:
			return {
				...state,
				searchType: action.payload
			};
		case CTHOTEL_SAVE_SEARCH_HOTEL:
			filter_hotel =
				action.payload !== null
					? action.payload.map(row => {
							let obj = row;
							let room = row["room-rates"]["room-rate"];
							let room_price = null;
							if (room instanceof Array) {
								room_price = room[0];
								obj["post-pay"] =
									room.filter(row => row["post-pay"] === true).length > 0;
							} else {
								room_price = room;
								obj["post-pay"] = room["post-pay"] === "true";
							}
							let rateElement = room_price["rate-breakdown"]["common:rate"];
							let price = 0;
							let tax = 0;
							let baseFare = 0;
							let discount = 0;
							if (rateElement instanceof Array) {
								rateElement.map((rates, index) => {
									let pricingElement =
										rates["common:pricing-elements"]["common:pricing-element"];
									if (pricingElement instanceof Array) {
										pricingElement.map(cost => {
											price += cost["common:amount"];
											if (cost["common:category"] === "TAX") {
												tax = cost["common:amount"];
											}
											if (cost["common:category"] === "BF") {
												baseFare = cost["common:amount"];
											}
											if (cost["common:category"] === "DIS" && index === 0) {
												discount += Math.abs(cost["common:amount"]);
											}
										});
									} else {
										price += pricingElement["common:amount"];
										baseFare = pricingElement["common:amount"];
									}
								});
							} else {
								let pricingElement =
									rateElement["common:pricing-elements"][
										"common:pricing-element"
									];
								if (pricingElement instanceof Array) {
									pricingElement.map((cost, index) => {
										price += cost["common:amount"];
										if (cost["common:category"] === "TAX") {
											tax = cost["common:amount"];
										}
										if (cost["common:category"] === "BF") {
											baseFare = cost["common:amount"];
										}
										if (cost["common:category"] === "DIS") {
											discount += Math.abs(cost["common:amount"]);
										}
									});
								} else {
									price += pricingElement["common:amount"];
									baseFare = pricingElement["common:amount"];
								}
							}
							obj.price = parseInt(Math.round(price));
							obj.tax = parseInt(Math.round(tax));
							obj.baseFare = parseInt(Math.round(baseFare));
							obj.discount = parseInt(Math.round(discount));
							obj.amenity = getHotelAmenities(row);
							return obj;
					  })
					: null;
			return {
				...state,
				hotels: filter_hotel,
				hotelsDefault: filter_hotel
			};
		case CTHOTEL_SAVE_SERVER_ERROR:
			return {
				...state,
				error: action.payload
			};
		case CTHOTEL_SAVE_IS_ANY_TIME:
			return {
				...state,
				isAnyTime: action.payload
			};
		case CTHOTEL_NO_HOTEL_ERROR:
			return {
				...state,
				noHotelError: action.payload
			};
		case CTHOTEL_SAVE_AUTO_COMPLETE:
			return {
				...state,
				searchQuery: action.payload
			};
		case CTHOTEL_SAVE_SEARCH_TYPE:
			return {
				...state,
				searchType: action.payload
			};
		case CTHOTEL_FILTER_HOTEL:
			let filter_hotel = [];
			if (action.payload.isFiltered) {
				// filtering according to price
				filter_hotel = state.hotelsDefault.slice().filter(hotel => {
					return (
						hotel.price >= action.payload.filterParameters.minPrice &&
						hotel.price <= action.payload.filterParameters.maxPrice
					);
				});

				// filtering according to star rating
				filter_hotel = filter_hotel.filter(hotel => {
					if (action.payload.filterParameters.starRating.length === 0) {
						return true;
					} else {
						return (
							action.payload.filterParameters.starRating.indexOf(
								parseInt(hotel["basic-info"]["hotel-info:star-rating"])
							) > -1
						);
					}
				});

				// filter according to post-pay
				filter_hotel = filter_hotel.filter(hotel => {
					if (action.payload.filterParameters.hotelPay === false) {
						return true;
					} else {
						return (
							hotel["post-pay"] === action.payload.filterParameters.hotelPay
						);
					}
				});
				//filter hotel according to facilities
				action.payload.filterParameters.facilities.map(facility => {
					if (facility.isChecked === true) {
						filter_hotel = filter_hotel.filter(
							hotel => hotel.amenity.indexOf(facility.type) > -1
						);
					}
				});
				filter_hotel = sortHotel(filter_hotel, action.payload.sortType);
			} else {
				filter_hotel = sortHotel(
					state.hotelsDefault.slice(),
					action.payload.sortType
				);
			}
			return {
				...state,
				hotels: filter_hotel
			};
		case CTHOTEL_SORT_HOTEL:
			filter_hotel = sortHotel(
				action.payload.hotels.slice(),
				action.payload.sortType
			);
			return {
				...state,
				hotels: filter_hotel
			};
		default:
			return state;
	}
};
