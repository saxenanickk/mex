import { all } from "redux-saga/effects";
import { searchHotelSaga } from "./Containers/SearchHotel/Saga";
import { splashSaga } from "./Containers/Splash/Saga";
import { selectHotelSaga } from "./Containers/SelectHotel/Saga";
import { selectRoomSaga } from "./Containers/SelectRoom/Saga";
import { bookHotelSaga } from "./Containers/BookHotel/Saga";
import { tripDetailSaga } from "./Containers/TripDetail/Saga";

export default function* clearTripHotelSaga() {
	yield all([
		searchHotelSaga(),
		splashSaga(),
		selectHotelSaga(),
		selectRoomSaga(),
		bookHotelSaga(),
		tripDetailSaga()
	]);
}
