import React from "react";
import {
	View,
	ImageBackground,
	Dimensions,
	TouchableOpacity
} from "react-native";
import { CLEARTRIP_SERVER_IMAGE_URL } from "../../Api/";
import { EMPTYIMAGE } from "../../Assets/Img/Image";
import { CtHotelTextRegular } from "../CTHotelText";
import styles from "./styles";

const { width, height } = Dimensions.get("window");

export const ImageCart = props => {
	return (
		<View style={styles.container}>
			{props.image.slice(0, 5).map((item, index) => {
				return (
					<View key={index}>
						<TouchableOpacity onPress={props.onImageSelect}>
							<ImageBackground
								loadingIndicatorSource={EMPTYIMAGE}
								source={{
									uri:
										CLEARTRIP_SERVER_IMAGE_URL +
										(index === 0
											? item["wide-angle-image-url"]
											: item["thumbnail-image-url"])
								}}
								style={{
									marginBottom: index === 0 ? width / 80 : 0,
									width: index === 0 ? width : width / 4.05,
									height: index === 0 ? height / 3.7 : height / 8,
									justifyContent: "center"
								}}
								resizeMode={"cover"}>
								{index === 4 && (
									<View style={styles.moreImagesContainer}>
										<CtHotelTextRegular style={styles.moreImagesText}>
											{"+" + (props.image.length - 5)}
										</CtHotelTextRegular>
									</View>
								)}
							</ImageBackground>
						</TouchableOpacity>
					</View>
				);
			})}
		</View>
	);
};
