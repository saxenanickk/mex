import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: {
		flexDirection: "row",
		flexWrap: "wrap",
		justifyContent: "space-between",
		marginTop: width / 30
	},
	moreImagesContainer: {
		width: width / 4.05,
		height: height / 8,
		backgroundColor: "rgba(0,0,0,0.7)",
		justifyContent: "center",
		alignItems: "center"
	},
	moreImagesText: {
		fontSize: height / 45,
		color: "#fff"
	}
});

export default styles;
