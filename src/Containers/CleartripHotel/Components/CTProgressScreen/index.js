import React from "react";
import { View, Text, ActivityIndicator } from "react-native";
import styles from "./styles";

const CTProgressScreen = props => {
	return (
		<View style={[styles.container, props.progressScreen]}>
			<View style={[styles.messageBar, props.messageBar]}>
				<ActivityIndicator
					color={props.indicatorColor}
					size={props.indicatorSize}
				/>
				<View style={styles.textArea}>
					<Text style={styles.hangOnText}>{"Hang On"}</Text>
					<Text style={styles.messageText}>{props.message}</Text>
				</View>
			</View>
		</View>
	);
};

CTProgressScreen.defaultProps = {
	indicatorColor: "#3366cc",
	message: "Loading...",
	indicatorSize: "small"
};

export default CTProgressScreen;
