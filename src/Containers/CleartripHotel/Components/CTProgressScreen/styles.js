import { StyleSheet, Dimensions, Platform } from "react-native";
import { fontFamily, fontFamily_semi_bold } from "../../../../Assets/styles";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: {
		position: "absolute",
		width: width,
		height: height,
		backgroundColor: "#rgba(0,0,0,0.5)",
		elevation: 10,
		zIndex: 3,
		justifyContent: "center",
		alignItems: "center"
	},
	messageBar: {
		paddingHorizontal: width / 30,
		height: height / 10,
		backgroundColor: "#fff",
		justifyContent: "space-around",
		alignItems: "center",
		flexDirection: "row",
		borderRadius: width / 70,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	hangOnText: {
		fontFamily: fontFamily_semi_bold,
		color: "#000",
		fontSize: height / 45
	},
	messageText: {
		fontFamily: fontFamily,
		color: "rgba(0,0,0,0.6)",
		fontSize: height / 55
	},
	textArea: {
		marginLeft: width / 30
	}
});

export default styles;
