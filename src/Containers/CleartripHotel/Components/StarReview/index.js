import React from "react";
import { View, Dimensions } from "react-native";
import { Icon } from "../../../../Components";

const { width, height } = Dimensions.get("window");

export default class StarReview extends React.Component {
	constructor(props) {
		super(props);
		this.stars = [1, 2, 3, 4, 5];
	}
	render() {
		return (
			<View style={{ flexDirection: "row" }}>
				{this.stars.map((item, index) => {
					return (
						<View key={index}>
							<Icon
								iconType={"ionicon"}
								iconSize={height / 35}
								iconName={"ios-star"}
								iconColor={
									this.props.rating >= item
										? this.props.darkStar
										: this.props.lightStar
								}
							/>
						</View>
					);
				})}
			</View>
		);
	}
}

StarReview.defaultProps = {
	lightStar: "rgba(0,0,0,0.2)",
	darkStar: "#EC9715"
};
