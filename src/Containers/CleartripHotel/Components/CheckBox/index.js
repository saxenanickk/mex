import React from "react";
import { View } from "react-native";
import Icon from "../../../../Components/Icon";

const CheckBox = props => {
	return (
		<View>
			{props.value === true ? (
				<Icon
					iconType={"ionicon"}
					iconName={"ios-checkmark-circle"}
					iconSize={1.5 * props.width}
					iconColor={props.checkColor}
				/>
			) : (
				<Icon
					iconType={"font_awesome"}
					iconName={"circle-thin"}
					iconSize={1.4 * props.width}
					iconColor={"rgba(0,0,0,0.5)"}
				/>
			)}
		</View>
	);
};

export default CheckBox;
