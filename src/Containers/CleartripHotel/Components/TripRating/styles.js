import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	fullCircle: {
		width: width / 40,
		height: width / 40,
		borderRadius: width / 80,
		backgroundColor: "#51972F"
	},
	semiCircle: {
		width: width / 40,
		height: width / 40,
		borderLeftWidth: width / 70,
		borderRadius: width / 80,
		borderColor: "#51972F"
	},
	starContainer: {
		width: width / 25,
		height: width / 25,
		borderRadius: width / 50,
		borderWidth: width / 220,
		borderColor: "#51972F",
		justifyContent: "center",
		alignItems: "center"
	},
	container: { flexDirection: "row" }
});

export default styles;
