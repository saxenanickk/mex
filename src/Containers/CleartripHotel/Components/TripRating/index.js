import React from "react";
import { View, Dimensions } from "react-native";
import styles from "./styles";

export default class TripRating extends React.Component {
	constructor(props) {
		super(props);
		this.stars = [0, 1, 2, 3, 4];
	}
	render() {
		return (
			<View style={styles.container}>
				{this.stars.map((item, index) => {
					return (
						<View style={styles.starContainer} key={index}>
							<View
								style={
									this.props.rating - item > 0
										? this.props.rating - item === 0.5
											? styles.semiCircle
											: styles.fullCircle
										: null
								}
							/>
						</View>
					);
				})}
			</View>
		);
	}
}
