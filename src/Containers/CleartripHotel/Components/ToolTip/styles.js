import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

const styles = StyleSheet.create({
	messageContainer: {
		width: width / 2,
		backgroundColor: "rgba(0,0,0,0.6)",
		justifyContent: "center",
		paddingHorizontal: width / 40,
		paddingVertical: height / 140,
		borderRadius: width / 100,
		position: "absolute",
		elevation: 2
	},
	messageText: {
		textAlign: "center",
		color: "#fff",
		fontSize: height / 65
	}
});

export default styles;
