import React, { Component } from "react";
import { View, TouchableOpacity, Dimensions, Modal } from "react-native";
import { CtHotelTextLight } from "../CTHotelText";
import styles from "./styles";

const { width, height } = Dimensions.get("window");

export default class ToolTip extends Component {
	static defaultProps = {
		onRequestClose: () => {}
	};

	render() {
		const { props } = this;
		return (
			<Modal
				transparent={true}
				visible={props.visible}
				onRequestClose={() => props.onRequestClose}>
				<TouchableOpacity
					onPress={() => props.onRequestClose()}
					style={{ flex: 1 }}>
					<View
						style={[
							styles.messageContainer,
							{
								left: props.left - width / 8,
								top: height / 80 + props.right
							}
						]}>
						<CtHotelTextLight
							style={styles.messageText}
							allowFontScaling={false}>
							{props.message}
						</CtHotelTextLight>
					</View>
				</TouchableOpacity>
			</Modal>
		);
	}
}
