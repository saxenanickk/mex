import React, { Fragment } from "react";
import { SafeAreaView } from "react-native";
import RegisterScreen from "./RegisterScreen";
import GoAppAnalytics from "../../utils/GoAppAnalytics";

export default class App extends React.Component {
	render() {
		return (
			<Fragment>
				{/** For the Top Color (Under the Notch) */}
				<SafeAreaView style={{ flex: 0, backgroundColor: "#3366cc" }} />
				{/** For the Bottom Color (Below the home touch) */}
				<SafeAreaView style={{ flex: 1 }}>
					<RegisterScreen
						nav={this.props.nav}
						onNavigationStateChange={(prevState, currState) =>
							GoAppAnalytics.logChangeOfScreenInStack(
								"cleartriphotel",
								prevState,
								currState
							)
						}
					/>
				</SafeAreaView>
			</Fragment>
		);
	}
}
