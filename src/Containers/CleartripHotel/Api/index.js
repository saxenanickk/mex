import I18n from "i18n-js";
import Config from "react-native-config";
import LocationApi from "../../../CustomModules/LocationModule/Api";

const SERVER_BASE_URL_CLEARTRIP_HOTEL = Config.SERVER_BASE_URL_CLEARTRIP_HOTEL;

class CleartripHotelApi {
	getCurrentLocation(params) {
		return new Promise(function(resolve, reject) {
			try {
				LocationApi.reverseGeocoding({
					latitude: params.latitude,
					longitude: params.longitude
				})
					.then(data => {
						var location = data.results[0];
						console.log("location is ", data.results[0]);
						resolve(location);
					})
					.catch(err => {
						reject(err);
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	getHotels(params) {
		return new Promise(function(resolve, reject) {
			let APP_TOKEN = params.appToken;
			let child = "";
			let adult = "";
			let childrenAge = "";
			params.PERSONALINFO.forEach((element, index) => {
				if (index > 0) {
					child += ",";
					adult += ",";
				}
				adult += element.adult;
				child += element.child.length;
				element.child.forEach((childItem, childIndex) => {
					if (index === 0 && childIndex === 0) {
						childrenAge = "ChildAge=";
					}
					childrenAge += "ca" + (childIndex + 1) + "=" + childItem.age + "&";
				});
			});
			console.log("children age are=", childrenAge);
			try {
				fetch(
					SERVER_BASE_URL_CLEARTRIP_HOTEL +
						"/search" +
						"?CheckInDate=" +
						params.checkInDate +
						"&CheckOutDate=" +
						params.checkOutDate +
						"&NoOfRooms=" +
						params.noOfRooms +
						"&Adults=" +
						adult +
						"&Children=" +
						child +
						"&" +
						childrenAge +
						"City=" +
						params.city +
						"&State=" +
						params.state +
						"&Country=" +
						params.country,
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": APP_TOKEN,
							"Accept-Language": I18n.currentLocale()
						}
					}
				)
					.then(response => {
						console.log("response of search hotel is==>", response);
						response
							.json()
							.then(res => {
								console.log("response of search hotel is==>", res);
								resolve(res);
							})
							.catch(error => {
								reject(error);
							});
					})
					.catch(error => {
						reject(error);
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	getHotelInfoById(params) {
		const APP_TOKEN = params.appToken;

		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_CLEARTRIP_HOTEL + "/info/" + params.hotelId, {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": APP_TOKEN,
						"Accept-Language": I18n.currentLocale()
					}
				})
					.then(response => {
						response
							.json()
							.then(res => {
								console.log("response of search hotel By ID is==>", res);
								resolve(res);
							})
							.catch(error => {
								reject(error);
							});
					})
					.catch(error => {
						reject(error);
					});
			} catch (error) {
				console.log("Search Hotel By Id error:", error);
				reject(error);
			}
		});
	}

	getRateInfoOfHotel(params) {
		return new Promise(function(resolve, reject) {
			let APP_TOKEN = params.appToken;
			let child = "";
			let adult = "";
			let childrenAge = "";
			params.PERSONALINFO.forEach((element, index) => {
				if (index > 0) {
					child += ",";
					adult += ",";
				}
				adult += element.adult;
				child += element.child.length;
				element.child.forEach((childItem, childIndex) => {
					if (index === 0 && childIndex === 0) {
						childrenAge = "ChildAge=";
					}
					childrenAge += "ca" + (childIndex + 1) + "=" + childItem.age + "&";
				});
			});
			console.log("children age are=", childrenAge);
			try {
				fetch(
					SERVER_BASE_URL_CLEARTRIP_HOTEL +
						"/search" +
						"?CheckInDate=" +
						params.checkInDate +
						"&CheckOutDate=" +
						params.checkOutDate +
						"&NoOfRooms=" +
						params.noOfRooms +
						"&Adults=" +
						adult +
						"&Children=" +
						child +
						"&" +
						childrenAge +
						"City=" +
						params.city +
						"&State=" +
						params.state +
						"&Country=" +
						params.country +
						"&CtHotelId=" +
						params.hotelId,
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": APP_TOKEN
						}
					}
				)
					.then(response => {
						console.log("response of search hotel is==>", response);
						response
							.json()
							.then(res => {
								console.log("response of search hotel is==>", res);
								resolve(res);
							})
							.catch(error => {
								reject(error);
							});
					})
					.catch(error => {
						reject(error);
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	getHotelCancelPolicy(params) {
		console.log("params are", params);
		let APP_TOKEN = params.appToken;
		let Body = JSON.stringify({
			HotelId: params.hotelId,
			CheckInDate: params.checkInDate,
			CheckOutDate: params.checkOutDate,
			NoOfRooms: params.rooms,
			Adults: params.adults,
			Children: params.children,
			BookId: params.bookId,
			RoomType: params.roomType
		});
		console.log("body is", Body);
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_CLEARTRIP_HOTEL + "/policy", {
					method: "POST",
					body: Body,
					headers: {
						"X-ACCESS-TOKEN": APP_TOKEN,
						"Content-Type": "application/json",
						"Accept-Language": I18n.currentLocale()
					}
				})
					.then(response => {
						console.log("response of hotel cancel policy is==>", response);
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(error => {
								reject(error);
							});
					})
					.catch(error => {
						reject(error);
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	getOTP(params) {
		let APP_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_CLEARTRIP_HOTEL + "/sendOTP", {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": APP_TOKEN,
						"Content-Type": "application/json",
						"Accept-Language": I18n.currentLocale()
					}
				})
					.then(response => {
						console.log("response of hotel cancel policy is==>", response);
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(error => {
								reject(error);
							});
					})
					.catch(error => {
						reject(error);
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	getProvisionalBooking(params) {
		const APP_TOKEN = params.appToken;

		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_CLEARTRIP_HOTEL + "/provisionalbook", {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": APP_TOKEN,
						"Content-Type": "application/json",
						"Accept-Language": I18n.currentLocale()
					},
					body: JSON.stringify({
						HotelId: params.HotelId,
						CheckInDate: params.CheckInDate,
						CheckOutDate: params.CheckOutDate,
						NoOfRooms: params.NoOfRooms,
						Adults: params.Adults,
						Children: params.Children,
						ChildAge: params.ChildAge,
						BookId: params.BookId,
						RoomType: params.RoomType,
						Country: params.Country,
						Currency: "INR",
						_title: params._title,
						firstName: params.firstName,
						lastName: params.lastName,
						mobileNo: params.mobileNo,
						Email: params.Email,
						post_pay: params.post_pay,
						ItineraryId: params.ItineraryId,
						Otp: params.Otp
					})
				})
					.then(response => {
						console.log("response of hotel provisional book is==>", response);
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(error => {
								reject(error);
							});
					})
					.catch(error => {
						reject(error);
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	getHotelBooking(params) {
		const APP_TOKEN = params.appToken;

		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_CLEARTRIP_HOTEL +
						"/book?payment_id=" +
						params.paymentId,
					{
						method: "POST",
						headers: {
							"X-ACCESS-TOKEN": APP_TOKEN,
							"Content-Type": "application/json",
							"Accept-Language": I18n.currentLocale()
						},
						body: JSON.stringify({
							HotelId: params.HotelId,
							CheckInDate: params.CheckInDate,
							CheckOutDate: params.CheckOutDate,
							NoOfRooms: params.NoOfRooms,
							Adults: params.Adults,
							Children: params.Children,
							ChildAge: params.ChildAge,
							BookId: params.BookId,
							RoomType: params.RoomType,
							Country: params.Country,
							Currency: "INR",
							_title: params._title,
							firstName: params.firstName,
							lastName: params.lastName,
							mobileNo: params.mobileNo,
							Email: params.Email,
							ProvisionalId: params.ProvisionalId,
							Amount: params.Amount,
							PostPay: params.PostPay
						})
					}
				)
					.then(response => {
						console.log("response of hotel book is==>", response);
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(error => {
								reject(error);
							});
					})
					.catch(error => {
						reject(error);
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	getTrip(params) {
		const APP_TOKEN = params.appToken;

		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_CLEARTRIP_HOTEL + "/view/" + params.tripId, {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": APP_TOKEN,
						"Accept-Language": I18n.currentLocale()
					}
				})
					.then(response => {
						console.log("response of trip is==>", response);
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(error => {
								reject(error);
							});
					})
					.catch(error => {
						reject(error);
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	getAutocompleteQuery(params) {
		let APP_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_CLEARTRIP_HOTEL +
						"/autocomplete?query=" +
						params.query,
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": APP_TOKEN,
							"Accept-Language": I18n.currentLocale()
						}
					}
				)
					.then(response => {
						response
							.json()
							.then(res => {
								console.log("response of autocomplete is==>", res);
								resolve(res);
							})
							.catch(error => {
								reject(error);
							});
					})
					.catch(error => {
						reject(error);
					});
			} catch (error) {
				reject(error);
			}
		});
	}
}

export default new CleartripHotelApi();
