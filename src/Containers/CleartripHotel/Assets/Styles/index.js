import { Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const cleartripButton = {
	elevation: 2,
	borderRadius: 2,
	width: width / 1.1,
	alignSelf: "center",
	marginTop: height / 20,
	marginBottom: height / 20,
	justifyContent: "center",
	backgroundColor: "#EC6733"
};
