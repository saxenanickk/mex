import * as I18n from "../../../../Assets/strings/i18n";

export default {
	/**
	 * Cleartrip App Strings
	 */
	...I18n.default.translations.en,
	no_of_adult: "No. of adults",
	no_of_child: "No. of children",
	remove: "Remove",
	room_guests: "Room & Guests",
	rooms: "Rooms",
	//child: "Child",
	//adult: "Adult",
	children: "Children",
	adults: "Adults",
	age: "'s age",
	years: "years",
	search_hotel: "Search hotels",
	pick_city_area_hotel: "PICK A CITY, AREA OR HOTEL",
	destination_hotel: "DESTINATION",
	travel_dates: "TRAVEL DATES",
	anytime: "Anytime",
	travellers: "TRAVELLERS",
	search_city_hotel: "Search for a city or hotel",
	current_location: "Current Location",
	all_star: "All stars",
	star: "Star",
	stars: "Stars",
	and: "And",
	multiple_rating: "Multiple Ratings",
	error_message_while_booking: "Sorry, We were unable to confirm your booking.",
	amount_return_message_on_booking_fail:
		"Deducted amount will be returned to your bank account",
	error_mesage_while_searching:
		"Sorry, We were unable to get hotels for this search query",
	try_different_search: "Please try with different search options",
	filter_results: "Filter results",
	show_pay_at_hotel: "Show only pay@hotel",
	free_cancel_hotel: "Free cancellation hotels",
	total_price: "Total Price",
	to: "to",
	star_rating: "Star Rating",
	tripadvisor_rating: "Tripadvisor Rating",
	and_above: "and above",
	reset_all: "RESET ALL",
	apply: "APPLY",
	shortlisted: "Shortlisted",
	sort_by: "SORT BY",
	featured: "FEATURED",
	price: "PRICE",
	rating: "RATING",
	saving: "SAVING",
	price_low_to_high: "Price - Low to High",
	price_high_to_low: "Price - High to Low",
	ratings: "Ratings",
	savings: "Savings",
	shortlist_searchHotel_C: "SHORTLIST",
	user_rating: "user rating",
	cancellation_charges_apply: "Cancellation charges apply",
	modify_search: "Modify Search",
	total: "Total",
	bangalore_shortList: "Bangalore shortList",

	details: "(details)",
	continue_booking: "CONTINUE BOOKING",
	select_room: "SELECT ROOM",
	select_room_C: "SELECT ROOM",
	pick_dates_and_rooms: "Pick dates and rooms",
	//room: "Room",
	//night: "night",
	deal: "DEAL",
	pay_securely: "PAY SECURELY",
	bangalore: "Bangalore",
	done: "done",
	please_select_a_room: "Please select a room",
	select_your_room: "Select your room",
	cancellation_policy: "Cancellation Policy",
	shortlist: "Shortlist",
	check_availability: "Check Availability",
	select_check_in_date: "select check-in date",
	check_in: "CHECK-IN",
	hotels: " Hotels",
	by: "BY",
	taxes: "taxes",
	guests: "guests",
	star_hotel: "star hotel",
	reviews: "reviews",
	amenities_and_information: "Amenities and Information",
	floors: "Floors",
	check_out: "CHECK-OUT",
	contact_details: "Contact detail",
	for: "for",
	total_booking_amount: "Total Booking Amount",
	error_message_while_filter: "It looks like you have applied too many filter",
	try_different_filter: "Please modify your filter options",
	cancelled: "Cancelled",
	go_back_without_trip_details:
		"Are you sure to go Back, without fetching trip details",
	cancel: "Cancel",
	ok: "Ok",
	unable_to_book_hotel: "Unable to book hotel now,try again later",
	payment_cancelled_by_user:
		"Payment cancelled by user, need to create the itinerary again",
	warning: "Warning",
	cancel_after_payment:
		"You have made the payment and booking is in progress, if you cancel now your progress will be lost.",
	cancel_before_payment:
		"If you go back, your booking will be cancelled and you have to create itinerary again",
	getting_provisional_book: "getting provisional book",
	booking_hotel: "Booking hotel",
	try_again: "Please try again",
	mr: "Mr",
	ms: "Ms",
	mrs: "Mrs",
	nights: "nights",
	non_refundable: "Charges on Cancellation",
	hotel_facilities: "Hotel facilities",
	room: {
		one: "room",
		other: "rooms"
	},
	adult: {
		one: "adult",
		other: "adults"
	},
	child: {
		one: "child",
		other: "children",
		zero: ""
	},
	night: {
		one: "night",
		other: "nights"
	},
	go_home: "Go To Home",
	room_C: "ROOM",
	from: "from",
	view_full_map: "view full map",
	more: "more",
	show_more: "Show More",
	location: "Location",
	close_C: "CLOSE",
	cities: "Cities",
	trip_id: "Trip ID : ",
	hotel_address: "Hotel Address",
	booking_details: "Booking Details",
	traveller: "Traveller",
	total_rooms: "Total Rooms",
	occupancy: "Occupancy",
	voucher_no: "Voucher No.",
	payment_details: "Payment Details",
	total_fare: "Total Fare",
	unable_to_fetch_detail: "Sorry, we were unable to fetch your trip detail",
	refresh: "REFRESH",
	unable_fetch_rate_message: "Unable to fetch Rate details, try later",
	select_city_hotel: "Please select a city or hotel",
	no_location_message: "Unable to find your location.",
	select_checkin_date: "select checkin date",
	select_checkout_date: "select checkout date",
	first_name: "First Name",
	last_name: "Last Name",
	email_id: "Email Id",
	ph_number: "Phone number",
	search_loc_hotel: "Search for location, hotel",
	cl_list: "LIST",
	cl_map: "MAP"
};
