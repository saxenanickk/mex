import I18n from "i18n-js";
import en from "./en";
import hi from "./hi";
import gu from "./gu";
import ja from "./ja";

I18n.translations = {
	en,
	hi,
	gu,
	ja
};

export default I18n;
