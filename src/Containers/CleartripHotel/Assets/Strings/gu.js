import * as I18n from "../../../../Assets/strings/i18n";

export default {
	/**
	 * Cleartrip App Strings
	 */
	...I18n.default.translations.gu,
	no_of_adult: "પુખ્તોની સંખ્યા",
	no_of_child: "બાળકોની સંખ્યા",
	remove: "દૂર કરો",
	room_guests: "રૂમ અને મહેમાનો",
	rooms: "રૂમ",
	children: "બાળકો",
	adults: "પુખ્ત",
	age: "ની ઉંમર",
	years: "વર્ષો",
	search_hotel: "હોટેલ્સ શોધો",
	pick_city_area_hotel: "શહેર, વિસ્તાર અથવા હોટેલ પસંદ કરો",
	destination_hotel: "સમાપ્તિ",
	travel_dates: "મુસાફરીની તારીખ",
	anytime: "કોઈપણ સમયે",
	travellers: "મુસાફરો",
	search_city_hotel: "શહેર અથવા હોટેલ માટે શોધો",
	current_location: "અત્યારની જ્ગ્યા",
	all_star: "બધા તારા",
	star: "સ્ટાર",
	stars: "સ્ટાર્સ",
	and: "અને",
	multiple_rating: "બહુવિધ રેટિંગ્સ",
	error_message_while_booking:
		"માફ કરશો, અમે તમારી બુકિંગની પુષ્ટિ કરવામાં અસમર્થ છીએ.",
	amount_return_message_on_booking_fail:
		"ઉપાડિત રકમ તમારા બેંક ખાતામાં પરત કરવામાં આવશે",
	error_mesage_while_searching:
		"માફ કરશો, અમે આ શોધ ક્વેરી માટે હોટલ મેળવવા માટે અસમર્થ હતાં",
	try_different_search: "કૃપા કરીને વિવિધ શોધ વિકલ્પો સાથે પ્રયત્ન કરો",
	filter_results: "ફિલ્ટર પરિણામો",
	show_pay_at_hotel: "માત્ર @ ચૂકવણી કરો હોટેલ બતાવો",
	free_cancel_hotel: "મુક્ત રદ કરવાની હોટેલ્સ",
	total_price: "કુલ કિંમત",
	to: "થી",
	star_rating: "સ્ટાર રેટિંગ",
	tripadvisor_rating: "Tripadvisor રેટિંગ",
	and_above: "અને ઉપર",
	reset_all: "બધા ફરીથી કરો",
	apply: "અરજી કરો",
	shortlisted: "ટૂંકી સૂચિબદ્ધ",
	sort_by: "દ્વારા સોર્ટ",
	featured: "ફીચર્ડ",
	price: "કિંમત",
	rating: "રેટીંગ",
	saving: "બચાવ",
	price_low_to_high: "ભાવ - લો થી હાઇ",
	price_high_to_low: "ભાવ - ઉચ્ચથી નીચો",
	ratings: "રેટિંગ્સ",
	savings: "બચત",
	shortlist: "શોર્ટલિસ્ટ",
	shortlist_searchHotel_C: "શોર્ટલિસ્ટ",
	user_rating: "વપરાશકર્તા રેટિંગ",
	cancellation_charges_apply: "રદ્દીકરણ ખર્ચ લાગુ પડે છે",
	modify_search: "સંશોધન સંશોધિત કરો",
	total: "કુલ",
	bangalore_shortList: "બેંગલોર શોર્ટલિસ્ટ",
	details: "(વિગતો)",
	continue_booking: "બુકિંગ ચાલુ રાખો",
	select_room: "પસંદ કરો રૂમ",
	select_room_C: "પસંદ કરો રૂમ",

	pick_dates_and_rooms: "તારીખો અને રૂમ ચૂંટો",
	go_home: "ઘરે જાવ",
	room_C: "રૂમ",
	from: "માંથી",
	view_full_map: "સંપૂર્ણ નકશો જુઓ",
	more: "વધુ",
	show_more: "વધારે બતાવ",
	location: "સ્થાન",
	close_C: "બંધ",
	cities: "શહેરો",
	trip_id: "ટ્રીપ આઈડી:",
	hotel_address: "હોટેલ સરનામું",
	booking_details: "બુકિંગ વિગતો",
	traveller: "મુસાફરી કરનાર",
	total_rooms: "કુલ રૂમ",
	occupancy: "વ્યવસાય",
	voucher_no: "વાઉચર નં.",
	payment_details: "ચુકવણીની વિગતો",
	total_fare: "કુલ ભાડું",
	unable_to_fetch_detail:
		"માફ કરશો, અમે તમારી સહેલની વિગતો મેળવવામાં અસમર્થ છીએ",
	refresh: "રીફ્રે",
	unable_fetch_rate_message:
		"રેટ વિગતો પ્રાપ્ત કરવામાં અસમર્થ, પછીથી પ્રયાસ કરો",
	select_city_hotel: "કૃપા કરીને કોઈ શહેર અથવા હોટેલ પસંદ કરો",
	no_location_message: "તમારું સ્થાન શોધવામાં અસમર્થ.",
	select_checkin_date: "ચેકિન તારીખ પસંદ કરો",
	select_checkout_date: "ચેકઆઉટ તારીખ પસંદ કરો",
	first_name: "પ્રથમ નામ",
	last_name: "છેલ્લું નામ",
	email_id: "ઇમેઇલ આઈડી",
	ph_number: "ફોન નંબર",
	search_loc_hotel: "સ્થાન અથવા હોટેલ માટે શોધો",
	hotels: "હોટલ",
	by: "દ્વારા",
	cl_list: "યાદી",
	cl_map: "નકશો"
};
