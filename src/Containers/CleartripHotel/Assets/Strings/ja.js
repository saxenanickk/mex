import * as I18n from "../../../../Assets/strings/i18n";

export default {
	...I18n.default.translations.ja,
	no_of_adult: "大人の数",
	no_of_child: "子供の数",
	remove: "削除する",
	room_guests: "ルーム＆ゲスト",
	rooms: "部屋",
	children: "お子様",
	adults: "大人",
	age: "'セージ",
	years: "年",
	search_hotel: "ホテルを検索",
	pick_city_area_hotel: "都市、エリア、またはホテルを選択",
	destination_hotel: "先",
	travel_dates: "旅行日",
	anytime: "どんなときも",
	travellers: "旅行者",
	search_city_hotel: "都市やホテルを探す",
	current_location: "現在位置",
	all_star: "すべての星",
	star: "星",
	stars: "星",
	and: "そして",
	multiple_rating: "複数の評価",
	error_message_while_booking:
		"申し訳ありませんが、ご予約を確認できませんでした。",
	amount_return_message_on_booking_fail:
		"差し引かれた金額はあなたの銀行口座に返金されます",
	error_mesage_while_searching:
		"申し訳ありませんが、この検索クエリに対応するホテルが見つかりませんでした",
	try_different_search: "別の検索オプションで試してください",
	filter_results: "結果を絞り込む",
	show_pay_at_hotel: "有料の@ホテルのみを表示",
	free_cancel_hotel: "無料キャンセルホテル",
	total_price: "合計金額",
	to: "に",
	star_rating: "星の評価",
	tripadvisor_rating: "Tripadvisorによる評価",
	and_above: "以上",
	reset_all: "すべてリセット",
	apply: "適用する",
	shortlisted: "候補者",
	sort_by: "並び替え",
	featured: "特徴",
	price: "価格",
	rating: "評価",
	saving: "セービング",
	price_low_to_high: "価格 - 安い順",
	price_high_to_low: "価格 - 高い順",
	ratings: "評価",
	savings: "貯蓄",
	shortlist: "ショートリスト",
	shortlist_searchHotel_C: "ショートリスト",

	user_rating: "ユーザー評価",
	cancellation_charges_apply: "キャンセル料がかかります",
	modify_search: "検索を変更する",
	total: "合計",
	bangalore_shortList: "バンガロールのショートリスト",
	details: "（詳細）",
	continue_booking: "予約を続けます。",
	select_room: "部屋の選択",
	select_room_C: "部屋の選択",

	pick_dates_and_rooms: "日付と部屋を選ぶ",
	guests: "ゲスト",
	room: {
		one: "ルーム",
		other: "部屋"
	},
	adult: {
		one: "アダルト",
		other: "大人"
	},
	child: {
		one: "子",
		other: "子供",
		zero: ""
	},
	night: {
		one: "夜",
		other: "夜"
	},
	date: {
		formats: {
			normal_day: "%a, %d %b",
			header_day: "%d %b"
		}
	},
	go_home: "家に行く",
	room_C: "ルーム",
	from: "から",
	view_full_map: "地図全体を見る",
	more: "もっと",
	show_more: "もっと見せる",
	location: "ロケーション",
	close_C: "閉じる",
	cities: "都市",
	trip_id: "旅行ID：",
	hotel_address: "ホテルの住所",
	booking_details: "予約明細",
	traveller: "旅行者",
	total_rooms: "総部屋",
	occupancy: "占有率",
	voucher_no: "伝票番号",
	payment_details: "支払詳細",
	total_fare: "合計運賃",
	unable_to_fetch_detail:
		"申し訳ありませんが、旅行の詳細情報を取得できませんでした",
	refresh: "リフレッシュ",
	unable_fetch_rate_message: "レートの詳細を取得できません。後で試してください",
	select_city_hotel: "都市またはホテルを選択してください",
	no_location_message: "あなたの場所が見つかりません。",
	select_checkin_date: "チェックイン日を選択",
	select_checkout_date: "チェックアウト日を選択",
	first_name: "性",
	last_name: "名",
	email_id: "電子メールID",
	ph_number: "電話番号",
	search_loc_hotel: "場所やホテルを探す",
	hotels: "ホテル",
	by: "によって",
	cl_list: "リスト",
	cl_map: "地図"
};
