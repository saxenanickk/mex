/* eslint-disable radix */
import { DatePickerAndroid } from "react-native";
import React from "react";
import AddTraveller from "../Containers/SearchHotel/AddTraveller";
import { GoToast } from "../../../Components";
import I18n from "../Assets/Strings/i18n.js";
export const EPOCH_ONE_YEAR_DURATION = 31536000000;
export const ONE_DAY_EPOCH_DURATION = 86400000;
class cleartripHotelUtil {
	constructor() {
		this.month = [
			"jan",
			"feb",
			"mar",
			"apr",
			"may",
			"jun",
			"jul",
			"aug",
			"sep",
			"oct",
			"nov",
			"dec"
		];
	}
	async openCheckInDatePicker(onDateSelected) {
		let now = new Date();
		let minDate = now;
		GoToast.show(I18n.t("select_checkin_date"), I18n.t("information"));
		try {
			const { action, year, month, day } = await DatePickerAndroid.open({
				date: now,
				minDate,
				maxDate: new Date(now.getTime() + EPOCH_ONE_YEAR_DURATION)
			});
			if (action !== DatePickerAndroid.dismissedAction) {
				return this.openCheckoutDatePicker(year, month, day, onDateSelected);
			}
		} catch ({ code, message }) {
			console.warn("Cannot open date picker", message);
		}
	}
	async openCheckoutDatePicker(ciyear, cimonth, ciday, onDateSelected) {
		cimonth = parseInt(cimonth) + 1;
		cimonth = cimonth < 10 ? "0" + cimonth : cimonth;
		ciday = ciday < 10 ? "0" + ciday : ciday;
		let dateString = ciyear + "/" + cimonth + "/" + ciday;
		let checkinDate = new Date(dateString);
		let minDate = new Date(
			new Date(dateString).getTime() + ONE_DAY_EPOCH_DURATION
		);
		GoToast.show(I18n.t("select_checkout_date"), I18n.t("information"));
		try {
			let { action, year, month, day } = await DatePickerAndroid.open({
				date: minDate,
				minDate,
				maxDate: new Date(new Date().getTime() + EPOCH_ONE_YEAR_DURATION)
			});
			if (action !== DatePickerAndroid.dismissedAction) {
				month = parseInt(month) + 1;
				month = month < 10 ? "0" + month : month;
				day = day < 10 ? "0" + day : day;
				dateString = year + "/" + month + "/" + day;
				let checkoutDate = new Date(dateString);
				onDateSelected(checkinDate, checkoutDate);
			}
		} catch ({ code, message }) {
			console.warn("Cannot open date picker", message);
		}
	}

	getMonthName(index) {
		return this.month[index];
	}

	displayTraveller = (
		closeModal,
		saveTravellerDetail,
		travellerModalVisible
	) => {
		return (
			<AddTraveller
				closeModal={closeModal}
				saveTravellerDetail={saveTravellerDetail}
				travellerModalVisible={travellerModalVisible}
			/>
		);
	};

	getDateAsParameter = date => {
		let month = date.getMonth() + 1;
		let day = date.getDate();
		month = month < 10 ? "0" + month : month;
		day = day < 10 ? "0" + day : day;
		return date.getYear() + 1900 + "-" + month + "-" + day;
	};
}
export default new cleartripHotelUtil();
