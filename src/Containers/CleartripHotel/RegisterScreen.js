import React from "react";
import { Stack } from "../../utils/Navigators";
import Splash from "./Containers/Splash";
import SearchHotel from "./Containers/SearchHotel";
import SelectHotel from "./Containers/SelectHotel";
import HotelDetail from "./Containers/HotelDetail";
import SelectRoom from "./Containers/SelectRoom";
import ShortListHotel from "./Containers/ShortListHotel";
import BookHotel from "./Containers/BookHotel";
import TripDetail from "./Containers/TripDetail";
import Search from "./Containers/SearchHotel/Search";
import AddTraveller from "./Containers/SearchHotel/AddTraveller";
import Filter from "./Containers/SelectHotel/Filter";

const Navigator = () => (
	<Stack.Navigator headerMode={"none"}>
		<Stack.Screen name={"Splash"} component={Splash} />
		<Stack.Screen name={"SearchHotel"} component={SearchHotel} />
		<Stack.Screen name={"SelectHotel"} component={SelectHotel} />
		<Stack.Screen name={"HotelDetail"} component={HotelDetail} />
		<Stack.Screen name={"SelectRoom"} component={SelectRoom} />
		<Stack.Screen name={"ShortListHotel"} component={ShortListHotel} />
		<Stack.Screen name={"BookHotel"} component={BookHotel} />
		<Stack.Screen name={"TripDetail"} component={TripDetail} />
		<Stack.Screen name={"SearchPlace"} component={Search} />
		<Stack.Screen name={"AddTraveller"} component={AddTraveller} />
		<Stack.Screen name={"Filter"} component={Filter} />
	</Stack.Navigator>
);

export default Navigator;
