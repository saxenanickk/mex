import App from "./App";
import React from "react";
import { AppRegistry } from "react-native";
import reducer from "./Reducer";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";
import { getNewReducer, removeExistingReducer } from "../../../App";

export default class CleartripHotel extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "cleartriphotel" });
		getNewReducer({ name: "cleartripHotel", reducer: reducer });
	}

	componentWillUnmount() {
		removeExistingReducer("cleartripHotel");
	}

	render() {
		return (
			<App nav={this.props.navigation} isUserLoggedIn={this.isUserLoggedIn} />
		);
	}
}

AppRegistry.registerComponent("CleartripHotel", () => CleartripHotel);
