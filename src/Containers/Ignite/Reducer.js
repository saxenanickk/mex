import { combineReducers } from "redux";
// Import Different Reducers Here
import { reducer as homeReducer } from "./Containers/Home/Reducer";
import { reducer as bookDetailReducer } from "./Containers/BookDetail/Reducer";
import global_reducer from "../../Reducer";

const igniteReducer = combineReducers({
	home: homeReducer,
	bookDetail: bookDetailReducer
});

export default igniteReducer;
