import React, { Fragment } from "react";
import { SafeAreaView, StatusBar } from "react-native";
import RegisterScreen from "./RegisterScreen";
import GoAppAnalytics from "../../utils/GoAppAnalytics";

export default class App extends React.Component {
	render() {
		return (
			<Fragment>
				{/** For the Top Color (Under the Notch) */}
				<SafeAreaView style={{ flex: 0, backgroundColor: "#ffffff" }} />
				{/** For the Bottom Color (Below the home touch) */}
				<SafeAreaView style={{ flex: 1 }}>
					<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
					<RegisterScreen
						nav={this.props.nav}
						onNavigationStateChange={(prevState, currState) =>
							GoAppAnalytics.logChangeOfScreenInStack(
								"ignite",
								prevState,
								currState
							)
						}
					/>
				</SafeAreaView>
			</Fragment>
		);
	}
}
