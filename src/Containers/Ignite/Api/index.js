// @ts-check
import Config from "react-native-config";

/**
 * @typedef {Object} GetParams
 * @property {string} appToken
 */

const { SERVER_BASE_URL_IGNITE, SERVER_BASE_URL_SITES } = Config;

class IgniteApi {
	constructor() {
		console.log("Ignite Api Instantiated.");
	}

	/**
	 * Fetch Books Api
	 * @param {GetParams} params
	 */
	getBooks(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_IGNITE + "getBooks", {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	/**
	 * Fetch Booths Api
	 * @param {GetParams} params
	 */
	getBooths(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_IGNITE + "getBooths", {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response", response);
						response
							.json()
							.then(res => {
								console.log("res is ", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	/**
	 * Fetch Genres Api
	 * @param {GetParams} params
	 */
	getGenres(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_IGNITE + "getGenres", {
					method: "GET",
					headers: {
						"Content-Type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response", response);
						response
							.json()
							.then(res => {
								console.log("res is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}

	/**
	 * generates the hash for book
	 */
	generateQRHash(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_IGNITE + "generateQRHash", {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					},
					body: JSON.stringify(params.body)
				})
					.then(response => {
						console.log("response", response);
						response
							.json()
							.then(res => {
								console.log("response of generating qr cash", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}

	/**
	 * fetch book detail for a book
	 */
	getBookDetail(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_IGNITE + "getBook?book_id=" + params.bookId, {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response", response);
						response
							.json()
							.then(res => {
								console.log("response of getBook", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}

	/**
	 * function to check whether user is eligible to take book or not
	 */
	isUserEligibleToTakeBook(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_IGNITE + "isUserEligible?book_id=" + params.bookId,
					{
						method: "GET",
						headers: {
							"Content-Type": "application/json",
							"X-ACCESS-TOKEN": params.appToken
						}
					}
				)
					.then(response => {
						console.log("response", response);
						response
							.json()
							.then(res => {
								console.log(
									"response of checking whether user is eligible to take book or not",
									res
								);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}

	/**
	 * function to take book
	 */
	pickBook(params) {
		console.log("params are", params);
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_IGNITE + "allotBook", {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						"X-ACCESS-TOKEN": params.appToken,
						qrhash: params.qrhash
					},
					body: JSON.stringify({
						allotment_days: params.days
					})
				})
					.then(response => {
						console.log("response", response);
						response
							.json()
							.then(res => {
								console.log("response of picking book", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}

	returnBook(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_IGNITE + "returnBook", {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						"X-ACCESS-TOKEN": params.appToken,
						qrhash: params.qrhash
					},
					body: JSON.stringify({
						copy_id: params.copyId
					})
				})
					.then(response => {
						console.log("response", response);
						response
							.json()
							.then(res => {
								console.log("response of returning book", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}

	getAllotedBooks(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_IGNITE + "getAllotedBooks", {
					method: "GET",
					headers: {
						"Content-Type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response", response);
						response
							.json()
							.then(res => {
								console.log("response of alloted book is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}

	getPodHash(params) {
		console.log("params are", params);
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_IGNITE + "generateBoothHash", {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					},
					body: JSON.stringify({
						booth_id: params.boothId
					})
				})
					.then(response => {
						console.log("response", response);
						response
							.json()
							.then(res => {
								console.log("response of booth hash is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}

	getBookDetailFromQRHash(params) {
		console.log("get book detail from qrhash", params);
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_IGNITE + "getBookByQRHash", {
					method: "GET",
					headers: {
						"Content-Type": "application/json",
						"X-ACCESS-TOKEN": params.appToken,
						qrHash: params.qrHash
					}
				})
					.then(response => {
						console.log("response", response);
						response
							.json()
							.then(res => {
								console.log("response of boothdetail from hash is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}

	getSearchBooks(params) {
		let url = SERVER_BASE_URL_IGNITE + "search";
		if (params.query !== null && params.query !== undefined) {
			url += "?value=" + params.query;
		}
		console.log("url is", url, params.filterBody);
		return new Promise(function(resolve, reject) {
			try {
				fetch(url, {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					},
					body: JSON.stringify(params.filterBody)
				})
					.then(response => {
						console.log("response", response);
						response
							.json()
							.then(res => {
								console.log("response of search and filter book is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}

	getFilteredBooks(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_IGNITE + "filter?genre=" + params.query, {
					method: "GET",
					headers: {
						"Content-Type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response", response);
						response
							.json()
							.then(res => {
								console.log("response of filter book is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}

	getAllSites(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_SITES + "/getSites", {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response", response);
						response
							.json()
							.then(res => {
								console.log("response of get sites is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}
}

export default new IgniteApi();
