import React from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import reducer from "./Reducer";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";
import { getNewReducer, removeExistingReducer } from "../../../App";

export default class Ignite extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "ignite" });
		getNewReducer({ name: "ignite", reducer: reducer });
	}

	componentWillUnmount() {
		removeExistingReducer("ignite");
	}

	render() {
		//Deeplink Params
		return <App params={this.props.params} />;
	}
}

AppRegistry.registerComponent("Ignite", () => Ignite);
