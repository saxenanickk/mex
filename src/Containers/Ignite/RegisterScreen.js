import React from "react";
import { Stack } from "../../utils/Navigators";
import Splash from "./Containers/Splash";
import Home from "./Containers/Home";
import BookDetail from "./Containers/BookDetail";
import Bookmark from "./Containers/Bookmark";
import MyBooks from "./Containers/MyBooks";

const Navigator = () => (
	<Stack.Navigator headerMode="none">
		<Stack.Screen name={"Splash"} component={Splash} />
		<Stack.Screen name={"Home"} component={Home} />
		<Stack.Screen name={"BookDetail"} component={BookDetail} />
		<Stack.Screen name={"Bookmark"} component={Bookmark} />
		<Stack.Screen name={"MyBooks"} component={MyBooks} />
	</Stack.Navigator>
);

export default Navigator;
