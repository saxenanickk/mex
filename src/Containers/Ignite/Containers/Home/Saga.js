// @ts-check
import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api";
import { igniteSaveBookEligibility } from "../BookDetail/Saga";

/**
 * Ignite Handle Books Constants
 */
export const IGNITE_GET_BOOKS = "IGNITE_GET_BOOKS";
export const IGNITE_SAVE_BOOKS = "IGNITE_SAVE_BOOKS";
export const IGNITE_SAVE_BOOKS_ERROR = "IGNITE_SAVE_BOOKS_ERROR";

/**
 * Ignite Handle Booths Constants
 */
export const IGNITE_GET_BOOTHS = "IGNITE_GET_BOOTHS";
export const IGNITE_SAVE_BOOTHS = "IGNITE_SAVE_BOOTHS";
export const IGNITE_SAVE_BOOTHS_ERROR = "IGNITE_SAVE_BOOTHS_ERROR";

/**
 * IGNITE Handle Genres Constants
 */
export const IGNITE_GET_GENRES = "IGNITE_GET_GENRES";
export const IGNITE_SAVE_GENRES = "IGNITE_SAVE_GENRES";
export const IGNITE_SAVE_GENRES_ERROR = "IGNITE_SAVE_GENRES_ERROR";

/**
 * Ignite get previous order history of the user
 */
export const IGNITE_GET_ALLOTED_BOOKS = "IGNITE_GET_ALLOTED_BOOKS";
export const IGNITE_SAVE_ALLOTED_BOOKS = "IGNITE_SAVE_ALLOTED_BOOKS";
export const IGNITE_GET_ALLOTED_BOOK_ERROR = "IGNITE_GET_ALLOTED_BOOK_ERROR";

/**
 * Ignite getBookDetail from qr hash
 */
export const IGNITE_GET_BOOK_DETAIL_QR_HASH = "IGNITE_GET_BOOK_DETAIL_QR_HASH";
export const IGNITE_SAVE_BOOK_DETAIL_QR_HASH =
	"IGNITE_SAVE_BOOK_DETAIL_QR_HASH";
export const IGNITE_BOOK_DETAIL_QR_HASH_ERROR =
	"IGNITE_BOOK_DETAIL_QR_HASH_ERROR";
export const IGNITE_GET_SEARCH_BOOK = "IGNITE_GET_SEARCH_BOOK";
export const IGNITE_GET_FILTER_BOOK = "IGNITE_GET_FILTER_BOOK";
export const IGNITE_SAVE_SELECTED_LOCATION = "IGNITE_SAVE_SELECTED_LOCATION";
export const IGNITE_GET_SITES = "IGNITE_GET_SITES";
export const IGNITE_SAVE_SITES = "IGNITE_SAVE_SITES";
export const IGNITE_GET_SITES_ERROR = "IGNITE_GET_SITES_ERROR";
/**
 * Get Books Action
 * @param {{ appToken: string; }} payload
 */

export const igniteGetBooks = payload => ({
	type: IGNITE_GET_BOOKS,
	payload
});

/**
 * @typedef {Object} BookType
 * @property {string} payload.booth_id
 * @property {{author: string; name: string;}} book_info
 * @property {boolean} is_available
 * @property {string} book_id
 * @property {string} image
 * @property {string} description
 */

/**
 * Save Books Action
 * @param {BookType[]} payload - Array of books
 */

export const igniteSaveBooks = payload => ({
	type: IGNITE_SAVE_BOOKS,
	payload
});

/**
 * When Fetch Books Api fails trigger error Action
 * @param {boolean} payload
 */
export const igniteGetBooksError = payload => ({
	type: IGNITE_SAVE_BOOKS_ERROR,
	payload
});

/**
 * Get Booths Action
 * @param {Object} payload
 * @param {string} payload.appToken
 */
export const igniteGetBooths = payload => ({
	type: IGNITE_GET_BOOTHS,
	payload
});

/**
 * @typedef {Object} BoothType
 * @property {string} campus
 * @property {string} id
 * @property {string} created_at
 * @property {{latitude: number; longitude: number}} location
 * @property {string} name
 * @property {number} status
 * @property {string} updated_at
 */

/**
 * Save Booths Action
 * @param {BoothType[]} payload Array of Booths

 */
export const igniteSaveBooths = payload => ({
	type: IGNITE_SAVE_BOOTHS,
	payload
});

/**
 * Failed to get booths error action
 * @param {boolean} payload
 */
export const igniteGetBoothsError = payload => ({
	type: IGNITE_SAVE_BOOKS_ERROR,
	payload
});

/**
 * Get Genres Action
 * @param {Object} payload
 * @param {string} payload.appToken
 */
export const igniteGetGenres = payload => ({
	type: IGNITE_GET_GENRES,
	payload
});

/**
 * @typedef {Object} GenreType
 * @property {string} id
 * @property {string} name
 */

/**
 * Save Genres Action
 * @param {GenreType[]} payload
 */
export const igniteSaveGenres = payload => ({
	type: IGNITE_SAVE_GENRES,
	payload
});

/**
 * Get genres api failed action
 * @param {boolean} payload
 */
export const igniteGetGenresError = payload => ({
	type: IGNITE_SAVE_GENRES_ERROR,
	payload
});

export const igniteGetAllotedBooks = payload => ({
	type: IGNITE_GET_ALLOTED_BOOKS,
	payload
});

export const igniteSaveAllotedBooks = payload => ({
	type: IGNITE_SAVE_ALLOTED_BOOKS,
	payload
});
export const igniteSaveAllotedBookError = payload => ({
	type: IGNITE_GET_ALLOTED_BOOK_ERROR,
	payload
});
export const igniteGetBookDetailFromQRHash = payload => ({
	type: IGNITE_GET_BOOK_DETAIL_QR_HASH,
	payload
});
export const igniteSaveBookDetailFromQRHash = payload => ({
	type: IGNITE_SAVE_BOOK_DETAIL_QR_HASH,
	payload
});
export const igniteBookDetailQRHashError = payload => ({
	type: IGNITE_BOOK_DETAIL_QR_HASH_ERROR,
	payload
});
export const igniteGetSearchBook = payload => ({
	type: IGNITE_GET_SEARCH_BOOK,
	payload
});
export const igniteGetFilterBook = payload => ({
	type: IGNITE_GET_FILTER_BOOK,
	payload
});
export const igniteGetSites = payload => ({
	type: IGNITE_GET_SITES,
	payload
});
export const igniteSaveSites = payload => ({
	type: IGNITE_SAVE_SITES,
	payload
});
export const igniteGetSitesError = payload => ({
	type: IGNITE_GET_SITES_ERROR,
	payload
});
/**
 * @param {any} dispatch
 */
export function* igniteHomeSaga(dispatch) {
	yield takeLatest(IGNITE_GET_BOOKS, handleBooks);
	yield takeLatest(IGNITE_GET_BOOTHS, handleBooths);
	yield takeLatest(IGNITE_GET_GENRES, handleGenres);
	yield takeLatest(IGNITE_GET_ALLOTED_BOOKS, handleGetAllotedBooks);
	yield takeLatest(
		IGNITE_GET_BOOK_DETAIL_QR_HASH,
		handleGetBookDetailFromQRHash
	);
	yield takeLatest(IGNITE_GET_SEARCH_BOOK, handleGetSearchBook);
	yield takeLatest(IGNITE_GET_FILTER_BOOK, handleGetFilterBook);
	yield takeLatest(IGNITE_GET_SITES, handleIgniteGetSites);
}

/**
 * @param {{ payload: { appToken: string; }; type: string}} action
 */
function* handleBooks(action) {
	console.log(action);
	//debugger
	try {
		let response = yield call(Api.getBooks, action.payload);
		console.log("response is", response);
		if (response.success === 1) {
			let books = response.data;
			let booksWithData = books.map(
				/**
				 * @param {{ image: string; description: string; }} book
				 */
				book => {
					book.description =
						"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat, ante ut ornare congue, massa ligula viverra dui, vel vehicula arcu mauris ac leo. Curabitur eleifend vulputate enim, tincidunt vulputate sapien rhoncus imperdiet. In hac habitasse platea dictumst. Quisque eget mollis elit. Nulla vel cursus ligula, ac volutpat augue. Maecenas cursus nulla nec ipsum malesuada, nec maximus urna euismod. Donec semper tempus fringilla. Aliquam id urna ac ex blandit semper imperdiet quis dolor. Nunc lacinia massa est, ut facilisis ex convallis commodo.";
					return book;
				}
			);
			yield put(igniteSaveBooks(booksWithData));
		} else {
			yield put(igniteGetBooksError(true));
			yield put(igniteSaveBooks([]));
		}
	} catch (error) {
		console.log("error is", error);
		yield put(igniteGetBooksError(true));
		yield put(igniteSaveBooks([]));
	}
}

/**
 * @param {{ payload: { appToken: string; }; type: string}} action
 */
function* handleBooths(action) {
	try {
		let response = yield call(Api.getBooths, action.payload);
		console.log("response", response);
		if (response.success === 1) {
			yield put(igniteSaveBooths(response.data));
		} else {
			yield put(igniteGetBoothsError(true));
		}
	} catch (error) {
		console.log(error, "error");
		yield put(igniteGetBoothsError(true));
	}
}

/**
 * @param {{ payload: { appToken: string; }; type: string}} action
 */
function* handleGenres(action) {
	try {
		let response = yield call(Api.getGenres, action.payload);
		console.log("response", response);
		if (response.success === 1) {
			yield put(igniteSaveGenres(response.data));
		} else {
			yield put(igniteGetGenresError(true));
		}
	} catch (error) {
		console.log(error, "error");
		yield put(igniteGetGenresError(true));
	}
}

function* handleGetAllotedBooks(action) {
	try {
		let response = yield call(Api.getAllotedBooks, action.payload);
		console.log("response of alloted boks is", response);
		if (response.success === 1) {
			yield put(igniteSaveAllotedBooks(response.data));
		} else {
			yield put(igniteSaveAllotedBookError(true));
		}
	} catch (error) {
		console.log(error, "error");
		yield put(igniteSaveAllotedBookError(true));
	}
}

function* handleGetBookDetailFromQRHash(action) {
	try {
		let response = yield call(Api.getBookDetailFromQRHash, action.payload);
		console.log("response of book detail is", response);
		if (response.success === 1) {
			const eligibility = yield call(Api.isUserEligibleToTakeBook, {
				...action.payload,
				bookId: response.data.book_details.book_id
			});
			if (eligibility.success === 1) {
				yield put(igniteSaveBookDetailFromQRHash(response.data));
			}
			yield put(igniteSaveBookEligibility(eligibility.success === 0));
		} else {
			yield put(igniteBookDetailQRHashError(true));
		}
	} catch (error) {
		yield put(igniteBookDetailQRHashError(true));
	}
}

function* handleGetSearchBook(action) {
	try {
		let response = yield call(Api.getSearchBooks, action.payload);
		console.log("response is", response);
		if (response.success === 1) {
			let books = response.data;
			let booksWithData = books;
			yield put(igniteSaveBooks(booksWithData));
		} else {
			yield put(igniteGetBooksError(true));
			yield put(igniteSaveBooks([]));
		}
	} catch (error) {
		console.log("error is", error);
		yield put(igniteGetBooksError(true));
		yield put(igniteSaveBooks([]));
	}
}

function* handleGetFilterBook(action) {
	try {
		let response = yield call(Api.getFilteredBooks, action.payload);
		console.log("response is", response);
		if (response.success === 1) {
			if (response.data === null || response.data === undefined) {
				yield put(igniteGetBooksError(true));
				yield put(igniteSaveBooks([]));
			} else {
				let books = response.data;
				let booksWithData = books.map(
					/**
					 * @param {{ image: string; description: string; }} book
					 */
					book => {
						book.description =
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat, ante ut ornare congue, massa ligula viverra dui, vel vehicula arcu mauris ac leo. Curabitur eleifend vulputate enim, tincidunt vulputate sapien rhoncus imperdiet. In hac habitasse platea dictumst. Quisque eget mollis elit. Nulla vel cursus ligula, ac volutpat augue. Maecenas cursus nulla nec ipsum malesuada, nec maximus urna euismod. Donec semper tempus fringilla. Aliquam id urna ac ex blandit semper imperdiet quis dolor. Nunc lacinia massa est, ut facilisis ex convallis commodo.";
						return book;
					}
				);
				yield put(igniteSaveBooks(booksWithData));
			}
		} else {
			yield put(igniteGetBooksError(true));
			yield put(igniteSaveBooks([]));
		}
	} catch (error) {
		console.log("error is", error);
		yield put(igniteGetBooksError(true));
		yield put(igniteSaveBooks([]));
	}
}

function* handleIgniteGetSites(action) {
	try {
		let response = yield call(Api.getAllSites, action.payload);
		if (response.success === 1) {
			let sites = response.data;
			sites.sort((site1, site2) => site1.name.localeCompare(site2.name));
			yield put(igniteSaveSites(sites));
		} else {
			yield put(igniteGetSitesError(true));
		}
	} catch (error) {
		console.log(error, "error");
		yield put(igniteGetSitesError(true));
	}
}
