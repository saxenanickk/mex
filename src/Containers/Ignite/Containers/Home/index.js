import React, { Component } from "react";
import {
	View,
	Dimensions,
	TextInput,
	FlatList,
	Image,
	ScrollView,
	TouchableOpacity,
	ActivityIndicator,
	Animated,
	Easing,
	Keyboard,
	Alert,
	LayoutAnimation,
	NativeModules,
	Platform
} from "react-native";
import { connect } from "react-redux";
import { CameraKitCamera } from "react-native-camera-kit";
import {
	Icon,
	ProgressScreen,
	QRCodeReader,
	HomeBack
} from "../../../../Components";
import {
	IgniteTextLight,
	IgniteTextRegular,
	IgniteTextMedium,
	IgniteTextBold
} from "../../Components/IgniteText";
// @ts-ignore
import { IGNITE, PICKUPBOOK, RETURNBOOK } from "../../Assets/Img/Image";
import {
	igniteSaveRequestBookResponse,
	igniteRequestBook,
	igniteSaveBookEligibility
} from "../BookDetail/Saga";
import I18n from "../../Assets/Strings/i18n";
import {
	igniteGetBooths,
	igniteGetGenres,
	igniteGetBookDetailFromQRHash,
	igniteSaveBookDetailFromQRHash,
	igniteBookDetailQRHashError,
	igniteGetAllotedBooks,
	igniteGetSearchBook,
	igniteSaveBooks,
	igniteSaveBooths,
	igniteSaveAllotedBooks
} from "./Saga";
import { styles } from "./style";
import GoPicker from "../../../../Components/GoPicker";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import debounce from "../../../../utils/debounce";
import Freshchat from "../../../../CustomModules/Freshchat";

const { height, width } = Dimensions.get("window");
const { UIManager } = NativeModules;
if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}
const NO_FILTER = "all";
/**
 * @typedef {import('./Saga').BookType} BookType
 * @typedef {import('./Saga').BoothType} BoothType
 * @typedef {import('./Saga').GenreType} GenreType
 * @augments {Component<{appToken: string; books: BookType[]; booths: BoothType[]; genres: GenreType[]; allotedBooks:Object[];navigation: object; dispatch: Function}>}
 */
class Home extends Component {
	/**
	 * @param {any} props
	 */
	constructor(props) {
		super(props);
		this.state = {
			scanning: false,
			openDialog: false,
			days: 1,
			searchText: "",
			disableGenre: false,
			selectedFilter: NO_FILTER,
			scrollEnabled: true,
			location: props.location.name,
			openFilter: false,
			isFilterOptionVisible: true
		};
		this.animatedValue = new Animated.Value(0);
		this.hashkey = "";
		this.handleBarCodeRead = this._handleBarCodeRead.bind(this);
		this.scrollY = 0;
		this.setStickyHeader = this.setStickyHeader.bind(this);
		this.dayArray = [{ day: 1 }, { day: 7 }, { day: 14 }, { day: 28 }];
		this.tempLocation = null;
	}

	componentDidMount() {
		this.props.dispatch(igniteGetBooths({ appToken: this.props.appToken }));
		this.props.dispatch(igniteGetGenres({ appToken: this.props.appToken }));
		if (this.props.location) {
			this.filterAndSearchBooks(
				this.state.selectedFilter,
				this.props.location.name,
				this.state.searchText
			);
		}
	}

	/**
	 * @param {any} item
	 */
	handleBookPress = item => {
		//this.props.dispatch(igniteSaveBookDetail(item))
		GoAppAnalytics.trackWithProperties("view_book", {
			bookId: item.book_id
		});
		Keyboard.dismiss();
		this.props.navigation.navigate("BookDetail", {
			bookId: item.book_id
		});
	};

	renderBooks = (item, index) => {
		try {
			if (
				this.props.allotedBooks &&
				this.props.allotedBooks.filter(row => row.book_id === item.book_id)
					.length > 0
			) {
				return null;
			} else {
				return (
					<View key={index}>
						<TouchableOpacity
							style={styles.bookView}
							activeOpacity={0.8}
							onPress={() => this.handleBookPress(item)}>
							<View style={styles.innerBookView}>
								<Image
									style={styles.coverImage}
									source={{ uri: item.book_info.image }}
									resizeMode={"cover"}
								/>
								<View style={styles.bookDescription}>
									<IgniteTextMedium
										style={styles.bookListName}
										numberOfLines={1}>
										{item.book_info.name}
									</IgniteTextMedium>
									<IgniteTextRegular
										style={styles.writerName}
										numberOfLines={1}>
										{item.book_info.author}
									</IgniteTextRegular>
									<IgniteTextLight
										style={styles.bookDetailText}
										numberOfLines={3}>
										{item.book_info.description}
									</IgniteTextLight>
								</View>
							</View>
						</TouchableOpacity>
						<View style={styles.listSeparator} />
					</View>
				);
			}
		} catch (error) {
			return null;
		}
	};

	componentWillUnmount() {
		this.props.dispatch(igniteSaveBooks(null));
		this.props.dispatch(igniteSaveBooths(null));
		this.props.dispatch(igniteSaveAllotedBooks(null));
	}
	/**
	 * Component to render the dialog box to take books
	 */
	renderTakeBookDialogBox = () => {
		const { bookDetail } = this.props;
		if (bookDetail !== null) {
			return (
				<TouchableOpacity style={styles.box} activeOpacity={1}>
					<IgniteTextMedium style={styles.headerInsideBox}>
						{I18n.t("book_details")}
					</IgniteTextMedium>
					<View style={styles.childInsideBox}>
						<IgniteTextMedium style={styles.bookName}>
							{bookDetail.book_details.name}
						</IgniteTextMedium>
						<View style={[styles.centeredRow, styles.genreListContent]}>
							<IgniteTextRegular
								numberOfLines={1}
								style={[styles.bookSemiDetailText, styles.bookTextAlignEnd]}>
								{bookDetail.book_details.author}
							</IgniteTextRegular>
							<View style={styles.dotIcon}>
								<Icon
									iconType={"entypo"}
									iconSize={height / 18}
									iconName={"dot-single"}
									iconColor={"#767676"}
								/>
							</View>
							<IgniteTextRegular
								numberOfLines={1}
								style={styles.bookSemiDetailText}>
								{bookDetail.book_details.genre}
							</IgniteTextRegular>
						</View>
					</View>
					<View style={styles.bottomMargin}>
						<IgniteTextMedium style={styles.daysText}>
							{"Days"}
						</IgniteTextMedium>
						<GoPicker
							selectedValue={this.state.days}
							data={this.dayArray}
							updateSelectedValue={({ itemValue }) => {
								console.log("update the selected value", itemValue);
								this.setState({ days: itemValue });
							}}
							pickerKey={"day"}
							label={"day"}
							value={"day"}
							mode={"dropdown"}
							style={{ width: width / 2 }}
						/>
					</View>

					<TouchableOpacity
						style={styles.pickUp}
						onPress={() => {
							const bookId = this.props.bookDetail.book_details.book_id;
							this.props.dispatch(
								igniteRequestBook({
									qrhash: this.hashkey,
									appToken: this.props.appToken,
									days: this.state.days,
									bookId
								})
							);
							GoAppAnalytics.trackWithProperties("take_book", {
								bookId: bookId,
								days: this.state.days
							});
							this.setState({ openDialog: false, requesting: true });
						}}>
						<IgniteTextMedium style={styles.pickUpText}>
							{I18n.t("ignite_pick_up")}
						</IgniteTextMedium>
					</TouchableOpacity>
				</TouchableOpacity>
			);
		} else if (this.state.openDialog) {
			return <ProgressScreen />;
		} else {
			return null;
		}
	};

	_handleBarCodeRead(e) {
		this.setState({ scanning: false, openDialog: true });
		console.log("bar code data is", e.data);
		this.hashkey = e.data;
		this.props.dispatch(
			igniteGetBookDetailFromQRHash({
				appToken: this.props.appToken,
				qrHash: e.data
			})
		);
	}
	/**
	 * @param {string} text
	 */
	searchBook = text => {
		this.setState({ searchText: text });
		if (this.state.disableGenre === false) {
			this.setState({ disableGenre: true });
		}
		this.filterAndSearchBooks(
			this.state.selectedFilter,
			this.state.location,
			text
		);
		debounce(() => {
			GoAppAnalytics.trackWithProperties("search_book", { query: text });
		}, 100);
	};

	animate() {
		this.animatedValue.setValue(0);
		Animated.timing(this.animatedValue, {
			toValue: 1,
			duration: 3000,
			easing: Easing.linear
		}).start(() => this.state.scanning && this.animate());
	}

	setStickyHeader = event => {
		if (event.nativeEvent.contentOffset.y > 80 && this.state.scrollEnabled) {
			this.setState({ scrollEnabled: false });
		} else if (
			event.nativeEvent.contentOffset.y < 80 &&
			!this.state.scrollEnabled
		) {
			this.setState({ scrollEnabled: true });
		}

		//hide or unhide the filter button
		if (
			event.nativeEvent.contentOffset.y > this.scrollY &&
			this.state.isFilterOptionVisible === true
		) {
			LayoutAnimation.easeInEaseOut();
			this.setState({ isFilterOptionVisible: false });
		} else if (
			event.nativeEvent.contentOffset.y < this.scrollY &&
			this.state.isFilterOptionVisible === false
		) {
			LayoutAnimation.easeInEaseOut();
			this.setState({ isFilterOptionVisible: true });
		} else if (
			event.nativeEvent.contentOffset.y === 0 &&
			this.state.isFilterOptionVisible === false
		) {
			this.setState({ isFilterOptionVisible: true });
		}
		this.scrollY = event.nativeEvent.contentOffset.y;
	};

	shouldComponentUpdate(props, state) {
		if (
			props.bookRequestResponseStatus !== null &&
			props.bookRequestResponseStatus === true
		) {
			Alert.alert("Information", "Book Alloted successfully");
			this.setState({ requesting: false });
			this.props.dispatch(igniteSaveRequestBookResponse(null));
			this.props.dispatch(igniteSaveBookDetailFromQRHash(null));
			this.props.dispatch(
				igniteGetAllotedBooks({
					appToken: props.appToken
				})
			);
			this.filterAndSearchBooks(
				state.selectedFilter,
				state.location,
				state.searchText
			);
			this.props.navigation.navigate("Home");
		}
		if (
			props.bookRequestResponseStatus !== null &&
			props.bookRequestResponseStatus === false
		) {
			Alert.alert("", "Unable to allot Books");
			this.setState({ requesting: false });
			this.props.dispatch(igniteSaveRequestBookResponse(null));
			this.props.dispatch(igniteSaveBookDetailFromQRHash(null));
		}
		if (
			props.bookDetailQRHashError &&
			props.bookDetailQRHashError !== this.props.bookDetailQRHashError &&
			props.bookDetailQRHashError === true
		) {
			Alert.alert("", "Wrong QR Code");
			this.setState({ openDialog: false });
			this.props.dispatch(igniteBookDetailQRHashError(false));
		}
		if (state.searchText === "" && state.searchText !== this.state.searchText) {
			this.setState({ disableGenre: false });
			Keyboard.dismiss();
			this.filterAndSearchBooks(
				state.selectedFilter,
				state.location,
				state.searchText
			);
		}
		if (
			state.openDialog === true &&
			props.isBookTaken === true &&
			props.isBookTaken !== this.props.isBookTaken
		) {
			this.setState({ openDialog: false });
			this.props.dispatch(igniteSaveBookEligibility(false));
			Alert.alert("Information", "Book already taken");
		}
		return true;
	}

	filterBooks = filterType => {
		if (filterType !== this.state.selectedFilter) {
			this.setState({ selectedFilter: filterType });
		}
		this.filterAndSearchBooks(
			filterType,
			this.state.location,
			this.state.searchText
		);
	};

	filterAndSearchBooks = (genre, campus, query) => {
		let filterBody = {};
		if (genre !== NO_FILTER) {
			filterBody.genre = genre;
		}
		if (campus !== null && campus !== undefined) {
			filterBody.campus = campus;
		}
		if (Object.keys(filterBody).length !== 0 || query.trim() !== "") {
			this.props.dispatch(igniteSaveBooks(null));
			this.props.dispatch(
				igniteGetSearchBook({
					appToken: this.props.appToken,
					query: query.trim() === "" ? null : query.trim().toLowerCase(),
					filterBody
				})
			);
		} else {
			this.props.dispatch(igniteSaveBooks(null));
			// this.props.dispatch(
			// 	igniteGetBooks({
			// 		appToken: this.props.appToken,
			// 	}),
			// )
		}
		if (genre !== this.state.selectedFilter || campus !== this.state.location) {
			GoAppAnalytics.trackWithProperties("filter_book", filterBody);
		}
	};

	handleOpenCamera = async () => {
		if (Platform.OS === "android") {
			const isCameraAuthorized = await CameraKitCamera.checkDeviceCameraAuthorizationStatus();
			if (isCameraAuthorized === 1) {
				this.setState({ scanning: true });
			} else {
				const isUserAuthorizedCamera = await CameraKitCamera.requestDeviceCameraAuthorization();
				console.log(isUserAuthorizedCamera);
			}
		} else {
			this.setState({ scanning: true });
		}
	};

	render() {
		const barPosition = this.animatedValue.interpolate({
			inputRange: [0, 0.5, 1],
			outputRange: [0, height / 2.4, 0]
		});
		return (
			<View style={styles.container}>
				<View style={styles.topHeader}>
					{this.state.scanning === false ? (
						<HomeBack
							navigation={this.props.navigation}
							iconColor={"#000"}
							style={[
								{ width: width, backgroundColor: "#fff" },
								styles.topHeader
							]}
						/>
					) : null}
					<View style={styles.header}>
						{this.state.scanning ? (
							<View style={{ flexDirection: "row", alignItems: "center" }}>
								<TouchableOpacity
									style={{ width: width / 5 }}
									onPress={() => this.setState({ scanning: false })}>
									<Icon
										iconType={"ionicon"}
										iconSize={height / 18}
										iconName={"ios-arrow-round-back"}
										iconColor={"#000"}
									/>
								</TouchableOpacity>
								<IgniteTextBold style={[styles.headerPrimaryText]}>
									{"SCAN QR CODE"}
								</IgniteTextBold>
							</View>
						) : (
							<View>
								<View style={styles.leftHeaderPart}>
									<Image
										source={IGNITE}
										style={styles.igniteImage}
										resizeMode={"contain"}
									/>
									<View style={styles.subHeader}>
										<IgniteTextBold style={styles.headerPrimaryText}>
											{/*
								// @ts-ignore */}
											{I18n.t("ignite_header")}
										</IgniteTextBold>
										{this.props.location ? (
											<View
												style={{
													flexDirection: "row",
													width: width / 5
												}}>
												<IgniteTextMedium style={styles.headerSecondaryText}>
													{`${this.props.location.name}  `}
												</IgniteTextMedium>
												<Icon
													iconType={"ionicon"}
													iconSize={height / 55}
													iconName={"ios-pin"}
													iconColor={"#838383"}
												/>
											</View>
										) : null}
									</View>
								</View>
							</View>
						)}
						{!this.state.scanning ? (
							<View style={styles.rightHeaderPart}>
								<TouchableOpacity
									style={{ alignItems: "center", marginRight: width / 100 }}
									onPress={() => {
										if (
											this.props.allotedBooks &&
											this.props.allotedBooks.length === 2
										) {
											Alert.alert(
												"Information",
												"You have reached maximum limit of book allotment"
											);
										} else {
											this.animate();
											this.handleOpenCamera();
										}
									}}>
									<View style={styles.headerButton}>
										<Image
											source={PICKUPBOOK}
											style={styles.bookImage}
											resizeMode={"contain"}
										/>
									</View>
									<IgniteTextMedium style={styles.headerSecondaryText}>
										{/*
								// @ts-ignore */}
										{I18n.t("ignite_pick_up")}
									</IgniteTextMedium>
								</TouchableOpacity>
								<TouchableOpacity
									style={{ alignItems: "center", marginRight: width / 100 }}
									onPress={() => {
										if (
											this.props.allotedBooks &&
											this.props.allotedBooks.length === 0
										) {
											Alert.alert("", "No Books to return");
										} else {
											this.props.navigation.navigate("MyBooks");
										}
									}}>
									<View style={styles.headerButton}>
										<Image
											source={RETURNBOOK}
											style={styles.bookImage}
											resizeMode={"contain"}
										/>
									</View>
									<IgniteTextMedium style={styles.headerSecondaryText}>
										{/*
								// @ts-ignore */}
										{`${I18n.t("ignite_return")}`}
									</IgniteTextMedium>
								</TouchableOpacity>
								<TouchableOpacity
									onPress={() => {
										Freshchat.launchSupportChat();
									}}>
									<View style={styles.headerButton}>
										<Icon
											iconType={"material"}
											iconSize={height / 30}
											iconName={"headset"}
											iconColor={"#000"}
										/>
									</View>
									<IgniteTextMedium style={styles.headerSecondaryText}>
										{/*
								// @ts-ignore */}
										{`${I18n.t("app_support")}`}
									</IgniteTextMedium>
								</TouchableOpacity>
							</View>
						) : null}
					</View>
				</View>
				<ScrollView
					style={styles.container}
					stickyHeaderIndices={[0]}
					// refreshControl={() => <ProgressScreen />}
					scrollEventThrottle={10}
					removeClippedSubviews={true}
					onScroll={this.setStickyHeader}
					keyboardDismissMode={"on-drag"}
					keyboardShouldPersistTaps={"always"}>
					<View
						style={[
							styles.searchRowView,
							this.state.scrollEnabled ? null : styles.searchElevationStyle
						]}>
						<View style={styles.searchBar}>
							<TextInput
								underlineColorAndroid={"transparent"}
								style={styles.textInput}
								// @ts-ignore
								placeholder={I18n.t("ignite_search")}
								placeholderTextColor={"#838383"}
								selectionColor={"#838383"}
								onChangeText={this.searchBook}
								value={this.state.searchText}
							/>
							<TouchableOpacity
								onPress={() => {
									Keyboard.dismiss();
									this.setState({ searchText: "" });
								}}
								style={styles.searchIcon}
								disabled={this.state.searchText === ""}>
								<Icon
									iconType={"ionicon"}
									iconSize={height / 30}
									iconName={
										this.state.searchText === "" ? "ios-search" : "ios-close"
									}
									iconColor={"#838383"}
								/>
							</TouchableOpacity>
						</View>
					</View>
					<View style={styles.genreContainer}>
						{this.props.genres && !this.state.disableGenre && (
							<View>
								<View style={styles.searchedHeader}>
									<IgniteTextMedium style={styles.genreHeaderText}>
										{/*
								// @ts-ignore */}
										{I18n.t("ignite_genre")}
									</IgniteTextMedium>
									<TouchableOpacity
										disabled={this.props.bookmark.length === 0}
										onPress={() => this.props.navigation.navigate("Bookmark")}>
										<Icon
											iconType={"font_awesome"}
											iconSize={height / 40}
											iconName={"bookmark"}
											iconColor={
												this.props.bookmark.length > 0 ? "#f9a35d" : "#777777"
											}
										/>
									</TouchableOpacity>
								</View>
								<FlatList
									horizontal={true}
									data={[
										...[{ name: NO_FILTER, id: "all" }],
										...this.props.genres
									]}
									showsHorizontalScrollIndicator={false}
									style={styles.filterList}
									extraData={this.props.genres}
									contentContainerStyle={styles.genreListContent}
									keyExtractor={item => item.id}
									renderItem={({ item }) => {
										return (
											<TouchableOpacity
												activeOpacity={1}
												onPress={() => {
													this.filterBooks(item.name);
												}}
												style={styles.centeredRow}>
												<IgniteTextMedium
													style={
														this.state.selectedFilter === item.name
															? styles.selectedGenreText
															: styles.genreText
													}>
													{item.name.toLocaleUpperCase()}
												</IgniteTextMedium>
												<View style={styles.genreSeparator}>
													<Icon
														iconType={"ionicon"}
														iconSize={height / 100}
														iconName={"ios-radio-button-on"}
														iconColor={"#838383"}
													/>
												</View>
											</TouchableOpacity>
										);
									}}
								/>
							</View>
						)}
					</View>
					{this.props.books ? (
						<View style={styles.searchedContainer}>
							{this.props.books.length > 0 ? (
								<View
									style={[
										styles.searchList,
										{
											marginTop: this.state.searchText === "" ? 0 : height / 40
										}
									]}>
									{this.props.books.map((item, index) =>
										this.renderBooks(item, index)
									)}
								</View>
							) : (
								<View
									style={[
										styles.noBookContainer,
										{
											marginTop: this.state.searchText === "" ? 0 : height / 40
										}
									]}>
									<IgniteTextMedium>{"No Books Available"}</IgniteTextMedium>
								</View>
							)}
						</View>
					) : (
						<View style={styles.centeredAlign}>
							<ActivityIndicator color={"#38AFFE"} />
						</View>
					)}
				</ScrollView>
				{this.state.scanning ? (
					<View style={styles.cameraContainer}>
						<QRCodeReader onRead={this.handleBarCodeRead} />
					</View>
				) : null}
				<TouchableOpacity
					onPress={() => this.setState({ openDialog: false })}
					style={
						this.state.openDialog ? styles.returnOpen : styles.returnClose
					}>
					{this.renderTakeBookDialogBox()}
				</TouchableOpacity>
				{this.state.requesting && <ProgressScreen color={"#f9a35d"} />}
			</View>
		);
	}
}

/**
 * @param {{ appToken: { token: string; }; ignite: { home: { books: Object[]; booths: Object[]; genres: Object[];allotedBooks:Object[]; }; }; }} state
 */
function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		books: state.ignite && state.ignite.home.books,
		booths: state.ignite && state.ignite.home.booths,
		genres: state.ignite && state.ignite.home.genres,
		location:
			state.appToken && state.appToken.selectedSite
				? state.appToken.selectedSite
				: null,
		allotedBooks: state.ignite && state.ignite.home.allotedBooks,
		isBookTaken: state.ignite && state.ignite.bookDetail.isBookTaken,
		bookRequestResponseStatus:
			state.ignite && state.ignite.bookDetail.bookRequestResponse,
		bookDetail: state.ignite && state.ignite.home.bookDetailQRHash,
		bookDetailQRHashError:
			state.ignite && state.ignite.home.bookDetailQRHashError,
		bookmark: state.ignite && state.ignite.bookDetail.bookmark,
		user: state.appToken.user
	};
}

export default connect(mapStateToProps)(Home);
