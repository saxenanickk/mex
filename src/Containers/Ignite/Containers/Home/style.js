import { StyleSheet, Dimensions, Platform } from "react-native";
import { font_two, font_three } from "../../../../Assets/styles";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: { flex: 1, backgroundColor: "#f7f7f7" },
	noBookContainer: {
		flex: 1,
		height: height / 1.9,
		backgroundColor: "#f7f7f7",
		justifyContent: "center",
		alignItems: "center"
	},
	header: {
		height: height / 9,
		width: width,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		backgroundColor: "#fff",
		paddingHorizontal: width / 23,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	topHeader: {
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	subHeader: {
		justifyContent: "center",
		alignItems: "flex-start",
		marginLeft: width / 30
	},
	headerPrimaryText: {
		color: "#505050",
		fontSize: height / 50
	},
	headerSecondaryText: {
		color: "#919191",
		fontSize: height / 60
	},
	searchBar: {
		width: width / 1.1,
		alignSelf: "center",
		backgroundColor: "#fff",
		height: height / 15,
		borderRadius: width / 60,
		elevation: 3,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.1,
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: width / 20,
		alignItems: "center"
	},
	textInput: {
		width: width / 1.5,
		fontFamily: font_three,
		fontSize: height / 50,
		color: "#838383"
	},
	searchedContainer: {
		width: width,
		alignSelf: "center",
		paddingHorizontal: width / 20
	},
	genreContainer: {
		width: width,
		alignSelf: "center",
		backgroundColor: "#f7f7f7",
		paddingHorizontal: width / 20
	},
	filterList: {
		height: height / 20,
		marginBottom: height / 80
	},
	searchHeader: {
		fontSize: height / 45,
		marginBottom: height / 60,
		color: "#505050"
	},
	searchList: {
		width: width
	},
	bookView: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		width: width / 1.12,
		backgroundColor: "#fff",
		elevation: 3,
		shadowOffset: { width: 3, height: 3 },
		shadowColor: "black",
		shadowOpacity: 0.3,
		borderRadius: width / 50,
		paddingVertical: height / 80,
		paddingHorizontal: width / 40
	},
	innerBookView: {
		flexDirection: "row",
		alignItems: "center",
		width: width / 1.7
	},
	coverImage: {
		width: width / 6,
		height: height / 7,
		borderRadius: width / 60
	},
	bookListName: {
		fontSize: height / 55,
		color: "#1c1c1c",
		width: width / 1.9,
		marginLeft: width / 30
	},
	writerName: {
		fontSize: height / 60,
		color: "#1c1c1c",
		width: width / 1.9,
		marginLeft: width / 30
	},
	bookDetailText: {
		fontSize: height / 65,
		color: "#1c1c1c",
		width: width / 1.9,
		marginLeft: width / 30,
		marginTop: height / 80
	},
	leftHeaderPart: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center"
	},
	genreText: {
		color: "#939393",
		fontSize: height / 55,
		fontFamily: font_two
	},
	selectedGenreText: {
		color: "#53c0f5",
		fontSize: height / 55
	},
	genreHeaderText: {
		color: "#939393",
		fontSize: height / 45,
		fontFamily: font_two
	},
	genreSeparator: {
		flexDirection: "row",
		width: width / 12,
		justifyContent: "center"
	},
	listSeparator: {
		height: height / 30
	},
	bookDescription: {
		borderWidth: 0,
		height: height / 8
	},
	bookLikes: {
		color: "#afafaf",
		fontSize: height / 55
	},
	likeContainer: {
		height: height / 8,
		flexDirection: "row",
		justifyContent: "space-between",
		width: width / 7.5
	},
	headerButton: {
		width: width / 10,
		height: width / 10,
		flexDirection: "row",
		borderRadius: width / 20,
		backgroundColor: "#e4e4e4",
		justifyContent: "center",
		alignItems: "center"
	},
	rightHeaderPart: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	searchedHeader: {
		width: width / 1.13,
		flexDirection: "row",
		justifyContent: "space-between"
	},
	locationText: {
		fontSize: height / 50
	},
	pickerSection: {
		flexDirection: "row",
		justifyContent: "space-between",
		height: height / 16,
		width: width,
		paddingHorizontal: width / 23,
		alignItems: "center",
		elevation: 5
	},
	picker: { width: width / 3 },
	centeredAlign: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		flex: 1,
		height: height / 1.5
	},
	cameraContainer: {
		...Platform.select({
			ios: {
				justifyContent: "center",
				alignItems: "center",
				position: "absolute",
				top: height / 9,
				elevation: 4,
				shadowOffset: { width: 2, height: 2 },
				shadowColor: "black",
				shadowOpacity: 0.2,
				height: height - height / 9,
				backgroundColor: "#fff"
			},
			android: {
				backgroundColor: "#fff",
				position: "absolute",
				top: height / 20,
				width: width
			}
		})
	},
	camera: {
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "transparent",
		width: width,
		height: height - height / 9
	},
	rectangleContainer: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "transparent"
	},
	barRecatangle: {
		position: "absolute",
		top: height / 4,
		borderColor: "#38AFFE",
		borderWidth: 1,
		width: width / 1.2,
		height: height / 2.4,
		alignItems: "center"
	},
	returnOpen: {
		position: "absolute",
		top: 0,
		left: 0,
		bottom: 0,
		right: 0,
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "rgba(0,0,0,0.9)",
		elevation: 4,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2
	},
	returnClose: {
		position: "absolute",
		top: height,
		flex: 1
	},
	box: {
		width: width / 1.1,
		borderRadius: width / 30,
		backgroundColor: "#fff",
		alignItems: "center",
		paddingVertical: height / 40,
		elevation: 4,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2
	},
	headerInsideBox: {
		fontSize: height / 40,
		fontWeight: "600",
		color: "#6E6E6E"
	},
	childInsideBox: {
		paddingTop: height / 50
	},
	bookName: {
		fontSize: height / 45,
		color: "#777777"
	},
	bookSemiDetailText: {
		color: "#afafaf",
		fontSize: height / 55,
		width: width / 3.5
	},
	bookTextAlignEnd: {
		textAlign: "right"
	},
	bottomMargin: {
		borderBottomWidth: 1,
		borderBottomColor: "#efefef"
	},
	daysText: {
		color: "#000",
		fontSize: height / 50,
		marginLeft: width / 60
	},
	pickUp: {
		backgroundColor: "#38AFFE",
		width: width / 2.2,
		height: height / 15,
		borderRadius: 10,
		alignItems: "center",
		justifyContent: "center",
		marginTop: height / 25
	},
	pickUpText: {
		color: "#fff",
		fontWeight: "bold"
	},
	pickerStyle: {
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center"
	},
	searchRowView: {
		backgroundColor: "#f7f7f7",
		justifyContent: "center",
		paddingVertical: height / 50
	},
	searchElevationStyle: {
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	searchIcon: {
		paddingHorizontal: width / 40
	},
	igniteImage: { width: width / 12, height: height / 18 },
	bookImage: { width: width / 18, height: height / 18 },
	genreListContent: {
		justifyContent: "center",
		alignItems: "center"
	},
	centeredRow: {
		flexDirection: "row",
		alignItems: "center"
	},
	animatedLineStyle: {
		position: "absolute",
		borderWidth: 1,
		borderColor: "#38AFFE",
		width: width / 1.2
	},
	dotIcon: {
		width: width / 9,
		alignItems: "center"
	},
	scannerTopContainer: {
		marginTop: height / 10
	},
	button: {
		position: "absolute",
		width: width / 8,
		height: width / 8,
		borderRadius: width / 16,
		bottom: height / 17,
		right: width / 30,
		backgroundColor: "#284f6d",
		justifyContent: "center",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	filterModal: {
		width: width / 1.5,
		position: "absolute",
		top: height / 10,
		left: width / 8,
		backgroundColor: "#fff",
		borderRadius: width / 30,
		paddingVertical: height / 90,
		paddingHorizontal: width / 30
	},
	modalContainer: {
		flex: 1,
		alignItems: "flex-start"
	},
	appName: {
		flex: 1,
		flexDirection: "row",
		alignItems: "center",
		paddingVertical: width / 35,
		borderBottomWidth: 1,
		borderBottomColor: "#ececec"
	},
	dialogModalBackgroundStyle: {
		backgroundColor: "#000000",
		opacity: 0.6
	},
	modalWrapper: {
		position: "absolute",
		width: width / 1.1,
		height: height / 1.8,
		borderRadius: height / 80,
		alignSelf: "center",
		backgroundColor: "#ffffff",
		top: height / 5,
		justifyContent: "center",
		paddingHorizontal: width / 26.3
	}
});
