// @ts-check
import {
	IGNITE_SAVE_BOOKS,
	IGNITE_SAVE_BOOKS_ERROR,
	IGNITE_SAVE_BOOTHS,
	IGNITE_SAVE_BOOTHS_ERROR,
	IGNITE_SAVE_GENRES,
	IGNITE_SAVE_GENRES_ERROR,
	IGNITE_SAVE_ALLOTED_BOOKS,
	IGNITE_GET_ALLOTED_BOOK_ERROR,
	IGNITE_SAVE_BOOK_DETAIL_QR_HASH,
	IGNITE_BOOK_DETAIL_QR_HASH_ERROR,
	IGNITE_SAVE_SELECTED_LOCATION,
	IGNITE_SAVE_SITES,
	IGNITE_GET_SITES_ERROR
} from "./Saga";

const initialState = {
	books: null,
	isBookLoadError: false,
	booths: [],
	isBoothsLoadError: false,
	genres: null,
	isGenresLoadError: false,
	allotedBooks: null,
	allotedBooksError: false,
	bookDetailQRHash: null,
	bookDetailQRHashError: false,
	location: null,
	sites: null,
	siteError: false
};

/**
 * @param {{ type: string; payload: any; }} action
 */
export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case IGNITE_SAVE_BOOKS:
			return {
				...state,
				books: action.payload
			};
		case IGNITE_SAVE_BOOKS_ERROR:
			return {
				...state,
				isBookLoadError: action.payload
			};
		case IGNITE_SAVE_BOOTHS:
			return {
				...state,
				booths: action.payload
			};
		case IGNITE_SAVE_BOOTHS_ERROR:
			return {
				...state,
				isBoothsLoadError: action.payload
			};
		case IGNITE_SAVE_GENRES:
			return {
				...state,
				genres: action.payload
			};
		case IGNITE_SAVE_GENRES_ERROR:
			return {
				...state,
				isGenresLoadError: action.payload
			};
		case IGNITE_SAVE_ALLOTED_BOOKS:
			return {
				...state,
				allotedBooks: action.payload
			};
		case IGNITE_GET_ALLOTED_BOOK_ERROR:
			return {
				...state,
				allotedBooksError: action.payload
			};
		case IGNITE_BOOK_DETAIL_QR_HASH_ERROR:
			return {
				...state,
				bookDetailQRHashError: action.payload
			};
		case IGNITE_SAVE_BOOK_DETAIL_QR_HASH:
			return {
				...state,
				bookDetailQRHash: action.payload
			};
		case IGNITE_SAVE_SELECTED_LOCATION:
			return {
				...state,
				location: action.payload
			};
		case IGNITE_SAVE_SITES:
			return {
				...state,
				sites: action.payload
			};
		case IGNITE_GET_SITES_ERROR:
			return {
				...state,
				siteError: action.payload
			};
		default:
			return state;
	}
};
