// @ts-check
import React, { Component } from "react";
import { Text, View, Dimensions, Image } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import { ProgressBar } from "../../../../Components";

import { Icon, GoToast } from "../../../../Components";
import { IGNITE } from "../../Assets/Img/Image";
import I18n from "../../Assets/Strings/i18n";
import ApplicationToken from "../../../../CustomModules/ApplicationToken";
import { igniteGetAllotedBooks } from "../Home/Saga";
import { igniteAddBookBookmark } from "../BookDetail/Saga";
import { CommonActions } from "@react-navigation/native";
const { width, height } = Dimensions.get("window");
class Splash extends Component {
	constructor(props) {
		super(props);
		this.state = {
			barProgress: 5
		};
	}

	componentDidMount() {
		let self = this;
		this.getItemFromStore();
		this.interval = setInterval(() => {
			if (self.state.barProgress <= 100) {
				self.setState({ barProgress: self.state.barProgress + 5 });
			}
		}, 100);
	}

	navigateToNextScreen = props => {
		this.props.navigation.dispatch(
			CommonActions.reset({
				index: 0,
				routes: [{ name: "Home" }]
			})
		);
		this.props.dispatch(
			igniteGetAllotedBooks({
				appToken: props.appToken
			})
		);
	};

	async getItemFromStore() {
		try {
			let books = JSON.parse(await AsyncStorage.getItem("ignite_books"));
			if (books && books.length > 0 && books instanceof Array) {
				this.props.dispatch(igniteAddBookBookmark(books));
			}
		} catch (error) {
			console.log("unable to fetch from local store", error);
		}
	}
	shouldComponentUpdate(props, state) {
		// check for appToken and then navigate to home screen
		console.log("state is", state);
		if (props.appToken === null && props.appToken !== this.props.appToken) {
			// @ts-ignore
			GoToast.show(I18n.t("token_error"), I18n.t("error"), "SHORT");
		}

		if (parseInt(state.barProgress) === 100 && props.appToken !== null) {
			clearInterval(this.interval);
			this.navigateToNextScreen(props);
		}
		return true;
	}

	render() {
		return (
			<View
				style={{
					flex: 1,
					backgroundColor: "#ffffff",
					justifyContent: "center",
					alignItems: "center"
				}}>
				<ApplicationToken />
				<Image
					source={IGNITE}
					style={{ width: width / 5, height: height / 5 }}
					resizeMode={"contain"}
				/>
				<ProgressBar
					width={width / 5}
					height={height / 200}
					value={this.state.barProgress}
					backgroundColor="#e75f5c"
					barEasing={"linear"}
				/>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		books: state.ignite && state.ignite.home.books,
		isBookLoadError: state.ignite && state.ignite.home.isBookLoadError,
		appToken: state.appToken.token
	};
}

export default connect(mapStateToProps)(Splash);
