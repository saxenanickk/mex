import React, { Component } from "react";
import {
	Text,
	View,
	StyleSheet,
	Dimensions,
	TextInput,
	FlatList,
	Image,
	TouchableOpacity,
	Platform
} from "react-native";
import { connect } from "react-redux";
import { Icon, ProgressScreen, GoToast } from "../../../../Components";
import { igniteSaveBookDetail } from "../BookDetail/Saga";
import {
	IgniteTextLight,
	IgniteTextRegular,
	IgniteTextMedium,
	IgniteTextBold
} from "../../Components/IgniteText";
import { font_two, font_one, font_three } from "../../../../Assets/styles";
import {
	IGNITE,
	BOOK,
	OPENBOOK,
	PICKUPBOOK,
	RETURNBOOK
} from "../../Assets/Img/Image";
import { igniteGetAllotedBooks, igniteSaveAllotedBooks } from "../Home/Saga";
import I18n from "../../Assets/Strings/i18n";
const { height, width } = Dimensions.get("window");

/**
 * this Component will render the Ordered books
 */
class MyBooks extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.renderBooks = this.renderBooks.bind(this);
	}

	componentDidMount() {
		this.props.dispatch(igniteSaveAllotedBooks(null));
		this.props.dispatch(
			igniteGetAllotedBooks({
				appToken: this.props.appToken
			})
		);
	}
	/**
	 * function will render a row of Ordered books
	 */
	renderBooks = ({ item }) => {
		const { book_info } = item;
		return (
			<TouchableOpacity
				style={styles.bookView}
				onPress={() => {
					//this.props.dispatch(igniteSaveBookDetail(book_info))
					this.props.navigation.navigate("BookDetail", {
						bookId: item.book_id,
						toReturn: true,
						copyId: book_info.copy_id
					});
				}}>
				<View style={styles.innerBookView}>
					<Image
						style={styles.coverImage}
						source={{ uri: book_info.image }}
						resizeMode={"cover"}
					/>
					<View>
						<IgniteTextMedium style={styles.bookName} numberOfLines={1}>
							{book_info.name}
						</IgniteTextMedium>
						<IgniteTextRegular style={styles.writerName} numberOfLines={1}>
							{book_info.author}
						</IgniteTextRegular>
					</View>
				</View>
				<Icon
					iconType={"ionicon"}
					iconSize={height / 33}
					iconName={"ios-play"}
					iconColor={"#656565"}
				/>
			</TouchableOpacity>
		);
	};
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<TouchableOpacity
						onPress={() => this.props.navigation.goBack()}
						style={{
							position: "absolute",
							top: height / 40,
							left: width / 20
						}}>
						<Icon
							iconType={"ionicon"}
							iconSize={height / 18}
							iconName={"ios-arrow-round-back"}
							iconColor={"#767676"}
						/>
					</TouchableOpacity>
					{/* <Image
						source={IGNITE}
						style={{ width: width / 12, height: height / 18 }}
						resizeMode={"contain"}
					/> */}
					<View style={styles.subHeader}>
						{/* <IgniteTextBold style={styles.headerPrimaryText}>
							{I18n.t("ignite_header")}
						</IgniteTextBold> */}
						<IgniteTextBold style={styles.headerPrimaryText}>
							{"Alloted Books"}
							{/* {"My Orders"} */}
						</IgniteTextBold>
					</View>
				</View>
				{this.props.books !== null ? (
					<View style={styles.searchedContainer}>
						{/* <View>
							<IgniteTextMedium style={styles.searchHeader}>
								{"Alloted Books"}
							</IgniteTextMedium>
						</View> */}
						<FlatList
							data={this.props.books}
							renderItem={this.renderBooks}
							style={styles.searchList}
							showsVerticalScrollIndicator={false}
						/>
					</View>
				) : (
					<View style={styles.progressContainer}>
						<ProgressScreen color={"#f9a35d"} />
					</View>
				)}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: { flex: 1, backgroundColor: "#f7f7f7" },
	header: {
		height: height / 11,
		width: width,
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#fff",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	subHeader: {
		height: height / 11,
		justifyContent: "center",
		alignItems: "flex-start"
		// marginLeft: width / 30,
	},
	headerPrimaryText: {
		color: "#505050",
		fontSize: height / 50
	},
	headerSecondaryText: {
		color: "#919191",
		fontSize: height / 55
	},
	searchBar: {
		width: width / 1.1,
		alignSelf: "center",
		backgroundColor: "#fff",
		height: height / 15,
		borderRadius: width / 60,
		marginVertical: height / 22,
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: width / 20,
		alignItems: "center",
		...Platform.select({
			android: { elevation: 4 },
			ios: {
				shadowOffset: { width: 1.5, height: 1.5 },
				shadowColor: "black",
				shadowOpacity: 0.15
			}
		})
	},
	textInput: {
		width: width / 1.5,
		fontFamily: font_three,
		fontSize: height / 50,
		color: "#838383"
	},
	searchedContainer: {
		flex: 1,
		width: width / 1.1,
		alignSelf: "center",
		marginTop: height / 30
	},
	searchHeader: {
		fontSize: height / 45,
		marginBottom: height / 60,
		color: "#505050"
	},
	searchList: {
		width: width / 1.1,
		height: height
	},
	bookView: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		width: width / 1.1,
		backgroundColor: "#f7f7f7",
		borderBottomWidth: 1,
		borderBottomColor: "#e6e6e6",
		paddingVertical: height / 50,
		paddingHorizontal: width / 30
	},
	innerBookView: {
		flexDirection: "row",
		alignItems: "center",
		width: width / 1.7,
		backgroundColor: "#f7f7f7"
	},
	coverImage: {
		width: width / 9,
		height: height / 11,
		borderRadius: width / 60
	},
	bookName: {
		fontSize: height / 50,
		color: "#1c1c1c",
		width: width / 2,
		marginLeft: width / 30
	},
	writerName: {
		fontSize: height / 55,
		color: "#1c1c1c",
		width: width / 2,
		marginLeft: width / 30
	},
	progressContainer: { flex: 1 }
});
function mapStateToProps(state) {
	return {
		books: state.ignite && state.ignite.home.allotedBooks,
		appToken: state.appToken.token
	};
}

export default connect(mapStateToProps)(MyBooks);
