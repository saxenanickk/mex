import { takeLatest, takeEvery, put, call, all } from "redux-saga/effects";
import Api from "../../Api";
import { igniteGetBooks, igniteGetAllotedBooks } from "../Home/Saga";
//export const IGNITE_SAVE_BOOK_DETAIL = "IGNITE_SAVE_BOOK_DETAIL"
export const IGNITE_SAVE_BOOK_BOOKMARK = "IGNITE_SAVE_BOOK_BOOKMARK";
export const IGNITE_REMOVE_BOOK_BOOKMARK = "IGNITE_REMOVE_BOOK_BOOKMARK";
export const IGNITE_GET_BOOK_DETAIL = "IGNITE_GET_BOOK_DETAIL";
export const IGNITE_SAVE_BOOK_DETAIL_ERROR = "IGNITE_SAVE_BOOK_DETAIL_ERROR";
export const IGNITE_SAVE_BOOK_DETAIL = "SAVE_BOOK_DETAIL";
export const IGNITE_SAVE_BOOK_ELIGIBILITY = "IGNITE_SAVE_BOOK_ELIGIBILITY";
export const IGNITE_REQUEST_BOOK = "IGNITE_REQUEST_BOOK";
export const IGNITE_SAVE_REQUEST_BOOK_RESPONSE =
	"IGNITE_SAVE_REQUEST_BOOK_RESPONSE";
export const IGNITE_RETURN_BOOK = "IGNITE_RETURN_BOOK";
export const IGNITE_RETURN_BOOK_RESPONSE = "IGNITE_RETURN_BOOK_RESPONSE";
export const IGNITE_VALIDATE_SCANNED_QRHASH = "IGNITE_VALIDATE_SCANNED_QRHASH";
export const IGNITE_DIFFERENT_BOOK_SCANNED = "IGNITE_DIFFERENT_BOOK_SCANNED";
export const IGNITE_ADD_BOOK_BOOKMARK = "IGNITE_ADD_BOOK_BOOKMARK";

export const igniteAddBookBookmark = payload => ({
	type: IGNITE_ADD_BOOK_BOOKMARK,
	payload
});
export const igniteSaveBookDetail = payload => ({
	type: IGNITE_SAVE_BOOK_DETAIL,
	payload
});
export const igniteSaveBookMark = payload => ({
	type: IGNITE_SAVE_BOOK_BOOKMARK,
	payload
});
export const igniteRemoveBookMark = payload => ({
	type: IGNITE_REMOVE_BOOK_BOOKMARK,
	payload
});
export const igniteGetBookDetail = payload => ({
	type: IGNITE_GET_BOOK_DETAIL,
	payload
});
export const igniteSaveBookDetailError = payload => ({
	type: IGNITE_SAVE_BOOK_DETAIL_ERROR,
	payload
});
export const igniteSaveBookEligibility = payload => ({
	type: IGNITE_SAVE_BOOK_ELIGIBILITY,
	payload
});
export const igniteRequestBook = payload => ({
	type: IGNITE_REQUEST_BOOK,
	payload
});
export const igniteSaveRequestBookResponse = payload => ({
	type: IGNITE_SAVE_REQUEST_BOOK_RESPONSE,
	payload
});
export const igniteReturnBook = payload => ({
	type: IGNITE_RETURN_BOOK,
	payload
});
export const igniteReturnBookResponse = payload => ({
	type: IGNITE_RETURN_BOOK_RESPONSE,
	payload
});
export const igniteDifferentBookScanned = payload => ({
	type: IGNITE_DIFFERENT_BOOK_SCANNED,
	payload
});
export const igniteValidateScannedQrHash = payload => ({
	type: IGNITE_VALIDATE_SCANNED_QRHASH,
	payload
});
export function* igniteBookDetailSaga(dispatch) {
	yield takeLatest(IGNITE_GET_BOOK_DETAIL, handleGetBookDetail);
	yield takeLatest(IGNITE_REQUEST_BOOK, handleRequestBook);
	yield takeLatest(IGNITE_RETURN_BOOK, handleReturnBook);
	yield takeLatest(IGNITE_VALIDATE_SCANNED_QRHASH, handleValidateScannedQrHash);
}

function* handleGetBookDetail(action) {
	try {
		const [detailResponse, eligibilityResponse] = yield all([
			call(Api.getBookDetail, action.payload),
			call(Api.isUserEligibleToTakeBook, action.payload)
		]);
		if (detailResponse.success === 1) {
			yield put(igniteSaveBookDetail(detailResponse.data));
			yield put(igniteSaveBookEligibility(eligibilityResponse.success === 1));
		} else {
			yield put(igniteSaveBookDetailError(true));
		}
	} catch (error) {
		yield put(igniteSaveBookDetailError(true));
	}
}

function* handleRequestBook(action) {
	try {
		const response = yield call(Api.pickBook, action.payload);
		console.log("response is", response);
		if (response.success === 1) {
			const eligibility = yield call(
				Api.isUserEligibleToTakeBook,
				action.payload
			);
			yield put(igniteGetBookDetail(action.payload));
			yield put(igniteSaveRequestBookResponse(true));
			yield put(igniteSaveBookEligibility(eligibility.success === 1));
		} else {
			yield put(igniteSaveRequestBookResponse(false));
		}
	} catch (error) {
		console.log("error is", error);
		yield put(igniteSaveRequestBookResponse(false));
	}
}

function* handleReturnBook(action) {
	try {
		if (action.payload.qrhash == undefined || action.payload.qrhash === null) {
			const qrHashResponse = yield call(Api.getPodHash, action.payload);
			if (qrHashResponse.success === 1) {
				const response = yield call(Api.returnBook, {
					appToken: action.payload.appToken,
					qrhash: qrHashResponse.data.hash,
					copyId: action.payload.copyId
				});
				console.log("response is", response);
				if (response.success === 1) {
					const eligibility = yield call(Api.isUserEligibleToTakeBook, {
						appToken: action.payload.appToken,
						bookId: action.payload.bookId
					});
					yield put(igniteGetBookDetail(action.payload));
					yield put(igniteReturnBookResponse(true));
					yield put(igniteSaveBookEligibility(eligibility.success === 1));
				} else {
					yield put(igniteReturnBookResponse(false));
				}
			} else {
				yield put(igniteReturnBookResponse(false));
			}
		} else {
			const response = yield call(Api.returnBook, {
				appToken: action.payload.appToken,
				qrhash: action.payload.qrhash,
				copyId: action.payload.copyId
			});
			console.log("response is", response);
			if (response.success === 1) {
				const eligibility = yield call(Api.isUserEligibleToTakeBook, {
					appToken: action.payload.appToken,
					bookId: action.payload.bookId
				});
				yield put(igniteReturnBookResponse(true));
				yield put(igniteSaveBookEligibility(eligibility.success === 1));
			} else {
				yield put(igniteReturnBookResponse(false));
			}
		}
	} catch (error) {
		console.log("error is", error);
		yield put(igniteReturnBookResponse(false));
	}
}

function* handleValidateScannedQrHash(action) {
	try {
		let response = yield call(Api.getBookDetailFromQRHash, action.payload);
		if (response.success === 1) {
			if (response.data.book_details.book_id === action.payload.bookId) {
				yield put(igniteDifferentBookScanned(false));
			} else {
				yield put(igniteDifferentBookScanned(true));
			}
		} else {
			yield put(igniteDifferentBookScanned(true));
		}
	} catch (error) {
		yield put(igniteDifferentBookScanned(true));
	}
}
