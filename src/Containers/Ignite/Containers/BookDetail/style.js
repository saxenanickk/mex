import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: { flex: 1, backgroundColor: "#f7f7f7" },
	header: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: width / 25,
		height: height / 12,
		alignItems: "center",
		backgroundColor: "#fff",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	detailHeader: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: width / 25,
		paddingVertical: height / 40,
		alignItems: "center"
	},
	coverImage: {
		width: width / 3.5,
		height: height / 4,
		borderRadius: width / 30
	},
	detailTextPart: {
		flex: 1,
		height: height / 4,
		marginLeft: width / 30,
		justifyContent: "space-around"
	},
	bookSemiDetailText: {
		color: "#afafaf",
		fontSize: height / 55,
		width: width / 3.5
	},
	bookDetailMainText: {
		color: "#afafaf",
		fontSize: height / 55
	},
	bookTextAlignEnd: {
		textAlign: "right"
	},
	justifiedCenter: {
		justifyContent: "center"
	},
	separator: {
		// height: height / 30,
		width: width / 14,
		justifyContent: "center",
		alignItems: "center"
	},
	dot: {
		width: width / 60,
		height: width / 60,
		borderRadius: width / 120,
		backgroundColor: "#d8d8d8"
	},
	bookmarkButton: {
		flexDirection: "row",
		height: height / 19,
		paddingHorizontal: width / 40,
		backgroundColor: "#f9a35d",
		alignItems: "center",
		alignSelf: "flex-start",
		justifyContent: "space-between",
		borderRadius: width / 40
	},
	bookmarkButtonText: {
		color: "#fff",
		fontSize: height / 50,
		paddingRight: width / 40
	},
	availableSection: {
		flexDirection: "row",
		paddingHorizontal: width / 40,
		alignItems: "center",
		marginTop: height / 50,
		justifyContent: "space-between"
	},
	centerAlignedView: {
		alignItems: "center"
	},
	availableText: {
		fontSize: height / 50,
		color: "#afafaf"
	},
	separatorLine: {
		flexDirection: "row",
		height: height / 600,
		backgroundColor: "#d8d8d8",
		width: width / 1.1,
		marginVertical: height / 40,
		alignSelf: "center"
	},
	podView: {
		justifyContent: "center",
		alignItems: "center"
	},
	addressPart: {
		paddingLeft: width / 20
	},
	cowrksText: {
		fontSize: height / 45,
		color: "#000"
	},
	addressText: {
		fontSize: height / 60,
		color: "#afafaf",
		width: width / 2
	},
	podAddressText: {
		fontSize: height / 70,
		color: "#afafaf",
		width: width / 2
	},
	podHeader: {
		flexDirection: "row",
		justifyContent: "flex-start",
		paddingHorizontal: width / 25,
		alignItems: "center"
	},
	prologueView: {
		justifyContent: "flex-start",
		paddingHorizontal: width / 25
	},
	prologueText: {
		color: "#afafaf",
		fontSize: height / 40
	},
	prologueInfoText: {
		width: width / 1.2,
		fontSize: height / 45,
		marginTop: height / 40
	},
	returnButton: {
		width: width / 2.6,
		paddingVertical: height / 60,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "center",
		backgroundColor: "#43adff",
		marginVertical: height / 50,
		borderRadius: width / 90,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	similarBookSection: {
		height: height / 3,
		backgroundColor: "#f8fbff",
		marginTop: height / 80,
		paddingHorizontal: width / 25
	},
	similarBookImage: {
		width: width / 2.5,
		height: height / 3.5,
		borderRadius: width / 50,
		marginRight: width / 30
	},
	lowerSection: {
		backgroundColor: "#f8fbff",
		marginTop: height / 40
	},
	similarBookText: {
		marginLeft: width / 25,
		color: "#000",
		fontSize: height / 40,
		marginTop: height / 80
	},
	returnOpen: {
		position: "absolute",
		top: 0,
		left: 0,
		bottom: 0,
		right: 0,
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "rgba(0,0,0,0.9)"
	},
	returnClose: {
		position: "absolute",
		top: height,
		flex: 1
	},
	box: {
		width: width / 1.1,
		borderRadius: width / 30,
		backgroundColor: "#fff",
		alignItems: "center",
		height: height / 3,
		paddingVertical: height / 40
	},
	bookBox: {
		width: width / 1.1,
		borderRadius: width / 30,
		backgroundColor: "#fff",
		alignItems: "center",
		height: height / 2.2,
		zIndex: 5,
		paddingVertical: height / 40
	},
	returnBoxText: {
		color: "#7a7a7a",
		fontSize: height / 45,
		textDecorationLine: "underline"
	},
	takenBookSection: {
		marginTop: height / 60
	},
	takenBookRow: {
		flexDirection: "row",
		width: width / 1.5,
		alignItems: "center",
		marginTop: height / 60
	},
	takenBookText: {
		color: "#828282",
		fontSize: height / 50,
		marginLeft: width / 30
	},
	returnTypeSection: {
		flexDirection: "row",
		flex: 1,
		marginTop: height / 60,
		alignItems: "center",
		justifyContent: "center"
	},
	scanText: {
		color: "#7a7a7a",
		fontSize: height / 50,
		textDecorationLine: "underline"
	},
	scanPodText: {
		color: "#7a7a7a",
		marginTop: height / 60,
		fontSize: height / 65
	},
	qrImage: {
		width: width / 4,
		height: height / 9,
		alignSelf: "center",
		marginTop: height / 30
	},
	returnTypeSeparator: {
		width: width / 7,
		alignItems: "center",
		alignSelf: "center",
		height: height / 5,
		justifyContent: "space-between"
	},
	innerReturnButton: {
		paddingVertical: height / 100,
		paddingHorizontal: height / 100,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "center",
		backgroundColor: "#43adff",
		marginTop: height / 50,
		borderRadius: 5
	},
	headerInsideBox: {
		fontSize: height / 40,
		fontWeight: "600",
		color: "#6E6E6E"
	},
	childInsideBox: {
		paddingTop: height / 50
	},
	authorName: {
		fontSize: height / 60,
		color: "#AFAFAF"
	},
	bookName: {
		fontSize: height / 45,
		color: "#777777",
		width: width / 1.7
	},
	orText: {
		fontSize: height / 45,
		color: "#777777"
	},
	pickUp: {
		backgroundColor: "#38AFFE",
		width: width / 2.2,
		height: height / 15,
		borderRadius: 10,
		alignItems: "center",
		justifyContent: "center",
		marginTop: height / 25
	},
	pickUpText: {
		color: "#fff",
		fontWeight: "bold"
	},
	takeBookBox: {
		width: width / 1.3,
		height: height / 2.3,
		borderRadius: 10,
		alignItems: "center"
	},
	requestBookButtonText: {
		color: "#fff",
		fontSize: height / 50
	},
	cameraContainer: {
		justifyContent: "center",
		alignItems: "center",
		position: "absolute",
		top: height / 12,
		elevation: 4,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		height: height - height / 12,
		backgroundColor: "#fff"
	},
	camera: {
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "transparent",
		width: width,
		height: height - height / 9
	},
	rectangleContainer: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "transparent"
	},
	barRecatangle: {
		position: "absolute",
		top: height / 4,
		borderColor: "#38AFFE",
		borderWidth: 1,
		width: width / 1.2,
		height: height / 2.4,
		alignItems: "center"
	},
	welcome: {
		fontSize: 20,
		textAlign: "center",
		margin: 10
	},
	instructions: {
		textAlign: "center",
		color: "#333333",
		marginBottom: 5
	},
	daysText: {
		color: "#000",
		fontSize: height / 50,
		marginLeft: width / 60
	},
	takeBox: {
		width: width / 1.1,
		borderRadius: width / 30,
		backgroundColor: "#fff",
		alignItems: "center"
	},
	bottomMargin: {
		borderBottomWidth: 1,
		borderBottomColor: "#efefef"
	},
	authorView: {
		flexDirection: "row",
		alignItems: "center",
		width: width / 1.7
	},
	openBookImage: { width: width / 18, height: height / 30 },
	podImage: { width: width / 12, height: height / 18 },
	animatedBarLine: {
		position: "absolute",
		borderWidth: 1,
		borderColor: "#38AFFE",
		width: width / 1.2
	},
	centeredRow: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center"
	},
	dotIcon: {
		width: width / 9,
		alignItems: "center"
	},
	scannerTopContainer: {
		marginTop: height / 10
	},
	viewMoreText: {
		fontSize: height / 65,
		marginTop: height / 250,
		color: "#2d76ed"
	},
	morePodDialog: {
		paddingVertical: height / 80,
		width: width / 1.5,
		alignSelf: "center",
		backgroundColor: "#fff",
		borderRadius: width / 60
	},
	morePodView: {
		paddingVertical: height / 100,
		marginHorizontal: width / 30,
		justifyContent: "center",
		borderBottomWidth: 1,
		borderBottomColor: "#ececec"
	},
	smallPodImage: {
		height: height / 40,
		width: height / 40
	},
	morePodText: {
		fontSize: height / 50,
		marginLeft: width / 30
	},
	morePodOkButton: {
		paddingVertical: height / 60,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "center",
		backgroundColor: "#43adff",
		marginVertical: height / 50,
		flexDirection: "row",
		width: width / 1.5 - width / 15,
		borderRadius: width / 90,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	morePodAddressText: {
		fontSize: height / 60,
		marginLeft: width / 30
	}
});
