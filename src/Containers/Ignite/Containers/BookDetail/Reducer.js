import {
	IGNITE_SAVE_BOOK_DETAIL,
	IGNITE_SAVE_BOOK_BOOKMARK,
	IGNITE_REMOVE_BOOK_BOOKMARK,
	IGNITE_SAVE_BOOK_ELIGIBILITY,
	IGNITE_SAVE_BOOK_DETAIL_ERROR,
	IGNITE_SAVE_REQUEST_BOOK_RESPONSE,
	IGNITE_RETURN_BOOK_RESPONSE,
	IGNITE_DIFFERENT_BOOK_SCANNED,
	IGNITE_ADD_BOOK_BOOKMARK
} from "./Saga";

const initialState = {
	bookDetail: null,
	bookmark: [],
	bookDetailError: null,
	isBookTaken: false,
	bookRequestResponse: null,
	bookReturnResponse: null,
	isDifferentBookScanned: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case IGNITE_SAVE_BOOK_DETAIL:
			return {
				...state,
				bookDetail: action.payload
			};
		case IGNITE_SAVE_BOOK_BOOKMARK:
			return {
				...state,
				bookmark: [...state.bookmark, action.payload]
			};
		case IGNITE_REMOVE_BOOK_BOOKMARK:
			let temp = state.bookmark.slice();
			let bookIndex = state.bookmark.findIndex(
				row => row.name === action.payload.name
			);
			if (bookIndex >= 0) {
				temp = temp
					.slice(0, bookIndex)
					.concat(temp.slice(bookIndex + 1, temp.length));
			}
			return {
				...state,
				bookmark: temp
			};
		case IGNITE_SAVE_BOOK_DETAIL_ERROR:
			return {
				...state,
				bookDetailError: action.payload
			};
		case IGNITE_SAVE_BOOK_ELIGIBILITY:
			return {
				...state,
				isBookTaken: action.payload
			};
		case IGNITE_SAVE_REQUEST_BOOK_RESPONSE:
			return {
				...state,
				bookRequestResponse: action.payload
			};
		case IGNITE_RETURN_BOOK_RESPONSE:
			return {
				...state,
				bookReturnResponse: action.payload
			};
		case IGNITE_DIFFERENT_BOOK_SCANNED:
			return {
				...state,
				isDifferentBookScanned: action.payload
			};
		case IGNITE_ADD_BOOK_BOOKMARK:
			return {
				...state,
				bookmark: action.payload
			};
		default:
			return state;
	}
};
