import React, { Component } from "react";
import {
	View,
	Dimensions,
	FlatList,
	Image,
	ScrollView,
	TouchableOpacity,
	BackHandler,
	Animated,
	Easing,
	Alert,
	Platform
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { CameraKitCamera } from "react-native-camera-kit";
import { connect } from "react-redux";
import {
	Icon,
	ProgressScreen,
	Seperator,
	QRCodeReader
} from "../../../../Components";
import {
	IgniteTextRegular,
	IgniteTextMedium,
	IgniteTextBold
} from "../../Components/IgniteText";
import { OPENBOOK, QRCODE, PODS } from "../../Assets/Img/Image";
import {
	igniteSaveBookMark,
	igniteRemoveBookMark,
	igniteSaveBookDetail,
	igniteRequestBook,
	igniteGetBookDetail,
	igniteReturnBook,
	igniteReturnBookResponse,
	igniteSaveRequestBookResponse,
	igniteValidateScannedQrHash,
	igniteDifferentBookScanned
} from "./Saga";
import I18n from "../../Assets/Strings/i18n";
import { styles } from "./style";
import { igniteGetBooks, igniteGetAllotedBooks } from "../Home/Saga";
import GoPicker from "../../../../Components/GoPicker";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import ReadMore from "../../../../Components/ReadMore";
import { CommonActions } from "@react-navigation/native";

const { height, width } = Dimensions.get("window");

/**
 * This Component will render detail of a book and
 * provide option to pick or return book
 */
class BookDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			openDialog: false,
			bookToReturn: [],
			selectedPod: null,
			scanning: false,
			days: 1,
			requesting: false,
			morePod: false
		};
		this.animatedValue = new Animated.Value(0);
		this.similarBooks = [];
		this.addToBookMark = this.addToBookMark.bind(this);
		this.isBookMarked = this.isBookMarked.bind(this);
		this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
			this.closeBookDetail();
			return true;
		});
		this.handleBarCodeRead = this._handleBarCodeRead.bind(this);
		this.dayArray = [{ day: 1 }, { day: 7 }, { day: 14 }, { day: 28 }];
		this.hashkey = null;

		const { route } = props;
		const { copyId = null, bookId = null, toReturn = false } = route.params
			? route.params
			: {};

		this.copyId = copyId;
		this.bookId = bookId;
		this.toReturn = toReturn;
	}

	/**
	 * function to bookmark a book
	 * @param  {Object} bookDetail javascript object if book which needs to be bookmarked
	 */
	addToBookMark = bookDetail => {
		console.log("add to bookmark");

		let findIndex = this.props.bookmark.findIndex(
			row => row.name === bookDetail.name
		);
		//check whether book is available in the bookmark list or not
		if (findIndex >= 0) {
			//remove from bookmark if book found
			this.props.dispatch(igniteRemoveBookMark(bookDetail));
			this.addOrRemoveBookFromLocalStore(bookDetail, false);
		} else {
			//add to bookmark if book not found
			this.props.dispatch(igniteSaveBookMark(bookDetail));
			this.addOrRemoveBookFromLocalStore(bookDetail, true);
		}
	};

	async addOrRemoveBookFromLocalStore(bookDetail, isAdd) {
		try {
			if (isAdd === true) {
				let books = JSON.parse(await AsyncStorage.getItem("ignite_books"));
				if (books === null || books === undefined) {
					books = [];
				}
				books.push(bookDetail);
				AsyncStorage.setItem("ignite_books", JSON.stringify(books));
			} else {
				let books = JSON.parse(await AsyncStorage.getItem("ignite_books"));
				let temp = books.slice();
				let bookIndex = books.findIndex(row => row.name === bookDetail.name);
				if (bookIndex >= 0) {
					temp = temp
						.slice(0, bookIndex)
						.concat(temp.slice(bookIndex + 1, temp.length));
				}
				AsyncStorage.setItem("ignite_books", JSON.stringify(temp));
			}
		} catch (error) {
			console.log("error in storing", error);
		}
	}
	/**
	 * function to check whether a book is bookmarked or not
	 * @param  {Object} bookDetail javascript object if book which needs to be bookmarked
	 * @return {boolean}         true if book is found else false
	 */
	isBookMarked = bookDetail => {
		let findIndex = this.props.bookmark.findIndex(
			row => row.name === bookDetail.name
		);
		return findIndex >= 0;
	};

	/**
	 * function will add the book to return list
	 * @param  {Object} item [book which is selected to return]
	 */
	addToReturnBook = item => {
		let index = this.state.bookToReturn.findIndex(
			row => row.name === item.name
		);
		if (index >= 0) {
			//check if book is already available in the list then remove it
			let temp = this.state.bookToReturn.slice();
			temp = temp.slice(0, index).concat(temp.slice(index + 1, temp.length));
			this.setState({ bookToReturn: temp });
		} else {
			//if not available in the list then add it
			this.setState({ bookToReturn: [...this.state.bookToReturn, item] });
		}
	};

	isBookAvailableAtThisCampus = () => {
		try {
			let availableBooth = this.getUniqueBooths(
				this.props.bookDetail.booth_info
			);
			return availableBooth.length > 0;
		} catch (error) {
			return false;
		}
	};
	/**
	 * Component to render the return dialog box
	 */
	renderReturnDialogBox = () => {
		return (
			<TouchableOpacity style={styles.box} activeOpacity={1}>
				<IgniteTextMedium style={styles.returnBoxText} numberOfLines={1}>
					{I18n.t("return_details")}
				</IgniteTextMedium>
				<View style={styles.returnTypeSection}>
					<View>
						<IgniteTextMedium style={styles.scanText} numberOfLines={1}>
							{I18n.t("scan_and_return")}
						</IgniteTextMedium>
						<IgniteTextMedium style={styles.scanPodText} numberOfLines={1}>
							{I18n.t("scan_qr_code")}
						</IgniteTextMedium>
						<TouchableOpacity
							onPress={() => {
								console.log("open the qr code");
								this.handleOpenCamera();
								this.animate();
							}}>
							<Image
								source={QRCODE}
								style={styles.qrImage}
								resizeMode={"contain"}
							/>
						</TouchableOpacity>
					</View>
					<View style={styles.returnTypeSeparator}>
						<Seperator height={height / 15} width={1} color={"#e9e9e9"} />
						<IgniteTextMedium style={styles.orText} numberOfLines={1}>
							{I18n.t("caps_or")}
						</IgniteTextMedium>
						<Seperator height={height / 15} width={1} color={"#e9e9e9"} />
					</View>
					<View>
						<IgniteTextMedium style={styles.scanText} numberOfLines={1}>
							{I18n.t("select_and_return")}
						</IgniteTextMedium>
						<IgniteTextMedium style={styles.scanPodText} numberOfLines={1}>
							{I18n.t("which_pod")}
						</IgniteTextMedium>
						{this.props.bookDetail && (
							<GoPicker
								selectedValue={this.state.selectedPod}
								data={this.getBoothAtThisCampus()}
								updateSelectedValue={({ itemValue }) => {
									this.setState({ selectedPod: itemValue });
								}}
								pickerKey={"id"}
								label={"name"}
								value={"name"}
								style={{ width: width / 3 }}
							/>
						)}

						<TouchableOpacity
							style={styles.innerReturnButton}
							onPress={() => {
								const boothId = this.props.booths.filter(
									row => row.name === this.state.selectedPod
								)[0].id;
								GoAppAnalytics.trackWithProperties("return_book", {
									copyId: this.copyId,
									bookId: this.bookId,
									boothId
								});
								this.props.dispatch(
									igniteReturnBook({
										appToken: this.props.appToken,
										copyId: this.copyId,
										boothId,
										bookId: this.bookId
									})
								);
								this.setState({ openDialog: false, requesting: true });
							}}>
							<IgniteTextMedium
								style={styles.requestBookButtonText}
								numberOfLines={1}>
								{I18n.t("return_book")}
							</IgniteTextMedium>
						</TouchableOpacity>
					</View>
				</View>
			</TouchableOpacity>
		);
	};

	/**
	 * Component to render the dialog box to take books
	 */
	renderTakeBookDialogBox = () => {
		const { bookDetail } = this.props;
		if (bookDetail !== null) {
			return (
				<TouchableOpacity style={styles.bookBox} activeOpacity={1}>
					<IgniteTextMedium style={styles.headerInsideBox}>
						{I18n.t("book_details")}
					</IgniteTextMedium>
					<View style={styles.childInsideBox}>
						<IgniteTextMedium style={styles.bookName}>
							{bookDetail.book_details.name}
						</IgniteTextMedium>
						<View style={[styles.centeredRow, styles.justifiedCenter]}>
							<IgniteTextRegular
								style={[styles.bookSemiDetailText, styles.bookTextAlignEnd]}
								numberOfLines={1}>
								{bookDetail.book_details.author}
							</IgniteTextRegular>
							<View style={styles.dotIcon}>
								<Icon
									iconType={"entypo"}
									iconSize={height / 18}
									iconName={"dot-single"}
									iconColor={"#767676"}
								/>
							</View>
							<IgniteTextRegular
								style={styles.bookSemiDetailText}
								numberOfLines={1}>
								{bookDetail.book_details.genre}
							</IgniteTextRegular>
						</View>
					</View>
					<View style={styles.bottomMargin}>
						<IgniteTextMedium style={styles.daysText}>
							{"Days"}
						</IgniteTextMedium>
						<GoPicker
							selectedValue={this.state.days}
							data={this.dayArray}
							updateSelectedValue={({ itemValue }) => {
								this.setState({ days: itemValue });
							}}
							pickerKey={"day"}
							label={"day"}
							value={"day"}
						/>
					</View>

					<TouchableOpacity
						style={styles.pickUp}
						onPress={() => {
							const bookId = this.bookId;

							GoAppAnalytics.trackWithProperties("take_book", {
								bookId
							});
							this.props.dispatch(
								igniteRequestBook({
									qrhash: this.hashkey,
									appToken: this.props.appToken,
									days: this.state.days,
									bookId
								})
							);
							this.setState({ openDialog: false, requesting: false });
						}}>
						<IgniteTextMedium style={styles.pickUpText}>
							{I18n.t("ignite_pick_up")}
						</IgniteTextMedium>
					</TouchableOpacity>
				</TouchableOpacity>
			);
		}
	};

	/**
	 * Component to render more Pods
	 */
	renderMorePodDialogbox = () => {
		return (
			<TouchableOpacity style={styles.morePodDialog} activeOpacity={1}>
				<FlatList
					data={this.getUniqueBooths(this.props.bookDetail.booth_info)}
					renderItem={({ item }) => (
						<View style={styles.morePodView}>
							<IgniteTextBold style={styles.morePodText}>
								{item.name}
							</IgniteTextBold>
							<IgniteTextMedium style={styles.morePodAddressText}>
								{item.address}
							</IgniteTextMedium>
						</View>
					)}
				/>
				<TouchableOpacity
					style={styles.morePodOkButton}
					onPress={() => this.setState({ openDialog: false, morePod: false })}>
					<IgniteTextMedium style={styles.requestBookButtonText}>
						{"CLOSE"}
					</IgniteTextMedium>
				</TouchableOpacity>
			</TouchableOpacity>
		);
	};
	/**
	 * check whether the book is added for return or not
	 * @param  {Object} item book
	 * @return {boolean}      true if book is added to return else false
	 */
	isBookAddedForReturn = item => {
		return (
			this.state.bookToReturn.findIndex(row => row.name === item.name) >= 0
		);
	};

	/**
	 * handler function to handle the closing of bookdetail page
	 */
	async closeBookDetail() {
		if (this.state.openDialog) {
			//if dialog box is open then close the dialog box
			this.setState({ openDialog: false });
		} else if (this.state.scanning) {
			this.setState({ scanning: false });
		} else {
			//else  go back to previous screen
			this.props.navigation.goBack();
		}
	}

	/**
	 * life cycle method which is invoked at the time of unmounting of component
	 */
	componentWillUnmount() {
		this.backHandler.remove();
		this.props.dispatch(igniteSaveBookDetail(null));
		this.props.dispatch(igniteReturnBookResponse(null));
		this.props.dispatch(igniteSaveRequestBookResponse(null));
	}

	shouldComponentUpdate(props, state) {
		if (
			props.bookRequestResponse !== null &&
			props.bookRequestResponse !== this.props.bookRequestResponse &&
			props.bookRequestResponse === true
		) {
			Alert.alert("Information", "Book Alloted successfully");
			this.setState({ requesting: false });
			this.props.dispatch(igniteSaveRequestBookResponse(null));
			this.props.dispatch(
				igniteGetBooks({
					appToken: this.props.appToken
				})
			);
			this.props.dispatch(
				igniteGetAllotedBooks({
					appToken: this.props.appToken
				})
			);
			this.props.navigation.goBack();
		}
		if (
			props.bookRequestResponse !== null &&
			props.bookRequestResponse !== this.props.bookRequestResponse &&
			props.bookRequestResponse === false
		) {
			Alert.alert("Information", "Unable to allot Books");
			this.props.dispatch(igniteSaveRequestBookResponse(null));
			this.setState({ requesting: false });
		}
		if (
			props.bookReturnResponse &&
			props.bookReturnResponse !== this.props.bookReturnResponse &&
			props.bookReturnResponse === true
		) {
			Alert.alert("Information", "Book Returned successfully");
			this.props.dispatch(igniteReturnBookResponse(null));
			this.props.dispatch(
				igniteGetBooks({
					appToken: this.props.appToken
				})
			);
			this.props.dispatch(
				igniteGetAllotedBooks({
					appToken: this.props.appToken
				})
			);
			this.setState({ requesting: false });
			this.props.navigation.navigate("Home");
		}
		if (
			props.bookReturnResponse !== null &&
			props.bookReturnResponse === false
		) {
			Alert.alert("Information", "Unable to return book");
			this.setState({ requesting: false });
			this.props.dispatch(igniteReturnBookResponse(null));
		}
		if (props.booths && props.booths !== this.props.booths) {
			if (this.props.location !== null) {
				this.setState({
					selectedPod: this.props.booths.filter(
						row => row.campus === this.props.location.name
					)[0].name
				});
			}
		}
		if (
			props.isDifferentBookScanned !== null &&
			props.isDifferentBookScanned !== this.props.isDifferentBookScanned &&
			props.isDifferentBookScanned === true
		) {
			this.props.dispatch(igniteDifferentBookScanned(null));
			this.setState({ requesting: false });
			Alert.alert("Information", "You have scanned a different book");
		}
		if (
			props.isDifferentBookScanned !== null &&
			props.isDifferentBookScanned !== this.props.isDifferentBookScanned &&
			props.isDifferentBookScanned === false
		) {
			this.props.dispatch(igniteDifferentBookScanned(null));
			this.setState({ requesting: false, openDialog: true });
		}
		return true;
	}

	_handleBarCodeRead(e) {
		if (!this.props.isBookTaken) {
			this.setState({ scanning: false, requesting: true });
			GoAppAnalytics.trackWithProperties("return_book", {
				copyId: this.copyId,
				bookId: this.bookId
			});
			this.props.dispatch(
				igniteReturnBook({
					appToken: this.props.appToken,
					copyId: this.copyId,
					bookId: this.bookId,
					qrhash: e.data
				})
			);
		} else {
			this.setState({ requesting: true, scanning: false });
			this.props.dispatch(
				igniteValidateScannedQrHash({
					appToken: this.props.appToken,
					bookId: this.bookId,
					qrHash: e.data
				})
			);
			this.hashkey = e.data;
		}
		console.log("bar code data is", e.data);
	}

	animate() {
		console.log("start the animation");
		this.animatedValue.setValue(0);
		Animated.timing(this.animatedValue, {
			toValue: 1,
			duration: 3000,
			easing: Easing.linear
		}).start(() => this.state.scanning && this.animate());
	}

	componentDidMount() {
		const bookId = this.bookId;
		this.props.dispatch(
			igniteGetBookDetail({
				appToken: this.props.appToken,
				bookId
			})
		);
		if (this.props.booths && this.props.booths.length > 0) {
			let selectedPod = this.props.booths.filter(
				row => row.campus === this.props.location.name
			);
			if (selectedPod.length > 0) {
				this.setState({
					selectedPod: selectedPod[0].name
				});
			}
		}
	}

	getUniqueBooths(allBooth) {
		try {
			let set = [];
			allBooth = allBooth.filter(
				row => row.campus === this.props.location.name
			);
			let boothArray = [];
			allBooth.map(item => {
				if (set.indexOf(item.id) < 0) {
					item = this.props.booths.filter(row => row.id === item.id);
					if (item !== null && item !== undefined && item.length > 0) {
						boothArray.push(item[0]);
						set.push(item[0].id);
					}
				}
			});
			return boothArray;
		} catch (error) {
			return [];
		}
	}

	getBoothAtThisCampus() {
		try {
			if (this.props.location === null) {
				return this.props.booths;
			} else {
				return this.props.booths.filter(
					row => row.campus === this.props.location.name
				);
			}
		} catch (error) {
			return [];
		}
	}

	handleOpenCamera = async () => {
		if (Platform.OS === "android") {
			const isCameraAuthorized = await CameraKitCamera.checkDeviceCameraAuthorizationStatus();
			if (isCameraAuthorized === 1) {
				this.setState({ scanning: true });
			} else {
				const isUserAuthorizedCamera = await CameraKitCamera.requestDeviceCameraAuthorization();
				console.log(isUserAuthorizedCamera);
			}
		} else {
			this.setState({ scanning: true });
		}
	};

	render() {
		const { bookDetail } = this.props;
		const toReturn = this.toReturn;

		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<TouchableOpacity onPress={() => this.closeBookDetail()}>
						<Icon
							iconType={"ionicon"}
							iconSize={height / 18}
							iconName={"ios-arrow-round-back"}
							iconColor={"#767676"}
						/>
					</TouchableOpacity>
					<TouchableOpacity
						disabled={this.props.bookmark.length === 0}
						onPress={() => {
							this.props.navigation.dispatch(
								CommonActions.reset({
									index: 0,
									routes: [{ name: "BookMark" }]
								})
							);
						}}>
						<Icon
							iconType={"font_awesome"}
							iconSize={height / 30}
							iconName={"bookmark"}
							iconColor={this.props.bookmark.length > 0 ? "#f9a35d" : "#777777"}
						/>
					</TouchableOpacity>
				</View>
				{this.props.bookDetail === null ? (
					<View style={styles.container}>
						<ProgressScreen color={"#38AFFE"} />
					</View>
				) : (
					<ScrollView showsVerticalScrollIndicator={false}>
						<View style={styles.detailHeader}>
							<View>
								<Image
									style={styles.coverImage}
									source={{ uri: bookDetail.book_details.image }}
									resizeMode={"cover"}
								/>
							</View>
							<View style={styles.detailTextPart}>
								<View>
									<IgniteTextBold style={styles.bookName} numberOfLines={2}>
										{bookDetail.book_details.name}
									</IgniteTextBold>
									<View style={styles.authorView}>
										<IgniteTextRegular
											style={styles.bookDetailMainText}
											numberOfLines={1}>
											{bookDetail.book_details.author}
										</IgniteTextRegular>
										<View style={styles.separator}>
											<View style={styles.dot} />
										</View>
										<View style={{ flex: 1 }}>
											<IgniteTextRegular
												style={styles.bookDetailMainText}
												numberOfLines={1}>
												{bookDetail.book_details.genre}
											</IgniteTextRegular>
										</View>
									</View>
								</View>
								<TouchableOpacity
									style={styles.bookmarkButton}
									onPress={() => this.addToBookMark(bookDetail.book_details)}>
									<IgniteTextMedium
										style={styles.bookmarkButtonText}
										numberOfLines={1}>
										{this.isBookMarked(bookDetail.book_details)
											? I18n.t("ignite_bookmarked")
											: I18n.t("ignite_bookmark")}
									</IgniteTextMedium>
									{!this.isBookMarked(bookDetail.book_details) && (
										<Icon
											iconType={"font_awesome"}
											iconSize={height / 40}
											iconName={"bookmark"}
											iconColor={"#fff"}
										/>
									)}
								</TouchableOpacity>
								<View style={styles.availableSection}>
									<View style={styles.centerAlignedView}>
										<Image
											source={OPENBOOK}
											style={styles.openBookImage}
											resizeMode={"contain"}
										/>
										<IgniteTextRegular
											numberOfLines={1}
											style={styles.availableText}>
											{this.isBookAvailableAtThisCampus()
												? I18n.t("ignite_available")
												: "Unavailable"}
										</IgniteTextRegular>
									</View>
								</View>
							</View>
						</View>
						<View style={styles.separatorLine} />
						<View style={styles.podHeader}>
							<View style={styles.podView}>
								<Image
									source={PODS}
									style={styles.podImage}
									resizeMode={"contain"}
								/>
								<IgniteTextRegular
									numberOfLines={1}
									style={styles.availableText}>
									{I18n.t("book_pods")}
								</IgniteTextRegular>
							</View>
							<View style={styles.addressPart}>
								<IgniteTextBold numberOfLines={1} style={styles.cowrksText}>
									{this.props.location !== null ? this.props.location.name : ""}
								</IgniteTextBold>
								{this.getUniqueBooths(bookDetail.booth_info).map(
									(item, index) => {
										switch (index) {
											case 0:
												return (
													<View key={index}>
														<IgniteTextRegular
															numberOfLines={1}
															style={styles.addressText}>
															{item.name}
														</IgniteTextRegular>
														<IgniteTextRegular
															numberOfLines={1}
															style={styles.podAddressText}>
															{item.address}
														</IgniteTextRegular>
													</View>
												);
											case 1:
												return (
													<TouchableOpacity
														onPress={() =>
															this.setState({ openDialog: true, morePod: true })
														}>
														<IgniteTextRegular
															numberOfLines={1}
															style={styles.viewMoreText}
															key={index}>
															{"VIEW MORE"}
														</IgniteTextRegular>
													</TouchableOpacity>
												);
											default:
												return null;
										}
									}
								)}
							</View>
						</View>
						<View style={styles.separatorLine} />
						<View style={styles.prologueView}>
							<IgniteTextMedium style={styles.prologueText} numberOfLines={1}>
								{"P R O L O G U E"}
							</IgniteTextMedium>
							<ReadMore numberOfLines={5}>
								{bookDetail.book_details.description}
							</ReadMore>
							<TouchableOpacity
								style={styles.returnButton}
								onPress={() => {
									if (!this.props.isBookTaken) {
										this.setState({ openDialog: true });
									} else {
										if (
											this.props.allotedBooks &&
											this.props.allotedBooks.length === 2
										) {
											Alert.alert(
												"",
												"You have reached maximum limit of book allotment"
											);
										} else if (!this.isBookAvailableAtThisCampus()) {
											Alert.alert(
												"",
												"Sorry, This book is not available at this campus"
											);
										} else {
											this.animate();
											this.handleOpenCamera();
										}
									}
								}}>
								<IgniteTextMedium
									style={styles.requestBookButtonText}
									numberOfLines={1}>
									{toReturn === false
										? this.props.isBookTaken
											? I18n.t("request_book")
											: I18n.t("return_book")
										: I18n.t("return_book")}
								</IgniteTextMedium>
							</TouchableOpacity>
						</View>
					</ScrollView>
				)}

				<TouchableOpacity
					onPress={() => this.setState({ openDialog: false, morePod: false })}
					style={
						this.state.openDialog ? styles.returnOpen : styles.returnClose
					}>
					{this.state.morePod
						? this.renderMorePodDialogbox()
						: !this.props.isBookTaken
						? this.renderReturnDialogBox()
						: this.renderTakeBookDialogBox()}
				</TouchableOpacity>
				{this.state.scanning && (
					<View style={styles.cameraContainer}>
						<View style={styles.rectangleContainer}>
							<QRCodeReader
								style={styles.camera}
								onRead={this.handleBarCodeRead}
							/>
						</View>
					</View>
				)}
				{this.state.requesting && <ProgressScreen color={"#f9a35d"} />}
			</View>
		);
	}
}

function mapStateToProps(state) {
	console.log("state is", state);
	return {
		bookDetail: state.ignite && state.ignite.bookDetail.bookDetail,
		bookmark: state.ignite && state.ignite.bookDetail.bookmark,
		bookDetailError: state.ignite && state.ignite.bookDetail.bookDetailError,
		isBookTaken: state.ignite && state.ignite.bookDetail.isBookTaken,
		bookRequestResponse:
			state.ignite && state.ignite.bookDetail.bookRequestResponse,
		appToken: state.appToken.token,
		bookReturnResponse:
			state.ignite && state.ignite.bookDetail.bookReturnResponse,
		booths: state.ignite && state.ignite.home.booths,
		allotedBooks: state.ignite && state.ignite.home.allotedBooks,
		location:
			state.appToken && state.appToken.selectedSite
				? state.appToken.selectedSite
				: null,
		isDifferentBookScanned:
			state.ignite && state.ignite.bookDetail.isDifferentBookScanned
	};
}

export default connect(mapStateToProps)(BookDetail);
