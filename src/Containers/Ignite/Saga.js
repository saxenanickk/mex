import { all } from "redux-saga/effects";
// Import different Sagas here
import { igniteHomeSaga } from "./Containers/Home/Saga";
import { igniteBookDetailSaga } from "./Containers/BookDetail/Saga";

export default function* igniteSaga() {
	yield all([igniteHomeSaga(), igniteBookDetailSaga()]);
}
