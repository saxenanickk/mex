import React, { Component } from "react";
import PropTypes from "prop-types";
import { StyleSheet, Dimensions, Vibration, View } from "react-native";
import { CameraKitCameraScreen } from "react-native-camera-kit";

const { width, height } = Dimensions.get("window");
export default class QRCodeReader extends Component {
	static propTypes = {
		onRead: PropTypes.func.isRequired,
		vibrate: PropTypes.bool,
		showMarker: PropTypes.bool,
		cameraType: PropTypes.oneOf(["front", "back"]),
		customMarker: PropTypes.element,
		containerStyle: PropTypes.any,
		cameraStyle: PropTypes.any,
		topViewStyle: PropTypes.any,
		bottomViewStyle: PropTypes.any,
		topContent: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
		bottomContent: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
		cameraProps: PropTypes.object
	};

	static defaultProps = {
		onRead: () => console.log("QR code scanned!"),
		reactivate: false,
		vibrate: true,
		showMarker: false,
		cameraType: "back",
		cameraProps: {}
	};

	constructor(props) {
		super(props);
	}

	_renderCamera() {
		return (
			<CameraKitCameraScreen
				showFrame={true}
				style={{
					flex: 1,
					backgroundColor: "#000",
					width: width,
					height: height
				}}
				scanBarcode={true}
				laserColor={"blue"}
				surfaceColor={"black"}
				frameColor={"yellow"}
				onReadCode={event =>
					this._handleBarCodeRead(event.nativeEvent.codeStringValue)
				}
				hideControls={true}
				colorForScannerFrame={"blue"}
			/>
		);
	}

	_handleBarCodeRead(e) {
		const value = {
			data: e
		};
		if (this.props.vibrate) {
			Vibration.vibrate();
		}
		this.props.onRead(value);
	}

	render() {
		return <View style={{ flex: 1 }}>{this._renderCamera()}</View>;
	}
}

const styles = StyleSheet.create({
	mainContainer: {
		flex: 1
	},
	infoView: {
		flex: 2,
		justifyContent: "center",
		alignItems: "center",
		width: Dimensions.get("window").width
	},

	camera: {
		flex: 0,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "transparent",
		height: Dimensions.get("window").width,
		width: Dimensions.get("window").width
	},

	rectangleContainer: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "transparent"
	},

	rectangle: {
		height: 250,
		width: 250,
		borderWidth: 2,
		borderColor: "#00FF00",
		backgroundColor: "transparent"
	}
});
