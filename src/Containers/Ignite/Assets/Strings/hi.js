import * as I18n from "../../../../Assets/strings/i18n";

export default {
	/**
	 * Ignite App Strings
	 */
	...I18n.default.translations.hi,
	book_load_error: "पुस्तक लोड करने में असमर्थ, कुछ समय बाद प्रयास करें",
	ignite_header: "IGNITE",
	book_pod_header: "पुस्तक फली",
	ignite_pick_up: "पिक अप",
	ignite_return: "वापसी",
	ignite_genre: "शैली",
	ignite_search: "खोज",
	ignite_bookmark: "बुकमार्क",
	ignite_bookmarked: "बुकमार्क है",
	ignite_available: "उपलब्ध",
	book_pods: "बुक पॉड्स",
	return_book: "वापसी पुस्तक",
	similar_book: "कुछ ऐसी ही किताबें",
	request_book: "अनुरोध पुस्तक",
	book_details: "पुस्तक विवरण",
	select_and_return: "चयन करें और वापस लौटें",
	which_pod: "कौन सी फली?",
	caps_or: "या",
	scan_qr_code: "पॉड क्यूआर कोड को स्कैन करें",
	scan_and_return: "स्कैन और वापसी",
	return_details: "विवरण लौटाएं",
	book_booth: "बुक बूथ",
	bookmarked_books: "बुकमार्क की गई पुस्तके"
};
