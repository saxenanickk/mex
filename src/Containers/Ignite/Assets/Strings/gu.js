import * as I18n from "../../../../Assets/strings/i18n";

export default {
	/**
	 * Ignite App Strings
	 */
	...I18n.default.translations.gu,
	book_load_error: "પુસ્તક લોડ કરવામાં અસમર્થ, થોડીવાર પછી પ્રયાસ કરો",
	ignite_header: "IGNITE",
	book_pod_header: "બુક પોડ",
	ignite_pick_up: "ચૂંટો",
	ignite_return: "પાછા ફરો",
	ignite_genre: "શૈલી",
	ignite_search: "શોધો",
	ignite_bookmark: "બુકમાર્ક",
	ignite_bookmarked: "બુકમાર્ક થયેલ",
	ignite_available: "ઉપલબ્ધ",
	book_pods: "પુસ્તક પીઓડી",
	return_book: "રીટર્ન બુક",
	similar_book: "કેટલાક સમાન પુસ્તકો",
	request_book: "વિનંતી પુસ્તક",
	book_details: "પુસ્તક વિગતો",
	select_and_return: "પસંદ કરો અને રીટર્ન",
	which_pod: "કયા પીઓડી?",
	caps_or: "અથવા",
	scan_qr_code: "સ્કેન પોડ ક્યુઆર કોડ",
	scan_and_return: "સ્કેન અને રીટર્ન",
	return_details: "પરત વિગતો",
	book_booth: "બુક બૂથ",
	bookmarked_books: "બુકમાર્ક થયેલ પુસ્તકો"
};
