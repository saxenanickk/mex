import * as I18n from "../../../../Assets/strings/i18n";

export default {
	/**
	 * Ignite App Strings
	 */
	...I18n.default.translations.en,
	book_load_error: "Unable to load book,try after sometime",
	ignite_header: "IGNITE",
	book_pod_header: "Book Pod",
	ignite_pick_up: "Pick up",
	ignite_return: "Return",
	ignite_genre: "Genre",
	ignite_search: "Search",
	ignite_bookmark: "Bookmark",
	ignite_bookmarked: "Bookmarked",
	ignite_available: "Available",
	book_pods: "Book Pods",
	return_book: "Return Book",
	similar_book: "Some Similar Books",
	request_book: "Request Book",
	book_details: "Book Details",
	select_and_return: "Select & Return",
	which_pod: "Which Pod?",
	caps_or: "OR",
	scan_qr_code: "Scan Pod QR code",
	scan_and_return: "Scan & Return",
	return_details: "Return Details",
	book_booth: "Book Booth",
	bookmarked_books: "Bookmarked Books"
};
