import * as I18n from "../../../../Assets/strings/i18n";

export default {
	/**
	 * Ignite App Strings
	 */
	...I18n.default.translations.ja,
	book_load_error: "書籍を読み込めません。しばらくしてから試してください",
	ignite_header: "IGNITE",
	book_pod_header: "ブックポッド",
	ignite_pick_up: "拾う",
	ignite_return: "戻る",
	ignite_genre: "ジャンル",
	ignite_search: "サーチ",
	ignite_bookmark: "しおり",
	ignite_bookmarked: "ブックマーク済み",
	ignite_available: "利用可能",
	book_pods: "ブックポッド",
	return_book: "リターンブック",
	similar_book: "類似の本",
	request_book: "リクエストブック",
	book_details: "本の詳細",
	select_and_return: "選択して戻る",
	which_pod: "どのポッド？",
	caps_or: "または",
	scan_qr_code: "スキャンポッドQRコード",
	scan_and_return: "スキャンして戻る",
	return_details: "詳細を返す",
	book_booth: "ブックブース",
	bookmarked_books: "ブックマークした本"
};
