export const BOOK = require("./book.png");
export const OPENBOOK = require("./open_book.png");
export const IGNITE = require("./ignite.png");
export const PICKUPBOOK = require("./pickup.png");
export const PODS = require("./pods.png");
export const QRCODE = require("./qrcode.png");
export const RETURNBOOK = require("./return.png");
