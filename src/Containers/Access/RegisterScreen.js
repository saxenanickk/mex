import React from "react";
import Splash from "./Containers/Splash";
import GetStarted from "./Containers/GetStarted";
import EnterCode from "./Containers/EnterCode";
import AuthenticationSuccessful from "./Containers/AuthenticationSuccessful";

import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

const RegisterScreen = props => (
	<Stack.Navigator headerMode="none">
		<Stack.Screen name="Splash" component={Splash} />
		<Stack.Screen name="GetStarted" component={GetStarted} />
		<Stack.Screen name="EnterCode" component={EnterCode} />
		<Stack.Screen
			name="AuthenticationSuccessful"
			component={AuthenticationSuccessful}
		/>
	</Stack.Navigator>
);

export default RegisterScreen;
