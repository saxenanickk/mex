import React from "react";
import { AppRegistry, BackHandler } from "react-native";
import App from "./App";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";

export default class Access extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "access", access_enabled: false });
		this.handleGoBack = this.handleGoBack.bind(this);
		BackHandler.addEventListener("hardwareBackPress", this.handleGoBack);
	}

	handleGoBack = () => {
		this.props.navigation.navigate("Goapp");
		return true;
	};

	componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.handleGoBack);
	}
	render() {
		return <App />;
	}
}

AppRegistry.registerComponent("Access", () => Access);
