import React, { Fragment } from "react";
import { StatusBar } from "react-native";
import RegisterScreen from "./RegisterScreen";

const App = () => {
	return (
		<Fragment>
			<StatusBar backgroundColor={"#fff"} barStyle={"dark-content"} />
			<RegisterScreen />
		</Fragment>
	);
};

export default App;
