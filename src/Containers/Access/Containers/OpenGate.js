import React from "react";
import { View, Dimensions } from "react-native";
import LottieView from "lottie-react-native";

import { GoappTextBold } from "../../../Components/GoappText";

const { height } = Dimensions.get("window");

const OpenGate = ({ showUnlock, showStatus, accessStatus, loop, source }) =>
	showUnlock ? (
		<View
			style={{
				position: "absolute",
				top: 0,
				bottom: 0,
				left: 0,
				right: 0,
				justifyContent: "center",
				alignItems: "center",
				backgroundColor: "transparent"
			}}>
			<View
				style={{
					position: "absolute",
					top: 0,
					bottom: 0,
					left: 0,
					right: 0,
					backgroundColor: "#000000",
					opacity: 0.9
				}}
			/>
			<LottieView
				style={{ width: 100, height: 100 }}
				autoPlay
				loop={loop}
				source={source}
			/>
			<GoappTextBold
				color={"#ffffff"}
				style={{ textAlign: "center" }}
				size={height / 40}>
				{showStatus(accessStatus)}
			</GoappTextBold>
		</View>
	) : null;

export default OpenGate;
