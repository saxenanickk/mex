import React, { Component } from "react";
import {
	View,
	ScrollView,
	Dimensions,
	StyleSheet,
	TouchableOpacity,
	Platform
} from "react-native";
import { MaskService } from "react-native-masked-text";
import Config from "react-native-config";
import {
	GoappTextRegular,
	GoappTextBold,
	GoappTextInputBold
} from "../../../Components/GoappText";
import Access from "../../../CustomModules/Access";
import { GoToast, ProgressScreen } from "../../../Components";
import { connect } from "react-redux";
import { validateAlphaNumeric } from "../../../utils/validation";
import GoAppAnalytics from "../../../utils/GoAppAnalytics";
import DialogContext from "../../../DialogContext";
import { ERRORCLOUD } from "../../../Assets/Img/Image";
import { mexGetProfile } from "../../../CustomModules/ApplicationToken/Saga";
import { font_three } from "../../../Assets/styles.android";
import { CommonActions } from "@react-navigation/native";

const { width, height } = Dimensions.get("window");

class EnterCode extends Component {
	constructor(props) {
		super(props);
		this.state = {
			value: "",
			error: false,
			isLoading: false
		};
		GoAppAnalytics.setPageView("access", "EnterCode");
	}

	handleTextChange = text =>
		this.setState({
			value: MaskService.toMask("custom", text, {
				mask: "SSSS - SSSS - SSSS - SSSS"
			}),
			error: false
		});

	authenticate = async () => {
		if (!this.props.isInternetAvailable)
			this.context.current.openDialog({
				type: "Information",
				title: "No Internet Connection",
				message: "You seem to have no internet connection. Try again later.",
				icon: ERRORCLOUD
			});
		else {
			const { value } = this.state;
			// Regex to replace spaces and hyphens(-)
			const TOKEN = `${value.replace(/[-|\s]/g, "").toUpperCase()}`;
			if (TOKEN.length !== 16) {
				this.setState({ error: true });
				GoToast.show("Please Enter 16 digit code.", "Error", "SHORT");
			} else if (validateAlphaNumeric(TOKEN)) {
				GoToast.show(
					"Only alphanumeric characters are allowed.",
					"Error",
					"SHORT"
				);
			} else {
				this.setState({ isLoading: true }, () => {
					Access.init(
						TOKEN,
						success => {
							if (success !== "MEX. Access Activated.") {
								// Show Error
								this.setState(
									{
										isLoading: false
									},
									() =>
										this.context.current.openDialog({
											type: "Information",
											title: "Error",
											message: "Something went wrong.",
											icon: ERRORCLOUD
										})
								);
							} else {
								Access.saveAccessCode(TOKEN.toUpperCase());
								fetch(`${Config.MEX_LOGIN_BASE_URL}addAccess`, {
									method: "POST",
									headers: {
										"x-access-token": this.props.appToken,
										"content-type": "application/json"
									},
									body: JSON.stringify({ passCode: TOKEN.toUpperCase() })
								})
									.then(response => response.json())
									.then(res => {
										if (res.success) {
											/**
											 * Navigate to Authentication Successful Screen
											 */
											this.props.dispatch(
												mexGetProfile({ appToken: this.props.appToken })
											);
											this.setState({ isLoading: false }, () =>
												this.props.navigation.dispatch(
													CommonActions.reset({
														index: 0,
														routes: [{ name: "AuthenticationSuccessful" }]
													})
												)
											);
										} else {
											throw new Error();
										}
									})
									.catch(error => {
										console.log(error);
										this.setState({ isLoading: false }, () =>
											this.context.current.openDialog({
												type: "Information",
												title: "Error",
												message: "Something went wrong.",
												icon: ERRORCLOUD
											})
										);
									});
							}
						},
						error => {
							console.log(error);
							this.setState({ isLoading: false }, () =>
								this.context.current.openDialog({
									title: "Error",
									type: "Information",
									message: error ? error : "Something went wrong.",
									icon: ERRORCLOUD
								})
							);
						}
					);
				});
			}
		}
	};

	render() {
		const { value, error, isLoading } = this.state;

		return (
			<ScrollView
				bounces={false}
				keyboardShouldPersistTaps={"never"}
				keyboardDismissMode={"interactive"}
				contentContainerStyle={styles.scrollContainerStyle}>
				<View style={styles.container}>
					<View style={styles.upperContainer}>
						<GoappTextBold
							size={height / 50}
							color={"#000000"}
							style={styles.digitalLabel}>
							{"Digital Access Code"}
						</GoappTextBold>
						<GoappTextRegular
							size={height / 60}
							color={"#000000"}
							style={styles.pleaseEnterLabel}>
							{"Please enter your 16 digit access code."}
						</GoappTextRegular>
						<View style={styles.textInputContainer}>
							<GoappTextInputBold
								autoCapitalize={"characters"}
								autoCorrect={false}
								autoFocus={true}
								maxLength={25}
								onChangeText={this.handleTextChange}
								onSubmitEditing={event => {
									this.authenticateButton.props.onPress();
								}}
								placeholder={""}
								style={[styles.inputBox, styles.inputStatus(error)]}
								value={value}
							/>
						</View>
					</View>
					<View style={styles.bottomContainer}>
						<TouchableOpacity
							disabled={value.length < 25 ? true : false}
							ref={ref => (this.authenticateButton = ref)}
							onPress={() => this.authenticate()}>
							<View
								style={[
									styles.authenticateButton,
									value.length < 25
										? styles.authenticateDisabled
										: styles.authenticateEnabled
								]}>
								<GoappTextRegular color={"#FFFFFF"} size={height / 50}>
									{"Authenticate"}
								</GoappTextRegular>
							</View>
						</TouchableOpacity>
						<GoappTextRegular
							size={height / 60}
							style={styles.noAccessCodeLabel}>
							{"Don't have the Access Code? Please contact your Company Admin."}
						</GoappTextRegular>
						<TouchableOpacity
							onPress={() =>
								this.props.navigation.navigate("LinkView", {
									url: "https://www.mexit.in/faqs",
									title: "FAQs"
								})
							}>
							<GoappTextRegular color={"#2C98F0"} style={styles.faqsLabel}>
								{"FAQs"}
							</GoappTextRegular>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.props.navigation.navigate("ContactUs")}>
							<GoappTextRegular color={"#2C98F0"} style={styles.contactLabel}>
								{"Contact Support"}
							</GoappTextRegular>
						</TouchableOpacity>
					</View>
				</View>
				{isLoading ? (
					<ProgressScreen
						isMessage={true}
						primaryMessage={"Hang On..."}
						message={"Connecting with Server"}
						indicatorColor={"#2C98F0"}
						indicatorSize={height / 50}
					/>
				) : null}
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	container: { flex: 1, padding: width / 35 },
	scrollContainerStyle: {
		flex: 1,
		padding: width / 25
	},
	upperContainer: {
		height: height / 3,
		justifyContent: "flex-start",
		alignItems: "center"
	},
	textInputContainer: {
		marginVertical: height / 30,
		flexDirection: "row",
		width: width / 1.1,
		alignItems: "center"
	},
	bottomContainer: { flex: 1.2, justifyContent: "flex-start" },
	authenticateButton: {
		paddingHorizontal: width / 40,
		paddingVertical: height / 50,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 10
	},
	authenticateEnabled: {
		backgroundColor: "#2C98F0"
	},
	authenticateDisabled: {
		backgroundColor: "grey"
	},
	inputBox: {
		flex: 1,
		marginHorizontal: width / 25,
		borderWidth: 1,
		paddingVertical: 5,
		borderRadius: 10,
		borderColor: "#bfbfbf",
		textAlign: "center",
		fontSize: 20,
		fontFamily: Platform.OS === "ios" ? "Avenir" : font_three
	},
	inputStatus: error => ({
		borderColor: error ? "#ff0000" : "#bfbfbf"
	}),
	noAccessCodeLabel: {
		marginVertical: height / 40,
		alignSelf: "center",
		textAlign: "center"
	},
	faqsLabel: {
		alignSelf: "center",
		textAlign: "center"
	},
	contactLabel: {
		marginVertical: height / 40,
		alignSelf: "center",
		textAlign: "center"
	},
	digitalLabel: {
		marginVertical: height / 30,
		alignSelf: "center"
	},
	pleaseEnterLabel: {
		alignSelf: "center"
	}
});

const mapStateToProps = state => ({
	appToken: state.appToken.token,
	isInternetAvailable: state.netInfo.status
});

EnterCode.contextType = DialogContext;
export default connect(mapStateToProps)(EnterCode);
