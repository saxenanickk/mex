import React from "react";
import { View, Dimensions, TouchableOpacity, StyleSheet } from "react-native";
import { GoappTextRegular, GoappTextBold } from "../../../Components/GoappText";
import FastImage from "react-native-fast-image";
import { AUTHENTICATIONSUCCESSFUL } from "../Assets/Img";
import GoAppAnalytics from "../../../utils/GoAppAnalytics";

const { width, height } = Dimensions.get("window");

class AuthenticationSuccessful extends React.Component {
	constructor(props) {
		super(props);
		GoAppAnalytics.setPageView("access", "AuthenticationSuccessful");
	}

	proceed = () => {
		this.props.navigation.pop();
	};

	render() {
		return (
			<View
				style={{
					flex: 1,
					backgroundColor: "#ffffff",
					padding: width / 25,
					justifyContent: "space-between"
				}}>
				<FastImage
					style={styles.image}
					source={AUTHENTICATIONSUCCESSFUL}
					resizeMode={"contain"}
				/>
				<View style={{ flex: 0.8, justifyContent: "space-between" }}>
					<View>
						<GoappTextBold
							size={height / 50}
							color={"#000000"}
							style={{
								marginVertical: height / 30,
								alignSelf: "center"
							}}>
							{"Authentication Successful"}
						</GoappTextBold>
						<GoappTextRegular
							style={{
								textAlign: "center"
							}}
							size={height / 54}>
							{
								"Enjoy the hasslefree unlocking for turnstiles and doors with your Mobile Devices."
							}
						</GoappTextRegular>
					</View>
					<TouchableOpacity onPress={() => this.proceed()}>
						<View style={styles.getStarted}>
							<GoappTextRegular color={"#FFFFFF"} size={height / 50}>
								{"Done"}
							</GoappTextRegular>
						</View>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	image: { flex: 0.2, margin: width / 10, width: undefined, height: undefined },
	getStarted: {
		backgroundColor: "#2C98F0",
		paddingHorizontal: width / 40,
		paddingVertical: height / 50,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 10
	}
});

export default AuthenticationSuccessful;
