import React from "react";
import { View, TouchableOpacity, StyleSheet, Dimensions } from "react-native";
import { GoappTextRegular } from "../../../Components/GoappText";
import FastImage from "react-native-fast-image";
import { ACCESSBACKGROUND } from "../Assets/Img";
import AsyncStorage from "@react-native-community/async-storage";
import GoAppAnalytics from "../../../utils/GoAppAnalytics";
import { CommonActions } from "@react-navigation/native";

const { width, height } = Dimensions.get("window");

export default class GetStarted extends React.Component {
	constructor(props) {
		super(props);
		GoAppAnalytics.setPageView("access", "GetStarted");
	}

	getStarted = () => {
		AsyncStorage.setItem("mex_access_get_started", JSON.stringify(true));
		this.props.navigation.dispatch(
			CommonActions.reset({
				index: 0,
				routes: [{ name: "EnterCode" }]
			})
		);
	};

	render() {
		return (
			<View
				style={{
					flex: 1,
					backgroundColor: "#ffffff",
					padding: width / 25,
					justifyContent: "space-between"
				}}>
				<FastImage
					style={styles.image}
					source={ACCESSBACKGROUND}
					resizeMode={"contain"}
				/>
				<View style={{ flex: 0.5, justifyContent: "space-between" }}>
					<View>
						<GoappTextRegular size={height / 54}>
							{"Welcome to MEX.Access!"}
						</GoappTextRegular>
						<GoappTextRegular
							style={{ marginTop: height / 50 }}
							size={height / 54}>
							{
								"A secure and convenient way to unlock turnstiles and doors with your mobile phone device."
							}
						</GoappTextRegular>
					</View>
					<TouchableOpacity onPress={() => this.getStarted()}>
						<View style={styles.getStarted}>
							<GoappTextRegular color={"#FFFFFF"} size={height / 50}>
								{"Get Started"}
							</GoappTextRegular>
						</View>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	image: { flex: 0.4, width: undefined, height: undefined },
	getStarted: {
		backgroundColor: "#2C98F0",
		paddingHorizontal: width / 40,
		paddingVertical: height / 50,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 10
	}
});
