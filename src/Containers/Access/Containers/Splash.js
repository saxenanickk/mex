import React from "react";
import { View, ActivityIndicator } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { CommonActions } from "@react-navigation/native";

export default class Splash extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isTermsOpen: false
		};
	}

	componentDidMount() {
		AsyncStorage.getItem("mex_access_get_started")
			.then(value => {
				const data = JSON.parse(value);
				if (data) {
					this.navigateTo("EnterCode");
				} else {
					this.navigateTo("GetStarted");
				}
			})
			.catch(error => {
				this.navigateTo("GetStarted");
			});
	}

	navigateTo = screenName => {
		this.props.navigation.dispatch(
			CommonActions.reset({
				index: 0,
				routes: [{ name: screenName }]
			})
		);
	};

	render() {
		return (
			<View
				style={{
					flex: 1,
					backgroundColor: "#fff",
					justifyContent: "center",
					alignItems: "center"
				}}>
				<ActivityIndicator />
			</View>
		);
	}
}
