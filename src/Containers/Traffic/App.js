import React, { Fragment } from "react";
import { StatusBar } from "react-native";
import RegisterScreen from "./RegisterScreen";
import { DrawerOpeningHack } from "../../Components";
import GoAppAnalytics from "../../utils/GoAppAnalytics";

export default class App extends React.Component {
	render() {
		return (
			<Fragment>
				<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
				<RegisterScreen
					{...this.props}
					onNavigationStateChange={(prevState, currState) =>
						GoAppAnalytics.logChangeOfScreenInStack(
							"traffic",
							prevState,
							currState
						)
					}
				/>
				<DrawerOpeningHack />
			</Fragment>
		);
	}
}
