import { combineReducers } from "redux";

const initialState = {};

const navReducer = (state = initialState, action) => {
	switch (action.type) {
		default:
			return state;
	}
};
const trafficReducer = combineReducers({
	navReducer: navReducer
});

export default trafficReducer;
