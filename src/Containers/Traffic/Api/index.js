import Config from "react-native-config";

const { SERVER_BASE_URL_TRAFFIC } = Config;
/**
 * @typedef {Object} GetTrafficImagesParams
 * @property {String} GetTrafficImagesParams.appToken
 * @property {String} GetTrafficImagesParams.zoneId
 * @property {String} GetTrafficImagesParams.moduleId
 */
class TrafficApi {
	constructor() {
		console.log("Traffic Api Instantiated.");
	}

	/**
	 *
	 * @param {GetTrafficImagesParams} params
	 */
	getTrafficImages(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(
					`${SERVER_BASE_URL_TRAFFIC}/getTrafficImages?zoneId=${
						params.zoneId
					}&moduleId=${params.moduleId}`,
					{
						method: "GET",
						headers: {
							"Content-Type": "application/json",
							"X-ACCESS-TOKEN": params.appToken
						}
					}
				)
					.then(response => {
						response
							.json()
							.then(res => {
								if (response.status === 200) {
									resolve(res.response);
								} else {
									reject("err");
								}
							})
							.catch(err => reject(err));
					})
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}
}

export default new TrafficApi();
