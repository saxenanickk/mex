import React from "react";
import { Stack } from "../../utils/Navigators";
import Splash from "./Containers/Splash";
import Home from "./Containers/Home";
import CamMap from "./Containers/CamMap";

let data = null;

const Navigator = () => (
	<Stack.Navigator headerMode="none">
		<Stack.Screen
			name={"Splash"}
			component={props => <Splash {...data} {...props} />}
		/>
		<Stack.Screen name={"Home"} component={Home} />
		<Stack.Screen name={"CamMap"} component={CamMap} />
	</Stack.Navigator>
);

const RegisterScreen = props => {
	data = props;
	return (
		<Navigator
			onNavigationStateChange={(prevState, currState) =>
				props.onNavigationStateChange(prevState, currState)
			}
		/>
	);
};

export default RegisterScreen;
