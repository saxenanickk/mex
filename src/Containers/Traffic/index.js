// @ts-check
import React from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";

export default class Traffic extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "traffic" });
		// getNewReducer({ name: "traffic", reducer: reducer })
	}

	componentWillUnmount() {
		// removeExistingReducer("traffic")
	}

	render() {
		// Deeplink Params
		return <App {...this.props} />;
	}
}

AppRegistry.registerComponent("Traffic", () => Traffic);
