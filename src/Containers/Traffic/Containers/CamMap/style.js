import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
	map: {
		flex: 1
	},
	header: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		paddingRight: 20,
		backgroundColor: "#fff",
		paddingBottom: 10
	},
	backArrow: {
		paddingHorizontal: 20
	},
	backArrowHeading: {
		flexDirection: "row",
		alignItems: "center"
	}
});
