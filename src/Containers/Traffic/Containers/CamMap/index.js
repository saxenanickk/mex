import React, { Component } from "react";
import { View, StatusBar, TouchableOpacity } from "react-native";
import MapView from "react-native-maps";
import { styles } from "./style";
import { Icon } from "../../../../Components";
import { GoappTextBold } from "../../../../Components/GoappText";

export default class CamMap extends Component {
	render() {
		return (
			<View style={{ flex: 1 }}>
				<StatusBar barStyle={"dark-content"} />
				<View style={styles.header}>
					<View style={styles.backArrowHeading}>
						<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
							<Icon
								iconType={"ionicon"}
								iconName={"md-arrow-back"}
								iconColor={"#000"}
								iconSize={24}
								style={styles.backArrow}
							/>
						</TouchableOpacity>
						<GoappTextBold>{"Camera Locations"}</GoappTextBold>
					</View>
				</View>
				<MapView
					style={styles.map}
					initialRegion={{
						latitude: 37.78825,
						longitude: -122.4324,
						latitudeDelta: 0.0922,
						longitudeDelta: 0.0421
					}}
				/>
			</View>
		);
	}
}
