import React, { Component } from "react";
import { View, Dimensions, Platform } from "react-native";
import { connect } from "react-redux";
import FastImage from "react-native-fast-image";
import { ProgressScreen } from "../../../../Components";
import Dropdown from "../../../../Components/Dropdown";
import { styles } from "./style";
import {
	GoappTextBold,
	GoappTextRegular
} from "../../../../Components/GoappText";
import Api from "../../Api";
import DialogContext from "../../../../DialogContext";

const { width } = Dimensions.get("window");

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			MainGateCam: new Array(0),
			selectedCam: "Loading available cams",
			availableCams: new Array(0),
			error: false,
			errorMsg: "",
			errorCount: 0
		};
	}

	availableCams = [];

	componentDidMount() {
		this.requestTrafficImagesFirstTime();
		this.interval = setInterval(() => {
			this.updateTrafficImages();
		}, 60000);
	}

	componentWillUnmount() {
		clearInterval(this.interval);
		// BackHandler.removeEventListener(
		// 	"hardwareBackPress",
		// 	this.handleBackButtonClick
		// );
	}

	handleBackButtonClick = () => {
		this.props.navigation.goBack();
	};

	requestTrafficImagesFirstTime = async () => {
		this.setState({ isLoading: true });
		const trafficImagesArray = await this.getTrafficImages({
			appToken: this.props.appToken,
			zoneId: this.props.selectedSite.ref_site_id,
			moduleId: 34
		});
		this.availableCams = trafficImagesArray[1].map(elem => elem.name);
		if (trafficImagesArray.length !== 0) {
			this.setState({
				isLoading: false,
				MainGateCam: trafficImagesArray[0],
				availableCams: trafficImagesArray[1],
				selectedCam: this.availableCams[0]
			});
		} else {
			this.setState({
				isLoading: false,
				error: true,
				errorMsg: "firstTimeFail"
			});
		}
	};

	updateTrafficImages = async () => {
		try {
			const trafficImagesArray = await this.getTrafficImages({
				appToken: this.props.appToken,
				zoneId: this.props.selectedSite.ref_site_id,
				moduleId: 34
			});
			this.availableCams = trafficImagesArray[1].map(elem => elem.name);
			if (trafficImagesArray.length !== 0) {
				this.setState({
					MainGateCam: trafficImagesArray[0],
					availableCams: trafficImagesArray[1],
					errorCount: 0
				});
			} else {
				this.setState({
					error: true
				});
			}
		} catch (error) {
			if (this.state.errorCount < 3) {
				this.setState({
					errorCount: this.state.errorCount + 1
				});
				this.updateTrafficImages();
			} else {
				this.context.current.openDialog({
					type: "Confirmation",
					title: "Something went wrong!",
					message: "Try again",
					leftTitle: "Go Back",
					leftPress: () => {
						this.handleBackButtonClick();
					},
					rightTitle: "Retry",
					rightPress: () => {
						this.updateTrafficImages();
						this.setState({ errorCount: 0 });
					}
				});
			}
		}
	};

	/**
	 * @param {import("../../Api").GetTrafficImagesParams} args
	 */
	getTrafficImages = async args => {
		try {
			/**
			 * @type {Array} CamerasArray
			 */
			const camerasArray = await Api.getTrafficImages({
				appToken: args.appToken,
				zoneId: args.zoneId,
				moduleId: args.moduleId
			});
			let MainGateCam = null;
			const MainGateIndex = camerasArray.findIndex(
				elem => elem.name === "Main Gate"
			);
			if (MainGateIndex > -1) {
				MainGateCam = camerasArray.splice(MainGateIndex, 1);
			} else {
				MainGateCam = camerasArray.splice(0, 1);
			}
			if (MainGateCam !== null) {
				MainGateCam = [
					{
						...MainGateCam[0],
						config: JSON.parse(MainGateCam[0].config)
					}
				];
			} else {
				return;
			}

			let availableCams = camerasArray.map(elem => ({
				...elem,
				config: JSON.parse(elem.config)
			}));
			return [MainGateCam, availableCams];
		} catch (error) {
			return [];
		}
	};

	getUrl = () => {
		let selectedCam = this.state.availableCams.find(
			elem => elem.name === this.state.selectedCam
		);
		return selectedCam.config.url;
	};
	selectedCamHandler = selectedCam => {
		this.setState({ selectedCam });
	};

	render() {
		let newAvailableCams = [];
		this.availableCams &&
			this.availableCams.map(cam => newAvailableCams.push({ name: cam }));
		if (this.state.isLoading) {
			return (
				<View style={styles.progressContainer}>
					<ProgressScreen
						isMessage={true}
						indicatorSize={25}
						indicatorColor={"#14314c"}
						primaryMessage={"Hang On..."}
						message={"Loading Traffic images"}
					/>
				</View>
			);
		} else if (this.state.error && this.state.errorMsg === "firstTimeFail") {
			return (
				<View>
					<GoappTextBold>Something went wrong</GoappTextBold>
				</View>
			);
		} else if (
			this.state.MainGateCam.length !== 0 &&
			this.state.availableCams.length !== 0
		) {
			return (
				<View style={styles.container}>
					<View style={styles.main}>
						<View style={[styles.imageContainer, { marginBottom: 5 }]}>
							<GoappTextRegular
								size={Platform.OS === "ios" ? width / 22 : 16}
								style={{ paddingVertical: 20 }}>
								{this.state.MainGateCam[0].name}
							</GoappTextRegular>
							<FastImage
								source={{
									uri: this.state.MainGateCam[0].config.url,
									cache: "web"
								}}
								style={styles.imageStyle}
							/>
						</View>
						<View style={[styles.imageContainer]}>
							<View style={styles.camsStyles}>
								<Dropdown
									data={newAvailableCams}
									dataStyle={{
										fontSize: Platform.OS === "ios" ? width / 22 : 16
									}}
									onPress={this.selectedCamHandler}
									selectedValue={this.state.selectedCam}
								/>
							</View>
							<FastImage
								source={{
									uri: this.getUrl(),
									cache: "web"
								}}
								style={styles.imageStyle}
							/>
						</View>
					</View>
				</View>
			);
		}
		return null;
	}
}

const mapStateToProps = state => ({
	appToken: state.appToken.token,
	selectedSite: state.appToken.selectedSite
});
Home.contextType = DialogContext;
export default connect(mapStateToProps)(Home);
