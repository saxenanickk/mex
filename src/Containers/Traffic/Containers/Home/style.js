import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	progressContainer: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	container: {
		backgroundColor: "#eff1f4",
		flex: 1
	},
	header: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		backgroundColor: "#fff"
	},
	backArrow: {
		paddingHorizontal: 20
	},
	backArrowHeading: {
		flexDirection: "row",
		alignItems: "center"
	},
	locationPicker: {
		width: width / 2.5
	},
	main: {
		flex: 1,
		justifyContent: "flex-start"
	},
	imageStyle: {
		aspectRatio: 1920 / 1080,
		borderRadius: 10
	},
	locationButton: {
		flex: 1,
		color: "#fff",
		backgroundColor: "#fff",
		position: "absolute",
		top: 5,
		right: 10,
		flexDirection: "row",
		alignItems: "center",
		borderRadius: 5
	},
	locationIcon: {
		justifyContent: "center",
		backgroundColor: "blue",
		alignSelf: "stretch",
		padding: 3,
		paddingHorizontal: 5,
		borderTopLeftRadius: 5,
		borderBottomLeftRadius: 5
	},
	locationText: {
		padding: 3
	},
	imageContainer: {
		flex: 1,
		backgroundColor: "#fff",
		paddingHorizontal: 10
	},
	camsStyles: {
		paddingVertical: height / 50
	}
});
