import React, { Component } from "react";
import { View } from "react-native";
import { connect } from "react-redux";
import { CommonActions } from "@react-navigation/native";
import ApplicationToken from "../../../../CustomModules/ApplicationToken";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";

class Splash extends Component {
	componentDidMount() {
		GoAppAnalytics.setPageView("Traffic", "splash");
		console.log(this.props, "I am the splash");

		if (this.props.appToken) {
			GoAppAnalytics.trackWithProperties("traffic-navigate", {
				nextScreen: "TrafficHome"
			});
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "Home" }]
				})
			);
		}
	}

	handleDeepLink = params => {
		let deepLinkParams = params ? JSON.parse(params) : null;
		if (deepLinkParams !== null) {
			return deepLinkParams.event_id;
		}
		return null;
	};

	componentDidUpdate(prevProps) {
		if (
			prevProps.appToken === null &&
			this.props.appToken !== prevProps.appToken
		) {
			GoAppAnalytics.trackWithProperties("traffic-navigate", {
				nextScreen: "TrafficHome"
			});
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "Home" }]
				})
			);
		}
	}

	render() {
		return (
			<View style={{ backgroundColor: "#000", flex: 1 }}>
				<ApplicationToken />
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token
	};
}

export default connect(mapStateToProps)(Splash);
