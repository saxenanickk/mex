import React, { Component, Fragment } from "react";
import { StatusBar } from "react-native";
import Home from "./Containers/Home";

class App extends Component {
	render() {
		return (
			<Fragment>
				<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
				<Home />
			</Fragment>
		);
	}
}

export default App;
