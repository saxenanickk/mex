import React from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";

export default class Shuttle extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "shuttle" });
		//getNewReducer({ name: "offers", reducer: reducer })
	}

	componentWillUnmount() {
		//removeExistingReducer("offers")
	}

	render() {
		return <App {...this.props} />;
	}
}

AppRegistry.registerComponent("Shuttle", () => Shuttle);
