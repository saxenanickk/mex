import Config from "react-native-config";
import polyline from "@mapbox/polyline";
class ShuttleApi {
	constructor(props) {
		console.log("shuttle api instatiated");
	}

	getParkingSpaceInfo(params) {
		return new Promise(function(resolve, reject) {
			fetch(
				`${Config.MEX_SHUTTLE_BASE_URL}getAllBusStopsBySiteId?site_id=${
					params.siteId
				}`,
				{
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": params.appToken
					}
				}
			)
				.then(result => {
					result
						.json()
						.then(response => {
							resolve(response);
						})
						.catch(error => {
							reject(error);
						});
				})
				.catch(error => {
					reject(error);
				});
		});
	}

	getShuttleData(params) {
		return new Promise(function(resolve, reject) {
			fetch(
				`${Config.MEX_SHUTTLE_BASE_URL}getAllShuttlesBySite?site_id=${
					params.siteId
				}`,
				{
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": params.appToken
					}
				}
			)
				.then(result => {
					result.json().then(response => {
						resolve(response);
					});
				})
				.catch(error => {
					console.log("error is", error);
				});
		});
	}

	trackShuttleData = params => {
		const promise = [];
		const empty_response = {
			longitude: null,
			latitude: null
		};
		let paths = params.shuttleData.filter(item => item.status === 1);
		paths.forEach(path => {
			promise.push(
				new Promise(function(resolve, reject) {
					fetch(
						`${Config.MEX_SHUTTLE_BASE_URL}getTrackingInfo?device_id=${
							path.device_id
						}`,
						{
							method: "GET",
							headers: {
								"X-ACCESS-TOKEN": params.appToken
							}
						}
					)
						.then(result => {
							result.json().then(res => {
								if (res.success === 1) {
									resolve({
										...res.data,
										shuttle_info: path
									});
								} else {
									resolve({
										...empty_response,
										shuttle_info: path
									});
								}
							});
						})
						.catch(error => {
							console.log("error is", error);
							resolve({
								...empty_response,
								shuttle_info: path
							});
						});
				})
			);
		});
		return new Promise((resolve, reject) => {
			Promise.all(promise)
				.then(response => resolve(response))
				.catch(error => reject(error));
		});
	};

	/**
	 * Google Maps API KEY required.
	 */
	drawMapPolyline(params) {
		let originLatitude = params.origin.latitude;
		let originLongitude = params.origin.longitude;
		let destinationLatitude = params.destination.latitude;
		let destinationLongitude = params.destination.longitude;
		const waypoints = params.waypoints ? params.waypoints : [];

		let apiRoute =
			"https://maps.googleapis.com/maps/api/directions/json?origin=" +
			originLatitude +
			"," +
			originLongitude +
			"&destination=" +
			destinationLatitude +
			"," +
			destinationLongitude;

		if (waypoints.length > 0) {
			apiRoute += "&waypoints=";
			for (let i = 0; i < waypoints.length; i++) {
				if (i < waypoints.length - 1) {
					apiRoute += waypoints[i].lat + "," + waypoints[i].lng + "|";
				} else {
					apiRoute += waypoints[i].lat + "," + waypoints[i].lng;
				}
			}
		}

		apiRoute += `&key=${Config.MEX_GOOGLE_MAP_PATH_KEY}`;

		return new Promise(function(resolve, reject) {
			fetch(apiRoute, {
				method: "GET"
			})
				.then(result => {
					result.json().then(res => {
						if (res.status !== "OK") {
							reject(res);
						}
						const points = polyline.decode(
							res.routes[0].overview_polyline.points
						);
						console.log(points);
						let coords = points.map((point, index) => {
							return {
								latitude: point[0],
								longitude: point[1]
							};
						});
						resolve({ path: coords });
					});
				})
				.catch(error => {
					console.log("Error of API (Requests): ", error);
					reject(error);
				});
		});
	}
}

export default new ShuttleApi();
