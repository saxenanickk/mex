import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	Platform,
	UIManager,
	PixelRatio
} from "react-native";
import { connect } from "react-redux";
import Api from "../../Api";
import { ProgressScreen, Icon } from "../../../../Components";
import styles from "./style";
import MapView, { Marker, PROVIDER_GOOGLE, Polyline } from "react-native-maps";
import DialogContext from "../../../../DialogContext";
import Config from "react-native-config";

const { CLOUDINARY_BASE_URL } = Config;
const MEXSHUTTLE =
	"v1574766163/44dfc7e0-103c-11ea-8df3-a505a775e547_100x100.png";
const SHUTTLESTAND =
	"v1573795430/1acd0b20-0768-11ea-b60b-d88eaaf0e6be_100x100.png";
const { width, height } = Dimensions.get("window");

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			mapPath: null,
			shuttle: null,
			cabImage: null,
			shuttleStand: null,
			shuttlePath: null,
			parking_space: null
		};
		this.interval = null;
		this.cabMarkerRef = [];
		this.shuttleData = [];
		this.getShuttleInfo = this.getShuttleInfo.bind(this);
		if (Platform.OS === "android") {
			UIManager.setLayoutAnimationEnabledExperimental(true);
		}
	}

	componentDidMount() {
		this.loadAtMount();
	}

	async loadAtMount() {
		try {
			let resp = await this.scaleImage(MEXSHUTTLE, "shuttle");
			this.setState({ cabImage: resp });
			resp = await this.scaleImage(SHUTTLESTAND, "shuttleStand");
			this.setState({ shuttleStand: resp });
			this.getMapPath();
			this.getShuttleInfo();
			this.getParkingSpaceInfo();
			this.startGettingShuttleInfo();
		} catch (error) {
			this.context.current.openDialog({
				type: "Information",
				title: "Error",
				message: "Sorry, we are unable to load Shuttle."
			});
		}
	}

	scaleImage(image, saveKey) {
		return new Promise(function(resolve, reject) {
			try {
				let widthForScale =
					saveKey === "shuttleStand" ? width / 15 : width / 20;
				let heightForScale = height / 24;
				let imageWidth = PixelRatio.getPixelSizeForLayoutSize(widthForScale);
				let imageHeight = PixelRatio.getPixelSizeForLayoutSize(heightForScale);
				resolve(
					`${CLOUDINARY_BASE_URL}w_${imageWidth},h_${imageHeight},c_scale/${image}`
				);
			} catch (error) {
				console.log("error", error);
				resolve(`${CLOUDINARY_BASE_URL}${image}`);
			}
		});
	}

	startGettingShuttleInfo = () => {
		this.interval = setInterval(this.getShuttleInfo, 20000);
	};

	componentWillUnmount() {
		clearInterval(this.interval);
	}

	async getMapPath() {
		try {
			const resp = await Api.drawMapPolyline({
				destination: { latitude: 12.929336, longitude: 77.684791 },
				origin: { latitude: 12.919707, longitude: 77.685353 }
			});
			if (resp && resp.path && resp.path.length > 0) {
				this.setState({ shuttlePath: resp.path });
			}
		} catch (error) {}
	}
	getParkingSpaceInfo = async () => {
		try {
			const response = await Api.getParkingSpaceInfo({
				appToken: this.props.appToken,
				siteId: this.props.siteId
			});
			if (response.success === 1) {
				this.setState({
					parking_space: response.data
				});
			}
		} catch (error) {
			console.log("error while fetching parking space", error);
		}
	};

	async getShuttleInfo() {
		try {
			let response = {};
			if (this.shuttleData.length === 0) {
				response = await Api.getShuttleData({
					appToken: this.props.appToken,
					siteId: this.props.siteId
				});
				this.shuttleData = response.data;
			}
			if (response.success == 1 || this.shuttleData.length > 0) {
				const resp = await Api.trackShuttleData({
					appToken: this.props.appToken,
					siteId: this.props.siteId,
					shuttleData: this.shuttleData
				});
				let coordinatesToFit = [];
				resp &&
					resp.forEach(item => {
						if (item.latitude && item.longitude) {
							let obj = {};
							obj.latitude = parseFloat(item.latitude);
							obj.longitude = parseFloat(item.longitude);
							if (!isNaN(obj.latitude) && !isNaN(obj.longitude)) {
								coordinatesToFit.push(obj);
							}
						}
					});
				this.setState({
					isLoading: false,
					shuttle: resp,
					mapPath: coordinatesToFit
				});
			} else {
				throw new Error("Unable to get shuttle data");
			}
		} catch (error) {
			this.setState({ isLoading: false });
			console.log("error while fetching traffic data", error);
		}
	}

	getFitToCoordinate = (coordinatesToFit = []) => {
		try {
			if (this.mapRef && coordinatesToFit.length > 0) {
				this.mapRef.fitToCoordinates(coordinatesToFit, {
					edgePadding: {
						top: 120,
						right: 120,
						bottom: 120,
						left: 120
					},
					animated: true
				});
			}
		} catch (error) {
			console.log("error in fitting to coord", error);
		}
	};
	render() {
		return (
			<View style={styles.container}>
				{this.state.isLoading ? (
					<View style={styles.progressContainer}>
						<ProgressScreen
							isMessage={true}
							indicatorSize={height / 30}
							indicatorColor={"#14314c"}
							primaryMessage={"Hang On..."}
							message={"Loading Shuttle location"}
						/>
					</View>
				) : (
					<MapView
						style={styles.map}
						provider={PROVIDER_GOOGLE}
						ref={ref => {
							this.mapRef = ref;
						}}
						onLayout={() => {
							this.getFitToCoordinate(this.state.mapPath);
						}}
						mapPadding={styles.mapPadding}>
						{this.state.shuttlePath ? (
							<Polyline
								coordinates={this.state.shuttlePath}
								strokeWidth={3}
								strokeColor={"#000"}
							/>
						) : null}
						{this.state.shuttle
							? this.state.shuttle.map((cab, index) =>
									cab.latitude && cab.longitude ? (
										<Marker.Animated
											style={
												cab.bearing
													? { transform: [{ rotateZ: `${cab.bearing}deg` }] }
													: null
											}
											key={index}
											title={`Shuttle-${index + 1},${
												cab.shuttle_info.shuttle_number
											}`}
											ref={ref => (this.cabMarkerRef[index] = ref)}
											coordinate={{
												latitude: parseFloat(cab.latitude),
												longitude: parseFloat(cab.longitude)
											}}
											image={this.state.cabImage}
										/>
									) : null
							  )
							: null}
						{this.state.parking_space &&
							this.state.parking_space.map((item, index) => (
								<Marker.Animated
									key={index}
									title={item.name}
									coordinate={{
										latitude: item.latitude,
										longitude: item.longitude
									}}
									image={this.state.shuttleStand}
								/>
							))}
					</MapView>
				)}
				<TouchableOpacity
					style={styles.fitToCoordinateButton}
					onPress={() => this.getFitToCoordinate(this.state.mapPath)}>
					<Icon
						iconType={"ionicon"}
						iconName={"md-locate"}
						iconColor={"#fff"}
						iconSize={height / 35}
					/>
				</TouchableOpacity>
			</View>
		);
	}
}

Home.contextType = DialogContext;
const mapStateToProps = state => ({
	appToken: state.appToken.token,
	siteId: state.appToken.selectedSite.id
});

export default connect(mapStateToProps)(Home);
