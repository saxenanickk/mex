import { Dimensions, StyleSheet, Platform } from "react-native";
const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	progressContainer: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	map: {
		borderWidth: 0,
		position: "absolute",
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		zIndex: -10
	},
	fitToCoordinateButton: {
		position: "absolute",
		right: width / 10,
		bottom: height / 20,
		width: width / 10,
		height: width / 10,
		borderRadius: width / 20,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#2C98F0",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	}
});

export default styles;
