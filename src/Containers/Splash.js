import React from "react";
import { View, StyleSheet } from "react-native";
import { CommonActions } from "@react-navigation/native";

import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import {
	saveAppToken,
	mexSaveSelectedSite
} from "../CustomModules/ApplicationToken/Saga";
import { mexSaveGlobalNavigationState } from "../Saga";
import { SplashScreen } from "../CustomModules/SplashScreen";

class Splash extends React.Component {
	constructor(props) {
		super(props);
		this.props.dispatch(mexSaveGlobalNavigationState(props.navigation));
	}

	componentDidMount() {
		this.getSelectedSite();
		this.getToken();
	}

	getSelectedSite = async () => {
		try {
			let selectedSite = await AsyncStorage.getItem("qc_selected_site");
			if (selectedSite) {
				this.props.dispatch(mexSaveSelectedSite(JSON.parse(selectedSite)));
			}
		} catch (error) {
			console.log("error in getting sites", error);
		}
	};

	getToken = async () => {
		// Todo
		try {
			let onBoardingFlag = await AsyncStorage.getItem("qc_is_onboard_shown");
			if (!onBoardingFlag) {
				AsyncStorage.setItem("qc_is_onboard_shown", "true");
				this.navigateTo("OnBoarding");
			} else {
				let user_token = await AsyncStorage.getItem("mex_app_token");
				if (!user_token) {
					this.navigateTo("Login");
				} else {
					this.props.dispatch(saveAppToken(JSON.parse(user_token)));
					this.navigateTo("Goapp");
				}
			}
		} catch (error) {
			this.navigateTo("Login");
		}
	};

	navigateTo = screenName => {
		SplashScreen.hide();
		// New code for reset
		this.props.navigation.dispatch(
			CommonActions.reset({
				index: 0,
				routes: [{ name: screenName }]
			})
		);
	};

	render() {
		return <View style={styles.container} />;
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		user: state.appToken.user
	};
}
export default connect(mapStateToProps)(Splash);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
		justifyContent: "center",
		alignItems: "center"
	}
});
