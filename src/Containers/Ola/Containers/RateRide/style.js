import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const indexStyles = StyleSheet.create({
	header: {
		width: width,
		height: height / 12,
		flexDirection: "row",
		elevation: 5,
		backgroundColor: "#ffffff",
		borderColor: "#ffffff"
	},
	fullFlex: {
		flex: 1
	},
	closeButton: { alignSelf: "center", padding: width / 30 },
	centeredRow: { alignSelf: "center", flexDirection: "row" },
	yourBillText: {
		marginLeft: width / 35,
		fontSize: width / 20,
		color: "#000000",
		textAlign: "center"
	},
	bookingStatusView: {
		alignSelf: "center",
		height: height / 5.7,
		backgroundColor: "#ffffff",
		width: width,
		paddingTop: height / 70
	},
	checkmarkCircleView: {
		backgroundColor: "rgb(255, 250, 213)",
		alignSelf: "center",
		justifyContent: "center",
		width: width / 1.2,
		height: height / 25,
		flexDirection: "row"
	},
	bookingStatusText: { textAlign: "center", textAlignVertical: "center" },
	iconView: {
		alignItems: "center",
		justifyContent: "center",
		flexDirection: "row"
	},
	iconText: {
		fontSize: width / 15,
		bottom: height / 170,
		color: "#000"
	},
	iconInfoText: { fontSize: width / 7, color: "#000" },
	payInCashView: { bottom: height / 10, alignSelf: "flex-end" },
	payInCashImage: {
		width: 80,
		height: 80,
		marginHorizontal: width / 8,
		transform: [{ rotate: "30deg" }]
	},
	addressView: {
		flexDirection: "row",
		alignSelf: "center",
		alignItems: "center",
		bottom: height / 20
	},
	spaceBetweenView: { justifyContent: "space-between" },
	greenBoundary: {
		backgroundColor: "green",
		borderRadius: 5,
		width: 10,
		height: 10,
		marginRight: width / 50
	},
	greyBoundary: {
		borderWidth: 1,
		borderStyle: "dashed",
		width: width / 3,
		height: 1,
		borderColor: "grey"
	},
	originAddressView: { width: width / 3 },
	originAddressText: { fontSize: width / 35 },
	handleCabView: { alignItems: "center", paddingBottom: width / 25 },
	handleCabImage: {
		width: 40,
		height: 40,
		marginHorizontal: width / 70
	},
	greyDottedBoundary: {
		borderWidth: 1,
		borderStyle: "dotted",
		width: width / 3,
		height: 0,
		borderColor: "grey"
	},
	redBoundary: {
		backgroundColor: "red",
		borderRadius: 5,
		width: 10,
		height: 10,
		marginLeft: width / 50
	},
	destinationAddressView: { width: width / 3, alignSelf: "flex-end" },
	destinationAddressText: { fontSize: width / 35, textAlign: "right" },
	rideFeedbackView: { marginTop: height / 20 },
	cabImageView: {
		alignSelf: "center",
		borderColor: "grey",
		borderRadius: 35,
		borderWidth: 1.3,
		width: 62,
		height: 62,
		alignItems: "center"
	},
	cabImage: {
		width: 55,
		height: 55,
		borderColor: "grey",
		borderWidth: 1.3,
		borderRadius: 30,
		marginTop: 2.6
	},
	howWasRideView: { alignSelf: "center", marginTop: width / 20 },
	howWasRideText: { fontSize: width / 19 },
	handleStarRatingView: {
		flexDirection: "row",
		justifyContent: "space-between",
		width: width / 1.5,
		alignSelf: "center",
		marginTop: width / 20
	},
	submitButton: {
		backgroundColor: "black",
		borderRadius: 5,
		width: width / 3,
		alignSelf: "center",
		margin: width / 10,
		height: height / 15
	},
	submitText: {
		color: "white",
		textAlign: "center",
		padding: width / 35
	},
	marginFromTop: { top: height / 19 }
});
