import React, { Component } from "react";
import {
	View,
	StatusBar,
	Dimensions,
	TouchableOpacity,
	Image,
	BackHandler
} from "react-native";
import { connect } from "react-redux";

import {
	olaRecordRideFeedback,
	olaRecordRideFeedbackSaveResponse
} from "./Saga";
import { Icon, Seperator, ProgressScreen } from "../../../../Components";
import {
	ERICK,
	DRIVER,
	KAALIPEELI,
	LUX,
	AUTO,
	MICRO,
	MINI,
	SHARE,
	SUV,
	PRIME,
	PAYINCASH,
	PAIDINCASH
} from "../../Assets/Img/Image";
import {
	OlaTextRegular,
	OlaTextBold,
	OlaTextLight
} from "../../Components/OlaText";
import { indexStyles as styles } from "./style";
import I18n from "../../Assets/Strings/i18n";
import { GoToast } from "../../../../Components";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import { CommonActions } from "@react-navigation/native";

const { width } = Dimensions.get("window");

const carImages = {
	mini: MINI,
	prime: PRIME,
	sedan: PRIME,
	suv: SUV,
	auto: AUTO,
	erick: ERICK,
	kp: KAALIPEELI,
	lux: LUX,
	micro: MICRO,
	exec: PRIME,
	prime_play: PRIME,
	share: SHARE
};

class RateRide extends Component {
	constructor(props) {
		super(props);
		this.state = {
			starInp1: "0",
			starInp2: "0",
			starInp3: "0",
			starInp4: "0",
			starInp5: "0"
		};
	}

	componentDidMount() {
		BackHandler.addEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
		try {
			const { bookingStatus } = this.props;
			const isCabShare =
				bookingStatus !== null &&
				bookingStatus.booking_id &&
				bookingStatus.booking_id.substring(0, 3) !== "CRN";

			const AMOUNT = !isCabShare
				? bookingStatus.trip_info && bookingStatus.trip_info.amount
				: bookingStatus.price_details && bookingStatus.price_details.fare;

			GoAppAnalytics.trackChargeWithProperties(
				parseInt(AMOUNT),
				bookingStatus.booking_id
			);
		} catch (error) {
			console.log("Error GoAppAnalytics.trackChargeWithProperties", error);
		}
	}

	shouldComponentUpdate(nextProps) {
		if (nextProps.rateRideResponse !== null) {
			if (nextProps.rateRideResponse.message === "FAILED") {
				GoToast.show(I18n.t("feedback_not_message"), I18n.t("information"));
			} else if (nextProps.rateRideResponse.message === "SUCCESS") {
				GoToast.show(I18n.t("feedback_recorded"), I18n.t("information"));
			}
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "Home" }]
				})
			);
			return false;
		}
		return true;
	}

	handleBackButtonClick = () => {
		this.props.navigation.goBack();
		return true;
	};

	componentWillUnmount() {
		this.props.dispatch(olaRecordRideFeedbackSaveResponse(null));
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
	}

	/**
	 * Handles star rating input change
	 */
	handleStarRatingInputChange = starInputCount => {
		switch (starInputCount) {
			case "1":
				this.setState({
					starInp1: "1",
					starInp2: "0",
					starInp3: "0",
					starInp4: "0",
					starInp5: "0"
				});
				break;

			case "2":
				this.setState({
					starInp1: "1",
					starInp2: "1",
					starInp3: "0",
					starInp4: "0",
					starInp5: "0"
				});
				break;
			case "3":
				this.setState({
					starInp1: "1",
					starInp2: "1",
					starInp3: "1",
					starInp4: "0",
					starInp5: "0"
				});
				break;
			case "4":
				this.setState({
					starInp1: "1",
					starInp2: "1",
					starInp3: "1",
					starInp4: "1",
					starInp5: "0"
				});
				break;
			case "5":
				this.setState({
					starInp1: "1",
					starInp2: "1",
					starInp3: "1",
					starInp4: "1",
					starInp5: "1"
				});
				break;
			default:
				break;
		}
	};

	/**
	 * Returns the formatted cab type text
	 */
	handleCabTypeText = cabType => {
		switch (cabType) {
			case "share":
				return "Share";

			case "mini":
				return "Mini";

			case "prime":
				return "Prime";

			case "sedan":
				return "Sedan";

			case "suv":
				return "SUV";

			case "auto":
				return "Auto";

			case "erick":
				return "E-Rickshaw";

			case "kp":
				return "Kaali Peeli";

			case "lux":
				return "LUX";

			case "micro":
				return "Micro";

			case "exec":
				return "Executive";

			case "prime_play":
				return "Prime Play";

			default:
				return "Micro";
		}
	};

	/**
	 * Makes API call to record the feedback
	 */
	handleOnSubmit = () => {
		const { state, props } = this;

		const rating =
			parseInt(state.starInp1) +
			parseInt(state.starInp2) +
			parseInt(state.starInp3) +
			parseInt(state.starInp4) +
			parseInt(state.starInp5);

		if (props.bookingStatus !== null) {
			props.dispatch(
				olaRecordRideFeedback({
					token_type: "Bearer",
					access_token: props.userToken,
					request_id: props.bookingStatus.booking_id,
					rating,
					comments: "",
					feedback: ""
				})
			);
		}
	};

	render() {
		const { bookingStatus, originAddr, destinationAddr } = this.props;

		const isCabShare =
			bookingStatus !== null &&
			bookingStatus.booking_id &&
			bookingStatus.booking_id.substring(0, 3) !== "CRN";

		return (
			<View style={styles.fullFlex}>
				<StatusBar ackgroundColor="#7f7f7f" barStyle="light-content" />
				{this.props.isLoading ? (
					<ProgressScreen indicatorColor={"rgb(76, 165, 53)"} />
				) : null}
				<View style={styles.header}>
					<TouchableOpacity
						onPress={() => this.props.navigation.goBack()}
						style={styles.closeButton}>
						<Icon
							iconType={"material"}
							iconSize={width / 15}
							iconName={"close"}
							iconColor={"#000000"}
						/>
					</TouchableOpacity>
					<View style={styles.centeredRow}>
						<OlaTextRegular style={styles.yourBillText}>
							{`${I18n.t("your_bill")}`}
						</OlaTextRegular>
					</View>
				</View>

				<View style={styles.bookingStatusView}>
					{bookingStatus &&
					bookingStatus.trip_info &&
					(bookingStatus.trip_info.discount > 0 ||
						bookingStatus.trip_info.pass_savings > 0) ? (
						<View style={styles.checkmarkCircleView}>
							<Icon
								iconType={"ionicon"}
								iconSize={width / 18}
								iconName={"md-checkmark-circle"}
								iconColor={"rgb(82, 155, 26)"}
							/>
							<OlaTextRegular style={styles.bookingStatusText}>
								{` ${"\u20b9"} ${bookingStatus.trip_info.discount +
									bookingStatus.trip_info.pass_savings} savings on fare`}
							</OlaTextRegular>
						</View>
					) : null}
					<View style={styles.iconView}>
						<OlaTextRegular style={styles.iconText}>{"\u20b9"}</OlaTextRegular>
						<OlaTextLight style={styles.iconInfoText}>
							{!isCabShare && bookingStatus
								? bookingStatus.trip_info
									? bookingStatus.trip_info.amount
									: ""
								: bookingStatus.price_details
								? bookingStatus.price_details.fare
								: ""}
						</OlaTextLight>
					</View>
				</View>
				<View style={styles.payInCashView}>
					{!isCabShare &&
					bookingStatus.trip_info &&
					bookingStatus.trip_info.payable_amount !== 0 ? (
						<Image source={PAYINCASH} style={styles.payInCashImage} />
					) : (
						<Image source={PAIDINCASH} style={styles.payInCashImage} />
					)}
				</View>

				<View style={styles.addressView}>
					<View style={styles.spaceBetweenView}>
						<View style={styles.centeredRow}>
							<View style={styles.greenBoundary} />
							<View style={styles.greyBoundary} />
						</View>
						<View style={styles.originAddressView}>
							<OlaTextRegular
								numberOfLines={2}
								ellipsizeMode="tail"
								style={styles.originAddressText}>
								{originAddr || ""}
							</OlaTextRegular>
						</View>
					</View>
					<View style={styles.handleCabView}>
						<Image
							source={
								!isCabShare && bookingStatus
									? carImages[bookingStatus.cab_details.cab_type]
									: carImages.share
							}
							style={styles.handleCabImage}
						/>
						<OlaTextRegular style={styles.originAddressText}>
							{!isCabShare && bookingStatus
								? this.handleCabTypeText(bookingStatus.cab_details.cab_type)
								: this.handleCabTypeText("share")}
						</OlaTextRegular>
					</View>
					<View style={styles.spaceBetweenView}>
						<View style={styles.centeredRow}>
							<View style={styles.greyDottedBoundary} />
							<View style={styles.redBoundary} />
						</View>
						<View style={styles.destinationAddressView}>
							<OlaTextRegular
								numberOfLines={2}
								ellipsizeMode="tail"
								style={styles.destinationAddressText}>
								{destinationAddr || ""}
							</OlaTextRegular>
						</View>
					</View>
				</View>

				<View style={styles.rideFeedbackView}>
					<Seperator
						width={width}
						height={0.5}
						color={"#cfcfcf"}
						style={styles.marginFromTop}
					/>
					<View style={styles.cabImageView}>
						<Image
							source={
								isCabShare
									? {
											uri: bookingStatus ? bookingStatus.driver_image_url : ""
									  }
									: DRIVER
							}
							style={styles.cabImage}
						/>
					</View>
					<View style={styles.howWasRideView}>
						<OlaTextBold style={styles.howWasRideText}>
							{`${I18n.t("how_was_ride")}?`}
						</OlaTextBold>
					</View>
					<View style={styles.handleStarRatingView}>
						<TouchableOpacity
							onPress={() => {
								this.handleStarRatingInputChange("1");
							}}>
							<View>
								{this.state.starInp1 === "1" ? (
									<Icon
										iconType={"ionicon"}
										iconName={"ios-star"}
										iconSize={width / 12}
										iconColor={"#rgba(237,140,40,0.8)"}
									/>
								) : (
									<Icon
										iconType={"ionicon"}
										iconName={"ios-star-outline"}
										iconSize={width / 12}
										iconColor={"#rgba(0,0,0,0.5)"}
									/>
								)}
							</View>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.handleStarRatingInputChange("2")}>
							<View>
								{this.state.starInp2 === "1" ? (
									<Icon
										iconType={"ionicon"}
										iconName={"ios-star"}
										iconSize={width / 12}
										iconColor={"#rgba(237,140,40,0.8)"}
									/>
								) : (
									<Icon
										iconType={"ionicon"}
										iconName={"ios-star-outline"}
										iconSize={width / 12}
										iconColor={"#rgba(0,0,0,0.5)"}
									/>
								)}
							</View>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.handleStarRatingInputChange("3")}>
							<View>
								{this.state.starInp3 === "1" ? (
									<Icon
										iconType={"ionicon"}
										iconName={"ios-star"}
										iconSize={width / 12}
										iconColor={"#rgba(237,140,40,0.8)"}
									/>
								) : (
									<Icon
										iconType={"ionicon"}
										iconName={"ios-star-outline"}
										iconSize={width / 12}
										iconColor={"#rgba(0,0,0,0.5)"}
									/>
								)}
							</View>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.handleStarRatingInputChange("4")}>
							<View>
								{this.state.starInp4 === "1" ? (
									<Icon
										iconType={"ionicon"}
										iconName={"ios-star"}
										iconSize={width / 12}
										iconColor={"#rgba(237,140,40,0.8)"}
									/>
								) : (
									<Icon
										iconType={"ionicon"}
										iconName={"ios-star-outline"}
										iconSize={width / 12}
										iconColor={"#rgba(0,0,0,0.5)"}
									/>
								)}
							</View>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.handleStarRatingInputChange("5")}>
							<View>
								{this.state.starInp5 === "1" ? (
									<Icon
										iconType={"ionicon"}
										iconName={"ios-star"}
										iconSize={width / 12}
										iconColor={"#rgba(237,140,40,0.8)"}
									/>
								) : (
									<Icon
										iconType={"ionicon"}
										iconName={"ios-star-outline"}
										iconSize={width / 12}
										iconColor={"#rgba(0,0,0,0.5)"}
									/>
								)}
							</View>
						</TouchableOpacity>
					</View>
					<TouchableOpacity
						onPress={() => {
							this.handleOnSubmit();
						}}
						disabled={
							parseInt(this.state.starInp1) +
								parseInt(this.state.starInp2) +
								parseInt(this.state.starInp3) +
								parseInt(this.state.starInp4) +
								parseInt(this.state.starInp5) <
							1
						}
						style={styles.submitButton}>
						<View>
							<OlaTextRegular style={styles.submitText}>
								{`${I18n.t("submit")}`}
							</OlaTextRegular>
						</View>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		location: state.location,
		appToken: state.appToken.token,
		userToken: state.ola && state.ola.auth.userCredentials,
		bookingStatus: state.ola && state.ola.chooseRide.bookingStatus,
		isLoading: state.ola && state.ola.rateRides.isLoading,
		rateRideResponse: state.ola && state.ola.rateRides.rateRideResponse,
		originAddr: state.ola && state.ola.cancelRides.originAddr,
		destinationAddr: state.ola && state.ola.cancelRides.destinationAddr
	};
}
export default connect(mapStateToProps)(RateRide);
