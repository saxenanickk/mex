import { takeLatest, call, put } from "redux-saga/effects";
import OlaApi from "../../Api";

/**
 * Actions
 */
export const OLA_RECORD_RIDE_FEEDBACK = "OLA_RECORD_RIDE_FEEDBACK";
export const OLA_RECORD_RIDE_FEEDBACK_SAVE_RESPONSE =
	"OLA_RECORD_RIDE_FEEDBACK_SAVE_RESPONSE";

/**
 * Action Creators
 */
export const olaRecordRideFeedback = payload => ({
	type: OLA_RECORD_RIDE_FEEDBACK,
	payload
});

export const olaRecordRideFeedbackSaveResponse = payload => ({
	type: OLA_RECORD_RIDE_FEEDBACK_SAVE_RESPONSE,
	payload
});

export function* olaRateRideSaga(dispatch) {
	yield takeLatest(OLA_RECORD_RIDE_FEEDBACK, handleOlaRecordRideFeedback);
}

/**
 * Handlers
 */
function* handleOlaRecordRideFeedback(action) {
	try {
		let rateRide = yield call(OlaApi.postRideFeedback, action.payload);
		console.log("Ola Rate Ride Data: ", rateRide);
		yield put(olaRecordRideFeedbackSaveResponse(rateRide));
	} catch (error) {
		console.log("Ola Cancel Ride Error: ", error);
	}
}
