import {
	OLA_RECORD_RIDE_FEEDBACK,
	OLA_RECORD_RIDE_FEEDBACK_SAVE_RESPONSE
} from "./Saga";

const initialState = {
	isLoading: false,
	rateRideResponse: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case OLA_RECORD_RIDE_FEEDBACK:
			return {
				...state,
				isLoading: true
			};
		case OLA_RECORD_RIDE_FEEDBACK_SAVE_RESPONSE:
			return {
				...state,
				isLoading: false,
				rateRideResponse: action.payload
			};
		default:
			return state;
	}
};
