import React, { Component } from "react";
import {
	Image,
	View,
	Platform,
	Dimensions,
	TouchableOpacity,
	BackHandler,
	Share,
	Linking,
	FlatList,
	TouchableWithoutFeedback,
	ImageEditor,
	PixelRatio
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";

import {
	Seperator,
	MapView,
	Icon,
	CabMarker,
	ProgressScreen,
	DialogModal,
	InformationFooter
} from "../../../../Components";
import { LocationText } from "../../Components";
import MapViewLib, { Marker } from "react-native-maps";
import { DisabledTextBox, LocationButton } from "../../Components";
import RouteDetailsModal from "./RouteDetailsModal";
import LocationApi from "../../../../CustomModules/LocationModule/Api";
import I18n from "../../Assets/Strings/i18n";
import { GoToast } from "../../../../Components";
import {
	MINIMAP,
	OLA,
	SHAREDROP,
	SHAREPICKUP,
	WAITTIME,
	SHAREMAP,
	PRIMEMAP,
	RENTALSMAP,
	AUTOMAP,
	LUXMAP,
	SUVMAP,
	SEDANMAP,
	MICROMAP,
	OUTSTATIONMAP
} from "../../Assets/Img/Image";
import { olaCurrentRide, olaMapPolyline, olaMapPath } from "../ChooseRide/Saga";
import {
	olaCancelRide,
	olaSosSignal,
	olaSaveOriginAddress,
	olaSaveDestinationAddress
} from "./Saga";
import { setNavigationToTrackRide } from "../ChooseRide/Saga";
import {
	updateDropLocation,
	updateDropLocationStatus
} from "../ChooseRide/Saga";
import OlaApi from "../../Api";
import {
	OlaTextRegular,
	OlaTextBold,
	OlaTextMedium
} from "../../Components/OlaText";
import { indexStyles as styles } from "./style";
import goAppAnalytics from "../../../../utils/GoAppAnalytics";
import Config from "react-native-config";
import { CommonActions } from "@react-navigation/native";

const { CLOUDINARY_BASE_URL } = Config;
const { width, height } = Dimensions.get("window");

class TrackRide extends Component {
	constructor(props) {
		super(props);
		this.state = {
			bookingCancelled: false,
			showCancelReasons: false,
			selectedReason: "",
			isRouteDetailsModalOpen: false,
			loadingCancelReasons: true,
			cancelReasonsResp: {},
			coordinate: {
				latitude: props.bookingStatus && props.bookingStatus.driver_lat,
				longitude: props.bookingStatus && props.bookingStatus.driver_lng
			}
		};
		this.mapRef = null;
		this.isArrived = false;
		this.isTracking = true;
		this.trackingInterval = null;
		this.openPlaceSearch = this.openPlaceSearch.bind(this);
		this.onTrack = this.onTrack.bind(this);
		this.shareRide = this.shareRide.bind(this);
		this.callDriver = this.callDriver.bind(this);
		this.cancelRide = this.cancelRide.bind(this);
		this.getStatusText = this.getStatusText.bind(this);
		this.sendSosSignal = this.sendSosSignal.bind(this);
		this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
		this.renderDestinationMarker = this.renderDestinationMarker.bind(this);
		this.renderPassengerInShare = this.renderPassengerInShare.bind(this);
		this.getCabImage = this.getCabImage.bind(this);
		this.waitTimeImage = null;
		this.cabMarkerRef = {};
	}

	openPlaceSearch() {
		this.props.navigation.navigate("PlaceSearch", {
			searchType: "destination",
			selectedLocation: locationObject => {
				this.props.dispatch(
					updateDropLocation({
						access_token: this.props.userToken,
						drop_lat: locationObject.latitude,
						drop_lng: locationObject.longitude,
						booking_id: this.props.bookingStatus.booking_id,
						appToken: this.props.appToken
					})
				);
			}
		});
	}

	handleBackButtonClick() {
		this.props.dispatch(setNavigationToTrackRide(false));
		this.props.navigation.goBack();
		return false;
	}

	componentDidMount() {
		AsyncStorage.setItem(
			"RIDE_IN_PROGRESS",
			JSON.stringify(this.props.bookingStatus)
		);
		goAppAnalytics.trackWithProperties("ola-booking-success", {
			booking_id: this.props.bookingStatus.booking_id
		});
		BackHandler.addEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
		const {
			props: { bookingStatus }
		} = this;
		if (
			bookingStatus &&
			bookingStatus.cab_details &&
			bookingStatus.cab_details.cab_type !== "share"
		) {
			this.getAddress(
				bookingStatus && bookingStatus.drop_lat,
				bookingStatus && bookingStatus.drop_lng,
				"dest"
			);
			this.getAddress(
				bookingStatus && bookingStatus.pickup_lat,
				bookingStatus && bookingStatus.pickup_lng,
				"origin"
			);
		} else {
			this.getAddress(
				bookingStatus &&
					bookingStatus.drop_location &&
					bookingStatus.drop_location.lat,
				bookingStatus &&
					bookingStatus.drop_location &&
					bookingStatus.drop_location.lng,
				"dest"
			);
			this.getAddress(
				bookingStatus &&
					bookingStatus.pickup_location &&
					bookingStatus.pickup_location.lat,
				bookingStatus &&
					bookingStatus.pickup_location &&
					bookingStatus.pickup_location.lng,
				"origin"
			);
		}
		this.onTrack();
	}

	componentWillReceiveProps(nextProps) {
		const duration = 500;
		if (
			nextProps.bookingStatus !== null &&
			nextProps.bookingStatus.booking_id &&
			nextProps.bookingStatus.booking_id.substring(0, 3) === "CRN"
		) {
			// Check if previous drop lat or lng changed or not
			if (
				nextProps.bookingStatus &&
				this.props.bookingStatus &&
				(nextProps.bookingStatus.drop_lat !==
					this.props.bookingStatus.drop_lat ||
					nextProps.bookingStatus.drop_lng !==
						this.props.bookingStatus.drop_lng)
			) {
				this.getAddress(
					nextProps.bookingStatus && nextProps.bookingStatus.drop_lat,
					nextProps.bookingStatus && nextProps.bookingStatus.drop_lng,
					"dest"
				);
			}
			// Check if previous pickup lat or lng changed or not
			if (
				nextProps.bookingStatus &&
				this.props.bookingStatus &&
				(nextProps.bookingStatus.pickup_lat !==
					this.props.bookingStatus.pickup_lat ||
					nextProps.bookingStatus.pickup_lng !==
						this.props.bookingStatus.pickup_lng)
			) {
				this.getAddress(
					nextProps.bookingStatus && nextProps.bookingStatus.pickup_lat,
					nextProps.bookingStatus && nextProps.bookingStatus.pickup_lng,
					"origin"
				);
			}
		} else {
			// Check if previous drop lat or lng changed or not
			if (
				nextProps.bookingStatus &&
				this.props.bookingStatus &&
				this.props.bookingStatus.drop_location &&
				nextProps.bookingStatus.drop_location &&
				(nextProps.bookingStatus.drop_location.lat !==
					this.props.bookingStatus.drop_location.lat ||
					nextProps.bookingStatus.drop_location.lng !==
						this.props.bookingStatus.drop_location.lng)
			) {
				this.getAddress(
					nextProps.bookingStatus &&
						nextProps.bookingStatus.drop_location &&
						nextProps.bookingStatus.drop_location.lat,
					nextProps.bookingStatus &&
						nextProps.bookingStatus.drop_location &&
						nextProps.bookingStatus.drop_location.lng,
					"dest"
				);
			}

			// Check if previous pickup lat or lng changed or not
			if (
				nextProps.bookingStatus &&
				this.props.bookingStatus &&
				this.props.bookingStatus.pickup_location &&
				nextProps.bookingStatus.pickup_location &&
				(nextProps.bookingStatus.pickup_location.lat !==
					this.props.bookingStatus.pickup_location.lat ||
					nextProps.bookingStatus.pickup_location.lng !==
						this.props.bookingStatus.pickup_location.lng)
			) {
				this.getAddress(
					nextProps.bookingStatus &&
						nextProps.bookingStatus.pickup_location &&
						nextProps.bookingStatus.pickup_location.lat,
					nextProps.bookingStatus &&
						nextProps.bookingStatus.pickup_location &&
						nextProps.bookingStatus.pickup_location.lng,
					"origin"
				);
			}
		}

		if (
			nextProps.bookingStatus &&
			this.props.bookingStatus &&
			typeof nextProps.bookingStatus.driver_lat !== "undefined" &&
			typeof nextProps.bookingStatus.driver_lng !== "undefined" &&
			(this.props.bookingStatus.driver_lat !==
				nextProps.bookingStatus.driver_lat ||
				this.props.bookingStatus.driver_lng !==
					nextProps.bookingStatus.driver_lng)
		) {
			if (this.mapRef !== null) {
				//this.mapRef.fitToCoordinate()
				this.getFitToCoordinate();
			}
			if (Platform.OS === "android") {
				if (this.cabMarkerRef) {
					const coordinate = {
						latitude: nextProps.bookingStatus.driver_lat,
						longitude: nextProps.bookingStatus.driver_lng
					};
					this.setState({ coordinate });
				}
			} else {
				if (this.cabMarkerRef) {
					const coordinate = {
						latitude: nextProps.bookingStatus.driver_lat,
						longitude: nextProps.bookingStatus.driver_lng
					};
					this.setState({ coordinate });
				}
			}
		}

		if (
			nextProps.mapPath !== null &&
			nextProps.mapPath !== this.props.mapPath &&
			this.mapRef !== null
		) {
			this.getFitToCoordinate();
		}
	}

	componentWillUnmount() {
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
		if (this.state.isRouteDetailsModalOpen) {
			this.closeRouteDetailsModal();
		}
		this.props.dispatch(olaMapPath(null));
		clearInterval(this.trackingInterval);
		this.props.dispatch(updateDropLocationStatus(null));
	}

	getAddress = (drop_lat, drop_lng, type) => {
		LocationApi.reverseGeocoding({
			latitude: drop_lat,
			longitude: drop_lng
		})
			.then(data => {
				console.log("address is", data.results[0].formatted_address);
				if (type === "dest") {
					this.props.dispatch(
						olaSaveDestinationAddress({
							destinationAddr: data.results[0].formatted_address
						})
					);
				} else {
					this.props.dispatch(
						olaSaveOriginAddress({
							originAddr: data.results[0].formatted_address
						})
					);
				}
			})
			.catch(err => {
				if (this.props.netInfo && this.props.netInfo.status) {
					console.log(err);
				} else {
					GoToast.show(I18n.t("internet_not_found"));
					return false;
				}
			});
	};
	renderDestinationMarker = latlng => {
		return (
			<Marker coordinate={latlng} image={this.props.scaledImage.locationDrop} />
		);
	};
	renderPassengerInShare(item, index) {
		return (
			<Marker
				key={index}
				coordinate={{
					latitude: item.lat,
					longitude: item.lng
				}}
				title={item.name}
				description={item.address}
				image={item.state === "pickup" ? SHAREPICKUP : SHAREDROP}
			/>
		);
	}
	renderCancelReasons = data => {
		const iconName =
			this.state.selectedReason === data.item ? "dot-circle-o" : "circle-thin";
		const iconColor =
			this.state.selectedReason === data.item ? "#3A40FF" : "#000";
		return (
			<TouchableOpacity
				style={styles.cancelReasonButton}
				onPress={() => {
					console.log("Cancel Reason  :>", data);
					this.setState({ selectedReason: data.item });
				}}>
				<OlaTextRegular>{data.item}</OlaTextRegular>
				<Icon
					iconType="font_awesome"
					iconName={iconName}
					iconSize={width / 20}
					iconColor={iconColor}
				/>
			</TouchableOpacity>
		);
	};
	renderWaitTimeMarker = () => {
		if (this.waitTimeImage === null) {
			let imagePath =
				"v1575438640/0065a2f0-165a-11ea-8137-4f8ed38e0828_100x100.png";
			let widthForScale = width / 11;
			let heightForScale = height / 12;

			let imageWidth = PixelRatio.getPixelSizeForLayoutSize(widthForScale);
			let imageHeight = PixelRatio.getPixelSizeForLayoutSize(heightForScale);

			this.waitTimeImage = `${CLOUDINARY_BASE_URL}w_${imageWidth},h_${imageHeight},c_scale/${imagePath}`;
		}

		return (
			<Marker
				coordinate={{
					latitude:
						typeof this.props.bookingStatus.pickup_location !== "undefined"
							? this.props.bookingStatus.pickup_location.lat
							: this.props.bookingStatus.pickup_lat,
					longitude:
						typeof this.props.bookingStatus.pickup_location !== "undefined"
							? this.props.bookingStatus.pickup_location.lng
							: this.props.bookingStatus.pickup_lng
				}}
				description={"your pickup"}
				image={this.waitTimeImage}>
				<OlaTextRegular style={styles.waitTimeImageRandomText}>
					{Math.random()}
				</OlaTextRegular>
				<OlaTextRegular style={styles.waitTimeImageText}>
					{this.props.bookingStatus.duration.value}
				</OlaTextRegular>
				<OlaTextRegular style={styles.waitTimeImageText}>
					{this.props.bookingStatus.duration.unit.substr(0, 3).toLowerCase()}
				</OlaTextRegular>
			</Marker>
		);
	};
	cancelRide(reason) {
		console.log("Cancel Ride Function");
		if (this.props.bookingStatus !== null) {
			this.setState({ bookingCancelled: true, showCancelReasons: false });
			this.props.dispatch(
				olaCancelRide({
					access_token: this.props.userToken,
					request_id: this.props.bookingStatus.booking_id,
					reason: reason,
					appToken: this.props.appToken
				})
			);
			goAppAnalytics.trackWithProperties("ola-cancel-booking", {
				request_id: this.props.bookingStatus.booking_id
			});
		}
	}

	shareRide() {
		Share.share(
			{
				message: this.props.bookingStatus.share_ride_url,
				url: this.props.bookingStatus.share_ride_url,
				title: I18n.t("share_ride")
			},
			{
				dialogTitle: I18n.t("share_ride")
			}
		);
	}

	callDriver() {
		const number = "tel: +91" + this.props.bookingStatus.driver_number;
		Linking.canOpenURL(number)
			.then(supported => {
				if (!supported) {
					console.log('Can"t handle number: ' + number);
				} else {
					return Linking.openURL(number);
				}
			})
			.catch(err => console.error("An error occurred", err));
	}

	shouldComponentUpdate(props, state) {
		console.log("Track Ride Props ", props.bookingStatus, state);
		if (
			props.bookingStatus.status === "SUCCESS" &&
			props.bookingStatus.booking_status === "CALL_DRIVER"
		) {
			return true;
		} else if (
			props.bookingStatus.status === "SUCCESS" &&
			props.bookingStatus.booking_status === "CLIENT_LOCATED"
		) {
			//
		} else if (
			props.bookingStatus.status === "SUCCESS" &&
			props.bookingStatus.booking_status === "IN_PROGRESS"
		) {
			//
			if (this.mapRef !== null) {
				//this.mapRef.fitToCoordinate()
				this.getFitToCoordinate();
			}
			if (this.state.showCancelReasons) {
				this.setState({ showCancelReasons: false });
			}
		} else if (
			props.bookingStatus.status === "SUCCESS" &&
			props.bookingStatus.booking_status === "COMPLETED"
		) {
			clearInterval(this.trackingInterval);
			GoToast.show(I18n.t("completed"), I18n.t("information"));
			this.props.dispatch(setNavigationToTrackRide(false));
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "Home" }, { name: "RateRide" }]
				})
			);
			return false;
		} else if (
			props.bookingStatus.status === "SUCCESS" &&
			props.bookingStatus.request_type === "BOOKING_CANCEL"
		) {
			clearInterval(this.trackingInterval);
			GoToast.show(props.bookingStatus.text, I18n.t("information"));
			this.props.dispatch(setNavigationToTrackRide(false));
			this.props.navigation.goBack();
			return false;
		} else if (
			props.bookingStatus.status === "SUCCESS" &&
			props.bookingStatus.booking_status === "BOOKING_CANCELLED"
		) {
			clearInterval(this.trackingInterval);
			GoToast.show(I18n.t("ride_cancel_message"), I18n.t("information"));
			this.props.dispatch(setNavigationToTrackRide(false));
			this.props.navigation.goBack();
			return false;
		} else if (props.updateStatus !== null) {
			GoToast.show(props.updateStatus.text, I18n.t("information"));
			props.dispatch(updateDropLocationStatus(null));
			return false;
		}
		return true;
	}

	onTrack() {
		if (this.props.bookingStatus !== null && this.isTracking) {
			this.isTracking = false;
			clearInterval(this.trackingInterval);
			this.trackingInterval = null;
			this.trackingInterval = setInterval(() => {
				console.log("on track");
				this.props.dispatch(
					olaCurrentRide({
						access_token: this.props.userToken,
						request_id: this.props.bookingStatus.booking_id,
						appToken: this.props.appToken
					})
				);
				if (
					this.props.bookingStatus &&
					this.props.bookingStatus.booking_id.substring(0, 3) === "CRN" &&
					this.props.bookingStatus.pickup_lat &&
					typeof this.props.bookingStatus.driver_lat !== "undefined" &&
					typeof this.props.bookingStatus.driver_lng !== "undefined"
				) {
					if (
						this.props.bookingStatus.previous_rider_drop_lat &&
						this.props.bookingStatus.previous_rider_drop_lng
					) {
						this.props.dispatch(
							olaMapPolyline({
								origin: {
									latitude: this.props.bookingStatus.driver_lat,
									longitude: this.props.bookingStatus.driver_lng
								},
								destination: {
									latitude: this.props.bookingStatus.pickup_lat,
									longitude: this.props.bookingStatus.pickup_lng
								},
								waypoints: [
									{
										lat: this.props.bookingStatus.previous_rider_drop_lat,
										lng: this.props.bookingStatus.previous_rider_drop_lng
									}
								]
							})
						);
					} else {
						this.props.dispatch(
							olaMapPolyline({
								origin: {
									latitude: this.props.bookingStatus.driver_lat,
									longitude: this.props.bookingStatus.driver_lng
								},
								destination: {
									latitude: this.props.bookingStatus.pickup_lat,
									longitude: this.props.bookingStatus.pickup_lng
								}
							})
						);
					}
				} else {
					if (
						typeof this.props.bookingStatus.driver_lat !== "undefined" &&
						typeof this.props.bookingStatus.driver_lng !== "undefined" &&
						this.props.bookingStatus.pickup_location
					) {
						this.props.dispatch(
							olaMapPolyline({
								origin: {
									latitude: this.props.bookingStatus.driver_lat,
									longitude: this.props.bookingStatus.driver_lng
								},
								destination: {
									latitude: this.props.bookingStatus.pickup_location.lat,
									longitude: this.props.bookingStatus.pickup_location.lng
								}
							})
						);
					}
				}
			}, 3000);
		}
	}

	sendSosSignal() {
		this.props.dispatch(
			olaSosSignal({
				access_token: this.props.userToken,
				request_id: this.props.bookingStatus.booking_id,
				appToken: this.props.appToken
			})
		);
	}

	getStatusText(status) {
		switch (status) {
			case "CALL_DRIVER":
				return I18n.t("call_driver");
			case "CLIENT_LOCATED":
				return I18n.t("cab_arrived");
			case "IN_PROGRESS":
				return I18n.t("in_progress");
			case "COMPLETED":
				return I18n.t("completed");
			case "BOOKING_CANCELLED":
				return I18n.t("booking_cancelled");
			default:
				return status;
		}
	}

	backButtonPress = () => {
		this.props.navigation.goBack();
	};

	getCabImage(cabType) {
		switch (cabType) {
			case "share":
				return SHAREMAP;
			case "micro":
				return MICROMAP;
			case "mini":
				return MINIMAP;
			case "prime":
				return SEDANMAP;
			case "suv":
				return SUVMAP;
			case "auto":
				return AUTOMAP;
			case "rental":
				return RENTALSMAP;
			case "outstation":
				return OUTSTATIONMAP;
			case "lux":
				return LUXMAP;
			default:
				return PRIMEMAP;
		}
	}
	openRouteDetailsModal = () => {
		this.setState({ isRouteDetailsModalOpen: true });
	};

	closeRouteDetailsModal = () => {
		this.setState({ isRouteDetailsModalOpen: false });
	};

	handleCancel = () => {
		this.setState({ loadingCancelReasons: true, showCancelReasons: true });
		let category;
		if (this.props.bookingStatus.booking_id.substring(0, 3) === "CRN") {
			category = this.props.bookingStatus.cab_details.cab_type;
		} else {
			category = "share";
		}
		OlaApi.getCancelReasons({
			appToken: this.props.appToken,
			access_token: this.props.userToken,
			category: category,
			pickup_lat: this.props.origin.latitude,
			pickup_lng: this.props.origin.longitude
		}).then(cancelReasonsResp => {
			this.setState({ cancelReasonsResp, loadingCancelReasons: false });
		});
	};

	getCoordinateToFit = () => {
		/**
		 * {Call Driver} driver + pickup
		 * {Inprogress} driver + drop
		 * {Completed} drop + drop
		 */
		return [
			{
				latitude:
					this.props.bookingStatus.booking_status === "COMPLETED" ||
					this.props.bookingStatus.booking_status === "IN_PROGRESS"
						? typeof this.props.bookingStatus.drop_location !== "undefined"
							? this.props.bookingStatus.drop_location.lat
							: this.props.bookingStatus.drop_lat
						: typeof this.props.bookingStatus.pickup_location !== "undefined"
						? this.props.bookingStatus.pickup_location.lat
						: this.props.bookingStatus.pickup_lat,
				longitude:
					this.props.bookingStatus.booking_status === "COMPLETED" ||
					this.props.bookingStatus.booking_status === "IN_PROGRESS"
						? typeof this.props.bookingStatus.drop_location !== "undefined"
							? this.props.bookingStatus.drop_location.lng
							: this.props.bookingStatus.drop_lng
						: typeof this.props.bookingStatus.pickup_location !== "undefined"
						? this.props.bookingStatus.pickup_location.lng
						: this.props.bookingStatus.pickup_lng
			},
			{
				latitude:
					this.props.bookingStatus.booking_status !== "COMPLETED"
						? this.props.bookingStatus.driver_lat
						: typeof this.props.bookingStatus.drop_location !== "undefined"
						? this.props.bookingStatus.drop_location.lat
						: this.props.bookingStatus.drop_lat,
				longitude:
					this.props.bookingStatus.booking_status !== "COMPLETED"
						? this.props.bookingStatus.driver_lng
						: typeof this.props.bookingStatus.drop_location !== "undefined"
						? this.props.bookingStatus.drop_location.lng
						: this.props.bookingStatus.drop_lng
			}
		];
	};

	getFitToCoordinate = () => {
		let coordinatesToFit = this.getCoordinateToFit();
		console.log("calling getFit ", coordinatesToFit);
		this.mapRef.fitToCoordinate(coordinatesToFit);
	};
	render() {
		console.log("props in track ride are", this.state);
		var statusText = "";
		var cancelRideComponent = (
			<TouchableOpacity
				onPress={this.handleCancel}
				style={styles.handleCancelButton}>
				<Icon
					iconType={"material"}
					iconName={"cancel"}
					iconSize={width / 15}
					iconColor={"#000000"}
				/>
				<OlaTextRegular>{I18n.t("cancel")}</OlaTextRegular>
			</TouchableOpacity>
		);

		if (
			this.props.bookingStatus !== null &&
			this.props.bookingStatus.hasOwnProperty("booking_status")
		) {
			statusText = this.getStatusText(this.props.bookingStatus.booking_status);

			if (this.props.bookingStatus.booking_status === "IN_PROGRESS") {
				cancelRideComponent = (
					<TouchableOpacity
						disabled={true}
						style={styles.handleCancelButtonInProgress}>
						<Icon
							iconType={"material"}
							iconName={"cancel"}
							iconSize={width / 15}
							iconColor={"#D3D3D3"}
						/>
						<OlaTextRegular>{I18n.t("cancel")}</OlaTextRegular>
					</TouchableOpacity>
				);
			}
		}

		let opacity = this.state.selectedReason.length === 0 ? 0.85 : 1; // used in cancel reasons

		const isCabShare =
			this.props.bookingStatus !== null &&
			this.props.bookingStatus.booking_id &&
			this.props.bookingStatus.booking_id.substring(0, 3) !== "CRN";

		return (
			<View style={styles.fullFlex}>
				{this.state.bookingCancelled ||
				this.props.bookingStatus === null ||
				!this.props.isInternet ? (
					<View style={styles.fullFlex}>
						<ProgressScreen
							progressScreen={styles.progressScreenElevation}
							indicatorColor={"#cddc39"}
						/>
						{!this.props.isInternet ? (
							<InformationFooter
								informationFooterText={I18n.t("check_internet")}
								informationFooterStyle={styles.informationFooterStyle}
								informationFooterTextStyle={styles.informationFooterTextStyle}
							/>
						) : null}
					</View>
				) : (
					<View style={styles.container}>
						{this.props.bookingStatus.booking_status !== "COMPLETED" ||
						this.props.bookingStatus.booking_status !== "BOOKING_CANCELLED" ? (
							<MapView
								mapStyle={
									this.props.bookingStatus &&
									this.props.bookingStatus.hasOwnProperty("price_details")
										? styles.mapStyleWithPriceDetails
										: styles.mapStyleWithoutPriceDetails
								}
								ref={ref => {
									this.mapRef = ref;
								}}
								showUserLocation
								followUserLocation
								onLayout={true}
								showPath={
									this.props.mapPath !== null &&
									this.props.bookingStatus.booking_status === "CALL_DRIVER"
								}
								mapPadding={styles.mapPadding}
								coords={this.props.mapPath}
								polylineColor={"#54A624"}>
								{this.props.bookingStatus.driver_lat !== undefined ? (
									<CabMarker
										ref={ref => (this.cabMarkerRef = ref)}
										coordinates={this.state.coordinate}
										img={
											!isCabShare && this.props.bookingStatus !== null
												? this.props.scaledImage[
														this.props.bookingStatus.cab_details.cab_type
												  ]
												: this.props.scaledImage.share
										}
										rotation={
											this.props.bookingStatus.bearing != null
												? this.props.bookingStatus.bearing
												: 0
										}
									/>
								) : null}
								{this.props.bookingStatus &&
									typeof this.props.bookingStatus.drop_lat !== "undefined" &&
									typeof this.props.bookingStatus.drop_lng !== "undefined" &&
									this.renderDestinationMarker({
										latitude: this.props.bookingStatus.drop_lat,
										longitude: this.props.bookingStatus.drop_lng
									})}

								{this.props.bookingStatus &&
									this.props.bookingStatus.hasOwnProperty("price_details") &&
									this.props.bookingStatus.booking_status === "CALL_DRIVER" &&
									this.props.bookingStatus.booking_status ===
										"CLIENT_LOCATED" &&
									this.renderDestinationMarker({
										latitude: this.props.bookingStatus.drop_location.lat,
										longitude: this.props.bookingStatus.drop_location.lng
									})}

								{this.props.bookingStatus.booking_status === "IN_PROGRESS" ? (
									this.props.bookingStatus &&
									typeof this.props.bookingStatus.route !== "undefined" &&
									this.props.bookingStatus.route.map((item, index) => {
										if (index < this.props.bookingStatus.route.length - 1) {
											return this.renderPassengerInShare(item, index);
										}
									})
								) : this.props.bookingStatus.booking_status ===
								  "CALL_DRIVER" ? (
									<View>
										{this.renderWaitTimeMarker()}
										{typeof this.props.bookingStatus.previous_rider_drop_lat !==
											"undefined" &&
										typeof this.props.bookingStatus.previous_rider_drop_lng !==
											"undefined" ? (
											<MapViewLib.Marker
												coordinate={{
													latitude: this.props.bookingStatus
														.previous_rider_drop_lat,
													longitude: this.props.bookingStatus
														.previous_rider_drop_lng
												}}
												description={"previous rider pickup"}
												image={SHAREDROP}
												width={width / 16.7}
												height={height / 29.5}
											/>
										) : null}
									</View>
								) : null}
							</MapView>
						) : null}
						{(!this.state.bookingCancelled &&
							this.props.bookingStatus !== null &&
							this.props.bookingStatus.booking_status !== "COMPLETED") ||
						this.props.bookingStatus.booking_status !== "BOOKING_CANCELLED" ? (
							<LocationButton
								locationStyle={[
									this.props.bookingStatus &&
									this.props.bookingStatus.hasOwnProperty("price_details")
										? [
												styles.locationButtonWithPriceDetail,
												styles.locationStyleShape
										  ]
										: [
												styles.locationButtonWithoutPriceDetail,
												styles.locationStyleShape
										  ],
									!this.state.bookingCancelled && { zIndex: 1 }
								]}
								iconColor={"#0076ff"}
								iconSize={width / 18}
								currentPosition={data => {
									this.getFitToCoordinate();
								}}
							/>
						) : null}
						{!this.props.netInfo.status ? (
							<View style={styles.netInfoView}>
								<OlaTextBold style={styles.whiteText}>
									{`${I18n.t("uh_no_internet")}`}
								</OlaTextBold>
							</View>
						) : null}
						<View style={[styles.headerBar]}>
							<TouchableOpacity onPress={this.backButtonPress}>
								<Icon
									iconType={"material"}
									iconSize={width / 15}
									iconName={"arrow-back"}
									iconColor={"#000000"}
								/>
							</TouchableOpacity>
							{statusText ? (
								<OlaTextBold style={styles.statusText}>
									{statusText}
								</OlaTextBold>
							) : (
								<Image style={styles.olaImage} source={OLA} />
							)}
							{this.props.bookingStatus.booking_status === "IN_PROGRESS" ? (
								<TouchableOpacity
									style={styles.sendSosButton}
									onPress={this.sendSosSignal}
									activeOpacity={0.5}>
									<OlaTextRegular style={styles.sendSosText}>
										{`${I18n.t("emergency")}`}
									</OlaTextRegular>
								</TouchableOpacity>
							) : (
								<View style={styles.sendSosEmptyView} />
							)}
						</View>
						{this.props.originAddr ? (
							<LocationText
								locationTextStyle={styles.originLocationText}
								placeholder={I18n.t("from")}
								locationType={"origin"}
								locationText={this.props.originAddr}
							/>
						) : null}
						{!isCabShare ? (
							<DisabledTextBox
								onPress={this.openPlaceSearch}
								disabledTextBoxStyle={
									this.state.selectedTextBox === "destination"
										? styles.disabledTextBoxForDestination
										: styles.disabledTextBoxForOrigin
								}
								textButtonStyle={styles.disabledTextBoxTextButton}
								textStyle={styles.disabledTextBoxText}
								text={this.props.destinationAddr}
								type={"destination"}
								iconColor={"#ff665a"}
								label={"Drop at"}
							/>
						) : (
							<LocationText
								locationTextStyle={styles.destinationLocationText}
								placeholder={I18n.t("to")}
								locationType={"destination"}
								locationText={this.props.destinationAddr}
							/>
						)}
						{/* Any promotional messages just above bottom bar */}
						{this.props.bookingStatus &&
						this.props.bookingStatus.hasOwnProperty("promotional_message") &&
						new RegExp("cancel").test(
							this.props.bookingStatus.promotional_message
						) ? (
							<View
								style={
									this.props.bookingStatus &&
									this.props.bookingStatus.price_details
										? styles.promotionalMessageViewWithPriceDetail
										: styles.promotionalMessageViewWithoutPriceDetail
								}>
								<Icon
									iconType={"entypo"}
									iconName={"info-with-circle"}
									iconColor={"grey"}
									iconSize={width / 20}
									style={styles.promotionMessageIcon}
								/>
								<OlaTextRegular
									numberOfLines={2}
									ellipsizeMode={"tail"}
									style={styles.promotionMessageText}>
									{this.props.bookingStatus.promotional_message}
								</OlaTextRegular>
							</View>
						) : null}
						{/* Check for share which give price details */}
						<View
							style={[
								styles.trackRideDetails,
								this.props.bookingStatus &&
								this.props.bookingStatus.hasOwnProperty("price_details")
									? styles.checkShareViewWithPriceDetails
									: styles.checkShareViewWithoutPriceDetails
							]}>
							{this.props.bookingStatus &&
							this.props.bookingStatus.hasOwnProperty("price_details") ? (
								<View>
									<View style={styles.cashToPaidView}>
										<View style={styles.cashToPaidInnerView}>
											<OlaTextRegular style={styles.cashToPaidHeaderText}>
												{`${I18n.t("cash_to_paid")}`}
											</OlaTextRegular>
											<OlaTextRegular style={styles.cashToPaidText}>
												{`₹ ${this.props.bookingStatus.price_details.fare}`}
											</OlaTextRegular>
										</View>
										{this.props.bookingStatus &&
										this.props.bookingStatus.hasOwnProperty("passengers") &&
										this.props.bookingStatus.hasOwnProperty("route") &&
										this.props.bookingStatus.route.length > 2 ? (
											<View style={styles.rowView}>
												<Seperator
													width={1}
													height={height / 23}
													color={"#dfdfdf"}
												/>
												<View style={styles.travellingWithView}>
													<View>
														<OlaTextRegular style={styles.travellingWithText}>
															{`${I18n.t("travelling_with")}`}
														</OlaTextRegular>
														<OlaTextRegular style={styles.totalPassengerText}>
															{`${this.props.bookingStatus.passengers.length -
																1} other`}
														</OlaTextRegular>
													</View>
													<View>
														<TouchableOpacity
															onPress={this.openRouteDetailsModal}>
															<Icon
																iconType={"simple_line"}
																iconName="options"
																iconSize={width / 25}
																iconColor={"grey"}
																style={styles.optionIcon}
															/>
														</TouchableOpacity>
													</View>
												</View>
												}
											</View>
										) : null}
									</View>
									<Seperator width={width} height={1} color={"#dfdfdf"} />
								</View>
							) : null}
							{/* Bottom strip with driver details and actions */}
							<View
								style={[
									styles.otpView,
									typeof this.props.bookingStatus.otp === "undefined" && {
										justifyContent: "flex-start"
									}
								]}>
								<View style={styles.otpInnerView}>
									<Image
										source={{
											uri: this.props.bookingStatus
												? this.props.bookingStatus.driver_image_url
												: ""
										}}
										style={styles.otpImage}
									/>
								</View>
								<View style={styles.driverNameView}>
									<OlaTextBold style={styles.driverNameText}>
										{this.props.bookingStatus !== null
											? this.props.bookingStatus.driver_name
											: ""}
									</OlaTextBold>
									<OlaTextRegular style={styles.normalText}>
										{this.props.bookingStatus !== null
											? typeof this.props.bookingStatus.pickup_location !==
											  "undefined"
												? this.props.bookingStatus.vehicle_color +
												  " " +
												  this.props.bookingStatus.vehicle_type
												: this.props.bookingStatus.cab_details &&
												  typeof this.props.bookingStatus.cab_details
														.car_color !== "undefined"
												? this.props.bookingStatus.cab_details.car_color +
												  " " +
												  this.props.bookingStatus.cab_details.car_model
												: ""
											: ""}
									</OlaTextRegular>
									<OlaTextRegular style={styles.normalText}>
										{this.props.bookingStatus !== null
											? typeof this.props.bookingStatus.pickup_location !==
											  "undefined"
												? this.props.bookingStatus.vehicle_number
												: this.props.bookingStatus.cab_details &&
												  this.props.bookingStatus.cab_details.cab_number
											: ""}
									</OlaTextRegular>
								</View>
								{this.props.bookingStatus != null &&
								typeof this.props.bookingStatus.otp !== "undefined" ? (
									<View style={styles.otpSectionView}>
										<OlaTextRegular style={styles.otpHeaderText}>
											{"OTP: "}
										</OlaTextRegular>
										<OlaTextBold style={styles.otpValueText}>
											{this.props.bookingStatus.otp.start_trip.value}
										</OlaTextBold>
									</View>
								) : null}
							</View>
							<Seperator width={width} height={1} color={"#dfdfdf"} />
							<View style={styles.contactView}>
								<TouchableOpacity
									onPress={this.callDriver}
									style={styles.contactButton}>
									<Icon
										iconType={"font_awesome"}
										iconName="phone"
										iconSize={width / 15}
										iconColor={"#000000"}
									/>
									<OlaTextRegular>{I18n.t("contact")}</OlaTextRegular>
								</TouchableOpacity>
								<TouchableOpacity
									onPress={this.shareRide}
									style={styles.shareDetailButton}>
									<Icon
										iconType={"ionicon"}
										iconName={"md-share"}
										iconSize={width / 15}
										iconColor={"#000000"}
									/>
									<OlaTextRegular style={{ textAlign: "center" }}>
										{I18n.t("share_details")}
									</OlaTextRegular>
								</TouchableOpacity>
								{cancelRideComponent}
								<TouchableOpacity
									style={styles.supportButton}
									onPress={() =>
										this.props.navigation.navigate("Support", {
											booking_ref_no: this.props.bookingStatus.booking_id.slice(
												3,
												this.props.bookingStatus.booking_id.length
											),
											brand:
												this.props.bookingStatus.booking_id.substring(0, 3) ===
												"CRN"
													? "ola_cabs"
													: "ola_share",
											current_lat: this.props.origin.latitude,
											current_lng: this.props.origin.longitude,
											userToken: this.props.userToken
										})
									}>
									<Icon
										iconType={"font_awesome"}
										iconName={"support"}
										iconSize={width / 15}
										iconColor={"#000000"}
									/>
									<OlaTextRegular>{I18n.t("support")}</OlaTextRegular>
								</TouchableOpacity>
							</View>
						</View>
						{/* Cancel reasons */}
						<DialogModal
							dialogModalBackgroundStyle={styles.dialogModalBackgroundStyle}
							visible={this.state.showCancelReasons}
							onRequestClose={() =>
								this.setState({ showCancelReasons: false })
							}>
							<View style={styles.cancelMainView}>
								<View>
									{this.state.loadingCancelReasons ? (
										<ProgressScreen />
									) : Object.keys(this.state.cancelReasonsResp).length ==
									  0 ? null : (
										<FlatList
											style={{ flexGrow: 0 }}
											data={
												this.state.cancelReasonsResp &&
												this.state.cancelReasonsResp.cancel_reasons &&
												this.props.bookingStatus.booking_id.substring(0, 3) ===
													"CRN"
													? this.state.cancelReasonsResp.cancel_reasons[
															this.props.bookingStatus.cab_details.cab_type
													  ]
													: this.state.cancelReasonsResp.cancel_reasons.share
											}
											ListHeaderComponent={() => (
												<View>
													<OlaTextBold style={styles.cancelRideHeaderText}>
														{`${I18n.t("cancel_ride")}`}
													</OlaTextBold>
													<View style={styles.cancelRideEmptyView} />
												</View>
											)}
											ListFooterComponent={() => (
												<View>
													{this.props.bookingStatus &&
													this.props.bookingStatus.hasOwnProperty(
														"promotional_message"
													) &&
													new RegExp("cancel").test(
														this.props.bookingStatus.promotional_message
													) ? (
														<View style={styles.infoIconView}>
															<Icon
																iconType={"entypo"}
																iconName={"info-with-circle"}
																iconColor={"grey"}
																iconSize={width / 20}
																style={styles.infoIcon}
															/>
															<OlaTextRegular
																numberOfLines={2}
																ellipsizeMode={"tail"}
																style={styles.promotionMessageGreyText}>
																{this.props.bookingStatus.promotional_message}
															</OlaTextRegular>
														</View>
													) : null}
													<View style={styles.centeredRow}>
														<TouchableWithoutFeedback
															onPress={() =>
																this.setState({
																	showCancelReasons: false,
																	selectedReason: ""
																})
															}>
															<View style={styles.dontCancelView}>
																<OlaTextMedium style={styles.dontCancelText}>
																	{`${I18n.t("dont_cancel")}`}
																</OlaTextMedium>
															</View>
														</TouchableWithoutFeedback>
														<TouchableWithoutFeedback
															disabled={this.state.selectedReason.length === 0}
															onPress={() => {
																console.log("Hello From Cancel Button");
																this.cancelRide(this.state.selectedReason);
															}}>
															<View
																style={[
																	styles.dontCancelView,
																	{ opacity: opacity }
																]}>
																<OlaTextMedium style={styles.dontCancelText}>
																	{`${I18n.t("cancel_C")}`}
																</OlaTextMedium>
															</View>
														</TouchableWithoutFeedback>
													</View>
												</View>
											)}
											renderItem={this.renderCancelReasons}
											keyExtractor={(item, index) => index.toString()}
										/>
									)}
								</View>
							</View>
						</DialogModal>
						<RouteDetailsModal
							visible={this.state.isRouteDetailsModalOpen}
							onRequestClose={this.closeRouteDetailsModal}
							routeData={
								this.props.bookingStatus &&
								this.props.bookingStatus.hasOwnProperty("route")
									? this.props.bookingStatus.route
									: []
							}
							passengers={
								this.props.bookingStatus &&
								this.props.bookingStatus.hasOwnProperty("passengers")
									? this.props.bookingStatus.passengers
									: []
							}
						/>
					</View>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		origin: state.ola && state.ola.bookRide.origin,
		mapPath: state.ola && state.ola.chooseRide.mapPath,
		userToken: state.ola && state.ola.auth.userCredentials,
		destination: state.ola && state.ola.bookRide.destination,
		bookingStatus: state.ola && state.ola.chooseRide.bookingStatus,
		rentalBookingStatus:
			state.ola && state.ola.chooseRentalRideReducer.bookingStatus,
		cancelRides: state.ola && state.ola.cancelRides.cancelReasons,
		originAddr: state.ola && state.ola.cancelRides.originAddr,
		destinationAddr: state.ola && state.ola.cancelRides.destinationAddr,
		updateStatus: state.ola && state.ola.chooseRide.updateStatus,
		netInfo: state.netInfo,
		isInternet: state.netInfo.status,
		scaledImage: state.ola && state.ola.bookRide.scaledImage,
		appToken: state.appToken.token
	};
}

export default connect(mapStateToProps)(TrackRide);
