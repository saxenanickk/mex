import {
	OLA_CANCEL_REASONS,
	OLA_SAVE_ORIGIN_ADDRESS,
	OLA_SAVE_DESTINATION_ADDRESS
} from "./Saga";

const initialState = {
	cancelReasons: [],
	originAddr: null,
	destinationAddr: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case OLA_CANCEL_REASONS:
			return {
				...state,
				cancelReasons: action.payload
			};
		case OLA_SAVE_ORIGIN_ADDRESS:
			return {
				...state,
				originAddr: action.payload.originAddr
			};

		case OLA_SAVE_DESTINATION_ADDRESS:
			return {
				...state,
				destinationAddr: action.payload.destinationAddr
			};
		default:
			return state;
	}
};
