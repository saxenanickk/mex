import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

export const indexStyles = StyleSheet.create({
	container: {
		flex: 1,
		borderWidth: 0,
		marginTop: Platform.OS === "ios" ? 18 : null
	},
	spinnerContainer: {
		flex: 1,
		borderWidth: 0,
		alignItems: "center",
		justifyContent: "center",
		marginTop: Platform.OS === "ios" ? 18 : null
	},
	map: {
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		zIndex: -10,
		borderWidth: 0,
		position: "absolute"
	},
	headerBar: {
		elevation: 0,
		backgroundColor: "#ffffff",
		justifyContent: "space-between",
		alignItems: "center",
		padding: width / 35,
		flexDirection: "row",
		width: width / 1,
		height: height / 12
	},
	trackRideDetails: {
		elevation: 5,
		position: "absolute",
		borderRadius: width / 140,
		bottom: 0,
		alignSelf: "center",
		width: width,
		height: height / 4.2,
		alignItems: "center",
		borderColor: "#ffffff",
		justifyContent: "center",
		backgroundColor: "#ffffff"
	},
	statusBar: {
		padding: width / 70,
		width: width / 1.15,
		alignItems: "center",
		marginLeft: width / 15,
		justifyContent: "center",
		backgroundColor: "#FFFFFF"
	},
	handleCancelButton: {
		justifyContent: "center",
		alignItems: "center",
		height: height / 10,
		width: width / 4,
		borderColor: "#000000"
	},
	handleCancelButtonInProgress: {
		justifyContent: "center",
		alignItems: "center",
		height: height / 10,
		width: width / 4,
		borderColor: "#D3D3D3"
	},
	fullFlex: { flex: 1 },
	progressScreenElevation: { elevation: 6 },
	mapStyleWithPriceDetails: {
		top: height / 12,
		bottom: height / 3.5
	},
	mapStyleWithoutPriceDetails: {
		top: height / 12,
		bottom: height / 4.2
	},
	mapPadding: {
		top: 100,
		right: 100,
		bottom: 100,
		left: 100
	},
	waitTimeImage: {
		width: width / 11,
		height: height / 12
	},
	waitTimeImageRandomText: { width: 0, height: 0 },
	waitTimeImageText: {
		color: "#fff",
		alignSelf: "center",
		fontSize: width / 30,
		marginLeft: width / 60
	},
	locationStyleShape: {
		width: width / 10.5,
		height: width / 10.5,
		borderRadius: width / 21
	},
	locationButtonWithPriceDetail: {
		elevation: 1,
		bottom: height / 2.5
	},
	locationButtonWithoutPriceDetail: {
		elevation: 1,
		bottom: height / 3
	},
	netInfoView: {
		position: "absolute",
		flexDirection: "row",
		top: height / 1.47,
		height: width / 13,
		width: width,
		backgroundColor: "#f00",
		padding: width / 40,
		alignSelf: "center",
		alignItems: "center",
		justifyContent: "space-around"
	},
	whiteText: {
		color: "#fff"
	},
	statusText: {
		fontSize: width / 23,
		color: "#000"
	},
	olaImage: {
		width: width / 5,
		height: height / 20,
		marginLeft: width / 7
	},
	sendSosButton: {
		width: width / 5,
		height: height / 25,
		elevation: 10,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 3,
		backgroundColor: "#B20033"
	},
	sendSosText: {
		color: "#ffffff",
		fontSize: width / 30
	},
	sendSosEmptyView: {
		width: width / 4,
		height: height / 20
	},
	originLocationText: {
		elevation: 0
	},
	disabledTextBoxForDestination: {
		top: height / 7.6,
		left: width / 30,
		zIndex: 3,
		width: width / 1.1,
		elevation: 5,
		backgroundColor: "#fff",
		opacity: 1,
		borderWidth: 0,
		borderColor: "#fff",
		borderRadius: width / 100
	},
	disabledTextBoxForOrigin: {
		top: height / 7.6,
		left: width / 30,
		zIndex: 3,
		width: width / 1.1,
		elevation: 3,
		backgroundColor: "#fff",
		opacity: 0.8,
		borderWidth: 0,
		borderColor: "#fff",
		borderRadius: width / 100
	},
	disabledTextBoxTextButton: {
		borderWidth: 0,
		borderColor: "#000000",
		borderRadius: width / 100,
		backgroundColor: "#ffffff"
	},
	disabledTextBoxText: {
		color: "#000",
		fontSize: width / 30.3
	},
	destinationLocationText: {
		elevation: 0
	},
	promotionalMessageViewWithPriceDetail: {
		position: "absolute",
		flexDirection: "row",
		height: width / 7,
		width: width / 1.05,
		elevation: 5,
		backgroundColor: "#ffffff",
		opacity: 1,
		borderWidth: 0,
		borderColor: "#ffffff",
		borderRadius: width / 100,
		padding: width / 40,
		alignSelf: "center",
		justifyContent: "space-around",
		top: height / 1.72
	},
	promotionalMessageViewWithoutPriceDetail: {
		position: "absolute",
		flexDirection: "row",
		height: width / 7,
		width: width / 1.05,
		elevation: 5,
		backgroundColor: "#ffffff",
		opacity: 1,
		borderWidth: 0,
		borderColor: "#ffffff",
		borderRadius: width / 100,
		padding: width / 40,
		alignSelf: "center",
		justifyContent: "space-around",
		top: height / 1.6
	},
	promotionMessageIcon: {
		paddingRight: width / 50,
		paddingLeft: width / 30,
		paddingTop: width / 70
	},
	promotionMessageText: {
		color: "grey",
		fontSize: width / 30,
		marginLeft: width / 20,
		marginRight: width / 20,
		marginTop: width / 200
	},
	checkShareViewWithPriceDetails: {
		height: height / 3.5
	},
	checkShareViewWithoutPriceDetails: {
		height: height / 4.2
	},
	cashToPaidView: {
		flexDirection: "row",
		width: width / 1.1,
		alignSelf: "center",
		justifyContent: "space-between"
	},
	cashToPaidInnerView: {
		justifyContent: "center",
		width: width / 2.2
	},
	cashToPaidHeaderText: {
		fontSize: width / 35
	},
	cashToPaidText: {
		fontSize: width / 28,
		color: "#000"
	},
	rowView: {
		flexDirection: "row"
	},
	travellingWithView: {
		flexDirection: "row",
		justifyContent: "space-around",
		width: width / 2.2,
		paddingLeft: width / 55
	},
	travellingWithText: {
		fontSize: width / 35
	},
	totalPassengerText: {
		fontSize: width / 28,
		color: "#000"
	},
	optionIcon: {
		margin: width / 37,
		paddingLeft: width / 20
	},
	otpView: {
		padding: width / 35,
		justifyContent: "space-between",
		alignItems: "center",
		flexDirection: "row",
		borderWidth: 0,
		borderColor: "#000000",
		width: width / 1.05,
		height: height / 8
	},
	otpInnerView: {
		justifyContent: "center",
		alignItems: "center",
		height: height / 7,
		borderColor: "#000000"
	},
	otpImage: {
		width: width / 7,
		height: width / 7,
		borderWidth: 0.5,
		borderColor: "#000000",
		borderRadius: width / 14
	},
	driverNameView: {
		justifyContent: "center",
		width: width / 2,
		height: height / 7,
		borderColor: "#000000"
	},
	driverNameText: {
		color: "#000000",
		fontSize: 16
	},
	normalText: {
		fontSize: 14
	},
	otpSectionView: {
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		height: height / 7,
		borderColor: "#000000"
	},
	otpHeaderText: {
		color: "#000000",
		fontSize: 14,
		backgroundColor: "#f8ffc9"
	},
	otpValueText: {
		color: "#000000",
		fontSize: 14,
		backgroundColor: "#f8ffc9"
	},
	contactView: {
		justifyContent: "center",
		alignItems: "center",
		flexDirection: "row",
		borderWidth: 0,
		borderColor: "#000000",
		width: width
	},
	contactButton: {
		justifyContent: "center",
		alignItems: "center",
		height: height / 10,
		width: width / 4,
		borderWidth: 0,
		borderColor: "#000000"
	},
	shareDetailButton: {
		justifyContent: "center",
		alignItems: "center",
		width: width / 4,
		borderWidth: 0,
		borderColor: "#000000"
	},
	supportButton: {
		justifyContent: "center",
		alignItems: "center",
		height: height / 10,
		width: width / 4,
		borderWidth: 0,
		borderColor: "#000000"
	},
	dialogModalBackgroundStyle: {
		backgroundColor: "rgba(0,0,0,0.6)"
	},
	cancelMainView: {
		top: height / 3.5,
		width: width / 1.15,
		borderColor: "#ffffff",
		borderRadius: width / 140,
		elevation: 5,
		alignSelf: "center",
		position: "absolute",
		justifyContent: "flex-start",
		alignItems: "center",
		backgroundColor: "#ffffff"
	},
	cancelRideHeaderText: {
		alignSelf: "center",
		padding: 10
	},
	cancelRideEmptyView: {
		borderBottomColor: "grey",
		borderBottomWidth: 1,
		marginHorizontal: width / 35
	},
	infoIconView: {
		flexDirection: "row",
		backgroundColor: "#f8ffc9",
		justifyContent: "space-around",
		height: height / 18
	},
	infoIcon: {
		marginRight: width / 20,
		marginLeft: width / 15,
		paddingTop: width / 70
	},
	promotionMessageGreyText: {
		color: "grey",
		fontSize: width / 32,
		marginLeft: width / 20,
		marginRight: width / 20,
		marginTop: width / 200
	},
	centeredRow: {
		flexDirection: "row",
		alignItems: "center"
	},
	dontCancelView: {
		borderLeftWidth: 0.5,
		borderColor: "#4f4f4f",
		justifyContent: "center",
		borderRadius: 0,
		alignItems: "center",
		width: width / 2.3,
		height: height / 15,
		backgroundColor: "#0f0f0f"
	},
	dontCancelText: {
		color: "#CCE500",
		fontSize: width / 30
	},
	cancelReasonButton: {
		padding: 10,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	}
});

export const routeDetailModalStyles = StyleSheet.create({
	headerBar: {
		elevation: 7,
		backgroundColor: "#ffffff",
		padding: width / 35,
		flexDirection: "row",
		width: width / 1,
		height: height / 12
	},
	rowView: {
		flexDirection: "row"
	},
	pickupView: {
		alignSelf: "center",
		width: width / 30,
		height: width / 30,
		borderRadius: 10,
		backgroundColor: "green",
		marginRight: width / 30
	},
	sharePickupImage: {
		width: width / 26,
		height: width / 26,
		borderWidth: 0.5,
		borderColor: "#000000",
		borderRadius: width / 14,
		marginRight: width / 30
	},
	sharePickupText: { marginTop: 0, alignSelf: "center" },
	yourDropText: {
		alignSelf: "center",
		width: width / 30,
		height: width / 30,
		borderRadius: 20,
		backgroundColor: "red",
		marginRight: width / 30
	},
	centeredRow: { flexDirection: "row", alignSelf: "center" },
	yourRouteText: {
		fontSize: width / 23,
		color: "#000",
		alignSelf: "center"
	},
	doneButton: {
		padding: width / 40,
		alignSelf: "center",
		justifyContent: "flex-end"
	},
	itemSeparator: {
		marginLeft: width / 26,
		width: width / 65,
		height: height / 30,
		backgroundColor: "#0ce2ff",
		borderRadius: 5
	},
	informationFooterStyle: { backgroundColor: "#ff0000" },
	informationFooterTextStyle: { color: "rgba(255,255,255,0.8)" },
	marginInRideDetail: { margin: width / 30 }
});
