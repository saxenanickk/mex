import React from "react";
import {
	Modal,
	Dimensions,
	View,
	TouchableOpacity,
	FlatList,
	StyleSheet,
	Image
} from "react-native";
import { SHAREDROP, SHAREPICKUP } from "../../Assets/Img/Image";
import { OlaTextRegular, OlaTextMedium } from "../../Components/OlaText";
import { routeDetailModalStyles as styles } from "./style";
import I18n from "../../Assets/Strings/i18n";
const { width, height } = Dimensions.get("window");

class RouteDetailsModal extends React.Component {
	static defaultProps = {
		visible: false,
		onRequestClose: () => {},
		routeData: [],
		passengers: []
	};

	renderRoute = data => {
		if (data && data.item) {
			const user = this.props.passengers.filter(
				passenger =>
					passenger.name !== data.item.name &&
					passenger.state !== data.item.state
			);

			if (data && data.item.state === "pickup") {
				return (
					<View style={styles.marginInRideDetail}>
						{user.length === 1 ? (
							<View style={styles.rowView}>
								<View style={styles.pickupView} />
								<OlaTextRegular>{`${I18n.t("your_pickup")}`}</OlaTextRegular>
							</View>
						) : (
							<View style={styles.rowView}>
								<Image source={SHAREPICKUP} style={styles.sharePickupImage} />
								<OlaTextRegular style={styles.sharePickupText}>
									{`Pickup ${data.item.name}`}
								</OlaTextRegular>
							</View>
						)}
					</View>
				);
			} else if (data && data.item.state === "drop") {
				return (
					<View style={styles.marginInRideDetail}>
						{user.length === 1 ? (
							<View style={styles.rowView}>
								<View style={styles.yourDropText} />
								<OlaTextRegular>{`${I18n.t("your_drop")}`}</OlaTextRegular>
							</View>
						) : (
							<View style={styles.rowView}>
								<Image source={SHAREDROP} style={styles.sharePickupImage} />
								<OlaTextRegular style={styles.sharePickupText}>{`${I18n.t(
									"drop"
								)} ${data.item.name}`}</OlaTextRegular>
							</View>
						)}
					</View>
				);
			}
		}
	};

	render() {
		const { visible, onRequestClose, routeData, passengers } = this.props;

		return (
			<Modal
				visible={visible}
				animationType={"slide"}
				transparent={false}
				onRequestClose={onRequestClose}>
				<View>
					<View style={[styles.headerBar]}>
						<View style={styles.centeredRow}>
							<OlaTextMedium style={styles.yourRouteText}>
								{`${I18n.t("your_route")}`}
							</OlaTextMedium>
						</View>
						<TouchableOpacity
							style={styles.doneButton}
							onPress={onRequestClose}>
							<View>
								<OlaTextRegular>{`${I18n.t("done")}`}</OlaTextRegular>
							</View>
						</TouchableOpacity>
					</View>
					<FlatList
						data={routeData}
						ItemSeparatorComponent={() => <View style={styles.itemSeparator} />}
						renderItem={this.renderRoute}
						keyExtractor={(item, index) => index.toString()}
					/>
				</View>
			</Modal>
		);
	}
}

export default RouteDetailsModal;
