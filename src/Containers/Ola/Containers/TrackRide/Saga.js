import { takeLatest, call, put } from "redux-saga/effects";
import OlaApi from "../../Api";
import { olaBookingStatus } from "../ChooseRide/Saga";
import { GoToast } from "../../../../Components";
import I18n from "../../Assets/Strings/i18n";

/**
 * Actions
 */
export const OLA_CANCEL_RIDE = "OLA_CANCEL_RIDE";
export const OLA_SOS_SIGNAL = "OLA_SOS_SIGNAL";
export const OLA_CANCEL_REASONS = "OLA_CANCEL_REASONS";
export const OLA_GET_CANCEL_REASONS = "OLA_GET_CANCEL_REASONS";
export const OLA_SAVE_ORIGIN_ADDRESS = "OLA_SAVE_ORIGIN_ADDRESS";
export const OLA_SAVE_DESTINATION_ADDRESS = "OLA_SAVE_DESTINATION_ADDRESS";

/**
 * Action Creators
 */
export const olaCancelRide = payload => ({ type: OLA_CANCEL_RIDE, payload });
export const olaSosSignal = payload => ({ type: OLA_SOS_SIGNAL, payload });
export const olaGetCancelReasons = payload => ({
	type: OLA_GET_CANCEL_REASONS,
	payload
});
export const olaCancelReasons = payload => ({
	type: OLA_CANCEL_REASONS,
	payload
});
export const olaSaveOriginAddress = payload => ({
	type: OLA_SAVE_ORIGIN_ADDRESS,
	payload
});

export const olaSaveDestinationAddress = payload => ({
	type: OLA_SAVE_DESTINATION_ADDRESS,
	payload
});

export function* olaTrackRideSaga(dispatch) {
	yield takeLatest(OLA_CANCEL_RIDE, handleOlaCancelRide);
	yield takeLatest(OLA_SOS_SIGNAL, handleOlaSosSignal);
	yield takeLatest(OLA_GET_CANCEL_REASONS, handleOlaGetCancelReasons);
}

/**
 * Handlers
 */
function* handleOlaCancelRide(action) {
	try {
		let cancelRide = yield call(OlaApi.cancelRide, action.payload);
		console.log("Ola Cancel Ride Data: ", cancelRide);
		cancelRide.status === "SUCCESS"
			? yield put(olaBookingStatus(cancelRide))
			: GoToast.show(cancelRide.text, I18n.t("information"));
	} catch (error) {
		console.log("Ola Cancel Ride Error: ", error);
	}
}

function* handleOlaSosSignal(action) {
	try {
		let sosSignal = yield call(OlaApi.sosSignal, action.payload);
		console.log("Ola Sos Signal Data: ", sosSignal);
	} catch (error) {
		console.log("Ola Sos Signal Error: ", error);
	}
}

function* handleOlaGetCancelReasons(action) {
	try {
		let cancelReasons = yield call(OlaApi.getCancelReasons, action.payload);
		console.log(`Cancel Reasons array: ${cancelReasons}`);
		yield put(olaCancelReasons(cancelReasons));
	} catch (error) {
		console.log(`unable to get cancel reasons: ${error}`);
	}
}
