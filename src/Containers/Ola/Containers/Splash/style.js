import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const indexStyles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#ffffff",
		justifyContent: "center",
		alignItems: "center"
	},
	splashImage: { width: width / 4, height: width / 4 }
});
