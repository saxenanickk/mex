import React from "react";
import { View, Image, Dimensions, StatusBar, PixelRatio } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import { SPLASH } from "../../Assets/Img/Image";
import LocationModule from "../../../../CustomModules/LocationModule";
import { indexStyles as styles } from "./style";
import {
	olaSaveScaledImage,
	olaOrigin,
	olaDestination,
	olaFetchOrigin,
	olaFetchDestination
} from "../Home/BookRide/Saga";
import ApplicationToken from "../../../../CustomModules/ApplicationToken";
import Config from "react-native-config";
import { CommonActions } from "@react-navigation/native";
const { CLOUDINARY_BASE_URL } = Config;
const { width, height } = Dimensions.get("window");

const carImages = {
	share: "v1574239377/bfb399d0-0b71-11ea-b943-cd44a8b866a7_100x100.png",
	micro: "v1574239409/d2bdde50-0b71-11ea-a101-278f847c8962_100x100.png",
	mini: "v1574239478/fbce9050-0b71-11ea-b0d5-b04ed96baede_100x100.png",
	prime: "v1574239523/171fd1c0-0b72-11ea-a085-8e1f4b285396_100x100.png",
	prime_play: "v1574239565/30115140-0b72-11ea-90ff-fe2d463f1815_100x100.png",
	suv: "v1574239614/4cdcaf90-0b72-11ea-a536-79f98a88e2b3_100x100.png",
	exec: "v1574239523/171fd1c0-0b72-11ea-a085-8e1f4b285396_100x100.png",
	auto: "v1574239660/684dae00-0b72-11ea-8223-42c6a8e11de2_100x100.png",
	rental: "v1574239682/7556edf0-0b72-11ea-8c82-e833ae57dc74_100x100.png",
	outstation: "v1574239707/84d48940-0b72-11ea-9d8f-6fac5839ba16_100x100.png",
	lux: "v1574239731/92cc6ef0-0b72-11ea-b556-e3ef40d51356_100x100.png",
	locationPickup:
		"v1574239770/a9b9e110-0b72-11ea-b5a7-05ca88b824d0_100x100.png",
	locationDrop: "v1574239794/b82dad80-0b72-11ea-9d7a-792f357b563c_100x100.png"
};

class Splash extends React.Component {
	navigateTo = screenName => {
		this.props.navigation.dispatch(
			CommonActions.reset({
				index: 0,
				routes: [{ name: screenName }]
			})
		);
	};

	/**
	 * Function to check all possible scenarios before opening OLA
	 */
	onboardingChecks = () => {
		// setTimeout(() => {
		/** Handle the flow if user credentials are already present */
		// if (this.props && this.props.deeplink && this.props.deeplink.params) {
		// 	const {
		// 		pickup_lat,
		// 		pickup_lng,
		// 		drop_lat,
		// 		drop_lng,
		// 		type,
		// 	} = this.props.deeplink.params
		// 	if (pickup_lat !== undefined && pickup_lng !== undefined) {
		// 		this.props.dispatch(
		// 			olaFetchOrigin({
		// 				latitude: parseFloat(pickup_lat),
		// 				longitude: parseFloat(pickup_lng),
		// 			}),
		// 		)
		// 	}
		// 	if (drop_lat !== undefined && drop_lng !== undefined) {
		// 		this.props.dispatch(
		// 			olaFetchDestination({
		// 				latitude: parseFloat(drop_lat),
		// 				longitude: parseFloat(drop_lng),
		// 			}),
		// 		)
		// 	}

		// 	switch (type) {
		// 		case "bookride":
		// 			this.navigateTo("ChooseRide")
		// 			break
		// 		default:
		// 			this.navigateTo("Home")
		// 	}
		// } else {
		this.getLocationFromStorage();
		this.navigateTo("Home");
		// }
		// }, 1000)
	};

	scaleImageForMap() {
		Object.keys(carImages).map(imageKey => {
			let imagePath = carImages[imageKey];
			try {
				let widthForScale =
					imageKey === "locationPickup" || imageKey === "locationDrop"
						? width / 8
						: width / 24;
				let heightForScale =
					imageKey === "locationPickup" || imageKey === "locationDrop"
						? height / 18
						: width / 12;

				let imageWidth = PixelRatio.getPixelSizeForLayoutSize(widthForScale);
				let imageHeight = PixelRatio.getPixelSizeForLayoutSize(heightForScale);

				let successURI = `${CLOUDINARY_BASE_URL}w_${imageWidth},h_${imageHeight},c_scale/${imagePath}`;

				this.props.dispatch(
					olaSaveScaledImage({ key: imageKey, path: successURI })
				);
			} catch (error) {
				console.log("error", error);
				this.props.dispatch(
					olaSaveScaledImage({
						key: imageKey,
						path: `${CLOUDINARY_BASE_URL}${imagePath}`
					})
				);
			}
		});
	}

	getLocationFromDeeplink = params => {
		if (params.origin) {
			console.log("xxxxxxx", "Ola origin from deeplink", params.origin);
			this.props.dispatch(olaFetchOrigin(params.origin));
		}
		if (params.destination) {
			console.log(
				"xxxxxxx",
				"Ola destination from deeplink",
				params.destination
			);
			this.props.dispatch(olaFetchDestination(params.destination));
		}
	};

	getLocationFromStorage = () => {
		AsyncStorage.getItem("origin")
			.then(origin => {
				if (origin !== null) {
					console.log("xxxxxxx", "Ola origin from storage", origin);
					let originData = JSON.parse(origin);
					this.props.dispatch(olaOrigin(originData));
				}
			})
			.catch(error => {
				console.log("Error while retrieving origin: ", error);
			});
		AsyncStorage.getItem("destination")
			.then(destination => {
				if (destination !== null) {
					console.log("xxxxxxx", "Ola destination from storage", destination);
					let destinationData = JSON.parse(destination);
					this.props.dispatch(olaDestination(destinationData));
				}
			})
			.catch(error => {
				console.log("Error while retrieving destination: ", error);
			});
	};

	componentDidMount() {
		this.scaleImageForMap();
		this.onboardingChecks();
	}

	componentWillReceiveProps(props) {
		// if (props.location.status !== null) this.onboardingChecks()
	}

	render() {
		return (
			<View style={styles.container}>
				{this.props.location.status === null && <LocationModule />}
				<StatusBar backgroundColor="#7f7f7f" barStyle="light-content" />
				<ApplicationToken />
				<Image style={styles.splashImage} source={SPLASH} />
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		location: state.location,
		deeplink: state.ola && state.ola.navState.deeplink
	};
}

export default connect(mapStateToProps)(Splash);
