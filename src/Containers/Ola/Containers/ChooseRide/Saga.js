import { takeLatest, takeEvery, call, put } from "redux-saga/effects";
import OlaApi from "../../Api";
import AsyncStorage from "@react-native-community/async-storage";
/**
 * Constans
 */

export const OLA_MAP_PATH = "OLA_MAP_PATH";
export const OLA_MAP_POLYLINE = "OLA_MAP_POLYLINE";
export const OLA_REQUEST_RIDE = "OLA_REQUEST_RIDE";
export const OLA_CURRENT_RIDE = "OLA_CURRENT_RIDE";
export const OLA_ABORT_BOOKING = "OLA_ABORT_BOOKING";
export const OLA_BOOKING_STATUS = "OLA_BOOKING_STATUS";
export const OLA_BOOKING_STATUS_ERROR = "OLA_BOOKING_STATUS_ERROR";
export const OLA_SAVE_COUPON_CODE = "OLA_SAVE_COUPON_CODE";
export const OLA_GET_RIDE_ESTIMATE = "OLA_GET_RIDE_ESTIMATE";
export const OLA_SAVE_RIDE_ESTIMATE = "OLA_SAVE_RIDE_ESTIMATE";
export const OLA_VALIDATE_COUPON_CODE = "OLA_VALIDATE_COUPON_CODE";
export const OLA_UPDATE_DROP_LOCATION = "OLA_UPDATE_DROP_LOCATION";
export const OLA_UPDATE_DROP_LOCATION_STATUS =
	"OLA_UPDATE_DROP_LOCATION_STATUS";
export const OLA_SET_NAVIGATION_TO_TRACK_RIDE =
	"OLA_SET_NAVIGATION_TO_TRACK_RIDE";
/**
 * Action Creators
 */

export const olaMapPath = payload => ({ type: OLA_MAP_PATH, payload });
export const olaCurrentRide = payload => ({ type: OLA_CURRENT_RIDE, payload });
export const olaMapPolyline = payload => ({ type: OLA_MAP_POLYLINE, payload });
export const olaRequestRide = payload => ({ type: OLA_REQUEST_RIDE, payload });
export const olaAbortBooking = payload => ({
	type: OLA_ABORT_BOOKING,
	payload
});
export const updateDropLocation = payload => ({
	type: OLA_UPDATE_DROP_LOCATION,
	payload
});
export const updateDropLocationStatus = payload => ({
	type: OLA_UPDATE_DROP_LOCATION_STATUS,
	payload
});
export const olaBookingStatus = payload => ({
	type: OLA_BOOKING_STATUS,
	payload
});
export const olaBookingStatusError = () => ({
	type: OLA_BOOKING_STATUS_ERROR
});
export const olaSaveCouponCode = payload => ({
	type: OLA_SAVE_COUPON_CODE,
	payload
});
export const olaGetRideEstimate = payload => ({
	type: OLA_GET_RIDE_ESTIMATE,
	payload
});
export const olaSaveRideEstimate = payload => ({
	type: OLA_SAVE_RIDE_ESTIMATE,
	payload
});
export const olaVaidateCouponCode = payload => ({
	type: OLA_VALIDATE_COUPON_CODE,
	payload
});

export const setNavigationToTrackRide = payload => ({
	type: OLA_SET_NAVIGATION_TO_TRACK_RIDE,
	payload
});
/**
 * Saga
 */

export function* olaChooseRideSaga(dispatch) {
	yield takeLatest(OLA_CURRENT_RIDE, handleOlaCurrentRide);
	yield takeLatest(OLA_MAP_POLYLINE, handleOlaMapPolyline);
	yield takeLatest(OLA_REQUEST_RIDE, handleOlaRequestRide);
	yield takeLatest(OLA_ABORT_BOOKING, handleOlaAbortBooking);
	yield takeLatest(OLA_GET_RIDE_ESTIMATE, handleOlaGetRideEstimate);
	yield takeLatest(OLA_VALIDATE_COUPON_CODE, handleOlaValidateCouponCode);
	yield takeLatest(OLA_UPDATE_DROP_LOCATION, handleOlaUpdateDropLocation);
}

/**
 * Handlers
 */

function* handleOlaValidateCouponCode(action) {
	try {
		let couponCode = yield call(OlaApi.validateCoupon, action.payload);
		console.log("Ola Coupon Code Saga Data: ", couponCode);
		yield put(olaSaveCouponCode(couponCode));
	} catch (error) {
		console.log("Coupon Code Saga Error: ", error);
	}
}

function* handleOlaMapPolyline(action) {
	try {
		let mapPolyline = yield call(OlaApi.drawMapPolyline, action.payload);
		console.log("Draw Polyline API Saga Data: ", mapPolyline);
		yield put(olaMapPath(mapPolyline.path));
	} catch (error) {
		console.log("Draw Polyline API SAGA Error: ", error);
	}
}

function* handleOlaRequestRide(action) {
	try {
		let requestRide = yield call(OlaApi.requests, action.payload);
		console.log("Request Ride Saga Data: ", requestRide);
		yield put(olaBookingStatus(requestRide));
	} catch (error) {
		console.log("Request Ride Saga Error: ", error);
	}
}

function* handleOlaAbortBooking(action) {
	try {
		let abortBooking = yield call(OlaApi.abortBooking, action.payload);
		console.log("Ola Abort Booking Saga: ", abortBooking);
		abortBooking.status === "SUCCESS"
			? yield put(
					olaBookingStatus({
						status: "CANCELLED_SUCCESSFULLY",
						message: abortBooking.message
					})
			  )
			: console.log("FAILURE");
	} catch (error) {
		console.log("Ola Abort Booking Error: ", error);
	}
}

function* handleOlaGetRideEstimate(action) {
	try {
		let estimate = yield call(OlaApi.getRideEstimate, action.payload);
		console.log("Ola Get Estimate Saga: ", estimate);
		yield put(olaSaveRideEstimate(estimate));
	} catch (error) {
		console.log("Ola Get Estimate Error: ", error);
	}
}

function* handleOlaCurrentRide(action) {
	try {
		let current = yield call(OlaApi.getCurrentRide, action.payload);
		if (current.code === "UNKNOWN" && current.message === "TRACK_RIDE_FAILED") {
			console.log("Share Ride Error");
		} else {
			if (
				current.booking_status === "COMPLETED" ||
				current.booking_status === "BOOKING_CANCELLED" ||
				current.booking_status === "BOOKING_CANCEL"
			) {
				AsyncStorage.setItem("RIDE_IN_PROGRESS", JSON.stringify(false));
			}
			yield put(olaBookingStatus(current));
		}
	} catch (error) {
		console.log("Ola Current Ride Error: ", error);
		yield put(olaBookingStatusError());
	}
}

function* handleOlaUpdateDropLocation(action) {
	try {
		let current = yield call(OlaApi.updateDropLocation, action.payload);
		// console.log("Ola Current Ride Saga: ", current);
		yield put(updateDropLocationStatus(current));
	} catch (error) {
		console.log("Ola Current Ride Error: ", error);
	}
}
