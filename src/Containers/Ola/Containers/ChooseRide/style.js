import { StyleSheet, Dimensions } from "react-native";
import { font_one, font_three } from "../../../../Assets/styles";

const { height, width } = Dimensions.get("window");

/**
 * styles for index.js
 */
export const indexStyle = StyleSheet.create({
	header: {
		width: width,
		height: height / 10,
		flexDirection: "row",
		alignItems: "center"
	},
	fullFlex: {
		flex: 1
	},
	rowFlex: {
		flexDirection: "row"
	},
	panelContainer: {
		borderWidth: 0,
		zIndex: 15,
		position: "absolute",
		bottom: 0,
		left: 0,
		right: 0
	},
	headerBar: {
		elevation: 3,
		shadowOffset: { width: 1.2, height: 1.2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		backgroundColor: "#ffffff",
		justifyContent: "flex-start",
		padding: width / 25,
		width: width / 1
	},
	searchBarContainer: {
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		backgroundColor: "#ffffff",
		alignItems: "center",
		justifyContent: "center",
		flexDirection: "row",
		width: width / 1.1,
		height: height / 10
	},
	originLocation: {
		margin: 10,
		marginLeft: 15,
		marginRight: 15,
		width: 7,
		height: 7,
		borderRadius: 3.5,
		backgroundColor: "#109C16"
	},
	destinationLocation: {
		margin: 10,
		marginLeft: 15,
		marginRight: 15,
		width: 7,
		height: 7,
		borderRadius: 3.5,
		backgroundColor: "#ff0000"
	},
	searchBarTextInput: {
		justifyContent: "center",
		alignItems: "center",
		marginRight: 5,
		borderWidth: 0,
		width: width / 1.15,
		fontSize: width / 30,
		color: "#3f3f3f"
	},
	confirmScreenButton: {
		height: height / 20,
		top: height / 50,
		left: height / 50
	},
	crossButtonStyle: {
		position: "absolute"
	},
	olaTickStyle: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		paddingHorizontal: width / 30
	},
	olaTickImage: { width: width / 8, height: height / 10 },
	rideScheduleText: { padding: width / 50, fontSize: 20, color: "#000000" },
	rideScheduleDriverText: {
		padding: width / 50,
		paddingBottom: 50,
		textAlign: "center"
	},
	rideScheduleSeparator: { color: "#d8d8d8" },
	gotItButton: { alignItems: "center" },
	gotItText: {
		color: "#00bfff",
		paddingTop: 30
	},
	rentalMapStyleWithoutRequest: {
		top: height / 6.5,
		bottom: height / 4
	},
	rentalMapStyleWithRequest: {
		top: height / 6.5,
		bottom: 0
	},
	mapStyleWithoutRequest: {
		top: height / 6.2,
		bottom: height / 4.9
	},
	mapStyleWithRequest: {
		top: 0,
		bottom: 0
	},
	cabNameStyle: {
		color: "#202020",
		fontSize: height / 45.71,
		marginLeft: width / 20
	},
	disabledTextBoxStyle: {
		position: "relative",
		flexDirection: "row",
		alignItems: "center",
		alignSelf: "center",
		backgroundColor: "#ffffff",
		borderColor: "#ffffff",
		borderRadius: width / 100,
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2
	},
	textButtonStyle: {
		borderWidth: 0,
		borderColor: "#000000",
		borderRadius: width / 100,
		backgroundColor: "#ffffff"
	},
	textStyle: {
		fontSize: width / 30,
		justifyContent: "flex-start",
		alignItems: "center",
		color: "#3f3f3f"
	},
	editIcon: { right: width / 20 },
	locationTextStyle: {
		alignItems: "center",
		paddingVertical: width / 30,
		alignSelf: "center",
		elevation: 0
	},
	rentalDisabledTextBoxStyle: {
		top: height / 8.3,
		flexDirection: "row",
		alignItems: "center",
		alignSelf: "center",
		backgroundColor: "#ffffff",
		borderColor: "#ffffff",
		borderRadius: width / 100,
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		borderTopWidth: 0.3,
		borderTopColor: "grey"
	},
	rentalTextButtonStyle: {
		borderWidth: 0,
		borderColor: "#000000",
		borderRadius: width / 100,
		backgroundColor: "#ffffff",
		paddingLeft: 0
	},
	rentalTextStyle: {
		fontSize: width / 30,
		justifyContent: "flex-start",
		alignItems: "center",
		color: "#3f3f3f"
	},
	locationStyleShape: {
		width: width / 10.5,
		height: width / 10.5,
		borderRadius: width / 21
	},
	locationButtonWithRideLater: {
		elevation: 3,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		bottom: height / 3.5
	},
	locationButtonWithoutRideLater: {
		elevation: 3,
		shadowOffset: { width: 1.2, height: 1.2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		bottom: height / 4.4
	},
	dialogModalBackgroundStyle: {
		backgroundColor: "#000",
		opacity: 0.3
	},
	dialogModalBackgroundView: {
		height: height / 4,
		top: height / 3,
		width: width / 1.3,
		borderColor: "#ffffff",
		borderRadius: width / 140,
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		left: width / 8,
		position: "absolute",
		justifyContent: "flex-start",
		alignItems: "center",
		backgroundColor: "#ffffff"
	},
	olaOutstationText: {
		padding: height / 60
	},
	outsideDropText: {
		padding: width / 18
	},
	changeDropView: {
		flexDirection: "row",
		alignItems: "flex-end"
	},
	changeDropButton: {
		borderLeftWidth: 0.5,
		borderColor: "#4f4f4f",
		justifyContent: "center",
		borderRadius: 0,
		alignItems: "center",
		width: width / 2.5,
		height: height / 15,
		backgroundColor: "#0f0f0f"
	},
	changeDropText: {
		color: "#CCE500",
		fontSize: width / 30
	},
	continueTextView: {
		borderLeftWidth: 0.5,
		borderColor: "#4f4f4f",
		justifyContent: "center",
		borderRadius: 0,
		alignItems: "center",
		width: width / 2.5,
		height: height / 15,
		backgroundColor: "#0f0f0f",
		marginTop: height / 120
	},
	continueText: {
		color: "#CCE500",
		fontSize: width / 30
	},
	spinnerView: {
		backgroundColor: "transparent",
		flex: 1,
		bottom: height / 2 - width / 4,
		left: width / 2 - width / 4,
		position: "absolute",
		height: width / 2,
		width: width / 2
	},
	requestRideAniatedView: {
		position: "absolute",
		borderWidth: 0,
		alignSelf: "center",
		width: width / 1.05,
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		backgroundColor: "#ffffff"
	},
	requestRideInsideView: {
		width: width,
		paddingVertical: height / 50,
		justifyContent: "center",
		alignItems: "center"
	},
	requestRideInsideText: {
		color: "#000000",
		fontSize: width / 25
	},
	requestRideSeparatorView: {
		backgroundColor: "#dfdfdf",
		width: width,
		height: height / 12
	},
	requestRideAbortBookingButton: {
		width: width,
		paddingVertical: height / 50,
		justifyContent: "center",
		alignItems: "center"
	},
	cancelTextActive: {
		color: "#ff0000",
		fontSize: width / 25
	},
	cancelTextUnActive: {
		color: "#000000",
		fontSize: width / 25
	},
	rideScheduleCrossButton: {
		height: height / 20,
		top: height / 50,
		left: height / 50
	},
	rideScheduleTickView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		paddingHorizontal: width / 30
	},
	rideScheduleTickImage: { width: width / 8, height: height / 10 },
	rideScheduleTickText: { padding: width / 50, fontSize: 20, color: "#000000" },
	rideScheduleCompleteText: {
		padding: width / 50,
		paddingBottom: 50,
		textAlign: "center"
	},
	centerAlign: { alignItems: "center" }
});

/**
 * styles for confirmRide Component
 */

export const confirmRideStyle = StyleSheet.create({
	container: {
		position: "absolute",
		width: width,
		alignItems: "center",
		justifyContent: "space-between",
		backgroundColor: "#ffffff",
		borderColor: "#ffffff",
		bottom: 0
	},
	headerBar: {
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		backgroundColor: "#ffffff",
		justifyContent: "flex-start",
		alignItems: "center",
		padding: 10,
		flexDirection: "row",
		width: width / 1,
		height: height / 12
	},
	recentPlacesContainer: {
		flexDirection: "row",
		width: width,
		height: height / 5,
		backgroundColor: "transparent",
		alignItems: "center",
		justifyContent: "space-around"
	},
	recentPlaceIcon: {
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		width: width / 6,
		height: width / 6,
		borderRadius: width / 16,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#ffffff"
	},
	searchBarContainer: {
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		backgroundColor: "#ffffff",
		alignItems: "center",
		flexDirection: "row",
		width: width / 1.1,
		height: height / 10,
		borderWidth: 0,
		borderColor: "#000000"
	},
	originLocation: {
		margin: 10,
		marginLeft: 15,
		marginRight: 15,
		width: 7,
		height: 7,
		backgroundColor: "#000000"
	},
	destinationLocation: {
		margin: 10,
		marginLeft: 15,
		marginRight: 15,
		width: 7,
		height: 7,
		borderRadius: 3.5,
		backgroundColor: "#000000"
	},
	fareCompare: {
		flexDirection: "row",
		width: width / 1.05,
		backgroundColor: "#F5F7D1",
		justifyContent: "center",
		alignItems: "center",
		marginVertical: width / 30,
		paddingVertical: width / 70
	},
	productView: {
		height: height / 12.8,
		justifyContent: "center",
		flexDirection: "row",
		alignItems: "center"
	},
	productInsideView: {
		justifyContent: "center",
		alignItems: "center"
	},
	productRowWiseView: {
		flexDirection: "row",
		alignItems: "center"
	},
	rupeeText: {
		fontSize: width / 25,
		color: "#000000"
	},
	cabTypeText: {
		marginLeft: width / 300,
		fontSize: width / 25,
		color: "#000000"
	},
	totalFareText: {
		color: "#808080",
		fontSize: width / 35
	},
	getFareView: {
		height: height / 12.8,
		justifyContent: "center",
		alignItems: "center"
	},
	getFareText: {
		fontSize: width / 20,
		color: "#0076ff"
	},
	retryGetFareText: { fontSize: width / 35 },
	gettingFareText: {
		height: height / 12.8,
		justifyContent: "center",
		alignItems: "center"
	},
	rideLaterView: {
		flexDirection: "row",
		width: width,
		justifyContent: "space-between",
		backgroundColor: "#fff",
		height: height / 19.2,
		alignItems: "center",
		borderBottomWidth: 0.3,
		borderBottomColor: "#bdbdbd"
	},
	rideLaterPickupImageView: {
		flexDirection: "row",
		marginLeft: width / 35
	},
	rideLaterPickupImage: {
		width: width / 24,
		height: width / 24,
		marginRight: width / 35,
		color: "#000"
	},
	rideLaterTimePickerButton: {
		flexDirection: "row",
		marginRight: width / 30,
		alignItems: "center"
	},
	rideLaterPickupText: {
		fontSize: height / 60.25,
		marginRight: width / 60,
		color: "#000"
	},
	applyCouponView: {
		height: height / 19.2,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "flex-start"
	},
	applyCouponButton: {
		width: width / 2,
		flexDirection: "row",
		alignItems: "center"
	},
	applyCouponImage: {
		marginLeft: width / 35,
		width: width / 24,
		height: width / 24,
		marginRight: width / 35
	},
	applyCouponText: {
		color: "#777777",
		fontSize: height / 50.5
	},
	seatCountView: {
		width: width / 2,
		flexDirection: "row",
		alignItems: "center"
	},
	seatCountImage: {
		width: width / 24,
		height: width / 24,
		marginHorizontal: width / 35
	},
	seatCountText: {
		color: "#777777",
		fontSize: height / 50.5
	},
	confirmEmptyView: {
		flex: 2.25,
		width: width / 2,
		flexDirection: "row",
		alignItems: "center"
	},
	confirmBookingButton: {
		flexDirection: "row",
		width: width,
		justifyContent: "center",
		alignItems: "center",
		height: height / 14.27,
		backgroundColor: "#0f0f0f"
	},
	confirmBookingText: {
		color: "#d4db28",
		fontSize: width / 25
	}
});

/**
 * styles for ApplyCoupon.js
 */
export const applyCouponStyle = StyleSheet.create({
	fullFlex: { flex: 1, alignItems: "center" },
	header: {
		width: width,
		height: height / 10,
		flexDirection: "row",
		alignItems: "center"
	},
	panelContainer: {
		borderWidth: 0,
		zIndex: 15,
		position: "absolute",
		bottom: 0,
		left: 0,
		right: 0
	},
	headerBar: {
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		backgroundColor: "#ffffff",
		justifyContent: "space-between",
		alignItems: "center",
		padding: 10,
		flexDirection: "row",
		width: width / 1,
		height: height / 12
	},
	searchBarContainer: {
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		backgroundColor: "#ffffff",
		alignItems: "center",
		justifyContent: "center",
		flexDirection: "row",
		width: width / 1.1,
		height: height / 10
	},
	originLocation: {
		margin: 10,
		marginLeft: 15,
		marginRight: 15,
		width: 7,
		height: 7,
		borderRadius: 3.5,
		backgroundColor: "#109C16"
	},
	destinationLocation: {
		margin: 10,
		marginLeft: 15,
		marginRight: 15,
		width: 7,
		height: 7,
		borderRadius: 3.5,
		backgroundColor: "#ff0000"
	},
	searchBarTextInput: {
		justifyContent: "center",
		alignItems: "center",
		marginRight: 5,
		borderWidth: 0,
		width: width / 1.15,
		fontSize: width / 30,
		color: "#3f3f3f"
	},
	container: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-around"
	},
	applyCouponButton: {
		flexDirection: "row",
		alignItems: "center",
		height: height / 12,
		width: width / 3,
		justifyContent: "space-between"
	},
	applyCouponText: {
		color: "#000000",
		fontFamily: font_three,
		fontSize: height / 45
	},
	applyTextWithValue: {
		color: "#1f73ff",
		fontSize: height / 54,
		opacity: 1
	},
	applyTextWithoutValue: {
		color: "#1f73ff",
		fontSize: height / 54,
		opacity: 0.2
	},
	enterCouponText: {
		width: width,
		borderWidth: 0.2,
		backgroundColor: "#ffffff",
		elevation: 1,
		shadowOffset: { width: 0.5, height: 0.5 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		marginTop: height / 40,
		fontFamily: font_one
	}
});
