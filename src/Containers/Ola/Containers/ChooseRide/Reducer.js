import {
	OLA_MAP_PATH,
	OLA_BOOKING_STATUS,
	OLA_BOOKING_STATUS_ERROR,
	OLA_SAVE_COUPON_CODE,
	OLA_SAVE_RIDE_ESTIMATE,
	OLA_GET_RIDE_ESTIMATE,
	OLA_UPDATE_DROP_LOCATION_STATUS,
	OLA_SET_NAVIGATION_TO_TRACK_RIDE
} from "./Saga";

const initialState = {
	bookingStatus: null,
	isBookingStatusError: false,
	mapPath: null,
	estimate: null,
	couponCode: null,
	updateStatus: null,
	isNavigationToTrackRide: false
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case OLA_GET_RIDE_ESTIMATE:
			return {
				...state,
				estimate: null
			};
		case OLA_SAVE_RIDE_ESTIMATE:
			return {
				...state,
				estimate: action.payload
			};
		case OLA_MAP_PATH:
			return {
				...state,
				mapPath: action.payload
			};
		case OLA_BOOKING_STATUS:
			return {
				...state,
				bookingStatus: action.payload,
				isBookingStatusError: false
			};
		case OLA_BOOKING_STATUS_ERROR:
			return {
				...state,
				isBookingStatusError: true
			};
		case OLA_SAVE_COUPON_CODE:
			return {
				...state,
				couponCode: action.payload
			};
		case OLA_UPDATE_DROP_LOCATION_STATUS:
			return {
				...state,
				updateStatus: action.payload
			};
		case OLA_SET_NAVIGATION_TO_TRACK_RIDE:
			return {
				...state,
				isNavigationToTrackRide: action.payload
			};
		default:
			return state;
	}
};
