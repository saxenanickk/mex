import React, { Component } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import Interactable from "react-native-interactable";
import { ProgressBar, Seperator } from "../../../../Components";
import I18n from "../../Assets/Strings/i18n";
import {
	OlaTextRegular,
	OlaTextBold,
	OlaTextMedium
} from "../../Components/OlaText";
import { indexStyle as styles } from "./style";
const { width, height } = Dimensions.get("window");

export default class CancelRide extends Component {
	constructor(props) {
		super(props);
		this.state = {
			progress: 5
		};
	}

	componentWillMount() {
		let self = this;
		this.progressInterval = setInterval(function() {
			if (self.state.progress < 95) {
				self.setState({
					progress: self.state.progress + 1.5
				});
			}
		}, 1000);
	}

	componentWillUnmount() {
		clearInterval(this.progressInterval);
	}
	render() {
		return (
			<View>
				<View style={styles.requestRideInsideView}>
					<OlaTextRegular style={styles.requestRideInsideText}>
						{this.props.cancelling
							? I18n.t("cancelling_request")
							: I18n.t("searching_ride")}
					</OlaTextRegular>
				</View>
				<ProgressBar
					value={this.state.progress}
					backgroundColor={"#138A3C"}
					height={height / 150}
					width={width / 1.05}
				/>
				<View style={styles.requestRideSeparatorView} />
				<Seperator color={"#777777"} width={width} height={0.2} />
				<TouchableOpacity
					style={styles.requestRideAbortBookingButton}
					onPress={() => {
						clearInterval(this.progressInterval);
						this.props.abortBooking();
					}}>
					<OlaTextRegular
						style={
							this.props.activateCancel
								? styles.cancelTextActive
								: styles.cancelTextUnActive
						}>
						{I18n.t("cancel_request")}
					</OlaTextRegular>
				</TouchableOpacity>
			</View>
		);
	}
}
