import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	Keyboard,
	BackHandler
} from "react-native";
import { Icon, TextBox } from "../../../../Components";
import { OlaTextMedium } from "../../Components/OlaText";
import { applyCouponStyle as styles } from "./style";
import I18n from "../../Assets/Strings/i18n";

const { width } = Dimensions.get("window");

export default class ConfirmRide extends Component {
	constructor(props) {
		super(props);
		this.state = {
			couponValue: "",
			isApplyButtonEnabled: true
		};
		this.handleBackPress = this.handleBackPress.bind(this);
	}

	handleBackPress() {
		this.props.navigation.goBack();
		return true;
	}

	componentWillMount() {
		BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
	}

	render() {
		return (
			<View style={styles.fullFlex}>
				<View style={[styles.headerBar]}>
					<View style={styles.container}>
						<TouchableOpacity
							style={styles.applyCouponButton}
							onPress={() => {
								Keyboard.dismiss();
								this.props.navigation.goBack();
							}}>
							<Icon
								iconType={"material"}
								iconSize={width / 18}
								iconName={"arrow-back"}
								iconColor={"#000000"}
							/>
							<OlaTextMedium style={styles.applyCouponText}>
								{I18n.t("apply_coupon")}
							</OlaTextMedium>
						</TouchableOpacity>
					</View>
					<TouchableOpacity
						onPress={() => {
							Keyboard.dismiss();
							setTimeout(() => {
								// Todo handle this scenario and other route.params properly
								const { route } = this.props;
								const { validateCoupon } = route.params
									? route.params
									: { validateCoupon: () => {} };
								validateCoupon(this.state.couponValue);
								this.props.navigation.goBack();
							}, 50);
						}}
						disabled={
							!this.state.isApplyButtonEnabled ||
							this.state.couponValue.trim() === ""
						}>
						<OlaTextMedium
							style={
								this.state.couponValue.trim() !== ""
									? styles.applyTextWithValue
									: styles.applyTextWithoutValue
							}>
							{I18n.t("apply")}
						</OlaTextMedium>
					</TouchableOpacity>
				</View>
				<TextBox
					textboxStyle={styles.enterCouponText}
					onChangeText={text => this.setState({ couponValue: text })}
					autoFocus={false}
					value={this.state.couponValue}
					placeholder={I18n.t("enter_coupon_code")}
				/>
			</View>
		);
	}
}
