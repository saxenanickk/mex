import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	Image,
	Modal,
	TouchableWithoutFeedback,
	Text,
	Platform,
	UIManager,
	LayoutAnimation,
	BackHandler
} from "react-native";
import { connect } from "react-redux";
import Interactable from "react-native-interactable";

import {
	MapView,
	Icon,
	Seperator,
	DialogModal,
	CabMarker
} from "../../../../Components";
import {
	olaGetRideEstimate,
	olaMapPolyline,
	olaSaveRideEstimate,
	olaRequestRide,
	olaCurrentRide,
	olaBookingStatus,
	olaVaidateCouponCode,
	olaAbortBooking,
	olaSaveCouponCode,
	olaMapPath,
	setNavigationToTrackRide
} from "./Saga";
import {
	olaDestination,
	olaFetchOrigin,
	olaFetchDestination
} from "../Home/BookRide/Saga";
import ConfirmRide from "./ConfirmRide";
import I18n from "../../Assets/Strings/i18n";
import {
	LocationText,
	LocationButton,
	DisabledTextBox
} from "../../Components";
import { Marker } from "react-native-maps";
import { OLATICK } from "../../Assets/Img/Image";
import LocationModule from "../../../../CustomModules/LocationModule";
import CircleLoader from "../../../../Components/CircleLoader";
import {
	OlaTextRegular,
	OlaTextBold,
	OlaTextMedium
} from "../../Components/OlaText";
import { indexStyle as styles } from "./style";
import Auth from "../Auth";
import { saveOlaDeeplinkState } from "../../Saga";

import goAppAnalytics from "../../../../utils/GoAppAnalytics";
import CancelRide from "./CancelRide";
import { GoToast } from "../../../../Components";
import { CommonActions } from "@react-navigation/native";
const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class ChooseRide extends Component {
	constructor(props) {
		super(props);

		const { route } = props;
		const {
			pickup_lat,
			pickup_lng,
			id,
			category,
			pickup_mode,
			pickup_time,
			sub_category,
			package_id,
			type,
			trip_id,
			drop_time,
			rideLater
		} = route.params ? route.params : {};

		this.id = id;
		this.category = category;
		this.pickup_mode = pickup_mode;
		this.sub_category = sub_category;
		this.package_id = package_id;
		this.type = type;
		this.trip_id = trip_id;
		this.drop_time = drop_time;
		this.pickup_time = pickup_time;
		this.rideLater = rideLater;

		this.state = {
			locationModule: false,
			showSpinner: false,
			requestRideCheck: false,
			activateCancel: false,
			cancelling: false,
			isRideLaterConfirm: false,
			showDifferentCityWarningModal: false,
			selectedTextBox: "origin",
			region: {
				latitude: props.origin.latitude
					? props.origin.latitude
					: parseFloat(pickup_lat),
				longitude: props.origin.longitude
					? props.origin.longitude
					: parseFloat(pickup_lng),
				latitudeDelta: LATITUDE_DELTA,
				longitudeDelta: LONGITUDE_DELTA
			}
		};
		this.cabCategory = null;
		this.pickup_mode = null;
		if (route.params) {
			this.cabCategory = id || category;
			this.pickup_mode = pickup_mode;
			this.pickup_time = pickup_mode && pickup_mode === "LATER" && pickup_time;
			if (Platform.OS === "android") {
				UIManager.setLayoutAnimationEnabledExperimental(true);
			}
			this.isTracking = false;
			this.requestStatus = "FIRST_CHECK";
			this.currentRideInterval = null;
			this.openPlaceSearch = this.openPlaceSearch.bind(this);
			this.requestRide = this.requestRide.bind(this);
			this.abortBooking = this.abortBooking.bind(this);
			this.validateCoupon = this.validateCoupon.bind(this);
			this.backButtonPress = this.backButtonPress.bind(this);
			this.renderOriginMarker = this.renderOriginMarker.bind(this);
			this.closeConfirmBooking = this.closeConfirmBooking.bind(this);
			this.showAlert = false; // For ride later show success message
			this.renderDestinationMarker = this.renderDestinationMarker.bind(this);
			this.getRideLaterEstimate = this.getRideLaterEstimate.bind(this);
			this.getOlaTime = this.getOlaTime.bind(this);
		}
	}

	openPlaceSearch(searchType) {
		this.props.navigation.navigate("PlaceSearch", {
			searchType: searchType,
			selectedLocation: locationObject => {
				this.mapRef.animateRegionChange({
					latitude: locationObject.latitude,
					longitude: locationObject.longitude,
					latitudeDelta: LATITUDE_DELTA,
					longitudeDelta: LONGITUDE_DELTA
				});
				this.props.dispatch(
					olaGetRideEstimate({
						access_token: this.props.userToken,
						pickup_lat: this.props.origin.latitude,
						pickup_lng: this.props.origin.longitude,
						drop_lat: this.props.destination.latitude,
						drop_lng: this.props.destination.longitude,
						category: this.id,
						appToken: this.props.appToken
					}),
					goAppAnalytics.trackWithProperties("ola-get-ride-estimate", {
						pickup_lat: this.props.origin.latitude,
						pickup_lng: this.props.origin.longitude,
						drop_lat: this.props.destination.latitude,
						drop_lng: this.props.destination.longitude,
						category: this.id
					})
				);
				this.props.dispatch(
					olaMapPolyline({
						origin: this.props.origin,
						destination: this.props.destination
					})
				);
			}
		});
	}

	navigateToHome = () => {
		if (this.category === "outstation") {
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "Home" }]
				})
			);
		} else {
			this.props.navigation.goBack();
		}
	};

	componentWillMount() {
		this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
			if (!this.state.showSpinner) {
				if (this.state.isRideLaterConfirm) {
					this.navigateToHome();
				}
				this.backButtonPress();
			}
			return true;
		});
	}

	async closeConfirmBooking() {
		if (this.state.showSpinner) {
			return true;
		}
		return false;
	}

	shouldComponentUpdate(props, state) {
		if (
			props.couponCode !== null &&
			props.couponCode !== this.props.couponCode &&
			props.couponCode.code === "INVALID_COUPON"
		) {
			this.setState({ showSpinner: false });
			this.props.dispatch(olaSaveCouponCode(null));
			GoToast.show(props.couponCode.message, I18n.t("information"), "SHORT");
		}
		if (props.estimate !== null && props.estimate !== this.props.estimate) {
			this.setState({ showSpinner: false });
			if (
				props.estimate.code === "INVALID_CITY_CAR_CATEGORY" ||
				props.estimate.code === "INVALID_CITY"
			) {
				GoToast.show(props.estimate.message, I18n.t("information"), "SHORT");
				this.props.navigation.goBack();
				return false;
			}
			if (props.estimate.code === "DIFFERENT_DROP_CITY") {
				this.setState({ showDifferentCityWarningModal: true });
			}
		}
		if (
			props.bookingStatus !== null &&
			this.props.bookingStatus !== props.bookingStatus
		) {
			if (
				props.bookingStatus.status === "SUCCESS" &&
				typeof props.bookingStatus.booking_status === "undefined"
			) {
				if (this.requestStatus === "FIRST_CHECK") {
					this.requestStatus = "CLEANED";
					this.setState({ activateCancel: true });
					this.currentRideInterval = setInterval(() => {
						this.props.dispatch(
							olaCurrentRide({
								access_token: this.props.userToken,
								request_id: props.bookingStatus.booking_id,
								appToken: this.props.appToken
							})
						);
					}, 2000);
				}
			}
			// Flow for Ride Later is different
			else if (this.pickup_mode === "LATER") {
				let message = props.bookingStatus.message;
				if (message === "BOOKING_FAILED") {
					GoToast.show(
						props.bookingStatus.code,
						I18n.t("information"),
						"SHORT"
					);
					this.props.navigation.goBack();
				} else if (props.bookingStatus.status === "SUCCESS") {
					this.setState({
						isRideLaterConfirm: true
					});
				} else if (props.bookingStatus.status === "FAILURE") {
					GoToast.show(
						props.bookingStatus.message,
						I18n.t("information"),
						"SHORT"
					);
					this.props.navigation.goBack();
				}
			} else if (
				props.bookingStatus.status === "SUCCESS" &&
				props.bookingStatus.booking_status === "ALLOTMENT_PENDING"
			) {
				console.log("BOOKING PENDING");
			} else if (
				props.bookingStatus.status === "SUCCESS" &&
				props.bookingStatus.booking_status === "ALLOTMENT_FAILED"
			) {
				GoToast.show(I18n.t("cant_serve"), I18n.t("information"), "SHORT");
				this.props.navigation.goBack();
				return false;
			} else if (
				props.bookingStatus.status === "SUCCESS" &&
				props.bookingStatus.booking_status === "CALL_DRIVER"
			) {
				this.isTracking = true;
				this.props.dispatch(
					olaDestination({
						latitude: this.props.destination.latitude,
						longitude: this.props.destination.longitude,
						address: this.props.destination.address,
						appToken: this.props.appToken
					})
				);
				this.props.dispatch(setNavigationToTrackRide(true));
				this.props.navigation.dispatch(
					CommonActions.reset({
						index: 1,
						routes: [{ name: "Home" }, { name: "TrackRide" }]
					})
				);
			} else if (
				props.bookingStatus.status === "FAILURE" &&
				props.bookingStatus.code === "NO_CABS_AVAILABLE"
			) {
				GoToast.show(
					props.bookingStatus.message,
					I18n.t("information"),
					"SHORT"
				);
				this.props.navigation.goBack();
				return false;
			} else if (props.bookingStatus.status === "FAILURE") {
				GoToast.show(
					props.bookingStatus.message,
					I18n.t("information"),
					"SHORT"
				);
				this.props.navigation.goBack();
				return false;
			} else if (props.bookingStatus.status === "CANCELLED_SUCCESSFULLY") {
				clearInterval(this.currentRideInterval);
				GoToast.show(
					props.bookingStatus.message,
					I18n.t("information"),
					"SHORT"
				);
				this.props.navigation.dispatch(
					CommonActions.reset({
						index: 0,
						routes: [{ name: "Home" }]
					})
				);
				return false;
			} else if (
				props.booking_status &&
				(props.booking_status.code === "invalid_access_token" ||
					props.booking_status.code === "INVALID_AUTH_TOKEN")
			) {
				GoToast.show(I18n.t("try_again"), I18n.t("information"), "SHORT");
				this.props.navigation.goBack();
				return false;
			} else {
				GoToast.show(I18n.t("try_again"), I18n.t("information"), "SHORT");
				this.props.navigation.goBack();
				return false;
			}
		}

		if (
			props.origin.latitude &&
			props.destination.latitude &&
			(!this.props.origin.latitude || !this.props.destination.latitude) &&
			!props.mapPath
		) {
			this.props.dispatch(
				olaMapPolyline({
					origin: props.origin,
					destination: props.destination
				})
			);
			return false;
		}

		if (
			props.mapPath !== null &&
			props.mapPath !== this.props.mapPath &&
			this.mapRef !== null
		) {
			this.getFitToCoordinate(props.mapPath);
		}
		LayoutAnimation.easeInEaseOut();
		return true;
	}

	getOlaTime(olaTimeFormat) {
		let dateTime = olaTimeFormat.split(" ");
		let date = dateTime[0].split("/");
		date[1] = date[1] <= 9 ? "0" + date[1] : date[1];
		let dateString =
			date[1] + "/" + date[0] + "/" + date[2] + " " + dateTime[1];
		return dateString;
	}

	componentDidMount() {
		if (this.cabCategory === "rental" || this.cabCategory === "outstation") {
			this.setState({
				requestRideCheck: true
			});
			let dispatchOptions = {
				access_token: this.props.userToken,
				pickup_lat: this.props.origin.latitude,
				pickup_lng: this.props.origin.longitude,
				category: this.cabCategory,
				pickup_mode: this.pickup_mode,
				sub_category: this.sub_category,
				appToken: this.props.appToken
			};
			if (this.cabCategory === "rental") {
				dispatchOptions = {
					...dispatchOptions,
					package_id: this.package_id
				};
			}
			if (this.cabCategory === "outstation") {
				dispatchOptions = {
					...dispatchOptions,
					type: this.type,
					drop_lat: this.props.destination.latitude,
					drop_lng: this.props.destination.longitude,
					trip_id: this.trip_id
				};
				if (this.type === "two_way") {
					dispatchOptions = {
						...dispatchOptions,
						drop_time: this.drop_time
					};
				}
			}
			if (this.pickup_mode === "LATER") {
				dispatchOptions = {
					...dispatchOptions,
					pickup_time: this.pickup_time
				};
			}
			this.props.dispatch(olaRequestRide(dispatchOptions));
		} else {
			if (this.pickup_mode === undefined) {
				this.props.dispatch(
					olaGetRideEstimate({
						access_token: this.props.userToken,
						pickup_lat: this.props.origin.latitude,
						pickup_lng: this.props.origin.longitude,
						drop_lat: this.props.destination.latitude,
						drop_lng: this.props.destination.longitude,
						category: this.cabCategory,
						appToken: this.props.appToken
					})
				);
			} else {
				let time = this.getOlaTime(this.pickup_time);
				this.props.dispatch(
					olaGetRideEstimate({
						access_token: this.props.userToken,
						pickup_lat: this.props.origin.latitude,
						pickup_lng: this.props.origin.longitude,
						drop_lat: this.props.destination.latitude,
						drop_lng: this.props.destination.longitude,
						category: this.cabCategory,
						pickup_mode: time,
						appToken: this.props.appToken
					})
				);
			}
			goAppAnalytics.trackWithProperties("ola-get-ride-estimate", {
				pickup_lat: this.props.origin.latitude,
				pickup_lng: this.props.origin.longitude,
				drop_lat: this.props.destination.latitude,
				drop_lng: this.props.destination.longitude,
				category: this.id
			});
			this.props.dispatch(
				olaMapPolyline({
					origin: this.props.origin,
					destination: this.props.destination
				})
			);
		}
	}

	getRideEstimate = () => {
		if (this.pickup_mode === undefined) {
			this.props.dispatch(
				olaGetRideEstimate({
					access_token: this.props.userToken,
					pickup_lat: this.props.origin.latitude,
					pickup_lng: this.props.origin.longitude,
					drop_lat: this.props.destination.latitude,
					drop_lng: this.props.destination.longitude,
					category: this.cabCategory,
					appToken: this.props.appToken
				})
			);
		} else {
			let time = this.getOlaTime(this.pickup_time);
			this.props.dispatch(
				olaGetRideEstimate({
					access_token: this.props.userToken,
					pickup_lat: this.props.origin.latitude,
					pickup_lng: this.props.origin.longitude,
					drop_lat: this.props.destination.latitude,
					drop_lng: this.props.destination.longitude,
					category: this.cabCategory,
					pickup_mode: time,
					appToken: this.props.appToken
				})
			);
		}
		goAppAnalytics.trackWithProperties("ola-get-ride-estimate", {
			pickup_lat: this.props.origin.latitude,
			pickup_lng: this.props.origin.longitude,
			drop_lat: this.props.destination.latitude,
			drop_lng: this.props.destination.longitude,
			category: this.id
		});
	};

	componentWillUnmount() {
		clearInterval(this.currentRideInterval);
		if (!this.isTracking) {
			this.props.dispatch(olaSaveRideEstimate(null));
			this.props.dispatch(olaBookingStatus(null));
		}
		this.props.dispatch(olaMapPath(null));
		if (this.props.deeplink) {
			this.props.dispatch(saveOlaDeeplinkState(null));
		}
		this.backHandler.remove();
	}

	async backButtonPress() {
		if (!this.props.navigation.goBack()) {
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "Home" }]
				})
			);
		}
	}

	getRideLaterEstimate(olaFormatTime) {
		let time = this.getOlaTime(olaFormatTime);
		this.props.dispatch(
			olaGetRideEstimate({
				access_token: this.props.userToken,
				pickup_lat: this.props.origin.latitude,
				pickup_lng: this.props.origin.longitude,
				drop_lat: this.props.destination.latitude,
				drop_lng: this.props.destination.longitude,
				category: this.cabCategory,
				pickup_mode: time,
				appToken: this.props.appToken
			})
		);
		goAppAnalytics.trackWithProperties("ola-get-ride-later-estimate", {
			pickup_lat: this.props.origin.latitude,
			pickup_lng: this.props.origin.longitude,
			drop_lat: this.props.destination.latitude,
			drop_lng: this.props.destination.longitude,
			category: this.id
		});
	}

	validateCoupon(couponValue = "") {
		if (couponValue !== "") {
			this.props.dispatch(
				olaVaidateCouponCode({
					access_token: this.props.userToken,
					coupon_code: couponValue,
					pickup_lat: this.props.origin.latitude,
					pickup_lng: this.props.origin.longitude,
					category: this.props.estimate.categories[0].id,
					appToken: this.props.appToken
					// fare_id: this.props.estimate.ride_estimate[0].upfront.fare_id
				})
			);
			this.setState({
				showSpinner: true
			});
		} else {
			GoToast.show(
				I18n.t("enter_valid_coupon"),
				I18n.t("information"),
				"SHORT"
			);
		}
	}

	abortBooking() {
		console.log("trying to cancel", this.state.activateCancel);
		if (this.state.activateCancel) {
			this.setState({ cancelling: true });
			this.props.dispatch(
				olaAbortBooking({
					access_token: this.props.userToken,
					request_id: this.props.bookingStatus.booking_id,
					appToken: this.props.appToken
				})
			);
			goAppAnalytics.trackWithProperties("ola-abort-booking", {
				request_id: this.props.bookingStatus.booking_id
			});
		}
	}

	requestRide(data) {
		let seatCount = data;
		this.setState({ showSpinner: true, requestRideCheck: true });
		if (this.cabCategory === "share") {
			this.props.dispatch(
				olaRequestRide({
					access_token: this.props.userToken,
					pickup_lat: this.props.origin.latitude,
					pickup_lng: this.props.origin.longitude,
					category: this.cabCategory,
					pickup_mode: "now",
					coupon_code: this.props.couponCode,
					drop_lat: this.props.destination.latitude,
					drop_lng: this.props.destination.longitude,
					seat_count: seatCount + 1,
					appToken: this.props.appToken
				})
			);
		} else {
			if (this.pickup_mode === undefined) {
				this.props.dispatch(
					olaRequestRide({
						access_token: this.props.userToken,
						pickup_lat: this.props.origin.latitude,
						pickup_lng: this.props.origin.longitude,
						category: this.cabCategory,
						pickup_mode: "now",
						coupon_code: this.props.couponCode,
						drop_lat: this.props.destination.latitude,
						drop_lng: this.props.destination.longitude,
						appToken: this.props.appToken
					})
				);
			} else {
				let time = this.getOlaTime(data);
				this.props.dispatch(
					olaRequestRide({
						access_token: this.props.userToken,
						pickup_lat: this.props.origin.latitude,
						pickup_lng: this.props.origin.longitude,
						category: this.cabCategory,
						pickup_mode: "LATER",
						pickup_time: time,
						drop_lat: this.props.destination.latitude,
						drop_lng: this.props.destination.longitude,
						appToken: this.props.appToken
					})
				);
			}
		}

		this.mapRef.animateRegionChange({
			latitude: this.props.origin.latitude,
			longitude: this.props.origin.longitude,
			latitudeDelta: LATITUDE_DELTA,
			longitudeDelta: LONGITUDE_DELTA
		});
	}

	renderOriginMarker = latlng => {
		return (
			<Marker coordinate={latlng} image={this.props.scaledImage.locationPickup}>
				<View>
					<Text>{""}</Text>
				</View>
			</Marker>
		);
	};

	renderDestinationMarker = latlng => {
		return (
			<Marker coordinate={latlng} image={this.props.scaledImage.locationDrop}>
				<View>
					<Text>{""}</Text>
				</View>
			</Marker>
		);
	};

	getFitToCoordinate = (temp = []) => {
		if (this.state.requestRideCheck) {
			console.log("calling with null");
			this.mapRef && this.mapRef.fitToCoordinate(null);
		} else {
			let X = [100, -100];
			let Y = [100, -100];
			let coord = [];
			if (temp !== null) {
				temp.map(item => {
					if (item.latitude < X[0]) {
						X[0] = item.latitude;
					}
					if (item.latitude > X[1]) {
						X[1] = item.latitude;
					}
					if (item.longitude < Y[0]) {
						Y[0] = item.longitude;
					}
					if (item.longitude > Y[1]) {
						Y[1] = item.longitude;
					}
				});
			}

			//pushing four coordinates
			X.forEach(lat => {
				Y.forEach(lng => {
					if (temp.length > 0) {
						let obj = {};
						obj.latitude = lat;
						obj.longitude = lng;
						coord.push(obj);
					}
				});
			});
			if (temp === null || temp.length === 0) {
				if (
					this.props.origin.latitude &&
					this.props.origin.longitude &&
					this.props.destination.latitude &&
					this.props.destination.longitude
				) {
					coord = [
						{
							latitude: this.props.origin.latitude,
							longitude: this.props.origin.longitude
						},
						{
							latitude: this.props.destination.latitude,
							longitude: this.props.destination.longitude
						}
					];
				}
			}
			console.log("executing get Fit to coordinate==>", coord);
			this.mapRef.fitToCoordinate(coord);
		}
	};

	resetNavigation = screenName => {
		this.props.navigation.dispatch(
			CommonActions.reset({
				index: 0,
				routes: [{ name: screenName }]
			})
		);
	};
	render() {
		console.log("=> ", this.state);
		const { origin, destination } = this.props;
		const cancelRideBoundary =
			Platform.OS === "ios" ? height / 1.45 : height / 1.4;
		return (
			<View style={styles.fullFlex}>
				{this.state.locationModule ? (
					<LocationModule
						onError={() => this.setState({ locationModule: false })}
					/>
				) : null}
				{this.cabCategory === "rental" || this.cabCategory === "outstation" ? (
					<MapView
						ref={ref => (this.mapRef = ref)}
						mapStyle={
							this.state.requestRideCheck
								? styles.rentalMapStyleWithRequest
								: styles.rentalMapStyleWithoutRequest
						}
						initialRegion={this.state.region}
						showUserLocation
						followUserLocation
					/>
				) : (
					<MapView
						ref={ref => (this.mapRef = ref)}
						mapStyle={
							this.state.requestRideCheck
								? styles.mapStyleWithRequest
								: styles.mapStyleWithoutRequest
						}
						initialRegion={this.state.region}
						showUserLocation
						followUserLocation
						onLayout={true}
						showPath={
							this.state.requestRideCheck ? null : this.props.mapPath !== null
						}
						coords={this.props.mapPath}
						polylineColor={"#5b8de1"}>
						{origin &&
							origin.latitude &&
							origin.longitude &&
							this.renderOriginMarker({
								latitude: origin.latitude,
								longitude: origin.longitude
							})}
						{destination &&
							destination.latitude &&
							destination.longitude &&
							this.renderDestinationMarker({
								latitude: destination.latitude,
								longitude: destination.longitude
							})}
						{this.props.products.length > 0 &&
							typeof this.props.products.filter(
								row => row.id === this.cabCategory
							)[0].all_cabs !== "undefined" &&
							this.props.products.filter(row => row.id === this.cabCategory)[0]
								.all_cabs.length > 0 &&
							this.props.products
								.filter(row => row.id === this.cabCategory)[0]
								.all_cabs.map((data, key) => {
									return (
										<CabMarker
											key={key}
											coordinates={{
												latitude: data.lat,
												longitude: data.lng
											}}
											rotation={data.bearing != null ? data.bearing : 0}
											img={this.props.scaledImage[this.id]}
										/>
									);
								})}
					</MapView>
				)}
				{
					<View style={[styles.headerBar]}>
						<View style={{ flexDirection: "row" }}>
							{!this.state.showSpinner ? (
								<TouchableOpacity onPress={this.backButtonPress}>
									<Icon
										iconType={"material"}
										iconSize={width / 18}
										iconName={"arrow-back"}
										iconColor={"#000000"}
									/>
								</TouchableOpacity>
							) : null}
							{this.state.showSpinner ? (
								<OlaTextBold style={styles.cabNameStyle}>
									{I18n.t("ola_find_ride")}
								</OlaTextBold>
							) : (
								<OlaTextBold style={styles.cabNameStyle}>
									{this.id === "share"
										? "Share"
										: this.id === "micro"
										? "Micro"
										: this.id === "mini"
										? "Mini"
										: this.id === "prime"
										? "Prime"
										: this.id === "prime_play"
										? "Prime Play"
										: this.id === "suv"
										? "Prime Suv"
										: this.id === "rental"
										? "Rentals"
										: this.id === "outstation"
										? "Outstation"
										: this.id === "lux"
										? "LUX"
										: "Ride"}
									{this.props.estimate !== null &&
									typeof this.props.estimate.code === "undefined"
										? this.props.estimate.categories[0].eta === -1
											? I18n.t("no_cab")
											: typeof this.rideLater === "undefined"
											? " " +
											  this.props.estimate.categories[0].eta +
											  " min away"
											: ""
										: null}
								</OlaTextBold>
							)}
						</View>
						{this.id !== "rental" ? (
							<DisabledTextBox
								onPress={() => {
									if (this.state.selectedTextBox !== "origin") {
										this.setState({ selectedTextBox: "origin" });
										if (this.props.origin.address !== null) {
											this.mapRef.animateRegionChange({
												latitude: this.props.origin.latitude,
												longitude: this.props.origin.longitude,
												latitudeDelta: LATITUDE_DELTA,
												longitudeDelta: LONGITUDE_DELTA
											});
										}
									} else {
										this.openPlaceSearch("origin");
									}
								}}
								disabledTextBoxStyle={styles.disabledTextBoxStyle}
								textButtonStyle={styles.textButtonStyle}
								textStyle={styles.textStyle}
								text={
									this.props.origin.address || I18n.t("enter_pickup_location")
								}
								type={"origin"}
								// selected={this.state.selectedTextBox === "origin"}
								iconColor={"#54a624"}
								label={""}>
								<TouchableOpacity
									onPress={() => {
										if (this.state.selectedTextBox !== "origin") {
											this.setState({ selectedTextBox: "origin" });
											if (this.props.origin.address !== null) {
												this.mapRef.animateRegionChange({
													latitude: this.props.origin.latitude,
													longitude: this.props.origin.longitude,
													latitudeDelta: LATITUDE_DELTA,
													longitudeDelta: LONGITUDE_DELTA
												});
											}
										} else {
											this.openPlaceSearch("origin");
										}
									}}>
									<Icon
										iconType={"entypo"}
										iconName={"edit"}
										iconColor={"grey"}
										iconSize={height / 52}
									/>
								</TouchableOpacity>
							</DisabledTextBox>
						) : (
							<LocationText
								locationTextStyle={styles.locationTextStyle}
								placeholder={I18n.t("from")}
								locationType={"origin"}
								locationText={this.props.origin.address}
							/>
						)}
					</View>
				}

				{this.cabCategory !== "rental" ? (
					<DisabledTextBox
						onPress={() => {
							if (this.state.selectedTextBox !== "destination") {
								this.setState({ selectedTextBox: "destination" });
								if (this.props.destination.address !== null) {
									this.mapRef.animateRegionChange({
										latitude: this.props.destination.latitude,
										longitude: this.props.destination.longitude,
										latitudeDelta: LATITUDE_DELTA,
										longitudeDelta: LONGITUDE_DELTA
									});
								}
							} else {
								this.openPlaceSearch("destination");
							}
						}}
						disabledTextBoxStyle={styles.rentalDisabledTextBoxStyle}
						textButtonStyle={styles.rentalTextButtonStyle}
						textStyle={styles.rentalTextStyle}
						text={
							this.props.destination.address || I18n.t("enter_drop_location")
						}
						type={"destination"}
						iconColor={"#ff665a"}
						label={""}>
						<TouchableOpacity
							onPress={() => {
								if (this.state.selectedTextBox !== "destination") {
									this.setState({ selectedTextBox: "destination" });
									if (this.props.destination.address !== null) {
										this.mapRef.animateRegionChange({
											latitude: this.props.destination.latitude,
											longitude: this.props.destination.longitude,
											latitudeDelta: LATITUDE_DELTA,
											longitudeDelta: LONGITUDE_DELTA
										});
									}
								} else {
									this.openPlaceSearch("destination");
								}
							}}>
							<Icon
								iconType={"entypo"}
								iconName={"edit"}
								iconColor={"grey"}
								iconSize={height / 52}
							/>
						</TouchableOpacity>
					</DisabledTextBox>
				) : null}
				{
					<LocationButton
						locationStyle={
							typeof this.rideLater === "undefined"
								? [
										styles.locationButtonWithoutRideLater,
										styles.locationStyleShape
								  ]
								: [
										styles.locationButtonWithRideLater,
										styles.locationStyleShape
								  ]
						}
						iconColor={"#0076ff"}
						iconSize={width / 18}
						currentPosition={data => {
							this.getFitToCoordinate();
						}}
						onError={() => this.setState({ locationModule: true })}
					/>
				}
				{!this.state.requestRideCheck ? (
					<ConfirmRide
						callbackRequestRide={data => {
							this.requestRide(data);
						}}
						openApplyCouponModal={() =>
							this.props.navigation.navigate("ApplyCoupon", {
								validateCoupon: this.validateCoupon
							})
						}
						product={this.props.estimate}
						pickup_mode={this.pickup_mode}
						pickup_time={this.pickup_time}
						getRideLaterEstimate={this.getRideLaterEstimate}
						category={this.cabCategory}
						getRideEstimate={this.getRideEstimate}
						type={"p2p"}
					/>
				) : null}
				{this.state.showDifferentCityWarningModal ? (
					<DialogModal
						dialogModalBackgroundStyle={styles.dialogModalBackgroundStyle}>
						<View style={styles.dialogModalBackgroundView}>
							<OlaTextBold style={styles.olaOutstationText}>
								{I18n.t("book_ola_outstation")}
							</OlaTextBold>
							<View style={styles.rowFlex}>
								<Seperator color={"#777777"} width={width / 1.5} height={0.8} />
							</View>
							<View style={styles.outsideDropText}>
								<OlaTextRegular>
									{I18n.t("drop_location_outside_city")}
								</OlaTextRegular>
								<OlaTextRegular>
									{I18n.t("continue_booking_outstation")}
								</OlaTextRegular>
							</View>
							<View style={styles.changeDropView}>
								<TouchableWithoutFeedback
									onPress={() => {
										this.setState({
											showDifferentCityWarningModal: false,
											selectedTextBox: "destination"
										});
										this.openPlaceSearch("destination");
									}}>
									<View style={styles.changeDropButton}>
										<OlaTextMedium style={styles.changeDropText}>
											{I18n.t("change_drop")}
										</OlaTextMedium>
									</View>
								</TouchableWithoutFeedback>
								<TouchableWithoutFeedback
									onPress={() => {
										this.setState({ showDifferentCityWarningModal: false });
										this.props.navigation.dispatch(
											CommonActions.reset({
												index: 0,
												routes: [{ name: "Home" }]
											})
										);
									}}>
									<View style={styles.continueTextView}>
										<OlaTextMedium style={styles.continueText}>
											{I18n.t("continue")}
										</OlaTextMedium>
									</View>
								</TouchableWithoutFeedback>
							</View>
						</View>
					</DialogModal>
				) : null}
				{this.state.showSpinner ? (
					<View style={styles.spinnerView}>
						<CircleLoader
							height={width / 2}
							width={width / 2}
							backgroundColor={"transparent"}
							showRectangle={false}
							color={"#5CB528"}
						/>
					</View>
				) : null}
				{this.state.requestRideCheck ? (
					<Interactable.View
						ref={this.policyRef}
						style={styles.requestRideAniatedView}
						verticalOnly={true}
						initialPosition={{ y: height / 1.2 }}
						boundaries={{ top: cancelRideBoundary }}
						snapPoints={[{ y: cancelRideBoundary }, { y: height / 1.2 }]}>
						<CancelRide
							abortBooking={this.abortBooking}
							cancelling={this.state.cancelling}
							activateCancel={this.state.activateCancel}
						/>
					</Interactable.View>
				) : null}
				<Modal
					visible={this.state.isRideLaterConfirm}
					onRequestClose={() => {
						this.setState({ isRideLaterConfirm: false });
						this.resetNavigation("Home");
					}}>
					<View style={styles.fullFlex}>
						<TouchableOpacity
							style={styles.rideScheduleCrossButton}
							onPress={() => {
								this.setState({ isRideLaterConfirm: false });
								this.resetNavigation("Home");
							}}>
							<Icon
								iconType={"entypo"}
								iconName={"cross"}
								iconSize={height / 20}
								iconColor={"#808080"}
								style={styles.crossButtonStyle}
							/>
						</TouchableOpacity>
						<View style={styles.rideScheduleTickView}>
							<Image style={styles.rideScheduleTickImage} source={OLATICK} />
							<OlaTextRegular style={styles.rideScheduleTickText}>
								{I18n.t("ride_scheduled")}
							</OlaTextRegular>
							<OlaTextRegular style={styles.rideScheduleCompleteText}>
								{I18n.t("your_ride_scheduled")}
							</OlaTextRegular>
							<Seperator
								width={width / 1.1}
								height={0.5}
								style={styles.rideScheduleSeparator}
							/>
							<TouchableOpacity
								style={styles.centerAlign}
								onPress={() => {
									this.setState({ isRideLaterConfirm: false });
									this.resetNavigation("Home");
								}}>
								<OlaTextRegular style={styles.gotItText}>
									{I18n.t("got_it")}
								</OlaTextRegular>
							</TouchableOpacity>
						</View>
					</View>
				</Modal>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		origin: state.ola && state.ola.bookRide.origin,
		deeplink: state.ola && state.ola.navState.deeplink,
		mapPath: state.ola && state.ola.chooseRide.mapPath,
		products: state.ola && state.ola.bookRide.products,
		estimate: state.ola && state.ola.chooseRide.estimate,
		userToken: state.ola && state.ola.auth.userCredentials,
		couponCode: state.ola && state.ola.chooseRide.couponCode,
		destination: state.ola && state.ola.bookRide.destination,
		bookingStatus: state.ola && state.ola.chooseRide.bookingStatus,
		isNavigationToTrackRide:
			state.ola && state.ola.chooseRide.isNavigationToTrackRide,
		scaledImage: state.ola && state.ola.bookRide.scaledImage
	};
}

export default connect(mapStateToProps)(ChooseRide);
