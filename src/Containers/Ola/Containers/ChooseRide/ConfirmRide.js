import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableWithoutFeedback,
	Image,
	TouchableOpacity
} from "react-native";
import { Seperator, DotsLoader } from "../../../../Components";
import I18n from "../../Assets/Strings/i18n";
import { SEATCOUNT, APPLYCOUPON, PICKUPTIME } from "../../Assets/Img/Image";
import {
	OlaTextBold,
	OlaTextRegular,
	OlaTextMedium
} from "../../Components/OlaText";
import { confirmRideStyle as styles } from "./style";

const { width, height } = Dimensions.get("window");

export default class ConfirmRide extends Component {
	constructor(props) {
		super(props);
		let rideLaterDate = null;

		if (this.props.type !== "outstation") {
			rideLaterDate =
				this.props.pickup_mode && this.props.pickup_mode === "LATER"
					? new Date(this.props.pickup_time).toString()
					: null;
			var rideLaterDateArray = rideLaterDate && rideLaterDate.split(" ");
		}

		this.state = {
			olaTime:
				this.props.type !== "outstation" &&
				this.props.pickup_time !== "undefined"
					? this.props.pickup_time
					: null,
			seat_count: 0,
			rideLaterTime:
				this.props.type !== "oustation" && rideLaterDate !== null
					? rideLaterDateArray[1] +
					  " " +
					  rideLaterDateArray[2] +
					  ", " +
					  rideLaterDateArray[4]
					: null
		};
		this.requestRide = this.requestRide.bind(this);
	}

	requestRide() {
		if (
			this.props.type === "outstation" ||
			this.props.pickup_mode === undefined ||
			this.props.pickup_mode === "NOW"
		) {
			this.props.callbackRequestRide(this.state.seat_count);
		} else {
			this.props.callbackRequestRide(this.state.olaTime);
		}
	}

	render() {
		return (
			<View style={[styles.container]}>
				{this.props.type !== "outstation" ? (
					<Seperator width={width} height={0.2} color={"#777777"} />
				) : null}
				{this.props.type !== "outstation" &&
					(this.props.product !== null ? (
						<View style={styles.productView}>
							{typeof this.props.product.code === "undefined" &&
							this.props.product.ride_estimate &&
							this.props.product.ride_estimate.length > 0 ? (
								<View style={styles.productInsideView}>
									<View style={styles.productRowWiseView}>
										<OlaTextBold style={styles.rupeeText}>{"₹"}</OlaTextBold>
										<OlaTextBold style={styles.cabTypeText}>
											{this.props.product !== null
												? this.props.product.ride_estimate[0].category ===
												  "share"
													? this.props.product.ride_estimate[0].fares[
															this.state.seat_count
													  ].cost
													: this.props.product.ride_estimate[0].amount_min +
													  " - " +
													  this.props.product.ride_estimate[0].amount_max
												: null}
										</OlaTextBold>
									</View>
									<OlaTextRegular style={styles.totalFareText}>
										{I18n.t("total_fare")}
									</OlaTextRegular>
								</View>
							) : (
								<View style={styles.getFareView}>
									<TouchableOpacity
										onPress={() => this.props.getRideEstimate()}>
										<OlaTextRegular style={styles.getFareText}>
											{I18n.t("get_total_fare")}
										</OlaTextRegular>
									</TouchableOpacity>
									<OlaTextRegular style={styles.retryGetFareText}>
										{I18n.t("retry_getting_fare")}
									</OlaTextRegular>
								</View>
							)}
						</View>
					) : (
						<View style={styles.gettingFareText}>
							<DotsLoader size={10} color={"#e0e0e0"} />
							<OlaTextRegular>{I18n.t("getting_fare_detail")}</OlaTextRegular>
						</View>
					))}
				<Seperator
					width={width}
					height={this.props.type === "outstation" ? 0.4 : 0.4}
					color={"#bdbdbd"}
				/>
				{this.props.type !== "outstation" &&
				this.props.pickup_mode &&
				this.props.pickup_mode === "LATER" ? (
					<View style={styles.rideLaterView}>
						<View style={styles.rideLaterPickupImageView}>
							<Image style={styles.rideLaterPickupImage} source={PICKUPTIME} />
							<OlaTextRegular style={styles.rideLaterPickupText}>
								{I18n.t("pickup_time")}
							</OlaTextRegular>
						</View>
						<TouchableOpacity
							activeOpacity={1}
							style={styles.rideLaterTimePickerButton}
							onPress={() => {
								// To Do: Add Date & Time Picker
							}}>
							<OlaTextRegular style={styles.rideLaterPickupText}>
								{this.state.rideLaterTime.substring(
									0,
									this.state.rideLaterTime.length - 3
								)}
							</OlaTextRegular>
							{/* <Icon
									iconType={"entypo"}
									iconSize={width / 35}
									iconName={"chevron-thin-down"}
									iconColor={"#000000"}
								/> */}
						</TouchableOpacity>
					</View>
				) : null}
				<View style={styles.applyCouponView}>
					<TouchableOpacity
						activeOpacity={1}
						style={styles.applyCouponButton}
						onPress={this.props.openApplyCouponModal}>
						<Image style={styles.applyCouponImage} source={APPLYCOUPON} />
						<OlaTextRegular style={styles.applyCouponText}>
							{I18n.t("apply_coupon")}
						</OlaTextRegular>
					</TouchableOpacity>
					{this.props.product !== null &&
					typeof this.props.product.code === "undefined" &&
					this.props.product.ride_estimate.length > 0 &&
					this.props.product.ride_estimate[0].category === "share" ? (
						<Seperator width={0.4} height={height / 25} color={"#bdbdbd"} />
					) : null}
					{this.props.product !== null &&
					typeof this.props.product.code === "undefined" &&
					this.props.product.ride_estimate &&
					this.props.product.ride_estimate.length > 0 &&
					this.props.product.ride_estimate[0].category === "share" ? (
						<TouchableWithoutFeedback
							onPress={() => {
								this.state.seat_count === 0
									? this.setState({ seat_count: 1 })
									: this.setState({ seat_count: 0 });
							}}>
							<View style={styles.seatCountView}>
								<Image source={SEATCOUNT} style={styles.seatCountImage} />
								<OlaTextRegular style={styles.seatCountText}>
									{this.state.seat_count === 0
										? "1" + I18n.t("ola_seat")
										: "2" + I18n.t("ola_seats")}
								</OlaTextRegular>
							</View>
						</TouchableWithoutFeedback>
					) : (
						<View style={styles.confirmEmptyView} />
					)}
				</View>
				<TouchableWithoutFeedback onPress={this.requestRide}>
					<View style={styles.confirmBookingButton}>
						<OlaTextMedium style={styles.confirmBookingText}>
							{I18n.t("confirm_booking_button")}
						</OlaTextMedium>
					</View>
				</TouchableWithoutFeedback>
			</View>
		);
	}
}
