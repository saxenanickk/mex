import React, { Component } from "react";
import { View } from "react-native";
import { connect } from "react-redux";
import { OlaTextRegular } from "../../Components/OlaText";
import I18n from "../../Assets/Strings/i18n";

class Summary extends Component {
	render() {
		return (
			<View>
				<OlaTextRegular>{`${I18n.t("summary_C")}`}</OlaTextRegular>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {};
}

export default connect(mapStateToProps)(Summary);
