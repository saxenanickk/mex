import { takeLatest, call } from "redux-saga/effects";
import OlaAPI from "../../Api/";
/**
 * Actions
 */
export const OLA_LOGIN = "OLA_LOGIN";
export const OLA_REFRESH_TOKEN = "OLA_REFRESH_TOKEN";
export const SAVE_OLA_CREDENTIALS = "SAVE_OLA_CREDENTIALS";
export const OLA_SAVE_LOGIN_STATE = "OLA_SAVE_LOGIN_STATE";
/**
 * Action Creators
 */
export const olaLogin = payload => ({ type: OLA_LOGIN, payload });
export const olaRefreshToken = payload => ({
	type: OLA_REFRESH_TOKEN,
	payload
});
export const saveOlaCredentials = payload => ({
	type: SAVE_OLA_CREDENTIALS,
	payload
});
export const olaSaveLoginState = payload => ({
	type: OLA_SAVE_LOGIN_STATE,
	payload
});

export function* olaAuthSaga(dispatch) {
	yield takeLatest(OLA_LOGIN, handleLogin);
	yield takeLatest(OLA_REFRESH_TOKEN, handleOlaRefreshToken);
}

/**
 * Handlers (Generator Functions)
 */
function* handleOlaRefreshToken(action) {
	try {
		let refreshToken = yield call(OlaAPI.refreshToken, action.payload);
		console.log("Refresh Token Saga data: ", refreshToken);
	} catch (e) {
		console.log("Refresh Token Saga Error: ", e);
	}
}

function* handleLogin(action) {
	console.log("Login: ", action);
	let results = yield call(OlaAPI.login, { phone: action.payload });
	console.log(results);
}
