import React from "react";
import { Stack } from "../../../../utils/Navigators";
import { connect } from "react-redux";
import Login from "./Login";
import WebView from "./WebView";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";

const Navigator = () => (
	<Stack.Navigator headerMode={"none"}>
		<Stack.Screen name={"Login"} component={Login} />
		<Stack.Screen name={"WebView"} component={WebView} />
	</Stack.Navigator>
);

class Auth extends React.Component {
	render() {
		return (
			<Navigator
				onNavigationStateChange={(prevState, currState) =>
					GoAppAnalytics.logChangeOfScreenInStack("ola", prevState, currState)
				}
			/>
		);
	}
}

function mapStateToProps(state) {
	return {};
}
export default connect(mapStateToProps)(Auth);
