import React from "react";
import Config from "react-native-config";
import { View, Image, TouchableOpacity, StatusBar } from "react-native";
import { connect } from "react-redux";
import I18n from "../../Assets/Strings/i18n";
import { OLA } from "../../Assets/Img/Image";
import { InformationFooter, ProgressScreen } from "../../../../Components";
import { OlaTextRegular } from "../../Components/OlaText";
import { loginStyles as styles } from "./style";
import OlaApi from "../../Api";

const { SERVER_BASE_URL_OLA_LOGIN } = Config;

class Login extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			showLoginSpinner: false
		};
		this.loginSuccess = this.loginSuccess.bind(this);
		this.onLogin = this.onLogin.bind(this);
	}

	onLogin() {
		this.setState({ showLoginSpinner: true });
		this.props.navigation.navigate("WebView", {
			callbackFunction: data => {
				console.log("Callback: ", data);
				if (data) {
					console.log("---->>>");
					this.setState({ successReturn: true });
				} else {
					this.setState({ showLoginSpinner: false });
				}
			},
			url: SERVER_BASE_URL_OLA_LOGIN
		});
	}

	callAuth = (appToken, authToken) => {
		OlaApi.saveUserAuth({ appToken, authToken });
	};

	loginSuccess() {
		this.callAuth(this.props.appToken, this.props.userToken);
		this.props.navigator.resetTo({
			screen: "ola.BookRide",
			navigatorStyle: {
				navBarHidden: true
			}
		});
	}

	shouldComponentUpdate(props) {
		if (props.userToken) {
			this.loginSuccess();
			return false;
		} else {
			if (this.state.showLoginSpinner) {
				this.setState({ showLoginSpinner: false });
			}
		}
		return true;
	}

	render() {
		if (this.props.userToken !== null) {
			if (this.props.userToken) {
				this.loginSuccess();
			}
		}
		if (this.state.showLoginSpinner) {
			return (
				<View style={styles.loginSpinnerView}>
					<ProgressScreen indicatorColor={"#3FAAD3"} indicatorSize={50} />
				</View>
			);
		} else {
			return (
				<View style={styles.container}>
					<StatusBar backgroundColor="#bdbdbd" barStyle="light-content" />
					<View style={styles.olaContainer}>
						<Image style={styles.olaImage} source={OLA} />
					</View>
					<TouchableOpacity
						style={
							this.props.isInternet
								? styles.loginButtonWithInternet
								: styles.loginButtonWithoutInternet
						}
						onPress={this.onLogin}>
						<OlaTextRegular style={styles.loginText}>
							{I18n.t("login_to_ola")}
						</OlaTextRegular>
					</TouchableOpacity>
					{!this.props.isInternet ? (
						<InformationFooter
							informationFooterText={I18n.t("check_internet")}
							informationFooterStyle={styles.informationFooterStyle}
							informationFooterTextStyle={styles.informationFooterTextStyle}
						/>
					) : null}
				</View>
			);
		}
	}
}

function mapStateToProps(state) {
	return {
		userToken: state.ola && state.ola.userToken,
		isInternet: state.netInfo.status,
		appToken: state.appToken.token
	};
}

export default connect(mapStateToProps)(Login);
