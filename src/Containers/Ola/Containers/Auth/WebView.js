import React from "react";
import { View, StatusBar, Dimensions, TouchableOpacity } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { WebView } from "react-native-webview";
import { connect } from "react-redux";
import { saveOlaCredentials, olaSaveLoginState } from "./Saga";
import { Icon, GoToast } from "../../../../Components";
import { OlaTextMedium } from "../../Components/OlaText";
import { webViewStyles as styles } from "./style";
import I18n from "../../Assets/Strings/i18n";
import OlaApi from "../../Api";

const { width } = Dimensions.get("window");

class Webview extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			getUrl: false
		};
		const { route } = props;
		const { callbackFunction, url } = route.params ? route.params : {};

		this.callbackFunction = callbackFunction;
		this.url = url;

		this.successfulDismiss = false;
	}

	componentWillUnmount() {
		if (!this.successfulDismiss) {
			this.callbackFunction();
		}
	}

	onNavStateChange(e) {
		if (!this.state.getUrl) {
			if (
				e.url.startsWith("https://api.platify.co/api/callback/olaCallbacktoken")
			) {
				console.log("Webview callback captured ", e);
				if (e.url.indexOf("#access_token") > -1) {
					var token = e.url.substring(
						e.url.indexOf("#access_token"),
						e.url.length
					);
				} else {
					GoToast.show("Please click on allow to book a ride in Ola");
					this.props.navigation.goBack();
					return;
				}
				token = token.substring(14, token.indexOf("&"));
				console.log("token is ", token);
				this.setState({
					getUrl: true
				});
				console.log("this is webview response: ", e.url);
				this.props.dispatch(saveOlaCredentials(token));
				AsyncStorage.setItem("ola_user_credentials", token);
				AsyncStorage.setItem(
					"ola_is_user_logged_in",
					JSON.stringify({ value: true })
				);
				OlaApi.saveUserAuth({
					appToken: this.props.appToken,
					authToken: token
				});
				this.successfulDismiss = true;
				this.props.dispatch(olaSaveLoginState(true));
			}
		}
	}

	render() {
		return (
			<View style={styles.fullFlex}>
				<StatusBar backgroundColor="#bdbdbd" barStyle="light-content" />
				<View style={styles.mainView}>
					<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
						<View style={styles.goBackButton}>
							<Icon
								iconType={"material"}
								iconSize={width / 16}
								iconName={"arrow-back"}
								iconColor={"#707070"}
							/>
						</View>
					</TouchableOpacity>
					<OlaTextMedium style={styles.loginToOla}>
						{I18n.t("login_to_ola")}
					</OlaTextMedium>
				</View>
				<WebView
					ref={ref => (this.webview = ref)}
					source={{ uri: this.url }}
					javaScriptEnabledAndroid={true}
					onNavigationStateChange={this.onNavStateChange.bind(this)}
				/>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token
	};
}

export default connect(mapStateToProps)(Webview);
