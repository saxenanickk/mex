import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const loginStyles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#ffffff",
		justifyContent: "space-between"
	},
	mobileContainer: {
		flexDirection: "row",
		padding: 10
	},
	flag: {
		width: 20,
		height: 15
	},
	code: {
		padding: 10,
		fontSize: 18,
		color: "#000000"
	},
	mobile: {
		padding: 10,
		fontSize: 18
	},
	headerStyle: {
		height: 30,
		backgroundColor: "#000000"
	},
	titleStyle: {
		color: "#ffffff",
		fontSize: 20,
		marginLeft: 20
	},
	socialContainer: {
		padding: 5,
		alignSelf: "center",
		alignItems: "center",
		backgroundColor: "#CDDC39",
		justifyContent: "center",
		borderRadius: width / 90
	},
	olaContainer: {
		width: width,
		height: height / 1.7,
		justifyContent: "center",
		alignItems: "center"
	},
	loginSpinnerView: {
		elevation: 20,
		position: "absolute",
		width: width,
		height: height,
		backgroundColor: "#000000",
		opacity: 0.5,
		justifyContent: "center",
		alignItems: "center"
	},
	olaImage: {
		width: width / 2,
		height: height / 10
	},
	loginButtonWithInternet: {
		height: height / 12.28,
		width: width / 1.1,
		alignSelf: "center",
		alignItems: "center",
		backgroundColor: "#d5db2a",
		justifyContent: "center",
		borderRadius: width / 90,
		bottom: width / 25
	},
	loginButtonWithoutInternet: {
		height: height / 12.28,
		width: width / 1.1,
		alignSelf: "center",
		alignItems: "center",
		backgroundColor: "#d5db2a",
		justifyContent: "center",
		borderRadius: width / 90,
		bottom: width / 10
	},
	loginText: {
		fontSize: height / 45,
		color: "#1e1f06"
	},
	informationFooterStyle: { backgroundColor: "#ff0000" },
	informationFooterTextStyle: { color: "rgba(255,255,255,0.8)" }
});

export const webViewStyles = StyleSheet.create({
	fullFlex: { flex: 1 },
	mainView: {
		width: width,
		elevation: 5,
		backgroundColor: "#ffffff",
		justifyContent: "flex-start",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33
	},
	goBackButton: {
		justifyContent: "center",
		width: width / 6,
		alignItems: "center",
		height: height / 13.33
	},
	loginToOla: {
		color: "#000000",
		fontSize: height / 45.71,
		marginLeft: width / 50
	}
});
