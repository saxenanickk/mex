import { SAVE_OLA_CREDENTIALS, OLA_SAVE_LOGIN_STATE } from "./Saga";

const initialState = {
	userCredentials: null,
	olaIsUserLoggedIn: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case SAVE_OLA_CREDENTIALS:
			return {
				...state,
				userCredentials: action.payload
			};
		case OLA_SAVE_LOGIN_STATE:
			return {
				...state,
				olaIsUserLoggedIn: action.payload
			};
		default:
			return state;
	}
};
