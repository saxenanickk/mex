import React from "react";
import {
	ScrollView,
	View,
	Dimensions,
	StatusBar,
	Image,
	SafeAreaView
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import { Button, Icon } from "../../../../../Components";
import {
	BOOKRIDE,
	LOGOUT,
	YOURRIDES,
	SUPPORT,
	BOOKRIDESELECTED,
	YOURRIDESSELECTED,
	SUPPORTSELECTED,
	LOGOUTSELECTED
} from "../../../Assets/Img/Image";
import I18n from "../../../Assets/Strings/i18n";
import { OlaTextRegular } from "../../../Components/OlaText";
import { indexStyles as styles } from "./styles";
import { olaSaveLoginState } from "../../Auth/Saga";
import Freshchat from "../../../../../CustomModules/Freshchat";

const { width } = Dimensions.get("window");

class Drawer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedRoute: "Book Ride"
		};
		this.handleLogout = this.handleLogout.bind(this);
	}

	async handleLogout() {
		await AsyncStorage.removeItem("ola_user_credentials");
		await AsyncStorage.removeItem("ola_is_user_logged_in");
		await AsyncStorage.removeItem("RIDE_IN_PROGRESS");
		this.props.dispatch(olaSaveLoginState(false));
	}

	render() {
		return (
			<ScrollView style={styles.mainScrollView}>
				<StatusBar backgroundColor="#bdbdbd" barStyle="light-content" />
				<View style={styles.boundaryView} />
				<SafeAreaView
					style={styles.container}
					forceInset={{ top: "always", horizontal: "never" }}>
					<Button
						style={styles.logoutButton}
						onPress={() => this.props.navigation.navigate("Goapp")}>
						<View style={styles.centeredRow}>
							<Image style={styles.logoutImage} source={LOGOUTSELECTED} />
							<OlaTextRegular style={styles.logoutText}>
								{"Go To Partners"}
							</OlaTextRegular>
						</View>
					</Button>
					{this.props.items.map((data, key) => {
						return (
							<Button
								style={
									this.state.selectedRoute === data.key
										? styles.drawerButtonOne
										: styles.drawerButtonTwo
								}
								key={key}
								onPress={() => {
									this.setState({ selectedRoute: data.key });
									this.props.navigation.navigate(data.routeName);
									this.props.navigation.closeDrawer();
								}}>
								<View style={styles.centeredRow}>
									<Image
										style={styles.drawerImage}
										source={
											data.key === "book_ride"
												? this.state.selectedRoute === data.key
													? BOOKRIDESELECTED
													: BOOKRIDE
												: data.key === "your_ride"
												? this.state.selectedRoute === data.key
													? YOURRIDESSELECTED
													: YOURRIDES
												: this.state.selectedRoute === data.key
												? SUPPORTSELECTED
												: SUPPORT
										}
									/>
									<OlaTextRegular style={styles.drawerImageText}>
										{I18n.t(data.key)}
									</OlaTextRegular>
								</View>
							</Button>
						);
					})}
					<Button
						style={styles.logoutButton}
						onPress={() => Freshchat.launchSupportChat()}>
						<View style={[styles.centeredRow, styles.chatSupportView]}>
							<Icon
								iconType={"material"}
								iconSize={width / 20}
								iconName={"headset"}
								iconColor={"rgba(0,0,0,0.5)"}
							/>
							<OlaTextRegular
								style={[styles.logoutText, { marginLeft: width / 20 }]}>
								{I18n.t("chat_support")}
							</OlaTextRegular>
						</View>
					</Button>

					<Button style={styles.logoutButton} onPress={this.handleLogout}>
						<View style={styles.centeredRow}>
							<Image style={styles.logoutImage} source={LOGOUT} />
							<OlaTextRegular style={styles.logoutText}>
								{I18n.t("logout")}
							</OlaTextRegular>
						</View>
					</Button>
				</SafeAreaView>
			</ScrollView>
		);
	}
}

function mapStateToProps(state) {
	return {};
}

export default connect(mapStateToProps)(Drawer);
