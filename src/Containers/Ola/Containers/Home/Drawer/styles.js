import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
export const indexStyles = StyleSheet.create({
	container: {
		flex: 1
	},
	mainScrollView: {
		backgroundColor: "#f3f6f8"
	},
	boundaryView: {
		backgroundColor: "#f3f6f8",
		width: width,
		height: height / 13.2,
		elevation: 5,
		borderColor: "#ffffff"
	},
	drawerButtonOne: { width: width, backgroundColor: "#e5e8e9" },
	drawerButtonTwo: { width: width, backgroundColor: "#f3f6f8" },
	centeredRow: {
		flexDirection: "row",
		alignItems: "center"
	},
	chatSupportView: {
		paddingLeft: width / 38
	},
	drawerImage: {
		width: width / 10,
		height: width / 10
	},
	drawerImageText: {
		marginLeft: width / 35,
		color: "#000000",
		fontSize: height / 50
	},
	logoutButton: {
		width: width,
		backgroundColor: "#f3f6f8"
	},
	logoutImage: {
		width: width / 10,
		height: width / 10
	},
	logoutText: {
		marginLeft: width / 35,
		color: "#000000",
		fontSize: height / 50
	}
});
