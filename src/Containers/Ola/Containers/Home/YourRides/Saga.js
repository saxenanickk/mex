import {
	call,
	put,
	takeLatest,
	takeEvery,
	all,
	take
} from "redux-saga/effects";
import OlaApi from "../../../Api";
import LocationApi from "../../../../../CustomModules/LocationModule/Api";
import { olaCancelReasons } from "../../TrackRide/Saga";
import { GoToast } from "../../../../../Components";
import I18n from "../../../Assets/Strings/i18n";
/**
 * Constants
 */

export const OLA_REQUEST_RIDES = "OLA_REQUEST_RIDES";
export const OLA_RECEIVE_RIDES = "OLA_RECEIVE_RIDES";
export const OLA_RECEIVE_RIDES_FAILED = "OLA_RECEIVE_RIDES_FAILED";
export const OLA_SINGLE_RIDE_REQUEST = "OLA_SINGLE_RIDE_REQUEST";
export const OLA_SINGLE_RIDE_RECEIVED = "OLA_SINGLE_RIDE_RECEIVED";
export const RESET_OLA_REQUESTED_RIDES = "RESET_OLA_REQUESTED_RIDES";
export const OLA_CANCEL_RIDES = "OLA_CANCEL_RIDE";
export const REPLACE_FIRST_THREE_RIDES = "REPLACE_FIRST_THREE_RIDES";
export const OLA_SAVE_BOOKING_ID = "OLA_SAVE_BOOKING_ID";
export const OLA_SINGLE_RIDE_DETAIL_ERROR = "OLA_SINGLE_RIDE_DETAIL_ERROR";

/**
 * Action Creators
 */

export const olaRequestRides = payload => ({
	type: OLA_REQUEST_RIDES,
	payload
});
export const olaReceiveRides = payload => ({
	type: OLA_RECEIVE_RIDES,
	payload
});

export const olaCancelRide = payload => ({ type: OLA_CANCEL_RIDES, payload });
export const olaRequestSingleRide = payload => ({
	type: OLA_SINGLE_RIDE_REQUEST,
	payload
});
export const olaReceiveSingleRide = payload => ({
	type: OLA_SINGLE_RIDE_RECEIVED,
	payload
});
export const replaceFirstThreeRides = payload => ({
	type: REPLACE_FIRST_THREE_RIDES,
	payload
});

export const resetOlaRequestedRides = payload => ({
	type: RESET_OLA_REQUESTED_RIDES,
	payload
});

export const olaSaveBookingId = payload => ({
	type: OLA_SAVE_BOOKING_ID,
	payload
});
export const olaReceiveRidesFailed = () => ({ type: OLA_RECEIVE_RIDES_FAILED });

export const olaSingleRideDetailError = payload => ({
	type: OLA_SINGLE_RIDE_DETAIL_ERROR,
	payload
});
/**
 * Saga
 */

export function* olaRidesSaga(dispatch) {
	yield takeEvery(OLA_REQUEST_RIDES, handleOlaRequestRides);
	yield takeEvery(OLA_SINGLE_RIDE_REQUEST, handleOlaSingleRideRequest);
	yield takeLatest(OLA_CANCEL_RIDES, handleOlaCancelRide);
}

/**
 * Handlers
 */

function* handleOlaCancelRide(action) {
	try {
		let cancelRide = yield call(OlaApi.cancelRide, action.payload);
		console.log("Ola Cancel Ride Data: ", cancelRide);
		yield put(
			olaRequestSingleRide({
				ride: {
					access_token: action.payload.access_token,
					request_id: action.payload.request_id,
					appToken: action.payload.appToken
				},
				pickup: {
					latitude: action.payload.pickup_lat,
					longitude: action.payload.pickup_lng
				},
				drop: {
					latitude: action.payload.drop_lat,
					longitude: action.payload.drop_lng
				}
			})
		);
		let rides = yield call(OlaApi.getUserHistory, action.payload);
		yield put(replaceFirstThreeRides(rides.bookings));

		if (cancelRide.status !== "SUCCESS") {
			GoToast.show(cancelRide.text, I18n.t("information"));
		}
	} catch (error) {
		console.log("Ola Cancel Ride Error: ", error);
	}
}

function* handleOlaRequestRides(action) {
	try {
		console.log("action are", action.payload);
		let bookings = [];
		if (action.payload.pages.length === 3) {
			let rides1 = yield call(OlaApi.getUserHistory, {
				access_token: action.payload.access_token,
				page: action.payload.pages[0],
				appToken: action.payload.appToken
			});
			console.log("rides1 are", rides1);
			if (
				typeof rides1 !== "undefined" &&
				typeof rides1.bookings === "object"
			) {
				bookings = [...bookings, ...rides1.bookings];

				if (rides1.bookings.length >= 3) {
					let rides2 = yield call(OlaApi.getUserHistory, {
						access_token: action.payload.access_token,
						page: action.payload.pages[1],
						appToken: action.payload.appToken
					});
					console.log("rides2 are", rides2);
					bookings = [...bookings, ...rides2.bookings];
					if (rides2.bookings.length >= 3) {
						let rides3 = yield call(OlaApi.getUserHistory, {
							access_token: action.payload.access_token,
							page: action.payload.pages[2],
							appToken: action.payload.appToken
						});
						bookings = [...bookings, ...rides3.bookings];
					}
				}
			} else {
				bookings = [...bookings];
			}
		} else {
			let rides = yield call(OlaApi.getUserHistory, {
				access_token: action.payload.access_token,
				page: action.payload.pages[0],
				appToken: action.payload.appToken
			});
			bookings = [...bookings, ...rides.bookings];
		}
		console.log("bookings are");
		yield put(olaReceiveRides(bookings));
	} catch (e) {
		yield put(olaReceiveRidesFailed());
		console.log("Ola Fetch my ride history failed: ", e);
	}
}

function* handleOlaSingleRideRequest(action) {
	try {
		let pickupAddress = "";
		let dropAddress = "";
		const rideDetails = yield call(OlaApi.getCurrentRide, action.payload.ride);
		console.log("rideDetails is", rideDetails);
		// extract pickup and drop address
		const [pickup, drop] = yield all([
			call(LocationApi.reverseGeocoding, {
				latitude: rideDetails.pickup_lat,
				longitude: rideDetails.pickup_lng
			}),
			call(LocationApi.reverseGeocoding, {
				latitude: rideDetails.drop_lat,
				longitude: rideDetails.drop_lng
			})
		]);

		if (pickup.results.length > 0) {
			pickupAddress = pickup.results[0].formatted_address;
		}
		if (drop.results.length > 0) {
			dropAddress = drop.results[0].formatted_address;
		}
		yield put(
			olaReceiveSingleRide({ rideDetails, pickupAddress, dropAddress })
		);
		//if ride is in progress extract calcel reasons
		if (
			rideDetails.booking_status === "CLIENT_LOCATED" ||
			rideDetails.booking_status === "ALLOTMENT_PENDING" ||
			rideDetails.booking_status === "CALL_DRIVER"
		) {
			const cancelReasons = yield call(OlaApi.getCancelReasons, {
				access_token: action.payload.ride.access_token,
				category: rideDetails.category,
				pickup_lat: rideDetails.pickup_lat,
				pickup_lng: rideDetails.pickup_lng,
				appToken: action.payload.appToken
			});
			if (
				cancelReasons.cancel_reasons !== null &&
				cancelReasons.cancel_reasons !== undefined
			) {
				yield put(olaCancelReasons(cancelReasons));
			}
		}
	} catch (e) {
		console.log("Reverse GeoCoding Failed", e);
		yield put(olaSingleRideDetailError(true));
	}
}
