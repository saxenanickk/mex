import React, { Component } from "react";
import {
	StatusBar,
	View,
	FlatList,
	Image,
	TouchableOpacity,
	Dimensions,
	Platform
} from "react-native";
import { connect } from "react-redux";
import I18n from "../../../Assets/Strings/i18n";
import {
	olaRequestRides,
	olaRequestSingleRide,
	resetOlaRequestedRides
} from "./Saga";
import { olaBookingStatus } from "../../ChooseRide/Saga";
import { olaGetCancelReasons } from "../../TrackRide/Saga";
import {
	Icon,
	Seperator,
	ErrorBoundary,
	ProgressScreen,
	EmptyView
} from "../../../../../Components";
import RideDetails from "./RideDetails";
import {
	MINISELECTED,
	PRIMESELECTED,
	SEDANSELECTED,
	SUVSELECTED,
	AUTOSELECTED,
	ERICKSELECTED,
	KAALIPEELISELECTED,
	LUXSELECTED,
	MICROSELECTED,
	DRIVER,
	CANCELLED,
	SHARESELECTED
} from "../../../Assets/Img/Image";
import { OlaTextRegular, OlaTextMedium } from "../../../Components/OlaText";
import { indexStyles as styles } from "./style";

const { width, height } = Dimensions.get("window");

const carImages = {
	mini: MINISELECTED,
	prime: PRIMESELECTED,
	sedan: SEDANSELECTED,
	suv: SUVSELECTED,
	auto: AUTOSELECTED,
	erick: ERICKSELECTED,
	kp: KAALIPEELISELECTED,
	lux: LUXSELECTED,
	micro: MICROSELECTED,
	exec: PRIMESELECTED,
	prime_play: SEDANSELECTED,
	share: SHARESELECTED
};

const carTypesText = {
	mini: "Mini",
	prime: "Prime",
	sedan: "Sedan",
	suv: "SUV",
	auto: "Auto",
	erick: "Erick",
	kp: "Kaali Peeli",
	lux: "LUX",
	micro: "Micro",
	exec: "Exec",
	prime_play: "Prime Play",
	share: "Share"
};

class RideList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			pagesLoaded: [1, 2, 3]
		};
		this.time = null;
		this._keyExtractor = this._keyExtractor.bind(this);
		this._renderRide = this._renderRide.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.openModal = this.openModal.bind(this);
		this.changeScreen = this.changeScreen.bind(this);
	}

	componentDidMount() {
		console.log("props are", this.props);
		this.props.dispatch(
			olaRequestRides({
				access_token: this.props.userToken,
				pages: this.state.pagesLoaded,
				appToken: this.props.appToken
			})
		);
	}

	closeModal() {
		this.props.navigation.goBack();
	}

	getStatusText = status => {
		switch (status) {
			case "CALL_DRIVER":
				return I18n.t("call_driver");
			case "CLIENT_LOCATED":
				return I18n.t("cab_arrived");
			case "IN_PROGRESS":
				return I18n.t("in_progress");
			case "COMPLETED":
				return "Ride Completed";
			default:
				return I18n.t("invalid");
		}
	};

	openModal(selectedCabDetail, index) {
		if (
			selectedCabDetail.booking_status === "CLIENT_LOCATED" ||
			selectedCabDetail.booking_status === "ALLOTMENT_PENDING" ||
			selectedCabDetail.booking_status === "CALL_DRIVER"
		) {
			console.log("calling get cancel reasons", selectedCabDetail);
			this.props.dispatch(
				olaGetCancelReasons({
					access_token: this.props.userToken,
					category: selectedCabDetail.category,
					pickup_lat: selectedCabDetail.pickup_lat,
					pickup_lng: selectedCabDetail.pickup_lng,
					appToken: this.props.appToken
				})
			);
		}
		this.props.dispatch(
			olaRequestSingleRide({
				ride: {
					access_token: this.props.userToken,
					request_id: selectedCabDetail.booking_id,
					appToken: this.props.appToken
				},
				appToken: this.props.appToken,
				pickup: {
					latitude: selectedCabDetail.pickup_lat,
					longitude: selectedCabDetail.pickup_lng
				},
				drop: {
					latitude: selectedCabDetail.drop_lat,
					longitude: selectedCabDetail.drop_lng
				}
			})
		);
		this.props.navigation.navigate("RideDetails", {
			time: this.time,
			image: carImages[selectedCabDetail.category],
			closeModal: () => this.closeModal(),
			changeScreen: (screenName, params = {}) =>
				this.changeScreen(screenName, params),
			selectedCab: selectedCabDetail
		});
	}

	changeScreen(screenName, params = {}) {
		try {
			this.props.dispatch(olaBookingStatus(this.props.myRides.rideDetail));
			this.props.navigation.navigate(screenName, params);
		} catch (error) {
			console.log("error in navigation is", error);
		}
	}

	_keyExtractor(item, index) {
		return index.toString();
	}

	/**
	 *
	 * @param item
	 * @returns {*}
	 * @private
	 */
	_renderRide({ item }, index) {
		let category = item.category.toUpperCase().replace(/[^A-Z0-9]+/gi, " ");
		let timeArray = item.booking_time ? item.booking_time.split(" ") : "";
		let difference = "";
		let day = "";
		let time = "";
		if (timeArray.length > 0) {
			timeArray.length = 5;
			difference =
				new Date().getDate() - new Date(timeArray.toString()).getDate();
			day =
				difference === 0
					? "Today "
					: difference === 1
					? "Yesterday "
					: timeArray[0] + " " + timeArray[2] + " " + timeArray[1] + ", ";
			time = timeArray[4].split(":");

			if (item.booking_status !== "ALLOTMENT_FAILED") {
				return (
					<TouchableOpacity
						style={styles.renderRideButton}
						onPress={() => {
							this.time =
								day + time[0] + ":" + time[1] + (time[0] < 12 ? " AM" : " PM");
							this.openModal(item, index);
						}}>
						<View style={styles.spaceBetweenRow}>
							<View style={styles.carInfo}>
								<Image
									source={carImages[item.category]}
									style={styles.categoryImage}
								/>
								<View style={styles.cabImage}>
									<OlaTextMedium style={styles.blackText}>
										{day +
											time[0] +
											":" +
											time[1] +
											(time[0] < 12 ? " AM" : " PM")}
									</OlaTextMedium>
									<View style={styles.flexStartRow}>
										<OlaTextRegular style={styles.regularText}>
											{item.drop_lat === 0
												? `${
														carTypesText.hasOwnProperty(item.category)
															? carTypesText[item.category]
															: category
												  } Rental`
												: category}
										</OlaTextRegular>
										<OlaTextRegular style={styles.regularText}>
											{` . ${item.booking_id}`}
										</OlaTextRegular>
									</View>
									{item.booking_status !== "BOOKING_CANCELLED" ? (
										<OlaTextMedium style={styles.bookingStatusText}>
											{item.booking_status === "ALLOTMENT_PENDING" &&
												"Scheduled"}
											{item.booking_status !== "ALLOTMENT_PENDING" &&
												this.getStatusText(item.booking_status)}
										</OlaTextMedium>
									) : null}
								</View>
							</View>
							<View style={styles.cancelledView}>
								{item.booking_status === "BOOKING_CANCELLED" ? (
									<Image source={CANCELLED} style={styles.cancelledImage} />
								) : null}
								{/*<Image source={DRIVER} style={styles.driverImage} />*/}
							</View>
						</View>
					</TouchableOpacity>
				);
			}
		} else {
			return null;
		}
	}

	componentWillUnmount() {
		this.props.dispatch(resetOlaRequestedRides());
	}

	_renderListEmptyComponent = () => {
		if (this.props.myRides.isLoading) {
			return <ProgressScreen indicatorColor={"#cddc39"} indicatorSize={50} />;
		} else if (!this.props.myRides.error) {
			return (
				<EmptyView
					noSorry={true}
					isRefresh={false}
					noRecordFoundMesage={`${I18n.t("no_ride_available")}`}
				/>
			);
		}
	};

	render() {
		return (
			<ErrorBoundary longText={true}>
				<View style={styles.fullFlex}>
					<StatusBar backgroundColor="#7f7f7f" barStyle="light-content" />
					<View style={styles.header}>
						<TouchableOpacity
							onPress={() => this.props.navigation.openDrawer()}>
							<Icon
								iconType={"material"}
								iconName={"menu"}
								iconColor={"#000000"}
								iconSize={width / 15}
							/>
						</TouchableOpacity>
						<OlaTextMedium style={styles.yourRideText}>
							{I18n.t("your_ride")}
						</OlaTextMedium>
					</View>
					<FlatList
						contentContainerStyle={styles.fullFlex}
						data={this.props.myRides.bookings}
						keyExtractor={this._keyExtractor}
						renderItem={this._renderRide}
						onMomentumScrollEnd={event => {
							if (
								event.nativeEvent.contentOffset.y > 0 &&
								this.props.myRides.bookings.length >= 9
							) {
								const pageToLoad =
									this.state.pagesLoaded[this.state.pagesLoaded.length - 1] + 1;

								this.setState(prevState => ({
									pagesLoaded: [...prevState.pagesLoaded, pageToLoad],
									requestedMoreRides: true
								}));
								this.props.dispatch(
									olaRequestRides({
										access_token: this.props.userToken,
										pages: [pageToLoad],
										appToken: this.props.appToken
									})
								);
							}
						}}
						ListEmptyComponent={this._renderListEmptyComponent()}
						ListFooterComponent={() => {
							if (
								this.state.requestedMoreRides &&
								this.props.myRides.isLoading
							) {
								return (
									<ProgressScreen
										indicatorColor={"#3FAAD3"}
										indicatorSize={50}
									/>
								);
							} else if (this.props.myRides.error) {
								return (
									<View style={styles.fetchFailView}>
										<OlaTextMedium style={styles.fetchFailText}>
											{`${I18n.t("failed_fetch_ride")}`}
										</OlaTextMedium>
										<TouchableOpacity
											onPress={() =>
												this.props.dispatch(
													olaRequestRides({
														access_token: this.props.userToken,
														pages: this.state.pagesLoaded,
														appToken: this.props.appToken
													})
												)
											}
											style={styles.tryAgainButton}>
											<OlaTextRegular style={styles.tryAgainText}>
												{`${I18n.t("ola_try_again")}`}
											</OlaTextRegular>
										</TouchableOpacity>
									</View>
								);
							}
							return null;
						}}
					/>
				</View>
			</ErrorBoundary>
		);
	}
}

function mapStateToProps(state) {
	return {
		userToken: state.ola && state.ola.auth.userCredentials,
		myRides: state.ola && state.ola.myRides,
		bookingStatus: state.ola && state.ola.chooseRide.bookingStatus,
		cancelReasons: state.ola && state.ola.cancelRides.cancelReasons,
		appToken: state.appToken.token
	};
}

export default connect(mapStateToProps)(RideList);
