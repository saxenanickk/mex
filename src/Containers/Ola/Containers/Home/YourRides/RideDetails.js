import React, { Component } from "react";
import {
	View,
	TouchableOpacity,
	Dimensions,
	Image,
	ScrollView,
	FlatList,
	TouchableWithoutFeedback,
	SafeAreaView
} from "react-native";
import { WebView } from "react-native-webview";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import {
	ProgressScreen,
	Hr,
	Icon,
	StarRating,
	DialogModal,
	Seperator
} from "../../../../../Components";
import { connect } from "react-redux";
import { APPLYCOUPON } from "../../../Assets/Img/Image";
import SimpleLineIcon from "react-native-vector-icons/SimpleLineIcons";
import EntypoIcon from "react-native-vector-icons/Entypo";
import IonIcon from "react-native-vector-icons/Ionicons";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
import { olaCancelRide, olaRequestSingleRide } from "./Saga";
import I18n from "../../../Assets/Strings/i18n";
import {
	MINISELECTED,
	PRIMESELECTED,
	SEDANSELECTED,
	SUVSELECTED,
	AUTOSELECTED,
	ERICKSELECTED,
	KAALIPEELISELECTED,
	LUXSELECTED,
	MICROSELECTED,
	DRIVER,
	CANCELLED,
	SPEEDMETER,
	LOCATIONPICKUP,
	LOCATIONDROP
} from "../../../Assets/Img/Image";
import {
	OlaTextRegular,
	OlaTextBold,
	OlaTextMedium
} from "../../../Components/OlaText";
import { rideDetailStyles as styles } from "./style";
import { _24to12 } from "../../../../../utils/dateUtils";
const carImages = {
	mini: MINISELECTED,
	prime: PRIMESELECTED,
	sedan: SEDANSELECTED,
	suv: SUVSELECTED,
	auto: AUTOSELECTED,
	erick: ERICKSELECTED,
	kp: KAALIPEELISELECTED,
	lux: LUXSELECTED,
	micro: MICROSELECTED,
	exec: PRIMESELECTED,
	prime_play: SEDANSELECTED
};

const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / (height * 0.25);
const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
class RideDetailsView extends Component {
	constructor(props) {
		super(props);
		const latitudeDelta =
			props && props.selectedCab && props.selectedCab.drop_lat
				? props.selectedCab.pickup_lat > props.selectedCab.drop_lat
					? props.selectedCab.drop_lat / (props.selectedCab.pickup_lat * 100)
					: props.selectedCab.pickup_lat / (props.selectedCab.drop_lat * 100)
				: 0.01;
		const longitudeDelta = latitudeDelta / ASPECT_RATIO;

		this.state = {
			isLoading: false,
			isLiteMode: false,
			region: {
				latitude: props.selectedCab.pickup_lat,
				longitude: props.selectedCab.pickup_lng,
				latitudeDelta: LATITUDE_DELTA,
				longitudeDelta: LONGITUDE_DELTA
			},
			showCancelReasons: false,
			selectedReason: "",
			showSupport: false
		};
		this.coord = [];
		this.isMapUpdated = true;
		this.closeModalAndNavigateScreen = this.closeModalAndNavigateScreen.bind(
			this
		);
	}

	componentDidMount() {
		this.props.dispatch(
			olaRequestSingleRide({
				ride: {
					access_token: this.props.userToken,
					request_id: this.props.myRides.selectedBookingId,
					appToken: this.props.appToken
				}
			})
		);
		this.setState({
			isLoading: true
		});
	}

	componentWillReceiveProps = nextProps => {
		console.log(nextProps, "component will receive props");
		if (
			nextProps.myRides &&
			nextProps.myRides.rideDetail &&
			nextProps.myRides.rideDetail !== this.props.myRides.rideDetail
		) {
			console.log("fit to coordinates called");
			this.coord = [
				{
					latitude:
						nextProps.myRides.rideDetail &&
						nextProps.myRides.rideDetail.pickup_location
							? nextProps.myRides.rideDetail.pickup_location.lat
							: nextProps.myRides.rideDetail.pickup_lat,
					longitude:
						nextProps.myRides.rideDetail &&
						nextProps.myRides.rideDetail.pickup_location
							? nextProps.myRides.rideDetail.pickup_location.lng
							: nextProps.myRides.rideDetail.pickup_lng
				},
				{
					latitude:
						nextProps.myRides.rideDetail &&
						nextProps.myRides.rideDetail.drop_location
							? nextProps.myRides.rideDetail.drop_location.lat
							: nextProps.myRides.rideDetail.drop_lat,
					longitude:
						nextProps.myRides.rideDetail &&
						nextProps.myRides.rideDetail.drop_location
							? nextProps.myRides.rideDetail.drop_location.lng
							: nextProps.myRides.rideDetail.drop_lng
				}
			];
			this.setState({ isLiteMode: true });
		}
	};

	closeModalAndNavigateScreen() {
		this.props.closeModal();
		this.props.changeScreen("TrackRide", {});
	}

	billDetails = () => {
		return this.props.myRides.rideDetail.fare_breakup.map((elem, index) => {
			return elem.value !== 0 ? (
				<View style={styles.billDetailInnerView} key={index.toString()}>
					<OlaTextRegular style={styles.fareText}>
						{elem.display_text}
					</OlaTextRegular>
					<OlaTextRegular style={styles.fareText}>{`₹ ${
						elem.value
					}`}</OlaTextRegular>
				</View>
			) : null;
		});
	};

	itineraryDetails() {
		return this.props.myRides.rideDetail.payment_details.map((elem, index) => (
			<View style={styles.billDetailInnerView} key={index.toString()}>
				<OlaTextRegular style={styles.fareText}>
					{elem.display_text}
				</OlaTextRegular>
				<OlaTextRegular style={styles.fareText}>{`₹ ${
					elem.value
				}`}</OlaTextRegular>
			</View>
		));
	}

	renderCabImage() {
		try {
			if (
				this.props.myRides &&
				this.props.myRides.rideDetail &&
				this.props.myRides.rideDetail.cab_details &&
				this.props.myRides.rideDetail.cab_details.cab_type
			) {
				return (
					<Image
						source={
							carImages[this.props.myRides.rideDetail.cab_details.cab_type]
						}
						style={styles.allotmentPendingDriverImage}
					/>
				);
			} else {
				return null;
			}
		} catch (error) {
			return null;
		}
	}

	getCancelReasons() {
		let data = [];
		try {
			let reasons = this.props.cancelReasons.cancel_reasons[
				this.props.myRides.rideDetail.cab_details.cab_type
			];
			if (reasons !== null && reasons !== undefined) {
				return reasons;
			} else {
				throw new Error("Reasons not found");
			}
		} catch (error) {
			data[0] = "Driver denied to go to destination";
			data[1] = "Driver denied to come to pickup";
			data[2] = "Expected a shorter wait time";
			data[3] = "Unable to contact driver";
			data[4] = "My reason is not listed";
			return data;
		}
	}
	_renderFromAndToAddress = () => {
		return (
			<View style={styles.fromToAddressView}>
				<View style={styles.fromToTime}>
					<OlaTextRegular style={styles.addressText}>
						{_24to12(new Date(this.props.selectedCab.epoch_booking_time))}
					</OlaTextRegular>
					{this.props.myRides.dropAddress.length > 0 &&
					this.props.myRides.rideDetail &&
					this.props.myRides.rideDetail.trip_info ? (
						<OlaTextRegular style={styles.addressText}>
							{_24to12(
								new Date(
									this.props.selectedCab.epoch_booking_time +
										this.props.myRides.rideDetail.trip_info.trip_time.value *
											60000
								)
							)}
						</OlaTextRegular>
					) : null}
				</View>
				<View style={styles.dotView}>
					<EntypoIcon name={"dot-single"} size={15} color={"#54a624"} />
					{this.props.myRides.dropAddress.length > 0 ? (
						<React.Fragment>
							<Seperator height={height / 20} width={1} color={"#ececec"} />
							<EntypoIcon name={"dot-single"} size={15} color={"#ff665a"} />
						</React.Fragment>
					) : null}
				</View>
				<View style={[styles.fromToAddressText]}>
					<OlaTextRegular numberOfLines={2} style={styles.addressText}>
						{this.props.myRides.pickupAddress}
					</OlaTextRegular>
					{this.props.myRides.dropAddress.length > 0 ? (
						<OlaTextRegular numberOfLines={2} style={styles.addressText}>
							{this.props.myRides.dropAddress}
						</OlaTextRegular>
					) : null}
				</View>
			</View>
		);
	};

	rideStatus(bookingStatus) {
		switch (bookingStatus) {
			case "CALL_DRIVER":
			case "CLIENT_LOCATED":
			case "IN_PROGRESS":
				return (
					<View>
						<View style={styles.bookingStatusView}>
							<Image
								source={{ uri: this.props.myRides.rideDetail.driver_image_url }}
								style={styles.bookingStatusImage}
							/>
							<View style={styles.driverNameView}>
								<OlaTextBold style={styles.driverNameText}>
									{this.props.myRides.rideDetail.driver_name}
								</OlaTextBold>
							</View>
						</View>
						<Seperator height={1} width={width} color={"#ececec"} />
						<View style={styles.bookingStatusView}>
							{this.renderCabImage()}
							<OlaTextBold style={styles.driverNameView}>
								{this.props.myRides.rideDetail &&
									this.props.myRides.rideDetail.cab_details &&
									this.props.myRides.rideDetail.cab_details.cab_type &&
									this.props.myRides.rideDetail.cab_details.cab_type !== "" &&
									(this.props.myRides.rideDetail.cab_details.cab_type ===
									"share"
										? `${this.props.myRides.rideDetail.cab_details.cab_type
												.toUpperCase()
												.replace(/[^A-Z0-9]+/gi, " ")} ${"\u2022"} ${
												this.props.myRides.rideDetail.vehicle_color
										  } ${this.props.myRides.rideDetail.vehicle_type}`
										: `${this.props.myRides.rideDetail.cab_details.cab_type
												.toUpperCase()
												.replace(/[^A-Z0-9]+/gi, " ")} ${
												this.props.myRides.rideDetail.cab_details.car_color
													? `${"\u2022"} ${
															this.props.myRides.rideDetail.cab_details
																.car_color
													  }`
													: ""
										  } ${this.props.myRides.rideDetail.cab_details.car_model ||
												""}`)}
							</OlaTextBold>
						</View>
						<Seperator height={1} width={width} color={"#ececec"} />
						{this._renderFromAndToAddress()}
					</View>
				);
			case "COMPLETED":
				return (
					<View>
						<View style={styles.driverNameView}>
							<Image
								source={{ uri: this.props.myRides.rideDetail.driver_image_url }}
								style={styles.driverImage}
							/>
							<View style={styles.driverText}>
								<OlaTextMedium style={styles.driverNameText}>
									{this.props.myRides.rideDetail.driver_name}
								</OlaTextMedium>
								<OlaTextRegular>
									{/* You rated{"   "} */}
									<StarRating
										numStars={this.props.myRides.rideDetail.driver_rating}
									/>
								</OlaTextRegular>
							</View>
						</View>
						<Seperator height={1} width={width} color={"#ececec"} />
						<View style={styles.bookingStatusView}>
							<Image source={this.props.image} style={styles.driverImage} />
							<OlaTextMedium style={styles.driverText}>
								{this.props.selectedCab.category === "share"
									? `${this.props.selectedCab.category
											.toUpperCase()
											.replace(/[^A-Z0-9]+/gi, " ")} ${"\u2022"} ${
											this.props.myRides.rideDetail.vehicle_color
									  } ${this.props.myRides.rideDetail.vehicle_type}`
									: `${this.props.selectedCab.category
											.toUpperCase()
											.replace(/[^A-Z0-9]+/gi, " ")} ${
											this.props.myRides.rideDetail.cab_details.car_color
												? `${"\u2022"} ${
														this.props.myRides.rideDetail.cab_details.car_color
												  }`
												: ""
									  } ${this.props.myRides.rideDetail.cab_details.car_model ||
											""}`}
							</OlaTextMedium>
						</View>
						<Seperator height={1} width={width} color={"#ececec"} />
						<View style={styles.bookingStatusView}>
							<Image
								source={SPEEDMETER}
								style={styles.driverImage}
								resizeMode={"contain"}
							/>
							<OlaTextMedium style={styles.tripAmountTextFirst}>
								{/* todo: add check for price details for share ride */}₹{" "}
								{this.props.myRides.rideDetail.trip_info.amount}
							</OlaTextMedium>
							<OlaTextMedium style={styles.tripAmountText}>
								{`${this.props.myRides.rideDetail.trip_info.distance.value.toString()} km`}
							</OlaTextMedium>
							<OlaTextMedium style={styles.tripAmountText}>
								{`${this.props.myRides.rideDetail.trip_info.trip_time.value.toString()} min`}
							</OlaTextMedium>
						</View>
						<Seperator height={1} width={width} color={"#ececec"} />
						{this._renderFromAndToAddress()}
						<Seperator height={1} width={width} color={"#ececec"} />
						<View style={styles.billDetailView}>
							<OlaTextMedium style={styles.driverNameText}>
								{`${I18n.t("bill_details")}`}
							</OlaTextMedium>
							{this.billDetails()}
						</View>
						<Seperator height={1} width={width} color={"#ececec"} />
						<View style={styles.billDetailView}>
							<OlaTextMedium style={styles.driverNameText}>
								{`${I18n.t("payment_details")}`}
							</OlaTextMedium>
							{this.itineraryDetails()}
						</View>
					</View>
				);
			case "BOOKING_CANCELLED":
				return (
					<View>
						<View
							style={[
								styles.bookingStatusView,
								{ justifyContent: "space-between" }
							]}>
							<Image
								source={DRIVER}
								style={styles.driverImage}
								resizeMode={"contain"}
							/>
							<Image
								source={CANCELLED}
								style={styles.driverImage}
								resizeMode={"contain"}
							/>
						</View>
						<Seperator height={1} width={width} color={"#ececec"} />

						<View style={styles.bookingStatusView}>
							<Image
								source={this.props.image}
								style={styles.driverImage}
								resizeMode={"contain"}
							/>
							<OlaTextMedium style={styles.textLeftPadding}>
								{this.props.selectedCab.category === "share"
									? `${this.props.selectedCab.category
											.toUpperCase()
											.replace(/[^A-Z0-9]+/gi, " ")} ${"\u2022"} ${
											this.props.myRides.rideDetail.vehicle_color
									  } ${this.props.myRides.rideDetail.vehicle_type}`
									: `${this.props.selectedCab.category
											.toUpperCase()
											.replace(/[^A-Z0-9]+/gi, " ")} ${
											this.props.myRides.rideDetail.cab_details.car_color
												? `${"\u2022"} ${
														this.props.myRides.rideDetail.cab_details.car_color
												  }`
												: ""
									  } ${this.props.myRides.rideDetail.cab_details.car_model ||
											""}`}
							</OlaTextMedium>
						</View>
						<Seperator height={1} width={width} color={"#ececec"} />
						<View style={styles.bookingStatusView}>
							<Image
								source={SPEEDMETER}
								style={styles.driverImage}
								resizeMode={"contain"}
							/>
							<OlaTextMedium style={styles.textLeftPadding}>
								{"₹ 0"}
							</OlaTextMedium>
							<OlaTextMedium style={styles.textLeftPadding}>
								{"0 km"}
							</OlaTextMedium>
							<OlaTextMedium style={styles.textLeftPadding}>
								{"0 min"}
							</OlaTextMedium>
						</View>
						<Seperator height={1} width={width} color={"#ececec"} />
						{this._renderFromAndToAddress()}
					</View>
				);
			case "ALLOTMENT_FAILED":
				return (
					<OlaTextRegular>{`${I18n.t("allotment_failed")}`}</OlaTextRegular>
				);
			case "ALLOTMENT_PENDING":
				return (
					<View>
						<View style={styles.allotmentPendingView}>
							{/* Add driver image */}
							<Image
								source={DRIVER}
								style={styles.allotmentPendingDriverImage}
							/>
							<OlaTextBold style={styles.rideScheduledText}>
								{`${I18n.t("ride_scheduled")}`}
							</OlaTextBold>
						</View>
						<Seperator height={1} width={width} color={"#ececec"} />
						<View style={styles.allotmentPendingView}>
							{this.renderCabImage()}

							<View style={styles.leftPadding}>
								<OlaTextBold style={styles.blackText}>
									{this.props.myRides.rideDetail.cab_details &&
										this.props.myRides.rideDetail.cab_details.cab_type &&
										this.props.myRides.rideDetail.cab_details.cab_type !== "" &&
										this.props.myRides.rideDetail.cab_details.cab_type
											.charAt(0)
											.toUpperCase() +
											this.props.myRides.rideDetail.cab_details.cab_type.slice(
												1
											)}
									{this.props.myRides.rideDetail.drop_lat === 0
										? " Rental"
										: ""}
								</OlaTextBold>
								<OlaTextRegular>{`${I18n.t("details_shared")} 10 ${I18n.t(
									"min_before_pickup"
								)}`}</OlaTextRegular>
							</View>
						</View>
						<Seperator height={1} width={width} color={"#ececec"} />
						{this._renderFromAndToAddress()}
					</View>
				);
			default:
				return null;
		}
	}

	_renderApplyCoupon = isDisabled => (
		<TouchableOpacity
			style={
				isDisabled ? styles.applyCouponButtonOne : styles.applyCouponButtonTwo
			}
			disabled={isDisabled}>
			<Image style={styles.applyCouponImage} source={APPLYCOUPON} />
			<OlaTextRegular style={styles.useCouponText}>
				{"USE COUPON"}
			</OlaTextRegular>
		</TouchableOpacity>
	);

	_renderMailInvoice = isDisabled => (
		<TouchableOpacity
			disabled={isDisabled}
			style={
				isDisabled ? styles.applyCouponButtonOne : styles.applyCouponButtonTwo
			}>
			<EntypoIcon name={"mail"} padding={5} size={15} />
			<OlaTextRegular style={styles.useCouponText}>{`${I18n.t(
				"mail_invoice"
			)}`}</OlaTextRegular>
		</TouchableOpacity>
	);

	_renderSupportIcon = () => (
		<TouchableOpacity
			style={styles.supportIconButton}
			onPress={() => {
				this.isMapUpdated = true;
				this.setState({ showSupport: true, isLiteMode: false });
			}}>
			<SimpleLineIcon name={"support"} size={15} />
			<OlaTextRegular style={styles.useCouponText}>{`${I18n.t(
				"support_C"
			)}`}</OlaTextRegular>
		</TouchableOpacity>
	);

	_renderSupport = () => {
		let { booking_id } = this.props.myRides.rideDetail;
		let booking_ref_no = booking_id.slice(3, booking_id.length);
		let brand = booking_id.slice(0, 3) === "CRN" ? "ola_cabs" : "ola_share";
		let {
			pickup_lat,
			pickup_lng,
			drop_lat,
			drop_lng
		} = this.props.myRides.rideDetail;
		let current_lat = drop_lat === 0 ? pickup_lat : drop_lat;
		let current_lng = drop_lng === 0 ? pickup_lng : drop_lng;
		return (
			<View style={styles.fullFlex}>
				<WebView
					source={{
						uri: "https://help.olacabs.com/self_serve/v1?page=myrides",
						headers: {
							"Content-Type": "application/x-www-form-urlencoded"
						},
						body: `booking_ref_no=${booking_ref_no}&brand=${brand}&current_lat_lng=${current_lat}%2C+${current_lng}&authorization=Bearer%20${
							this.props.userToken
						}&X-APP-TOKEN=dea39464c59e4589a7d10119826f8870`,
						method: "POST"
					}}
				/>
			</View>
		);
	};

	_renderTrackRide = isDisabled => (
		<TouchableOpacity
			disabled={isDisabled}
			style={
				isDisabled ? styles.applyCouponButtonOne : styles.applyCouponButtonTwo
			}
			onPress={this.closeModalAndNavigateScreen}>
			<MaterialCommunityIcon name={"target"} size={15} />
			<OlaTextRegular style={styles.useCouponText}>{`${I18n.t(
				"track_ride"
			)}`}</OlaTextRegular>
		</TouchableOpacity>
	);

	_renderCancel = () => (
		<TouchableOpacity
			style={styles.supportIconButton}
			onPress={() => this.setState({ showCancelReasons: true })}>
			<IonIcon name={"ios-close-circle-outline"} padding={5} size={15} />
			<OlaTextRegular style={styles.useCouponText}>{`${I18n.t(
				"cancel_C"
			)}`}</OlaTextRegular>
		</TouchableOpacity>
	);

	cancelRide = reason => {
		this.setState({ showCancelReasons: false });
		console.log(this.props.userToken);
		this.props.dispatch(
			olaCancelRide({
				access_token: this.props.userToken,
				request_id: this.props.myRides.rideDetail.booking_id,
				reason: reason,
				pickup_lat: this.props.myRides.rideDetail.pickup_lat,
				pickup_lng: this.props.myRides.rideDetail.pickup_lng,
				drop_lat: this.props.myRides.rideDetail.drop_lat,
				drop_lng: this.props.myRides.rideDetail.drop_lng,
				appToken: this.props.appToken
			})
		);
	};

	renderCancelReasons = data => {
		console.log("data to cancel is", data);
		const iconName =
			this.state.selectedReason === data.item ? "dot-circle-o" : "circle-thin";
		const iconColor =
			this.state.selectedReason === data.item ? "#3A40FF" : "#000";
		return (
			<TouchableOpacity
				style={styles.cancelReasonButton}
				onPress={() => this.setState({ selectedReason: data.item })}>
				<OlaTextRegular>{data.item}</OlaTextRegular>
				<Icon
					iconType="font_awesome"
					iconName={iconName}
					iconSize={width / 20}
					iconColor={iconColor}
				/>
			</TouchableOpacity>
		);
	};

	rideActions(bookingStatus) {
		switch (bookingStatus) {
			case "ALLOTMENT_PENDING":
				return (
					<View style={styles.rideActionsView}>
						{this._renderApplyCoupon(false)}
						{this._renderTrackRide(true)}
						{this._renderCancel()}
						{this._renderSupportIcon()}
					</View>
				);
			case "CALL_DRIVER":
			case "CLIENT_LOCATED":
				return (
					<View style={styles.rideActionsView}>
						{this._renderApplyCoupon(false)}
						{this._renderTrackRide(false)}
						{this._renderCancel()}
						{this._renderSupportIcon()}
					</View>
				);
			case "IN_PROGRESS":
				return (
					<View style={styles.rideActionsView}>
						{this._renderApplyCoupon(false)}
						{this._renderMailInvoice(false)}
						{this._renderTrackRide(false)}
						{this._renderSupportIcon()}
					</View>
				);
			case "COMPLETED":
			case "ALLOTMENT_FAILED":
				return (
					<View style={styles.rideActionsView}>
						{this._renderApplyCoupon(true)}
						{this._renderMailInvoice(false)}
						{this._renderSupportIcon()}
					</View>
				);
			case "BOOKING_CANCELLED":
				return (
					<View style={styles.rideActionsView}>
						{this._renderApplyCoupon(true)}
						{this._renderMailInvoice(true)}
						{this._renderSupportIcon()}
					</View>
				);
		}
	}

	onCloseModal = () => {
		if (this.state.showSupport) {
			this.setState({
				showSupport: false
			});
		} else {
			this.props.navigation.goBack();
		}
	};

	componentDidUpdate = (prevProps, prevState) => {
		if (
			prevState.showSupport === true &&
			this.state.showSupport === false &&
			this.state.isLiteMode === false
		) {
			this.setState({ isLiteMode: true });
		}
	};
	render() {
		console.log("props in ride details are", this.props);
		return (
			<SafeAreaView style={{ flex: 1 }}>
				<View style={styles.whiteView}>
					<View style={styles.header}>
						<TouchableOpacity onPress={() => this.onCloseModal()}>
							<Icon
								iconType={"material"}
								iconName={"arrow-back"}
								iconColor={"#000000"}
								iconSize={width / 15}
							/>
						</TouchableOpacity>
						<View style={styles.timeView}>
							<OlaTextMedium style={styles.timeText}>
								{this.props.time}
							</OlaTextMedium>
							<OlaTextRegular style={styles.bookingIdText}>
								{this.props.selectedCab.booking_id}
							</OlaTextRegular>
						</View>
					</View>
					{!this.props.myRides.isRideDetailLoading &&
					!this.state.showSupport ? (
						<ScrollView style={styles.fullFlex}>
							<MapView
								provider={PROVIDER_GOOGLE}
								liteMode={this.state.isLiteMode}
								style={styles.mapView}
								ref={ref => (this.mapRef = ref)}
								onLayout={() => {
									if (this.mapRef && this.isMapUpdated) {
										this.mapRef.fitToCoordinates(this.coord);
										this.isMapUpdated = false;
									}
								}}
								initialRegion={this.state.region}>
								<Marker
									image={LOCATIONDROP}
									width={width / 20}
									height={height / 20}
									coordinate={{
										latitude:
											this.props.myRides.rideDetail &&
											this.props.myRides.rideDetail.drop_location
												? this.props.myRides.rideDetail.drop_location.lat
												: this.props.myRides.rideDetail.drop_lat,
										longitude:
											this.props.myRides.rideDetail &&
											this.props.myRides.rideDetail.drop_location
												? this.props.myRides.rideDetail.drop_location.lng
												: this.props.myRides.rideDetail.drop_lng
									}}
								/>
								<Marker
									image={LOCATIONPICKUP}
									width={width / 20}
									height={height / 20}
									coordinate={{
										latitude:
											this.props.myRides.rideDetail &&
											this.props.myRides.rideDetail.pickup_location
												? this.props.myRides.rideDetail.pickup_location.lat
												: this.props.myRides.rideDetail.pickup_lat,
										longitude:
											this.props.myRides.rideDetail &&
											this.props.myRides.rideDetail.pickup_location
												? this.props.myRides.rideDetail.pickup_location.lng
												: this.props.myRides.rideDetail.pickup_lng
									}}
								/>
							</MapView>
							{this.rideStatus(this.props.myRides.rideDetail.booking_status)}
						</ScrollView>
					) : null}
					{!this.props.myRides.isRideDetailLoading &&
						!this.state.showSupport &&
						this.rideActions(this.props.myRides.rideDetail.booking_status)}
					{!this.props.myRides.isRideDetailLoading &&
					this.state.showCancelReasons ? (
						<DialogModal
							visible={this.state.showCancelReasons}
							onRequestClose={() => this.setState({ showCancelReasons: false })}
							dialogModalBackgroundStyle={styles.dialogModalBackgroundStyle}>
							<View style={styles.dialogModalInnerView}>
								<View>
									<FlatList
										style={{ flexGrow: 0 }}
										data={this.getCancelReasons()}
										ListHeaderComponent={() => (
											<View>
												<OlaTextBold style={styles.cancelRideText}>
													{`${I18n.t("cancel_ride")}`}
												</OlaTextBold>
												<View style={styles.greyBoundary} />
											</View>
										)}
										ListFooterComponent={() => (
											<View style={styles.centeredRow}>
												<TouchableWithoutFeedback
													onPress={() =>
														this.setState({ showCancelReasons: false })
													}>
													<View style={styles.dontCancelView}>
														<OlaTextMedium style={styles.dontCancelText}>
															{`${I18n.t("dont_cancel")}`}
														</OlaTextMedium>
													</View>
												</TouchableWithoutFeedback>
												<TouchableWithoutFeedback
													disabled={this.state.selectedReason.length === 0}
													onPress={() => {
														this.cancelRide(this.state.selectedReason);
														this.setState({ showCancelReasons: false });
													}}>
													<View
														style={
															this.state.selectedReason.length === 0
																? styles.cancelButtonOne
																: styles.cancelButtonTwo
														}>
														<OlaTextMedium style={styles.dontCancelText}>
															{`${I18n.t("cancel_C")}`}
														</OlaTextMedium>
													</View>
												</TouchableWithoutFeedback>
											</View>
										)}
										keyExtractor={(item, index) => index.toString()}
										renderItem={this.renderCancelReasons}
									/>
								</View>
							</View>
						</DialogModal>
					) : null}
					{!this.props.myRides.isRideDetailLoading &&
						this.state.showSupport &&
						this._renderSupport()}
					{this.props.myRides.isRideDetailLoading && <ProgressScreen />}
				</View>
			</SafeAreaView>
		);
	}
}

class RideDetails extends Component {
	render() {
		return <RideDetailsView {...this.props} {...this.props.route.params} />;
	}
}

function mapStateToProps(state) {
	return {
		cancelReasons: state.ola && state.ola.cancelRides.cancelReasons,
		userToken: state.ola && state.ola.auth.userCredentials,
		bookingStatus: state.ola && state.ola.chooseRide.bookingStatus,
		myRides: state.ola && state.ola.myRides,
		appToken: state.appToken.token
	};
}

export default connect(mapStateToProps)(RideDetails);
