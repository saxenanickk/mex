import {
	OLA_REQUEST_RIDES,
	OLA_RECEIVE_RIDES,
	OLA_SINGLE_RIDE_REQUEST,
	OLA_SINGLE_RIDE_RECEIVED,
	RESET_OLA_REQUESTED_RIDES,
	REPLACE_FIRST_THREE_RIDES,
	OLA_RECEIVE_RIDES_FAILED,
	OLA_SAVE_BOOKING_ID,
	OLA_SINGLE_RIDE_DETAIL_ERROR
} from "./Saga";

const initialState = {
	bookings: [],
	isLoading: false,
	error: false,
	rideDetail: null,
	isRideDetailLoading: false,
	pickupAddress: "", // Address in text
	dropAddress: "", // Address in text
	selectedBookingId: null,
	olaSingleRideDetailError: false
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case OLA_REQUEST_RIDES:
			return {
				...state,
				isLoading: true,
				error: false
			};
		case OLA_RECEIVE_RIDES:
			return {
				...state,
				bookings: [...state.bookings, ...action.payload],
				isLoading: false
			};
		case OLA_RECEIVE_RIDES_FAILED:
			return {
				...state,
				error: true,
				isLoading: false
			};
		case RESET_OLA_REQUESTED_RIDES:
			return {
				...state,
				bookings: []
			};
		case OLA_SINGLE_RIDE_REQUEST:
			return {
				...state,
				isRideDetailLoading: true,
				rideDetail: null
			};
		case OLA_SINGLE_RIDE_RECEIVED:
			return {
				...state,
				isRideDetailLoading: false,
				rideDetail: action.payload.rideDetails,
				pickupAddress: action.payload.pickupAddress,
				dropAddress: action.payload.dropAddress
			};
		case REPLACE_FIRST_THREE_RIDES:
			return {
				...state,
				bookings: [...action.payload, ...state.bookings.slice(3)]
			};
		case OLA_SAVE_BOOKING_ID:
			return {
				...state,
				selectedBookingId: action.payload
			};
		case OLA_SINGLE_RIDE_DETAIL_ERROR:
			return {
				...state,
				olaSingleRideDetailError: action.payload
			};
		default:
			return state;
	}
};
