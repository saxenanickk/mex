import React from "react";
import RideList from "./RideList";
import RideDetails from "./RideDetails";
import { Stack } from "../../../../../utils/Navigators";

const YourRides = () => (
	<Stack.Navigator headerMode={"none"} mode={"modal"}>
		<Stack.Screen name={"RideList"} component={RideList} />
		<Stack.Screen name={"RideDetails"} component={RideDetails} />
	</Stack.Navigator>
);

export default YourRides;
