import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const indexStyles = StyleSheet.create({
	header: {
		width: width,
		height: height / 12,
		flexDirection: "row",
		alignItems: "center",
		paddingLeft: width / 35,
		elevation: 5,
		backgroundColor: "#ffffff",
		borderColor: "#ffffff"
	},
	button: {
		justifyContent: "center",
		alignItems: "center",
		height: height / 18,
		borderRadius: width / 20,
		paddingHorizontal: width / 35
	},
	buttonActive: {
		backgroundColor: "#bfec30"
	},
	renderRideButton: {
		width: width,
		backgroundColor: "#fff",
		paddingHorizontal: width / 42,
		height: height / 9,
		justifyContent: "center",
		borderBottomWidth: 1,
		borderBottomColor: "#ececec"
	},
	spaceBetweenRow: {
		flexDirection: "row",
		justifyContent: "space-between",
		height: height / 9,
		alignItems: "center"
	},
	flexStartRow: {
		flexDirection: "row",
		justifyContent: "flex-start"
	},
	carInfo: {
		flexDirection: "row",
		height: height / 9,
		alignItems: "center"
	},
	cabImage: {
		marginLeft: width / 35,
		height: height / 9,
		justifyContent: "center"
	},
	blackText: {
		color: "#000000",
		fontSize: height / 54
	},
	regularText: {
		color: "#000000",
		fontSize: height / 65,
		marginTop: height / 450
	},
	bookingStatusText: {
		color: "#54a624",
		fontSize: height / 60,
		marginTop: height / 250
	},
	cancelledView: {
		marginBottom: 10,
		alignItems: "flex-end"
	},
	cancelledImage: {
		width: 45,
		height: 45,
		zIndex: 2,
		marginBottom: -22
	},
	driverImage: {
		width: 40,
		height: 40,
		zIndex: 1
	},
	fullFlex: {
		flex: 1,
		backgroundColor: "#ffffff"
	},
	yourRideText: {
		marginLeft: width / 16.7,
		fontSize: height / 45.71,
		color: "#000000"
	},
	fetchFailView: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		height: height - height / 12
	},
	fetchFailText: {
		fontSize: width / 20
	},
	tryAgainButton: {
		backgroundColor: "#000",
		marginTop: width / 30,
		paddingHorizontal: width / 30,
		paddingVertical: width / 50,
		borderRadius: 5
	},
	tryAgainText: {
		color: "#bfec30",
		fontSize: height / 40
	},
	categoryImage: { width: width / 10, height: width / 10 },
	separatorStyle: { marginVertical: height / 120, marginLeft: width / 7 }
});

export const rideDetailStyles = StyleSheet.create({
	header: {
		width: width,
		height: height / 12,
		flexDirection: "row",
		alignItems: "center",
		paddingLeft: width / 35,
		elevation: 11,
		backgroundColor: "#ffffff",
		borderColor: "#ffffff"
	},
	billDetailView: {
		justifyContent: "space-between",
		flexDirection: "row",
		paddingVertical: width / 55,
		padding: width / 25
	},
	fromToAddressView: {
		paddingVertical: width / 15,
		flex: 1,
		flexDirection: "row",
		justifyContent: "space-around",
		paddingHorizontal: width / 25
	},
	bookingStatusView: {
		flexDirection: "row",
		justifyContent: "flex-start",
		padding: width / 25,
		alignItems: "center"
	},
	driverImage: {
		width: width / 8,
		height: width / 8,
		borderRadius: width / 16
	},
	driverText: {
		marginLeft: width / 30
	},
	bookingStatusImage: { width: 45, height: 45 },
	driverNameView: {
		flexDirection: "row",
		paddingLeft: width / 35,
		paddingVertical: height / 50
	},
	driverNameText: {
		fontSize: height / 45,
		color: "#000"
	},
	tripAmountText: {
		paddingLeft: width / 10,
		fontSize: height / 45,
		color: "#000"
	},
	tripAmountTextFirst: {
		marginLeft: width / 30,
		fontSize: height / 45,
		color: "#000"
	},
	billDetailInnerView: {
		paddingVertical: height / 100,
		flexDirection: "row",
		width: width / 1.1,
		justifyContent: "space-between"
	},
	textLeftPadding: {
		paddingLeft: width / 35,
		fontSize: height / 55,
		color: "#000"
	},
	leftPadding: {
		paddingLeft: width / 35,
		fontSize: height / 55,
		color: "#000"
	},
	allotmentPendingView: {
		flexDirection: "row",
		alignItems: "center",
		padding: width / 20
	},
	allotmentPendingDriverImage: {
		width: width / 10,
		height: width / 10
	},
	rideScheduledText: {
		paddingLeft: width / 15,
		color: "#000"
	},
	blackText: {
		color: "#000"
	},
	applyCouponButtonOne: {
		alignItems: "center",
		padding: 5,
		opacity: 0.2
	},
	applyCouponButtonTwo: {
		alignItems: "center",
		padding: 5,
		opacity: 0.8
	},
	applyCouponImage: {
		width: width / 25,
		height: width / 25,
		padding: 5
	},
	useCouponText: { fontSize: 12, padding: 5 },
	supportIconButton: { alignItems: "center", padding: 5 },
	fullFlex: { flex: 1 },
	cancelReasonButton: {
		padding: 10,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	rideActionsView: {
		flexDirection: "row",
		borderTopWidth: 1,
		borderTopColor: "grey",
		alignItems: "center",
		justifyContent: "space-around"
	},
	whiteView: {
		backgroundColor: "#fff",
		flex: 1
	},
	timeView: {
		marginLeft: width / 35,
		justifyContent: "center"
	},
	timeText: {
		fontSize: height / 45,
		color: "#000000"
	},
	bookingIdText: {
		fontSize: height / 45
	},
	mapView: { height: height * 0.25 },
	dialogModalBackgroundStyle: {
		backgroundColor: "#000",
		opacity: 0.6
	},
	dialogModalInnerView: {
		top: height / 3.5,
		width: width / 1.5,
		borderColor: "#ffffff",
		borderRadius: width / 140,
		elevation: 5,
		left: width / 6,
		position: "absolute",
		justifyContent: "flex-start",
		alignItems: "center",
		backgroundColor: "#ffffff"
	},
	cancelRideText: {
		alignSelf: "center",
		padding: 10
	},
	greyBoundary: {
		borderBottomColor: "grey",
		borderBottomWidth: 1,
		marginHorizontal: width / 35
	},
	centeredRow: {
		flexDirection: "row",
		alignItems: "center"
	},
	dontCancelView: {
		borderLeftWidth: 0.5,
		borderColor: "#4f4f4f",
		justifyContent: "center",
		borderRadius: 0,
		alignItems: "center",
		width: width / 3,
		height: height / 15,
		backgroundColor: "#0f0f0f"
	},
	dontCancelText: {
		color: "#CCE500",
		fontSize: width / 30
	},
	cancelButtonOne: {
		borderLeftWidth: 0.5,
		borderColor: "#4f4f4f",
		justifyContent: "center",
		borderRadius: 0,
		alignItems: "center",
		width: width / 3,
		height: height / 15,
		backgroundColor: "#0f0f0f",
		opacity: 0
	},
	cancelButtonTwo: {
		borderLeftWidth: 0.5,
		borderColor: "#4f4f4f",
		justifyContent: "center",
		borderRadius: 0,
		alignItems: "center",
		width: width / 3,
		height: height / 15,
		backgroundColor: "#0f0f0f",
		opacity: 0.85
	},
	dotView: {
		alignItems: "center",
		paddingHorizontal: width / 20
	},
	fromToAddressText: {
		flex: 1,
		justifyContent: "space-between"
	},
	addressText: {
		fontSize: height / 65,
		color: "#000"
	},
	fareText: {
		fontSize: height / 55,
		color: "#000"
	},
	fromToTime: {
		height: height / 11.5,
		justifyContent: "space-between"
	}
});
