import React, { Component, Fragment } from "react";
import {
	View,
	Dimensions,
	Animated,
	Easing,
	TouchableOpacity,
	FlatList
} from "react-native";
import { Seperator, CheckBox } from "../../../../../Components";
import I18n from "../../../Assets/Strings/i18n";
import { OlaTextRegular, OlaTextBold } from "../../../Components/OlaText";
import { DisabledTextBox } from "../../../Components";
import { pickupDropComponentStyles as styles } from "./style";

const { width, height } = Dimensions.get("window");

export default class PickupDropComponent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			hotSpotVisibility: this.props.isHotSpotZone,
			selectedBox: true
		};
		this.pickupDropAnimatedValue = new Animated.Value(0);
	}

	fadeOutAnimation = () => {
		this.setState({ hotSpotVisibility: false, selectedBox: false });
		Animated.timing(this.pickupDropAnimatedValue, {
			toValue: 1,
			duration: 500,
			easing: Easing.linear
		}).start();
	};

	fadeInAnimation = () => {
		this.setState({
			hotSpotVisibility: this.props.isHotSpotZone,
			selectedBox: true
		});
		Animated.timing(this.pickupDropAnimatedValue, {
			toValue: 0,
			duration: 500,
			easing: Easing.linear
		}).start();
	};

	scrollToItem = item => {
		if (this.listRef && this.listRef !== undefined) {
			let index =
				this.props.hotspotZone &&
				this.props.hotspotZone.pickup_points &&
				this.props.hotspotZone.pickup_points.findIndex(
					row => row.id === item.id
				);
			this.listRef.scrollToOffset({
				animated: true,
				offset: (height / 18) * index
			});
		}
	};
	render() {
		const mainViewTop = this.pickupDropAnimatedValue.interpolate({
			inputRange: [0, 1],
			outputRange: [height / 11, height / 15]
		});
		const dropTop = this.props.dropSelected
			? 0
			: this.props.isHotSpotZone
			? this.props.hotspotZone.pickup_points.length >= 3
				? height / 10.4
				: this.props.hotspotZone.pickup_points.length == 2
				? height / 12.4
				: height / 31
			: 0;
		const dropBoxTop = this.pickupDropAnimatedValue.interpolate({
			inputRange: [0, 1],
			outputRange: [0, -height / 18]
		});
		const pickupSelected = this.pickupDropAnimatedValue.interpolate({
			inputRange: [0, 1],
			outputRange: [this.props.pickupSelected, false]
		});
		return (
			<Animated.View
				style={[
					{
						top: mainViewTop
					},
					styles.absolutePosition
				]}>
				{/* for hotspot zone*/}
				{!this.props.dropSelected &&
				this.props.isHotSpotZone &&
				this.state.hotSpotVisibility ? (
					<View style={styles.hotSpotZoneVisibility}>
						<View style={styles.hotSpotZoneVisibilityInner}>
							<View>
								<OlaTextRegular
									numberOfLines={1}
									style={styles.originAddressText}>
									{`${I18n.t("at")} ${this.props.originAddress}`}
								</OlaTextRegular>
								<OlaTextRegular style={styles.selectPickupText}>
									{I18n.t("select_pickup")}
								</OlaTextRegular>
							</View>
							<TouchableOpacity onPress={this.props.onHotspotPickupPress}>
								<OlaTextBold style={styles.changeText}>
									{I18n.t("change")}
								</OlaTextBold>
							</TouchableOpacity>
						</View>
						<Seperator
							width={width / 1.1}
							height={height / 975}
							color={"rgba(0,0,0,0.3)"}
						/>
						<FlatList
							data={this.props.hotspotZone.pickup_points}
							ref={ref => (this.listRef = ref)}
							style={{
								width: width / 1.1,
								height:
									this.props.hotspotZone.pickup_points.length >= 3
										? height / 9
										: null
							}}
							renderItem={({ item }) => {
								return (
									<TouchableOpacity
										onPress={() => this.props.onHotspotPickupItemPress(item)}
										style={styles.hotSpotZoneButton}>
										<CheckBox
											width={width / 25}
											height={width / 25}
											borderRadius={width / 50}
											checkColor={"#29B204"}
											value={
												this.props.hotspotZone.default_pickup_point_id ===
												item.id
											}
										/>
										<OlaTextRegular style={styles.hotSpotZoneButtonName}>
											{item.name}
										</OlaTextRegular>
									</TouchableOpacity>
								);
							}}
						/>
					</View>
				) : (
					<DisabledTextBox
						onPress={this.props.onPickupPress}
						disabledTextBoxStyle={
							this.props.dropSelected
								? styles.disabledTextBoxStyleOne
								: styles.disabledTextBoxStyleTwo
						}
						textButtonStyle={styles.disabledTextButtonStyle}
						textStyle={this.props.pickupTextStyle}
						text={this.props.pickupText}
						selected={this.props.pickupSelected && this.state.selectedBox}
						iconColor={"#54a624"}
						label={I18n.t("pickup_from")}
					/>
				)}
				{/* From Input */}

				{/* To Input */}
				{this.props.showDropBox ? (
					<DisabledTextBox
						onPress={this.props.onDropPress}
						disabledTextBoxStyle={[
							{
								top: dropBoxTop
							},
							this.props.dropSelected
								? styles.showDropBoxDropSelected
								: styles.showDropBoxUnSelected,
							styles.showDropBoxNormal
						]}
						textButtonStyle={styles.dropBoxText}
						textStyle={
							this.props.dropLocation && this.props.dropSelected
								? styles.showDropBoxTextOne
								: !this.props.dropLocation
								? styles.showDropBoxTextTwo
								: styles.showDropBoxTextThree
						}
						text={this.props.dropText}
						selected={this.props.dropSelected && this.state.selectedBox}
						iconColor={"#ff665a"}
						label={I18n.t("drop_at")}
					/>
				) : null}
			</Animated.View>
		);
	}
}
