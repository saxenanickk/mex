import {
	OLA_FADE,
	OLA_ORIGIN,
	OLA_PRODUCTS,
	OLA_DESTINATION,
	OLA_GET_PRODUCTS,
	OLA_USER_FAVOURITES,
	OLA_SAVE_HOTSPOT_ZONE,
	OLA_IS_HOTSPOT_ZONE,
	OLA_SAVE_SCALED_IMAGE
} from "./Saga";

const initialState = {
	products: [],
	fade: true,
	origin: {
		latitude: null,
		longitude: null,
		address: null
	},
	destination: {
		latitude: null,
		longitude: null,
		address: null
	},
	isProductsFetching: false,
	isProductsError: false,
	userFavourites: [],
	hotspotZone: {},
	isHotSpotZone: false,
	scaledImage: {}
};

// {
// 					latitude: 13.0950287,
// 					longitude: 77.6496671,
// 					address: "Kannuru, Bengaluru, Karnataka",
// 				},
export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case OLA_FADE:
			return {
				...state,
				fade: action.payload
			};
		case OLA_ORIGIN:
			return {
				...state,
				origin: action.payload
			};
		case OLA_DESTINATION:
			return {
				...state,
				destination: action.payload
			};
		case OLA_GET_PRODUCTS:
			return {
				...state,
				isProductsFetching: true
			};
		case OLA_PRODUCTS:
			return {
				...state,
				products: action.payload,
				isProductsFetching: false
			};
		case OLA_USER_FAVOURITES:
			return {
				...state,
				userFavourites: action.payload
			};
		case OLA_SAVE_HOTSPOT_ZONE:
			return {
				...state,
				hotspotZone: action.payload
			};
		case OLA_IS_HOTSPOT_ZONE:
			return {
				...state,
				isHotSpotZone: action.payload
			};
		case OLA_SAVE_SCALED_IMAGE:
			let obj = state.scaledImage;
			obj[action.payload.key] = action.payload.path;
			return {
				...state,
				scaledImage: obj
			};
		default:
			return state;
	}
};
