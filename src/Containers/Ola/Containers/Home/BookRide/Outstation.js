import React, { Component } from "react";
import {
	View,
	Image,
	TouchableWithoutFeedback,
	Dimensions,
	Animated,
	Easing
} from "react-native";
import I18n from "../../../Assets/Strings/i18n";
import { outstationStyles as styles } from "./style";
import {
	OUTSTATIONSELECTED,
	ADVANCEBOOKING,
	ONEWAYTRIPS,
	ROUNDTRIPS
} from "../../../Assets/Img/Image";
import { DotsLoader } from "../../../../../Components";
import {
	OlaTextRegular,
	OlaTextBold,
	OlaTextMedium
} from "../../../Components/OlaText";

const { width, height } = Dimensions.get("window");

class Outstation extends Component {
	constructor(props) {
		super(props);
		this.footerAnimatedValue = new Animated.Value(-height / 3.8);
	}

	fadeOutAnimation = () => {
		Animated.timing(this.footerAnimatedValue, {
			toValue: -height / 3.8,
			duration: 300,
			easing: Easing.linear
		}).start();
	};

	fadeInAnimation = () => {
		Animated.timing(this.footerAnimatedValue, {
			toValue: 0,
			duration: 300,
			easing: Easing.linear
		}).start();
	};

	componentDidMount = () => {
		setTimeout(() => {
			this.fadeInAnimation();
		}, 301);
	};

	componentWillUnmount = () => {
		this.fadeOutAnimation();
	};

	render() {
		let eta;
		this.props.products.map((data, key) => {
			if (data.id === "outstation") {
				eta = data.eta;
			}
		});
		return (
			<Animated.View
				style={[styles.bottomPane, { bottom: this.footerAnimatedValue }]}>
				<View>
					<View style={styles.rentalLogoView}>
						<Image style={styles.rentalLogo} source={OUTSTATIONSELECTED} />
					</View>
					<View style={styles.etaTextView}>
						{eta > 0 ? (
							<OlaTextRegular style={styles.etaText}>
								{eta} mins away
							</OlaTextRegular>
						) : (
							<DotsLoader size={5} color={"#e0e0e0"} />
						)}
						<View style={styles.rentCabView}>
							<OlaTextBold style={styles.description}>
								{I18n.t("rent_out_of_town_at_affordable_prices")}
							</OlaTextBold>
						</View>
					</View>
				</View>
				<View style={styles.features}>
					<View style={styles.feature}>
						<Image style={styles.advanceBookingImage} source={ADVANCEBOOKING} />
						<View>
							<OlaTextMedium style={styles.featureText}>
								{I18n.t("advance")}
							</OlaTextMedium>
							<OlaTextMedium style={styles.featureText}>
								{I18n.t("booking")}
							</OlaTextMedium>
						</View>
					</View>
					<View style={styles.feature}>
						<Image style={styles.oneWayImage} source={ONEWAYTRIPS} />
						<View>
							<OlaTextMedium style={styles.featureText}>
								{I18n.t("one_way")}
							</OlaTextMedium>
							<OlaTextMedium style={styles.featureText}>
								{I18n.t("trips")}
							</OlaTextMedium>
						</View>
					</View>
					<View style={styles.feature}>
						<Image style={styles.roundTripImage} source={ROUNDTRIPS} />
						<View>
							<OlaTextMedium style={styles.featureText}>
								{I18n.t("round")}
							</OlaTextMedium>
							<OlaTextMedium style={styles.featureText}>
								{I18n.t("trips")}
							</OlaTextMedium>
						</View>
					</View>
				</View>
				<TouchableWithoutFeedback
					onPress={() =>
						this.props.changeScreen("OutStation", { time: new Date() })
					}>
					<View style={styles.continueView}>
						<OlaTextMedium style={styles.continueText}>
							{I18n.t("out_continue")}
						</OlaTextMedium>
					</View>
				</TouchableWithoutFeedback>
			</Animated.View>
		);
	}
}

export default Outstation;
