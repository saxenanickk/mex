import React, { Component } from "react";
import {
	View,
	Dimensions,
	FlatList,
	TouchableWithoutFeedback,
	Image,
	Animated,
	Easing,
	TouchableOpacity
} from "react-native";
import { Seperator, DotsLoader } from "../../../../../Components";
import I18n from "../../../Assets/Strings/i18n";
import {
	AUTO,
	SHARE,
	MICRO,
	MINI,
	PRIME,
	SUV,
	LUX,
	OUTSTATION,
	RENTALS
} from "../../../Assets/Img/Image";
import { OlaTextRegular } from "../../../Components/OlaText";
import { olaProductStyles as styles } from "./style";
import goAppAnalytics from "../../../../../utils/GoAppAnalytics";

const { width, height } = Dimensions.get("window");

export default class OlaProducts extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedRide: 1,
			id: null,
			primeSelected: false
		};
		this.product = [];
		this.primeProduct = [];
		this.requestRide = this.requestRide.bind(this);
		this.onPressProduct = this.onPressProduct.bind(this);
		this.renderProductList = this.renderProductList.bind(this);
		this.requestRideLater = this.requestRideLater.bind(this);
		this.footerAnimatedValue = new Animated.Value(-height / 5.475);
	}

	fadeOutAnimation = () => {
		Animated.timing(this.footerAnimatedValue, {
			toValue:
				this.state.primeSelected === false ? -height / 5.475 : -height / 4.3,
			duration: 300,
			easing: Easing.linear
		}).start();
	};

	fadeInAnimation = () => {
		Animated.timing(this.footerAnimatedValue, {
			toValue: 0,
			duration: 300,
			easing: Easing.linear
		}).start();
	};

	requestRide() {
		this.props.callbackRequestRide({
			selectedRide: this.state.selectedRide,
			id: this.state.id
		});
	}

	requestRideLater() {
		this.props.callbackRequestRide({
			selectedRide: this.state.selectedRide,
			id: this.state.id,
			rideLater: true
		});
	}

	onPressProduct(item) {
		goAppAnalytics.trackWithProperties("ola-cab-selected", {
			cab: item.id
		});
		this.setState({
			selectedRide: item.selectedRide,
			id: item.id,
			primeSelected: item.primeSelected
		});
	}

	componentDidMount() {
		let isPrimeAdded = false;
		this.props.productData.map(data => {
			if (data.display_name.toUpperCase().indexOf("PRIME") >= 0) {
				if (!isPrimeAdded) {
					if (this.props.isPrimeSelected) {
						this.product.push(
							this.props.productData[this.props.selectedCabIndex]
						);
					} else {
						this.product.push(data);
					}
					isPrimeAdded = true;
				}
				this.primeProduct.push(data);
			} else {
				this.product.push(data);
			}
		});
		this.setState({
			selectedRide: this.product.indexOf(
				this.props.productData[this.props.selectedCabIndex]
			),
			id: this.props.productData[this.props.selectedCabIndex].id,
			primeSelected: this.props.isPrimeSelected
		});
		setTimeout(() => {
			this.fadeInAnimation();
		}, 301);
	}

	componentWillUnmount = () => {
		this.fadeOutAnimation();
	};

	shouldComponentUpdate(props) {
		if (props.productData !== this.props.productData) {
			this.product = [];
			this.primeProduct = [];
			let isPrimeAdded = false;
			this.props.productData.map(data => {
				if (data.display_name.toUpperCase().indexOf("PRIME") >= 0) {
					if (!isPrimeAdded) {
						this.product.push(data);
						isPrimeAdded = true;
					}
					this.primeProduct.push(data);
				} else {
					this.product.push(data);
				}
			});
			return true;
		}
		return true;
	}

	renderProductList(data) {
		console.log("=> ", data);
		if (data.item.id === "rental" || data.item.id === "outstation") {
			return null;
		}
		return (
			<TouchableWithoutFeedback
				onPress={() => {
					let IsPrimeSelected = false;
					if (data.item.display_name.toUpperCase().indexOf("PRIME") >= 0) {
						IsPrimeSelected = true;
					}
					let item = this.props.productData.filter(
						row => row.id === data.item.id
					);
					this.props.setPrimeSelected(IsPrimeSelected);
					this.props.selectedRide(this.props.productData.indexOf(item[0]));
					this.onPressProduct({
						selectedRide: data.index,
						id: data.item.id,
						primeSelected: IsPrimeSelected
					});
					this.props.getAvailableCabsInLocation();
				}}>
				<View style={styles.cabView}>
					{data.item.eta === -1 ? (
						<View style={styles.dotsLoaderView}>
							<DotsLoader size={5} color={"#e0e0e0"} />
						</View>
					) : (
						<OlaTextRegular style={styles.etaText}>
							{data.item.eta + "min"}
						</OlaTextRegular>
					)}
					<View style={styles.imageView}>
						{data.index === this.state.selectedRide ? (
							<Image
								style={styles.uriImage}
								source={{ uri: data.item.image }}
							/>
						) : data.item.id === "auto" ? (
							<Image style={styles.cabImage} source={AUTO} />
						) : data.item.id === "share" ? (
							<Image style={styles.cabImage} source={SHARE} />
						) : data.item.id === "micro" ? (
							<Image style={styles.cabImage} source={MICRO} />
						) : data.item.id === "mini" ? (
							<Image style={styles.cabImage} source={MINI} />
						) : data.item.id === "prime" ? (
							<Image style={styles.cabImage} source={PRIME} />
						) : data.item.id === "prime_play" ? (
							<Image style={styles.cabImage} source={PRIME} />
						) : data.item.id === "suv" ? (
							<Image style={styles.cabImage} source={SUV} />
						) : data.item.id === "lux" ? (
							<Image style={styles.cabImage} source={LUX} />
						) : data.item.id === "outstation" ? (
							<Image style={styles.cabImage} source={OUTSTATION} />
						) : data.item.id === "rental" ? (
							<Image style={styles.cabImage} source={RENTALS} />
						) : (
							<Image style={styles.cabImage} source={PRIME} />
						)}
					</View>
					{data.index === this.state.selectedRide ? (
						<OlaTextRegular
							style={styles.displayNameText}
							numberOfLines={1}
							ellipsizeMode={"tail"}>
							{data.item.display_name.toUpperCase().includes("PRIME")
								? "Prime"
								: data.item.display_name}
						</OlaTextRegular>
					) : (
						<OlaTextRegular
							style={styles.displayNameTextTwo}
							numberOfLines={1}
							ellipsizeMode={"tail"}>
							{data.item.display_name.toUpperCase().includes("PRIME")
								? "Prime"
								: data.item.display_name}
						</OlaTextRegular>
					)}
				</View>
			</TouchableWithoutFeedback>
		);
	}

	renderPrimeProduct(data) {
		return (
			<TouchableOpacity
				onPress={() => {
					this.props.selectedRide(this.state.selectedRide + data.index);
					this.product[this.state.selectedRide] = data.item;
					this.onPressProduct({
						selectedRide: this.state.selectedRide,
						id: data.item.id,
						primeSelected: true
					});
				}}>
				<View
					style={
						this.state.id === data.item.id
							? styles.primeDisplayViewOne
							: styles.primeDisplayViewTwo
					}>
					<OlaTextRegular style={styles.primeDisplayText}>
						{data.item.display_name}
					</OlaTextRegular>
					<OlaTextRegular style={styles.primeDisplayEta}>
						{data.item.eta === -1
							? I18n.t("no_cab")
							: data.item.eta + " " + data.item.time_unit}
					</OlaTextRegular>
				</View>
			</TouchableOpacity>
		);
	}

	render() {
		console.log("data=========>", this.state);
		return (
			<Animated.View
				style={[
					styles.container,
					this.props.productContainer,
					{ bottom: this.footerAnimatedValue }
				]}>
				{this.state.primeSelected === true ? (
					<FlatList
						style={{
							width: width
						}}
						showsHorizontalScrollIndicator={false}
						horizontal={true}
						data={this.primeProduct}
						renderItem={data => this.renderPrimeProduct(data)}
						keyExtractor={(item, index) => index.toString()}
					/>
				) : null}
				<FlatList
					style={styles.productList}
					contentContainerStyle={{
						alignItems: "center",
						borderColor: "#000000",
						height: height / 9
					}}
					showsHorizontalScrollIndicator={false}
					horizontal={true}
					data={this.product}
					renderItem={data => this.renderProductList(data)}
					keyExtractor={(item, index) => index.toString()}
				/>
				<View style={styles.rowFlex}>
					{typeof this.props.productData[this.props.selectedCabIndex] !==
						"undefined" &&
					this.props.productData[this.props.selectedCabIndex]
						.ride_later_allowed === true ? (
						<View style={styles.rowFlex}>
							<TouchableWithoutFeedback
								disabled={
									this.props.productData[this.props.selectedCabIndex]
										.ride_later_enabled === "false"
								}
								onPress={this.requestRideLater}>
								<View style={styles.rideLaterButton}>
									<OlaTextRegular style={styles.rideLaterText}>
										{I18n.t("ride_later")}
									</OlaTextRegular>
								</View>
							</TouchableWithoutFeedback>
							<Seperator
								width={width / 540}
								height={height / 14.26}
								color={"#474747"}
							/>
						</View>
					) : null}
					<TouchableWithoutFeedback onPress={this.requestRide}>
						<View
							style={
								typeof this.props.productData[this.props.selectedCabIndex] !==
									"undefined" &&
								this.props.productData[this.props.selectedCabIndex]
									.ride_later_allowed === true
									? styles.rideLaterAllowed
									: styles.rideLaterNotAllowed
							}>
							<OlaTextRegular style={styles.rideNowText}>
								{I18n.t("ride_now")}
							</OlaTextRegular>
						</View>
					</TouchableWithoutFeedback>
				</View>
			</Animated.View>
		);
	}
}
