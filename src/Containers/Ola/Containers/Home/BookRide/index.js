import React from "react";
import {
	View,
	Dimensions,
	BackHandler,
	StatusBar,
	TouchableOpacity,
	Image,
	Platform,
	UIManager
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { Polyline, Marker } from "react-native-maps";
import { connect } from "react-redux";
import {
	MapView,
	LocationPointer,
	ProgressScreen,
	CabMarker,
	InformationFooter,
	DrawerOpeningHack,
	GoToast,
	DatePicker,
	TimePicker
} from "../../../../../Components";
import I18n from "../../../Assets/Strings/i18n";
import {
	olaFetchOrigin,
	olaOrigin,
	olaGetProducts,
	olaFetchDestination,
	olaProducts,
	olaGetUserFavourites,
	olaSaveHotSpotZone
} from "./Saga";
import {
	LOCATIONNOT,
	LOCATIONPICKUP,
	LOCATIONDROP,
	AUTOMAP,
	SHAREMAP,
	MICROMAP,
	MINIMAP,
	PRIMEMAP,
	SUVMAP,
	LUXMAP,
	OUTSTATIONMAP,
	RENTALSMAP,
	SEDANMAP
} from "../../../Assets/Img/Image";
import OlaProducts from "./OlaProducts";
import { DrawerHeader, LocationButton } from "../../../Components";
import Rentals from "./Rentals";
import Outstation from "./Outstation";
import { olaCurrentRide } from "../../ChooseRide/Saga";
import PickupDropComponent from "./PickupDropComponent";
import GoAppAnalytics from "../../../../../utils/GoAppAnalytics";
import LocationModule from "../../../../../CustomModules/LocationModule";
import {
	OlaTextRegular,
	OlaTextMedium,
	OlaTextBold
} from "../../../Components/OlaText";
import { indexStyles as styles } from "./style";

const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.015;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const carImages = {
	share: SHAREMAP,
	micro: MICROMAP,
	mini: MINIMAP,
	prime: SEDANMAP,
	prime_play: PRIMEMAP,
	suv: SUVMAP,
	exec: PRIMEMAP,
	auto: AUTOMAP,
	rental: RENTALSMAP,
	outstation: OUTSTATIONMAP,
	lux: LUXMAP
};

class BookRide extends React.Component {
	static navigationOptions = {
		header: null
	};

	constructor(props) {
		super(props);
		const { latitude, longitude } = props.origin;
		let region;
		this.firstLoad = false;
		if (latitude !== null) {
			region = {
				latitude,
				longitude,
				latitudeDelta: LATITUDE_DELTA,
				longitudeDelta: LONGITUDE_DELTA
			};
			this.firstLoad = true;
		} else {
			region = null;
			this.firstLoad = false;
		}
		if (Platform.OS === "android") {
			UIManager.setLayoutAnimationEnabledExperimental(true);
		}
		this.state = {
			isMapReady: false,
			locationModule: false,
			region,
			searchType: true,
			showSpinner: true,
			dropLocation: true,
			pickupMarker: true,
			selectedTextBox: "origin",
			originDestination: false,
			rideTypes: ["daily_ride", "rental", "outstation"],
			selectedRideType: 0,
			cabs: [],
			footerVisible: false,
			selectedCabIndex: 0,
			prevSelectedCabIndex: 0,
			isPrimeSelected: false,
			showDatePicker: false,
			showTimePicker: false,
			cabMarkers: []
		};
		console.log(this.state);
		this.rideInProgressCheck = false;
		this.mapFlag = true;
		this.openPlaceSearch = this.openPlaceSearch.bind(this);
		this.chooseRide = this.chooseRide.bind(this);
		this.changeRideType = this.changeRideType.bind(this);
		this.getProducts = this.getProducts.bind(this);
		this.regionChangeComplete = this.regionChangeComplete.bind(this);
		this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
		this.getAvailableCabsInLocation = this.getAvailableCabsInLocation.bind(
			this
		);
		this.changeRideType = this.changeRideType.bind(this);
		this.changeScreen = this.changeScreen.bind(this);
		this.interval; // Using it to ping ola products if it is null
		this.onTimeSelected = this.onTimeSelected.bind(this);
		this.hideDrawer = false; // For Animations
		this.data = null;
		this.day = null;
		this.month = null;
		this.year = null;
		this.isFirstRegionChangeComplete = true;
	}

	addDays(theDate, days) {
		return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
	}

	openPlaceSearch(searchType) {
		this.props.navigation.navigate("PlaceSearch", {
			searchType: searchType,
			selectedLocation: locationObject => {
				this.mapRef &&
					this.mapRef.animateRegionChange({
						latitude: locationObject.latitude,
						longitude: locationObject.longitude,
						latitudeDelta: LATITUDE_DELTA,
						longitudeDelta: LONGITUDE_DELTA
					});
			}
		});
	}

	onTimeSelected(data, time) {
		if (new Date(time).getTime() < new Date().getTime()) {
			GoToast.show(I18n.t("correct_time"), I18n.t("information"));
		} else {
			this.props.navigation.navigate("ChooseRide", {
				id: data.id,
				pickup_time: time,
				pickup_mode: "LATER"
			});
		}
	}

	componentWillMount() {
		BackHandler.addEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
	}

	getProducts(lat, long) {
		if (lat == null || long == null) {
			return;
		}
		this.props.dispatch(
			olaGetProducts({
				access_token: this.props.userToken,
				pickup_lat: lat,
				pickup_lng: long,
				appToken: this.props.appToken
			})
		);
	}

	getAvailableCabsInLocation() {
		this.setState({ cabMarkers: [] });
		this.getProducts(this.props.origin.latitude, this.props.origin.longitude);
	}

	componentWillUnmount() {
		console.log("interval is", this.interval);
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.handleBackButtonClick
		);
		this._unsubscribeFocus();
		this.props.dispatch(olaProducts([]));
		clearInterval(this.interval);
	}

	componentDidUpdate(prevProps) {
		let self = this;
		if (this.props.products.length > 0) {
			clearInterval(this.interval);
			this.interval = setInterval(function() {
				self.getProducts(
					self.props.origin.latitude,
					self.props.origin.longitude
				);
			}, 60000);
		}
	}
	handleBackButtonClick() {
		if (!this.props.location.status) {
			return false;
		}
		return false;
	}

	changeRideType(index) {
		let selectedCabIndex;
		this.getAvailableCabsInLocation();
		if (index === 1 && this.props.products.length > 0) {
			selectedCabIndex = this.props.products.findIndex(elem => {
				return elem.id === "rental";
			});
			this.setState(state => {
				return {
					selectedRideType: index,
					selectedCabIndex,
					prevSelectedCabIndex:
						state.selectedRideType === 0
							? state.selectedCabIndex
							: state.prevSelectedCabIndex
				};
			});
		} else if (index === 2 && this.props.products.length > 0) {
			selectedCabIndex = this.props.products.findIndex(elem => {
				return elem.id === "outstation";
			});
			this.setState(state => {
				return {
					selectedRideType: index,
					selectedCabIndex,
					prevSelectedCabIndex:
						state.selectedRideType === 0
							? state.selectedCabIndex
							: state.prevSelectedCabIndex
				};
			});
		} else if (index === 0 && this.props.products.length > 0) {
			selectedCabIndex = this.state.prevSelectedCabIndex;
			let isSelectedCabPrime =
				this.props.products[selectedCabIndex].display_name
					.toUpperCase()
					.indexOf("PRIME") >= 0;
			this.setState(state => {
				return {
					selectedRideType: index,
					selectedCabIndex: state.prevSelectedCabIndex,
					isPrimeSelected: isSelectedCabPrime
				};
			});
		}
	}

	getCoordinateToFit = () => {
		if (
			this.props.products.length > 0 &&
			typeof this.props.products[this.state.selectedCabIndex].all_cabs !==
				"undefined" &&
			this.props.products[this.state.selectedCabIndex].all_cabs.length > 0
		) {
			const cabLatLngArr = this.props.products[
				this.state.selectedCabIndex
			].all_cabs.map(item => {
				return {
					latitude: item.lat,
					longitude: item.lng
				};
			});
			return cabLatLngArr;
		}
		return [];
	};

	shouldComponentUpdate(props, state) {
		if (props.bookingStatus !== null) {
			if (
				typeof props.bookingStatus.booking_status !== "undefined" &&
				this.rideInProgressCheck
			) {
				if (props.bookingStatus.status === "SUCCESS") {
					if (
						props.bookingStatus.booking_status === "COMPLETED" ||
						props.bookingStatus.booking_status === "CANCELLED" ||
						props.bookingStatus.booking_status === "BOOKING_CANCELLED"
					) {
						this.rideInProgressCheck = false;
					} else {
						this.rideInProgressCheck = false;
						this.props.navigation.navigate("TrackRide");
					}
				}
			}
			return true;
		}
		if (props.products !== null && props.products.hasOwnProperty("code")) {
			this.footerMessage = props.products.message;
			if (!this.state.footerVisible) {
				this.setState({
					footerVisible: true
				});
			}
		} else if (
			props.products.length > 0 &&
			!props.products.hasOwnProperty("code")
		) {
			if (this.state.footerVisible) {
				this.setState({
					footerVisible: false
				});
			}
			if (props.products !== this.props.products) {
				this.setState({ cabMarkers: props.products });
			}
		}
		if (
			props.location.status &&
			(this.props.location.status === null ||
				this.props.location.status === false)
		) {
			const { latitude, longitude, address, locality } = props.location;
			if (props.origin.latitude === null) {
				let region = {
					latitude,
					longitude,
					latitudeDelta: LATITUDE_DELTA,
					longitudeDelta: LONGITUDE_DELTA
				};
				this.mapRef && this.mapRef.animateRegionChange(region, 500);
				this.props.dispatch(
					olaOrigin({
						latitude,
						longitude,
						address,
						locality
					})
				);
				this.setState({ showSpinner: false });
			} else {
				let region = {
					latitude: props.origin.latitude,
					longitude: props.origin.longitude,
					latitudeDelta: LATITUDE_DELTA,
					longitudeDelta: LONGITUDE_DELTA
				};
				this.mapRef && this.mapRef.animateRegionChange(region, 500);
				this.setState({ showSpinner: false });
			}
		} else if (props.location.status !== null && !props.location.status) {
			// maybe here
			if (state.showSpinner) {
				this.setState({ showSpinner: false }, () =>
					GoToast.show(I18n.t("ola_no_location_found"), I18n.t("information"))
				);
			}
		}
		if (
			props.products === null &&
			props.origin.latitude !== null &&
			props.origin.latitude !== this.props.origin.latitude
		) {
			this.getProducts(props.origin.latitude, props.origin.longitude);
		}
		if (
			props.hotspotZone.hasOwnProperty("is_hotspot_zone") &&
			props.hotspotZone !== this.props.hotspotZone
		) {
			if (props.hotspotZone.is_hotspot_zone === true) {
				let pickup = props.hotspotZone.pickup_points.filter(
					row => row.id === props.hotspotZone.default_pickup_point_id
				)[0];
				this.pickupDropRef.scrollToItem(pickup);
				this.mapRef &&
					this.mapRef.animateRegionChange({
						latitude: pickup.lat,
						longitude: pickup.lng,
						latitudeDelta: LATITUDE_DELTA,
						longitudeDelta: LONGITUDE_DELTA
					});
			}
		}
		return true;
	}

	componentDidMount() {
		this._unsubscribeFocus = this.props.navigation.addListener("focus", () => {
			GoAppAnalytics.setPageView("ola", "daily_rides");
		});

		AsyncStorage.getItem("RIDE_IN_PROGRESS")
			.then(result => {
				if (JSON.parse(result)) {
					if (!this.props.isNavigationToTrackRide) {
						this.rideInProgressCheck = true;
						let rideInProgressData = JSON.parse(result);
						this.props.dispatch(
							olaCurrentRide({
								access_token: this.props.userToken,
								request_id: rideInProgressData.booking_id,
								appToken: this.props.appToken
							})
						);
						AsyncStorage.setItem("RIDE_IN_PROGRESS", JSON.stringify(false));
					} else {
						// Todo add check to display strip in home page if ride in progress
					}
				}
			})
			.catch(error => {
				console.log("This is error: ", error);
			});
		if (
			this.props.estimate !== null &&
			this.props.estimate.code !== "undefined" &&
			this.props.estimate.code === "DIFFERENT_DROP_CITY"
		) {
			this.changeScreen("OutStation", { time: new Date() });
		}
		if (!this.props.isNavigationToTrackRide) {
			if (this.props.origin.latitude !== null) {
				let region = {
					latitude: this.props.origin.latitude,
					longitude: this.props.origin.longitude,
					latitudeDelta: LATITUDE_DELTA,
					longitudeDelta: LONGITUDE_DELTA
				};
				this.setState({
					showSpinner: false
				});
				this.state.isMapReady && this.mapRef.animateRegionChange(region, 500);
				console.log("Check 3");
				this.getProducts(region.latitude, region.longitude);
			} else if (this.props.location.status === true) {
				const { latitude, longitude, address, locality } = this.props.location;
				this.props.dispatch(
					olaOrigin({
						latitude,
						longitude,
						address,
						locality
					})
				);
				console.log("Check 2");
				this.getProducts(latitude, longitude);
				this.setState({
					region: {
						latitude,
						longitude,
						latitudeDelta: LATITUDE_DELTA,
						longitudeDelta: LONGITUDE_DELTA
					},
					showSpinner: false,
					originDestination: false
				});
			} else {
				if (this.state.showSpinner && this.props.location.status === false) {
					this.setState({ showSpinner: false }, () =>
						GoToast.show(
							I18n.t("ola_no_location_found"),
							I18n.t("information"),
							"LONG"
						)
					);
				}
			}
			let self = this;
			if (
				this.props.products.length === 0 &&
				this.props.origin.latitude !== null
			) {
				this.interval = setInterval(function() {
					self.getProducts(
						self.props.origin.latitude,
						self.props.origin.longitude
					);
				}, 2000);
			} else if (this.props.products.length > 0) {
				this.interval = setInterval(function() {
					self.getProducts(
						self.props.origin.latitude,
						self.props.origin.longitude
					);
				}, 60000);
			}
			this.props.dispatch(
				olaGetUserFavourites({
					access_token: this.props.userToken,
					appToken: this.props.appToken
				})
			);
		} else {
			this.setState({ showSpinner: false });
		}
	}

	chooseRide(data) {
		if (this.props.destination.latitude !== null) {
			if (!data.hasOwnProperty("rideLater")) {
				this.props.navigation.navigate("ChooseRide", { id: data.id });
			} else {
				this.data = data;
				this.setState({ showDatePicker: true });
			}
		} else {
			this.mapFlag = false;
			this.setState({
				dropLocation: false,
				selectedTextBox: "destination"
			});
			this.openPlaceSearch("destination");
			GoToast.show(I18n.t("drop_location_empty_warning"));
		}
	}

	changeScreen(screenName, params = {}) {
		if (screenName === "Rentals") {
			this.props.navigation.navigate(screenName, params);
		} else if (this.props.destination.latitude !== null) {
			this.props.navigation.navigate(screenName, params);
		} else {
			this.setState({ dropLocation: false, selectedTextBox: "destination" });
			GoToast.show(
				I18n.t("drop_location_empty_warning"),
				I18n.t("information")
			);
		}
	}

	// https://stackoverflow.com/questions/639695/how-to-convert-latitude-or-longitude-to-meters
	calculateMetersBetweenLatAndLng(lat1, lon1, lat2, lon2) {
		// generally used geo measurement function
		let R = 6378.137; // Radius of earth in KM
		let P = Math.PI / 180;
		let dLat = lat2 * P - lat1 * P;
		let dLon = lon2 * P - lon1 * P;
		let a =
			Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.cos((lat1 * Math.PI) / 180) *
				Math.cos((lat2 * Math.PI) / 180) *
				Math.sin(dLon / 2) *
				Math.sin(dLon / 2);
		let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		let d = R * c;
		return d * 1000; // meters
	}

	regionChangeComplete(region) {
		if (this.isFirstRegionChangeComplete) {
			this.isFirstRegionChangeComplete = false;
			this.props.origin.latitude !== null &&
				this.mapRef.animateRegionChange({
					latitude: this.props.origin.latitude,
					longitude: this.props.origin.longitude,
					latitudeDelta: LATITUDE_DELTA,
					longitudeDelta: LATITUDE_DELTA
				});
		} else {
			this.hideDrawer = false;
			this.drawerRef.fadeInAnimation();
			this.pickupDropRef.fadeInAnimation();
			if (this.props.products.length > 0 && this.props.isInternet) {
				this.bottomRef.fadeInAnimation();
			}
			if (this.state.selectedTextBox === "origin") {
				this.getProducts(region.latitude, region.longitude);

				clearInterval(this.interval);
				let self = this;
				this.interval = setInterval(function() {
					self.getProducts(region.latitude, region.longitude);
				}, 60000);
				if (!this.firstLoad) {
					this.props.dispatch(
						olaFetchOrigin({
							latitude: region.latitude,
							longitude: region.longitude
						})
					);
				} else {
					this.firstLoad = false;
				}
			} else {
				if (!this.state.dropLocation) {
					this.setState({ dropLocation: true });
				}
				this.props.dispatch(
					olaFetchDestination({
						latitude: region.latitude,
						longitude: region.longitude
					})
				);
			}
		}
	}

	zoomOut = data => {
		let eta =
			this.props.products &&
			this.props.products[this.state.selectedCabIndex].hasOwnProperty("eta")
				? this.props.products[this.state.selectedCabIndex].eta > 5
					? 10
					: this.props.products[this.state.selectedCabIndex].eta <= 1
					? 2
					: this.props.products[this.state.selectedCabIndex].eta * 2
				: 10;
		this.state.isMapReady &&
			this.mapRef &&
			this.mapRef.animateRegionChange({
				latitude: data.latitude,
				longitude: data.longitude,
				latitudeDelta: LATITUDE_DELTA * eta,
				longitudeDelta: LONGITUDE_DELTA * eta
			});
		setTimeout(() => {
			this.zoomIn(data);
		}, 700);
	};

	zoomIn = data => {
		this.state.isMapReady &&
			this.mapRef &&
			this.mapRef.animateRegionChange({
				latitude: data.latitude,
				longitude: data.longitude,
				latitudeDelta: LATITUDE_DELTA,
				longitudeDelta: LONGITUDE_DELTA
			});
	};

	handleRegionChange = () => {};

	handlePanDrag = () => {
		if (!this.hideDrawer) {
			this.drawerRef.fadeOutAnimation();
			this.pickupDropRef.fadeOutAnimation();
			this.bottomRef && this.bottomRef.fadeOutAnimation();
			if (this.props.products.length > 0 && this.props.isInternet) {
				this.bottomRef.fadeOutAnimation();
			}
			this.hideDrawer = true;
			this.forceUpdate();
		}
	};

	onMapLayout = () => {
		this.setState({ isMapReady: true });
		if (this.props.location.status && this.mapRef) {
			this.mapRef.animateRegionChange(this.state.region);
		}
	};

	render() {
		console.log("hotspot zone is", this.props);
		return (
			<View style={styles.mainContainer}>
				{this.state.locationModule ? (
					<LocationModule
						onError={() =>
							this.setState({
								locationModule: false
							})
						}
					/>
				) : null}
				<StatusBar backgroundColor="#bdbdbd" barStyle="light-content" />
				<MapView
					ref={ref => (this.mapRef = ref)}
					onLayout={true}
					showsUserLocation={true}
					followUserLocation={false}
					initialRegion={this.state.region}
					onPanDrag={this.handlePanDrag}
					coordinatesToFit={
						this.mapFlag === true ? this.getCoordinateToFit() : []
					}
					onMapReady={this.onMapLayout}
					onRegionChange={this.handleRegionChange}
					onRegionChangeComplete={region =>
						this.regionChangeComplete(region, false, this.state.selectedTextBox)
					}>
					{this.state.isMapReady &&
						this.state.cabMarkers.length > 0 &&
						typeof this.state.cabMarkers[this.state.selectedCabIndex]
							.all_cabs !== "undefined" &&
						this.state.cabMarkers[this.state.selectedCabIndex].all_cabs.length >
							0 &&
						this.state.cabMarkers[this.state.selectedCabIndex].all_cabs.map(
							(data, key) => {
								return (
									<CabMarker
										key={key}
										coordinates={{
											latitude: data.lat,
											longitude: data.lng
										}}
										rotation={data.bearing != null ? data.bearing : 0}
										img={
											this.props.scaledImage[
												this.state.cabMarkers[this.state.selectedCabIndex].id
											]
										}
									/>
								);
							}
						)}
					{this.state.isMapReady && this.props.hotspotZone.is_hotspot_zone ? (
						<Polyline
							coordinates={this.props.hotspotZone.hotspot_boundary}
							strokeWidth={2}
							strokeColor={"black"}
						/>
					) : null}
					{this.state.isMapReady &&
						this.props.hotspotZone.is_hotspot_zone &&
						this.props.hotspotZone.pickup_points.map((data, key) => {
							return (
								<Marker
									key={data.id}
									coordinate={{
										latitude: data.lat,
										longitude: data.lng
									}}>
									<View>
										<View style={styles.hotspotZoneView}>
											<View style={styles.hotspotZoneBorder} />
											<View style={styles.hotspotZoneInner} />
										</View>
									</View>
								</Marker>
							);
						})}
				</MapView>
				{/* Drawer Opening Hack */}
				<DrawerOpeningHack />

				{this.state.selectedTextBox === "origin" ||
				this.props.destination.latitude === null ? (
					<LocationPointer
						pointerIconStyle={styles.pointerIconStyle}
						pointerStyle={styles.pointerStyle}
						pointerIcon={LOCATIONPICKUP}
					/>
				) : (
					<LocationPointer
						pointerStyle={styles.pointerStyle}
						pointerIconStyle={styles.pointerIconStyle}
						pointerIcon={LOCATIONDROP}
					/>
				)}
				<DrawerHeader
					headerStyle={styles.drawerHeaderStyle}
					rideTypes={this.state.rideTypes}
					selectedRideType={this.state.selectedRideType}
					changeRideType={this.changeRideType}
					onPress={() => this.props.navigation.openDrawer()}
					ref={ref => (this.drawerRef = ref)}
				/>
				{!this.hideDrawer ? (
					<LocationButton
						locationStyle={
							this.state.selectedRideType === 1 ||
							this.state.selectedRideType === 2
								? [styles.locationStyleOne, styles.locationStyleShape]
								: this.state.isPrimeSelected === true
								? [styles.locationStyleTwo, styles.locationStyleShape]
								: [styles.locationStyleThree, styles.locationStyleShape]
						}
						iconColor={
							this.props.origin.latitude === this.props.location.latitude &&
							this.props.origin.longitude === this.props.location.longitude
								? "#5B5F58"
								: "#0076ff"
						}
						iconSize={width / 18}
						currentPosition={data => {
							this.mapFlag = true;
							this.setState({ selectedTextBox: "origin" });
							this.props.products.length > 0 && this.zoomOut(data);
							this.props.dispatch(
								olaFetchOrigin({
									latitude: data.latitude,
									longitude: data.longitude
								})
							);
							this.getProducts(data.latitude, data.longitude);
						}}
					/>
				) : null}
				<PickupDropComponent
					onPickupPress={() => {
						if (this.state.selectedTextBox !== "origin") {
							this.setState({ selectedTextBox: "origin" });
							this.props.origin.latitude !== null &&
								this.mapRef &&
								this.mapRef.animateRegionChange({
									latitude: this.props.origin.latitude,
									longitude: this.props.origin.longitude,
									latitudeDelta: LATITUDE_DELTA,
									longitudeDelta: LONGITUDE_DELTA
								});
						} else {
							this.mapFlag = false;
							this.openPlaceSearch("origin");
						}
					}}
					pickupTextStyle={
						this.state.selectedTextBox === "origin"
							? styles.pickupTextStyleWithOrigin
							: styles.pickupTextStyleWithoutOrigin
					}
					pickupText={
						this.props.origin.address || I18n.t("enter_pickup_location")
					}
					pickupSelected={this.state.selectedTextBox === "origin"}
					showDropBox={this.state.selectedRideType !== 1}
					onDropPress={() => {
						if (this.state.selectedTextBox !== "destination") {
							this.setState({ selectedTextBox: "destination" });
							if (this.props.destination.address !== null) {
								this.mapRef &&
									this.mapRef.animateRegionChange({
										latitude: this.props.destination.latitude,
										longitude: this.props.destination.longitude,
										latitudeDelta: LATITUDE_DELTA,
										longitudeDelta: LONGITUDE_DELTA
									});
							}
						} else {
							this.mapFlag = false;
							this.openPlaceSearch("destination");
						}
					}}
					dropText={
						this.props.destination.address || I18n.t("enter_drop_location")
					}
					dropLocation={this.state.dropLocation}
					dropSelected={this.state.selectedTextBox === "destination"}
					onHotspotPickupPress={() => {
						this.mapFlag = false;
						this.openPlaceSearch("origin");
					}}
					onHotspotPickupItemPress={item => {
						this.props.dispatch(
							olaOrigin({
								address: this.props.origin.address,
								latitude: item.lat,
								longitude: item.lng
							})
						);
						this.props.dispatch(
							olaSaveHotSpotZone({
								...this.props.hotspotZone,
								default_pickup_point_id: item.id
							})
						);
						this.mapRef &&
							this.mapRef.animateRegionChange({
								latitude: item.lat,
								longitude: item.lng,
								latitudeDelta: LATITUDE_DELTA,
								longitudeDelta: LONGITUDE_DELTA
							});
					}}
					originAddress={this.props.origin.address}
					isHotSpotZone={this.props.isHotSpotZone}
					hotspotZone={this.props.hotspotZone}
					ref={ref => (this.pickupDropRef = ref)}
				/>
				{this.rideInProgressCheck ? (
					<TouchableOpacity
						style={
							this.state.isPrimeSelected === false
								? styles.rideInProgressButtonWithoutPrime
								: styles.rideInProgressButtonWithPrime
						}
						onPress={() => this.props.navigation.navigate("TrackRide")}>
						<OlaTextRegular style={styles.rideInProgressText}>
							{I18n.t("ride_in_progress_go_track_ride")}
						</OlaTextRegular>
					</TouchableOpacity>
				) : null}
				{this.props.isInternet &&
				this.state.selectedRideType === 0 &&
				this.props.products.length > 0 ? (
					<OlaProducts
						callbackRequestRide={newState => {
							this.chooseRide(newState);
						}}
						selectedRide={selectedCabIndex => {
							console.log("selected cab index", selectedCabIndex);
							this.setState({ selectedCabIndex });
						}}
						setPrimeSelected={primeSelected => {
							this.setState({ isPrimeSelected: primeSelected });
						}}
						selectedCabIndex={this.state.selectedCabIndex}
						productData={this.props.products}
						isLoading={this.props.isProductsFetching}
						ref={ref => (this.bottomRef = ref)}
						isPrimeSelected={this.state.isPrimeSelected}
						getAvailableCabsInLocation={this.getAvailableCabsInLocation}
					/>
				) : null}
				{this.state.selectedRideType === 1 && !this.state.footerVisible ? (
					<Rentals
						changeScreen={this.changeScreen}
						products={this.props.products}
						ref={ref => (this.bottomRef = ref)}
					/>
				) : null}
				{this.state.selectedRideType === 2 && !this.state.footerVisible ? (
					<Outstation
						changeScreen={this.changeScreen}
						products={this.props.products}
						ref={ref => (this.bottomRef = ref)}
					/>
				) : null}
				{this.state.showSpinner ? (
					<ProgressScreen indicatorColor={"#cddc39"} />
				) : null}
				{this.state.footerVisible ? (
					<InformationFooter informationFooterText={this.footerMessage} />
				) : null}
				{!this.props.isInternet ? (
					<InformationFooter
						informationFooterText={I18n.t("check_internet")}
						informationFooterStyle={styles.informationFooterStyle}
						informationFooterTextStyle={styles.informationFooterTextStyle}
					/>
				) : null}
				{this.props.location.status !== null &&
				!this.props.location.status &&
				this.props.location.latitude === null &&
				this.props.origin.latitude === null ? (
					<View style={styles.locationStatusView}>
						<Image source={LOCATIONNOT} style={styles.noLocationImage} />
						<View style={styles.locationStatusInnerView}>
							<View style={styles.centerAlign}>
								<OlaTextBold style={styles.gpsOffText}>
									{I18n.t("gps_turned_off")}
								</OlaTextBold>
								<OlaTextRegular style={styles.textCenterAlign}>
									{I18n.t("allow_gps_for_accurate_pickup")}
								</OlaTextRegular>
							</View>
							<TouchableOpacity
								onPress={() => this.setState({ locationModule: true })}>
								<View style={styles.turnGpsOn}>
									<OlaTextMedium style={styles.turnGpsOnText}>
										{I18n.t("turn_on_gps")}
									</OlaTextMedium>
								</View>
							</TouchableOpacity>
						</View>
						<TouchableOpacity
							onPress={() => {
								this.mapFlag = false;
								this.openPlaceSearch("origin");
							}}>
							<OlaTextRegular style={styles.manualLocationText}>
								{I18n.t("enter_location_manually")}
							</OlaTextRegular>
						</TouchableOpacity>
					</View>
				) : null}
				{this.state.showDatePicker ? (
					<DatePicker
						date={new Date()}
						minimumDate={new Date()}
						maximumDate={this.addDays(new Date(), 6)}
						onDateChange={date => {
							if (date !== null) {
								const { day, month, year } = date;
								this.day = day;
								this.month = month;
								this.year = year;
								this.setState({ showDatePicker: false, showTimePicker: true });
							} else {
								this.setState({ showDatePicker: false });
							}
						}}
					/>
				) : null}
				{this.state.showTimePicker ? (
					<TimePicker
						onTimeChange={time => {
							if (time !== null) {
								const { hour, minute } = time;
								let olaTimeFormat = `${
									this.month < 9 ? "0" + (this.month + 1) : this.month + 1
								}/${this.day}/${this.year} ${hour}:${minute}`;
								console.log(new Date(olaTimeFormat).getTime());
								if (new Date(olaTimeFormat).getTime() < new Date().getTime()) {
									GoToast.show(I18n.t("correct_time"), I18n.t("information"));
									this.setState({
										showDatePicker: true,
										showTimePicker: false
									});
								} else {
									this.setState({ showTimePicker: false });
									this.props.navigation.navigate("ChooseRide", {
										id: this.data.id,
										pickup_time: olaTimeFormat,
										pickup_mode: "LATER"
									});
								}
							} else {
								this.setState({ showTimePicker: false });
							}
						}}
					/>
				) : null}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		location: state.location,
		isInternet: state.netInfo.status,
		fade: state.ola && state.ola.bookRide.fade,
		appToken: state.appToken.token,
		origin: state.ola && state.ola.bookRide.origin,
		products: state.ola && state.ola.bookRide.products,
		userToken: state.ola && state.ola.auth.userCredentials,
		destination: state.ola && state.ola.bookRide.destination,
		bookingStatus: state.ola && state.ola.chooseRide.bookingStatus,
		isProductsFetching: state.ola && state.ola.bookRide.isProductsFetching,
		isNavigationToTrackRide:
			state.ola && state.ola.chooseRide.isNavigationToTrackRide,
		estimate: state.ola && state.ola.chooseRide.estimate,
		hotspotZone: state.ola && state.ola.bookRide.hotspotZone,
		isHotSpotZone: state.ola && state.ola.bookRide.isHotSpotZone,
		scaledImage: state.ola && state.ola.bookRide.scaledImage
	};
}

export default connect(mapStateToProps)(BookRide);
