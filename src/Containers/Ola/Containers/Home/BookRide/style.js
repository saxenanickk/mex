import { StyleSheet, Dimensions, Platform } from "react-native";
import { font_two } from "../../../../../Assets/styles";

const { width, height } = Dimensions.get("window");

/**
 * styles for index.js
 */

export const indexStyles = StyleSheet.create({
	mainContainer: {
		flex: 1,
		alignItems: "center",
		justifyContent: "flex-start"
	},
	hotspotZoneView: {
		width: 16,
		height: 16,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "transparent"
	},
	hotspotZoneBorder: {
		position: "absolute",
		width: 16,
		height: 16,
		backgroundColor: "black",
		opacity: 0.3,
		borderRadius: 8
	},
	hotspotZoneInner: {
		width: 12,
		height: 12,
		borderRadius: 6,
		borderColor: "#fff",
		borderWidth: 1,
		backgroundColor: "green",
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2
	},
	pointerIconStyle: {
		width: width / 8,
		height: height / 18
	},
	pointerStyle: {
		top: height / 2.35,
		left: width / 2.3
	},
	drawerHeaderStyle: {
		backgroundColor: "#ffffff",
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2
	},
	locationStyleShape: {
		width: width / 10.5,
		height: width / 10.5,
		borderRadius: width / 21
	},
	locationStyleOne: {
		bottom: height / 3.5,
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2
	},
	locationStyleTwo: {
		bottom: height / 3.9,
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2
	},
	locationStyleThree: {
		bottom: height / 4.7,
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2
	},
	pickupTextStyleWithOrigin: {
		color: "#000000",
		fontSize: width / 30
	},
	pickupTextStyleWithoutOrigin: {
		color: "#5f5f5f",
		fontSize: width / 30
	},
	rideInProgressButtonWithPrime: {
		position: "absolute",
		bottom: height / 4.2,
		backgroundColor: "#D5DB0C",
		width: width,
		alignItems: "center"
	},
	rideInProgressButtonWithoutPrime: {
		position: "absolute",
		bottom: height / 5.475,
		backgroundColor: "#D5DB0C",
		width: width,
		alignItems: "center"
	},
	rideInProgressText: {
		fontSize: width / 28,
		paddingVertical: width / 70
	},
	informationFooterStyle: { backgroundColor: "#ff0000" },
	informationFooterTextStyle: { color: "rgba(255,255,255,0.8)" },
	locationStatusView: {
		position: "absolute",
		flex: 1,
		top: 0,
		bottom: 0,
		elevation: 9,
		shadowOffset: { width: 4, height: 4 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		backgroundColor: "#fff"
	},
	noLocationImage: { width: width, flex: 1, paddingTop: height / 12 },
	locationStatusInnerView: {
		width: width,
		flex: 1,
		paddingVertical: width / 35,
		alignItems: "center",
		justifyContent: "space-between"
	},
	centerAlign: {
		alignItems: "center"
	},
	gpsOffText: {
		padding: width / 35
	},
	textCenterAlign: { textAlign: "center" },
	turnGpsOn: {
		justifyContent: "center",
		borderRadius: 2,
		alignItems: "center",
		width: width - width / 12,
		height: height / 12,
		backgroundColor: "#0f0f0f"
	},
	turnGpsOnText: {
		color: "#CCE500",
		fontSize: width / 25
	},
	manualLocationText: {
		color: "#0000ff",
		fontSize: width / 25,
		alignSelf: "center",
		marginVertical: width / 30
	}
});
/**
 * styles for outstation component
 */
export const outstationStyles = StyleSheet.create({
	features: {
		width: width / 1.2,
		alignSelf: "center",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-around"
	},
	feature: {
		width: width / 3.5,
		paddingHorizontal: width / 20,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-around"
	},
	featureIcon: {
		alignSelf: "center"
	},
	featureText: {
		color: "#777777",
		fontSize: width / 50
	},
	rideButton: {
		alignItems: "center",
		backgroundColor: "#000",
		padding: 10,
		width: "50%"
	},
	continueButton: {
		alignItems: "center",
		backgroundColor: "#000",
		padding: 10
	},
	rideView: {
		alignItems: "flex-end",
		justifyContent: "center",
		flexDirection: "row"
	},
	rideText: {
		color: "#bfec30"
	},
	bottomPane: {
		position: "absolute",
		bottom: 0,
		left: 0,
		right: 0,
		height: height / 3.8,
		justifyContent: "space-between",
		backgroundColor: "#fff"
	},
	rentalLogoView: {
		alignItems: "center",
		marginTop: width / 60
	},
	rentalLogo: {
		borderRadius: width / 20,
		width: width / 10,
		height: width / 10
	},
	etaTextView: {
		marginTop: width / 70,
		alignItems: "center"
	},
	etaText: {
		fontSize: height / 70
	},
	description: {
		marginTop: width / 70,
		color: "#000000",
		fontSize: height / 58.8
	},
	continueView: {
		borderLeftWidth: 0.5,
		borderColor: "#4f4f4f",
		justifyContent: "center",
		borderRadius: 0,
		alignItems: "center",
		width: width,
		height: height / 14.28,
		backgroundColor: "#202020"
	},
	continueText: {
		color: "#CCE500",
		fontSize: width / 25
	},
	rentCabView: {
		flexDirection: "row",
		alignItems: "center"
	},
	advanceBookingImage: {
		width: width / 33.75,
		height: height / 34.9
	},
	oneWayImage: {
		width: width / 21.176,
		height: height / 40.85
	},
	roundTripImage: {
		width: width / 18.94,
		height: height / 33.103
	}
});

/**
 * styles for Rentals.js
 */

export const rentalStyles = StyleSheet.create({
	features: {
		width: width / 1.2,
		alignSelf: "center",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-around"
	},
	feature: {
		width: width / 3.5,
		paddingHorizontal: width / 20,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-around"
	},
	featureIcon: {
		alignSelf: "center"
	},
	featureText: {
		color: "#777777",
		fontSize: width / 50
	},
	rideButton: {
		alignItems: "center",
		backgroundColor: "#000",
		padding: 10,
		width: "50%"
	},
	continueButton: {
		alignItems: "center",
		backgroundColor: "#000",
		padding: 10
	},
	rideView: {
		alignItems: "flex-end",
		justifyContent: "center",
		flexDirection: "row"
	},
	rideText: {
		color: "#bfec30"
	},
	bottomPane: {
		position: "absolute",
		bottom: 0,
		left: 0,
		right: 0,
		height: height / 3.8,
		justifyContent: "space-between",
		backgroundColor: "#fff"
	},
	rentalLogoView: {
		alignItems: "center",
		marginTop: width / 60
	},
	rentalLogo: {
		borderRadius: width / 20,
		width: width / 10,
		height: width / 10
	},
	etaTextView: {
		marginTop: width / 70,
		alignItems: "center"
	},
	etaText: {
		fontSize: height / 70
	},
	description: {
		marginTop: width / 70,
		color: "#000000",
		fontSize: height / 58.8
	},
	continueView: {
		justifyContent: "center",
		alignItems: "center",
		width: width / 2,
		height: height / 14.28,
		backgroundColor: "#202020"
	},
	continueText: {
		color: "#DBE147",
		fontSize: width / 25
	},
	rideLaterView: {
		justifyContent: "center",
		alignItems: "center",
		width: width / 2,
		height: height / 14.28,
		backgroundColor: "#202020"
	},
	rideLaterText: {
		color: "#DBE147",
		fontSize: width / 25
	},
	availableImage: {
		width: width / 20,
		height: width / 20
	},
	multipleStopsImage: {
		width: height / 35.71,
		height: height / 35.71
	},
	hourlyPackageImage: {
		width: height / 32,
		height: height / 32
	},
	rentCabView: {
		flexDirection: "row",
		alignItems: "center"
	}
});

/**
 * OlaProduct styles
 */

export const olaProductStyles = StyleSheet.create({
	container: {
		position: "absolute",
		width: width,
		backgroundColor: "#ffffff",
		borderColor: "#202020"
	},
	loadingContainer: {
		position: "absolute",
		width: width,
		height: height / 4,
		backgroundColor: "transparent",
		justifyContent: "center",
		borderWidth: 0,
		bottom: 0
	},
	cabView: {
		width: width / 5.5,
		justifyContent: "center",
		alignItems: "center",
		borderColor: "#000",
		height: height / 9
	},
	etaText: {
		color: "#000000",
		fontSize: height / 66.66
	},
	dotsLoaderView: {
		width: width / 7.5,
		height: height / 45,
		justifyContent: "center",
		paddingTop: height / 100,
		alignItems: "center"
	},
	imageView: {
		justifyContent: "center",
		alignItems: "center",
		width: width / 9.6,
		height: width / 9.6
	},
	uriImage: {
		borderRadius: width / 20,
		width: width / 11.25,
		height: width / 11.25
	},
	cabImage: {
		borderColor: "#000000",
		borderRadius: width / 20,
		width: width / 11.25,
		height: width / 11.25
	},
	displayNameText: {
		color: "#000000",
		fontSize: height / 66.66
	},
	displayNameTextTwo: {
		color: "#777777",
		fontSize: height / 66.66
	},
	primeDisplayViewOne: {
		width: width / 3.5,
		borderWidth: 0.5,
		borderColor: "rgba(0,0,0,0.3)",
		justifyContent: "center",
		alignItems: "center",
		margin: width / 90,
		borderRadius: 3,
		backgroundColor: "#DBE147"
	},
	primeDisplayViewTwo: {
		width: width / 3.5,
		borderWidth: 0.5,
		borderColor: "rgba(0,0,0,0.3)",
		justifyContent: "center",
		alignItems: "center",
		margin: width / 90,
		borderRadius: 3,
		backgroundColor: "#FFF"
	},
	primeDisplayText: {
		color: "#000000",
		fontSize: height / 66.66
	},
	primeDisplayEta: {
		color: "#000000",
		fontSize: height / 66.66
	},
	fullWidth: {
		width: width
	},
	productList: {
		borderColor: "#bdbdbd",
		borderTopWidth: height / 1000
	},
	productListContainer: {
		alignItems: "center",
		borderColor: "#000000",
		height: height / 9
	},
	rowFlex: {
		flexDirection: "row"
	},
	rideLaterButton: {
		justifyContent: "center",
		alignItems: "center",
		width: width / 2,
		height: height / 14.26,
		backgroundColor: "#202020"
	},
	rideLaterText: {
		color: "#DBE147",
		fontSize: width / 25
	},
	rideNowText: {
		color: "#DBE147",
		fontSize: width / 25
	},
	rideLaterAllowed: {
		justifyContent: "center",
		alignItems: "center",
		width: width / 2,
		height: height / 14.26,
		backgroundColor: "#202020"
	},
	rideLaterNotAllowed: {
		justifyContent: "center",
		alignItems: "center",
		width: width,
		height: height / 14.26,
		backgroundColor: "#202020"
	}
});

/**
 * pickupDropComponent styles
 */

export const pickupDropComponentStyles = StyleSheet.create({
	hotSpotZoneVisibility: {
		position: "relative",
		top: 0,
		width: width / 1.1,
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		backgroundColor: "#fff",
		borderTopLeftRadius: width / 100,
		borderTopRightRadius: width / 100
	},
	hotSpotZoneVisibilityInner: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: width / 30,
		paddingVertical: width / 70,
		alignItems: "center"
	},
	originAddressText: {
		fontSize: width / 30,
		width: width / 3,
		color: "#000"
	},
	selectPickupText: {
		fontSize: width / 35,
		color: "rgba(0,0,0,0.5)"
	},
	changeText: {
		fontSize: width / 32,
		color: "#0076ff"
	},
	hotSpotZoneButton: {
		width: width / 1.1,
		flexDirection: "row",
		borderBottomWidth: 0.5,
		borderBottomColor: "rgba(0,0,0,0.3)",
		alignItems: "center",
		paddingHorizontal: width / 30,
		paddingVertical: height / 80
	},
	hotSpotZoneButtonName: {
		fontSize: width / 35,
		marginLeft: width / 30,
		color: "#000"
	},
	disabledTextBoxStyleOne: {
		top: 0,
		left: 0,
		position: "relative",
		width: width / 1.1,
		elevation: 3,
		backgroundColor: "#ffffff",
		borderWidth: 0,
		borderColor: "#ffffff",
		borderRadius: width / 100,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2
	},
	disabledTextBoxStyleTwo: {
		top: 0,
		left: 0,
		position: "relative",
		width: width / 1.1,
		elevation: 5,
		backgroundColor: "#ffffff",
		borderWidth: 0,
		borderColor: "#ffffff",
		borderRadius: width / 100,
		shadowOffset: { width: 4, height: 4 },
		shadowColor: "black",
		shadowOpacity: 0.2
	},
	disabledTextButtonStyle: {
		borderWidth: 0,
		borderColor: "#000000",
		borderRadius: width / 100,
		backgroundColor: "#ffffff"
	},
	dropBoxText: {
		borderWidth: 0,
		borderColor: "#000000",
		borderRadius: width / 100,
		backgroundColor: "#ffffff"
	},
	absolutePosition: { position: "absolute" },
	showDropBoxNormal: {
		position: "relative",
		width: width / 1.1,
		borderWidth: 0,
		borderColor: "#ffffff",
		borderRadius: width / 100,
		backgroundColor: "#ffffff"
	},
	showDropBoxDropSelected: {
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		opacity: 1
	},
	showDropBoxUnSelected: {
		elevation: 3,
		shadowOffset: { width: 1.2, height: 1.2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		opacity: 1
	},
	showDropBoxTextOne: {
		color: "#000",
		fontSize: width / 30.3
	},
	showDropBoxTextTwo: {
		color: "#ff665a",
		fontSize: width / 30.3
	},
	showDropBoxTextThree: {
		color: "#5f5f5f",
		fontSize: width / 30.3
	}
});

/**
 * style for placeSearchContainer
 */

export const placeSearchContainerStyles = StyleSheet.create({
	favouriteBar: {
		height: height / 13.5,
		paddingHorizontal: width / 25,
		justifyContent: "space-around"
	},
	favouriteInner: {
		flexDirection: "row",
		alignItems: "center"
	},
	favouriteName: {
		fontSize: height / 48,
		color: "#242424"
	},
	favouriteAddress: {
		fontSize: height / 56.5,
		color: "#6f6f6f"
	},
	searchItemBar: {
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.5,
		paddingHorizontal: width / 25
	},
	searchItemInner: {
		width: width / 1.3,
		marginLeft: width / 35
	},
	searchItemName: {
		fontSize: height / 48,
		color: "#242424"
	},
	searchItemAddress: {
		fontSize: height / 56.5,
		color: "#6f6f6f"
	},
	fullFlex: { flex: 1 },
	rowFlex: { flexDirection: "row" },
	placeSearchMainContainer: {
		width: width,
		elevation: 2,
		shadowOffset: { width: 1.2, height: 1.2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		backgroundColor: "#ffffff",
		justifyContent: "flex-start",
		flexDirection: "row",
		height: height / 9.82
	},
	goBackButton: {
		justifyContent: "center",
		width: width / 6,
		alignItems: "center",
		height: height / 13.33
	},
	enterLocationView: {
		height: height / 13.33,
		justifyContent: "center"
	},
	enterLocationText: {
		color: "#000000",
		fontSize: height / 45.71,
		marginLeft: width / 50
	},
	textInputBar: {
		position: "absolute",
		backgroundColor: "#fff",
		elevation: 3,
		shadowOffset: { width: 1.2, height: 1.2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		top: height / 13.33,
		width: width / 1.1,
		height: height / 15,
		fontFamily: font_two,
		fontSize: width / 25,
		padding: width / 35,
		alignSelf: "center",
		borderRadius: 5,
		borderWidth: 1,
		borderColor: "#ffffff"
	},
	favouriteMain: {
		marginTop: height / 20,
		paddingHorizontal: width / 25
	},
	favouriteList: {
		borderRadius: width / 70,
		flexGrow: 0,
		backgroundColor: "#fff",
		elevation: 2,
		shadowOffset: { width: 1.2, height: 1.2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		alignSelf: "center",
		marginTop: height / 80
	},
	mainList: {
		width: width / 1.1,
		borderRadius: width / 70,
		flexGrow: 0,
		backgroundColor: "#fff",
		elevation: 2,
		shadowOffset: { width: 1.2, height: 1.2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		alignSelf: "center",
		marginTop: height / 18
	},
	emptyComponent: {
		width: width,
		height: height / 1.5,
		justifyContent: "center"
	},
	informationFooterStyle: { backgroundColor: "#ff0000" },
	informationFooterTextStyle: { color: "rgba(255,255,255,0.8)" },
	favouriteBarView: { width: width / 1.2 },
	noLocationText: {
		fontSize: height / 35,
		marginLeft: width / 4
	},
	noLocationMessage: {
		fontSize: height / 48,
		marginLeft: width / 35
	},
	noLocationInnerView: {
		marginLeft: width / 6,
		flexDirection: "row",
		alignItems: "center",
		marginTop: height / 30
	}
});
