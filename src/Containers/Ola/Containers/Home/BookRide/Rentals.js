import React, { Component, Fragment } from "react";
import {
	View,
	Image,
	TouchableWithoutFeedback,
	Dimensions,
	Animated,
	Easing
} from "react-native";
import { rentalStyles as styles } from "./style";
import {
	Seperator,
	DotsLoader,
	DatePicker,
	TimePicker
} from "../../../../../Components";
import {
	RENTALSSELECTED,
	HOURLYPACKAGES,
	MULTIPLESTOPS,
	ALWAYSAVAILABLE
} from "../../../Assets/Img/Image";
import I18n from "../../../Assets/Strings/i18n";
import {
	OlaTextRegular,
	OlaTextBold,
	OlaTextMedium
} from "../../../Components/OlaText";

const { width, height } = Dimensions.get("window");

class Rentals extends Component {
	constructor(props) {
		super(props);
		this.state = { showDatePicker: false, showTimePicker: false };
		this.footerAnimatedValue = new Animated.Value(-height / 3.8);
	}

	addDays(theDate, days) {
		return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
	}

	componentDidMount = () => {
		setTimeout(() => {
			this.fadeInAnimation();
		}, 301);
	};

	componentWillUnmount = () => {
		this.fadeOutAnimation();
	};

	fadeOutAnimation = () => {
		Animated.timing(this.footerAnimatedValue, {
			toValue: -height / 3.8,
			duration: 300,
			easing: Easing.linear
		}).start();
	};

	fadeInAnimation = () => {
		Animated.timing(this.footerAnimatedValue, {
			toValue: 0,
			duration: 300,
			easing: Easing.linear
		}).start();
	};

	render() {
		let eta;
		if (this.props.products.length > 0) {
			this.props.products.map((data, key) => {
				if (data.id === "rental") {
					eta = data.eta;
				}
			});
		} else {
			eta = 0;
		}

		return (
			<Fragment>
				<Animated.View
					style={[styles.bottomPane, { bottom: this.footerAnimatedValue }]}>
					<View>
						<View style={styles.rentalLogoView}>
							<Image style={styles.rentalLogo} source={RENTALSSELECTED} />
						</View>
						<View style={styles.etaTextView}>
							{eta > 0 ? (
								<OlaTextRegular style={styles.etaText}>{`${eta} ${I18n.t(
									"mins_away"
								)}`}</OlaTextRegular>
							) : (
								<DotsLoader size={5} color={"#e0e0e0"} />
							)}
							<View style={styles.rentCabView}>
								<OlaTextBold style={styles.description}>
									{I18n.t("rent_cab_flexible_hourly_packages")}
								</OlaTextBold>
							</View>
						</View>
					</View>
					<View style={styles.features}>
						<View style={styles.feature}>
							<Image
								style={styles.hourlyPackageImage}
								source={HOURLYPACKAGES}
							/>
							<View>
								<OlaTextMedium style={styles.featureText}>
									{I18n.t("hourly")}
								</OlaTextMedium>
								<OlaTextMedium style={styles.featureText}>
									{I18n.t("packages")}
								</OlaTextMedium>
							</View>
						</View>
						<View style={styles.feature}>
							<Image style={styles.multipleStopsImage} source={MULTIPLESTOPS} />
							<View>
								<OlaTextMedium style={styles.featureText}>
									{I18n.t("multiple")}
								</OlaTextMedium>
								<OlaTextMedium style={styles.featureText}>
									{I18n.t("stops")}
								</OlaTextMedium>
							</View>
						</View>
						<View style={styles.feature}>
							<Image style={styles.availableImage} source={ALWAYSAVAILABLE} />
							<View>
								<OlaTextMedium style={styles.featureText}>
									{I18n.t("always")}
								</OlaTextMedium>
								<OlaTextMedium style={styles.featureText}>
									{I18n.t("available")}
								</OlaTextMedium>
							</View>
						</View>
					</View>
					<View style={styles.rideView}>
						<TouchableWithoutFeedback
							onPress={() => this.setState({ showDatePicker: true })}>
							<View style={styles.rideLaterView}>
								<OlaTextMedium style={styles.rideLaterText}>
									{I18n.t("ride_later")}
								</OlaTextMedium>
							</View>
						</TouchableWithoutFeedback>
						<Seperator
							width={width / 540}
							height={height / 14.28}
							color={"#474747"}
						/>
						<TouchableWithoutFeedback
							onPress={() =>
								this.props.changeScreen("Rentals", { isRideNow: true })
							}>
							<View style={styles.continueView}>
								<OlaTextMedium style={styles.continueText}>
									{I18n.t("ride_now")}
								</OlaTextMedium>
							</View>
						</TouchableWithoutFeedback>
					</View>
				</Animated.View>
				{this.state.showDatePicker ? (
					<DatePicker
						date={new Date()}
						minimumDate={new Date()}
						maximumDate={this.addDays(new Date(), 6)}
						onDateChange={date => {
							if (date !== null) {
								const { day, month, year } = date;
								this.day = day;
								this.month = month;
								this.year = year;
								this.setState({ showDatePicker: false, showTimePicker: true });
							} else {
								this.setState({ showDatePicker: false });
							}
						}}
					/>
				) : null}
				{this.state.showTimePicker ? (
					<TimePicker
						onTimeChange={time => {
							if (time !== null) {
								const { hour, minute } = time;
								let olaTimeFormat = `${this.day}/${
									this.month < 9 ? "0" + (this.month + 1) : this.month + 1
								}/${this.year} ${hour}:${minute}`;
								this.setState({ showTimePicker: false });
								this.props.changeScreen("Rentals", {
									olaTimeFormat,
									isRideNow: false,
									pickup_time: olaTimeFormat
								});
							} else {
								this.setState({ showTimePicker: false });
							}
						}}
					/>
				) : null}
			</Fragment>
		);
	}
}

export default Rentals;
