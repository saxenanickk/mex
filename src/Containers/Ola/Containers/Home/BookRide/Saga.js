import { takeLatest, call, put } from "redux-saga/effects";
import OlaApi from "../../../Api";
import LocationApi from "../../../../../CustomModules/LocationModule/Api";
import {
	saveGlobalDestination,
	saveGlobalOrigin
} from "../../../../../CustomModules/LocationModule/Saga";
/**
 * Constants
 */

export const OLA_ORIGIN = "OLA_ORIGIN";
export const OLA_PRODUCTS = "OLA_PRODUCTS";
export const OLA_DESTINATION = "OLA_DESTINATION";
export const OLA_FETCH_ORIGIN = "OLA_FETCH_ORIGIN";
export const OLA_GET_PRODUCTS = "OLA_GET_PRODUCTS";
export const OLA_FETCH_DESTINATION = "OLA_FETCH_DESTINATION";
export const OLA_USER_FAVOURITES = "OLA_USER_FAVOURITES";
export const OLA_GET_USER_FAVOURITES = "OLA_GET_USER_FAVOURITES";
export const OLA_SAVE_HOTSPOT_ZONE = "OLA_SAVE_HOTSPOT_ZONE";
export const OLA_IS_HOTSPOT_ZONE = "OLA_IS_HOTSPOT_ZONE";
export const OLA_SAVE_SCALED_IMAGE = "OLA_SAVE_SCALED_IMAGE";
/**
 * Action Creators
 */

export const olaOrigin = payload => ({ type: OLA_ORIGIN, payload });
export const olaProducts = payload => ({ type: OLA_PRODUCTS, payload });
export const olaDestination = payload => ({ type: OLA_DESTINATION, payload });
export const olaFetchOrigin = payload => ({ type: OLA_FETCH_ORIGIN, payload });
export const olaGetProducts = payload => ({ type: OLA_GET_PRODUCTS, payload });
export const olaSaveScaledImage = payload => ({
	type: OLA_SAVE_SCALED_IMAGE,
	payload
});
export const olaFetchDestination = payload => ({
	type: OLA_FETCH_DESTINATION,
	payload
});
export const olaGetUserFavourites = payload => ({
	type: OLA_GET_USER_FAVOURITES,
	payload
});
export const olaUserFavourites = payload => ({
	type: OLA_USER_FAVOURITES,
	payload
});
export const olaSaveHotSpotZone = payload => ({
	type: OLA_SAVE_HOTSPOT_ZONE,
	payload
});
export const olaIsHotSpotZone = payload => ({
	type: OLA_IS_HOTSPOT_ZONE,
	payload
});
/**
 * Saga
 */

export function* olaBookRideSaga(dispatch) {
	yield takeLatest(OLA_FETCH_ORIGIN, handleOlaFetchOrigin);
	yield takeLatest(OLA_GET_PRODUCTS, handleOlaGetProducts);
	yield takeLatest(OLA_FETCH_DESTINATION, handleOlaFetchDestination);
	yield takeLatest(OLA_GET_USER_FAVOURITES, handleOlaGetUserFavourites);
}

/**
 * Handlers
 */

function* handleOlaFetchOrigin(action) {
	try {
		let origin = yield call(LocationApi.reverseGeocoding, action.payload);
		console.log("This is Ola Fetch Origin Saga: ", origin);
		if (origin.results.length > 0) {
			yield put(
				olaOrigin({
					address: origin.results[0].formatted_address,
					latitude: action.payload.latitude,
					longitude: action.payload.longitude
				})
			);
			yield put(
				saveGlobalOrigin({
					locality: null,
					address: origin.results[0].formatted_address,
					latitude: action.payload.latitude,
					longitude: action.payload.longitude
				})
			);
		} else {
			yield put(
				olaOrigin({
					address: "No address found",
					latitude: action.payload.latitude,
					longitude: action.payload.longitude
				})
			);
			yield put(
				saveGlobalOrigin({
					address: "No address found",
					latitude: action.payload.latitude,
					longitude: action.payload.longitude
				})
			);
		}
	} catch (error) {
		console.log("Ola Fetch Origin Error: ", error);
	}
}

function* handleOlaFetchDestination(action) {
	try {
		yield put(
			olaDestination({
				address: null,
				latitude: action.payload.latitude,
				longitude: action.payload.longitude
			})
		);
		let destination = yield call(LocationApi.reverseGeocoding, action.payload);
		console.log("This is Ola Fetch Destination Saga: ", destination);
		if (destination.results.length > 0) {
			yield put(
				olaDestination({
					address: destination.results[0].formatted_address,
					latitude: action.payload.latitude,
					longitude: action.payload.longitude
				})
			);
			yield put(
				saveGlobalDestination({
					address: destination.results[0].formatted_address,
					latitude: action.payload.latitude,
					longitude: action.payload.longitude,
					locality: null
				})
			);
		} else {
			yield put(
				olaDestination({
					address: "No address found",
					latitude: action.payload.latitude,
					longitude: action.payload.longitude
				})
			);
			yield put(
				saveGlobalDestination({
					address: "No address found",
					latitude: action.payload.latitude,
					longitude: action.payload.longitude,
					locality: null
				})
			);
		}
	} catch (error) {
		console.log("Ola Fetch Destination Error: ", error);
	}
}

function* handleOlaGetProducts(action) {
	try {
		let products = yield call(OlaApi.getRideProducts, action.payload);
		console.log("Ola Get Products Saga: ", products);
		products.hasOwnProperty("code")
			? yield put(olaProducts(products))
			: yield put(olaProducts(products.categories));
		if (products.hotspot_zone.is_hotspot_zone) {
			// convert lat lng for polyline
			let hotspot_boundary = products.hotspot_zone.hotspot_boundary.map(
				item => {
					return {
						latitude: item[0],
						longitude: item[1]
					};
				}
			);
			let hotspotZone = {
				...products.hotspot_zone,
				hotspot_boundary
			};
			yield put(olaSaveHotSpotZone(hotspotZone));
			yield put(olaIsHotSpotZone(true));
		} else {
			yield put(olaIsHotSpotZone(false));
		}
		// else {
		// 	yield put(olaSaveHotSpotZone(products.hotspot_zone))
		// }
	} catch (error) {
		console.log("Ola Get Products Error: ", error);
	}
}

function* handleOlaGetUserFavourites(action) {
	try {
		let userFavouritesArray = yield call(
			OlaApi.getUserFavourites,
			action.payload
		);
		console.log("user favourites", userFavouritesArray);
		yield put(olaUserFavourites(userFavouritesArray));
		if (userFavouritesArray.length > 0) {
			let addressObj = userFavouritesArray.reduce((acc, curVal) => {
				if (curVal.address_type === "HOME") {
					return {
						...acc,
						home_address: curVal.address_line1,
						home_location: {
							latitude: curVal.lat,
							longitude: curVal.lng
						}
					};
				}
				if (curVal.address_type === "WORK") {
					return {
						...acc,
						work_address: curVal.address_line1,
						work_location: {
							latitude: curVal.lat,
							longitude: curVal.lng
						}
					};
				}
				return acc;
			}, {});
			yield call(OlaApi.saveFavouriteLocation, {
				appToken: action.payload.appToken,
				body: addressObj
			});
		}
	} catch (e) {
		console.log("Unable to get user Favourites");
	}
}
