import React, { Component } from "react";
import {
	View,
	TextInput,
	TouchableOpacity,
	Dimensions,
	FlatList,
	Keyboard
} from "react-native";
import { Icon, Seperator, InformationFooter } from "../../../../../Components";
import { connect } from "react-redux";
import I18n from "../../../Assets/Strings/i18n";
import { olaOrigin, olaDestination, olaGetProducts } from "./Saga";
import RNGooglePlaces from "react-native-google-places";
import debounce from "../../../../../utils/debounce";
import { OlaTextMedium, OlaTextRegular } from "../../../Components/OlaText";
import { placeSearchContainerStyles as styles } from "./style";
import goAppAnalytics from "../../../../../utils/GoAppAnalytics";

const { width } = Dimensions.get("window");

class PlaceSearch extends Component {
	constructor(props) {
		super(props);
		this.state = {
			text: "",
			searchData: []
		};
		const { route } = props;
		const { searchType, selectedLocation } = route.params
			? route.params
			: { selectedLocation: () => {} };

		this.searchType = searchType ? searchType : "origin";
		this.selectedLocation = selectedLocation;
		this.isNoData = false;
	}

	searchPlace = debounce(query => {
		if (this.searchType === "origin") {
			goAppAnalytics.trackWithProperties("ola-search-query-origin", {
				query
			});
			RNGooglePlaces.getAutocompletePredictions(query, {
				country: "IN"
			})
				.then(results => {
					console.log(results);
					this.isNoData = results.length === 0 ? true : false;
					this.setState({ searchData: results });
				})
				.catch(error => {
					console.log(error.message);
				});
		} else {
			goAppAnalytics.trackWithProperties("ola-search-query-destination", {
				query
			});
			if (this.props.origin.latitude !== null) {
				RNGooglePlaces.getAutocompletePredictions(query, {
					country: "IN",
					latitude: this.props.origin.latitude,
					longitude: this.props.origin.longitude,
					radius: 70
				})
					.then(results => {
						console.log(results);
						this.isNoData = results.length === 0 ? true : false;
						this.setState({ searchData: results });
					})
					.catch(error => {
						console.log(error.message);
					});
			}
		}
	}, 750);

	// https://stackoverflow.com/questions/639695/how-to-convert-latitude-or-longitude-to-meters
	calculateMetersBetweenLatAndLng(lat1, lon1, lat2, lon2) {
		// generally used geo measurement function
		let R = 6378.137; // Radius of earth in KM
		let P = Math.PI / 180;
		let dLat = lat2 * P - lat1 * P;
		let dLon = lon2 * P - lon1 * P;
		let a =
			Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.cos((lat1 * Math.PI) / 180) *
				Math.cos((lat2 * Math.PI) / 180) *
				Math.sin(dLon / 2) *
				Math.sin(dLon / 2);
		let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		let d = R * c;
		return d * 1000; // meters
	}

	renderFavourites = ({ item }) => {
		return (
			<TouchableOpacity
				onPress={() => {
					Keyboard.dismiss();
					if (this.searchType === "origin") {
						this.props.dispatch(
							olaOrigin({
								address: item.address_line1,
								latitude: item.lat,
								longitude: item.lng
							})
						);
					} else {
						this.props.dispatch(
							olaDestination({
								address: item.address_line1,
								latitude: item.lat,
								longitude: item.lng
							})
						);
					}
					this.selectedLocation({
						latitude: item.lat,
						longitude: item.lng
					});
					this.props.navigation.goBack();
				}}
				style={styles.favouriteBar}>
				<View style={styles.favouriteInner}>
					<View style={styles.favouriteBarView}>
						<OlaTextMedium style={styles.favouriteName}>
							{item.name}
						</OlaTextMedium>
						<OlaTextRegular
							style={styles.favouriteAddress}
							numberOfLines={1}
							ellipsizeMode={"tail"}>
							{item.address_line1}
						</OlaTextRegular>
					</View>
				</View>
			</TouchableOpacity>
		);
	};

	renderSearchItem = ({ item }) => {
		return (
			<TouchableOpacity
				onPress={() => {
					Keyboard.dismiss();
					RNGooglePlaces.lookUpPlaceByID(item.placeID)
						.then(results => {
							if (this.searchType === "origin") {
								goAppAnalytics.trackWithProperties(
									"ola-search-origin-selected",
									{
										address: results.address,
										latitude: results.location.latitude,
										longitude: results.location.longitude
									}
								);
								this.props.dispatch(
									olaOrigin({
										address: results.address,
										latitude: results.location.latitude,
										longitude: results.location.longitude
									})
								);
								if (this.calculateMetersBetweenLatAndLng() > 10) {
									this.props.dispatch(
										olaGetProducts({
											access_token: this.props.userToken,
											pickup_lat: results.location.latitude,
											pickup_lng: results.location.longitude,
											appToken: this.props.appToken
										})
									);
								}
							} else {
								goAppAnalytics.trackWithProperties(
									"ola-search-destination-selected",
									{
										address: results.address,
										latitude: results.location.latitude,
										longitude: results.location.longitude
									}
								);
								this.props.dispatch(
									olaDestination({
										address: results.address,
										latitude: results.location.latitude,
										longitude: results.location.longitude
									})
								);
							}
							this.selectedLocation({
								latitude: results.location.latitude,
								longitude: results.location.longitude
							});
							this.props.navigation.goBack();
						})
						.catch(error => {
							console.log("Err: ", error);
						});
				}}
				style={styles.searchItemBar}>
				<Icon
					iconType={"material"}
					iconSize={width / 20}
					iconName={"location-on"}
					iconColor={"#bdbdbd"}
				/>
				<View style={styles.searchItemInner}>
					<OlaTextMedium
						numberOfLines={1}
						ellipsizeMode={"tail"}
						style={styles.searchItemName}>
						{item.primaryText}
					</OlaTextMedium>
					<OlaTextRegular
						numberOfLines={1}
						ellipsizeMode={"tail"}
						style={styles.searchItemAddress}>
						{item.secondaryText}
					</OlaTextRegular>
				</View>
			</TouchableOpacity>
		);
	};

	render() {
		return (
			<View style={styles.fullFlex}>
				<View style={styles.placeSearchMainContainer}>
					<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
						<View style={styles.goBackButton}>
							<Icon
								iconType={"material"}
								iconSize={width / 16}
								iconName={"arrow-back"}
								iconColor={"#707070"}
							/>
						</View>
					</TouchableOpacity>
					<View style={styles.enterLocationView}>
						<OlaTextMedium style={styles.enterLocationText}>
							{this.searchType === "origin"
								? I18n.t("enter_pickup_location")
								: I18n.t("enter_drop_location")}
						</OlaTextMedium>
					</View>
				</View>
				<TextInput
					placeholder={I18n.t("search_for_locality_landmark")}
					numberOfLines={1}
					ellipsizeMode={"tail"}
					autoFocus
					underlineColorAndroid={"transparent"}
					style={styles.textInputBar}
					onChangeText={text => {
						this.setState({ text });
						this.searchPlace(text);
					}}
					value={this.state.text}
				/>
				{this.state.text.length === 0 &&
				this.props.favourites !== null &&
				this.props.favourites.length > 0 ? (
					<View style={styles.favouriteMain}>
						<OlaTextRegular>{I18n.t("favourites")}</OlaTextRegular>
						<FlatList
							showsVerticalScrollIndicator={false}
							data={this.props.favourites}
							ItemSeparatorComponent={() => (
								<View style={styles.rowFlex}>
									<Seperator
										width={width / 25}
										height={0.4}
										color={"#ffffff"}
									/>
									<Seperator
										width={width / 1.2}
										height={0.4}
										color={"#777777"}
									/>
									<Seperator
										width={width / 25}
										height={0.4}
										color={"#ffffff"}
									/>
								</View>
							)}
							keyboardShouldPersistTaps={"always"}
							renderItem={this.renderFavourites}
							keyExtractor={(item, index) => index.toString()}
							extraData={this.props.favourites}
							style={styles.favouriteList}
						/>
					</View>
				) : null}
				{this.state.text.length !== 0 ? (
					<FlatList
						showsVerticalScrollIndicator={false}
						data={this.state.searchData !== null ? this.state.searchData : []}
						renderItem={this.renderSearchItem}
						keyExtractor={(item, index) => index.toString()}
						keyboardShouldPersistTaps={"always"}
						extraData={this.state.searchData}
						ItemSeparatorComponent={() => (
							<View style={styles.rowFlex}>
								<Seperator width={width / 25} height={0.4} color={"#ffffff"} />
								<Seperator width={width / 1.2} height={0.4} color={"#777777"} />
								<Seperator width={width / 25} height={0.4} color={"#ffffff"} />
							</View>
						)}
						style={styles.mainList}
						ListEmptyComponent={() =>
							this.isNoData ? (
								<View style={styles.emptyComponent}>
									<OlaTextMedium style={styles.noLocationText}>
										{I18n.t("ola_no_location_found")}
									</OlaTextMedium>
									<View style={styles.noLocationInnerView}>
										<Icon
											iconType={"material_community_icon"}
											iconSize={width / 18}
											iconName={"lightbulb-on-outline"}
											iconColor={"#bdbdbd"}
										/>
										<View>
											<OlaTextRegular style={styles.noLocationMessage}>
												{I18n.t("ola_check_spell_error")}
											</OlaTextRegular>
											<OlaTextRegular style={styles.noLocationMessage}>
												{I18n.t("ola_search_nearby")}
											</OlaTextRegular>
											<OlaTextRegular style={styles.noLocationMessage}>
												{I18n.t("ola_try_shorter_query")}
											</OlaTextRegular>
										</View>
									</View>
								</View>
							) : null
						}
					/>
				) : null}
				{!this.props.isInternet ? (
					<InformationFooter
						informationFooterText={I18n.t("check_internet")}
						informationFooterStyle={styles.informationFooterStyle}
						informationFooterTextStyle={styles.informationFooterTextStyle}
					/>
				) : null}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		favourites: state.ola && state.ola.bookRide.userFavourites,
		userToken: state.ola && state.ola.auth.userCredentials,
		origin: state.ola && state.ola.bookRide.origin,
		destination: state.ola && state.ola.bookRide.destination,
		isInternet: state.netInfo.status,
		appToken: state.appToken.token
	};
}

export default connect(mapStateToProps)(PlaceSearch);
