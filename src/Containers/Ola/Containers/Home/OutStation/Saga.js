import { takeLatest, takeEvery, call, put } from "redux-saga/effects";
import OlaApi from "../../../Api";

/**
 * Constants
 */

export const OLA_GET_OUTSTATION_PRODUCTS = "OLA_GET_OUTSTATION_PRODUCTS";
export const OLA_SAVE_OUTSTATION_PRODUCTS = "OLA_SAVE_OUTSTATION_PRODUCTS";

/**
 * Action Creators
 */

export const olaGetOutstationProducts = payload => ({
	type: OLA_GET_OUTSTATION_PRODUCTS,
	payload
});
export const olaSaveOutstationProducts = payload => ({
	type: OLA_SAVE_OUTSTATION_PRODUCTS,
	payload
});

/**
 * Saga
 */

export function* olaOutstationSaga(dispatch) {
	yield takeLatest(OLA_GET_OUTSTATION_PRODUCTS, handleOlaGetOutstationProducts);
}

/**
 * Handlers
 */

function* handleOlaGetOutstationProducts(action) {
	try {
		let products = yield call(OlaApi.getOutstationProducts, action.payload);
		console.log("Ola Get Outstation Product Data: ", products);
		yield put(olaSaveOutstationProducts(products));
	} catch (error) {
		console.log("Ola Outstation Products Error: ", error);
	}
}
