import React from "react";
import {
	View,
	Dimensions,
	FlatList,
	TouchableOpacity,
	Image
} from "react-native";
import { Seperator } from "../../../../../Components";
import {
	MINI,
	PRIME,
	SEDAN,
	SUV,
	LOCATIONOUTSTATION,
	MICRO
} from "../../../Assets/Img/Image";
import { OlaTextRegular, OlaTextMedium } from "../../../Components/OlaText";
import { cabListStyles as styles } from "./style";
import I18n from "../../../Assets/Strings/i18n";
const { width, height } = Dimensions.get("window");

const carImages = id => {
	switch (id) {
		case "micro":
			return MICRO;
		case "mini":
			return MINI;
		case "prime_play":
			return PRIME;
		case "prime":
			return SEDAN;
		case "suv":
			return SUV;
		default:
			return PRIME;
	}
};

const getAvailableCabType = cabModels => {
	let cab = "";
	console.log("cabmodels are", cabModels);
	if (cabModels !== undefined) {
		cabModels.map((item, index) => {
			let splitCabName = item.split(" ");
			cab = cab + splitCabName[splitCabName.length - 1];
			if (index < cabModels.length - 1) {
				cab = cab + ",";
			}
		});
	}
	return cab;
};

const renderItem = (data, props) => {
	let fareIndex = props.isOneWayAvailable === true ? 1 : 0;
	if (
		data.item.id &&
		((props.isTripOneWay &&
			data.index <
				props.products.ride_estimate.one_way.sub_categories.length) ||
			!props.isTripOneWay)
	) {
		return (
			<TouchableOpacity
				activeOpacity={1}
				style={
					props.isTripOneWay === true
						? styles.tripOneWayButton
						: styles.tripNotOneWayButton
				}
				onPress={() => {
					props.handleCabClick(data.item, data.index);
				}}>
				<View style={styles.centeredRow}>
					<Image source={carImages(data.item.id)} style={styles.cabImage} />
					<View style={styles.displayNameView}>
						<OlaTextRegular style={styles.displayNameText}>
							{data.item.display_name}
						</OlaTextRegular>
						<OlaTextRegular
							style={styles.availableCabText}
							numberOfLines={1}
							ellipsizeMode={"tail"}>
							{getAvailableCabType(data.item.car_models)}
						</OlaTextRegular>
						<OlaTextRegular
							style={styles.availableCabText}
							numberOfLines={1}
							ellipsizeMode={"tail"}>
							{data.item.description}
						</OlaTextRegular>
					</View>
				</View>
				<OlaTextRegular style={styles.fareText}>
					{props.isTripOneWay
						? "₹" +
						  props.products.ride_estimate.one_way.sub_categories[data.index]
								.amount
						: "₹" +
						  data.item.fare_breakup[fareIndex].price_per_distance +
						  "/km"}
				</OlaTextRegular>
			</TouchableOpacity>
		);
	}
};

const CabList = props => {
	console.log("props in cablist", props);
	return (
		<View style={styles.mainContainer}>
			{
				<View style={styles.outStationView}>
					<Image style={styles.outStationImage} source={LOCATIONOUTSTATION} />
					<View style={styles.fareDistanceView}>
						{props.isTripOneWay ? (
							<OlaTextMedium style={styles.fareDistanceText}>
								{`${I18n.t("one_way_trip_of_about")} ${
									props.products.ride_estimate.one_way.sub_categories[0]
										.minimum_distance
								} ${I18n.t("km")}`}
							</OlaTextMedium>
						) : null}
						<OlaTextRegular style={styles.messageText}>
							{`\u2714  ${I18n.t("no_pre_payment_required")}`}
						</OlaTextRegular>
						<OlaTextRegular style={styles.messageText}>
							{`\u2714  ${I18n.t("free_cancellation")}`}
						</OlaTextRegular>
					</View>
				</View>
			}
			<Seperator width={width} height={0.5} color={"#777777"} />
			<FlatList
				data={props.products.categories[0].sub_categories}
				keyExtractor={props._keyExtractor}
				ItemSeparatorComponent={() => <Seperator width={width} height={0.5} />}
				renderItem={item => renderItem(item, props)}
			/>
		</View>
	);
};

export default CabList;
