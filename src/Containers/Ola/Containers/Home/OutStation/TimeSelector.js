import React, { Component } from "react";
import { View, FlatList, TouchableOpacity, Dimensions } from "react-native";
import { Icon, ProgressScreen, Seperator } from "../../../../../Components";
import { OlaTextRegular, OlaTextMedium } from "../../../Components/OlaText";
import { timeSelectorStyles as styles } from "./style";
import I18n from "../../../Assets/Strings/i18n";

const { width } = Dimensions.get("window");
//upper limit duration in milliseconds
const MAX_UPPER_LIMIT_DURTION = 518400000;
const EmptyComponent = () => (
	<View style={styles.emptyComponentView}>
		<OlaTextRegular style={styles.emptyComponentText}>
			{`${I18n.t("no_slots_available")}`}
		</OlaTextRegular>
	</View>
);

class TimeSelectorScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedDate: new Date().toDateString("en-In", {
				month: "short",
				year: "numeric"
			}),
			timeOfTheDay: 0,
			selectedTime: null,
			showSpinner: true
		};
		this.unixTime;
		this.handleDateChange = this.handleDateChange.bind(this);
		this.handleTimeOfTheDayChange = this.handleTimeOfTheDayChange.bind(this);
		this.renderWeek = this.renderWeek.bind(this);
		this.renderTimes = this.renderTimes.bind(this);
		this._keyExtractor = this._keyExtractor.bind(this);
		this.handleTimeAndGoBack = this.handleTimeAndGoBack.bind(this);
	}

	componentDidMount() {
		this.unixTime;
		if (this.props.timeType === "leave") {
			this.unixTime = this.props.leaveTime;
		} else if (this.props.returnTime === "SELECT") {
			this.unixTime = this.props.leaveTime + 1000 * 3600 * 8;
		} else {
			this.unixTime = this.props.returnTime;
		}
		let selectedDate = new Date(this.unixTime).toDateString("en-In", {
			month: "short",
			year: "numeric"
		});
		let hours = new Date(this.unixTime).getHours();
		let minutes = new Date(this.unixTime).getMinutes();
		minutes = minutes > 9 ? minutes : `0${minutes}`;
		let timeOfTheDay;
		if (hours > 17) {
			timeOfTheDay = 2;
		} else if (hours > 11) {
			timeOfTheDay = 1;
		}
		let selectedTime = this.tConvert(`${hours}:${minutes}`);
		this.setState({
			showSpinner: false,
			selectedDate: selectedDate,
			timeOfTheDay: timeOfTheDay,
			selectedTime: selectedTime
		});
	}

	_keyExtractor(item, index) {
		return index.toString();
	}
	/**
	 * Convert 24 hout time to 12 hour time
	 * @param {string} time - example input: 18:00
	 */
	tConvert(time) {
		// Check correct time format and split into components
		time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)?$/) || [time];
		if (time.length > 1) {
			// If time format correct
			time = time.slice(1); // Remove full string match value
			time[5] = +time[0] < 12 ? "AM" : "PM"; // Set AM/PM
			time[0] = +time[0] % 12 || 12; // Adjust hours
			time[0] = time[0] > 9 ? time[0] : `0${time[0]}`;
		}
		time.splice(3, 2);
		return time.join(" "); // return adjusted time or original string
	}

	getMonthNameAndYear() {
		const monthNames = [
			"Jan",
			"Feb",
			"Mar",
			"Apr",
			"May",
			"Jun",
			"Jul",
			"Aug",
			"Sep",
			"Oct",
			"Nov",
			"Dec"
		];
		const now = new Date();
		return `${monthNames[now.getMonth()]} ${now.getFullYear()}`;
	}
	/**
	 * Get week array
	 * @returns {[{date: number, day: string, fullDate: Date}]}
	 */
	getCurrentWeek() {
		let now;
		if (this.props.timeType === "leave") {
			now = new Date();
		} else {
			let returnTime = this.props.leaveTime + 1000 * 3600 * 8;
			now = new Date(returnTime);
		}
		const days = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
		let week = [
			{
				date: now.getDate(),
				day: days[now.getDay()],
				fullDate: now.toDateString("en-In", {
					month: "short",
					year: "numeric"
				})
			}
		];
		for (let i = 0; i < 7; i++) {
			now.setDate(now.getDate() + 1);
			week.push({
				date: now.getDate(),
				day: days[now.getDay()],
				fullDate: now.toDateString("en-In", {
					month: "short",
					year: "numeric"
				})
			});
		}
		return week;
	}

	handleDateChange(newDate) {
		this.setState({
			selectedDate: newDate
		});
	}

	handleTimeOfTheDayChange(value) {
		this.setState({
			timeOfTheDay: value
		});
	}

	renderWeek({ item }) {
		const backgroundColor =
			this.state.selectedDate === item.fullDate ? "#0076ff" : "#fff";
		const color = this.state.selectedDate === item.fullDate ? "#fff" : "#000";

		return (
			<TouchableOpacity
				style={styles.renderWeekView}
				onPress={this.handleDateChange.bind(this, item.fullDate)}>
				<OlaTextRegular style={styles.dayText}>{item.day}</OlaTextRegular>
				<View
					style={
						this.state.selectedDate === item.fullDate
							? styles.dateView
							: styles.dateViewUnSelected
					}>
					<OlaTextRegular
						style={
							this.state.selectedDate === item.fullDate
								? styles.dateTextSelected
								: styles.dateTextUnSelected
						}>
						{item.date}
					</OlaTextRegular>
				</View>
			</TouchableOpacity>
		);
	}

	/**
	 *
	 * @param timeOfTheDay number(morning: 0, afternoon: 1, evening: 2)
	 */
	getTimes(timeOfTheDay) {
		let now;
		if (this.props.timeType === "leave") {
			now = new Date();
		} else {
			now = new Date(this.props.leaveTime + 1000 * 3600 * 8);
		}
		let quarterHours = ["00", "15", "30", "45"];
		let times = [[], [], []];
		//comparision of date between current date and selected date
		let compareDate = Date.parse(now) < Date.parse(this.state.selectedDate);
		//the upper limit date available
		let upperLimitDate = new Date(now.getTime() + MAX_UPPER_LIMIT_DURTION);
		//comparision between upper limit date and selected date
		let maxDateCompare =
			Date.parse(this.state.selectedDate) < Date.parse(upperLimitDate);
		for (let i = 0; i < 24; i++) {
			for (let j = 0; j < 4; j++) {
				if (
					(i > now.getHours() || compareDate) &&
					i < 12 &&
					(maxDateCompare ||
						(i <= upperLimitDate.getHours() &&
							quarterHours[j] < upperLimitDate.getMinutes()))
				) {
					let hour = i % 12 || 12;
					let time = `${hour < 10 ? "0" + hour : hour} : ${quarterHours[j]}`;
					times[0].push(time + " AM");
				} else if (
					(i > now.getHours() || compareDate) &&
					i < 18 &&
					(maxDateCompare ||
						(i <= upperLimitDate.getHours() &&
							quarterHours[j] < upperLimitDate.getMinutes()))
				) {
					let hour = i % 12 || 12;
					let time = `${hour < 10 ? "0" + hour : hour} : ${quarterHours[j]}`;
					times[1].push(time + " PM");
				} else if (
					(i > now.getHours() || compareDate) &&
					i < 24 &&
					(maxDateCompare ||
						(i <= upperLimitDate.getHours() &&
							quarterHours[j] < upperLimitDate.getMinutes()))
				) {
					let hour = i % 12 || 12;
					let time = `${hour < 10 ? "0" + hour : hour} : ${quarterHours[j]}`;
					times[2].push(time + " PM");
				}
			}
		}
		return times[timeOfTheDay];
	}

	handleTimeAndGoBack(item) {
		this.setState({ selectedTime: item });
		console.log(item, "-----item-------");
		const formattedTime = item.replace(" ", "");
		const unixTime = Date.parse(this.state.selectedDate + " " + formattedTime);
		// const offSet = new Date().getTimezoneOffset() // hint: offset Indian time is -330 mins
		// const indianUnixTime = unixTime + offSet * 60
		// console.log("INDIAN UNIX TIME: ", unixTime)
		this.props.setTime(unixTime);
		this.props.closeModal();
		this.props.navigation.goBack();
	}

	renderTimes({ item }) {
		return (
			<TouchableOpacity
				style={styles.renderTimeView}
				onPress={() => this.handleTimeAndGoBack(item)}>
				<OlaTextRegular
					style={
						this.state.selectedTime === item
							? styles.renderTimeTextSelected
							: styles.renderTimeTextUnSelected
					}>
					{item}
				</OlaTextRegular>
				{this.state.selectedTime === item ? (
					<Icon
						iconType={"font_awesome"}
						iconName={"check"}
						iconSize={width / 25}
						iconColor={this.state.selectedTime === item ? "#0076ff" : "#000"}
					/>
				) : null}
			</TouchableOpacity>
		);
	}

	render() {
		let tempArr = this.state.selectedDate.split(" ");
		let displayDate = tempArr[1].toUpperCase() + " " + tempArr[3];
		return (
			<View style={{ flex: 1 }}>
				{this.state.showSpinner ? (
					<ProgressScreen indicatorColor={"#cddc39"} />
				) : (
					<View style={styles.container}>
						<View style={styles.goBackView}>
							<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
								<View style={styles.goBackButton}>
									<Icon
										iconType={"material"}
										iconSize={width / 16}
										iconName={"arrow-back"}
										iconColor={"#707070"}
									/>
								</View>
							</TouchableOpacity>
							<OlaTextMedium style={styles.selectDateText}>
								{`${I18n.t("select_date_time")}`}
							</OlaTextMedium>
						</View>
						<View style={styles.displayDateView}>
							<OlaTextMedium style={styles.displayDateText}>
								{displayDate}
							</OlaTextMedium>
							<FlatList
								data={this.getCurrentWeek()}
								renderItem={this.renderWeek}
								extraData={this.state.selectedDate}
								horizontal={true}
								showsHorizontalScrollIndicator={false}
								removeClippedSubviews={false}
								keyextractor={this._keyExtractor}
							/>
						</View>
						<Seperator width={width} height={0.4} color={"#777777"} />
						<View>
							<OlaTextMedium style={styles.selectTimeText}>
								{`${I18n.t("select_time")}`}
							</OlaTextMedium>
							<View style={styles.handleTimeChangeView}>
								<TouchableOpacity
									activeOpacity={1}
									style={[
										styles.button,
										styles.buttonView,
										this.state.timeOfTheDay === 0
											? styles.selectedBackground
											: styles.unSelectedBackground
									]}
									onPress={this.handleTimeOfTheDayChange.bind(this, 0)}>
									<OlaTextRegular
										style={
											this.state.timeOfTheDay === 0
												? styles.selectedColorText
												: styles.unSelectedColorText
										}>
										{`${I18n.t("morning")}`}
									</OlaTextRegular>
								</TouchableOpacity>
								<TouchableOpacity
									activeOpacity={1}
									style={[
										styles.button,
										this.state.timeOfTheDay === 1
											? styles.selectedBackground
											: styles.unSelectedBackground
									]}
									onPress={this.handleTimeOfTheDayChange.bind(this, 1)}>
									<OlaTextRegular
										style={
											this.state.timeOfTheDay === 1
												? styles.selectedColorText
												: styles.unSelectedColorText
										}>
										{`${I18n.t("evening")}`}
									</OlaTextRegular>
								</TouchableOpacity>
								<TouchableOpacity
									activeOpacity={1}
									style={[
										styles.button,
										styles.nightButtonBorder,
										this.state.timeOfTheDay === 2
											? styles.selectedBackground
											: styles.unSelectedBackground
									]}
									onPress={this.handleTimeOfTheDayChange.bind(this, 2)}>
									<OlaTextRegular
										style={
											this.state.timeOfTheDay === 2
												? styles.selectedColorText
												: styles.unSelectedColorText
										}>
										{`${I18n.t("ola_night")}`}
									</OlaTextRegular>
								</TouchableOpacity>
							</View>
						</View>
						<FlatList
							data={this.getTimes(this.state.timeOfTheDay)}
							renderItem={this.renderTimes}
							style={{ padding: width / 25 }}
							ListEmptyComponent={EmptyComponent}
							extraData={this.state.selectedDate}
							keyExtractor={this._keyExtractor}
						/>
					</View>
				)}
			</View>
		);
	}
}

const TimeSelector = props => (
	<TimeSelectorScreen {...props} {...props.route.params} />
);

export default TimeSelector;
