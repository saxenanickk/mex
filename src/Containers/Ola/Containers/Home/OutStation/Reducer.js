import { OLA_SAVE_OUTSTATION_PRODUCTS } from "./Saga";

const initialState = {
	products: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case OLA_SAVE_OUTSTATION_PRODUCTS:
			return {
				...state,
				products: action.payload
			};
		default:
			return state;
	}
};
