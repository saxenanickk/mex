import { Dimensions, StyleSheet } from "react-native";
const { width, height } = Dimensions.get("window");

/**
 * styles for index.js
 */
export const indexStyles = StyleSheet.create({
	locationDetails: {
		backgroundColor: "#fff"
	},
	locationDetailsText: {
		padding: 10
	},
	tripDetails: {
		backgroundColor: "#fff"
	},
	tripType: {
		flexDirection: "row",
		justifyContent: "flex-start"
	},
	tripLeaveOn: {
		width: width,
		padding: width / 25,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between"
	},
	tripReturn: {
		width: width,
		padding: width / 25,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between"
	},
	tripTimeText: {
		color: "#0076ff",
		fontSize: width / 25
	},
	mainContainer: {
		width: width,
		elevation: 5,
		backgroundColor: "#ffffff",
		justifyContent: "flex-start",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		shadowOffset: {
			width: 2,
			height: 2
		},
		shadowColor: "grey",
		shadowOpacity: 1,
		shadowRadius: 4
	},
	goBackButton: {
		justifyContent: "center",
		width: width / 6,
		alignItems: "center",
		height: height / 13.33
	},
	bookYourRideText: {
		color: "#000000",
		fontSize: height / 45.71,
		marginLeft: width / 50
	},
	originLocationTextStyle: {
		// height: height / 21.62,
		borderBottomWidth: 0.4,
		borderColor: "#d9d9d9"
	},
	destinationTextStyle: {
		// height: height / 21.62,
	},
	dateTimeView: {
		paddingHorizontal: width / 25,
		paddingTop: width / 25,
		paddingBottom: width / 50
	},
	dateTimeText: {
		fontSize: width / 30
	},
	tripTypeButton: {
		width: width / 2,
		padding: width / 25,
		flexDirection: "row",
		justifyContent: "flex-start"
	},
	roundTripView: { marginLeft: width / 35 },
	oneWayText: {
		fontSize: width / 25,
		color: "#000000"
	},
	droppedOffText: {
		marginTop: width / 50,
		fontSize: width / 35
	},
	roundTripTypeButton: {
		width: width / 2,
		paddingVertical: width / 25,
		flexDirection: "row",
		justifyContent: "flex-start"
	},
	centerAlign: {
		alignItems: "center"
	},
	leaveOnText: {
		fontSize: width / 25,
		color: "#000000"
	},
	selectVehicleView: {
		paddingHorizontal: width / 25,
		paddingTop: width / 25,
		paddingBottom: width / 50
	},
	selectVehicleText: {
		fontSize: width / 30
	},
	dialogModalBackgroundStyle: {
		backgroundColor: "#000",
		opacity: 0.3
	},
	dialogModalBackgroundView: {
		height: height / 4,
		top: height / 3,
		width: width / 1.3,
		borderColor: "#ffffff",
		borderRadius: width / 140,
		elevation: 5,
		left: width / 8,
		position: "absolute",
		justifyContent: "flex-start",
		alignItems: "center",
		backgroundColor: "#ffffff"
	}
});

/**
 * styles for CabList.js
 */

export const cabListStyles = StyleSheet.create({
	tripOneWayButton: {
		flexDirection: "row",
		paddingHorizontal: width / 25,
		paddingVertical: width / 150,
		alignItems: "center",
		justifyContent: "space-between"
	},
	tripNotOneWayButton: {
		flexDirection: "row",
		paddingHorizontal: width / 25,
		paddingVertical: width / 100,
		alignItems: "center",
		justifyContent: "space-between"
	},
	centeredRow: { flexDirection: "row", alignItems: "center" },
	cabImage: {
		width: width / 10,
		height: width / 20
	},
	displayNameView: {
		width: width / 1.6,
		padding: width / 35,
		marginLeft: width / 35
	},
	displayNameText: {
		fontSize: width / 25,
		color: "#000000"
	},
	availableCabText: {
		marginTop: height / 100,
		fontSize: width / 30
	},
	fareText: {
		fontSize: width / 25,
		color: "#000000"
	},
	mainContainer: {
		elevation: 2,
		borderColor: "#ffffff",
		backgroundColor: "#ffffff",
		width: width / 1.04,
		alignSelf: "center",
		marginBottom: width - width / 1.02
	},
	outStationView: {
		paddingHorizontal: width / 25,
		paddingVertical: width / 50,
		backgroundColor: "#eeffe3",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "flex-start"
	},
	outStationImage: {
		width: width / 15,
		height: width / 15
	},
	fareDistanceView: { marginLeft: width / 13 },
	fareDistanceText: {
		fontSize: width / 30,
		color: "#65bf2b"
	},
	messageText: {
		color: "#65bf2b",
		fontSize: width / 35
	}
});

/**
 * styles for ConfirmBooking.js
 */

export const confirmBookingStyles = StyleSheet.create({
	headerBar: {
		position: "absolute",
		elevation: 5,
		backgroundColor: "#ffffff",
		justifyContent: "flex-start",
		alignItems: "center",
		padding: 10,
		flexDirection: "row",
		width: width / 1,
		height: height / 12
	},
	tripLeaveOn: {
		width: width,
		backgroundColor: "#ffffff",
		paddingHorizontal: width / 25,
		paddingVertical: width / 35,
		flexDirection: "row",
		justifyContent: "space-between"
	},
	tripTimeText: {
		color: "#757575",
		fontSize: width / 25
	},
	highlightsIcon: {
		flexDirection: "row",
		paddingHorizontal: width / 30
	},
	bullet: {
		width: 4,
		height: 4,
		borderRadius: 2,
		backgroundColor: "rgba(0,0,0,0.5)"
	},
	ruleStyle: {
		fontSize: width / 33,
		color: "rgba(0,0,0,0.5)"
	},
	mainFareContainer: {
		width: width,
		backgroundColor: "#fff"
	},
	baseFareContainer: {
		flexDirection: "row",
		flex: 1,
		justifyContent: "space-between",
		paddingTop: height / 40
	},
	baseFareView: {
		marginLeft: width / 30
	},
	baseFareText: {
		fontSize: width / 28,
		color: "#000"
	},
	twoWayText: {
		fontSize: width / 33,
		color: "rgba(0,0,0,0.5)"
	},
	twoWayFareText: {
		fontSize: width / 28,
		color: "#000",
		marginRight: width / 30
	},
	extraDistanceView: {
		flexDirection: "row",
		marginTop: width / 40,
		justifyContent: "space-between"
	},
	extraDistanceInnerView: {
		marginLeft: width / 30
	},
	extraFareText: {
		fontSize: width / 28,
		color: "#000"
	},
	taxAndFeeView: {
		paddingVertical: height / 40
	},
	fullCenteredRow: {
		flexDirection: "row",
		justifyContent: "space-between",
		flex: 1
	},
	taxText: {
		fontSize: width / 28,
		color: "#000",
		marginLeft: width / 30
	},
	taxInfoText: {
		fontSize: width / 33,
		color: "#000",
		marginRight: width / 30
	},
	optionText: {
		fontSize: width / 28,
		color: "rgba(0,0,0,0.5)",
		marginLeft: width / 30
	},
	optionInfoText: {
		fontSize: width / 33,
		color: "rgba(0,0,0,0.5)",
		marginRight: width / 30
	},
	estimatedFareView: {
		flexDirection: "row",
		flex: 1,
		borderWidth: 1,
		paddingVertical: height / 40,
		borderTopWidth: 0.6,
		borderColor: "#777777",
		justifyContent: "space-between",
		backgroundColor: "#ffffff"
	},
	estimatedFareText: {
		fontSize: width / 25,
		color: "#000",
		marginLeft: width / 30
	},
	estimatedFareInfo: {
		fontSize: width / 25,
		color: "#000",
		marginRight: width / 30
	},
	fareRuleView: {
		paddingHorizontal: width / 30,
		paddingBottom: width / 30,
		marginTop: height / 30
	},
	centeredRow: { flexDirection: "row", alignItems: "center" },
	fareRuleMargin: { marginLeft: width / 40 },
	mainContainer: {
		width: width,
		elevation: 5,
		backgroundColor: "#ffffff",
		justifyContent: "flex-start",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33
	},
	goBack: {
		justifyContent: "center",
		width: width / 6,
		alignItems: "center",
		height: height / 13.33
	},
	bookingConfirm: {
		color: "#000000",
		fontSize: height / 45.71,
		marginLeft: width / 50
	},
	cabImageView: {
		width: width,
		backgroundColor: "#ffffff",
		flexDirection: "row",
		alignItems: "center",
		padding: width / 35
	},
	cabImage: {
		width: width / 10,
		height: width / 20
	},
	displayNameView: {
		width: width / 2,
		padding: width / 35,
		marginLeft: width / 35
	},
	displayNameText: {
		fontSize: width / 25,
		color: "#000000"
	},
	availableCabText: {
		color: "#757575",
		fontSize: width / 30
	},
	featuredView: {
		flexDirection: "row",
		paddingBottom: width / 20,
		marginLeft: width / 35
	},
	featuredImage: {
		width: width / 12,
		height: width / 22
	},
	featuredText: {
		fontSize: width / 30,
		marginLeft: width / 80
	},
	originLocation: {
		elevation: 0
	},
	destinationTextStyle: {
		elevation: 0
	},
	leaveOnText: {
		fontSize: width / 25,
		color: "#757575"
	},
	returnByText: {
		fontSize: width / 25,
		color: "#757575"
	},
	tripTypeView: {
		width: width,
		backgroundColor: "#ffffff",
		paddingHorizontal: width / 25,
		paddingVertical: width / 50,
		justifyContent: "center",
		alignItems: "center"
	},
	selectedCabFareText: {
		fontSize: width / 28,
		color: "#000",
		marginTop: width / 30,
		alignSelf: "center"
	},
	estimatedFareInnerBorder: { width: width / 3 },
	estimatedFareInnerView: {
		width: width / 3,
		alignItems: "center",
		justifyContent: "center"
	},
	lowestInFareImageView: {
		width: width / 3,
		justifyContent: "center",
		alignItems: "center"
	},
	lowestInFareImage: {
		width: width / 8,
		height: width / 8
	},
	seeFareDetailButton: {
		width: width,
		paddingVertical: width / 35,
		backgroundColor: "#fff",
		justifyContent: "center",
		alignItems: "center"
	},
	seeFareDetailText: {
		fontSize: width / 30,
		color: "#0E89DF"
	},
	freeCancellationView: {
		backgroundColor: "#eeffe3",
		width: width,
		paddingVertical: width / 50,
		borderColor: "#777777",
		flexDirection: "row",
		alignItems: "center",
		paddingHorizontal: width / 25
	},
	freeCancellationText: {
		fontSize: width / 30,
		color: "#65bf2b"
	},
	knowMoreText: {
		fontSize: width / 30,
		color: "#65bf2b"
	},
	oneCarMultipleStopView: {
		backgroundColor: "#ffffff",
		flexDirection: "row",
		height: height / 8.13,
		paddingHorizontal: width / 25,
		width: width,
		alignItems: "center"
	},
	oneCarMultipleStopImage: {
		width: width / 4,
		height: height / 9.5
	},
	auditedCarView: {
		paddingVertical: width / 35,
		height: height / 8.13,
		marginLeft: width / 20,
		justifyContent: "space-around"
	},
	iconText: {
		color: "#202020"
	},
	iconInfoText: {
		fontSize: height / 56,
		width: width / 1.6,
		color: "#202020"
	},
	borderHeight: { height: height / 7.5 },
	fullWhiteView: { backgroundColor: "#ffffff", flex: 1 },
	normalLocationText: {
		color: "#757575"
	}
});

/**
 * style for TimeSelector.js
 */

export const timeSelectorStyles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#ffffff"
	},
	header: {
		elevation: 5,
		backgroundColor: "#ffffff",
		justifyContent: "space-between",
		alignItems: "center",
		padding: 10,
		flexDirection: "row",
		width: width / 1,
		height: height / 12
	},
	headerText: {
		paddingLeft: 50,
		paddingTop: 5,
		fontSize: 20
	},
	button: {
		borderColor: "#0076ff",
		width: width / 3.2,
		padding: width / 50,
		borderWidth: 2,
		borderLeftWidth: 0,
		justifyContent: "center",
		alignItems: "center"
	},
	emptyComponentView: {
		alignItems: "center",
		justifyContent: "center"
	},
	emptyComponentText: {
		fontSize: width / 22
	},
	renderWeekView: {
		alignItems: "center",
		paddingHorizontal: width / 25,
		height: height / 10
	},
	dayText: {
		paddingBottom: width / 35
	},
	dateView: {
		backgroundColor: "#0076ff",
		justifyContent: "center",
		alignItems: "center",
		width: width / 14,
		height: width / 14,
		borderRadius: width / 28
	},
	dateViewUnSelected: {
		backgroundColor: "#fff",
		justifyContent: "center",
		alignItems: "center",
		width: width / 14,
		height: width / 14,
		borderRadius: width / 28
	},
	dateTextSelected: {
		color: "#fff",
		fontSize: width / 25
	},
	dateTextUnSelected: {
		color: "#000",
		fontSize: width / 25
	},
	renderTimeView: {
		padding: width / 35,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	renderTimeTextSelected: {
		fontSize: width / 25,
		color: "#0076ff"
	},
	renderTimeTextUnSelected: {
		fontSize: width / 25,
		color: "#000"
	},
	goBackView: {
		width: width,
		elevation: 5,
		backgroundColor: "#ffffff",
		justifyContent: "flex-start",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33
	},
	goBackButton: {
		justifyContent: "center",
		width: width / 6,
		alignItems: "center",
		height: height / 13.33
	},
	selectDateText: {
		color: "#000000",
		fontSize: height / 45.71,
		marginLeft: width / 50
	},
	displayDateView: {
		elevation: 0,
		backgroundColor: "#ffffff",
		marginBottom: width / 35
	},
	displayDateText: {
		padding: width / 25,
		fontSize: height / 48
	},
	selectTimeText: {
		paddingHorizontal: width / 25,
		paddingTop: width / 15,
		paddingBottom: width / 35,
		fontSize: height / 48
	},
	handleTimeChangeView: {
		width: width,
		paddingHorizontal: width / 25,
		flexDirection: "row",
		alignItems: "center"
	},
	buttonView: {
		borderLeftWidth: 1,
		borderTopLeftRadius: width / 50,
		borderBottomLeftRadius: width / 50
	},
	selectedBackground: {
		borderWidth: 1,
		backgroundColor: "#0076ff"
	},
	unSelectedBackground: {
		borderWidth: 1,
		backgroundColor: "#fff"
	},
	selectedColorText: {
		fontSize: width / 30,
		color: "#fff"
	},
	unSelectedColorText: {
		fontSize: width / 30,
		color: "#0076ff"
	},
	nightButtonBorder: {
		borderWidth: 1,
		borderTopRightRadius: width / 50,
		borderBottomRightRadius: width / 50
	}
});
