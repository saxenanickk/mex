import React, { Component } from "react";
import {
	View,
	StyleSheet,
	ScrollView,
	TouchableOpacity,
	Dimensions,
	TouchableWithoutFeedback
} from "react-native";
import { indexStyles as styles } from "./style";
import { LocationText } from "../../../Components";
import I18n from "../../../Assets/Strings/i18n";
import { connect } from "react-redux";
import {
	Seperator,
	Icon,
	ProgressScreen,
	DialogModal
} from "../../../../../Components";
import { olaGetOutstationProducts, olaSaveOutstationProducts } from "./Saga";
import CabList from "./CabList";
import {
	OlaTextMedium,
	OlaTextRegular,
	OlaTextBold
} from "../../../Components/OlaText";
import GoAppAnalytics from "../../../../../utils/GoAppAnalytics";
import { GoToast } from "../../../../../Components";

const { width, height } = Dimensions.get("window");

const ONE_DAY_DURATION_IN_SEC = 86400;
class OutStation extends Component {
	static navigationOptions = {
		title: "Book Your Outstation Ride"
	};

	constructor(props) {
		super(props);
		this.state = {
			isTripOneWay: true,
			leaveTime: null,
			returnTime: null,
			isOneWayAvailable: true,
			readyToRender: false,
			sameCityError: false
		};
		this.selectedCab = null;
		this.tripId = null;
		this.options = null;
		this.duration = 1;
		this.timeType = null;
		this.onlyRoundTrip = false;
		this.leaveTimeStamp = null;
		this.returnTimeStamp = "SELECT";
		this.handleTimeClick = this.handleTimeClick.bind(this);
		this.formatAMPM = this.formatAMPM.bind(this);
		this.getReadableTime = this.getReadableTime.bind(this);
	}

	shouldComponentUpdate(props, state) {
		if (props.products !== null && typeof props.products.code !== "undefined") {
			GoToast.show(props.products.message, I18n.t("information"));
			this.props.navigation.goBack();
			return false;
		}
		if (props.products !== null && props.products !== this.props.products) {
			if (
				props.products.ride_estimate.one_way.hasOwnProperty("sub_categories")
			) {
				this.setState({
					isTripOneWay: true && this.state.isTripOneWay,
					readyToRender: true
				});
			} else {
				this.setState({
					isTripOneWay: false,
					isOneWayAvailable: false,
					readyToRender: true
				});
			}
		}
		if (
			state.leaveTime !== this.state.leaveTime ||
			state.returnTime !== this.state.returnTime
		) {
			if (!this.state.isTripOneWay && this.returnTimeStamp !== "SELECT") {
				let date1 = new Date(this.leaveTimeStamp);
				let date2 = new Date(this.returnTimeStamp);
				let timeDiff = date2 - date1;
				this.duration = Math.ceil(timeDiff / (1000 * 3600 * 24));
			} else {
				this.duration = 0;
			}
		}
		return true;
	}

	componentDidMount() {
		this._unsubscribeFocus = this.props.navigation.addListener("focus", () => {
			GoAppAnalytics.setPageView("ola", "outstation_screen");
		});

		let nowDate = new Date();
		nowDate.setHours(nowDate.getHours() + 1);
		let mins = nowDate.getMinutes();
		if (mins < 15) {
			mins = 0;
		} else if (mins < 30) {
			mins = 15;
		} else if (mins < 45) {
			mins = 30;
		} else {
			mins = 45;
		}
		nowDate.setMinutes(mins);
		let now = this.getReadableTime(Date.parse(nowDate));
		this.leaveTimeStamp = Date.parse(nowDate);
		this.props.dispatch(
			olaGetOutstationProducts({
				access_token: this.props.userToken,
				pickup_lat: this.props.origin.latitude,
				pickup_lng: this.props.origin.longitude,
				drop_lat: this.props.destination.latitude,
				drop_lng: this.props.destination.longitude,
				pickup_mode: "now",
				pickup_time: this.leaveTimeStamp / 1000,
				drop_time: this.leaveTimeStamp / 1000 + ONE_DAY_DURATION_IN_SEC,
				appToken: this.props.appToken
			})
		);
		this.setState({
			leaveTime: now
		});
	}

	componentWillUnmount() {
		this._unsubscribeFocus();
		this.props.dispatch(olaSaveOutstationProducts(null));
	}

	selectedLeaveTime(time) {
		console.log(time);
		this.setState({
			leaveTime: time
		});
	}

	handleTimeClick(timeType) {
		this.timeType = timeType;
		this.props.navigation.navigate("TimeSelector", {
			timeType: this.timeType,
			leaveTime: this.leaveTimeStamp,
			returnTime: this.returnTimeStamp,
			setTime: time => {
				if (this.timeType === "leave") {
					this.leaveTimeStamp = time;
					this.returnTimeStamp = time + 1000 * 3600 * 8;
				} else {
					this.returnTimeStamp = time;
				}
				this.timeType === "leave"
					? this.setState({
							leaveTime: this.getReadableTime(time),
							returnTime: this.getReadableTime(this.returnTimeStamp)
					  })
					: this.setState({ returnTime: this.getReadableTime(time) });
			},
			closeModal: () => {
				this.props.dispatch(
					olaGetOutstationProducts({
						access_token: this.props.userToken,
						pickup_lat: this.props.origin.latitude,
						pickup_lng: this.props.origin.longitude,
						drop_lat: this.props.destination.latitude,
						drop_lng: this.props.destination.longitude,
						pickup_mode: "later",
						pickup_time: this.leaveTimeStamp / 1000,
						drop_time: this.returnTimeStamp / 1000,
						appToken: this.props.appToken
					})
				);
			}
		});
	}

	formatAMPM(date) {
		let hours = date.getHours();
		let minutes = date.getMinutes();
		let ampm = hours >= 12 ? "PM" : "AM";
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? "0" + minutes : minutes;
		let strTime = hours + ":" + minutes + " " + ampm;
		return strTime;
	}

	getReadableTime(time) {
		const monthNames = [
			"Jan",
			"Feb",
			"Mar",
			"Apr",
			"May",
			"Jun",
			"Jul",
			"Aug",
			"Sep",
			"Oct",
			"Nov",
			"Dec"
		];
		time = new Date(time);
		let month = time.getMonth();
		let date = time.getDate();
		let timeInAmPm = this.formatAMPM(time);
		return `${date}, ${monthNames[month]}, ${timeInAmPm}`;
	}

	handleCabClick = (data, tripId) => {
		let type = this.state.isTripOneWay ? "one_way" : "two_way";
		if (!this.state.isTripOneWay && this.state.returnTime === null) {
			return this.handleTimeClick("return");
		}
		let olaLeaveTime = new Date(this.leaveTimeStamp);
		let leaveMonth = olaLeaveTime.getMonth();
		if (leaveMonth < 9) {
			leaveMonth = leaveMonth + 1;
			leaveMonth = "0" + leaveMonth;
		} else {
			leaveMonth += 1;
		}
		let leaveMinutes = olaLeaveTime.getMinutes();
		leaveMinutes = leaveMinutes === 0 ? `0${leaveMinutes}` : leaveMinutes;
		let leaveTime = `${olaLeaveTime.getDate()}/${leaveMonth}/${olaLeaveTime.getFullYear()} ${olaLeaveTime.getHours()}:${leaveMinutes}`;
		this.options = {
			pickup_mode: "LATER",
			category: "outstation",
			sub_category: data.id,
			pickup_time: leaveTime,
			epoch_leave_time: this.leaveTimeStamp,
			epoch_return_time: this.returnTimeStamp,
			type: type,
			trip_id: tripId,
			id: "outstation"
		};
		if (type === "two_way") {
			let olaReturnTime = new Date(this.returnTimeStamp);
			let returnMonth = olaReturnTime.getMonth();
			if (returnMonth < 9) {
				returnMonth += 1;
				returnMonth = `0${returnMonth}`;
			} else {
				returnMonth += 1;
			}
			let returnMinutes = olaReturnTime.getMinutes();
			returnMinutes = returnMinutes === 0 ? `0${returnMinutes}` : returnMinutes;
			let returnTime = `${olaReturnTime.getDate()}/${returnMonth}/${olaReturnTime.getFullYear()} ${olaReturnTime.getHours()}:${returnMinutes}`;
			this.options = { ...this.options, drop_time: returnTime };
		}
		this.selectedCab = data;
		this.tripId = tripId;
		GoAppAnalytics.trackWithProperties("ola-outstation-selected-cab", {
			category: data.id
		});
		this.props.navigation.navigate("ConfirmBooking", {
			options: this.options,
			selectedCab: this.selectedCab,
			tripId: this.tripId,
			getReadableTime: this.getReadableTime,
			isOneWayAvailable: this.state.isOneWayAvailable
		});
	};

	handleTripType = isOneWay => {
		if (this.state.isOneWayAvailable) {
			this.setState({
				isTripOneWay: isOneWay
			});
			this.duration = isOneWay === true ? 0 : 1;
		} else {
			if (isOneWay === true) {
				GoToast.show(
					I18n.t("one_way_not_message"),
					I18n.t("information"),
					"LONG"
				);
			}
		}
	};

	_keyExtractor = (item, index) => index.toString();

	render() {
		return (
			<View style={StyleSheet.absoluteFill}>
				<View style={styles.mainContainer}>
					<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
						<View style={styles.goBackButton}>
							<Icon
								iconType={"material"}
								iconSize={width / 16}
								iconName={"arrow-back"}
								iconColor={"#707070"}
							/>
						</View>
					</TouchableOpacity>
					<OlaTextMedium style={styles.bookYourRideText}>
						{I18n.t("book_your_ride")}
					</OlaTextMedium>
				</View>
				<ScrollView>
					<LocationText
						locationTextStyle={styles.originLocationTextStyle}
						placeholder={I18n.t("from")}
						locationType={"origin"}
						locationText={this.props.origin.address}
					/>
					<LocationText
						locationTextStyle={styles.destinationTextStyle}
						placeholder={I18n.t("where_to")}
						locationType={"destination"}
						locationText={this.props.destination.address}
					/>
					<View style={styles.dateTimeView}>
						<OlaTextMedium style={styles.dateTimeText}>
							{`${I18n.t("select_date_time")}`}
						</OlaTextMedium>
					</View>
					<Seperator width={width} height={0.5} color={"#777777"} />
					<View style={styles.tripDetails}>
						<View style={styles.tripType}>
							<TouchableOpacity
								activeOpacity={1}
								onPress={this.handleTripType.bind(this, true)}>
								<View style={styles.tripTypeButton}>
									<Icon
										iconType={"font_awesome"}
										iconName={
											!this.state.isTripOneWay ? "circle-o" : "dot-circle-o"
										}
										iconSize={width / 16}
										iconColor={!this.state.isTripOneWay ? "#000" : "#0076ff"}
									/>
									<View style={styles.roundTripView}>
										<OlaTextRegular style={styles.oneWayText}>
											{I18n.t("one_way")}
										</OlaTextRegular>
										<OlaTextRegular style={styles.droppedOffText}>
											{I18n.t("get_dropped_off")}
										</OlaTextRegular>
									</View>
								</View>
							</TouchableOpacity>
							<TouchableOpacity
								activeOpacity={1}
								onPress={this.handleTripType.bind(this, false)}>
								<View style={styles.roundTripTypeButton}>
									<Icon
										iconType={"font_awesome"}
										iconName={
											this.state.isTripOneWay ? "circle-o" : "dot-circle-o"
										}
										iconSize={width / 16}
										iconColor={this.state.isTripOneWay ? "#000" : "#0076ff"}
									/>
									<View style={styles.roundTripView}>
										<OlaTextRegular style={styles.oneWayText}>
											{I18n.t("round_trip")}
										</OlaTextRegular>
										<OlaTextRegular style={styles.droppedOffText}>
											{I18n.t("keep_car")}
										</OlaTextRegular>
									</View>
								</View>
							</TouchableOpacity>
						</View>
						<View style={styles.centerAlign}>
							<Seperator width={width / 1.08} height={0.4} color={"#777777"} />
							<View style={styles.tripLeaveOn}>
								<OlaTextRegular style={styles.leaveOnText}>
									{I18n.t("leave_on")}
								</OlaTextRegular>
								<TouchableOpacity
									activeOpacity={1}
									onPress={() => this.handleTimeClick("leave")}>
									<OlaTextRegular style={styles.tripTimeText}>
										{this.state.leaveTime}
									</OlaTextRegular>
								</TouchableOpacity>
							</View>
							{!this.state.isTripOneWay ? (
								<Seperator
									width={width / 1.08}
									height={0.4}
									color={"#777777"}
								/>
							) : null}
							{!this.state.isTripOneWay ? (
								<View style={styles.tripReturn}>
									<OlaTextRegular style={styles.oneWayText}>
										{I18n.t("return_by")}
									</OlaTextRegular>
									<TouchableOpacity
										activeOpacity={1}
										onPress={() => this.handleTimeClick("return")}>
										<OlaTextRegular style={styles.tripTimeText}>
											{this.state.returnTime === null
												? I18n.t("select")
												: this.state.returnTime}
										</OlaTextRegular>
									</TouchableOpacity>
								</View>
							) : null}
							<Seperator width={width} height={0.4} color={"#777777"} />
						</View>
					</View>
					{this.props.products !== null &&
					typeof this.props.products.code === "undefined" ? (
						<View>
							<View style={styles.selectVehicleView}>
								<OlaTextMedium style={styles.selectVehicleText}>
									{I18n.t("select_vehicle")}
								</OlaTextMedium>
							</View>
							{this.state.readyToRender === true ? (
								<CabList
									duration={
										this.duration === 0 && !this.state.isTripOneWay
											? 1
											: this.duration
									}
									products={this.props.products}
									isTripOneWay={this.state.isTripOneWay}
									isOneWayAvailable={this.state.isOneWayAvailable}
									_keyExtractor={this._keyExtractor}
									handleCabClick={this.handleCabClick}
								/>
							) : null}
						</View>
					) : null}
				</ScrollView>
				{!this.state.readyToRender ? (
					<ProgressScreen indicatorColor={"#cddc39"} />
				) : null}
				{this.state.sameCityError ? (
					<DialogModal
						dialogModalBackgroundStyle={styles.dialogModalBackgroundStyle}>
						<View style={styles.dialogModalBackgroundView}>
							<OlaTextBold style={{ padding: height / 60 }}>
								{I18n.t("oops")}
							</OlaTextBold>
							<View style={{ flexDirection: "row" }}>
								<Seperator color={"#777777"} width={width / 1.5} height={0.8} />
							</View>
							<View style={{ padding: width / 18 }}>
								<OlaTextRegular>{this.props.products.message}</OlaTextRegular>
							</View>
							<View style={{ flexDirection: "row", alignItems: "flex-end" }}>
								<TouchableWithoutFeedback
									onPress={() => this.props.navigation.goBack()}>
									<View
										style={{
											backgroundColor: "#0f0f0f",
											flex: 1,
											justifyContent: "center",
											alignItems: "center",
											height: height / 14
										}}>
										<OlaTextMedium
											style={{ color: "#CCE500", fontSize: width / 30 }}>
											{"OK"}
										</OlaTextMedium>
									</View>
								</TouchableWithoutFeedback>
							</View>
						</View>
					</DialogModal>
				) : null}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		origin: state.ola && state.ola.bookRide.origin,
		products: state.ola && state.ola.outstation.products,
		userToken: state.ola && state.ola.auth.userCredentials,
		destination: state.ola && state.ola.bookRide.destination
	};
}

export default connect(mapStateToProps)(OutStation);
