import React from "react";
import {
	View,
	ScrollView,
	Dimensions,
	TouchableOpacity,
	Image,
	FlatList,
	BackHandler
} from "react-native";
import { connect } from "react-redux";
import { Icon, Seperator } from "../../../../../Components";
import {
	MINI,
	VALUEFORMONEY,
	AC,
	SPACIOUS,
	TOPRATEDPARTNERS,
	LOWESTINFARES,
	ONECARMULTIPLESTOPS,
	PRIME,
	SEDAN,
	SUV,
	MICRO
} from "../../../Assets/Img/Image";
import I18n from "../../../Assets/Strings/i18n";
import { LocationText } from "../../../Components";
import ConfirmRide from "../../ChooseRide/ConfirmRide";
import {
	OlaTextRegular,
	OlaTextMedium,
	OlaTextBold
} from "../../../Components/OlaText";
import { confirmBookingStyles as styles } from "./style";
import { GoToast } from "../../../../../Components";
import GoAppAnalytics from "../../../../../utils/GoAppAnalytics";

const { width, height } = Dimensions.get("window");

const featuresImage = {
	"Value For Money": VALUEFORMONEY,
	AC: AC,
	"Top Rated Partners": TOPRATEDPARTNERS,
	Spacious: SPACIOUS,
	WiFi: AC
};

const carImages = {
	micro: MICRO,
	mini: MINI,
	prime_play: PRIME,
	prime: SEDAN,
	suv: SUV
};
class ConfirmBookingScreen extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			fareDetails: false
		};
		this.handleBackPress = this.handleBackPress.bind(this);
	}

	componentWillMount() {
		BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
	}

	handleBackPress() {
		this.props.navigation.goBack();
		return true;
	}

	getAvailableCabType = cabModels => {
		let cab = "";
		if (cabModels !== undefined) {
			cabModels.map((item, index) => {
				let splitCabName = item.split(" ");
				cab = cab + splitCabName[splitCabName.length - 1];
				if (index < cabModels.length - 1) {
					cab = cab + ",";
				}
			});
			return cab;
		}
	};

	renderFareDetails = props => {
		let fareIndex = props.isOneWayAvailable === true ? 1 : 0;
		let taxes_and_fees =
			props.options.type === "two_way"
				? Math.round(
						props.products.ride_estimate[props.options.type].sub_categories[
							props.tripId
						].day_allowance_charge +
							props.products.ride_estimate[props.options.type].sub_categories[
								props.tripId
							].night_charge +
							props.products.ride_estimate[props.options.type].sub_categories[
								props.tripId
							].taxes
				  )
				: Math.round(
						props.products.ride_estimate[props.options.type].sub_categories[
							props.tripId
						].day_allowance_charge +
							props.products.ride_estimate[props.options.type].sub_categories[
								props.tripId
							].night_allowance_charge +
							props.products.ride_estimate[props.options.type].sub_categories[
								props.tripId
							].taxes
				  );

		return (
			<View style={styles.mainFareContainer}>
				{/* for base fare*/}
				<View style={styles.baseFareContainer}>
					<View style={styles.baseFareView}>
						<OlaTextRegular style={styles.baseFareText}>
							{I18n.t("base_fare")}
						</OlaTextRegular>
						<OlaTextRegular style={styles.twoWayText}>
							{props.options.type === "two_way"
								? Math.round(
										props.products.ride_estimate.two_way.sub_categories[
											props.tripId
										].minimum_distance
								  ) +
								  " km x ₹" +
								  props.selectedCab.fare_breakup[fareIndex].price_per_distance
								: null}
						</OlaTextRegular>
					</View>
					<OlaTextRegular style={styles.twoWayFareText}>
						{props.options.type === "two_way"
							? "₹" +
							  Math.round(
									props.products.ride_estimate.two_way.sub_categories[
										props.tripId
									].minimum_distance
							  ) *
									props.selectedCab.fare_breakup[fareIndex].price_per_distance
							: "₹" + props.selectedCab.fare_breakup[0].base_fare}
					</OlaTextRegular>
				</View>
				{/* if extra distance is travelled */}
				{props.products.ride_estimate[props.options.type].sub_categories[
					props.tripId
				].extra_distance > 0 ? (
					<View style={styles.extraDistanceView}>
						<View style={styles.extraDistanceInnerView}>
							<OlaTextRegular style={styles.extraFareText}>
								{I18n.t("fare_for_remaining_km")}
							</OlaTextRegular>
							<OlaTextRegular style={styles.twoWayText}>
								{props.options.type === "two_way"
									? Math.round(
											props.products.ride_estimate.two_way.sub_categories[
												props.tripId
											].extra_distance
									  ) +
									  " km x ₹" +
									  props.selectedCab.fare_breakup[fareIndex]
											.price_per_distance +
									  I18n.t("charged_only_if_travelled")
									: null}
							</OlaTextRegular>
						</View>
						<OlaTextRegular style={styles.twoWayFareText}>
							{props.options.type === "two_way"
								? "₹" +
								  Math.round(
										props.products.ride_estimate.two_way.sub_categories[
											props.tripId
										].extra_distance_charge
								  )
								: "₹" +
								  Math.round(
										props.products.ride_estimate.one_way.sub_categories[
											props.tripId
										].extra_distance_charge
								  )}
						</OlaTextRegular>
					</View>
				) : null}
				{/* for taxes and fees*/}
				<View style={styles.taxAndFeeView}>
					<View style={styles.fullCenteredRow}>
						<OlaTextRegular style={styles.taxText}>
							{I18n.t("taxes_and_fees")}
						</OlaTextRegular>
						<OlaTextRegular style={styles.taxInfoText}>
							{props.options.type && "₹" + taxes_and_fees}
						</OlaTextRegular>
					</View>
					<View style={styles.fullCenteredRow}>
						<OlaTextRegular style={styles.optionText}>
							{I18n.t("driver_allowance")}
						</OlaTextRegular>
						<OlaTextRegular style={styles.optionInfoText}>
							{props.options.type &&
								"₹" +
									Math.round(
										props.products.ride_estimate[props.options.type]
											.sub_categories[props.tripId].day_allowance_charge
									)}
						</OlaTextRegular>
					</View>
					<View style={styles.fullCenteredRow}>
						<OlaTextRegular style={styles.optionText}>
							{I18n.t("night_time_allowance")}
						</OlaTextRegular>
						<OlaTextRegular style={styles.optionInfoText}>
							{props.options.type && props.options.type == "two_way"
								? "₹" +
								  Math.round(
										props.products.ride_estimate[props.options.type]
											.sub_categories[props.tripId].night_charge
								  )
								: "₹" +
								  Math.round(
										props.products.ride_estimate[props.options.type]
											.sub_categories[props.tripId].night_allowance_charge
								  )}
						</OlaTextRegular>
					</View>
					<View style={styles.fullCenteredRow}>
						<OlaTextRegular style={styles.optionText}>
							{I18n.t("taxes")}
						</OlaTextRegular>
						<OlaTextRegular style={styles.optionInfoText}>
							{props.options.type &&
								"₹" +
									Math.round(
										props.products.ride_estimate[props.options.type]
											.sub_categories[props.tripId].taxes
									)}
						</OlaTextRegular>
					</View>
				</View>
				{/* for estimated fare */}
				<View style={styles.fullCenteredRow}>
					<OlaTextRegular style={styles.estimatedFareText}>
						{I18n.t("estimated_fare")}
					</OlaTextRegular>
					<OlaTextRegular style={styles.estimatedFareInfo}>
						{props.options.type &&
							"₹" +
								Math.round(
									props.products.ride_estimate[props.options.type]
										.sub_categories[props.tripId].amount
								)}
					</OlaTextRegular>
				</View>
				{/*  fare rules */}
				<View style={styles.fareRuleView}>
					<OlaTextRegular style={styles.ruleStyle}>
						{I18n.t("fare_rules")}
					</OlaTextRegular>
					<View style={styles.centeredRow}>
						<View style={styles.bullet} />
						<OlaTextRegular style={[styles.ruleStyle, styles.fareRuleMargin]}>
							{I18n.t("excluded_toll_costs_parking_permits_tax")}
						</OlaTextRegular>
					</View>
					<View style={styles.centeredRow}>
						<View style={styles.bullet} />
						<OlaTextRegular style={[styles.ruleStyle, styles.fareRuleMargin]}>
							{`₹ ${
								props.selectedCab.fare_breakup[fareIndex]
									.time_charge_per_extra_time
							} ${I18n.t("per_hr_will_be_charged_for_additional_hours")}`}
						</OlaTextRegular>
					</View>
					<View style={styles.centeredRow}>
						<View style={styles.bullet} />
						<OlaTextRegular style={[styles.ruleStyle, styles.fareRuleMargin]}>
							{`₹ ${
								props.selectedCab.fare_breakup[fareIndex].price_per_distance
							} ${I18n.t("per_km_will_be_charged_for_extra_km")}`}
						</OlaTextRegular>
					</View>
					<View style={styles.centeredRow}>
						<View style={styles.bullet} />
						<OlaTextRegular style={[styles.ruleStyle, styles.fareRuleMargin]}>
							{`${I18n.t("driver_allowance_per")} 24 ${I18n.t("hours")} - ₹200`}
						</OlaTextRegular>
					</View>
					<View style={styles.centeredRow}>
						<View style={styles.bullet} />
						<OlaTextRegular style={[styles.ruleStyle, styles.fareRuleMargin]}>
							{`${I18n.t(
								"night_time_allowance"
							)} (11:00PM - 06:00AM) - ₹150/${I18n.t("ola_night")}`}
						</OlaTextRegular>
					</View>
				</View>
			</View>
		);
	};

	render() {
		let fare = null;
		let fareIndex = this.props.isOneWayAvailable === true ? 1 : 0;
		fare =
			this.props.options.type === "one_way"
				? this.props.products.ride_estimate.one_way.sub_categories[
						this.props.tripId
				  ].amount
				: this.props.products.ride_estimate.two_way.sub_categories[
						this.props.tripId
				  ].amount;
		return (
			<View style={{ flex: 1 }}>
				<View style={styles.mainContainer}>
					<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
						<View style={styles.goBack}>
							<Icon
								iconType={"material"}
								iconSize={width / 16}
								iconName={"arrow-back"}
								iconColor={"#707070"}
							/>
						</View>
					</TouchableOpacity>
					<OlaTextMedium style={styles.bookingConfirm}>
						{I18n.t("confirm_your_booking")}
					</OlaTextMedium>
				</View>
				<ScrollView
					style={styles.fullWhiteView}
					showsVerticalScrollIndicator={false}>
					<View style={styles.cabImageView}>
						<Image
							source={carImages[this.props.selectedCab.id]}
							style={styles.cabImage}
						/>
						<View style={styles.displayNameView}>
							<OlaTextRegular style={styles.displayNameText}>
								{this.props.selectedCab.display_name}
							</OlaTextRegular>
							<OlaTextRegular
								style={styles.availableCabText}
								numberOfLines={1}
								ellipsizeMode={"tail"}>
								{this.props.selectedCab.capacity}
								{I18n.t("seater") + " - "}
								{this.getAvailableCabType(this.props.selectedCab.car_models)}
							</OlaTextRegular>
						</View>
					</View>
					<FlatList
						style={{ flex: 1 }}
						showsHorizontalScrollIndicator={false}
						data={this.props.selectedCab.highlights}
						horizontal={true}
						renderItem={({ item, index }) => {
							return (
								<View style={styles.featuredView}>
									<Image
										source={featuresImage[item]}
										style={styles.featuredImage}
									/>
									<OlaTextRegular style={styles.featuredText}>
										{item}
									</OlaTextRegular>
								</View>
							);
						}}
						keyExtractor={(item, index) => index.toString()}
					/>
					<Seperator width={width} height={0.6} color={"#777777"} />
					<LocationText
						locationTextStyle={styles.originLocation}
						textStyle={styles.normalLocationText}
						placeholder={I18n.t("from")}
						locationType={"origin"}
						locationText={this.props.origin.address}
					/>
					<Seperator
						style={{ alignSelf: "center" }}
						width={width / 1.08}
						height={0.6}
						color={"#d9d9d9"}
					/>
					<LocationText
						locationTextStyle={styles.destinationTextStyle}
						textStyle={styles.normalLocationText}
						placeholder={I18n.t("where_to")}
						locationType={"destination"}
						locationText={this.props.destination.address}
					/>
					<Seperator width={width} height={0.5} color={"#777777"} />
					<View style={styles.tripLeaveOn}>
						<OlaTextRegular style={styles.leaveOnText}>
							{I18n.t("leave_on")}
						</OlaTextRegular>
						<OlaTextRegular style={styles.tripTimeText}>
							{this.props.getReadableTime(this.props.options.epoch_leave_time)}
						</OlaTextRegular>
					</View>
					{this.props.options.hasOwnProperty("drop_time") ? (
						<Seperator width={width / 1.08} height={0.4} color={"#d9d9d9"} />
					) : null}
					{this.props.options.hasOwnProperty("drop_time") ? (
						<View style={[styles.tripLeaveOn]}>
							<OlaTextRegular style={styles.returnByText}>
								{I18n.t("return_by")}
							</OlaTextRegular>
							<OlaTextRegular style={styles.tripTimeText}>
								{this.props.getReadableTime(
									this.props.options.epoch_return_time
								)}
							</OlaTextRegular>
						</View>
					) : null}
					<Seperator width={width} height={0.4} color={"#777777"} />
					<Seperator width={width} height={height / 43.63} color={"#f2f2f2"} />
					<Seperator width={width} height={0.4} color={"#777777"} />
					<View style={styles.tripTypeView}>
						<OlaTextRegular style={styles.availableCabText}>
							{this.props.options.type === "one_way"
								? I18n.t("one_way")
								: I18n.t("round_trip")}
						</OlaTextRegular>
					</View>
					<Seperator width={width} height={0.5} />
					{this.props.options.hasOwnProperty("drop_time") ? (
						<OlaTextRegular style={styles.selectedCabFareText}>
							{`₹ ${
								this.props.selectedCab.fare_breakup[fareIndex]
									.price_per_distance
							} ${I18n.t("per_km")}`}
						</OlaTextRegular>
					) : null}
					<View style={styles.estimatedFareView}>
						<View style={styles.estimatedFareInnerBorder} />
						<View style={styles.estimatedFareInnerView}>
							<OlaTextMedium style={styles.estimatedFareText}>
								{`₹ ${fare}`}
							</OlaTextMedium>
							<OlaTextRegular
								style={styles.availableCabText}
								numberOfLines={1}
								ellipsizeMode={"tail"}>
								{I18n.t("estimated_fare")}
							</OlaTextRegular>
						</View>
						<View style={styles.lowestInFareImageView}>
							<Image style={styles.lowestInFareImage} source={LOWESTINFARES} />
						</View>
					</View>
					<Seperator width={width} height={0.6} color={"#777777"} />
					<TouchableOpacity
						style={styles.seeFareDetailButton}
						onPress={() => {
							this.setState({ fareDetails: !this.state.fareDetails });
						}}>
						<OlaTextBold style={styles.seeFareDetailText}>
							{this.state.fareDetails === false
								? I18n.t("see_fare_details")
								: I18n.t("hide_fare_details")}
						</OlaTextBold>
					</TouchableOpacity>
					{this.state.fareDetails && <this.renderFareDetails {...this.props} />}
					<Seperator width={width} height={0.6} color={"#777777"} />
					<Seperator width={width} height={height / 43.63} color={"#f2f2f2"} />
					<Seperator width={width} height={0.4} color={"#777777"} />
					<View style={styles.freeCancellationView}>
						<OlaTextRegular style={styles.freeCancellationText}>
							{`${I18n.t("free_cancellation_available")}`}
						</OlaTextRegular>
						<TouchableOpacity
							onPress={() =>
								alert(
									"₹150" +
										I18n.t("cancellation_fee_applicable_if_cancelled_after") +
										"5" +
										I18n.t("minutes")
								)
							}>
							<OlaTextMedium style={styles.knowMoreText}>
								{` ${I18n.t("know_more")}`}
							</OlaTextMedium>
						</TouchableOpacity>
					</View>
					<Seperator width={width} height={0.6} color={"#777777"} />
					<Seperator width={width} height={height / 43.63} color={"#f2f2f2"} />
					<Seperator width={width} height={0.6} color={"#777777"} />
					<View style={styles.oneCarMultipleStopView}>
						<Image
							style={styles.oneCarMultipleStopImage}
							source={ONECARMULTIPLESTOPS}
						/>
						<View style={styles.auditedCarView}>
							<View style={styles.centeredRow}>
								<OlaTextRegular style={styles.iconText}>
									{"\u2022  "}
								</OlaTextRegular>
								<OlaTextRegular style={styles.iconInfoText}>
									{`${I18n.t("regularly_audited_cars")}`}
								</OlaTextRegular>
							</View>
							<View style={styles.centeredRow}>
								<OlaTextRegular style={styles.iconText}>
									{"\u2022  "}
								</OlaTextRegular>
								<OlaTextRegular style={styles.iconInfoText}>
									{`24x7 ${I18n.t("on_road_assistance")}`}
								</OlaTextRegular>
							</View>
							<View style={styles.centeredRow}>
								<OlaTextRegular style={styles.iconText}>
									{"\u2022  "}
								</OlaTextRegular>
								<OlaTextRegular style={styles.iconInfoText}>
									{`${I18n.t("real_time_tracking")}`}
								</OlaTextRegular>
							</View>
						</View>
					</View>
					<Seperator width={width} height={0.4} color={"#777777"} />
					<Seperator width={width} height={height / 43.63} color={"#f2f2f2"} />
					<View style={styles.borderHeight} />
					<ConfirmRide
						callbackRequestRide={data => {
							GoAppAnalytics.trackWithProperties(
								"ola-outstation-ride-confirm",
								{
									...this.props.options
								}
							);
							this.props.navigation.navigate("ChooseRide", this.props.options);
						}}
						type={"outstation"}
						openApplyCouponModal={() => {
							/**
							 * Apply Coupon Problem to be resolved.
							 */
							GoToast.show(
								I18n.t("feature_under_development"),
								I18n.t("information")
							);
							// this.setState({ modalVisible: true });
						}}
						product={this.props.products}
					/>
				</ScrollView>
			</View>
		);
	}
}

const ConfirmBooking = props => (
	<ConfirmBookingScreen {...props} {...props.route.params} />
);

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		origin: state.ola && state.ola.bookRide.origin,
		products: state.ola && state.ola.outstation.products,
		userToken: state.ola && state.ola.auth.userCredentials,
		destination: state.ola && state.ola.bookRide.destination
	};
}

export default connect(mapStateToProps)(ConfirmBooking);
