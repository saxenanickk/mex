import React from "react";
import { Stack, Drawer } from "../../../../utils/Navigators";
import BookRide from "./BookRide";
import Rentals from "./Rentals";
import OutStation from "./OutStation";
import Confirm from "./Confirm";
import YourRides from "./YourRides";
import Support from "./Support";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import I18n from "../../Assets/Strings/i18n";

const BookRideNavigator = () => (
	<Stack.Navigator headerMode={"none"}>
		<Stack.Screen name={"BookRide"} component={BookRide} />
		<Stack.Screen name={"Rentals"} component={Rentals} />
		<Stack.Screen name={"OutStation"} component={OutStation} />
		<Stack.Screen name={"Confirm"} component={Confirm} />
	</Stack.Navigator>
);

export default class BookRideDrawer extends React.Component {
	constructor(props) {
		super(props);
		this.olaDrawer = () => (
			<Drawer.Navigator initialRouteName={props.initialRouteName}>
				<Drawer.Screen
					name={I18n.t("book_ride")}
					component={BookRideNavigator}
				/>
				<Drawer.Screen name={I18n.t("your_ride")} component={YourRides} />
				<Drawer.Screen name={I18n.t("support")} component={Support} />
			</Drawer.Navigator>
		);
	}

	render() {
		return (
			<this.olaDrawer
				onNavigationStateChange={(prevState, currState) =>
					GoAppAnalytics.logChangeOfScreenInDrawer("ola", prevState, currState)
				}
			/>
		);
	}
}
