import React from "react";
import { View, StatusBar, Dimensions, TouchableOpacity } from "react-native";
import { WebView } from "react-native-webview";
import { Icon } from "../../../../../Components";
import { OlaTextBold } from "../../../Components/OlaText";
import { indexStyles as styles } from "./style";
import I18n from "../../../Assets/Strings/i18n";
import { connect } from "react-redux";

const { width } = Dimensions.get("window");
class SupportScreen extends React.Component {
	render() {
		const {
			userToken,
			origin: { latitude, longitude },
			type,
			booking_ref_no,
			brand,
			current_lat,
			current_lng
		} = this.props;

		console.log(this.props);

		const URI = type
			? "https://help.olacabs.com/self_serve/v1?page=myrides"
			: "https://help.olacabs.com/self_serve/v1";
		const BODY = type
			? `booking_ref_no=${booking_ref_no}&brand=${brand}&current_lat_lng=${current_lat}%2C+${current_lng}&authorization=Bearer%20${userToken}&X-APP-TOKEN=dea39464c59e4589a7d10119826f8870`
			: `current_lat_lng=${latitude}%2C+${longitude}&authorization=Bearer%20${userToken}&X-APP-TOKEN=dea39464c59e4589a7d10119826f8870`;

		console.log(BODY);

		return (
			<View style={styles.fullFlex}>
				<StatusBar backgroundColor="#7f7f7f" barStyle="light-content" />
				<View style={styles.header}>
					<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
						<Icon
							iconType={"material"}
							iconSize={width / 16}
							iconName={"arrow-back"}
							iconColor={"#707070"}
						/>
					</TouchableOpacity>
					<OlaTextBold style={styles.textStyle}>{`${I18n.t(
						"support"
					)}`}</OlaTextBold>
				</View>
				<WebView
					source={{
						uri: URI,
						headers: {
							"Content-Type": "application/x-www-form-urlencoded"
						},
						method: "POST",
						body: BODY
					}}
				/>
			</View>
		);
	}
}

class Support extends React.Component {
	render() {
		return <SupportScreen {...this.props} {...this.props.route.params} />;
	}
}

function mapStateToProps(state) {
	return {
		userToken: state.ola && state.ola.auth.userCredentials,
		origin: state.ola && state.ola.bookRide.origin
	};
}

export default connect(mapStateToProps)(Support);
