import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const indexStyles = StyleSheet.create({
	fullFlex: { flex: 1 },
	header: {
		width: width,
		height: height / 12,
		flexDirection: "row",
		alignItems: "center",
		paddingLeft: width / 35,
		elevation: 5,
		backgroundColor: "#ffffff",
		borderColor: "#ffffff"
	},
	textStyle: {
		marginLeft: width / 35,
		fontSize: width / 20,
		color: "#000000"
	}
});
