import React, { Component } from "react";
import {
	StatusBar,
	View,
	ScrollView,
	FlatList,
	TouchableOpacity,
	Image,
	Dimensions,
	TouchableWithoutFeedback
} from "react-native";
import { connect } from "react-redux";
import { indexStyles as styles } from "./style";
import I18n from "../../../Assets/Strings/i18n";
import { Seperator, Icon } from "../../../../../Components";
import {
	MINISELECTED,
	PRIMESELECTED,
	SEDANSELECTED,
	SUVSELECTED,
	AUTOSELECTED,
	ERICKSELECTED,
	KAALIPEELISELECTED,
	LUXSELECTED,
	MICROSELECTED,
	ONECARMULTIPLESTOPS
} from "../../../Assets/Img/Image";
import {
	OlaTextRegular,
	OlaTextBold,
	OlaTextMedium
} from "../../../Components/OlaText";
import GoAppAnalytics from "../../../../../utils/GoAppAnalytics";

const { width, height } = Dimensions.get("window");

const carImages = {
	mini: MINISELECTED,
	prime: PRIMESELECTED,
	sedan: SEDANSELECTED,
	suv: SUVSELECTED,
	auto: AUTOSELECTED,
	erick: ERICKSELECTED,
	kp: KAALIPEELISELECTED,
	lux: LUXSELECTED,
	micro: MICROSELECTED,
	exec: PRIMESELECTED,
	prime_play: SEDANSELECTED
};

export class Rentals extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isPackageSelected: false,
			selectedPackage: null,
			isCabSelected: false,
			selectedCab: null
		};

		const { route } = props;
		const { isRideNow, pickup_time } = route.params ? route.params : {};

		this.isRideNow = isRideNow;
		this.pickup_time = pickup_time;

		this.changeScreen = this.changeScreen.bind(this);
	}

	changeScreen(screenName, params = {}) {
		if (!this.isRideNow) {
			params = {
				...params,
				pickup_time: this.pickup_time,
				pickup_mode: "LATER"
			};
		} else {
			params = { ...params, pickup_mode: "NOW" };
		}
		// this.props.navigation.dispatch(goToChooseRide);
		this.props.navigation.navigate(screenName, params);
	}

	getRentalArray() {
		let { products } = this.props;
		return products.find(product => product.id === "rental");
	}

	getAvailableCars = () => {
		let rentalData = this.getRentalArray();
		let rentalSubCategories = rentalData.sub_categories;
		let { package_id } = this.state.selectedPackage;
		let cars = rentalSubCategories.reduce((acc, curVal) => {
			let availableCategory = curVal.packages.findIndex(pack => {
				return pack.package_id === package_id;
			});
			if (availableCategory !== -1) {
				curVal.package = curVal.packages[availableCategory];
				return [...acc, curVal];
			}
			return acc;
		}, []);
		return cars;
	};

	_keyExtractor = (item, index) => index.toString();

	_handlePackageClick = item => {
		this.setState({
			isPackageSelected: true,
			selectedPackage: item
		});
	};

	modifySelectedPackage = () => {
		this.setState({
			isPackageSelected: false,
			isCabSelected: false,
			selectedCab: null
		});
	};

	_renderAvailableTimeSlots = ({ item }) => {
		let selectedPackage;
		if (this.state.selectedPackage) {
			selectedPackage =
				this.state.selectedPackage.package_id === item.package_id
					? "dot-circle-o"
					: "circle-o";
		} else {
			selectedPackage = "circle-o";
		}
		return (
			<TouchableOpacity
				style={styles.packageDescriptionButton}
				onPress={this._handlePackageClick.bind(this, item)}>
				<Icon
					iconType={"font_awesome"}
					iconName={selectedPackage}
					iconSize={width / 22}
					iconColor={selectedPackage === "dot-circle-o" ? "#0076ff" : "#CDCDD1"}
				/>
				<OlaTextRegular style={styles.itemText}>
					{item.package_description}
				</OlaTextRegular>
			</TouchableOpacity>
		);
	};

	_handleCabClick = item => {
		this.setState({
			isCabSelected: true,
			selectedCab: item,
			pickup_mode: this.isRideNow ? "NOW" : "LATER"
		});
	};

	modifySelectedCab = () => {
		this.setState({
			isCabSelected: false
		});
	};

	_renderAvailableCars = ({ item }) => {
		let selectedCab;
		if (this.state.selectedCab !== null) {
			selectedCab =
				this.state.selectedCab.id === item.id ? "dot-circle-o" : "circle-o";
		} else {
			selectedCab = "circle-o";
		}
		return (
			<TouchableOpacity
				style={styles.availableCarList}
				onPress={this._handleCabClick.bind(this, item)}>
				<View>
					<Icon
						iconType={"font_awesome"}
						iconName={selectedCab}
						iconSize={width / 22}
						iconColor={selectedCab === "dot-circle-o" ? "#0076ff" : "#CDCDD1"}
					/>
				</View>
				<View style={styles.carListImageView}>
					<Image source={carImages[item.id]} style={styles.cabImage} />
					{this.isRideNow ? (
						<OlaTextRegular style={styles.etaText}>
							{item.eta > 0 ? `${item.eta} ${I18n.t("min")}` : " --- "}
						</OlaTextRegular>
					) : null}
				</View>
				<View style={styles.carListDescriptionView}>
					<OlaTextRegular style={styles.blackText}>
						{item.display_name}
					</OlaTextRegular>
					<OlaTextRegular numberOfLines={1} ellipsizeMode={"tail"}>
						{item.car_models.toString()}
					</OlaTextRegular>
				</View>
				<View style={styles.carListFare}>
					<OlaTextRegular style={styles.blackText}>
						{`₹${parseInt(item.package.base_fare)}`}
					</OlaTextRegular>
				</View>
			</TouchableOpacity>
		);
	};

	componentDidMount() {
		this._unsubscribeFocus = this.props.navigation.addListener("focus", () => {
			GoAppAnalytics.setPageView("ola", "rental_screen");
		});
	}

	componentWillUnmount() {
		this._unsubscribeFocus();
	}

	render() {
		return (
			<View style={styles.fullFlex}>
				<StatusBar backgroundColor="#bdbdbd" barStyle="light-content" />
				<View style={styles.mainContainer}>
					<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
						<View style={styles.goBackButton}>
							<Icon
								iconType={"material"}
								iconSize={width / 16}
								iconName={"arrow-back"}
								iconColor={"#707070"}
							/>
						</View>
					</TouchableOpacity>
					<OlaTextMedium style={styles.bookYourRideText}>
						{I18n.t("book_your_ride")}
					</OlaTextMedium>
				</View>
				<ScrollView style={styles.scrollViewStyle}>
					<View style={styles.pickupView}>
						<OlaTextRegular style={styles.pickupLocationText}>
							{`1. ${I18n.t("pickup_location")}`}
						</OlaTextRegular>
						<OlaTextRegular
							style={styles.pickupText}
							numberOfLines={1}
							ellipsizeMode={"tail"}>
							{this.props.origin.address}
						</OlaTextRegular>
					</View>
					<View style={styles.packageView}>
						{this.state.isPackageSelected ? (
							<TouchableOpacity
								onPress={this.modifySelectedPackage}
								style={styles.selectedPackage}>
								<View>
									<OlaTextRegular style={styles.packageText}>
										{`2. ${I18n.t("package")}`}
									</OlaTextRegular>
									<OlaTextRegular style={styles.packageDescriptionText}>
										{this.state.selectedPackage.package_description}
									</OlaTextRegular>
								</View>
								<Icon
									iconType={"font_awesome"}
									iconName={"angle-down"}
									iconSize={height / 28}
									iconColor={"#c7c7cc"}
									style={styles.selectedPackageIcon}
								/>
							</TouchableOpacity>
						) : (
							<View>
								<OlaTextRegular style={styles.selectePackageText}>
									{`2. ${I18n.t("select_package")}`}
								</OlaTextRegular>
								<FlatList
									data={this.props.packages}
									renderItem={this._renderAvailableTimeSlots}
									keyExtractor={this._keyExtractor}
								/>
							</View>
						)}
					</View>
					<View style={styles.cabTypeView}>
						{this.state.isPackageSelected ? (
							<View>
								{this.state.isCabSelected ? (
									<TouchableOpacity
										onPress={this.modifySelectedCab}
										style={styles.selectedCab}>
										<View>
											<OlaTextRegular style={styles.packageText}>
												{`3. ${I18n.t("cab_type")}`}
											</OlaTextRegular>
											<OlaTextRegular style={styles.packageEtaText}>
												{this.state.selectedCab.display_name}{" "}
												{this.state.selectedCab.eta > 0
													? `* ${this.state.selectedCab.eta} ${I18n.t("min")}`
													: ""}
											</OlaTextRegular>
											<View style={styles.centeredRow}>
												<Image
													source={carImages.micro}
													style={styles.cabImage}
												/>
												<Seperator
													width={0.4}
													height={width / 11.5}
													color={"#d9d9d9"}
												/>
												<OlaTextRegular style={styles.selectedCabText}>
													{`₹${this.state.selectedCab.package.base_fare}`}
												</OlaTextRegular>
											</View>
										</View>
										<Icon
											iconType={"font_awesome"}
											iconName={"angle-down"}
											iconSize={height / 28}
											iconColor={"#c7c7cc"}
											style={styles.selectedPackageIcon}
										/>
									</TouchableOpacity>
								) : (
									<View>
										<OlaTextRegular style={styles.chooseCabTypeText}>
											{`3. ${I18n.t("choose_cab_type")}`}
										</OlaTextRegular>
										<FlatList
											data={this.getAvailableCars()}
											renderItem={this._renderAvailableCars}
											keyExtractor={this._keyExtractor}
										/>
									</View>
								)}
							</View>
						) : null}
					</View>
					{this.state.isCabSelected ? (
						<View>
							<View style={styles.additionalInfo}>
								<OlaTextRegular style={styles.fareDetailRuleText}>
									{I18n.t("fare_detail_rules")}
								</OlaTextRegular>
								<View style={styles.ruleView}>
									<OlaTextRegular style={styles.ruleIcon}>
										{"\u2022  "}
									</OlaTextRegular>
									<OlaTextRegular style={styles.ruleText}>
										{`${I18n.t("charges_applicable_upon_exceeding")}`}
									</OlaTextRegular>
								</View>
								<View style={styles.ruleView}>
									<OlaTextRegular style={styles.ruleIcon}>
										{"\u2022  "}
									</OlaTextRegular>
									<OlaTextRegular style={styles.ruleText}>
										{`${I18n.t("additional_gst_on_final_bill")}`}
									</OlaTextRegular>
								</View>
								<View style={styles.ruleView}>
									<OlaTextRegular style={styles.ruleIcon}>
										{"\u2022  "}
									</OlaTextRegular>
									<OlaTextRegular style={styles.ruleText}>
										{`${I18n.t("toll_auto_added")}`}
									</OlaTextRegular>
								</View>
								<View style={styles.ruleView}>
									<OlaTextRegular style={styles.ruleIcon}>
										{"\u2022  "}
									</OlaTextRegular>
									<OlaTextRegular style={styles.ruleText}>{`${I18n.t(
										"local_travel_only"
									)}`}</OlaTextRegular>
								</View>
							</View>
							<View style={styles.oneCarMultipleStopView}>
								<Image
									style={styles.oneCarMultipleStopImage}
									source={ONECARMULTIPLESTOPS}
								/>
								<View style={styles.leftMarginView}>
									<OlaTextBold numberOfLines={2} style={styles.packageText}>
										{`${I18n.t("one_car_multiple_stop")}`}
									</OlaTextBold>
									<OlaTextRegular
										numberOfLines={2}
										style={styles.allPickupText}>
										{`${I18n.t("one_booking_all_pickup_drops")}`}
									</OlaTextRegular>
								</View>
							</View>
						</View>
					) : null}
				</ScrollView>
				{this.state.isCabSelected ? (
					<TouchableWithoutFeedback
						onPress={() => {
							GoAppAnalytics.trackWithProperties("ola-rental-cab-selected", {
								sub_category: this.state.selectedCab.id,
								package_id: this.state.selectedPackage.package_id
							});
							this.changeScreen("ChooseRide", {
								sub_category: this.state.selectedCab.id,
								package_id: this.state.selectedPackage.package_id,
								category: "rental",
								id: "rental",
								pickup_lat: this.props.origin.pickup_lat,
								pickup_lng: this.props.origin.pickup_lng
							});
						}}>
						<View style={styles.confirmButton}>
							<OlaTextMedium style={styles.confirmText}>
								{I18n.t("accept_confirm")}
							</OlaTextMedium>
						</View>
					</TouchableWithoutFeedback>
				) : null}
			</View>
		);
	}
}

const getAvailablePackages = products => {
	let rentalData = products.find(product => product.id === "rental");

	//In case thr data in not loaded before we have the data
	if (!rentalData) {
		return [];
	}

	let rentalSubCategories = rentalData.sub_categories;
	let allPackages = rentalSubCategories.reduce((acc, curVal) => {
		if ("packages" in curVal) {
			return [...acc, ...curVal.packages];
		}
		return acc;
	}, []);
	// Removing duplicate packages
	let availablePackages = allPackages.filter((singlePackage, index, self) => {
		return (
			index ===
			self.findIndex(item => item.package_id === singlePackage.package_id)
		);
	});

	function compare(a, b) {
		let a_first = a.package_id.substr(0, a.package_id.indexOf(" "));
		let b_first = b.package_id.substr(0, b.package_id.indexOf(" "));
		if (a_first < b_first) {
			return -1;
		}
		if (a_first > b_first) {
			return 1;
		}
		return 0;
	}

	return availablePackages.sort(compare);
};

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		origin: state.ola && state.ola.bookRide.origin,
		products: state.ola && state.ola.bookRide.products,
		userToken: state.ola && state.ola.auth.userCredentials,
		packages: getAvailablePackages(state.ola.bookRide.products)
	};
}

export default connect(mapStateToProps)(Rentals);
