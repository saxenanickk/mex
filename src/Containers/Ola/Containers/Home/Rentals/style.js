import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const indexStyles = StyleSheet.create({
	pickupView: {
		justifyContent: "center",
		borderBottomWidth: 0.4,
		borderColor: "#d9d9d9",
		backgroundColor: "#ffffff",
		paddingHorizontal: width / 25,
		marginBottom: height / 42.6,
		paddingVertical: width / 30
	},
	pickupText: {
		color: "#000",
		fontSize: height / 50.5
	},
	packageView: {
		borderTopWidth: 0.4,
		borderBottomWidth: 0.4,
		paddingHorizontal: width / 25,
		borderColor: "#d9d9d9",
		backgroundColor: "#fff",
		marginBottom: 15
	},
	cabTypeView: {
		paddingHorizontal: width / 25,
		borderTopWidth: 0.4,
		borderBottomWidth: 0.4,
		borderColor: "#d9d9d9",
		backgroundColor: "#fff"
	},
	itemText: {
		padding: width / 25,
		color: "#000",
		fontSize: height / 48
	},
	selectedPackageIcon: {
		padding: 5
	},
	selectedPackage: {
		height: height / 12.75,
		alignItems: "center",
		flexDirection: "row",
		justifyContent: "space-between"
	},
	availableCarList: {
		flexDirection: "row",
		justifyContent: "flex-start",
		alignItems: "center",
		borderTopWidth: 0.4,
		borderColor: "#d9d9d9",
		paddingVertical: width / 35
	},
	carListImageView: {
		paddingLeft: 15,
		justifyContent: "flex-start",
		width: 60
	},
	carListDescriptionView: {
		// paddingLeft: 15,
		flex: 1
	},
	carListFare: {
		paddingRight: 15,
		justifyContent: "flex-end",
		marginLeft: "auto"
	},
	selectedCab: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingVertical: width / 35
	},
	additionalInfo: {
		backgroundColor: "#fff",
		paddingHorizontal: width / 25,
		paddingVertical: width / 35,
		borderBottomWidth: 0.4,
		borderColor: "#d9d9d9"
	},
	ruleHeading: {
		flexDirection: "row",
		flex: 1,
		alignItems: "center",
		justifyContent: "space-between"
	},
	ruleText: {
		color: "#242424",
		fontSize: height / 50
	},
	insurance: {
		flexDirection: "row",
		backgroundColor: "#fff",
		marginTop: 15,
		alignItems: "center"
	},
	ad: {
		backgroundColor: "#fff",
		marginTop: width / 35,
		padding: width / 35,
		borderBottomWidth: 0.4,
		borderTopWidth: 0.4,
		borderColor: "#d9d9d9",
		alignItems: "center"
	},
	accept: {
		backgroundColor: "#000",
		alignItems: "center",
		padding: 10
	},
	headerBar: {
		elevation: 5,
		backgroundColor: "#ffffff",
		justifyContent: "flex-start",
		alignItems: "center",
		padding: width / 25,
		flexDirection: "row",
		width: width / 1,
		height: height / 13.33
	},
	packageDescriptionButton: {
		borderTopWidth: 0.4,
		borderTopColor: "#d9d9d9",
		flexDirection: "row",
		alignItems: "center"
	},
	etaText: {
		fontSize: 12
	},
	blackText: {
		color: "#000"
	},
	fullFlex: { flex: 1 },
	mainContainer: {
		width: width,
		elevation: 5,
		backgroundColor: "#ffffff",
		justifyContent: "flex-start",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		shadowOffset: {
			width: 2,
			height: 2
		},
		shadowColor: "grey",
		shadowOpacity: 1,
		shadowRadius: 4
	},
	goBackButton: {
		justifyContent: "center",
		width: width / 6,
		alignItems: "center",
		height: height / 13.33
	},
	bookYourRideText: {
		color: "#000000",
		fontSize: height / 45.71,
		marginLeft: width / 50
	},
	scrollViewStyle: { flex: 1, borderBottomWidth: 0.5, borderColor: "#202020" },
	packageText: {
		fontSize: height / 56.5
	},
	packageDescriptionText: { color: "#0076ff" },
	selectePackageText: {
		height: height / 18.11,
		textAlignVertical: "center",
		fontSize: height / 56.5
	},
	packageEtaText: {
		color: "#0076ff",
		marginVertical: height / 60
	},
	centeredRow: {
		flexDirection: "row",
		alignItems: "center"
	},
	cabImage: {
		width: width / 11.5,
		height: width / 11.5,
		marginRight: width / 25
	},
	selectedCabText: {
		color: "#000000",
		marginHorizontal: width / 25
	},
	chooseCabTypeText: {
		height: height / 18.11,
		textAlignVertical: "center",
		fontSize: height / 56.5
	},
	fareDetailRuleText: {
		color: "#0076ff",
		fontSize: height / 56
	},
	ruleView: {
		flexDirection: "row",
		alignItems: "center",
		marginTop: height / 60
	},
	ruleIcon: {
		color: "#707070"
	},
	oneCarMultipleStopView: {
		backgroundColor: "#ffffff",
		flexDirection: "row",
		marginTop: height / 44,
		height: height / 8.13,
		paddingHorizontal: width / 25,
		width: width,
		borderWidth: 0.4,
		borderColor: "#d9d9d9",
		alignItems: "center"
	},
	oneCarMultipleStopImage: {
		width: width / 4,
		height: height / 9.5
	},
	leftMarginView: {
		marginLeft: width / 20
	},
	allPickupText: {
		fontSize: height / 56,
		width: width / 1.6,
		color: "#8e8e8e"
	},
	confirmButton: {
		justifyContent: "center",
		alignItems: "center",
		width: width,
		height: height / 14.28,
		backgroundColor: "#202020"
	},
	confirmText: {
		color: "#d4db28",
		fontSize: width / 25
	},
	pickupLocationText: {
		color: "#6e6e70",
		fontSize: height / 56.5
	}
});
