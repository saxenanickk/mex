import React, { Component } from "react";
import { View, Dimensions, TouchableOpacity, BackHandler } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { Seperator, MapView, ProgressScreen } from "../../../../../Components";
import { connect } from "react-redux";
import {
	olaRequestRentalRide,
	olaRentalAbortBooking,
	olaRentalCurrentRide,
	olaRentalBookingStatus
} from "./Saga";
import Icon from "react-native-vector-icons/FontAwesome";
import I18n from "../../../Assets/Strings/i18n";
import { LocationText } from "../../../Components";
import { OlaTextBold, OlaTextRegular } from "../../../Components/OlaText";
import { indexStyles as styles } from "./style";
import GoAppAnalytics from "../../../../../utils/GoAppAnalytics";
import { GoToast } from "../../../../../Components";

const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class Confirm extends Component {
	static navigationOptions = {
		header: null
	};

	constructor(props) {
		super(props);
		this.state = {
			progress: 0.3,
			showSpinner: true,
			modalVisible: false,
			activateCancel: false,
			cancelling: false,
			region: {
				latitude: props.origin.latitude,
				longitude: props.origin.longitude,
				latitudeDelta: LATITUDE_DELTA,
				longitudeDelta: LONGITUDE_DELTA
			}
		};
		this.currentRideInterval = null;
		this.isTracking = false;
		this.requestStatus = "FIRST_CHECK";
		this.abortBooking = this.abortBooking.bind(this);
		this.closeModal = this.closeModal.bind(this);

		const { route } = props;
		const {
			category,
			pickup_mode,
			sub_category,
			package_id,
			type,
			drop_time,
			pickup_time
		} = route.params ? route.params : {};

		this.category = category;
		this.pickup_mode = pickup_mode;
		this.sub_category = sub_category;
		this.package_id = package_id;
		this.type = type;
		this.drop_time = drop_time;
		this.pickup_time = pickup_time;
	}

	componentDidMount() {
		this._unsubscribeFocus = this.props.navigation.addListener("focus", () => {
			GoAppAnalytics.setPageView("ola", "confirm_ride");
		});

		BackHandler.addEventListener(
			"hardwareBackPress",
			this.onBackButtonPressAndroid
		);
		this.setState({
			showSpinner: true
		});
		let dispatchOptions = {
			access_token: this.props.userToken,
			pickup_lat: this.props.origin.latitude,
			pickup_lng: this.props.origin.longitude,
			category: this.category,
			pickup_mode: this.pickup_mode,
			sub_category: this.sub_category,
			appToken: this.props.appToken
		};
		if (this.category === "rental") {
			dispatchOptions = {
				...dispatchOptions,
				package_id: this.package_id
			};
		}
		if (this.category === "outstation") {
			dispatchOptions = {
				...dispatchOptions,
				type: this.type,
				drop_lat: this.props.destination.latitude,
				drop_lng: this.props.destination.longitude,
				trip_id: this.trip_id
			};
			if (this.type === "two_way") {
				dispatchOptions = {
					...dispatchOptions,
					drop_time: this.drop_time
				};
			}
		}
		if (this.pickup_mode === "LATER") {
			dispatchOptions = {
				...dispatchOptions,
				pickup_time: this.pickup_time
			};
		}
		this.props.dispatch(olaRequestRentalRide(dispatchOptions));
	}

	shouldComponentUpdate(nextProps, nextState) {
		console.log(`%c ${nextProps}`, "background: green; color: white;");
		if (
			nextProps.bookingStatus !== null &&
			this.props.bookingStatus !== nextProps.bookingStatus
		) {
			console.log("%c Inside 1", "background: green; color: white;");
			if (
				nextProps.bookingStatus.status === "SUCCESS" &&
				typeof nextProps.bookingStatus.booking_status === "undefined" &&
				this.requestStatus === "FIRST_CHECK"
			) {
				this.requestStatus = "CLEANED";
				console.log("%c Inside 2", "background: green; color: white;");
				this.setState({ activateCancel: true });
				this.currentRideInterval = setInterval(() => {
					this.props.dispatch(
						olaRentalCurrentRide({
							access_token: this.props.userToken,
							request_id: nextProps.bookingStatus.booking_id,
							appToken: this.props.appToken
						})
					);
				}, 5000);
			} else if (
				nextProps.bookingStatus.status === "SUCCESS" &&
				nextProps.bookingStatus.booking_status === "ALLOTMENT_PENDING"
			) {
				console.log("BOOKING PENDING");
			} else if (
				nextProps.bookingStatus.status === "SUCCESS" &&
				nextProps.bookingStatus.booking_status === "ALLOTMENT_FAILED"
			) {
				GoToast.show(I18n.t("no_cab"), I18n.t("information"), "LONG");
				this.props.navigation.goBack();
				return false;
			} else if (
				nextProps.bookingStatus.status === "SUCCESS" &&
				nextProps.bookingStatus.booking_status === "CALL_DRIVER"
			) {
				this.isTracking = true;
				AsyncStorage.setItem(
					"RIDE_IN_PROGRESS",
					JSON.stringify(nextProps.bookingStatus)
				);
				// GoAppAnalytics.trackWithProperties("ola-booking-success", {
				// 	booking_id: this.props.bookingStatus.booking_id,
				// })
				this.props.navigation.navigate("TrackRide");
			} else if (
				nextProps.bookingStatus.status === "FAILURE" &&
				nextProps.bookingStatus.code === "NO_CABS_AVAILABLE"
			) {
				GoToast.show(
					nextProps.bookingStatus.message,
					I18n.t("information"),
					"LONG"
				);
				this.props.navigation.goBack();
				return false;
			} else if (nextProps.bookingStatus.status === "FAILURE") {
				GoToast.show(
					nextProps.bookingStatus.message,
					I18n.t("information"),
					"LONG"
				);
				this.props.navigation.goBack();
				return false;
			} else if (nextProps.bookingStatus.status === "CANCELLED_SUCCESSFULLY") {
				GoToast.show(nextProps.bookingStatus.message, I18n.t("information"));
				this.props.navigation.goBack();
				return false;
			} else {
				GoToast.show(I18n.t("try_again"), I18n.t("information"), "LONG");
				this.props.navigation.goBack();
				return false;
			}
		}
		return true;
	}

	componentWillUnmount() {
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.onBackButtonPressAndroid
		);
		this._unsubscribeFocus();
		clearInterval(this.currentRideInterval);
		if (!this.isTracking) {
			this.props.dispatch(olaRentalBookingStatus(null));
		}
	}

	onBackButtonPressAndroid() {
		return true;
	}

	abortBooking() {
		if (this.state.activateCancel) {
			this.setState({ cancelling: true });
			this.props.dispatch(
				olaRentalAbortBooking({
					access_token: this.props.userToken,
					request_id: this.props.bookingStatus.booking_id,
					appToken: this.props.appToken
				})
			);
			GoAppAnalytics.trackWithProperties("ola-cancel-ride", {
				request_id: this.props.bookingStatus.booking_id
			});
		}
	}

	closeModal() {
		this.setState({ modalVisible: false });
	}

	render() {
		return (
			<View style={styles.fullFlex}>
				<MapView
					mapStyle={
						this.state.requestRideCheck
							? styles.mapWithRequestRide
							: styles.mapWithoutRequestRideCheck
					}
					region={this.state.region}
					showUserLocation
					followUserLocation
				/>
				<View style={[styles.headerBar]}>
					<TouchableOpacity onPress={this.backButtonPress}>
						<Icon
							iconType={"material"}
							iconSize={width / 15}
							iconName={"arrow-back"}
							iconColor={"#000000"}
						/>
					</TouchableOpacity>
					<OlaTextBold style={styles.findingRideText}>
						{I18n.t("finding_ride")}
					</OlaTextBold>
				</View>
				<LocationText
					locationTextStyle={styles.locationText}
					placeholder={I18n.t("from")}
					locationType={"origin"}
					locationText={this.props.origin.address}
				/>
				{this.state.showSpinner ? (
					<ProgressScreen
						progressScreen={{ elevation: 4 }}
						indicatorColor={"#cddc39"}
					/>
				) : null}
				{/* Bottom Pane */}
				<View style={styles.bottomPaneView}>
					<View style={styles.bottomTextView}>
						<OlaTextRegular style={styles.bottomText}>
							{this.state.cancelling
								? I18n.t("cancelling_request")
								: I18n.t("searching_ride")}
						</OlaTextRegular>
					</View>
					<Seperator width={width} height={0.5} />
					<View style={styles.separatorBar} />
					<Seperator width={width} height={0.5} />
					<TouchableOpacity
						style={styles.cancelButton}
						onPress={this.abortBooking}>
						<OlaTextRegular
							style={
								this.state.activateCancel
									? styles.activeCancelText
									: styles.unactiveCancelText
							}>
							{I18n.t("cancel_request")}
						</OlaTextRegular>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		origin: state.ola && state.ola.bookRide.origin,
		destination: state.ola && state.ola.bookRide.destination,
		userToken: state.ola && state.ola.auth.userCredentials,
		bookingStatus: state.ola && state.ola.chooseRentalRideReducer.bookingStatus
	};
}

export default connect(mapStateToProps)(Confirm);
