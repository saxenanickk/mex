import { StyleSheet, Dimensions } from "react-native";
import { font_one } from "../../../../../Assets/styles";

const { width, height } = Dimensions.get("window");

export const indexStyles = StyleSheet.create({
	fullFlex: {
		flex: 1
	},
	headerBar: {
		elevation: 5,
		backgroundColor: "#ffffff",
		justifyContent: "flex-start",
		alignItems: "center",
		padding: 10,
		flexDirection: "row",
		width: width / 1,
		height: height / 12
	},
	mapWithRequestRide: {
		top: height / 4,
		bottom: 0
	},
	mapWithoutRequestRideCheck: {
		top: height / 4,
		bottom: height / 4
	},
	findingRideText: {
		color: "#000000",
		fontSize: width / 22,
		marginLeft: width / 35
	},
	locationText: {
		elevation: 0
	},
	bottomPaneView: {
		position: "absolute",
		bottom: width / 35,
		borderWidth: 0,
		alignSelf: "center",
		width: width / 1.05,
		height: height / 4,
		elevation: 5,
		backgroundColor: "#ffffff"
	},
	bottomTextView: {
		width: width,
		height: height / 12,
		justifyContent: "center",
		alignItems: "center"
	},
	bottomText: {
		color: "#000000",
		fontSize: width / 25
	},
	separatorBar: {
		backgroundColor: "#dfdfdf",
		width: width,
		height: height / 12
	},
	cancelButton: {
		width: width,
		height: height / 12,
		justifyContent: "center",
		alignItems: "center"
	},
	activeCancelText: {
		color: "#ff0000",
		fontSize: width / 25
	},
	unactiveCancelText: {
		color: "#000000",
		fontSize: width / 25
	}
});
