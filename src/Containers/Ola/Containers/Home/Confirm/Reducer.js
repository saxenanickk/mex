import { OLA_RENTAL_BOOKING_STATUS } from "./Saga";

const initialState = {
	bookingStatus: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case OLA_RENTAL_BOOKING_STATUS:
			return {
				...state,
				bookingStatus: action.payload
			};
		default:
			return state;
	}
};
