import { call, put, takeLatest } from "redux-saga/effects";
import OlaApi from "../../../Api";

/**
 * Constans
 */

export const OLA_REQUEST_RENTAL_RIDE = "OLA_REQUEST_RENTAL_RIDE";
export const OLA_RENTAL_CURRENT_RIDE = "OLA_RENTAL_CURRENT_RIDE";
export const OLA_RENTAL_ABORT_BOOKING = "OLA_RENTAL_ABORT_BOOKING";
export const OLA_RENTAL_BOOKING_STATUS = "OLA_RENTAL_BOOKING_STATUS";

/**
 * Action Creators
 */

export const olaRequestRentalRide = payload => ({
	type: OLA_REQUEST_RENTAL_RIDE,
	payload
});
export const olaRentalBookingStatus = payload => ({
	type: OLA_RENTAL_BOOKING_STATUS,
	payload
});
export const olaRentalCurrentRide = payload => ({
	type: OLA_RENTAL_CURRENT_RIDE,
	payload
});
export const olaRentalAbortBooking = payload => ({
	type: OLA_RENTAL_ABORT_BOOKING,
	payload
});

/**
 * Saga
 */

export function* olaConfirmRentalRideSaga(dispatch) {
	yield takeLatest(OLA_REQUEST_RENTAL_RIDE, handleOlaRequestRentalRide);
	yield takeLatest(OLA_RENTAL_ABORT_BOOKING, handleOlaRentalAbortBooking);
	yield takeLatest(OLA_RENTAL_CURRENT_RIDE, handleOlaRentalCurrentRide);
}

/**
 * Handlers
 */

function* handleOlaRequestRentalRide(action) {
	console.log(action, "---------Action---------");
	try {
		let requestRide = yield call(OlaApi.requests, action.payload);
		console.log("Request Ride Saga Data: ", requestRide);
		yield put(olaRentalBookingStatus(requestRide));
	} catch (error) {
		console.log("Request Ride Saga Error: ", error);
	}
}

function* handleOlaRentalAbortBooking(action) {
	try {
		let abortBooking = yield call(OlaApi.abortBooking, action.payload);
		console.log("Ola Abort Booking Saga: ", abortBooking);
		abortBooking.status === "SUCCESS"
			? yield put(
					olaRentalBookingStatus({
						status: "CANCELLED_SUCCESSFULLY",
						message: abortBooking.message
					})
			  )
			: console.log("FAILURE");
	} catch (e) {
		console.log("Ola abort booking error", e);
	}
}

function* handleOlaRentalCurrentRide(action) {
	try {
		let current = yield call(OlaApi.getCurrentRide, action.payload);
		console.log("Ola Current Ride Saga: ", current);
		current.code === "UNKNOWN" && current.message === "TRACK_RIDE_FAILED"
			? console.log("Share Ride Error")
			: yield put(olaRentalBookingStatus(current));
	} catch (error) {
		console.log("Ola Current Ride Error: ", error);
	}
}
