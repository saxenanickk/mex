import React from "react";
import RegisterScreen from "./RegisterScreen";
import { connect } from "react-redux";
import { olaSaveBookingId } from "./YourRides/Saga";
class Home extends React.Component {
	constructor(props) {
		super(props);

		const { route } = props;
		const { booking_id } = route.params ? route.params : {};

		this.booking_id = booking_id;
	}
	componentWillMount() {
		if (this.booking_id) this.props.dispatch(olaSaveBookingId(this.booking_id));
	}

	getScreenType = screenName => {
		switch (screenName) {
			case "bookride":
				return "book_ride";
			case "yourride":
				return "your_ride";
			case "support":
				return "support";
			default:
				return "book_ride";
		}
	};

	render() {
		return (
			<RegisterScreen
				initialRouteName={
					this.type ? this.getScreenType(this.type) : "book_ride"
				}
			/>
		);
	}
}

export default connect()(Home);
