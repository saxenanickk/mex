import { combineReducers } from "redux";
import { SAVE_OLA_DEEPLINK_STATE } from "./Saga";
import { reducer as olaAuthReducer } from "./Containers/Auth/Reducer";
import { reducer as bookRideReducer } from "./Containers/Home/BookRide/Reducer";
import { reducer as chooseRideReducer } from "./Containers/ChooseRide/Reducer";
import { reducer as outstationReducer } from "./Containers/Home/OutStation/Reducer";
import { reducer as chooseRentalRideReducer } from "./Containers/Home/Confirm/Reducer";
import { reducer as myRidesReducer } from "./Containers/Home/YourRides/Reducer";
import { reducer as cancelRidesReducer } from "./Containers/TrackRide/Reducer";
import { reducer as rateRideReducer } from "./Containers/RateRide/Reducer";

const initialState = null;

const navStateReducer = (state = initialState, action) => {
	switch (action.type) {
		case SAVE_OLA_DEEPLINK_STATE:
			return {
				...state,
				deeplink: action.payload
			};
		default:
			return state;
	}
};

const olaReducer = combineReducers({
	navState: navStateReducer,
	auth: olaAuthReducer,
	bookRide: bookRideReducer,
	chooseRide: chooseRideReducer,
	outstation: outstationReducer,
	chooseRentalRideReducer: chooseRentalRideReducer,
	myRides: myRidesReducer,
	cancelRides: cancelRidesReducer,
	rateRides: rateRideReducer
});

export default olaReducer;
