import React from "react";

import Splash from "./Containers/Splash";
import Auth from "./Containers/Auth";
import Home from "./Containers/Home";
import ChooseRide from "./Containers/ChooseRide";
import TrackRide from "./Containers/TrackRide";
import Summary from "./Containers/Summary";
import RateRide from "./Containers/RateRide";
import ApplyCoupon from "./Containers/ChooseRide/ApplyCoupon";
import PlaceSearch from "./Containers/Home/BookRide/PlaceSearch";
import TimeSelector from "./Containers/Home/OutStation/TimeSelector";
import ConfirmBooking from "./Containers/Home/OutStation/ConfirmBooking";
import GoAppAnalytics from "../../utils/GoAppAnalytics";
import { Stack } from "../../utils/Navigators";

export const AuthNavigator = () => (
	<Stack.Navigator headerMode={"none"}>
		<Stack.Screen name={"Auth"} component={Auth} />
	</Stack.Navigator>
);

let data = null;

const Navigator = () => (
	<Stack.Navigator headerMode={"none"}>
		<Stack.Screen
			name={"Splash"}
			component={props => <Splash {...data} {...props} />}
		/>
		<Stack.Screen name={"Home"} component={Home} />
		<Stack.Screen name={"ChooseRide"} component={ChooseRide} />
		<Stack.Screen name={"TrackRide"} component={TrackRide} />
		<Stack.Screen name={"RateRide"} component={RateRide} />
		<Stack.Screen name={"ApplyCoupon"} component={ApplyCoupon} />
		<Stack.Screen name={"PlaceSearch"} component={PlaceSearch} />
		<Stack.Screen name={"TimeSelector"} component={TimeSelector} />
		<Stack.Screen name={"ConfirmBooking"} component={ConfirmBooking} />
	</Stack.Navigator>
);

const RegisterScreen = props => {
	data = props;
	return (
		<Navigator
			onNavigationStateChange={(prevState, currState) =>
				GoAppAnalytics.logChangeOfScreenInStack("ola", prevState, currState)
			}
		/>
	);
};

export default RegisterScreen;
