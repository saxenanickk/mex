import React from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import { getNewReducer, removeExistingReducer } from "../../../App";
import reducer from "./Reducer";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";

export default class Ola extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "ola" });
		getNewReducer({ name: "ola", reducer: reducer });
	}

	componentWillUnmount() {
		removeExistingReducer("ola");
	}

	render() {
		//Deeplink Params
		return <App {...this.props} />;
	}
}

AppRegistry.registerComponent("Ola", () => Ola);
