import React from "react";
import { Text } from "react-native";
import {
	font_one,
	font_two,
	font_three,
	font_four
} from "../../../Assets/styles";

export const OlaTextLight = props => (
	<Text
		style={[{ fontFamily: font_one }, props.style]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}
		placeholder={props.placeholder}
		placeholderTextColor={props.placeholderTextColor}>
		{props.children}
	</Text>
);

export const OlaTextRegular = props => (
	<Text
		style={[{ fontFamily: font_two }, props.style]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}
		placeholder={props.placeholder}
		placeholderTextColor={props.placeholderTextColor}>
		{props.children}
	</Text>
);

export const OlaTextMedium = props => (
	<Text
		style={[{ fontFamily: font_three }, props.style]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}
		placeholder={props.placeholder}
		placeholderTextColor={props.placeholderTextColor}>
		{props.children}
	</Text>
);

export const OlaTextBold = props => (
	<Text
		style={[{ fontFamily: font_four }, props.style]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}
		placeholder={props.placeholder}
		placeholderTextColor={props.placeholderTextColor}>
		{props.children}
	</Text>
);
