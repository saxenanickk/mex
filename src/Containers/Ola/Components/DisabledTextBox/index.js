import React from "react";
import { View, TouchableOpacity, Animated } from "react-native";
import { OlaTextRegular } from "../../Components/OlaText";
import styles from "./styles";

const DisabledTextBox = props => {
	return (
		<Animated.View style={[styles.container, props.disabledTextBoxStyle]}>
			<TouchableOpacity
				style={[styles.textButtonContainer, props.textButtonStyle]}
				activeOpacity={1}
				onPress={props.onPress}>
				<View
					style={[
						styles.iconView,
						props.iconStyle,
						{
							backgroundColor: props.iconColor
						}
					]}
				/>
				<View style={styles.textViewContainer}>
					{props.selected ? (
						<OlaTextRegular
							numberOfLines={1}
							ellipsizeMode={"tail"}
							style={[styles.labelText]}>
							{props.label}
						</OlaTextRegular>
					) : null}
					<OlaTextRegular
						numberOfLines={1}
						ellipsizeMode={"tail"}
						style={[styles.text, props.textStyle]}>
						{props.text}
					</OlaTextRegular>
				</View>
			</TouchableOpacity>
			{props.children ? (
				<View
					style={{
						flex: 0.1,
						justifyContent: "center",
						alignItems: "center"
					}}>
					{props.children}
				</View>
			) : null}
		</Animated.View>
	);
};

DisabledTextBox.defaultProps = {
	iconStyle: {}
};

export default DisabledTextBox;
