import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	text: {
		margin: 0,
		padding: 0
	},
	labelText: {
		fontSize: width / 35,
		color: "#707070"
	},
	textViewContainer: {
		marginLeft: width / 35,
		justifyContent: "center"
	},
	iconView: {
		alignSelf: "center",
		width: width / 60,
		height: width / 60,
		borderRadius: width / 70
	},
	textButtonContainer: {
		height: height / 17,
		flexDirection: "row",
		alignItems: "center",
		flex: 0.9
	},
	container: {
		paddingHorizontal: width / 40,
		position: "absolute",
		flexDirection: "row",
		alignItems: "center",
		width: width / 1.07,
		height: height / 16,
		backgroundColor: "#efefef",
		justifyContent: "space-between"
	}
});

export default styles;
