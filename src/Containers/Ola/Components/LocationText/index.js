import React from "react";
import { View, Dimensions } from "react-native";
import I18n from "../../Assets/Strings/i18n";
import { OlaTextRegular } from "../OlaText";
import styles from "./style";
import { GoToast } from "../../../../Components";
const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const LocationText = props => {
	return (
		<View style={[styles.container, props.locationTextStyle]}>
			<View
				style={
					props.locationType === "origin"
						? styles.originLocation
						: styles.destinationLocation
				}
			/>

			<OlaTextRegular
				onPress={() =>
					GoToast.show(I18n.t("feature_not_available"), I18n.t("information"))
				}
				ellipsizeMode={"tail"}
				numberOfLines={1}
				style={[styles.searchBarTextInput, props.textStyle]}
				placeholderTextColor={"#3f3f3f"}
				placeholder={props.placeholder}>
				{props.locationText}
			</OlaTextRegular>
		</View>
	);
};

export default LocationText;
