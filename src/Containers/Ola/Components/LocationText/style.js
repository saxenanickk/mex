import { StyleSheet, Dimensions } from "react-native";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: {
		elevation: 5,
		backgroundColor: "#ffffff",
		alignItems: "center",
		justifyContent: "flex-start",
		flexDirection: "row",
		width: width,
		paddingHorizontal: width / 25,
		paddingVertical: width / 22
	},
	originLocation: {
		width: 7,
		height: 7,
		borderRadius: 3.5,
		backgroundColor: "#54a624"
	},
	destinationLocation: {
		width: 7,
		height: 7,
		borderRadius: 3.5,
		backgroundColor: "#ff665a"
	},
	searchBarTextInput: {
		justifyContent: "flex-start",
		alignItems: "center",
		marginLeft: width / 35,
		borderWidth: 0,
		fontSize: width / 30,
		color: "#3f3f3f"
	}
});

export default styles;
