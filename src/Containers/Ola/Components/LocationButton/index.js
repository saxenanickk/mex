import React from "react";
import { TouchableOpacity, Dimensions, Platform } from "react-native";
import Icon from "../../../../Components/Icon";
import Geolocation from "@react-native-community/geolocation";
import * as Location from "../../../../CustomModules/LocationModule/golocation";
import styles from "./styles";

const { width, height } = Dimensions.get("window");

const onLocationClick = props => {
	if (Platform.OS === "ios") {
		Geolocation.getCurrentPosition(
			position => {
				props.currentPosition({
					latitude: position.coords.latitude,
					longitude: position.coords.longitude
				});
			},
			error => {
				console.log(error);
				props.onError();
			}
		);
	} else {
		// @ts-ignore
		Location.getCurrentPosition(
			/**
			 * @param {{ coords: { latitude: number; longitude: number; }; }} position
			 */
			position => {
				props.currentPosition({
					latitude: position.coords.latitude,
					longitude: position.coords.longitude
				});
			},
			/**
			 * @param {any} error
			 */
			error => {
				console.log(error);
				props.onError();
			}
		);
	}
};

const LocationButton = props => {
	return (
		<TouchableOpacity
			onPress={() => onLocationClick(props)}
			style={[styles.container, props.locationStyle]}>
			<Icon
				iconType={
					typeof props.iconType !== "undefined" || props.iconType != null
						? props.iconName
						: "material"
				}
				iconName={
					typeof props.iconName !== "undefined" || props.iconName != null
						? props.iconName
						: "my-location"
				}
				iconSize={
					typeof props.iconSize !== "undefined" || props.iconSize != null
						? props.iconSize
						: width / 14
				}
				iconColor={
					typeof props.iconColor !== "undefined" || props.iconColor != null
						? props.iconColor
						: "#000000"
				}
			/>
		</TouchableOpacity>
	);
};

LocationButton.defaultProps = {
	onError: () => {}
};

export default LocationButton;
