import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: {
		position: "absolute",
		right: width / 25,
		bottom: height / 3.5,
		backgroundColor: "#ffffff",
		width: width / 9,
		height: width / 9,
		borderRadius: width / 18,
		borderColor: "#ffffff",
		elevation: 8,
		justifyContent: "center",
		alignItems: "center"
	}
});

export default styles;
