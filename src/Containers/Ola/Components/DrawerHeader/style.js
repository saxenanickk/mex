import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

const styles = StyleSheet.create({
	header: {
		width: width,
		height: height / 13.33,
		flexDirection: "row",
		alignItems: "center",
		paddingLeft: width / 25
	},
	button: {
		justifyContent: "center",
		alignItems: "center",
		height: height / 21,
		borderRadius: width / 20,
		paddingHorizontal: width / 35
	},
	buttonActive: {
		backgroundColor: "#DBE147"
	},
	textStyle: {
		fontSize: width / 30.3,
		color: "#000000"
	},
	textStyleActive: {
		fontSize: width / 30.3,
		color: "#000000"
	},
	drawerView: {
		flex: 1,
		paddingHorizontal: width / 35,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-around"
	}
});

export default styles;
