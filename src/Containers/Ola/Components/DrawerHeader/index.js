import React from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	Animated,
	Easing
} from "react-native";
import { Icon } from "../../../../Components";
import I18n from "../../Assets/Strings/i18n";
import { OlaTextMedium, OlaTextRegular } from "../OlaText";
import styles from "./style";
const { width, height } = Dimensions.get("window");

const DrawerHeader = class extends React.Component {
	constructor(props) {
		super(props);
		this.headerAnimatedValue = new Animated.Value(0);
	}

	fadeOutAnimation = () => {
		Animated.timing(this.headerAnimatedValue, {
			toValue: -height / 13.33,
			duration: 500,
			easing: Easing.linear
		}).start();
	};

	fadeInAnimation = () => {
		Animated.timing(this.headerAnimatedValue, {
			toValue: 0,
			duration: 500,
			easing: Easing.linear
		}).start();
	};

	render() {
		return (
			<Animated.View
				style={[
					styles.header,
					this.props.headerStyle,
					{ top: this.headerAnimatedValue }
				]}>
				<TouchableOpacity onPress={this.props.onPress}>
					<Icon
						iconType={"material"}
						iconName={"menu"}
						iconColor={"#000000"}
						iconSize={width / 15}
					/>
				</TouchableOpacity>
				<View style={styles.drawerView}>
					{this.props.rideTypes.map((rideType, index) => {
						return (
							<TouchableOpacity
								activeOpacity={1}
								key={index}
								style={
									index === this.props.selectedRideType
										? [styles.button, styles.buttonActive]
										: styles.button
								}
								onPress={() => this.props.changeRideType(index)}>
								{index === this.props.selectedRideType ? (
									<OlaTextMedium style={styles.textStyleActive}>
										{I18n.t(rideType)}
									</OlaTextMedium>
								) : (
									<OlaTextRegular style={styles.textStyle}>
										{I18n.t(rideType)}
									</OlaTextRegular>
								)}
							</TouchableOpacity>
						);
					})}
				</View>
			</Animated.View>
		);
	}
};

export default DrawerHeader;
