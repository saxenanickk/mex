import { all } from "redux-saga/effects";
import { olaAuthSaga } from "./Containers/Auth/Saga";
import { olaBookRideSaga } from "./Containers/Home/BookRide/Saga";
import { olaChooseRideSaga } from "./Containers/ChooseRide/Saga";
import { olaTrackRideSaga } from "./Containers/TrackRide/Saga";
import { olaOutstationSaga } from "./Containers/Home/OutStation/Saga";
import { olaConfirmRentalRideSaga } from "./Containers/Home/Confirm/Saga";
import { olaRidesSaga } from "./Containers/Home/YourRides/Saga";
import { olaRateRideSaga } from "./Containers/RateRide/Saga";

export const SAVE_OLA_DEEPLINK_STATE = "SAVE_OLA_DEEPLINK_STATE";

export const saveOlaDeeplinkState = payload => ({
	type: SAVE_OLA_DEEPLINK_STATE,
	payload
});

export default function* olaSaga() {
	yield all([
		olaAuthSaga(),
		olaBookRideSaga(),
		olaChooseRideSaga(),
		olaTrackRideSaga(),
		olaOutstationSaga(),
		olaConfirmRentalRideSaga(),
		olaRidesSaga(),
		olaRateRideSaga()
	]);
}
