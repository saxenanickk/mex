import Config from "react-native-config";
import qs from "query-string";
import polyline from "@mapbox/polyline";

const {
	SERVER_BASE_URL_OLA_V1,
	SERVER_BASE_URL_OLA_V5,
	SERVER_BASE_OLA_CONTEXT_URL,
	SERVER_BASE_SAVE_FAVOURITE_LOCATION,
	SERVER_TYPE
} = Config;

class OlaApi {
	constructor() {
		console.log("Ola Api");
	}

	/**
	 * Makes an API call to Get users rides
	 * @param {object} params
	 * params: {
	 * access_token: <access_token>,
	 * page: <page_no>
	 * }
	 */
	getUserHistory(params) {
		let ACCESS_TOKEN = "Bearer " + params.access_token;
		const PAGE = params.page ? params.page : 1;
		let USER_TOKEN = params.appToken;
		console.log("API Called (getUserHistory): ");
		return new Promise(function(resolve, reject) {
			fetch(`${SERVER_BASE_URL_OLA_V1}/history?page=${PAGE}`, {
				method: "GET",
				headers: {
					authorization: ACCESS_TOKEN,
					"X-Access-Token": USER_TOKEN
				}
			})
				.then(result => {
					result.json().then(res => {
						resolve(res);
					});
				})
				.catch(error => {
					console.log("Error of API (HISTORY): ", error);
					reject(error);
				});
		});
	}

	/*
	 * API to validate coupon codes
	 * Params : couponCode,pickupLat, pickupLng, category
	 */
	validateCoupon(params) {
		let ACCESS_TOKEN = "Bearer " + params.access_token;
		let couponCode = params.coupon_code;
		let pickupLat = params.pickup_lat;
		let pickupLng = params.pickup_lng;
		let category = params.category;
		let fareId = params.fare_id;
		let USER_TOKEN = params.appToken;

		return new Promise(function(resolve, reject) {
			fetch(
				SERVER_BASE_URL_OLA_V1 +
					"/coupons/validate?coupon_code=" +
					couponCode +
					"&pickup_lat=" +
					pickupLat +
					"&pickup_lng" +
					pickupLng +
					"&category=" +
					category +
					"&fare_id=" +
					fareId,
				{
					method: "GET",
					headers: {
						authorization: ACCESS_TOKEN,
						"X-Access-Token": USER_TOKEN
					}
				}
			)
				.then(result => {
					console.log(result);
					result.json().then(res => {
						resolve(res);
					});
				})
				.catch(error => {
					console.log("Error of API (validateCoupon): ", error);
					reject(error);
				});
		});
	}

	getRideProducts(params) {
		let ACCESS_TOKEN = "Bearer " + params.access_token;
		let USER_TOKEN = params.appToken;
		let pickupLat = params.pickup_lat;
		let pickupLng = params.pickup_lng;
		let category = params.category;

		return new Promise(function(resolve, reject) {
			fetch(
				SERVER_BASE_URL_OLA_V1 +
					"/products?pickup_lat=" +
					pickupLat +
					"&pickup_lng=" +
					pickupLng +
					"&category=" +
					category +
					"",
				{
					method: "GET",
					headers: {
						Authorization: ACCESS_TOKEN,
						"X-Access-Token": USER_TOKEN
					}
				}
			)
				.then(result => {
					result.json().then(res => {
						resolve(res);
					});
				})
				.catch(error => {
					console.log("Error of API (RIDE PRODUCTS): ", error);
					reject(error);
				});
		});
	}

	getOutstationProducts(params) {
		let ACCESS_TOKEN = params.access_token;
		let USER_TOKEN = params.appToken;
		let pickupLat = params.pickup_lat;
		let pickupLng = params.pickup_lng;
		let dropLat = params.drop_lat;
		let dropLng = params.drop_lng;
		let pickupTime = params.pickup_time;
		let pickupMode = params.pickup_mode;
		let dropTime = params.drop_time !== undefined ? params.drop_time : "";

		return new Promise(function(resolve, reject) {
			console.log("outstation request for products", params);
			fetch(
				SERVER_BASE_URL_OLA_V5 +
					"/products?" +
					"pickup_lat=" +
					pickupLat +
					"&pickup_lng=" +
					pickupLng +
					"&drop_lat=" +
					dropLat +
					"&drop_lng=" +
					dropLng +
					"&category=outstation&pickup_time=" +
					pickupTime +
					"&pickup_mode=" +
					pickupMode +
					"&drop_time=" +
					dropTime,
				{
					method: "GET",
					headers: {
						Authorization: ACCESS_TOKEN,
						"X-Access-Token": USER_TOKEN
					}
				}
			)
				.then(result => {
					console.log("result is", result);
					result.json().then(res => {
						console.log("outstation products response");
						resolve(res);
					});
				})
				.catch(error => {
					console.log("Error of API (OUTSTATION): ", error);
					reject(error);
				});
		});
	}

	/*
	 * API to trigger sos signal
	 * Params : couponCode,pickupLat, pickupLng, category
	 */
	sosSignal(params) {
		let ACCESS_TOKEN = "Bearer " + params.access_token;
		let requestId = params.request_id;
		let USER_TOKEN = params.appToken;

		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_URL_OLA_V1 + "/sos/signal", {
				method: "POST",
				headers: {
					authorization: ACCESS_TOKEN,
					"X-Access-Token": USER_TOKEN
				},
				body: qs.stringify({
					request_id: requestId
				})
			})
				.then(result => {
					result.json().then(res => {
						resolve(res);
					});
				})
				.catch(error => {
					console.log("Error of API (sosSignal): ", error);
					reject(error);
				});
		});
	}

	/**
	 * Estimate API
	 * Params :  pickupLat,pickupLng,dropLat,dropLng, category (optional)
	 */
	getRideEstimate(params) {
		let ACCESS_TOKEN = "Bearer " + params.access_token;
		let pickupLat = params.pickup_lat;
		let pickupLng = params.pickup_lng;
		let category = params.category;
		let dropLat = params.drop_lat;
		let dropLng = params.drop_lng;
		let USER_TOKEN = params.appToken;

		return new Promise(function(resolve, reject) {
			fetch(
				SERVER_BASE_URL_OLA_V1 +
					"/requests/estimate?pickup_lat=" +
					pickupLat +
					"&pickup_lng=" +
					pickupLng +
					"&drop_lat=" +
					dropLat +
					"&drop_lng=" +
					dropLng +
					"&category=" +
					category +
					"",
				{
					method: "GET",
					headers: {
						authorization: ACCESS_TOKEN,
						"X-Access-Token": USER_TOKEN
					}
				}
			)
				.then(result => {
					result.json().then(res => {
						resolve(res);
					});
				})
				.catch(error => {
					console.log("Error of API (RIDE ESTIMATE): ", error);
					reject(error);
				});
		});
	}

	/**
	 * @param {*} params
	 * success response
	 * booking_id:"CRN121223845"
			cab_number:"KA102HZ081"
			cab_type:"suv"
			car_color:"White"
			car_model:"Etios"
			crn: "121223845"
			driver_lat:13.005369
			driver_lng:77.688898
			driver_name:"Driver"
			driver_number:"7190102081"
			eta
			:
			1
			otp
			:
			start_trip
			:
			{text: "OTP to start ride", value: "8057"}
			__proto__
			:
			Object
			share_ride_url
			:
			"https://olacabs-dev.in/track/23jjel5l4"
			status
			:
			"SUCCESS"
			surcharge_value
			:
			"1.5X"
	 */

	requests(params) {
		console.log("params are", params);
		let bodyObject = {
			pickup_lat: SERVER_TYPE === "live" ? params.pickup_lat : 12.9502, // Pickup Latitude (Mandatory)
			pickup_lng: SERVER_TYPE === "live" ? params.pickup_lng : 77.6417, // Pickup Longitude (Mandatory)
			category: params.category, // Category (Mandatory)
			pickup_mode: params.pickup_mode, // Pickup Mode ["now", "later"] (Mandatory)
			coupon_code: params.coupon_code // Coupon Code (Optional)
		};
		let ACCESS_TOKEN = "Bearer " + params.access_token;
		let USER_TOKEN = params.appToken;

		if (params.category === "rental") {
			if (params.pickup_mode === "LATER") {
				bodyObject = {
					...bodyObject,
					pickup_time: params.pickup_time
				};
			}
			bodyObject = {
				...bodyObject,
				sub_category: params.sub_category, // Sub Category (Mandatory)
				package_id: params.package_id // Package Id (Mandatory)
			};
		} else if (params.category === "outstation") {
			bodyObject = {
				...bodyObject,
				drop_lat: params.drop_lat, // Drop Latitude (Mandatory)
				drop_lng: params.drop_lng, // Drop Longitude (Mandatory)
				sub_category: params.sub_category, // Sub Category (Mandatory)
				type: params.type, // Type ["onw_way", "two_way"] (Mandatory)
				pickup_time: params.pickup_time, // Pickup Time (Mandatory)
				drop_time: params.drop_time, // Drop Time (Optional, But Mandatory if two_way)
				trip_id: params.trip_id
			};
		} else if (params.category === "share") {
			bodyObject = {
				...bodyObject,
				drop_lat: params.drop_lat, // Drop Latitude (Mandatory)
				drop_lng: params.drop_lng, // Drop Longitude (Mandatory)
				seats: params.seat_count // Seat Count (Mandatory)
			};
		} else {
			if (params.pickup_mode === "LATER") {
				bodyObject = {
					...bodyObject,
					pickup_time: params.pickup_time
				};
			}
			bodyObject = {
				...bodyObject,
				drop_lat: params.drop_lat, // Drop Latitude (Mandatory)
				drop_lng: params.drop_lng // Drop Longitude (Mandatory)
			};
		}

		console.log("Request Api Data: ", bodyObject);

		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_URL_OLA_V5 + "/requests", {
				method: "POST",
				headers: {
					authorization: ACCESS_TOKEN,
					"X-Access-Token": USER_TOKEN,
					"Content-Type": "application/x-www-form-urlencoded"
				},
				body: qs.stringify(bodyObject)
			})
				.then(result => {
					result.json().then(res => {
						resolve(res);
					});
				})
				.catch(error => {
					console.log("Error of API (Requests): ", error);
					reject(error);
				});
		});
	}

	/**
	 * Abort Ride API
	 * Params :  requestId (Booking Id)
	 */
	abortBooking(params) {
		let ACCESS_TOKEN = "Bearer " + params.access_token;
		let requestId = params.request_id;
		let USER_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_URL_OLA_V1 + "/requests/abort", {
				method: "POST",
				headers: {
					authorization: ACCESS_TOKEN,
					"X-Access-Token": USER_TOKEN,
					"Content-Type": "application/x-www-form-urlencoded"
				},
				body: qs.stringify({
					request_id: requestId
				})
			})
				.then(result => {
					result.json().then(res => {
						resolve(res);
					});
				})
				.catch(error => {
					console.log("Error of API (Abort): ", error);
					reject(error);
				});
		});
	}

	/**
	 * Update drop location api
	 */
	updateDropLocation(params) {
		console.log("params are", params);
		let ACCESS_TOKEN = "Bearer" + " " + params.access_token;
		let USER_TOKEN = params.appToken;

		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_URL_OLA_V1 + "/requests/update/drop_location", {
				method: "POST",
				headers: {
					authorization: ACCESS_TOKEN,
					"X-Access-Token": USER_TOKEN,
					"Content-Type": "application/x-www-form-urlencoded"
				},
				body: qs.stringify({
					drop_lat: params.drop_lat,
					drop_lng: params.drop_lng,
					booking_id: params.booking_id
				})
			})
				.then(result => {
					result.json().then(res => {
						console.log("Result of API (updateDropLocation): ", res);
						resolve(res);
					});
				})
				.catch(error => {
					console.log("Error of API (updateDropLocation): ", error);
					reject(error);
				});
		});
	}

	getCurrentRide(params) {
		let ACCESS_TOKEN = "Bearer " + params.access_token;
		let USER_TOKEN = params.appToken;
		let requestId = params.request_id;

		return new Promise(function(resolve, reject) {
			fetch(
				SERVER_BASE_URL_OLA_V1 + "/requests/current?request_id=" + requestId,
				{
					method: "GET",
					headers: {
						authorization: ACCESS_TOKEN,
						"X-Access-Token": USER_TOKEN
					}
				}
			)
				.then(result => {
					if (result.status !== 200) {
						return reject();
					}
					result.json().then(res => {
						resolve(res);
					});
				})
				.catch(error => {
					console.log("Error of API (getCurrentRide): ", error);
					reject(error);
				});
		});
	}

	cancelRide(params) {
		let ACCESS_TOKEN = "Bearer " + params.access_token;
		let USER_TOKEN = params.appToken;
		let requestId = params.request_id;
		let reason = params.reason;
		console.log("cancel ride is called from the api", params);
		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_URL_OLA_V1 + "/requests/cancel", {
				method: "POST",
				headers: {
					authorization: ACCESS_TOKEN,
					"X-Access-Token": USER_TOKEN,
					"Content-Type": "application/x-www-form-urlencoded"
				},
				body: qs.stringify({
					request_id: requestId,
					reason: reason
				})
			})
				.then(result => {
					result.json().then(res => {
						resolve(res);
					});
				})
				.catch(error => {
					console.log("Error of API (cancelRide): ", error);
					reject(error);
				});
		});
	}

	getCancelReasons(params) {
		let ACCESS_TOKEN = "Bearer " + params.access_token;
		let USER_TOKEN = params.appToken;
		let pickupLat = params.pickup_lat;
		let pickupLng = params.pickup_lng;
		let category = params.category;
		console.log("parameter in cancel reason", params);
		return new Promise(function(resolve, reject) {
			fetch(
				SERVER_BASE_URL_OLA_V1 +
					"/requests/cancel/reasons?pickup_lat=" +
					pickupLat +
					"&pickup_lng=" +
					pickupLng +
					"&category=" +
					category +
					"",
				{
					method: "GET",
					headers: {
						authorization: ACCESS_TOKEN,
						"X-Access-Token": USER_TOKEN
					}
				}
			)
				.then(result => {
					result.json().then(res => {
						resolve(res);
					});
				})
				.catch(error => {
					console.log("Error of API (getCancelReasons): ", error);
					reject(error);
				});
		});
	}

	/**
	 * @param {*} requestId,rating,feedback, comment
	 */
	postRideFeedback(params) {
		let ACCESS_TOKEN = params.token_type + " " + params.access_token;
		let USER_TOKEN = params.appToken;
		let requestId = params.request_id;
		let comments = params.comments;
		let rating = params.rating;
		let feedback = params.feedback;

		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_URL_OLA_V1 + "/requests/feedback", {
				method: "POST",
				headers: {
					authorization: ACCESS_TOKEN,
					"X-Access-Token": USER_TOKEN,
					"Content-Type": "application/x-www-form-urlencoded"
				},
				body: qs.stringify({
					request_id: requestId,
					rating: rating,
					feedback: feedback,
					comments: comments
				})
			})
				.then(result => {
					result.json().then(res => {
						resolve(res);
					});
				})
				.catch(error => {
					console.log("Error of API (postRideFeedback): ", error);
					reject(error);
				});
		});
	}
	/**
	 * @param {*} category optional
	 */
	getFeedbackOptions(params) {
		let ACCESS_TOKEN = params.token_type + " " + params.access_token;
		let USER_TOKEN = params.appToken;
		let category = params.category;

		return new Promise(function(resolve, reject) {
			fetch(
				SERVER_BASE_URL_OLA_V1 +
					"/requests/feedback/options?category=" +
					category +
					"",
				{
					method: "GET",
					headers: {
						authorization: ACCESS_TOKEN,
						"X-Access-Token": USER_TOKEN
					}
				}
			)
				.then(result => {
					result.json().then(res => {
						resolve(res);
					});
				})
				.catch(error => {
					console.log("Error of API (getFeedbackOptions): ", error);
					reject(error);
				});
		});
	}

	/**
	 * Returns user favourite places
	 */
	getUserFavourites(params) {
		let ACCESS_TOKEN = "Bearer " + params.access_token;
		let USER_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_URL_OLA_V1 + "/user/favourites", {
				method: "GET",
				headers: {
					authorization: ACCESS_TOKEN,
					"X-Access-Token": USER_TOKEN
				}
			})
				.then(result => {
					result.json().then(res => {
						resolve(res);
					});
				})
				.catch(error => {
					console.log("Unable to fetch user favourites", error);
					reject(error);
				});
		});
	}

	/**
	 * Google Maps API KEY required.
	 */
	drawMapPolyline(params) {
		let originLatitude = params.origin.latitude;
		let originLongitude = params.origin.longitude;
		let destinationLatitude = params.destination.latitude;
		let destinationLongitude = params.destination.longitude;
		const waypoints = params.waypoints ? params.waypoints : [];

		let apiRoute =
			"https://maps.googleapis.com/maps/api/directions/json?origin=" +
			originLatitude +
			"," +
			originLongitude +
			"&destination=" +
			destinationLatitude +
			"," +
			destinationLongitude;

		if (waypoints.length > 0) {
			apiRoute += "&waypoints=";
			for (let i = 0; i < waypoints.length; i++) {
				if (i < waypoints.length - 1) {
					apiRoute += waypoints[i].lat + "," + waypoints[i].lng + "|";
				} else {
					apiRoute += waypoints[i].lat + "," + waypoints[i].lng;
				}
			}
		}

		apiRoute += `&key=${Config.MEX_GOOGLE_MAP_PATH_KEY}`;

		return new Promise(function(resolve, reject) {
			fetch(apiRoute, {
				method: "GET"
			})
				.then(result => {
					result.json().then(res => {
						if (res.status !== "OK") {
							reject(res);
						}
						const points = polyline.decode(
							res.routes[0].overview_polyline.points
						);
						console.log(points);
						let coords = points.map((point, index) => {
							return {
								latitude: point[0],
								longitude: point[1]
							};
						});
						resolve({ path: coords });
					});
				})
				.catch(error => {
					console.log("Error of API (Requests): ", error);
					reject(error);
				});
		});
	}

	/**
	 * Send Ola access token to server for context purpose
	 */

	saveUserAuth(params) {
		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_OLA_CONTEXT_URL, {
				method: "POST",
				headers: {
					"X-ACCESS-TOKEN": params.appToken,
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					auth: `Bearer ${params.authToken}`
				})
			});
		});
	}

	/**
	 * SAVE FAVOURITE LOCATION FOR CONTEXT
	 */
	saveFavouriteLocation(params) {
		return new Promise(function(resolve, reject) {
			fetch(SERVER_BASE_SAVE_FAVOURITE_LOCATION, {
				method: "POST",
				headers: {
					"X-ACCESS-TOKEN": params.appToken,
					"Content-Type": "application/json"
				},
				body: JSON.stringify(params.body)
			});
		});
	}
}

export default new OlaApi();
