import React, { Fragment } from "react";
import { SafeAreaView } from "react-native";
import { get } from "lodash";
import AsyncStorage from "@react-native-community/async-storage";
import RegisterScreen, { AuthNavigator } from "./RegisterScreen";
import { connect } from "react-redux";
import { saveOlaDeeplinkState } from "./Saga";
import { olaSaveLoginState, saveOlaCredentials } from "./Containers/Auth/Saga";
import OlaApi from "./Api";
import GoAppAnalytics from "../../utils/GoAppAnalytics";
class App extends React.Component {
	constructor(props) {
		super(props);
		this.checkLoginStatus = this.checkLoginStatus.bind(this);
	}

	componentWillMount() {
		this.checkLoginStatus();
		this.props.dispatch(
			saveOlaDeeplinkState({
				params: this.props.params
					? JSON.parse(this.props.params)
					: this.props.params
			})
		);
	}

	callAuth = (appToken, authToken) => {
		OlaApi.saveUserAuth({ appToken, authToken });
	};

	/**
	 * Function to check login status in ola
	 */
	checkLoginStatus() {
		/* Get user logged in status */
		AsyncStorage.getItem("ola_is_user_logged_in")
			.then(res => {
				if (res) {
					/* Get user credentials like: access_token, refresh_token etc. */
					AsyncStorage.getItem("ola_user_credentials").then(result => {
						// TODO: Check if token is expired or not. If yes then refresh token.
						/* Save user credentials to the store */
						this.props.dispatch(saveOlaCredentials(result));
						this.callAuth(this.props.appToken, result);
					});
					this.props.dispatch(olaSaveLoginState(true));
				} else {
					this.props.dispatch(olaSaveLoginState(false));
				}
			})
			.catch(e => {
				// TODO: Failure Case Handling
				console.log("OLA ASYNC: ", e);
				this.props.dispatch(olaSaveLoginState(false));
			});
	}

	render() {
		return (
			<Fragment>
				{/** For the Top Color (Under the Notch) */}
				<SafeAreaView style={{ flex: 0, backgroundColor: "grey" }} />
				{/** For the Bottom Color (Below the home touch) */}
				<SafeAreaView style={{ flex: 1 }}>
					{this.props.olaIsUserLoggedIn !== null &&
					!this.props.olaIsUserLoggedIn ? (
						<AuthNavigator
							onNavigationStateChange={(prevState, currState) =>
								GoAppAnalytics.logChangeOfScreenInStack(
									"ola",
									prevState,
									currState
								)
							}
						/>
					) : (
						<RegisterScreen {...this.props} />
					)}
				</SafeAreaView>
			</Fragment>
		);
	}
}

function mapStateToProps(state) {
	return {
		location: state.location,
		olaIsUserLoggedIn: get(state, "ola.auth.olaIsUserLoggedIn", null),
		appToken: state.appToken.token
	};
}

export default connect(mapStateToProps)(App);
