import React from "react";
import { Stack } from "../../utils/Navigators";
import Home from "./Containers/Home";
import Splash from "./Containers/Splash";
import LinkView from "./Containers/LinkView";

const RegisterScreen = () => (
	<Stack.Navigator headerMode="none">
		<Stack.Screen name={"Splash"} component={Splash} />
		<Stack.Screen name={"Home"} component={Home} />
		<Stack.Screen name={"LinkView"} component={LinkView} />
	</Stack.Navigator>
);

export default RegisterScreen;
