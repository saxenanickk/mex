import React, { Fragment } from "react";
import { SafeAreaView, StatusBar } from "react-native";
import RegisterScreen from "./RegisterScreen";
import GoAppAnalytics from "../../utils/GoAppAnalytics";
import { CustomAlert } from "../../Components";
import { AlertContext } from "./AlertContext";

export default class App extends React.Component {
	alertRef = React.createRef();
	render() {
		return (
			<Fragment>
				<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
				<AlertContext.Provider value={this.alertRef}>
					<RegisterScreen
						nav={this.props.nav}
						onNavigationStateChange={(prevState, currState) =>
							GoAppAnalytics.logChangeOfScreenInStack(
								"freewifi",
								prevState,
								currState
							)
						}
					/>
				</AlertContext.Provider>
				<CustomAlert ref={this.alertRef} />
			</Fragment>
		);
	}
}
