import { fork, all } from "redux-saga/effects";
import { freeWifiSaga } from "./Containers/Saga";

export default function* freewifiSaga(dispatch) {
	yield all([fork(freeWifiSaga)]);
}
