import React from "react";
import { View, Image, Dimensions } from "react-native";
import { connect } from "react-redux";
import ApplicationToken from "../../../../CustomModules/ApplicationToken";
import { FREEWIFI } from "../../Assets/Img";
import { AlertContext } from "../../AlertContext";
import { freeWifiGetSsid } from "../Saga";
import { CommonActions } from "@react-navigation/native";

class Splash extends React.Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		this.checkAppToken(this.props);
	}

	shouldComponentUpdate(props, state) {
		if (props.appToken !== null && props.appToken !== this.props.appToken) {
			this.checkAppToken(props);
		}
		return true;
	}

	checkAppToken(props) {
		if (props.appToken !== null) {
			this.props.dispatch(
				freeWifiGetSsid({
					appToken: props.appToken,
					site_id: props.selectedSite.id
				})
			);
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "Home" }]
				})
			);
		}
	}

	render() {
		return (
			<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
				<ApplicationToken />
				<Image
					source={FREEWIFI}
					style={{
						width: Dimensions.get("window").width / 2,
						height: Dimensions.get("window").height / 2
					}}
					resizeMode={"contain"}
				/>
			</View>
		);
	}
}

Splash.contextType = AlertContext;
function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		user: state.appToken.user,
		selectedSite: state.appToken && state.appToken.selectedSite
	};
}

export default connect(mapStateToProps)(Splash);
