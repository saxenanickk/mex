import React, { Component } from "react";
import { TouchableOpacity, View, Dimensions, BackHandler } from "react-native";
import { WebView } from "react-native-webview";
import { connect } from "react-redux";
import { Icon, ProgressBar } from "../../../../Components";
import styles from "./style";
import { FreeWifiTextRegular } from "../../Components";

const { width, height } = Dimensions.get("window");
class LinkView extends Component {
	constructor(props) {
		super(props);
		this.state = {
			barProgress: 100
		};
	}

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.headerBar}>
					<View style={styles.centeredRow}>
						<TouchableOpacity
							style={styles.goBackButton}
							onPress={() => this.props.navigation.goBack()}>
							<Icon
								iconType={"ionicon"}
								iconName={"ios-arrow-back"}
								iconSize={width / 16}
								iconColor={"#000"}
							/>
						</TouchableOpacity>
						<FreeWifiTextRegular style={styles.bookText} numberOfLines={1}>
							{"http://www.mexit.in/"}
						</FreeWifiTextRegular>
					</View>
				</View>
				{this.state.barProgress !== 100 ? (
					<ProgressBar
						width={width / 5}
						height={height / 200}
						value={this.state.barProgress}
						backgroundColor="#000"
						barEasing={"linear"}
					/>
				) : null}
				<WebView
					source={{
						uri: "http://www.mexit.in/"
					}}
					onLoad={event => console.log("on load", event.nativeEvent)}
					onLoadProgress={event =>
						console.log("on load progress", event.nativeEvent)
					}
					ref={ref => (this.webViewRef = ref)}
				/>
			</View>
		);
	}
}

function mapStateToProps(state) {
	console.log(state);
	return {};
}

export default connect(
	mapStateToProps,
	null,
	null
)(LinkView);
