import { takeLatest, put, call } from "redux-saga/effects";
import Api from "../Api";

/**
 * constants
 */

export const FREEWIFI_SAVE_SELECTED_SITE = "FREEWIFI_SAVE_SELECTED_SITE";
export const FREEWIFI_GET_AVAILABLE_SSID = "FREEWIFI_GET_AVAILABLE_SSID";
export const FREEWIFI_SAVE_AVAILABLE_SSID = "FREEWIFI_SAVE_AVAILABLE_SSID";

export const freeWifiSaveSelectedSite = payload => ({
	type: FREEWIFI_SAVE_SELECTED_SITE,
	payload
});

export const freeWifiGetSsid = payload => ({
	type: FREEWIFI_GET_AVAILABLE_SSID,
	payload
});

export const freeWifiSaveSsid = payload => ({
	type: FREEWIFI_SAVE_AVAILABLE_SSID,
	payload
});
export function* freeWifiSaga(dispatch) {
	yield takeLatest(FREEWIFI_GET_AVAILABLE_SSID, handleFreeWifiGetSsid);
}

/**
 * Handlers
 */

function* handleFreeWifiGetSsid(action) {
	try {
		const ssid = yield call(Api.getAllSsid, {
			...action.payload,
			site_id: action.payload.site_id
		});
		console.log("ssid are", ssid);
		if (ssid.success === 1) {
			yield put(freeWifiSaveSsid(ssid.data[0]));
		}
	} catch (error) {
		console.log("error is", error);
	}
}
