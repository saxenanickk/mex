import React, { Component } from "react";
import {
	View,
	Dimensions,
	Image,
	TouchableOpacity,
	ScrollView,
	Linking,
	Clipboard,
	Platform
} from "react-native";
import { connect } from "react-redux";
import { styles } from "./style";
import { FreeWifiTextMedium, FreeWifiTextRegular } from "../../Components";
import { USERPIC, TAP, LINE, ARCHIVES, PADLOCK } from "../../Assets/Img";
import Api from "../../Api";
import wifi from "react-native-android-wifi";
import { ProgressScreen, GoToast } from "../../../../Components";
import {
	wifiCurrentSSID,
	wifiConnect,
	wifiDisconnect
} from "../../../../CustomModules/IosWifi.ios";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";

const { height, width } = Dimensions.get("window");

const secondarySuggestion = [
	"If disconnected, please use the same voucher code.",
	"The voucher code & OTP will be valid for 24 hrs.",
	"Once the code expires, visit MEX.WiFi for new code."
];

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isTried: false,
			isAlloted: false,
			voucherCode: "XXXXXX",
			loaderMessage: "",
			isLoading: false
		};
		this.count = 0;
		this.checkWifiInterval = null;
	}

	componentDidMount() {
		this.checkAllotment();
	}

	async checkAllotment() {
		try {
			this.setState({
				isLoading: true,
				loaderMessage: "Checking Alloted Voucher"
			});
			const resp = await Api.checkAllotment({
				appToken: this.props.appToken
			});
			if (resp.success === 0) {
				this.setState({ isAlloted: false, isLoading: false });
			} else {
				this.setState({
					isAlloted: true,
					isLoading: false,
					voucherCode: resp.data
				});
			}
		} catch (error) {
			this.setState({
				isLoading: false
			});
		}
	}

	async copyAndProceed() {
		try {
			GoAppAnalytics.trackWithProperties("copy_proceed_clicked", {
				siteId: this.props.selectedSite.ref_site_id,
				ssid: this.props.ssid.name
			});
			if (this.state.isAlloted) {
				this.setState({
					loaderMessage: "Voucher code copied.",
					isLoading: true
				});
				GoAppAnalytics.trackWithProperties("voucher_code_alloted", {
					siteId: this.props.selectedSite.ref_site_id,
					ssid: this.props.ssid.name
				});
				Clipboard.setString(this.state.voucherCode);
				Platform.OS === "android"
					? this.findAndConnectToWifi()
					: this.findAndConnectToWifiIOS();
			} else {
				this.setState({
					loaderMessage: "Generating Voucher code.",
					isLoading: true
				});
				const resp = await Api.makeAllotment({
					appToken: this.props.appToken,
					siteId: this.props.selectedSite.ref_site_id
				});
				GoAppAnalytics.trackWithProperties("voucher_code_alloted", {
					siteId: this.props.selectedSite.ref_site_id,
					ssid: this.props.ssid.name
				});
				if (resp.success === 0) {
					this.setState({ isAlloted: false, isLoading: false });
					GoToast.show("Unable to allot voucher code.", "Error");
				} else {
					this.setState({ isAlloted: true, voucherCode: resp.data });
					this.setState({ loaderMessage: "Voucher code copied." });
					Clipboard.setString(resp.data);
					Platform.OS === "android"
						? this.findAndConnectToWifi()
						: this.findAndConnectToWifiIOS();
				}
			}
		} catch (error) {
			this.setState({ isLoading: false });
			GoToast.show("Unable to allot voucher code.", "Error");
		}
	}

	async findAndConnectToWifi() {
		let self = this;
		if (this.props.ssid) {
			await wifi.isEnabled(enabled => {
				if (!enabled) {
					self.setState({ loaderMessage: "Turning WiFi On." });
					wifi.setEnabled(true);
					wifi.reScanAndLoadWifiList(
						success => {
							self.setState({ loaderMessage: "Connecting To WiFi." });
							self.connectToWifi();
						},
						error => console.log("error in loading list", error)
					);
				} else {
					self.setState({ loaderMessage: "Connecting To WiFi." });
					wifi.reScanAndLoadWifiList(
						success => {
							self.connectToWifi();
						},
						error => console.log("error in loading list", error)
					);
				}
			});
		} else {
			GoToast.show("Unable To Detect any free wifi", "Error");
		}
	}

	async findAndConnectToWifiIOS() {
		try {
			let self = this;
			if (this.props.ssid) {
				self.setState({ loaderMessage: "Connecting To WiFi." });
				const res = await wifiCurrentSSID();
				if (res === self.props.ssid.name) {
					Linking.openURL("http://www.mexit.in");
				} else {
					await wifiDisconnect(self.props.ssid.name);
				}
				const resp = await wifiConnect(self.props.ssid.name);
				console.log("response is", resp);
				self.setState({ isLoading: false });
				if (resp === "success") {
					setTimeout(() => this.checkAndNavigateIos(), 1000);
				}
			} else {
				GoToast.show("Unable To Detect any free wifi", "Error");
			}
		} catch (error) {
			this.setState({ isLoading: false });
			console.log("error in wifimanager", error);
		}
	}

	async checkAndNavigateIos() {
		let self = this;
		try {
			const res = await wifiCurrentSSID();
			if (res === self.props.ssid.name) {
				GoAppAnalytics.trackWithProperties("connected_wifi", {
					siteId: self.props.selectedSite.ref_site_id,
					ssid: self.props.ssid.name
				});
				Linking.openURL("http://www.mexit.in");
			} else {
				GoToast.show("Unable To Connect to Wifi", "Error");
			}
		} catch (error) {
			console.log("error while navigatio");
		}
	}

	connectToWifi() {
		let self = this;
		wifi.findAndConnect(self.props.ssid.name, "", found => {
			if (found) {
				self.checkWifiInterval = setInterval(() => {
					this.checkAndNavigate();
				}, 500);
			} else {
				this.setState({ isLoading: false });
				GoToast.show("WiFi is not in range, Try again.", "Error");
			}
		});
	}

	checkAndNavigate() {
		let self = this;
		wifi.getSSID(ssid => {
			if (ssid === self.props.ssid.name) {
				this.setState({ isLoading: false });
				GoAppAnalytics.trackWithProperties("connected_wifi", {
					siteId: self.props.selectedSite.ref_site_id,
					ssid: self.props.ssid.name
				});
				this.props.navigation.navigate("LinkView");
				self.count = 0;
				clearInterval(self.checkWifiInterval);
			}
			if (self.count > 3) {
				self.count = 0;
				clearInterval(self.checkWifiInterval);
				this.setState({ isLoading: false });
				GoToast.show("Unable to connect to WiFi, Try again.", "Error");
			}
			self.count++;
		});
	}

	render() {
		return (
			<ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
				{this.state.isLoading && (
					<ProgressScreen
						isMessage={true}
						message={this.state.loaderMessage}
						indicatorColor={"#3398f0"}
						indicatorSize={width / 15}
						primaryMessage={"Hang On"}
						messageBar={{ width: width / 1.5 }}
					/>
				)}
				<View style={styles.header}>
					<FreeWifiTextRegular numberOfLines={1} style={styles.headerFirstText}>
						{"MEX. WiFi"}
					</FreeWifiTextRegular>
					<FreeWifiTextRegular
						numberOfLines={1}
						style={styles.headerSecondaryText}>
						{"Tired of slow connectivity?"}
					</FreeWifiTextRegular>
					<FreeWifiTextRegular
						numberOfLines={1}
						style={styles.headerSecondaryText}>
						{"Get MEX. WiFi copy the code below."}
					</FreeWifiTextRegular>
				</View>
				<View style={styles.userImageSection}>
					<Image source={USERPIC} resizeMode={"contain"} />
				</View>
				<View style={styles.voucherCodeSection}>
					<FreeWifiTextRegular size={height / 55}>
						{"Voucher Code"}
					</FreeWifiTextRegular>
				</View>
				<View style={styles.voucherCodeSection}>
					<View style={styles.voucherText}>
						<FreeWifiTextRegular numberOfLines={1} size={height / 55}>
							{this.state.voucherCode}
						</FreeWifiTextRegular>
					</View>
					<TouchableOpacity
						style={[styles.voucherText, styles.voucherButton]}
						onPress={() => this.copyAndProceed()}>
						<FreeWifiTextRegular
							numberOfLines={1}
							size={height / 55}
							color={"#fff"}>
							{"Copy & Proceed"}
						</FreeWifiTextRegular>
					</TouchableOpacity>
				</View>
				<View style={styles.separator} />
				<View style={styles.howWorkSection}>
					<FreeWifiTextMedium size={height / 50}>
						{"How it works?"}
					</FreeWifiTextMedium>
					<View style={styles.howWorkInnerSection}>
						<Image
							source={TAP}
							style={styles.miniImage}
							resizeMode={"contain"}
						/>
						<View style={styles.suggestionSection}>
							<FreeWifiTextMedium size={height / 60}>
								{"MEX.WiFi"}
							</FreeWifiTextMedium>
							<FreeWifiTextRegular
								numberOfLines={2}
								size={height / 65}
								color={"#777777"}>
								{"Select MEX.WiFi"}
							</FreeWifiTextRegular>
						</View>
					</View>
					<View style={styles.howWorkLine}>
						<Image
							source={LINE}
							style={styles.miniImage}
							resizeMode={"contain"}
						/>
					</View>
					<View style={styles.howWorkInnerSection}>
						<Image
							source={ARCHIVES}
							style={styles.miniImage}
							resizeMode={"contain"}
						/>
						<View style={styles.suggestionSection}>
							<FreeWifiTextMedium size={height / 60}>
								{"Copy Code"}
							</FreeWifiTextMedium>
							<FreeWifiTextRegular
								numberOfLines={2}
								size={height / 65}
								color={"#777777"}>
								{"Copy voucher code & proceed to next page."}
							</FreeWifiTextRegular>
						</View>
					</View>
					<View style={styles.howWorkLine}>
						<Image
							source={LINE}
							style={styles.miniImage}
							resizeMode={"contain"}
						/>
					</View>
					<View style={styles.howWorkInnerSection}>
						<Image
							source={PADLOCK}
							style={styles.miniImage}
							resizeMode={"contain"}
						/>
						<View style={styles.suggestionSection}>
							<FreeWifiTextMedium size={height / 60}>
								{"Authenticate"}
							</FreeWifiTextMedium>
							<FreeWifiTextRegular
								numberOfLines={2}
								size={height / 65}
								color={"#777777"}>
								{
									"Enter voucher code & verify mobile number on airtel wifi page."
								}
							</FreeWifiTextRegular>
						</View>
					</View>
				</View>
				<View style={styles.separator} />
				<View style={styles.howWorkSection}>
					{secondarySuggestion.map((item, index) => (
						<View style={styles.howWorkInnerSection} key={index}>
							<View style={styles.suggestionLowerSection}>
								<FreeWifiTextRegular
									numberOfLines={2}
									size={height / 65}
									color={"#777777"}>
									{`${index + 1}.  ${item}`}
								</FreeWifiTextRegular>
							</View>
						</View>
					))}
				</View>
				<View
					style={[
						styles.separator,
						{ backgroundColor: "#fff", marginVertical: 0 }
					]}
				/>
			</ScrollView>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		netInfo: state.netInfo,
		ssid: state.freewifi && state.freewifi.ssid,
		user: state.appToken.user,
		selectedSite: state.appToken.selectedSite
	};
}

export default connect(mapStateToProps)(Home);
