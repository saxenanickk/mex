import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff"
	},
	header: {
		width: width,
		paddingHorizontal: width / 18.75,
		paddingVertical: height / 50
	},
	headerFirstText: {
		fontSize: height / 35
	},
	headerSecondaryText: {
		fontSize: height / 60,
		color: "#777777"
	},
	userImageSection: {
		width: width,
		alignItems: "flex-end",
		paddingHorizontal: width / 18.75,
		paddingVertical: height / 50
	},
	voucherCodeSection: {
		width: width,
		paddingHorizontal: width / 18.75,
		flexDirection: "row",
		justifyContent: "space-between"
	},
	voucherText: {
		width: width / 2.5,
		marginTop: height / 90,
		alignItems: "center",
		justifyContent: "center",
		paddingHorizontal: width / 60,
		paddingVertical: height / 70,
		borderWidth: 1,
		borderRadius: width / 90,
		borderColor: "#777777"
	},
	voucherButton: {
		backgroundColor: "#3398f0",
		borderColor: "#3398f0"
	},
	separator: {
		width: width,
		height: height / 30,
		marginVertical: height / 50,
		backgroundColor: "#eff1f4"
	},
	howWorkSection: {
		paddingHorizontal: width / 18.75
	},
	howWorkInnerSection: {
		paddingVertical: height / 90,
		marginLeft: width / 60,
		flexDirection: "row",
		alignItems: "center"
	},
	howWorkLine: {
		marginLeft: width / 60,
		flexDirection: "row",
		alignItems: "center"
	},
	miniImage: {
		width: width / 15,
		height: width / 15
	},
	suggestionSection: {
		paddingHorizontal: width / 30,
		width: width / 1.6
	},
	suggestionLowerSection: {
		paddingHorizontal: width / 30,
		width: width / 1.2
	}
});
