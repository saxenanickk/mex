import App from "./App";
import React from "react";
import { AppRegistry } from "react-native";
import reducer from "./Reducer";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";
import { getNewReducer } from "../../../App";

export default class FreeWiFi extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "freewifi" });
		getNewReducer({ name: "freewifi", reducer: reducer });
	}

	componentWillUnmount() {
		// removeExistingReducer("freewifi");
	}

	render() {
		return <App nav={this.props.navigation} />;
	}
}

AppRegistry.registerComponent("FreeWiFi", () => FreeWiFi);
