export const ARCHIVES = require("./archives.png");
export const FREEWIFI = require("./free_wifi.png");
export const LINE = require("./line.png");
export const PADLOCK = require("./padlock.png");
export const RIGHTARROW = require("./right_arrow.png");
export const TAP = require("./tap.png");
export const USERPIC = require("./user_pic.png");
