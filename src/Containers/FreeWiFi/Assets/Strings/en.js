import * as I18n from "../../../../Assets/strings/i18n";

export default {
	/**
	 * FreeWifi App Strings
	 */
	...I18n.default.translations.en
};
