import { FREEWIFI_SAVE_AVAILABLE_SSID } from "./Containers/Saga";

const initialState = {
	type: null,
	ssid: null
};

const freewifiReducer = (state = initialState, action) => {
	switch (action.type) {
		case FREEWIFI_SAVE_AVAILABLE_SSID:
			return {
				...state,
				ssid: action.payload
			};
		default:
			return state;
	}
};

export default freewifiReducer;
