import Config from "react-native-config";

const { SERVER_BASE_URL_WIFI, SERVER_BASE_URL_SITES } = Config;

class FreeWifi {
	constructor() {
		console.log("free wifi instantiated");
	}

	/**
	 *
	 * @param {Object} params
	 * @param {String} params.appToken
	 */
	checkAllotment(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${SERVER_BASE_URL_WIFI}isAlreadyAllocated`, {
					method: "GET",
					headers: {
						"Content-Type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(err => reject(err));
					})
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 *
	 * @param {Object} params
	 * @param {String} params.appToken
	 */
	makeAllotment(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(
					`${SERVER_BASE_URL_WIFI}allocateVoucher?ref_site_id=${params.siteId}`,
					{
						method: "GET",
						headers: {
							"Content-Type": "application/json",
							"X-ACCESS-TOKEN": params.appToken
						}
					}
				)
					.then(response => {
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(err => reject(err));
					})
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * @param {{ appToken: string; }} params
	 */
	getAllSites(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_SITES + "/getSites", {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}

	/**
	 * @param {{ site_id: string; }} params
	 */
	getAllSsid(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(`${SERVER_BASE_URL_WIFI}/getAllSsid?site_id=${params.site_id}`, {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}
}

export default new FreeWifi();
