import React from "react";
import { Text, Platform, TextInput, TextProps } from "react-native";
import {
	font_one,
	font_two,
	font_three,
	font_four
} from "../../../Assets/styles";

/**
 * @param {TextProps} props
 */
export const FreeWifiTextLight = props => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_one,
				fontWeight: Platform.OS === "ios" ? "300" : undefined,
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

/**
 * @param {TextProps} props
 */
export const FreeWifiTextRegular = props => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_two,
				fontWeight: Platform.OS === "ios" ? "400" : undefined,
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

/**
 * @param {TextProps} props
 */
export const FreeWifiTextMedium = props => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_three,
				fontWeight: Platform.OS === "ios" ? "500" : undefined,
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

/**
 * @param {TextProps} props
 */
export const FreeWifiTextBold = props => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_four,
				fontWeight: Platform.OS === "ios" ? "700" : undefined,
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);
