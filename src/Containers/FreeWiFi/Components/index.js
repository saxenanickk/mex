import {
	FreeWifiTextBold,
	FreeWifiTextLight,
	FreeWifiTextMedium,
	FreeWifiTextRegular
} from "./FreeWifiText";

/**
 * Export Components
 */
export {
	FreeWifiTextBold,
	FreeWifiTextLight,
	FreeWifiTextMedium,
	FreeWifiTextRegular
};
