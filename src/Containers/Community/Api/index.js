import { Platform } from "react-native";
import Config from "react-native-config";
/**
 * @typedef {Object} GetParams
 * @property {string} appToken
 */

const {
	SERVER_BASE_URL_COMMUNITY,
	SERVER_BASE_URL_CONNECT,
	SERVER_BASE_URL_AUTH,
	SERVER_BASE_URL_SITES
} = Config;

const updateProfileParams = [
	"image",
	"gender",
	"email",
	"current_designation",
	"current_organisation",
	"education",
	"site_id",
	"past_organisation",
	"home_town",
	"date_of_birth",
	"hobbies_interests",
	"skills",
	"album",
	"name",
	"about",
	"linkedin",
	"facebook"
];
const createFormData = photo => {
	const data = new FormData();

	data.append("image", {
		name: photo.fileName,
		type: photo.type,
		uri:
			Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
	});
	data.append("dimensions", photo.width + "x" + photo.height);
	return data;
};
const createVideoFormData = video => {
	const data = new FormData();

	data.append("video", {
		name: "video",
		type: Platform.OS === "android" ? "video/mp4" : "video/mov",
		uri:
			Platform.OS === "android" ? video.uri : video.uri.replace("file://", "")
	});
	return data;
};
class CommunityApi {
	constructor() {
		console.log("CommunityApi Instantiated.");
	}

	getActivityLog(params) {
		return new Promise((resolve, reject) => {
			try {
				let url = `${SERVER_BASE_URL_COMMUNITY}getUserActivityList/?fetchSize=20${
					params.offset ? `&offset=${params.offset}` : ""
				}`;
				fetch(url, {
					method: "GET",
					headers: {
						"Content-Type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				}).then(response => {
					response
						.json()
						.then(res => {
							resolve(res);
						})
						.catch(err => reject(err));
				});
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * Fetch Clubs Api
	 * @param {GetParams} params
	 */
	getClubs(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + "getClubs", {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	/**
	 * join or leave a club
	 */
	userModifyClubMemberShip(params) {
		console.log("params are", params);
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + params.url, {
					method: "POST",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					},
					body: JSON.stringify({
						id: params.clubId
					})
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}
	/**
	 * get detail of a clud with club id
	 */
	getClubDetail(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + "getClub?id=" + params.clubId, {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	/**
	 * create new posts
	 */
	createPost(params) {
		console.log("params are", params);
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + "createPost", {
					method: "POST",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					},
					body: JSON.stringify({
						post_type: params.postType,
						title: params.postTitle,
						image: params.postImage,
						video: params.postVideo,
						description: params.postDescription,
						link: params.postLink,
						parent_id: params.parentId !== null ? params.parentId : null,
						club_id: params.clubId !== null ? params.clubId : null,
						visibility: params.postVisibility,
						links: params.urlPreview !== null ? params.urlPreview : null,
						uniqueIdentifier:
							params.postType === "comment" &&
							params.uniqueIdentifier !== null &&
							params.uniqueIdentifier !== undefined &&
							params.uniqueIdentifier
					})
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	/**
	 * function to access all the posts
	 */
	getPosts(params) {
		let body = {};
		if (params.nextOffSet !== null && params.nextOffSet !== undefined) {
			body.offset = params.nextOffSet;
		}
		if (params.clubId !== null && params.clubId !== undefined) {
			body.club_id = params.clubId;
		}
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + "getPosts", {
					method: "POST",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					},
					body: JSON.stringify(body)
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								var flags = [];
								var output = [];
								if (res && res.data) {
									let l = res.data.length;
									let i = 0;
									for (i = 0; i < l; i++) {
										if (flags[res.data[i].id]) {
											continue;
										}
										flags[res.data[i].id] = true;
										output.push(res.data[i]);
									}
								}
								if (output.length) {
									resolve({
										data: output,
										success: res.success,
										offset: res.offset
									});
								} else {
									resolve(res);
								}
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	/**
	 * function to upload the image
	 */
	uploadImage(params) {
		let body =
			params.photo !== null && params.photo !== undefined
				? createFormData(params.photo)
				: createFormData(params.video);
		console.log("body is", body);
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_AUTH + "upload", {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"content-type": "multipart/form-data",
						"cache-control": "no-cache"
					},
					body: body
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => {
						console.log("error in image upload", error);
						reject(error);
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	uploadVideo(params) {
		let body =
			params.video !== null && params.video !== undefined
				? createVideoFormData(params.video)
				: null;
		console.log("body is", body);
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_AUTH + "upload", {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken
					},
					body: body
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => {
						console.log("error in video upload", error);
						reject(error);
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}
	/**
	 * function to fetch Comments Related To a post
	 */

	getComments(params) {
		let body = {};
		if (params.nextOffSet !== null) {
			body.offset = params.nextOffSet;
		}

		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_COMMUNITY + "getComments?post_id=" + params.postId,
					{
						method: "POST",
						headers: {
							"Content-type": "application/json",
							"X-ACCESS-TOKEN": params.appToken
						},
						body: JSON.stringify(body)
					}
				)
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getPostDetail(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + "getPost?post_id=" + params.postId, {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"content-type": "application/json",
						"cache-control": "no-cache"
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	likeOrUnlikePost(params) {
		const url = params.isLike ? "addLike" : "removeLike";
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + url, {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"content-type": "application/json"
					},
					body: JSON.stringify({
						post_id: params.postId,
						uniqueIdentifier: params.uniqueIdentifier,
						post_type: "post"
					})
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	removePost(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + "removePost", {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"content-type": "application/json"
					},
					body: JSON.stringify({
						post_id: params.postId,
						post_type: params.postType,
						uniqueIdentifier: params.uniqueIdentifier
					})
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	hidePost(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + "hidePost", {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"content-type": "application/json"
					},
					body: JSON.stringify({
						post_id: params.postId,
						uniqueIdentifier: params.uniqueIdentifier
					})
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	savePost(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + "savePost", {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"content-type": "application/json"
					},
					body: JSON.stringify({
						post_id: params.postId,
						uniqueIdentifier: params.uniqueIdentifier
					})
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	reportPost(params) {
		console.log("post to report is", params);
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + "createReportForPost", {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"content-type": "application/json"
					},
					body: JSON.stringify({
						post_id: params.postId,
						uniqueIdentifier: params.uniqueIdentifier
					})
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	followUnfollowUser(params) {
		let body = {};
		let url = "";
		if (params.isFollowing) {
			body.unfollowing_user_id = params.userId;
			url = "unfollowUser";
		} else {
			body.following_user_id = params.userId;
			url = "followUser";
		}
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + url, {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"content-type": "application/json"
					},
					body: JSON.stringify(body)
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	removeComment(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + "removePost", {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"content-type": "application/json"
					},
					body: JSON.stringify({
						post_id: params.postId,
						post_type: params.postType,
						uniqueIdentifier: params.uniqueIdentifier,
						uniqueIdentifier_parent: params.uniqueIdentifierParent
					})
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getLikedMembers(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_COMMUNITY +
						"getLikedMembers?post_id=" +
						params.postId,
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": params.appToken,
							"content-type": "application/json",
							"cache-control": "no-cache"
						}
					}
				)
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	blockUnblockUser(params) {
		let body = {};
		let url = "";
		if (params.blockUserId !== null && params.blockUserId !== undefined) {
			body.block_user_id = params.blockUserId;
			url = "blockUser";
		} else {
			body.unblock_user_id = params.unBlockUserId;
			url = "unblockUser";
		}
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + url, {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"content-type": "application/json"
					},
					body: JSON.stringify(body)
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getUserProfile(params) {
		let url = "getProfile";
		if (params.userId !== null && params.userId !== undefined) {
			url = url + "?user_id=" + params.userId;
		}
		console.log("url is", url);
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + url, {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"content-type": "application/json",
						"cache-control": "no-cache"
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	updateUserProfile(params) {
		let body = {};
		if (params.profile) {
			updateProfileParams.map(item => {
				if (item === "image") {
					if (params.profile[item] && params.profile[item].startsWith("http")) {
						body[item] = params.profile[item];
					}
				} else if (
					item === "home_town" ||
					item === "current_designation" ||
					item === "name" ||
					item === "about"
				) {
					if (params.profile[item] !== undefined) {
						body[item] = params.profile[item]
							? params.profile[item].trim()
							: params.profile[item];
					}
				} else {
					if (params.profile[item] !== undefined) {
						body[item] = params.profile[item];
					}
				}
			});
		} else {
			body = {
				name: params.name,
				email: params.email,
				gender: params.gender,
				image: params.image
			};
		}
		console.log("body is", body);
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + "updateProfile", {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"content-type": "application/json",
						"cache-control": "no-cache"
					},
					body: JSON.stringify(body)
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	uploadProfilePic(params) {
		let body =
			params.photo !== null &&
			params.photo !== undefined &&
			createFormData(params.photo);
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + "uploadProfilePicture", {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"content-type": "multipart/form-data",
						"cache-control": "no-cache"
					},
					body: body
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => {
						console.log("error in profile pic upload", error);
						reject(error);
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	/**
	 * function to get all the follower
	 */
	getAllFollower(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + "getAllFollowers", {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	/**
	 * function to get all the following
	 */
	getAllFollowing(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + "getAllFollowing", {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	/**
	 * function to get all my posts
	 */
	getMyPostsOnly(params) {
		let body = {};
		if (params.nextOffset !== null && params.nextOffset !== undefined) {
			body.offset = params.nextOffset;
		}
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + "getPostsByProfile", {
					method: "POST",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					},
					body: JSON.stringify(body)
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getClubMembers(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_COMMUNITY + "getClubMembers?club_id=" + params.clubId,
					{
						method: "GET",
						headers: {
							"Content-type": "application/json",
							"X-ACCESS-TOKEN": params.appToken
						}
					}
				)
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getUrlPreview(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_AUTH + "getMetaData?url=" + params.url, {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getMatchingConnections(params) {
		let body = {};
		if (params.keyword && params.keyword.trim() !== "") {
			body.keyword = params.keyword;
		}
		if (params.filter && params.filter.length > 0) {
			body.filter = params.filter;
		}
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_CONNECT + "getMatchList", {
					method: "POST",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					},
					body: JSON.stringify(body)
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getAllSkills(params) {
		return new Promise(function(resolve, reject) {
			let url = params.searchQuery
				? "getSkills?name=" + params.searchQuery
				: "getSkills";
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + url, {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getInviteDetails(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_CONNECT +
						"getInviteDetails?invite_id=" +
						params.inviteId,
					{
						method: "GET",
						headers: {
							"Content-type": "application/json",
							"X-ACCESS-TOKEN": params.appToken
						}
					}
				)
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	rejectAndAcceptInvite(params) {
		console.log("params are", params);
		const url = params.isAccept ? "acceptInvite" : "rejectInvite";
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_CONNECT + url, {
					method: "POST",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					},
					body: JSON.stringify({
						invite_id: params.inviteId
					})
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	blockInvite(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_CONNECT + "blockInvite", {
					method: "POST",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					},
					body: JSON.stringify({
						invite_id: params.inviteId,
						invited_by_user_id: params.invitedUserId
					})
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response of skill", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getHobbiesAndInterests(params) {
		return new Promise(function(resolve, reject) {
			let url = params.searchQuery
				? "getHobbiesAndInterests?name=" + params.searchQuery
				: "getHobbiesAndInterests";
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + url, {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response of hobby", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getAllSites(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_SITES + "/getSites", {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response", response);
						response
							.json()
							.then(res => {
								console.log("response of get sites is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("Error:", error);
				reject(error);
			}
		});
	}

	getSocialLobbyOtp(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_CONNECT + "requestOtp", {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken,
						qrhash: params.qrhash
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getEducationInstitute(params) {
		return new Promise(function(resolve, reject) {
			let url = params.searchQuery
				? "getEducations?name=" + params.searchQuery
				: "getEducations";
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + url, {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}
}

export default new CommunityApi();
