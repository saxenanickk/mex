import { all } from "redux-saga/effects";
import { communityClubSaga } from "./Containers/Club/Saga";
import { communityPostSaga } from "./Containers/Activity/Saga";
import { communityPostDetailSaga } from "./Containers/PostDetail/Saga";
import { communityMyProfileSaga } from "./Containers/EditProfile/Saga";
import { communityProfileSaga } from "./Containers/UserProfile/Saga";
import { communityConnectionMatchSaga } from "./Containers/ConnectionsMatch/Saga";
import { communitySocialInviteSaga } from "./Containers/SocialInvite/Saga";
import { communityCreatePostSaga } from "./Containers/CreatePost/Saga";
import { communityActivityLogSaga } from "./Containers/ActivityLog/Saga";
//constant to save error for community app
export const COMMUNITY_SAVE_ERROR = "COMMUNITY_SAVE_ERROR";
export const COMMUNITY_RESET_ERROR = "COMMUNITY_RESET_ERROR";
export const COMMUNITY_SAVE_CURRENT_DISPLAY_SCREEN =
	"COMMUNITY_SAVE_CURRENT_DISPLAY_SCREEN";
export const COMMUNITY_SAVE_DEEPLINK_PARAMS = "COMMUNITY_SAVE_DEEPLINK_PARAMS";
//method to save error for community app
export const communitySaveError = payload => ({
	type: COMMUNITY_SAVE_ERROR,
	payload
});
export const communityResetError = payload => ({
	type: COMMUNITY_RESET_ERROR,
	payload
});
export const communitySaveCurrentDisplayScreen = payload => ({
	type: COMMUNITY_SAVE_CURRENT_DISPLAY_SCREEN,
	payload
});
export const communitySaveDeeplinkParams = payload => ({
	type: COMMUNITY_SAVE_DEEPLINK_PARAMS,
	payload
});
export default function* communitySaga() {
	yield all([
		communityClubSaga(),
		communityPostSaga(),
		communityPostDetailSaga(),
		communityCreatePostSaga(),
		communityMyProfileSaga(),
		communityProfileSaga(),
		communityConnectionMatchSaga(),
		communitySocialInviteSaga(),
		communityActivityLogSaga()
	]);
}
