import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: {
		backgroundColor: "#fff",
		borderBottomColor: "#f2f2f2",
		borderBottomWidth: StyleSheet.hairlineWidth,
		flexDirection: "row",
		elevation: 5,
		alignItems: "center",
		paddingVertical: width / 30
	},
	backButton: {
		marginLeft: width / 30
	},
	header: {
		marginLeft: width / 30
	}
});
