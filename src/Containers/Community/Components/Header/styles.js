import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	header: {
		width: width,
		height: height / 12,
		backgroundColor: "transparent",
		paddingHorizontal: width / 30,
		flexDirection: "row",
		alignItems: "center"
	},
	backButton: {
		flexDirection: "row",
		alignItems: "center",
		width: width / 6,
		height: height / 12
		// justifyContent: "center",
	}
});
