import React from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity, Dimensions } from "react-native";
import { Icon } from "../../../../Components";
import { styles } from "./styles";
import { CommunityTextBold } from "../CommunityText";

const { height } = Dimensions.get("window");

const CommunityHeader = props => (
	<View style={[styles.header, props.style]}>
		<TouchableOpacity style={styles.backButton} onPress={props.onPress}>
			<Icon
				iconType={"feather"}
				iconSize={Dimensions.get("window").width / 18}
				iconName={"arrow-left"}
				iconColor={"#000"}
			/>
			<CommunityTextBold
				style={{ marginLeft: 10 }}
				color={"#000000"}
				size={height / 44}>{`Back`}</CommunityTextBold>
		</TouchableOpacity>
	</View>
);

CommunityHeader.propTypes = {
	onPress: PropTypes.func.isRequired
};

CommunityHeader.defaultProps = {
	//
};

export default CommunityHeader;
