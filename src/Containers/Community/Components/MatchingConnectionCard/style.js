import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff"
	},
	header: {
		flexDirection: "row",
		backgroundColor: "#fff",
		justifyContent: "space-between",
		paddingHorizontal: width / 30,
		width: width,
		height: height / 15,
		alignItems: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	backButton: {
		width: width / 10,
		alignItems: "flex-start",
		justifyContent: "center",
		height: height / 15
	},
	textSection: {
		flexDirection: "row",
		width: width * 0.83,
		height: height / 15,
		justifyContent: "space-between",
		alignItems: "center"
	},
	filterSection: {
		flexDirection: "row",
		width: width / 3,
		height: height / 15,
		justifyContent: "flex-end",
		alignItems: "center"
	},
	rotateIcon: {
		transform: [{ rotate: "90deg" }]
	},
	filterText: {
		marginLeft: width / 50
	},
	matchingList: {
		height: height,
		marginTop: height / 60
	},
	matchingConnectionSection: {
		width: width * 0.94,
		paddingTop: height / 60,
		paddingBottom: height / 40,
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#FFFFFF",
		paddingHorizontal: width / 30,
		borderRadius: width / 60,
		flexDirection: "row",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	listSeparator: {
		width: width,
		height: height / 60
	},
	userImage: {
		width: width / 7,
		height: width / 7,
		borderRadius: width / 14,
		justifyContent: "center",
		alignItems: "center"
	},
	noUserImage: {
		width: width / 6,
		height: width / 6,
		borderRadius: width / 12,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#fff"
	},
	connectionInfoView: {
		width: width * 0.65,
		marginLeft: width / 30
	},
	connectionInfoSection: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		width: width * 0.65
	},
	nameText: {
		width: width * 0.35
	},
	matchingPercentageView: {
		width: width * 0.65,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		marginTop: height / 150
	},
	matchingProgressSection: {
		width: width * 0.65,
		backgroundColor: "#fff",
		height: height / 120,
		marginTop: height / 100,
		borderRadius: width / 30
	},
	matchingProgressMarkSection: {
		width: 0,
		backgroundColor: "#000",
		height: height / 120,
		borderRadius: width / 30
	},
	editImage: {
		width: width / 20,
		height: width / 20
	},
	matchingConnectionFirstSection: {
		width: width,
		alignSelf: "center",
		backgroundColor: "#F2F2F2",
		borderRadius: width / 60,
		paddingBottom: height / 30
	},
	lowerButtonSection: {
		width: width * 0.9,
		alignItems: "flex-end",
		marginTop: height / 60,
		position: "absolute",
		bottom: width / 50,
		right: width / 30
	},
	unfollowButton: {
		width: width / 4.5,
		borderWidth: 2,
		borderColor: "#5BADF0",
		backgroundColor: "#fff",
		paddingVertical: height / 130,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 10,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	followButton: {
		width: width / 4.5,
		backgroundColor: "#fff",
		justifyContent: "center",
		paddingVertical: height / 130,
		borderWidth: 2,
		borderColor: "#5BADF0",
		alignItems: "center",
		borderRadius: width / 10,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	inputField: {
		width: width * 0.92,
		marginTop: height / 90,
		alignSelf: "center",
		justifyContent: "center",
		backgroundColor: "#fff",
		borderRadius: width / 50,
		...Platform.select({
			android: { elevation: 1 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	commentField: {
		width: width * 0.84,
		alignSelf: "center",
		marginTop: height / 50,
		height: height / 15,
		justifyContent: "center",
		paddingHorizontal: width / 30,
		backgroundColor: "#eeeff5",
		borderRadius: width / 50
	},
	rowView: {
		flexDirection: "row",
		alignItems: "center",
		height: height / 15,
		paddingHorizontal: width / 30
	},
	textInput: {
		width: width / 2,
		marginLeft: width / 50
	},
	loaderView: {
		height: height * 0.9,
		justifyContent: "center",
		alignItems: "center"
	},
	companynameText: {
		marginBottom: height / 150
	},
	userInfotext: {
		paddingTop: height / 150
	}
});
