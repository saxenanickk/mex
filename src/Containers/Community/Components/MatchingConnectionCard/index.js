import React, { Component } from "react";
import { View, Dimensions, Image, TouchableOpacity } from "react-native";
import { styles } from "./style";
import {
	CommunityTextMedium,
	CommunityTextRegular,
	CommunityTextBold
} from "../CommunityText";
import { Icon } from "../../../../Components";
import { replace } from "lodash";
import I18n from "../../Assets/Strings/i18n";
const { width, height } = Dimensions.get("window");
export default class MatchingConnectioncard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			searchQuery: "",
			is_Following: props.item.is_Following
		};
	}

	static getDerivedStateFromProps(nextProps, prevState) {
		return nextProps.item.is_Following !== prevState.is_Following
			? { is_Following: nextProps.item.is_Following }
			: {};
	}

	renderUserImage = userProfile => {
		try {
			let image = userProfile.image;
			if (
				image !== null &&
				image !== undefined &&
				image.trim() !== "" &&
				image.startsWith("http")
			) {
				return <Image source={{ uri: image }} style={styles.userImage} />;
			} else {
				throw new Error("User profile pic not found");
			}
		} catch (error) {
			return (
				<View style={styles.noUserImage}>
					<Icon
						iconType={"ionicon"}
						iconSize={width / 12}
						iconName={"ios-person"}
						iconColor={"rgba(0,0,0,0.5)"}
					/>
				</View>
			);
		}
	};

	onFollowUnfollowUser = item => {
		this.setState({ is_Following: !this.state.is_Following });
		this.props.followUnfollowUser(item);
	};

	getTotalMatchingttribute = item => {
		try {
			let total_matched_attribute = 0;
			Object.keys(item.matched).map(
				key =>
					(total_matched_attribute =
						total_matched_attribute + item.matched[key].length)
			);
			return total_matched_attribute;
		} catch (error) {
			return 0;
		}
	};

	renderMatchingAttribute = item => {
		try {
			let string = "";
			let total_matched_attribute = 0;
			Object.keys(item.matched).map(
				key =>
					(total_matched_attribute =
						key !== "site_id" && key !== "clubIds"
							? total_matched_attribute + item.matched[key].length
							: total_matched_attribute)
			);
			let count = 0;
			Object.keys(item.matched).map(key => {
				if (count < 3 && key !== "site_id" && key !== "clubIds") {
					item.matched[key].forEach(element => {
						if (count < 3) {
							element = replace(element, "<em>", "");
							element = replace(element, "</em>", "");
							string = string + element;
							string =
								count < 2 && count < total_matched_attribute - 1
									? string + ", "
									: string;
							count++;
						}
					});
				}
			});
			return count > 0
				? total_matched_attribute > 3
					? `${string}... ${I18n.t("com_helping_verb", {
							count: count
					  })} matching attributes`
					: `${string} ${I18n.t("com_helping_verb", {
							count: count
					  })} matching attribute`
				: "";
		} catch (error) {
			console.log("error in matching", error);
			return "";
		}
	};
	render() {
		const { item } = this.props;
		return (
			<TouchableOpacity
				activeOpacity={0.5}
				onPress={() => this.props.onPressCard(item)}
				style={styles.matchingConnectionFirstSection}>
				<View style={styles.matchingConnectionSection}>
					{this.renderUserImage(item.record)}
					<View style={styles.connectionInfoView}>
						<View style={styles.connectionInfoSection}>
							<CommunityTextMedium
								size={height / 47}
								numberOfLines={1}
								color="#353535"
								style={styles.nameText}>
								{item.record.name.trim()}
							</CommunityTextMedium>
							<CommunityTextBold
								size={height / 50}
								numberOfLines={1}
								color="#0C0C0C">
								{this.getTotalMatchingttribute(item) > 1
									? `${this.getTotalMatchingttribute(item)} matches`
									: `${this.getTotalMatchingttribute(item)} match`}
							</CommunityTextBold>
						</View>
						{item.record.current_organisation && (
							<CommunityTextMedium
								numberOfLines={1}
								size={height / 58}
								style={styles.userInfotext}
								color="#525151">
								{`${item.record.current_organisation}`}
							</CommunityTextMedium>
						)}
						{item.record.site_name && (
							<CommunityTextMedium
								numberOfLines={1}
								size={height / 58}
								style={styles.userInfotext}
								color="#525151">
								{`${item.record.site_name}`}
							</CommunityTextMedium>
						)}
						<CommunityTextRegular
							size={height / 70}
							color="#a6a3a3"
							numberOfLines={1}
							style={[styles.companynameText, styles.userInfotext]}>
							{this.renderMatchingAttribute(item)}
						</CommunityTextRegular>
					</View>
				</View>
				<View style={styles.lowerButtonSection}>
					<TouchableOpacity
						onPress={() => this.onFollowUnfollowUser(item)}
						style={
							this.state.is_Following
								? styles.unfollowButton
								: styles.followButton
						}>
						<CommunityTextBold
							numberOfLines={1}
							size={height / 60}
							color={this.state.is_Following ? "#000" : "#2F2F2F"}>
							{this.state.is_Following ? "Following" : "Follow"}
						</CommunityTextBold>
					</TouchableOpacity>
				</View>
			</TouchableOpacity>
		);
	}
}
