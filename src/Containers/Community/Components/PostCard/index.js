import React, { Component } from "react";
import { View, Dimensions, TouchableOpacity, Keyboard } from "react-native";
import { connect } from "react-redux";
import { styles } from "./style";
import I18n from "../../Assets/Strings/i18n";
import { Icon } from "../../../../Components";
import PostOptions from "../../Containers/PostDetail/PostOptions";
import Hyperlink from "react-native-hyperlink";
import {
	communityCreatePost,
	communityChangeClubPostCommentCountLocal,
	communityChangePostCommentCountLocal,
	communityChangePostLikeLocal,
	communityChangeClubPostLikeLocal
} from "../../Containers/Activity/Saga";
import {
	communityLikeOrUnlikePost,
	communitySaveSinglePostDetail,
	communitySaveSinglePostComment,
	communitySaveCommentOffset
} from "../../Containers/PostDetail/Saga";
import {
	CommunityTextRegular,
	CommunityTextBold,
	CommunityTextMedium
} from "../CommunityText";
import { startsWith } from "lodash";
import FastImage from "react-native-fast-image";
import {
	communityChangeMyPostCommentLocal,
	communityChangeMyPostLikeLocal
} from "../../Containers/EditProfile/Saga";

import PostImage from "../PostImage";
import PostVideo from "../PostVideo";
import { getSince } from "../../../../utils/getSince";
import MiniApp from "../../../../CustomModules/MiniApp";

const { width, height } = Dimensions.get("window");

class PostCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLiked: null,
			isImageView: false,
			postInfo: null,
			commentText: "",
			openModal: false,
			isLoading: false,
			measurements: {}
		};
	}
	openOptions = () => {
		if (this.optionsRef) {
			this.optionsRef.measure((x, y, width, height, pageX, pageY) => {
				this.setState({
					measurements: { x, y, width, height, pageX, pageY },
					openModal: true
				});
			});
		}
	};

	likeUnLike = () => {
		let isLike = null;
		let likeCount = 0;
		const { postDetail, isClubPost, isMyPost } = this.props;
		if (this.state.isLiked === null) {
			likeCount = postDetail.hasLiked
				? postDetail.liked_count - 1
				: postDetail.liked_count + 1;
			this.setState({
				isLiked: !postDetail.hasLiked,
				likeCount
			});
			isLike = !postDetail.hasLiked;
		} else {
			likeCount = this.state.isLiked
				? this.state.likeCount - 1
				: this.state.likeCount + 1;
			this.setState({
				isLiked: !this.state.isLiked,
				likeCount
			});
			isLike = !this.state.isLiked;
		}
		this.props.dispatch(
			communityLikeOrUnlikePost({
				appToken: this.props.appToken,
				postId: this.props.postDetail.id,
				isLike,
				uniqueIdentifier: this.props.postDetail.uniqueIdentifier
			})
		);
		if (isClubPost) {
			this.props.dispatch(
				communityChangeClubPostLikeLocal({
					postId: postDetail.id,
					likeCount,
					isLike
				})
			);
		}
		if (isMyPost) {
			this.props.dispatch(
				communityChangeMyPostLikeLocal({
					postId: postDetail.id,
					likeCount,
					isLike
				})
			);
		}

		this.props.dispatch(
			communityChangePostLikeLocal({
				postId: postDetail.id,
				likeCount,
				isLike
			})
		);
	};

	// Add-comment removed from the homescreen in futere need to add it again
	saveComment = item => {
		Keyboard.dismiss();
		const { postDetail, isClubPost, isMyPost } = this.props;
		let count = postDetail.comment_count;
		if (
			this.state.commentText !== undefined &&
			this.state.commentText.trim() !== ""
		) {
			this.props.dispatch(
				communityCreatePost({
					postType: "comment",
					postDescription: this.state.commentText,
					appToken: this.props.appToken,
					parentId: item.id,
					uniqueIdentifier: item.uniqueIdentifier
				})
			);
			if (isClubPost) {
				this.props.dispatch(
					communityChangeClubPostCommentCountLocal({
						postId: postDetail.id,
						commentCount: count + 1
					})
				);
			} else if (isMyPost) {
				this.props.dispatch(
					communityChangeMyPostCommentLocal({
						postId: postDetail.id,
						commentCount: count + 1
					})
				);
			} else {
				this.props.dispatch(
					communityChangePostCommentCountLocal({
						postId: postDetail.id,
						commentCount: count + 1
					})
				);
			}
			this.setState({ commentText: "" });
		}
	};

	navigateToPostDetail = autoFocus => {
		const { postDetail, isClubPost, isMyPost } = this.props;
		this.setState({ isLiked: null });
		if (
			this.props.savedPostDetail &&
			this.props.savedPostDetail.id !== postDetail.id
		) {
			this.props.dispatch(communitySaveSinglePostDetail(null));
			this.props.dispatch(communitySaveSinglePostComment(null));
		} else {
			this.props.dispatch(
				communitySaveCommentOffset({
					isRefresh: true
				})
			);
		}
		!(this.props.isClubPost && !this.props.isMember) &&
			this.props.navigation.navigate("PostDetail", {
				postId: postDetail.id,
				isClubPost: isClubPost,
				isMyPost: isMyPost,
				autoFocus: autoFocus
			});
	};

	navigateToLikedMember = () => {
		const { postDetail } = this.props;
		if (postDetail.liked_count <= 0) {
			return;
		}
		this.props.navigation.navigate("LikedMember", {
			postId: postDetail.id,
			userId: postDetail.user_id,
			likeCount:
				this.state.isLiked === null
					? postDetail.liked_count
					: this.state.likeCount
		});
	};

	renderUserImage = postDetail => {
		try {
			let image = postDetail.user_info.image;
			if (
				image !== null &&
				image !== undefined &&
				image.trim() !== "" &&
				image.startsWith("http")
			) {
				return (
					<FastImage source={{ uri: image }} style={styles.userInfoImage} />
				);
			} else {
				throw new Error("User profile pic not found");
			}
		} catch (error) {
			return (
				<View style={styles.noUserImage}>
					<Icon
						iconType={"ionicon"}
						iconSize={width / 15}
						iconName={"ios-person"}
						iconColor={"rgba(0,0,0,0.5)"}
					/>
				</View>
			);
		}
	};

	renderPreviewImage = urlPreview => {
		try {
			let image = urlPreview.image;
			if (
				startsWith(image, "https://") === false &&
				startsWith(image, "http://") === false
			) {
				image = urlPreview.url + image;
			}
			return (
				<FastImage
					source={{ uri: image }}
					style={styles.previewImage}
					resizeMode={"cover"}
				/>
			);
		} catch (error) {
			return null;
		}
	};

	navigateToLinkView = url => {
		MiniApp.launchWebApp("Community", url);
	};

	renderUrlPreview = () => {
		try {
			const { postDetail } = this.props;
			let urlPreview =
				postDetail.links !== null && typeof postDetail.links === "string"
					? JSON.parse(postDetail.links)
					: null;
			if (urlPreview !== null) {
				return (
					<TouchableOpacity
						onPress={() => this.navigateToLinkView(urlPreview.url)}
						activeOpacity={0.3}>
						<View style={styles.urlPreviewView}>
							{urlPreview.image ? this.renderPreviewImage(urlPreview) : null}
							<View style={styles.previewTextSection}>
								{urlPreview.title ? (
									<CommunityTextBold
										numberOfLines={1}
										color={"#000"}
										style={
											urlPreview.image
												? styles.previewText
												: styles.fullPreviewText
										}
										size={height / 55}>
										{urlPreview.title}
									</CommunityTextBold>
								) : null}
								{urlPreview.appName ? (
									<CommunityTextRegular
										numberOfLines={1}
										color={"#000"}
										style={
											urlPreview.image
												? styles.previewText
												: styles.fullPreviewText
										}
										size={height / 55}>
										{urlPreview.appName}
									</CommunityTextRegular>
								) : null}
								{urlPreview.description ? (
									<CommunityTextRegular
										numberOfLines={3}
										color={"#000"}
										style={
											urlPreview.image
												? styles.previewText
												: styles.fullPreviewText
										}
										size={height / 55}>
										{urlPreview.description}
									</CommunityTextRegular>
								) : null}
							</View>
						</View>
					</TouchableOpacity>
				);
			} else {
				return null;
			}
		} catch (error) {
			return null;
		}
	};

	shouldComponentUpdate(props, state) {
		if (
			props.currentDisplayScreen &&
			props.currentDisplayScreen !== this.props.currentDisplayScreen &&
			process.currentDisplayScreen !== "Home"
		) {
			this.setState({ videoPaused: true });
		}
		if (this.props.isPostDetail) {
			this.forceUpdate();
		}
		if (
			props.posts &&
			props.posts !== this.props.posts &&
			!this.props.isPostDetail
		) {
			this.setState({
				isLiked: null,
				isImageView: false,
				postInfo: null,
				commentText: "",
				videoPaused: true,
				playStarted: false
			});
		}
		return true;
	}

	openUserProfile = postDetail => {
		try {
			if (!postDetail.isYourPost) {
				const params = postDetail.isYourPost
					? {}
					: {
							userInfo: {
								user_id: postDetail.user_id
							}
					  };
				this.props.navigation.navigate("UserProfile", params);
			}
		} catch (error) {
			console.log("error in navigating to user profile", error);
		}
	};

	dateDifferrence(date) {
		try {
			let currentDate = new Date().getTime();
			let postDate = new Date(date).getTime();
			return getSince(currentDate, postDate);
		} catch (error) {
			console.log("error in navigating to post created at", error);
		}
	}

	render() {
		const {
			postDetail,
			isClubPost,
			isMember,
			isMyPost,
			setScrollOffset,
			pause
		} = this.props;
		const { measurements } = this.state;
		if (this.props.isPostDetail) {
			console.log(this.props, "props");
			console.log(this.state, "state");
		}
		return (
			<TouchableOpacity
				disabled={true ? this.props.currentDisplayScreen !== "Home" : null}
				activeOpacity={1}
				style={styles.mainContainer}
				onPress={() => this.navigateToPostDetail(false)}>
				<View style={styles.userContainer}>
					<View style={styles.userSection}>
						<TouchableOpacity
							style={styles.imageView}
							onPress={() => this.openUserProfile(postDetail)}>
							{this.renderUserImage(postDetail)}
						</TouchableOpacity>
						<View>
							<TouchableOpacity
								onPress={() => this.openUserProfile(postDetail)}>
								<CommunityTextMedium
									numberOfLines={1}
									style={styles.userInfoName}>
									{postDetail.user_info ? postDetail.user_info.name : ""}
								</CommunityTextMedium>
							</TouchableOpacity>
							<CommunityTextRegular style={styles.createdAt} size={height / 65}>
								{this.dateDifferrence(postDetail.created_at)}
							</CommunityTextRegular>
						</View>
					</View>
					<View>
						<TouchableOpacity
							ref={ref => {
								this.optionsRef = ref;
							}}
							style={styles.menuView}
							onPress={() => this.openOptions()}>
							<Icon
								iconType={"simple_line"}
								iconSize={width / 25}
								iconName={"options-vertical"}
								iconColor={"#828282"}
							/>
						</TouchableOpacity>
					</View>
				</View>
				{postDetail.description && postDetail.description.trim() !== "" ? (
					<View style={styles.textView}>
						<Hyperlink
							onPress={(url, text) => this.navigateToLinkView(url)}
							linkStyle={styles.linkColor}>
							<CommunityTextRegular style={styles.discription}>
								{postDetail.description}
							</CommunityTextRegular>
						</Hyperlink>
					</View>
				) : null}
				{this.renderUrlPreview()}
				{postDetail.image ? (
					<PostImage
						postDetail={postDetail}
						isClubPost={isClubPost}
						isMember={isMember}
						isMyPost={isMyPost}
						navigation={this.props.navigation}
					/>
				) : null}
				{postDetail.video ? (
					<PostVideo
						pause={pause}
						setScrollOffset={setScrollOffset}
						video={postDetail.video}
						currentDisplayScreen={this.props.currentDisplayScreen}
						navigation={this.props.navigation}
					/>
				) : null}
				<View style={styles.postLikeContainer}>
					<View style={{ flexDirection: "row", alignItems: "center" }}>
						<TouchableOpacity
							onPress={() => this.likeUnLike()}
							style={styles.actionButton}>
							<Icon
								style={styles.postLikeImage}
								iconType={"font_awesome"}
								iconSize={height / 45}
								iconName={"heart"}
								iconColor={
									this.state.isLiked === null
										? postDetail.hasLiked
											? "#d75654"
											: "#ABABAB"
										: this.state.isLiked
										? "#d75654"
										: "#ABABAB"
								}
							/>
						</TouchableOpacity>
						<TouchableOpacity onPress={this.navigateToLikedMember}>
							<CommunityTextRegular style={styles.likeText}>
								{I18n.t("com_like", {
									count:
										this.state.isLiked === null
											? postDetail.liked_count
											: this.state.likeCount
								})}
							</CommunityTextRegular>
						</TouchableOpacity>
					</View>

					<TouchableOpacity
						activeOpacity={1}
						onPress={() =>
							this.props.isPostDetail ? null : this.navigateToPostDetail(true)
						}
						style={styles.actionButton}>
						<Icon
							style={styles.postLikeImage}
							iconType={"icomoon"}
							iconSize={height / 55}
							iconName={"comments"}
							iconColor={"#ABABAB"}
						/>
						<CommunityTextRegular style={styles.likeText}>
							{I18n.t("com_comment", {
								count: postDetail.comment_count
							})}
						</CommunityTextRegular>
					</TouchableOpacity>
				</View>
				{this.state.openModal ? (
					<PostOptions
						postDetail={postDetail}
						isClubPost={this.props.isClubPost}
						openModal={this.state.openModal}
						setModal={val => this.setState({ openModal: val })}
						setLoading={val => this.setState({ isLoading: val })}
						appToken={this.props.appToken}
						dispatch={this.props.dispatch}
						measurements={measurements}
					/>
				) : null}
			</TouchableOpacity>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		error: state.community && state.community.error,
		myProfile: state.community && state.community.profile.myProfile,
		currentDisplayScreen:
			state.community && state.community.error.currentDisplayScreen,
		posts: state.community && state.community.postReducer.posts,
		savedPostDetail:
			state.community && state.community.postDetailReducer.postDetail
	};
}

export default connect(mapStateToProps)(PostCard);
