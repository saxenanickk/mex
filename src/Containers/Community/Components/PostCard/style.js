import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#eeeff5",
		paddingVertical: height / 60
	},
	inputField: {
		width: width * 0.92,
		alignSelf: "center",
		height: height / 15,
		justifyContent: "center",
		paddingHorizontal: width / 30,
		backgroundColor: "#fff",
		borderRadius: width / 50
	},
	noUserImage: {
		width: width / 10,
		height: width / 10,
		borderRadius: width / 20,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#eeeff5"
	},
	commentField: {
		width: width * 0.84,
		alignSelf: "center",
		marginTop: height / 50,
		height: height / 15,
		justifyContent: "center",
		paddingHorizontal: width / 30,
		backgroundColor: "#eeeff5",
		borderRadius: width / 50
	},
	editImage: {
		width: width / 20,
		height: width / 20
	},
	rowView: {
		flexDirection: "row",
		alignItems: "center"
	},
	textInput: {
		width: width / 2,
		marginLeft: width / 50
	},
	commentTextInput: {
		width: width / 1.5
	},
	postSection: {
		alignItems: "center",
		flex: 1
	},
	postCard: {
		width: width * 0.92,
		paddingVertical: height / 50,
		backgroundColor: "#ffffff",
		borderRadius: width / 30,
		...Platform.select({
			android: { elevation: 1 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	userInfo: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: height / 60,
		alignItems: "center"
	},
	userImageSection: {
		flexDirection: "row",
		width: width / 1.5,
		alignItems: "center"
	},
	userImageView: {
		width: width / 8,
		height: width / 8,
		borderRadius: width / 12,
		//elevation: 5,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#ff0000",
		borderWidth: 1
	},
	imageIcon: {
		width: width / 6.5,
		height: width / 6.5,
		borderRadius: width / 13
	},
	userPostDescription: {
		paddingHorizontal: width / 30,
		paddingVertical: height / 60
	},
	mainImage: {
		width: width * 0.84,
		height: height / 5,
		borderRadius: width / 30,
		alignSelf: "center"
	},
	videoImage: {
		width: width * 0.84,
		height: height / 5,
		alignSelf: "center",
		marginTop: width / 35,
		backgroundColor: "#000"
	},
	mainImageSection: {
		width: width * 0.84,
		height: height / 5,
		borderRadius: width / 30,
		alignSelf: "center",
		elevation: 5,
		backgroundColor: "rgba(0,0,0,0.5)"
	},
	likeSection: {
		width: 0.84 * width,
		height: height / 14,
		alignItems: "center",
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: width / 90,
		alignSelf: "center"
	},
	heartImage: {
		width: width / 18,
		height: width / 18
	},
	likeCommentCount: {
		justifyContent: "space-between",
		flexDirection: "row"
	},
	heartIcon: {
		width: width / 10,
		paddingVertical: height / 90
	},
	saveCommentButton: {
		borderWidth: 0,
		paddingVertical: height / 50,
		paddingHorizontal: width / 30
	},
	userImage: {
		width: width / 6,
		height: width / 6,
		borderRadius: width / 12,
		justifyContent: "center",
		alignItems: "center"
	},
	bottomContent: {
		position: "absolute",
		bottom: 0,
		width: width * 0.84,
		height: height / 30,
		backgroundColor: "#000",
		flexDirection: "row",
		alignItems: "center"
	},
	playButton: {
		height: height / 20,
		justifyContent: "center",
		alignItems: "center",
		paddingHorizontal: width / 30
	},
	urlPreviewView: {
		backgroundColor: "#f4f4f4",
		marginBottom: height / 90,
		paddingVertical: height / 150,
		paddingHorizontal: width / 50,
		flexDirection: "row",
		borderRadius: width / 30,
		width: width * 0.95,
		alignSelf: "center"
	},
	previewImage: {
		width: width / 4,
		height: height / 10,
		marginRight: width / 50
	},
	previewText: {
		width: width * 0.5
	},
	fullPreviewText: {
		width: width * 0.8
	},
	previewTextSection: {
		flex: 1,
		marginLeft: width / 60
	},
	mainContainer: {
		width,
		backgroundColor: "#fff",
		marginBottom: width / 70
	},
	imageView: {
		flexDirection: "row",
		alignItems: "center"
	},
	userInfoImage: {
		width: width / 9,
		height: width / 9,
		borderRadius: width / 18
	},
	userInfoName: {
		fontSize: height / 46,
		marginLeft: width / 80,
		width: width / 1.5
	},
	createdAt: {
		color: "#9da2ae",
		marginLeft: width / 80
	},

	discription: {
		fontSize: height / 50,
		color: "#0f0f0f"
	},
	imageContainer: {
		marginTop: width / 35
	},
	postImage: {
		width: width - width / 10,
		height: width / 1.9,
		borderRadius: 20
	},
	postLike: {
		color: "#a3a9b3",
		padding: width / 35,
		paddingRight: width / 30,
		marginTop: width / 35,
		marginRight: width / 40
	},
	actionButton: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		paddingVertical: height * 0.01,
		paddingHorizontal: height * 0.01
	},
	postComments: {
		color: "#a3a9b3",
		padding: width / 35,
		paddingRight: width / 30,
		marginTop: width / 35,
		marginLeft: width / 40,
		marginRight: width / 40
	},
	postShares: {
		color: "#a3a9b3",
		padding: width / 35,
		paddingRight: width / 30,
		marginTop: width / 35,
		marginLeft: width / 40,
		marginRight: width / 40
	},
	textView: { paddingHorizontal: width / 23, paddingBottom: height / 60 },
	postLikeContainer: {
		flexDirection: "row",
		paddingVertical: height * 0.01,
		paddingHorizontal: width / 35,
		marginBottom: height / 80
	},
	postLikeImage: {
		// marginTop: width / 60
	},
	comments: {
		height: height / 35,
		width: width / 15,
		marginLeft: width / 70
	},
	likeText: {
		paddingRight: width / 17,
		fontSize: height / 55,
		color: "#070707",
		marginLeft: width / 60
	},
	commentUser: {
		width: width / 10
	},
	commentUserImage: {
		width: width / 10,
		height: width / 10,
		borderRadius: width / 10,

		marginRight: width / 40
	},
	commentUserInfo: {
		marginLeft: width / 20,
		marginTop: width / 60
	},
	commentUserName: {
		fontSize: height / 47,
		color: "#000000"
	},
	commentUserDis: {
		color: "#8f95a3",
		marginTop: width / 85,
		paddingRight: width / 10
	},
	commentTime: {
		color: "#8f95a3",
		marginTop: width / 70,
		marginRight: width / 40,
		fontSize: height / 47
	},
	commentLike: {
		color: "#8f95a3",
		marginTop: width / 70,
		marginLeft: width / 10,
		marginRight: width / 40,
		fontSize: height / 47
	},
	commentReply: {
		color: "#8f95a3",
		marginTop: width / 70,
		marginLeft: width / 10,
		fontSize: height / 47
	},
	commentTimeView: {
		flexDirection: "row"
	},
	userContainer: {
		borderWidth: 0,
		paddingHorizontal: width / 25,
		flexDirection: "row",
		justifyContent: "space-between",
		marginBottom: width / 35,
		marginTop: width / 35,
		alignItems: "center",
		flex: 1
	},
	userSection: {
		flexDirection: "row",
		flex: 1
	},
	menuView: {
		justifyContent: "center",
		paddingHorizontal: width / 37,
		paddingVertical: width / 38
	},
	menuImage: {
		width: width / 18,
		height: width / 18,
		color: "#8f8f8f"
	},
	linkColor: {
		color: "#0d66d6"
	}
});
