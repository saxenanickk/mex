import React from "react";
import { TouchableOpacity } from "react-native";
import { StackActions } from "@react-navigation/native";
import FastImage from "react-native-fast-image";
import { styles } from "./style";

function PostImage(props) {
	const { postDetail, isClubPost, isMember, isMyPost, navigation } = props;

	const navigateToImageScreen = () => {
		navigation.dispatch(
			StackActions.push("ImageScreen", {
				postDetail: postDetail,
				isClubPost: isClubPost,
				isMember: isMember,
				isMyPost: isMyPost
			})
		);
	};

	const renderPostImage = () => {
		let { image } = postDetail;
		try {
			if (image.startsWith("http")) {
				if (
					image.startsWith("http://res.cloudinary.com/") ||
					image.startsWith("https://res.cloudinary.com/")
				) {
					let imageArray = image.split("/upload/");
					image = `${imageArray[0]}/upload/q_30/${imageArray[1]}`;
				}
				return (
					<TouchableOpacity
						style={styles.imageContainer}
						onPress={() => navigateToImageScreen()}>
						<FastImage
							resizeMode="contain"
							style={styles.postImage}
							source={{
								uri: image
							}}
						/>
					</TouchableOpacity>
				);
			}
			return null;
		} catch (error) {
			console.log("error is", error);
			return null;
		}
	};

	return renderPostImage();
}
export default PostImage;
