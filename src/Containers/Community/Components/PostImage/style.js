import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	imageContainer: {
		aspectRatio: 1,
		backgroundColor: "rgba(0,0,0,0.02)"
	},
	postImage: StyleSheet.absoluteFill
});
