import React from "react";
import { StyleSheet, Dimensions, Platform } from "react-native";
const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	clubCard: {
		marginBottom: height * 0.02,
		marginHorizontal: width * 0.03
	},
	clubImageContainer: {
		width: width / 2.1,
		height: width / 1.6,
		justifyContent: "space-between"
	},
	clubImage: {
		borderRadius: width * 0.05
	},
	clubsScrollView: {
		backgroundColor: "#fff",
		flexDirection: "row",
		flexWrap: "wrap"
	},
	clubSection: {
		width: width / 2,
		height: height / 3.76,
		justifyContent: "center",
		alignItems: "center",
		borderBottomWidth: 1,
		borderBottomColor: "#f2f2f2"
	},
	leftBorder: {
		borderLeftWidth: 1,
		borderLeftColor: "#f2f2f2"
	},
	clubNameGradient: {
		paddingBottom: height / 60,
		paddingTop: height / 10,
		paddingHorizontal: width * 0.03,
		borderBottomLeftRadius: width * 0.05,
		borderBottomRightRadius: width * 0.05
	},
	clubNameText: {
		color: "#fff"
	},
	clubButton: (status, CLUB_MEMBER) => ({
		width: width / 5,
		height: height / 25,
		backgroundColor: status === CLUB_MEMBER ? "#EAF4FC" : "#2C98F0", //"#e8e8e8"
		// borderWidth: 0.5,
		// borderColor: "#c3c3c3",
		borderRadius: width / 10,
		marginTop: height / 40,
		marginRight: width * 0.03,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "flex-end"
	})
});

/**
 * ...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3,
			},
		}),
 */
