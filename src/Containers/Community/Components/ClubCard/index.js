import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	Alert,
	ImageBackground
} from "react-native";
import { connect } from "react-redux";
import { styles } from "./style";
import I18n from "../../Assets/Strings/i18n";
import { CommunityTextMedium } from "../../Components/CommunityText";
import _ from "lodash";
import { communityModifyClubMemberShip } from "../../Containers/Club/Saga";
import { NOIMAGE } from "../../../../Assets/Img/Image";
import {
	CLUB_MEMBER,
	NOT_CLUB_MEMBER,
	PENDING_MEMBER_STATUS
} from "../../Constants";
import LinearGradient from "react-native-linear-gradient";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";

const { height } = Dimensions.get("window");

class ClubCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isMember: props.club.isMember,
			status: props.club.Status,
			followCount: props.club.followers
		};
		this.commentIndex = null;
	}

	shouldComponentUpdate(props, state) {
		if (props.club && props.club !== this.props.club) {
			this.setState({
				status: props.club.Status,
				followCount: props.club.followers
			});
		}
		return true;
	}
	modifySelectedClub = club => {
		try {
			let obj = {
				appToken: this.props.appToken,
				clubId: club.id
			};
			let club_name = club.name;
			if (this.state.status === CLUB_MEMBER) {
				Alert.alert(
					"Leave Group",
					"Are you sure you want to leave the group?",
					[
						{
							text: "Cancel",
							onPress: () => {
								console.log("cancel pressed");
							},
							style: "cancel"
						},
						{
							text: "OK",
							onPress: () => {
								obj.url = "leaveClub";
								if (club_name) {
									GoAppAnalytics.trackWithProperties("Left Group", {
										group_name: club_name
									});
								}
								this.setState({
									status:
										this.state.status === NOT_CLUB_MEMBER
											? PENDING_MEMBER_STATUS
											: NOT_CLUB_MEMBER
								});
								this.props.dispatch(communityModifyClubMemberShip(obj));
							}
						}
					],
					{ cancelable: false }
				);
			} else if (this.state.status === NOT_CLUB_MEMBER) {
				obj.url = "joinClub";
				if (club_name) {
					GoAppAnalytics.trackWithProperties("Joined Group", {
						group_name: club_name
					});
				}
				this.setState({
					status:
						this.state.status === NOT_CLUB_MEMBER
							? PENDING_MEMBER_STATUS
							: NOT_CLUB_MEMBER
				});
				this.props.dispatch(communityModifyClubMemberShip(obj));
			}
		} catch (error) {}
	};
	render() {
		const { club } = this.props;
		return (
			<View style={styles.clubCard}>
				<ImageBackground
					defaultSource={NOIMAGE}
					source={
						club.icon !== null &&
						club.icon !== undefined &&
						club.icon.trim() !== ""
							? { uri: club.icon }
							: NOIMAGE
					}
					style={styles.clubImageContainer}
					imageStyle={styles.clubImage}
					resizeMode={"cover"}
					resizeMethod={"auto"}>
					<TouchableOpacity
						style={styles.clubButton(this.state.status, CLUB_MEMBER)}
						onPress={() =>
							this.state.status !== PENDING_MEMBER_STATUS
								? this.modifySelectedClub(club)
								: Alert.alert(
										I18n.t("error"),
										"Your membership status is pending"
								  )
						}>
						<CommunityTextMedium
							size={height / 55}
							color={this.state.status === CLUB_MEMBER ? "#2C98F0" : "#fff"}>
							{this.state.status === CLUB_MEMBER
								? "Leave"
								: this.state.status === PENDING_MEMBER_STATUS
								? "Pending"
								: "Join"}
						</CommunityTextMedium>
					</TouchableOpacity>
					<LinearGradient
						start={{ x: 0, y: 0 }}
						end={{ x: 0, y: 1 }}
						colors={["rgba(0, 0, 0 , 0)", "rgba(0, 0 , 0, 1)"]}
						style={styles.clubNameGradient}>
						<CommunityTextMedium
							size={height / 45}
							color={"#000"}
							style={styles.clubNameText}>
							{_.capitalize(club.name) + " " + "Group"}
						</CommunityTextMedium>
					</LinearGradient>
				</ImageBackground>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		error: state.community && state.community.error
	};
}

export default connect(mapStateToProps)(ClubCard);
