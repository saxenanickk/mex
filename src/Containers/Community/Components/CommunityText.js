import React from "react";
import { Text, Platform, TextInput, TextProps } from "react-native";
import {
	font_one,
	font_two,
	font_three,
	font_four
} from "../../../Assets/styles";

/**
 * @param {TextProps} props
 */
export const CommunityTextLight = props => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_one,
				fontWeight: Platform.OS === "ios" ? "300" : undefined,
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

/**
 * @param {TextProps} props
 */
export const CommunityTextRegular = props => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_two,
				fontWeight: Platform.OS === "ios" ? "400" : undefined,
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

/**
 * @param {TextProps} props
 */
export const CommunityTextMedium = props => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_three,
				fontWeight: Platform.OS === "ios" ? "500" : undefined,
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

/**
 * @param {TextProps} props
 */
export const CommunityTextBold = props => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_four,
				fontWeight: Platform.OS === "ios" ? "700" : undefined,
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

export const CommunityTextInputRegular = ({
	size,
	color,
	style,
	placeholder,
	value,
	onChangeText,
	multiline,
	numberOfLines,
	ellipsizeMode,
	editable,
	...restProps
}) => (
	<TextInput
		{...restProps}
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_two,
				fontWeight: Platform.OS === "ios" ? "400" : undefined,
				fontSize: size,
				color: color,
				margin: 0,
				padding: 0
			},
			style
		]}
		underlineColorAndroid={"transparent"}
		placeholder={placeholder}
		value={value}
		onChangeText={onChangeText}
		multiline={multiline}
		numberOfLines={numberOfLines}
		ellipsizeMode={ellipsizeMode}
		editable={editable}
	/>
);

export const CommunityTextInputMedium = ({
	size,
	color,
	style,
	placeholder,
	value,
	onChangeText,
	multiline,
	numberOfLines,
	ellipsizeMode,
	editable,
	...restProps
}) => (
	<TextInput
		{...restProps}
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_three,
				fontWeight: Platform.OS === "ios" ? "500" : undefined,
				fontSize: size,
				color: color,
				margin: 0,
				padding: 0
			},
			style
		]}
		underlineColorAndroid={"transparent"}
		placeholder={placeholder}
		value={value}
		onChangeText={onChangeText}
		multiline={multiline}
		numberOfLines={numberOfLines}
		ellipsizeMode={ellipsizeMode}
		editable={editable}
	/>
);

export const CommunityTextInputBold = ({
	size,
	color,
	style,
	placeholder,
	value,
	onChangeText,
	multiline,
	numberOfLines,
	ellipsizeMode,
	editable,
	...restProps
}) => (
	<TextInput
		{...restProps}
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_four,
				fontWeight: Platform.OS === "ios" ? "700" : undefined,
				fontSize: size,
				color: color,
				margin: 0,
				padding: 0
			},
			style
		]}
		underlineColorAndroid={"transparent"}
		placeholder={placeholder}
		value={value}
		onChangeText={onChangeText}
		multiline={multiline}
		numberOfLines={numberOfLines}
		ellipsizeMode={ellipsizeMode}
		editable={editable}
	/>
);

const defaultProps = {
	size: null,
	color: "#000",
	editable: true
};

CommunityTextBold.defaultProps = defaultProps;
CommunityTextLight.defaultProps = defaultProps;
CommunityTextMedium.defaultProps = defaultProps;
CommunityTextRegular.defaultProps = defaultProps;
