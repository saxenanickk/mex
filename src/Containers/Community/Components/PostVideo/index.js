import React, { Component } from "react";
import { TouchableOpacity, View, Dimensions } from "react-native";
import { styles } from "./style";
import Video from "react-native-video";
import { Icon } from "../../../../Components";
import FastImage from "react-native-fast-image";

const { height } = Dimensions.get("window");

class PostVideo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			videoPaused: true,
			playStarted: false
		};
	}

	static getDerivedStateFromProps(nextProps, prevState) {
		if (nextProps.pause && !prevState.videoPaused) {
			return {
				videoPaused: !prevState.videoPaused
			};
		}
		return null;
	}

	toggleVideoPlay = () => {
		this.props.setScrollOffset(() =>
			this.setState({ videoPaused: !this.state.videoPaused })
		);
	};

	navigateToVideoPlayer = () => {
		const { video } = this.props;

		this.props.navigation.navigate("VideoPlayer", { videoUri: video });
	};

	getVideoname = () => {
		const { video } = this.props;
		try {
			if (video !== null && video !== undefined && video.trim() !== "") {
				let videoThumbnail = video.split("/");
				let imageUrl = "";
				videoThumbnail.map((element, index) => {
					if (index !== videoThumbnail.length - 1) {
						imageUrl = imageUrl + element + "/";
					} else {
						let videoName = element.split(".")[0];
						imageUrl = imageUrl + videoName + ".jpg";
					}
				});
				return imageUrl;
			} else {
				throw new Error("Null video found");
			}
		} catch (error) {
			return null;
		}
	};

	renderVideoThumbnail = () => {
		const { video } = this.props;
		try {
			if (video !== null && video !== undefined && video.trim() !== "") {
				let imageUrl = this.getVideoname();
				return (
					<View style={styles.imageContainer}>
						<FastImage
							source={{ uri: imageUrl }}
							style={styles.mainImage}
							resizeMode={"contain"}
						/>
					</View>
				);
			}
		} catch (error) {
			return null;
		}
	};

	renderPostVideo = () => {
		const { video } = this.props;
		try {
			return (
				<TouchableOpacity
					onPress={() => this.navigateToVideoPlayer()}
					activeOpacity={1}
					style={styles.videoImage}>
					{(this.props.currentDisplayScreen === "Home" ||
						this.props.currentDisplayScreen === "ClubDetail" ||
						this.props.currentDisplayScreen === "UserProfile") &&
					(!this.state.videoPaused || this.state.playStarted) ? (
						<Video
							source={{
								uri: video
							}}
							ref={ref => {
								this.player = ref;
							}}
							style={styles.video}
							controls={false}
							paused={this.state.videoPaused}
							progressUpdateInterval={200}
							onEnd={() => {
								this.setState({ videoPaused: true, playStarted: false });
							}}
							onLoad={response => {
								this.setState({ playStarted: true });
							}}
							resizeMode={"contain"}
						/>
					) : (
						this.renderVideoThumbnail()
					)}
					<View style={styles.bottomContent}>
						<TouchableOpacity
							style={styles.playButton}
							onPress={this.toggleVideoPlay}>
							<Icon
								iconType={"ionicon"}
								iconSize={height / 30}
								iconName={this.state.videoPaused ? "ios-play" : "ios-pause"}
								iconColor={"#fff"}
							/>
						</TouchableOpacity>
					</View>
				</TouchableOpacity>
			);
		} catch (error) {
			console.error("error is", error);
			return null;
		}
	};
	render() {
		return this.renderPostVideo();
	}
}
export default PostVideo;
