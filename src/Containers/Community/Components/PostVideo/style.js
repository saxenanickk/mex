import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	videoImage: {
		flex: 1,
		backgroundColor: "#000",
		aspectRatio: 1
	},
	video: StyleSheet.absoluteFill,
	bottomContent: {
		position: "absolute",
		bottom: height / 80,
		left: height / 80,
		height: height / 30,
		flexDirection: "row",
		alignItems: "center"
	},
	playButton: {
		height: height / 20,
		justifyContent: "center",
		alignItems: "center",
		paddingHorizontal: width / 25
	},
	imageContainer: {
		aspectRatio: 1,
		backgroundColor: "rgba(0,0,0,0.02)"
	},
	mainImage: StyleSheet.absoluteFill
});
