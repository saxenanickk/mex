import React, { Component } from "react";
import { Dimensions, StyleSheet } from "react-native";
const { width } = Dimensions.get("window");

import LottieView from "lottie-react-native";

class CommunityLoader extends Component {
	render() {
		return (
			<LottieView
				source={require("../../Assets/Lotte/community_loader.json")}
				style={[styles.container, this.props.containerStyle]}
				autoPlay={true}
				loop={true}
			/>
		);
	}
}

export default CommunityLoader;

const styles = StyleSheet.create({
	container: {
		height: width / 10,
		width: width / 10
	}
});
