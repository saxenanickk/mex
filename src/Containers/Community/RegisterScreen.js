import React from "react";
import { Easing, Animated } from "react-native";
import { Stack } from "../../utils/Navigators";

import Splash from "./Containers/Splash";
import Home from "./Containers/Home";
import ClubDetail from "./Containers/ClubDetail";
import CreatePost from "./Containers/CreatePost";
import PostDetail from "./Containers/PostDetail";
import ImageScreen from "./Containers/ImageScreen";
import LikedMember from "./Containers/LikedMember";
import UserProfile from "./Containers/UserProfile";
import EditProfile from "./Containers/EditProfile";
import UserListView from "./Containers/UserListView";
import ConnectionsMatch from "./Containers/ConnectionsMatch";
import Welcome from "./Containers/Connect/Welcome";
import Auth from "./Containers/Connect/Auth";
import SocialInvite from "./Containers/SocialInvite";
import VideoPlayer from "./Containers/VideoPlayer";
import LinkView from "./Containers/LinkView";
import ActivityLog from "./Containers/ActivityLog";

const transitionConfig = () => {
	return {
		transitionSpec: {
			duration: 500,
			easing: Easing.out(Easing.poly(4)),
			timing: Animated.timing,
			useNativeDriver: true
		},
		screenInterpolator: sceneProps => {
			const { layout, position, scene, index, scenes } = sceneProps;
			const toIndex = index;
			const thisSceneIndex = scene.index;
			const width = layout.initWidth;
			const height = layout.initHeight;

			const translateX = position.interpolate({
				inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
				outputRange: [width, 0, 0]
			});

			const translateY = position.interpolate({
				inputRange: [0, thisSceneIndex],
				outputRange: [height, 0]
			});

			const slideFromRight = { transform: [{ translateX }] };
			const slideFromBottom = { transform: [{ translateY }] };

			const lastSceneIndex = scenes[scenes.length - 1].index;

			// Test whether we're skipping back more than one screen
			if (lastSceneIndex - toIndex > 1) {
				// Do not transoform the screen being navigated to
				if (scene.index === toIndex) return;
				// Hide all screens in between
				if (scene.index !== lastSceneIndex) return { opacity: 0 };
				// Slide top screen down
				return slideFromBottom;
			}

			return slideFromRight;
		}
	};
};

const Navigator = () => (
	<Stack.Navigator headerMode={"none"}>
		<Stack.Screen name="Splash" component={Splash} />
		<Stack.Screen name="Home" component={Home} />
		<Stack.Screen name="ClubDetail" component={ClubDetail} />
		<Stack.Screen name="CreatePost" component={CreatePost} />
		<Stack.Screen name="PostDetail" component={PostDetail} />
		<Stack.Screen name="ImageScreen" component={ImageScreen} />
		<Stack.Screen name="LikedMember" component={LikedMember} />
		<Stack.Screen name="UserProfile" component={UserProfile} />
		{/* <Stack.Screen name="EditProfile" component={EditProfile} /> */}
		<Stack.Screen name="UserListView" component={UserListView} />
		<Stack.Screen name="ConnectionsMatch" component={ConnectionsMatch} />
		<Stack.Screen name="ConnectWelcome" component={Welcome} />
		<Stack.Screen name="ConnectAuth" component={Auth} />
		<Stack.Screen name="SocialInvite" component={SocialInvite} />
		<Stack.Screen name="VideoPlayer" component={VideoPlayer} />
		<Stack.Screen name="LinkView" component={LinkView} />
		<Stack.Screen name="ActivityLog" component={ActivityLog} />
	</Stack.Navigator>
);

export default Navigator;
