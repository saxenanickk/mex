import * as I18n from "../../../../Assets/strings/i18n";

export default {
	/**
	 * Ignite App Strings
	 */
	...I18n.default.translations.hi,
	com_follower: {
		one: "अनुयायी",
		other: "अनुयायी",
		zero: "अनुयायी"
	},
	com_member: {
		one: "सदस्य",
		other: "सदस्य",
		zero: "सदस्य"
	},
	com_like: {
		one: "{{count}} पसंद",
		other: "{{count}} पसंद",
		zero: "{{count}} पसंद"
	},
	com_comment: {
		one: "{{count}} टिप्पणी",
		other: "{{count}} टिप्पणी",
		zero: "{{count}} टिप्पणी"
	},
	com_helping_verb: {
		one: "is",
		other: "are"
	},
	com_write_something: "कुछ लिखो...",
	com_read_more: "और पढो",
	com_leave_club: "छोड़ना",
	com_pending_club: "अपूर्ण",
	com_join_club: "शामिल हों",
	com_something_wrong: "कुछ गलत हो गया",
	com_sure_to_leave_club_header: "Leave Group",
	com_sure_to_leave_club: "क्या आप क्लब छोड़ना सुनिश्चित करते हैं",
	com_select_image: "छवि चुने",
	com_take_photo: "फोटो लो",
	com_choose_from_lib: "पुस्तकालय से चुनें",
	com_empty_post_error: "खाली पोस्ट अपलोड नहीं कर सकते",
	com_create_post: "पोस्ट बनाएँ",
	com_cancel: "रद्द करना",
	com_ok: "ठीक",
	com_post: "पद",
	com_image: "छवि",
	com_username: "नाम",
	com_male: "पुरुष",
	com_female: "महिला",
	com_update_profile: "प्रोफ़ाइल अपडेट करें",
	com_following: "अनुगामी",
	com_sure_to_delete_post: "क्या आप पोस्ट हटाना सुनिश्चित कर रहे हैं",
	com_sure_to_report_post_header: "Report the Post",
	com_sure_to_report_post: "क्या आप पोस्ट की रिपोर्ट करना सुनिश्चित करते हैं",
	com_sure_to_delete_comment: "क्या आप टिप्पणी हटाना सुनिश्चित कर रहे हैं",
	com_write_comment: "टिप्पणी लिखें...",
	com_report_this_post: "इस पोस्ट को रिपोर्ट करे",
	com_report_post_desc:
		"यदि आपको कोई अनुचित सामग्री मिलती है तो आप इस पोस्ट की रिपोर्ट कर सकते हैं",
	com_hide_this_post: "पोस्ट छिपाएं",
	com_see_few_post: "इस तरह कम पोस्ट देखें",
	com_unfollow: "अनफॉलो",
	com_follow: "फॉलो",
	com_stop_see_post: "इस व्यक्ति के पोस्ट देखना बंद करें",
	com_start_see_post: "इस व्यक्ति के पोस्ट देखने शुरू करें",
	com_delete_post: "पोस्ट को हटाएं",
	com_delete_post_from_wall: "अपनी पोस्ट हटाएं",
	com_no_post_found: "कोई पोस्ट नहीं मिली",
	com_sorry: "माफ़ कीजिये",
	com_following_no_one: "आप किसी को फॉलो नहीं कर रहे।",
	com_no_followers: "आपका कोई अनुयायी नहीं है।",
	com_no_club_members: "कोई क्लब सदस्य नहीं मिला।",
	com_club_member: "क्लब सदस्य",
	com_video_length_out_of_limit_header: "Video size not supported",
	com_video_length_out_of_limit:
		"Upload a video of length between 3 to 60 Seconds."
};
