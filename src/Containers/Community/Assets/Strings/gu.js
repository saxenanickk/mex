import * as I18n from "../../../../Assets/strings/i18n";

export default {
	/**
	 * Ignite App Strings
	 */
	...I18n.default.translations.gu,
	com_follower: {
		one: "અનુયાયી",
		other: "અનુયાયી",
		zero: "અનુયાયી"
	},
	com_member: {
		one: "સભ્ય",
		other: "સભ્ય",
		zero: "સભ્ય"
	},
	com_like: {
		one: "{{count}} જેમ",
		other: "{{count}} જેમ",
		zero: "{{count}} જેમ"
	},
	com_comment: {
		one: "{{count}} ટિપ્પણી",
		other: "{{count}} ટિપ્પણી",
		zero: "{{count}} ટિપ્પણી"
	},
	com_helping_verb: {
		one: "is",
		other: "are"
	},
	com_write_something: "કંઇક લખો...",
	com_read_more: "વધુ વાંચો",
	com_leave_club: "છોડો",
	com_pending_club: "બાકી",
	com_join_club: "જોડાઓ",
	com_something_wrong: "કંઈક ખોટું થયું",
	com_sure_to_leave_club_header: "Leave Group",
	com_sure_to_leave_club: "શું તમે ક્લબ છોડી જઇ રહ્યા છો?",
	com_select_image: "છબી પસંદ કરો",
	com_take_photo: "ફોટો પાડ",
	com_choose_from_lib: "લાઇબ્રેરીમાંથી પસંદ કરો",
	com_empty_post_error: "ખાલી પોસ્ટ અપલોડ કરી શકતા નથી",
	com_create_post: "પોસ્ટ બનાવો",
	com_cancel: "રદ કરો",
	com_ok: "ઠીક છે",
	com_post: "પોસ્ટ",
	com_image: "છબી",
	com_username: "નામ",
	com_male: "પુરુષ",
	com_female: "સ્ત્રી",
	com_update_profile: "પ્રોફાઇલ અપડેટ કરો",
	com_following: "અનુસરણ",
	com_club_member: "ક્લબ સભ્ય",
	com_sure_to_delete_post: "શું તમે પોસ્ટ કાઢી નાખવા માંગો છો?",
	com_sure_to_report_post_header: "Report the Post",
	com_sure_to_report_post: "શું તમે પોસ્ટની જાણ કરો છો?",
	com_sure_to_delete_comment: "શું તમે ટિપ્પણીને કાઢી નાખવા માંગો છો?",
	com_write_comment: "ટિપ્પણી લખો ...",
	com_report_this_post: "આ પોસ્ટની જાણ કરો",
	com_report_post_desc:
		"જો તમને કોઈ અનુચિત સામગ્રી મળે તો તમે આ પોસ્ટની જાણ કરી શકો છો",
	com_hide_this_post: "પોસ્ટ છુપાવો",
	com_see_few_post: "આના જેવા ઓછા પોસ્ટ્સ જુઓ",
	com_unfollow: "અનુસરવાનું",
	com_follow: "અનુસરો",
	com_stop_see_post: "આ વ્યક્તિની પોસ્ટ્સ જોવાનું રોકો",
	com_start_see_post: "આ વ્યક્તિની પોસ્ટ્સ જોવાનું શરૂ કરો",
	com_delete_post: "પોસ્ટ કાઢી નાખો",
	com_delete_post_from_wall: "તમારી પ્રવૃત્તિ દિવાલમાંથી પોસ્ટ કાઢી નાખો",
	com_no_post_found: "કોઈ પોસ્ટ મળી નથી",
	com_sorry: "માફ કરશો",
	com_following_no_one: "તમે કોઈનું અનુસરતા નથી.",
	com_no_followers: "તમારી પાસે કોઈ અનુયાયીઓ નથી.",
	com_no_club_members: "કોઈ ક્લબ સભ્યો મળ્યાં નથી.",
	com_video_length_out_of_limit_header: "Video size not supported",
	com_video_length_out_of_limit:
		"Upload a video of length between 3 to 60 Seconds."
};
