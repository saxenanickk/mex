import * as I18n from "../../../../Assets/strings/i18n";

export default {
	...I18n.default.translations.en,
	com_follower: {
		one: "Follower",
		other: "Followers",
		zero: "Follower"
	},
	com_member: {
		one: "Member",
		other: "Members",
		zero: "Member"
	},
	com_like: {
		one: "{{count}} like",
		other: "{{count}} likes",
		zero: "{{count}} like"
	},
	com_comment: {
		one: "{{count}} comment",
		other: "{{count}} comments",
		zero: "{{count}} comment"
	},
	com_helping_verb: {
		one: "is",
		other: "are"
	},
	com_posts: {
		one: "Post",
		other: "Posts",
		zero: "Post"
	},

	com_write_something: "What's on your mind?",
	comments_write: "What's on your mind?",
	com_read_more: "Read More",
	com_leave_club: "Leave",
	com_pending_club: "Pending",
	com_join_club: "Join",
	com_something_wrong: "Something went wrong",
	com_sure_to_leave_club_header: "Leave Group",
	com_sure_to_leave_club: "Are you sure you want to leave the group?",
	com_select_image: "Select Image",
	com_take_photo: "Take Photo",
	com_choose_from_lib: "Choose From Library",
	com_empty_post_error: "can not upload empty post",
	com_create_post: "New Post",
	com_cancel: "Cancel",
	com_ok: "Ok",
	com_post: "Post",
	com_image: "Image",
	com_username: "Name",
	com_male: "Male",
	com_female: "Female",
	com_update_profile: "Update Profile",
	com_following: "Following",
	com_club_member: "Group Member",
	com_group_member: "Group Member",
	com_sure_to_delete_post: "Are you sure to delete the post",
	com_sure_to_report_post_header: "Report the Post",
	com_sure_to_report_post: "Are you sure you want to report this post?",
	com_sure_to_delete_comment: "Are you sure to delete  the comment",
	com_write_comment: "Write a comment...",
	com_report_this_post: "Report this post",
	com_report_post_desc:
		"You can report this post if you find any inappropriate content",
	com_hide_this_post: "Hide post",
	com_see_few_post: "See fewer posts like this",
	com_unfollow: "Unfollow",
	com_follow: "Follow",
	com_stop_see_post: "Stop seeing posts from this person",
	com_start_see_post: "Start seeing posts from this person",
	com_delete_post: "Delete post",
	com_delete_post_from_wall: "Delete post from your activity wall",
	com_no_post_found: "No post found",
	com_sorry: "Sorry",
	com_following_no_one: "You are not following any one.",
	com_no_followers: "You have no followers.",
	com_no_club_members: "No Group Members found.",
	com_video_length_out_of_limit_header: "Video size not supported",
	com_video_length_out_of_limit:
		"Upload a video of length between 3 to 60 Seconds.",
	com_find_connections: "Find connections",
	com_education: "education",
	com_Education: "Education",
	com_professional_skills: "Professional Skills",
	com_hobbies_and_interests: "Hobbies & Interests",
	com_past_organisations: "Past Organisations",
	com_about: "About",
	com_home_town: "Home Town",
	com_group_joined: "Groups Joined"
};
