import * as I18n from "../../../../Assets/strings/i18n";

export default {
	/**
	 * Ignite App Strings
	 */
	...I18n.default.translations.ja,
	com_follower: {
		one: "フォロワー",
		other: "フォロワー",
		zero: "フォロワー"
	},
	com_member: {
		one: "メンバー",
		other: "メンバー",
		zero: "メンバー"
	},
	com_like: {
		one: "{{count}} 好き",
		other: "{{count}} 好き",
		zero: "{{count}} 好き"
	},
	com_comment: {
		one: "{{count}} コメント",
		other: "{{count}} コメント",
		zero: "{{count}} コメント"
	},
	com_helping_verb: {
		one: "is",
		other: "are"
	},
	com_write_something: "何か書いて...",
	com_read_more: "続きを読む",
	com_leave_club: "立ち去る",
	com_pending_club: "保留中",
	com_join_club: "参加する",
	com_something_wrong: "何かがうまくいかなかった",
	com_sure_to_leave_club_header: "Leave Group",
	com_sure_to_leave_club: "あなたはクラブを去りますか？",
	com_select_image: "画像を選択",
	com_take_photo: "写真を撮る",
	com_choose_from_lib: "ライブラリから選択",
	com_empty_post_error: "空の投稿をアップロードできません",
	com_create_post: "投稿を作成",
	com_cancel: "キャンセル",
	com_ok: "OK",
	com_post: "役職",
	com_image: "画像",
	com_username: "名",
	com_male: "男性",
	com_female: "女性",
	com_update_profile: "プロフィールを更新",
	com_following: "以下",
	com_club_member: "クラブ会員",
	com_sure_to_delete_post: "投稿を削除しますか？",
	com_sure_to_report_post_header: "Report the Post",
	com_sure_to_report_post: "投稿を報告してよろしいですか",
	com_sure_to_delete_comment: "コメントを削除してよろしいですか",
	com_write_comment: "コメントを書く...",
	com_report_this_post: "この投稿を報告する",
	com_report_post_desc:
		"不適切なコンテンツが見つかった場合は、この投稿を報告することができます",
	com_hide_this_post: "投稿を隠す",
	com_see_few_post: "このような少数の記事を見なさい",
	com_unfollow: "フォローを解除",
	com_follow: "た",
	com_stop_see_post: "この人からの投稿を見るのをやめる",
	com_start_see_post: "この人からの投稿を見始めます",
	com_delete_post: "投稿を削除",
	com_delete_post_from_wall: "アクティビティウォールから投稿を削除する",
	com_no_post_found: "投稿が見つかりません",
	com_sorry: "ごめんなさい",
	com_following_no_one: "あなたは誰もフォローしていません。",
	com_no_followers: "あなたにはフォロワーがいません。",
	com_no_club_members: "クラブ会員は見つかりませんでした。",
	com_video_length_out_of_limit_header: "Video size not supported",
	com_video_length_out_of_limit:
		"Upload a video of length between 3 to 60 Seconds."
};
