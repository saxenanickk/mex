import CARETDOWNCPG from "./caret_down_clubpage.png";
import CARETDOWN from "./caret_down.png";
import CARETUPCPG from "./caret_up_clubpage.png";
import DELETE from "./delete.png";
import EDIT from "./edit.png";
import FLIGHT from "./flight.png";
import HIDE from "./hide.png";
import SCENERY from "./picture.png";
import PLAYBUTTON from "./play_button.png";
import REPORT from "./report.png";
import SAVE from "./save.png";
import SENDARROW from "./send_arrow.png";
import UNFOLLOW from "./unfollow.png";
import CONNECTLOGO from "./connect_logo.png";

export {
	CARETDOWNCPG,
	CARETDOWN,
	CARETUPCPG,
	DELETE,
	EDIT,
	FLIGHT,
	HIDE,
	SCENERY,
	PLAYBUTTON,
	REPORT,
	SAVE,
	SENDARROW,
	UNFOLLOW,
	CONNECTLOGO
};
