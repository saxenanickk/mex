import { combineReducers } from "redux";
// Import Different Reducers Here
import { reducer as activityLogReducer } from "./Containers/ActivityLog/Reducer";
import { reducer as clubReducer } from "./Containers/Club/Reducer";
import { reducer as postReducer } from "./Containers/Activity/Reducer";
import { reducer as postDetailReducer } from "./Containers/PostDetail/Reducer";
import { reducer as myProfileReducer } from "./Containers/EditProfile/Reducer";
import { reducer as profileReducer } from "./Containers/UserProfile/Reducer";
import { reducer as connectionMatchReducer } from "./Containers/ConnectionsMatch/Reducer";
import { reducer as socialInviteReducer } from "./Containers/SocialInvite/Reducer";
import { reducer as createPostReducer } from "./Containers/CreatePost/Reducer";
import {
	COMMUNITY_SAVE_ERROR,
	COMMUNITY_RESET_ERROR,
	COMMUNITY_SAVE_CURRENT_DISPLAY_SCREEN,
	COMMUNITY_SAVE_DEEPLINK_PARAMS
} from "./Saga";
const initialState = {
	errorScreen: null,
	errorMessage: null,
	isError: false,
	currentDisplayScreen: null,
	deeplinkParams: null
};
export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case COMMUNITY_SAVE_ERROR:
			return {
				...state,
				errorScreen: action.payload.errorScreen,
				errorMessage: action.payload.errorMessage,
				isError: action.payload.isError
			};
		case COMMUNITY_RESET_ERROR:
			return {
				...state,
				errorScreen: null,
				errorMessage: null,
				isError: false
			};
		case COMMUNITY_SAVE_CURRENT_DISPLAY_SCREEN:
			return {
				...state,
				currentDisplayScreen: action.payload
			};
		case COMMUNITY_SAVE_DEEPLINK_PARAMS:
			return {
				...state,
				deeplinkParams: action.payload
			};
		default:
			return {
				...state
			};
	}
};
const communityReducer = combineReducers({
	activityLog: activityLogReducer,
	error: reducer,
	clubReducer: clubReducer,
	postReducer: postReducer,
	postDetailReducer: postDetailReducer,
	myProfileReducer: myProfileReducer,
	profile: profileReducer,
	connectionMatchReducer: connectionMatchReducer,
	createPostReducer: createPostReducer,
	socialInviteReducer: socialInviteReducer
});

export default communityReducer;
