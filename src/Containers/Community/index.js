import React from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import reducer from "./Reducer";
import { getNewReducer, removeExistingReducer } from "../../../App";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";

export default class Community extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "community" });
		getNewReducer({ name: "community", reducer: reducer });
		console.disableYellowBox = true;
	}

	componentWillUnmount() {
		removeExistingReducer("community");
	}
	render() {
		//Deeplink Params
		return <App {...this.props} />;
	}
}

AppRegistry.registerComponent("Community", () => Community);
