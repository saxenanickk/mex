import React, { Fragment } from "react";
import { SafeAreaView, StatusBar } from "react-native";
import RegisterScreen from "./RegisterScreen";
import GoAppAnalytics from "../../utils/GoAppAnalytics";
import { DrawerOpeningHack } from "../../Components";
import { connect } from "react-redux";
import { communitySaveDeeplinkParams } from "./Saga";
import { communitySaveMyProfile } from "./Containers/UserProfile/Saga";
class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			prevState: null,
			currState: null
		};
	}
	detectChangeOfScreen = (prevState, currState) => {
		if (prevState && currState) {
			let prevStateRouteLength = prevState.routes.length;
			let currStateRouteLength = currState.routes.length;
			if (prevStateRouteLength > 0 && currStateRouteLength > 0) {
				let prevRoute = prevState.routes[prevStateRouteLength - 1];
				let currRoute = currState.routes[currStateRouteLength - 1];
				if (prevRoute.routeName !== currRoute.routeName) {
					GoAppAnalytics.setPageView("community", currRoute.routeName);
				}
			}
		}
	};

	componentDidMount() {
		try {
			if (this.props.params) {
				console.log("params in app are", this.props.params);
				if (typeof this.props.params === "string") {
					let params = JSON.parse(this.props.params);
					this.props.dispatch(communitySaveDeeplinkParams(params));
				} else {
					this.props.dispatch(communitySaveDeeplinkParams(this.props.params));
				}
			}
			if (this.props.route.params) {
				let params = JSON.parse(this.props.route.params);
				this.props.dispatch(communitySaveDeeplinkParams(params));
			}
		} catch (e) {
			console.log("Do nothing. deep link format was not JSON");
		}
	}
	componentWillUnmount() {
		this.props.dispatch(communitySaveMyProfile(null));
	}
	render() {
		return (
			<Fragment>
				{/** For the Top Color (Under the Notch) */}
				<SafeAreaView style={{ flex: 0, backgroundColor: "#ffffff" }} />
				{/** For the Bottom Color (Below the home touch) */}
				<SafeAreaView style={{ flex: 1 }}>
					<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
					<RegisterScreen
						nav={this.props.nav}
						onNavigationStateChange={(prevState, currState) =>
							this.detectChangeOfScreen(prevState, currState)
						}
					/>
					<DrawerOpeningHack />
				</SafeAreaView>
			</Fragment>
		);
	}
}

export default connect()(App);
