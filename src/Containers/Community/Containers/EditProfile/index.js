import React, { Component } from "react";
import {
	View,
	ScrollView,
	Image,
	TouchableOpacity,
	Dimensions,
	Alert,
	BackHandler,
	Platform
} from "react-native";
import { connect } from "react-redux";
import { remove } from "lodash";
import {
	communityRefreshMyPost,
	communityGetSites,
	communitySaveAllSkills,
	communitySaveAllHobbiesAndIneterests,
	communitySaveEducationInstitute
} from "./Saga";
import I18n from "../../Assets/Strings/i18n";
import { styles } from "./style";
import {
	Icon,
	DatePicker,
	GoPicker,
	GoappCamera
} from "../../../../Components";
import CommunityHeader from "../../Components/Header";
import {
	CommunityTextRegular,
	CommunityTextMedium,
	CommunityTextInputRegular
} from "../../Components/CommunityText";
import MultipleItemSelector from "../../../../Components/MultipleItemSelector";
import ItemSelection from "./ItemSelection";
import {
	communityUpdateUserProfile,
	communityUpdateUserSuccess
} from "../UserProfile/Saga";
import CommunityLoader from "../../Components/CommunityLoader";
import { communityResetError } from "../../Saga";
import { font_two } from "../../../../Assets/styles.android";
import Config from "react-native-config";
import { CommonActions } from "@react-navigation/native";

const { SERVER_BASE_URL_LINKEDIN_API } = Config;
const { width, height } = Dimensions.get("window");

// Image Picker Options
const options = {
	storageOptions: {
		skipBackup: true,
		path: "images",
		cameraRoll: true,
		waitUntilSaved: true
	},
	maxHeight: 400,
	maxWidth: 400,
	quality: 0.5
};

const minDate = new Date("01/01/1900");
const maxDate = new Date();

class EditProfile extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: false,
			isEditable: false,
			photo: null,
			profile: {
				...props.myProfile,
				current_organisation: props.user.tenant_name
			},
			itemSelector: null
		};

		const { route } = props;
		const { linkedInTokenDetails, fbTokenDetails, isUseConnect } = route.params
			? route.params
			: {};

		this.linkedInTokenDetails = linkedInTokenDetails;
		this.fbTokenDetails = fbTokenDetails;
		this.isUseConnect = isUseConnect;

		this.handleGoBack = this.handleGoBack.bind(this);
		this.cameraRef = React.createRef();
		BackHandler.addEventListener("hardwareBackPress", this.handleGoBack);
	}

	componentDidMount() {
		this.props.dispatch(communityGetSites({ appToken: this.props.appToken }));
		this.isScreenLoadedFromConnect();
	}

	shouldComponentUpdate(nextProps, nextState) {
		if (nextProps.sites.length !== this.props.sites.length) {
			let user_site_id = this.props.user.site_id;
			let siteDetails = nextProps.sites.find(item => {
				return item.ref_site_id == user_site_id;
			});
			if (siteDetails && siteDetails.id !== user_site_id) {
				this.setState((state, props) => {
					return {
						profile: {
							...state.profile,
							site_id: siteDetails.id
						}
					};
				});
			}
		}

		if (this.props.isMyProfileLoading && !nextProps.isMyProfileLoading) {
			this.setState({
				isLoading: false
			});
		}

		if (
			nextProps.error &&
			nextProps.error !== this.props.error &&
			nextProps.error.isError &&
			nextProps.error.errorScreen === "EditProfile"
		) {
			Alert.alert(I18n.t("error"), nextProps.error.errorMessage);
			this.props.dispatch(communityResetError());
			this.setState({ isLoading: false, isEditable: false });
		}

		if (
			nextProps.isProfileUpdateSuccess &&
			nextProps.isProfileUpdateSuccess === true
		) {
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 1,
					routes: [{ name: "Home" }, { name: "UserProfile" }]
				})
			);
		}
		return true;
	}

	componentWillUnmount() {
		this.props.dispatch(communityRefreshMyPost(null));
		this.props.dispatch(communityUpdateUserSuccess(null));
		this.props.dispatch(communitySaveAllSkills([]));
		this.props.dispatch(communitySaveAllHobbiesAndIneterests([]));
		this.props.dispatch(communitySaveEducationInstitute([]));
		BackHandler.removeEventListener("hardwareBackPress", this.handleGoBack);
	}

	isScreenLoadedFromConnect = () => {
		const linkedInTokenDetails = this.linkedInTokenDetails || null;

		if (linkedInTokenDetails !== null) {
			this.addLinkedInDataToProfile(linkedInTokenDetails);
			return true;
		}

		return false;
	};

	addLinkedInDataToProfile = async tokenDetails => {
		try {
			this.setState({ isLoading: true });
			let linkedInProfile = {};
			const profileResp = fetch(
				`${SERVER_BASE_URL_LINKEDIN_API}me?projection=(id,localizedFirstName,localizedLastName,vanityName,localizedHeadline,profilePicture(displayImage~:playableStreams))`,
				{
					method: "GET",
					headers: {
						Authorization: `Bearer ${tokenDetails.token}`
					}
				}
			).then(res => {
				if (res.status === 200) {
					return res.json();
				}
			});

			const emailResponse = fetch(
				`${SERVER_BASE_URL_LINKEDIN_API}emailAddress?q=members&projection=(elements*(handle~))`,
				{
					method: "GET",
					headers: {
						Authorization: `Bearer ${tokenDetails.token}`
					}
				}
			).then(res => {
				if (res.status === 200) {
					return res.json();
				}
			});

			const [profile] = await Promise.all([profileResp, emailResponse]);
			//let profile = null
			if (profile) {
				linkedInProfile = {
					...linkedInProfile,
					// Name is not going to be updated
					// ...(profile.localizedFirstName && {
					// 	firstName: profile.localizedFirstName,
					// }),
					// ...(profile.localizedLastName && {
					// 	lastName: profile.localizedLastName,
					// }),
					...(profile.profilePicture && {
						profilePicture:
							profile.profilePicture["displayImage~"].elements[
								profile.profilePicture["displayImage~"].elements.length - 1
							].identifiers[0].identifier
					})
				};
			}
			this.setState(
				(state, props) => {
					return {
						profile: {
							...state.profile,
							linkedin: tokenDetails
						}
					};
				},
				() => {
					const fbTokenDetails = this.fbTokenDetails || null;

					if (fbTokenDetails !== null) {
						this.addFacebookDataToProfile();
					} else {
						this.setState({ isLoading: false });
					}
				}
			);
		} catch (error) {
			console.log("error of adding linkedin data is", error);
			this.setState({ isLoading: false });
		}
	};

	_responseInfoCallback = (error, result) => {
		try {
			if (error) {
				// Do something about error
				this.setState({ isLoading: false });
			} else {
				const fbTokenDetails = this.fbTokenDetails || null;

				// let hobbies_interests =
				// 	result && result.likes
				// 		? result.likes.data.map(likeObj => likeObj.name)
				// 		: []
				// let date_of_birth = result.birthday
				// let home_town = result.hometown
				this.setState((state, props) => {
					return {
						isLoading: false,
						profile: {
							...state.profile,
							facebook: fbTokenDetails
							// hobbies_interests: unionBy(
							// 	state.profile.hobbies_interests,
							// 	hobbies_interests,
							// ),
							// ...(state.profile.home_town === null ? { home_town } : {}),
							// ...(state.profile.date_of_birth === null
							// 	? { date_of_birth }
							// 	: {}),
						}
					};
				});
			}
		} catch (error) {
			console.log("error when adding facebook", error);
			this.setState({ isLoading: false });
		}
	};

	addFacebookDataToProfile = async () => {
		const fbTokenDetails = this.fbTokenDetails || null;
		this.setState({
			isLoading: false,
			profile: {
				...this.state.profile,
				facebook: fbTokenDetails
			}
		});
		//let accessToken = await AccessToken.getCurrentAccessToken()
		// if (accessToken !== null) {
		// 	const infoRequest = new GraphRequest(
		// 		"/me",
		// 		{
		// 			parameters: {
		// 				fields: {
		// 					//string: "birthday, age_range, likes{name}, hometown",
		// 					string: "birthday, age_range",
		// 				},
		// 			},
		// 		},
		// 		this._responseInfoCallback,
		// 	)
		// 	new GraphRequestManager().addRequest(infoRequest).start()
		// }
	};

	checkProfileValidity = () => {
		try {
			if (
				this.props.myProfile.hobbies_interests === null ||
				this.props.myProfile.hobbies_interests === undefined ||
				this.props.myProfile.hobbies_interests.length === 0
			) {
				return false;
			} else if (
				!this.props.myProfile.about ||
				this.props.myProfile.about.trim() === "" ||
				this.props.myProfile.about.length <= 30
			) {
				return false;
			} else if (
				!this.props.myProfile.name ||
				this.props.myProfile.name.trim() === ""
			) {
				return false;
			} else {
				return true;
			}
		} catch (error) {
			console.log("error while check valid", error);
			return false;
		}
	};
	async handleGoBack() {
		if (this.state.isEditable) {
			this.setState({ isEditable: false });
		} else {
			const linkedInTokenDetails = this.linkedInTokenDetails || null;

			const isUseConnect = this.isUseConnect || false;

			if (
				(isUseConnect || linkedInTokenDetails) &&
				this.checkProfileValidity() === false
			) {
				Alert.alert(
					"Warning",
					"If you go Back,you can not use connect feature without adding your name, about yourself and at least one hobby in your profile",
					[
						{
							text: "CANCEL",
							onPress: () => console.log("do nothing")
						},
						{
							text: "OK",
							onPress: () => this.props.navigation.goBack()
						}
					],
					{ cancelable: true }
				);
			} else {
				this.props.navigation.goBack();
			}
		}
	}

	handleTextChange = (type, text) => {
		this.setState((state, props) => {
			return {
				profile: {
					...state.profile,
					[type]: text
				}
			};
		});
	};

	handleTextChangeWithMaxLength = (type, text, maxLength) => {
		if (text.length == maxLength) {
			Alert.alert(
				"Info",
				"Sorry to interrupt, please keep your description short and sweet (140 characters)"
			);
		} else {
			this.handleTextChange(type, text);
		}
	};

	addItem = (data, type) => {
		if (type === "skills") {
			this.setState((state, props) => {
				return {
					itemSelector: null,
					profile: {
						...state.profile,
						skills: data.map(item => (item.value ? item.value : item))
					}
				};
			});
		} else if (type === "education") {
			this.setState((state, props) => {
				return {
					itemSelector: null,
					profile: {
						...state.profile,
						education: data.map(item =>
							item.value ? { name: item.value } : { name: item }
						)
					}
				};
			});
		} else {
			this.setState((state, props) => {
				return {
					itemSelector: null,
					profile: {
						...state.profile,
						hobbies_interests: data.map(item =>
							item.value ? item.value : item
						)
					}
				};
			});
		}
	};

	removeItem = (data, type) => {
		if (type === "skills") {
			let temp = this.state.profile.skills.slice();
			remove(temp, item => item === data);
			this.setState((state, props) => {
				return {
					profile: {
						...state.profile,
						skills: temp
					},
					itemSelector: null
				};
			});
		} else if (type === "education") {
			let temp = this.state.profile.education.slice();
			remove(temp, item => item.name === data);
			this.setState((state, props) => {
				return {
					profile: {
						...state.profile,
						education: temp
					},
					itemSelector: null
				};
			});
		} else {
			let temp = this.state.profile.hobbies_interests.slice();
			remove(temp, item => item === data);
			this.setState((state, props) => {
				return {
					itemSelector: null,
					profile: {
						...state.profile,
						hobbies_interests: temp
					}
				};
			});
		}
	};

	handleSave = () => {
		const linkedInTokenDetails = this.linkedInTokenDetails || null;

		const isUseConnect = this.isUseConnect || false;

		if (!this.state.profile.name || this.state.profile.name.trim() === "") {
			Alert.alert("Information", "You need to add your name to use Community");
			return;
		}
		if (
			(isUseConnect || linkedInTokenDetails) &&
			(!this.state.profile.hobbies_interests ||
				this.state.profile.hobbies_interests.length === 0)
		) {
			Alert.alert(
				"Information",
				"You need to add your hobbies and interests to use connect feature."
			);
			return;
		}
		if (
			(isUseConnect || linkedInTokenDetails) &&
			(!this.state.profile.about ||
				this.state.profile.about.trim() === "" ||
				this.state.profile.about.length <= 30)
		) {
			Alert.alert(
				"Information",
				"You need to add something about yourself (Minimum 30 characters) to use connect feature."
			);
			return;
		}
		this.setState({ isLoading: true });
		this.props.dispatch(
			communityUpdateUserProfile({
				profile: this.state.profile,
				appToken: this.props.appToken,
				photo: this.state.photo
			})
		);
	};

	openImagePicker = () => {
		this.cameraRef.handleCameraPermission();
	};

	renderUserImage = () => {
		try {
			let image = this.state.profile && this.state.profile.image;
			if (image !== null && image !== undefined && image.trim() !== "") {
				return <Image source={{ uri: image }} style={styles.userImage} />;
			} else {
				throw new Error("User profile pic not found");
			}
		} catch (error) {
			return (
				<View style={styles.noUserImage}>
					<Icon
						iconType={"ionicon"}
						iconSize={width / 8}
						iconName={"ios-person"}
						iconColor={"rgba(0,0,0,0.5)"}
					/>
				</View>
			);
		}
	};

	getDataForItemSelector = data => {
		return data.map(item => {
			let obj = {};
			obj.value = item;
			obj.isMatching = false;
			obj.isEditable = true;
			return obj;
		});
	};

	handlePickerValue = ({ itemValue, itemIndex }) => {
		this.setState((state, props) => {
			return {
				profile: {
					...state.profile,
					gender: itemValue
				}
			};
		});
	};

	validateTypeOfImage = image => {
		try {
			let response = image;
			if (image.type === null || image.type === undefined) {
				let type = image.fileName.split(".");
				type = type[type.length - 1].toLowerCase();
				response = {
					...response,
					type: `image\/${type}`
				};
			}
			return response;
		} catch (error) {
			return image;
		}
	};
	render() {
		const linkedInTokenDetails = this.linkedInTokenDetails || null;

		const isUseConnect = this.isUseConnect || false;

		return (
			<View style={styles.container}>
				<View style={styles.dataContainer}>
					<CommunityHeader onPress={this.handleGoBack} />
					<ScrollView style={{}} showsVerticalScrollIndicator={false}>
						<View
							style={{
								flexDirection: "row",
								flex: 1
							}}>
							<View style={styles.imageView}>
								{this.renderUserImage()}
								<TouchableOpacity
									style={styles.changeImage}
									activeOpacity={1}
									onPress={this.openImagePicker}>
									<Icon
										iconType={"ionicon"}
										iconSize={width / 24}
										iconName={"md-create"}
										iconColor={"rgba(0,0,0,0.5)"}
									/>
								</TouchableOpacity>
							</View>
							<View
								style={{
									marginHorizontal: 20,
									flex: 1
								}}>
								<View style={styles.rowAlign}>
									<View style={{ flexDirection: "row", width: "40%" }}>
										<CommunityTextMedium numberOfLines={1}>
											{"Name"}
										</CommunityTextMedium>
									</View>
									<CommunityTextInputRegular
										style={styles.inputStyle}
										placeholder={"Name"}
										color={"#000"}
										value={this.state.profile.name}
										onChangeText={text => this.handleTextChange("name", text)}
									/>
								</View>
								<View style={styles.rowAlign}>
									<CommunityTextMedium style={styles.labelStyle}>
										{"Gender"}
									</CommunityTextMedium>
									<GoPicker
										data={["Male", "Female", "Other"]}
										selectedValue={
											this.state.profile.gender ? this.state.profile.gender : ""
										}
										updateSelectedValue={this.handlePickerValue}
										textStyle={{
											fontSize: height / 60,
											fontFamily: Platform.OS === "ios" ? "Avenir" : font_two,
											fontWeight: Platform.OS === "ios" ? "400" : undefined
										}}
									/>
								</View>
								<TouchableOpacity
									style={styles.rowAlign}
									onPress={() => this.setState({ showDatePicker: true })}>
									<CommunityTextMedium style={styles.labelStyle}>
										{"Birth Day"}
									</CommunityTextMedium>
									<View style={styles.inputStyle}>
										<CommunityTextRegular color={"#000"} numberOfLines={1}>
											{this.state.profile.date_of_birth
												? I18n.l(
														"date.formats.date_of_birth",
														this.state.profile.date_of_birth
												  )
												: "DD/MM/YYYY"}
										</CommunityTextRegular>
									</View>
								</TouchableOpacity>
							</View>
						</View>
						<View
							style={[
								styles.columnAlign,
								{ paddingHorizontal: 10, paddingVertical: 10 }
							]}>
							<View style={[styles.rowAlign]}>
								<CommunityTextMedium style={styles.labelStyle}>
									Designation
								</CommunityTextMedium>
								<CommunityTextInputRegular
									style={styles.inputStyle}
									placeholder={"Designation"}
									color={"#000"}
									numberOfLines={1}
									value={
										this.state.profile.current_designation
											? this.state.profile.current_designation
											: ""
									}
									onChangeText={text =>
										this.handleTextChange("current_designation", text)
									}
								/>
							</View>
							<View style={[styles.rowAlign, { paddingVertical: 10 }]}>
								<CommunityTextMedium style={styles.labelStyle}>
									Organisation
								</CommunityTextMedium>
								<View style={[styles.inputStyle, { borderBottomWidth: 0 }]}>
									<CommunityTextRegular color={"#000"} numberOfLines={1}>
										{this.state.profile.current_organisation
											? this.state.profile.current_organisation
											: ""}
									</CommunityTextRegular>
								</View>
							</View>
							<View style={[styles.rowAlign, { paddingBottom: 10 }]}>
								<CommunityTextMedium style={styles.labelStyle}>
									Home Town
								</CommunityTextMedium>
								<CommunityTextInputRegular
									style={styles.inputStyle}
									placeholder={"Home town"}
									color={"#000"}
									numberOfLines={1}
									value={
										this.state.profile.home_town
											? this.state.profile.home_town
											: ""
									}
									onChangeText={text =>
										this.handleTextChange("home_town", text)
									}
								/>
							</View>
							<View
								style={{
									alignSelf: "flex-start",
									width: "100%",
									borderWidth: 0.5,
									borderColor: "#000000",
									borderRadius: width / 100,
									paddingVertical: width / 100
								}}>
								<View style={{ flexDirection: "row" }}>
									<CommunityTextMedium
										style={{ marginHorizontal: width / 100 }}>
										About
									</CommunityTextMedium>
									{linkedInTokenDetails || isUseConnect ? (
										<CommunityTextMedium
											style={{
												lineHeight: height / 40,
												color: "#ff0000"
											}}>
											*
										</CommunityTextMedium>
									) : null}
								</View>
								<CommunityTextInputRegular
									style={[
										styles.inputStyle,
										{
											width: "95%",
											margin: width / 100,
											textAlignVertical: "top",
											borderBottomWidth: 0
										}
									]}
									placeholder={"Write about yourself in 140 Characters"}
									color={"#000"}
									multiline={true}
									value={
										this.state.profile.about ? this.state.profile.about : ""
									}
									onChangeText={text =>
										this.handleTextChangeWithMaxLength("about", text, 141)
									}
									maxLength={141}
									returnKeyType={Platform.OS === "ios" ? "done" : "default"}
									blurOnSubmit={Platform.OS === "ios" ? true : false}
								/>
							</View>
							<View style={{ alignSelf: "flex-start" }}>
								<MultipleItemSelector
									data={
										this.state.profile.skills ? this.state.profile.skills : []
									}
									type={"Professional skills"}
									isAddMore={true}
									isEditable={true}
									addMore={selectionType =>
										this.setState({
											itemSelector: selectionType
										})
									}
									removeItem={item => this.removeItem(item, "skills")}
								/>
							</View>
							<View style={{ alignSelf: "flex-start" }}>
								<MultipleItemSelector
									data={
										this.state.profile.hobbies_interests
											? this.state.profile.hobbies_interests
											: []
									}
									type={"Hobbies & Interests"}
									isAddMore={true}
									addMore={selectionType =>
										this.setState({
											itemSelector: selectionType
										})
									}
									isEditable={true}
									isMandatory={linkedInTokenDetails || isUseConnect}
									removeItem={item => this.removeItem(item, "hobby")}
								/>
							</View>
							<View style={{ alignSelf: "flex-start" }}>
								<MultipleItemSelector
									data={
										this.state.profile.education
											? this.state.profile.education.map(item => item.name)
											: []
									}
									type={"Education"}
									isAddMore={true}
									addMore={selectionType =>
										this.setState({
											itemSelector: selectionType
										})
									}
									isEditable={true}
									removeItem={item => this.removeItem(item, "education")}
								/>
							</View>
							{this.state.profile.past_organisation && (
								<View style={{ alignSelf: "flex-start" }}>
									<MultipleItemSelector
										data={
											this.state.profile.past_organisation
												? this.state.profile.past_organisation
												: []
										}
										type={"Past Organisations"}
									/>
								</View>
							)}
						</View>
					</ScrollView>

					{this.state.isLoading && (
						<View style={styles.progressContainer}>
							<CommunityLoader size={width / 15} />
						</View>
					)}
				</View>
				<TouchableOpacity
					style={{
						padding: 10,
						backgroundColor: "#3b64e5",
						justifyContent: "center",
						alignItems: "center",
						marginVertical: 10,
						borderRadius: 5
					}}
					onPress={this.handleSave}>
					<CommunityTextMedium color={"#ffffff"} size={height / 50}>
						{"Save"}
					</CommunityTextMedium>
				</TouchableOpacity>
				{typeof this.state.itemSelector === "string" ? (
					<ItemSelection
						visible={true}
						type={this.state.itemSelector}
						selectedSkills={this.state.profile.skills}
						selectedHobby={this.state.profile.hobbies_interests}
						selectedEducation={this.state.profile.education}
						onBackPress={() =>
							this.setState({
								itemSelector: null
							})
						}
						onDonePress={(data, type) => this.addItem(data, type)}
					/>
				) : null}
				{this.state.showDatePicker ? (
					<DatePicker
						date={maxDate}
						minimumDate={minDate}
						maximumDate={maxDate}
						onDateChange={date => {
							if (date) {
								const { month, day, year } = date;
								let selectedDate = month + 1 + "/" + day + "/" + year;
								this.handleTextChange("date_of_birth", selectedDate);
							}
							this.setState({ showDatePicker: false });
						}}
					/>
				) : null}
				<GoappCamera
					ref={ref => (this.cameraRef = ref)}
					options={options}
					quality={0.5}
					onImageSelect={response => {
						this.setState((state, props) => {
							response = this.validateTypeOfImage(response);
							return {
								profile: {
									...state.profile,
									image: response.uri
								},
								photo: response
							};
						});
					}}
				/>
			</View>
		);
	}
}

const mapStateToProps = state => ({
	appToken: state.appToken.token,
	user: state && state.appToken && state.appToken.user && state.appToken.user,
	myProfile: state.community && state.community.profile.myProfile,
	isMyProfileLoading:
		state.community && state.community.profile.isMyProfileLoading,
	sites: state.community && state.community.myProfileReducer.sites,
	error: state.community && state.community.error,
	isProfileUpdateSuccess:
		state.community && state.community.profile.isProfileUpdateSuccess
});

export default connect(mapStateToProps)(EditProfile);
