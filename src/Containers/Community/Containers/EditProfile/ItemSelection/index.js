import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	FlatList,
	Modal,
	TextInput,
	Keyboard
} from "react-native";
import { connect } from "react-redux";
import { Icon } from "../../../../../Components";
import { styles } from "./style";
import {
	CommunityTextMedium,
	CommunityTextRegular
} from "../../../Components/CommunityText";
import { remove, debounce } from "lodash";
import {
	communityGetAllSkills,
	communityGetEducationInstitute,
	communityGetAllHobbiesAndInterests,
	communitySaveAllSkills,
	communitySaveEducationInstitute,
	communitySaveAllHobbiesAndIneterests
} from "../Saga";
const { width, height } = Dimensions.get("window");

class ItemSelection extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedItemList:
				this.props.type === "Professional skills"
					? this.props.selectedSkills
						? this.props.selectedSkills.slice()
						: []
					: this.props.type === "Education"
					? this.props.selectedEducation
						? this.props.selectedEducation.slice().map(item => item.name)
						: []
					: this.props.selectedHobby
					? this.props.selectedHobby.slice()
					: []
		};
	}

	componentWillUnmount() {
		if (this.props.type === "Professional skills") {
			this.props.dispatch(communitySaveAllSkills([]));
		} else if (this.props.type === "Education") {
			this.props.dispatch(communitySaveEducationInstitute([]));
		} else {
			this.props.dispatch(communitySaveAllHobbiesAndIneterests([]));
		}
	}

	toggleItemSelection = selectedItem => {
		Keyboard.dismiss();
		try {
			let temp = this.state.selectedItemList
				? this.state.selectedItemList.slice()
				: [];

			if (
				this.state.selectedItemList.findIndex(row => row === selectedItem) >= 0
			) {
				remove(temp, item => item === selectedItem);
				this.setState({ selectedItemList: temp });
			} else {
				temp.push(selectedItem);
				this.setState({ selectedItemList: temp });
			}
		} catch (error) {
			return null;
		}
	};

	getDataForItemSelection = () => {
		let data = [];
		if (this.props.type === "Professional skills") {
			data = this.props.skills;
		} else if (this.props.type === "Education") {
			data = this.props.education;
		} else {
			data = this.props.hobbiesAndInterests;
		}
		return data;
	};

	onDonePress = () => {
		if (this.props.type === "Professional skills") {
			this.props.onDonePress(this.state.selectedItemList, "skills");
		} else if (this.props.type === "Education") {
			this.props.onDonePress(this.state.selectedItemList, "education");
		} else {
			this.props.onDonePress(this.state.selectedItemList, "hobby");
		}
	};

	isItemSelected = value => {
		try {
			if (this.props.type === "Professional skills") {
				return (
					this.state.selectedItemList.findIndex(row =>
						typeof row === "string" ? row === value : row.value === value
					) >= 0
				);
			} else {
				return (
					this.state.selectedItemList.findIndex(row =>
						typeof row === "string" ? row === value : row.value === value
					) >= 0
				);
			}
		} catch (error) {
			return false;
		}
	};

	handleTextChange = debounce(text => {
		if (text.length > 0) {
			if (this.props.type === "Professional skills") {
				this.props.dispatch(
					communityGetAllSkills({
						appToken: this.props.appToken,
						searchQuery: text
					})
				);
			} else if (this.props.type === "Education") {
				this.props.dispatch(
					communityGetEducationInstitute({
						appToken: this.props.appToken,
						searchQuery: text
					})
				);
			} else {
				this.props.dispatch(
					communityGetAllHobbiesAndInterests({
						appToken: this.props.appToken,
						searchQuery: text
					})
				);
			}
		} else {
			if (this.props.type === "Professional skills") {
				this.props.dispatch(communitySaveAllSkills([]));
			} else if (this.props.type === "Education") {
				this.props.dispatch(communitySaveEducationInstitute([]));
			} else {
				this.props.dispatch(communitySaveAllHobbiesAndIneterests([]));
			}
		}
	}, 300);

	getPlaceholderText = () => {
		const { props } = this;
		return props.type === "Professional skills"
			? "Please enter your Skill"
			: props.type === "Hobbies & Interests"
			? "Please enter your Hobby"
			: "Please enter the name of your college";
	};
	render() {
		const { props } = this;
		return (
			<Modal
				visible={props.visible}
				transparent={true}
				animationType={"slide"}
				onRequestClose={() => props.onBackPress()}>
				<View style={styles.container}>
					<View style={styles.dataContainer}>
						<View style={styles.header}>
							<TouchableOpacity
								style={styles.backButton}
								onPress={() => props.onBackPress()}>
								<CommunityTextRegular
									style={{ marginLeft: 10 }}
									color={"#000000"}>
									{"Back"}
								</CommunityTextRegular>
							</TouchableOpacity>
							<CommunityTextMedium>{props.type}</CommunityTextMedium>
							<TouchableOpacity
								style={styles.backButton}
								onPress={() => this.onDonePress()}>
								<CommunityTextRegular
									style={{ marginRight: 10 }}
									color={"#000000"}>
									{"Done"}
								</CommunityTextRegular>
							</TouchableOpacity>
						</View>
						<View style={styles.messageSection}>
							<TextInput
								style={styles.searchTextInput}
								placeholder={this.getPlaceholderText()}
								onChangeText={this.handleTextChange}
								underlineColorAndroid={"transparent"}
								selectionColor={"#000"}
							/>
						</View>
						<FlatList
							extraData={this.state}
							data={this.getDataForItemSelection()}
							style={styles.list}
							showsVerticalScrollIndicator={false}
							keyboardShouldPersistTaps={"always"}
							renderItem={({ item }) => {
								return (
									<TouchableOpacity
										style={styles.listItem}
										onPress={() => this.toggleItemSelection(item.name)}>
										<Icon
											iconType={"ionicon"}
											iconSize={width / 18}
											iconName={"ios-checkmark-circle"}
											iconColor={
												this.isItemSelected(item.name) ? "#5da4df" : "#eeeff5"
											}
										/>
										<CommunityTextRegular style={styles.listItemName}>
											{item.name}
										</CommunityTextRegular>
									</TouchableOpacity>
								);
							}}
							ItemSeparatorComponent={() => (
								<View style={styles.listItemSeparator} />
							)}
						/>
					</View>
				</View>
			</Modal>
		);
	}
}

function mapStateToProps(state) {
	console.log("state is", state);
	return {
		appToken: state.appToken.token,
		error: state.community && state.community.error,
		skills: state.community && state.community.myProfileReducer.skills,
		education: state.community && state.community.myProfileReducer.education,
		hobbiesAndInterests:
			state.community && state.community.myProfileReducer.hobbiesAndInterests
	};
}

ItemSelection.defaultProps = {
	visible: false,
	onRequestClose: () => console.log("try to close the modal")
};
export default connect(mapStateToProps)(ItemSelection);
