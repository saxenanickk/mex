import {
	COMMUNITY_SAVE_MY_POST,
	COMMUNITY_REFRESH_MY_POST,
	COMMUNITY_SAVE_MY_OFFSET,
	COMMUNITY_SAVE_ALL_CLUB_MEMBERS,
	COMMUNITY_SAVE_ALL_SKILLS,
	COMMUNITY_SAVE_SITES,
	COMMUNITY_SAVE_ALL_HOBBIES_AND_INTERESTS,
	COMMUNITY_CHANGE_MY_POST_LIKE_LOCAL,
	COMMUNITY_CHANGE_MY_POST_COMMENT_LOCAL,
	COMMUNITY_SAVE_EDUCATION_INSTITUTE
} from "./Saga";
import _ from "lodash";
const initialState = {
	myPosts: null,
	myPostNextOffset: null,
	clubMembers: null,
	sites: [],
	skills: [],
	hobbiesAndInterests: [],
	education: []
};

/**
 * @param {{ type: string; payload: any; }} action
 */
export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case COMMUNITY_SAVE_MY_POST:
			return {
				...state,
				myPosts:
					state.myPosts === null
						? action.payload
						: state.myPosts.slice().concat(action.payload)
			};
		case COMMUNITY_SAVE_MY_OFFSET:
			return {
				...state,
				myPostNextOffset: action.payload.user !== null ? action.payload : null
			};
		case COMMUNITY_REFRESH_MY_POST:
			return {
				...state,
				myPosts: action.payload
			};
		case COMMUNITY_SAVE_ALL_CLUB_MEMBERS:
			return {
				...state,
				clubMembers: action.payload
			};
		case COMMUNITY_SAVE_ALL_SKILLS:
			return {
				...state,
				skills: action.payload
			};
		case COMMUNITY_SAVE_ALL_HOBBIES_AND_INTERESTS:
			return {
				...state,
				hobbiesAndInterests: action.payload
			};
		case COMMUNITY_SAVE_SITES:
			return {
				...state,
				sites: action.payload
			};
		case COMMUNITY_CHANGE_MY_POST_LIKE_LOCAL:
			let temp = state.myPosts !== null ? state.myPosts.slice() : state.myPosts;
			if (temp !== null) {
				let index = _.findIndex(state.myPosts, ["id", action.payload.postId]);
				if (temp[index]) {
					temp[index].hasLiked = action.payload.isLike;
					temp[index].liked_count = action.payload.likeCount;
				}
			}
			return {
				...state,
				myPosts: temp
			};
		case COMMUNITY_CHANGE_MY_POST_COMMENT_LOCAL:
			temp = state.myPosts !== null ? state.myPosts.slice() : state.myPosts;
			if (temp !== null) {
				let index = _.findIndex(state.myPosts, ["id", action.payload.postId]);
				if (temp[index]) {
					temp[index].comment_count = action.payload.commentCount;
				}
			}
			return {
				...state,
				myPosts: temp
			};
		case COMMUNITY_SAVE_EDUCATION_INSTITUTE:
			return {
				...state,
				education: action.payload
			};
		default:
			return state;
	}
};
