import { takeLatest, put, call } from "redux-saga/effects";
import Api from "../../Api";
import { communitySaveError } from "../../Saga";

export const COMMUNITY_GET_MY_POST = "COMMUNITY_GET_MY_POST";
export const COMMUNITY_SAVE_MY_POST = "COMMUNITY_SAVE_MY_POST";
export const COMMUNITY_REFRESH_MY_POST = "COMMUNITY_REFRESH_MY_POST";
export const COMMUNITY_SAVE_MY_OFFSET = "COMMUNITY_SAVE_MY_OFFSET";
export const COMMUNITY_GET_ALL_CLUB_MEMBERS = "COMMUNITY_GET_ALL_CLUB_MEMBERS";
export const COMMUNITY_SAVE_ALL_CLUB_MEMBERS =
	"COMMUNITY_SAVE_ALL_CLUB_MEMBERS";
export const COMMUNITY_GET_ALL_SKILLS = "COMMUNITY_GET_ALL_SKILLS";
export const COMMUNITY_SAVE_ALL_SKILLS = "COMMUNITY_SAVE_ALL_SKILLS";
export const COMMUNITY_GET_ALL_INTERESTS = "COMMUNTY_GET_ALL_INTERESTS";
export const COMMUNITY_SAVE_ALL_INTERESTS = "COMMUNTY_SAVE_ALL_INTERESTS";
export const COMMUNITY_UPDATE_USER_PROFILE_PIC =
	"COMMUNITY_UPDATE_USER_PROFILE_PIC";
export const COMMUNITY_GET_ALL_HOBBIES_AND_INTERESTS =
	"COMMUNTY_GET_ALL_HOBBIES_AND_INTERESTS";
export const COMMUNITY_SAVE_ALL_HOBBIES_AND_INTERESTS =
	"COMMUNTY_SAVE_ALL_HOBBIES_AND_INTERESTS";
export const COMMUNITY_GET_SITES = "COMMUNITY_GET_SITES";
export const COMMUNITY_SAVE_SITES = "COMMUNITY_SAVE_SITES";
export const COMMUNITY_CHANGE_MY_POST_LIKE_LOCAL =
	"COMMUNITY_CHANGE_MY_POST_LIKE_LOCAL";
export const COMMUNITY_CHANGE_MY_POST_COMMENT_LOCAL =
	"COMMUNITY_CHANGE_MY_POST_COMMENT_LOCAL";
export const COMMUNITY_GET_EDUCATION_INSTITUTE =
	"COMMUNITY_GET_EDUCATION_INSTITUTE";
export const COMMUNITY_SAVE_EDUCATION_INSTITUTE =
	"COMMUNITY_SAVE_EDUCATION_INSTITUTE";
export const communityGetMyPost = payload => ({
	type: COMMUNITY_GET_MY_POST,
	payload
});
export const communitySaveMyPost = payload => ({
	type: COMMUNITY_SAVE_MY_POST,
	payload
});
export const communityRefreshMyPost = payload => ({
	type: COMMUNITY_REFRESH_MY_POST,
	payload
});
export const communitySaveMyOffset = payload => ({
	type: COMMUNITY_SAVE_MY_OFFSET,
	payload
});
export const communityGetAllClubMembers = payload => ({
	type: COMMUNITY_GET_ALL_CLUB_MEMBERS,
	payload
});
export const communitySaveAllClubMembers = payload => ({
	type: COMMUNITY_SAVE_ALL_CLUB_MEMBERS,
	payload
});
export const communityGetAllSkills = payload => ({
	type: COMMUNITY_GET_ALL_SKILLS,
	payload
});
export const communitySaveAllSkills = payload => ({
	type: COMMUNITY_SAVE_ALL_SKILLS,
	payload
});
export const communityGetAllHobbiesAndInterests = payload => ({
	type: COMMUNITY_GET_ALL_HOBBIES_AND_INTERESTS,
	payload
});
export const communitySaveAllHobbiesAndIneterests = payload => ({
	type: COMMUNITY_SAVE_ALL_HOBBIES_AND_INTERESTS,
	payload
});
export const communityUpdateUserProfilePic = payload => ({
	type: COMMUNITY_UPDATE_USER_PROFILE_PIC,
	payload
});

export const communityGetSites = payload => ({
	type: COMMUNITY_GET_SITES,
	payload
});

export const communitySaveSites = payload => ({
	type: COMMUNITY_SAVE_SITES,
	payload
});

export const communityChangeMyPostLikeLocal = payload => ({
	type: COMMUNITY_CHANGE_MY_POST_LIKE_LOCAL,
	payload
});

export const communityChangeMyPostCommentLocal = payload => ({
	type: COMMUNITY_CHANGE_MY_POST_COMMENT_LOCAL,
	payload
});

export const communityGetEducationInstitute = payload => ({
	type: COMMUNITY_GET_EDUCATION_INSTITUTE,
	payload
});

export const communitySaveEducationInstitute = payload => ({
	type: COMMUNITY_SAVE_EDUCATION_INSTITUTE,
	payload
});

/**
 * Saga
 * @param {any} dispatch
 */
export function* communityMyProfileSaga(dispatch) {
	yield takeLatest(COMMUNITY_GET_MY_POST, handleCommunityGetMyPost);
	yield takeLatest(
		COMMUNITY_GET_ALL_CLUB_MEMBERS,
		handleCommunityGetAllClubMembers
	);
	yield takeLatest(COMMUNITY_GET_ALL_SKILLS, handleCommunityGetAllSkills);
	yield takeLatest(
		COMMUNITY_GET_ALL_HOBBIES_AND_INTERESTS,
		handleCommunityGetAllHobbiesAndInterests
	);
	yield takeLatest(COMMUNITY_GET_SITES, handleCommunityGetSites);
	yield takeLatest(
		COMMUNITY_GET_EDUCATION_INSTITUTE,
		handleCommunityGetEducationInstitute
	);
}

function* handleCommunityUpdateUserProfilePic(action) {
	try {
		if (action.payload.photo !== null && action.payload.photo !== undefined) {
			yield call(Api.uploadProfilePic, action.payload);
		}
	} catch (error) {
		console.log(error, "error");
		yield put(
			communitySaveError({
				errorScreen: "EditProfile",
				errorMessage: "Unable to update profile pic,try again",
				isError: true
			})
		);
	}
}
function* handleCommunityGetMyPost(action) {
	try {
		let response = yield call(Api.getMyPostsOnly, action.payload);
		console.log("response", response);
		if (response.success === 1) {
			let myPost = response.data;
			if (
				action.payload.isRefresh !== undefined &&
				action.payload.isRefresh === true
			) {
				yield put(communityRefreshMyPost(myPost));
			} else {
				yield put(communitySaveMyPost(myPost));
			}
			if (response.offset !== null && response.offset !== undefined) {
				yield put(communitySaveMyOffset(response.offset));
			}
		} else {
			throw new Error("unable to fetch posts");
		}
	} catch (error) {
		console.log(error, "error");
		yield put(communityRefreshMyPost([]));
	}
}

function* handleCommunityGetAllClubMembers(action) {
	try {
		let response = yield call(Api.getClubMembers, action.payload);
		if (response.success === 1) {
			yield put(communitySaveAllClubMembers(response.data));
		} else {
			throw new Error("Unable to fetch");
		}
	} catch (error) {
		yield put(
			communitySaveError({
				errorScreen: "UserListView",
				errorMessage: "OOps,Something went wrong, Please try again later",
				isError: true
			})
		);
	}
}

function* handleCommunityGetAllSkills(action) {
	try {
		let response = yield call(Api.getAllSkills, action.payload);
		if (response.success === 1) {
			yield put(communitySaveAllSkills(response.data));
		} else {
			throw new Error("Unable to fetch");
		}
	} catch (error) {
		console.log("error in getting skills", error);
	}
}

function* handleCommunityGetAllHobbiesAndInterests(action) {
	try {
		let response = yield call(Api.getHobbiesAndInterests, action.payload);
		if (response.success === 1) {
			yield put(communitySaveAllHobbiesAndIneterests(response.data));
		}
	} catch (error) {
		console.log("error in getting hobby", error);
	}
}

function* handleCommunityGetSites(action) {
	try {
		let response = yield call(Api.getAllSites, action.payload);
		if (response.success === 1) {
			yield put(communitySaveSites(response.data));
		}
	} catch (error) {
		// ignore
	}
}

function* handleCommunityGetEducationInstitute(action) {
	try {
		let response = yield call(Api.getEducationInstitute, action.payload);
		console.log("response of education", response);
		if (response.success === 1) {
			yield put(communitySaveEducationInstitute(response.data));
		}
	} catch (error) {
		// ignore
		console.log("error of education", error);
	}
}
