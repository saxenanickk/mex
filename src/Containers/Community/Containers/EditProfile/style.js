import React from "react";
import { StyleSheet, Dimensions, Platform } from "react-native";
const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#eeeff5",
		padding: 10
	},
	header: {
		width: width,
		height: height / 12,
		backgroundColor: "transparent",
		paddingHorizontal: width / 30,
		flexDirection: "row",
		alignItems: "center"
	},
	dataContainer: {
		backgroundColor: "#ffffff",
		borderRadius: 10,
		padding: 10,
		flex: 1,
		...Platform.select({
			android: { elevation: 2 },
			ios: {
				shadowOffset: { width: 0, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	supportTextButton: {
		backgroundColor: "#000",
		flexDirection: "row",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		paddingVertical: height / 150,
		borderRadius: width / 20,
		justifyContent: "flex-start",
		paddingHorizontal: width / 50,
		alignItems: "center"
	},
	supportText: {
		color: "#fff",
		marginRight: width / 30
	},
	backButton: {
		width: width / 6,
		height: height / 12,
		justifyContent: "center"
	},
	profileEditSection: {
		flex: 1,
		alignItems: "center",
		paddingVertical: height / 30
	},
	userImage: {
		width: width / 5,
		height: width / 5,
		borderRadius: width / 10
	},
	noUserImage: {
		width: width / 5,
		height: width / 5,
		borderRadius: width / 10,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#eeeff5"
	},
	changeImage: {
		position: "absolute",
		bottom: 1,
		left: width / 15,
		width: width / 15,
		height: width / 15,
		borderRadius: width / 30,
		backgroundColor: "#eeeff5",
		justifyContent: "center",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 2 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	imageView: {
		height: width / 4.4
	},
	userProfileImage: {
		paddingVertical: 10
	},
	updateButton: {
		paddingHorizontal: width / 30,
		paddingVertical: height / 40,
		marginTop: height / 7,
		backgroundColor: "#eeeff5",
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 30
	},
	commentField: {
		width: width * 0.84,
		alignSelf: "center",
		marginTop: height / 50,
		height: height / 15,
		justifyContent: "center",
		paddingHorizontal: width / 30,
		backgroundColor: "#eeeff5",
		borderRadius: width / 50
	},
	rowView: {
		flexDirection: "row",
		alignItems: "center"
	},
	saveCommentButton: {
		borderWidth: 0,
		paddingVertical: height / 50,
		paddingHorizontal: width / 30
	},
	commentTextInput: {
		width: width / 1.5
	},
	genderSection: {
		width: width * 0.84,
		alignSelf: "center",
		marginTop: height / 50,
		flexDirection: "row",
		flexWrap: "wrap",
		height: height / 15,
		justifyContent: "space-between",
		alignItems: "center",
		paddingHorizontal: width / 30,
		borderRadius: width / 50
	},
	genderButton: {
		flexDirection: "row",
		alignItems: "center"
	},
	genderText: {
		marginLeft: width / 30
	},
	progressContainer: {
		height: height,
		justifyContent: "center",
		alignItems: "center",
		position: "absolute",
		top: 0,
		left: 0,
		bottom: 0,
		right: 0,
		backgroundColor: "rgba(0,0,0,0.5)"
	},
	profileViewSection: {
		width: width * 0.92,
		height: height / 4,
		alignSelf: "center",
		backgroundColor: "#fff",
		marginTop: height / 8,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	editProfileButtonView: {
		alignItems: "flex-end",
		width: width * 0.92,
		paddingHorizontal: width / 30,
		paddingVertical: height / 90
	},
	editProfileButton: {
		justifyContent: "flex-start",
		alignItems: "flex-end",
		width: width / 5,
		height: height / 20
	},
	profileInfoSection: {
		width: width * 0.92,
		alignItems: "center"
	},
	textStyle: {
		width: width / 1.2,
		textAlign: "center"
	},
	bottomButtonView: {
		position: "absolute",
		bottom: 0,
		width: width * 0.92,
		flexDirection: "row",
		justifyContent: "space-between"
	},
	followButton: {
		width: width * 0.46,
		height: height / 20,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		borderWidth: 1,
		borderColor: "#eeeff5"
	},
	followTextStyle: {
		marginLeft: width / 50,
		width: width / 5
	},
	userImageView: {
		justifyContent: "center",
		alignItems: "center"
	},
	rowAlign: {
		flex: 1,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "flex-start"
	},
	labelStyle: {
		width: "40%"
	},
	inputStyle: {
		borderBottomWidth: 0.5,
		width: "60%"
	},
	columnAlign: {
		flex: 1,
		alignItems: "center",
		justifyContent: "flex-start"
	}
});
