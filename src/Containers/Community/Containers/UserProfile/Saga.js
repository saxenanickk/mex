// @ts-check
import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api";
import { communitySaveError } from "../../Saga";

//  Constants
export const COMMUNITY_GET_MY_PROFILE = "COMMUNITY_GET_MY_PROFILE";
export const COMMUNITY_SAVE_MY_PROFILE = "COMMUNITY_SAVE_MY_PROFILE";
export const COMMUNTITY_UPDATE_USER_PROFILE = "COMMUNTITY_UPDATE_USER_PROFILE";
export const COMMUNTITY_UPDATE_USER_PROFILE_ERROR =
	"COMMUNTITY_UPDATE_USER_PROFILE_ERROR";
export const COMMUNITY_GET_ALL_FOLLOWER = "COMMUNITY_GET_ALL_FOLLOWER";
export const COMMUNITY_SAVE_ALL_FOLLOWER = "COMMUNITY_SAVE_ALL_FOLLOWER";
export const COMMUNITY_GET_ALL_FOLLOWING = "COMMUNITY_GET_ALL_FOLLOWING";
export const COMMUNITY_SAVE_ALL_FOLLOWING = "COMMUNITY_SAVE_ALL_FOLLOWING";
export const COMMUNITY_GET_SECOND_USER_PROFILE =
	"COMMUNITY_GET_SECOND_USER_PROFILE";
export const COMMUNITY_SAVE_SECOND_USER_PROFILE =
	"COMMUNITY_SAVE_SECOND_USER_PROFILE";
export const COMMUNITY_UPDATE_USER_SUCCESS = "COMMUNITY_UPDATE_USER_SUCCESS";
//  Action Creators
export const communityGetMyProfile = payload => ({
	type: COMMUNITY_GET_MY_PROFILE,
	payload
});
export const communitySaveMyProfile = payload => ({
	type: COMMUNITY_SAVE_MY_PROFILE,
	payload
});
export const communityUpdateUserProfile = payload => ({
	type: COMMUNTITY_UPDATE_USER_PROFILE,
	payload
});
export const communityUpdateUserProfileError = payload => ({
	type: COMMUNTITY_UPDATE_USER_PROFILE_ERROR,
	payload
});
export const communityGetAllFollower = payload => ({
	type: COMMUNITY_GET_ALL_FOLLOWER,
	payload
});
export const communitySaveAllFollower = payload => ({
	type: COMMUNITY_SAVE_ALL_FOLLOWER,
	payload
});
export const communityGetAllFollowing = payload => ({
	type: COMMUNITY_GET_ALL_FOLLOWING,
	payload
});
export const communitySaveAllFollowing = payload => ({
	type: COMMUNITY_SAVE_ALL_FOLLOWING,
	payload
});
export const communityGetSecondUserProfile = payload => ({
	type: COMMUNITY_GET_SECOND_USER_PROFILE,
	payload
});
export const communitySaveSecondUserProfile = payload => ({
	type: COMMUNITY_SAVE_SECOND_USER_PROFILE,
	payload
});
export const communityUpdateUserSuccess = payload => ({
	type: COMMUNITY_UPDATE_USER_SUCCESS,
	payload
});
//  Saga
export function* communityProfileSaga(dispatch) {
	yield takeLatest(COMMUNITY_GET_MY_PROFILE, handleCommunityGetMyProfile);
	yield takeLatest(
		COMMUNTITY_UPDATE_USER_PROFILE,
		handleCommunityUpdateUserProfile
	);
	yield takeLatest(COMMUNITY_GET_ALL_FOLLOWER, handleCommunityGetAllFollower);
	yield takeLatest(COMMUNITY_GET_ALL_FOLLOWING, handleCommunityGetAllFollowing);
	yield takeLatest(
		COMMUNITY_GET_SECOND_USER_PROFILE,
		handleCommunityGetSecondUserProfile
	);
}

//  Handlers
function* handleCommunityGetMyProfile(action) {
	try {
		let response = yield call(Api.getUserProfile, action.payload);
		console.log("response", response);
		if (response.success === 1) {
			yield put(communitySaveMyProfile(response.data));
		} else {
			throw new Error("unable to get user profile");
		}
	} catch (error) {
		console.log(error, "error");
		yield put(
			communitySaveError({
				errorScreen: "Splash",
				errorMessage: "Unable to get User Profile",
				isError: true
			})
		);
	}
}

function* handleCommunityUpdateUserProfile(action) {
	try {
		let photoUploadResponse = null;
		if (action.payload.photo !== null && action.payload.photo !== undefined) {
			photoUploadResponse = yield call(Api.uploadProfilePic, action.payload);
		}
		const profileInfoUpdate = yield call(Api.updateUserProfile, action.payload);
		if (profileInfoUpdate.success === 1) {
			yield put(communityUpdateUserSuccess(true));
			yield put(communityGetMyProfile(action.payload));
		} else {
			throw new Error("Unable to update profile");
		}
	} catch (error) {
		yield put(communityUpdateUserProfileError(true));
		yield put(communityUpdateUserSuccess(false));
		yield put(
			communitySaveError({
				errorScreen: "EditProfile",
				errorMessage: "Unable to save your profile",
				isError: true
			})
		);
	}
}

function* handleCommunityGetAllFollower(action) {
	try {
		let response = yield call(Api.getAllFollower, action.payload);
		if (response.success === 1) {
			yield put(communitySaveAllFollower(response.data));
		} else {
			throw new Error("Unable to fetch");
		}
	} catch (error) {
		yield put(
			communitySaveError({
				errorScreen: "UserListView",
				errorMessage: "OOps,Something went wrong, Please try again later",
				isError: true
			})
		);
	}
}

function* handleCommunityGetAllFollowing(action) {
	try {
		let response = yield call(Api.getAllFollowing, action.payload);
		if (response.success === 1) {
			yield put(communitySaveAllFollowing(response.data));
		} else {
			throw new Error("Unable to fetch");
		}
	} catch (error) {
		yield put(
			communitySaveError({
				errorScreen: "UserListView",
				errorMessage: "OOps,Something went wrong, Please try again later",
				isError: true
			})
		);
	}
}

function* handleCommunityGetSecondUserProfile(action) {
	try {
		let response = yield call(Api.getUserProfile, action.payload);
		console.log("response", response);
		if (response.success === 1) {
			yield put(communitySaveSecondUserProfile(response.data));
		} else {
			throw new Error("unable to get user profile");
		}
	} catch (error) {
		console.log(error, "error");
		yield put(
			communitySaveError({
				errorScreen: "UserProfile",
				errorMessage: "Unable to get User Profile",
				isError: true
			})
		);
	}
}
