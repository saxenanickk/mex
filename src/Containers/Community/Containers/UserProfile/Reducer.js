import {
	COMMUNITY_SAVE_MY_PROFILE,
	COMMUNITY_SAVE_SECOND_USER_PROFILE,
	COMMUNITY_SAVE_ALL_FOLLOWER,
	COMMUNITY_SAVE_ALL_FOLLOWING,
	COMMUNTITY_UPDATE_USER_PROFILE,
	COMMUNTITY_UPDATE_USER_PROFILE_ERROR,
	COMMUNITY_UPDATE_USER_SUCCESS
} from "./Saga";

const initialState = {
	isMyProfileLoading: false,
	myProfile: null,
	secondUserProfile: null,
	isProfileUpdateSuccess: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case COMMUNTITY_UPDATE_USER_PROFILE:
			return {
				...state,
				isMyProfileLoading: true
			};
		case COMMUNTITY_UPDATE_USER_PROFILE_ERROR:
			return {
				...state,
				isMyProfileLoading: false
			};
		case COMMUNITY_SAVE_MY_PROFILE:
			return {
				...state,
				myProfile: action.payload,
				isMyProfileLoading: false
			};
		case COMMUNITY_SAVE_SECOND_USER_PROFILE:
			return {
				...state,
				secondUserProfile: action.payload
			};
		case COMMUNITY_SAVE_ALL_FOLLOWER:
			return {
				...state,
				myProfile: { ...state.myProfile, ...{ followers: action.payload } }
			};
		case COMMUNITY_SAVE_ALL_FOLLOWING:
			return {
				...state,
				myProfile: { ...state.myProfile, ...{ following: action.payload } }
			};
		case COMMUNITY_UPDATE_USER_SUCCESS:
			return {
				...state,
				isProfileUpdateSuccess: action.payload
			};
		default:
			return state;
	}
};
