import { StyleSheet, Dimensions, Platform } from "react-native";
const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#eeeff5"
	},
	header: {
		width: width,
		height: height / 12,
		backgroundColor: "#ffffff",
		paddingHorizontal: width / 30,
		flexDirection: "row",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	nameAndMatchText: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	userNameText: {
		fontSize: 16,
		flex: 1
		// width: width / 2.4
	},
	dataContainer: {
		backgroundColor: "#ffffff",
		paddingBottom: 10,
		flex: 1,
		flexGrow: 1,
		...Platform.select({
			android: { elevation: 2 },
			ios: {
				shadowOffset: { width: 0, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	userInfoText: {
		width: width / 1.1,
		textAlign: "center"
	},
	backButton: {
		width: width / 5,
		height: height / 15,
		justifyContent: "center",
		paddingHorizontal: width / 25
	},
	userProfileView: {
		height: height / 15,
		justifyContent: "center",
		marginLeft: width / 30,
		borderBottomWidth: 1,
		borderBottomColor: "#ececec"
	},
	userImage: {
		width: width / 5,
		height: width / 5,
		borderRadius: width / 10
	},
	noUserImage: {
		width: width / 5,
		height: width / 5,
		borderRadius: width / 10,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#eeeff5"
	},
	userImageView: {
		justifyContent: "center",
		alignItems: "center"
	},
	userInfoSection: {
		marginTop: height / 20,
		alignItems: "center"
	},
	emailSection: {
		marginTop: height / 80
	},
	followUnfollowButton: {
		marginTop: height / 2.5,
		width: width / 3,
		paddingVertical: height / 90,
		borderRadius: width / 50,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "#f4f4f4"
	},
	followUnfollowText: {
		textAlign: "center"
	},
	userPrimaryInfoHeader: {
		flexDirection: "row",
		paddingBottom: height / 40,
		paddingHorizontal: width / 20
	},
	changeImage: {
		position: "absolute",
		bottom: -height / 90,
		left: width / 14,
		width: width / 15,
		height: width / 15,
		borderRadius: width / 30,
		backgroundColor: "#eeeff5",
		justifyContent: "center",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 2 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	professionalInfoSection: {
		flex: 1,
		paddingLeft: width / 30,
		justifyContent: "center"
	},
	connectedMemberCountSection: {
		borderRadius: width / 30,
		paddingVertical: height / 40,
		backgroundColor: "#FFFFFF",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		marginHorizontal: width / 20,
		marginVertical: height / 60,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	connectedMemberCountView: {
		flex: 1,
		paddingHorizontal: width / 30,
		justifyContent: "center",
		alignItems: "center"
	},
	connectedMemberCount: {
		borderRightWidth: 2,
		borderColor: "#EFF1F4"
	},
	editButton: {
		width: width / 5,
		height: height / 25,
		borderColor: "#2C98F0",
		borderWidth: 1.5,
		borderRadius: width / 10,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "flex-end",
		marginLeft: width / 25
	},
	followText: {
		width: width / 5.5,
		textAlign: "center"
	},
	clubJoinedSection: {
		width: width * 0.92,
		alignSelf: "center",
		paddingVertical: height / 60
	},
	imageView: {
		width: width / 7,
		height: width / 7,
		borderRadius: width / 14,
		backgroundColor: "#fff",
		justifyContent: "center",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 3 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.2
			}
		})
	},
	clubImage: {
		width: width / 12,
		height: width / 12
	},
	joinedClubSection: {
		width: width * 0.92,
		flexDirection: "row",
		alignItems: "center",
		flexWrap: "wrap"
	},
	joinedClubSingleView: {
		width: width * 0.23,
		paddingVertical: height / 90
	},
	joinClubButton: {
		borderWidth: 1,
		borderColor: "#8cb9e0"
	},
	manageSkillsSection: {
		width: width * 0.92,
		alignSelf: "center"
	},
	connectedMemberHeaderText: {
		width: width / 4,
		borderWidth: 1
	},
	hrLine: {
		height: height / 50,
		width: width,
		backgroundColor: "#EFF1F4"
	},
	editIcon: {
		paddingHorizontal: width / 50,
		borderRadius: height / 90,
		paddingVertical: width / 200
	},
	box: {
		flex: 1
	},
	communityLoader: {
		height: height,
		justifyContent: "center",
		alignItems: "center"
	},
	findConnection: {
		padding: height / 80,
		backgroundColor: "#2C98F0",
		justifyContent: "center",
		alignItems: "center",
		marginVertical: height / 100,
		borderRadius: width / 10,
		marginHorizontal: width / 40
	},
	labelTextBox: { width: width / 2.3 }
});

export const grey = {
	shade1: "#818590",
	shade2: "#727272",
	shade3: "#5e615e",
	shade4: "#474747"
};
export const BLACK_COLOR = "#000";
export const WHITE_COLOR = "#ffffff";
export const USER_ACTIVITIES_TEXT_HIEGHT = height / 60;
export const USER_INFO_TEXT_HEIGHT = height / 55;
