import React, { Component } from "react";
import {
	View,
	Dimensions,
	Image,
	TouchableOpacity,
	Alert,
	ScrollView
} from "react-native";
import { connect } from "react-redux";
import _ from "lodash";
import {
	styles,
	grey,
	BLACK_COLOR,
	WHITE_COLOR,
	USER_ACTIVITIES_TEXT_HIEGHT,
	USER_INFO_TEXT_HEIGHT
} from "./style";
import I18n from "../../Assets/Strings/i18n";
import { Icon } from "../../../../Components";
import {
	CommunityTextMedium,
	CommunityTextBold
} from "../../Components/CommunityText";
import { communityFollowUnfollowUser } from "../PostDetail/Saga";
import CommunityLoader from "../../Components/CommunityLoader";
import { communityResetError } from "../../Saga";
import {
	communityGetSecondUserProfile,
	communitySaveSecondUserProfile,
	communityGetMyProfile
} from "./Saga";
import AboutAndPosts from "./AboutAndPosts";
import About from "./AboutAndPosts/About";
import {
	communityGetMyPost,
	communityRefreshMyPost
} from "../EditProfile/Saga";

const { width, height } = Dimensions.get("window");

const UserProfileViewComponent = ({ isMyProfile, children, style }) => {
	if (isMyProfile) {
		return <View style={styles.dataContainer}>{children}</View>;
	} else {
		return (
			<ScrollView
				style={styles.dataContainer}
				showsVerticalScrollIndicator={false}>
				{children}
			</ScrollView>
		);
	}
};

const SKY_BLUE_COLOR = "#2C98F0";
class UserProfile extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isFollowing: false
		};
		this.tabRef = React.createRef();

		const { route } = props;

		const { userInfo } = route && route.params ? route.params : {};

		this.userInfo = userInfo || null;
	}

	fetchUserInfo = () => {
		if (this.userInfo && this.userInfo.user_id !== undefined) {
			this.props.dispatch(
				communityGetSecondUserProfile({
					userId: this.userInfo.user_id,
					appToken: this.props.appToken
				})
			);
		} else {
			this.props.dispatch(
				communityGetMyProfile({
					appToken: this.props.appToken
				})
			);
		}
	};

	shouldComponentUpdate(props, state) {
		const {
			error: prevError,
			secondUserProfile: prevSecondUserProfile
		} = props;
		const {
			error: nextError,
			navigation,
			dispatch,
			secondUserProfile: nextSecondUserProfile
		} = this.props;

		if (
			prevError !== nextError &&
			prevError.isError &&
			prevError.errorScreen === "UserProfile"
		) {
			Alert.alert(I18n.t("error"), props.error.errorMessage);
			dispatch(communityResetError());
			navigation.goBack();
		}
		if (
			prevSecondUserProfile &&
			prevSecondUserProfile !== nextSecondUserProfile
		) {
			this.setState({ isFollowing: prevSecondUserProfile.is_Following });
		}
		return true;
	}

	followUnfollowUser(profileToDisplay) {
		const { user_id, name } = profileToDisplay;
		const { dispatch, appToken } = this.props;
		if (user_id) {
			dispatch(
				communityFollowUnfollowUser({
					appToken: appToken,
					userId: user_id,
					isFollowing: this.state.isFollowing,
					userName: name
				})
			);
			this.setState({ isFollowing: !this.state.isFollowing });
		}
	}

	componentDidMount() {
		const { navigation, dispatch } = this.props;

		this._unsubscribeFocus = navigation.addListener("focus", () => {
			this.fetchUserInfo();
		});

		this._unsubscribeBlur = navigation.addListener("blur", () => {
			dispatch(communitySaveSecondUserProfile(null));
		});
	}

	componentWillUnmount = () => {
		this._unsubscribeFocus();
		this._unsubscribeBlur();
		this.props.dispatch(communitySaveSecondUserProfile(null));
	};

	_renderUserImage = userProfile => {
		try {
			let image = userProfile.image;
			if (this.hasUserProfileImageUrl(image)) {
				return (
					<View style={styles.userImageView}>
						<Image source={{ uri: image }} style={styles.userImage} />
					</View>
				);
			} else {
				throw new Error("User profile pic not found");
			}
		} catch (error) {
			return (
				<View style={styles.noUserImage}>
					<Icon
						iconType={"ionicon"}
						iconSize={width / 10}
						iconName={"ios-person"}
						iconColor={grey.shade2}
					/>
				</View>
			);
		}
	};

	handlePostsPress = () => {
		// 1 is harcoded index for posts
		const { dispatch, appToken } = this.props;
		this.tabRef.current.changeTab(1);
		dispatch(communityRefreshMyPost(null));
		dispatch(communityGetMyPost({ appToken }));
	};

	handleGoBack = () => {
		if (!this.props.navigation.goBack) {
			this.props.navigation.navigate("Home");
		} else {
			this.props.navigation.goBack();
		}
	};

	getTotalMatchingttribute = profileToDisplay =>
		profileToDisplay
			? Object.keys(profileToDisplay.matched).reduce(
					(key, total_matched_attribute) =>
						(total_matched_attribute += profileToDisplay.matched[key].length),
					0
			  )
			: 0;

	checkValidityForConnect = () => {
		try {
			if (
				this.isHobbiesAndInterestEmpty() ||
				this.isUserProfileAboutEmpty() ||
				this.isUserNameEmpty()
			) {
				return false;
			} else {
				return true;
			}
		} catch (error) {
			console.log("error while check valid", error);
			return false;
		}
	};

	handleMexconnectPress = () => {
		const { navigation, myProfile } = this.props;
		if (myProfile.linkedin !== null) {
			if (this.checkValidityForConnect()) {
				navigation.navigate("ConnectionsMatch");
			} else {
				this.props.navigation.navigate("EditProfile", { isUseConnect: true });
			}
		} else {
			navigation.navigate("ConnectWelcome");
		}
	};

	hasProfileImageUrl = () =>
		_.get(this.props, "myProfile.image", "")
			.trim()
			.startsWith("http");

	isUserNameEmpty = () => _.get(this.props, "myProfile.name", "").trim() === "";

	isUserProfileAboutEmpty = () =>
		_.get(this.props, "myProfile.about", "").trim().length <= 30;

	isHobbiesAndInterestEmpty = () =>
		_.get(this.props, "myProfile.hobbies_interests", []).length === 0;

	hasUserProfileImageUrl = image =>
		image !== null &&
		image !== undefined &&
		image.trim() !== "" &&
		image.startsWith("http");

	handleNavigate = () => {
		const param = { isCommunity: true, isNavigate: true };
		this.props.navigation.navigate("EditProfile", { ...param });
	};

	render() {
		const isUserInfoAvailable = this.userInfo !== null;
		const { myProfile, secondUserProfile, navigation } = this.props;
		const { navigate } = navigation;
		const profileToDisplay = this.userInfo ? secondUserProfile : myProfile;
		return (
			<View style={styles.container}>
				<UserProfileViewComponent
					style={styles.dataContainer}
					isMyProfile={this.userInfo === null}>
					{this._renderBackButton()}
					{profileToDisplay ? (
						<View style={styles.box}>
							<View style={styles.userPrimaryInfoHeader}>
								{this._renderUserImage(profileToDisplay)}
								<View style={styles.professionalInfoSection}>
									<View style={[styles.nameAndMatchText]}>
										{this._renderUserName(profileToDisplay)}
										{this._renderEditOrFollowButton(profileToDisplay)}
									</View>
									{this._renderOrganisationName(profileToDisplay)}
									{this._renderSiteName(profileToDisplay)}
								</View>
							</View>
							<View style={styles.connectedMemberCountSection}>
								<TouchableOpacity
									style={[
										styles.connectedMemberCountView,
										styles.connectedMemberCount
									]}
									onPress={this.handlePostsPress}
									disabled={isUserInfoAvailable}
									activeOpacity={1}>
									{this._renderUserActivityLabel(
										I18n.t("com_posts", {
											count: profileToDisplay.number_of_post
										}),

										profileToDisplay.number_of_post
									)}
								</TouchableOpacity>
								<TouchableOpacity
									style={[
										styles.connectedMemberCountView,
										styles.connectedMemberCount
									]}
									disabled={isUserInfoAvailable}
									onPress={() =>
										navigate("UserListView", { isFollowing: false })
									}>
									{this._renderUserActivityLabel(
										I18n.t("com_follower", {
											count: profileToDisplay
												? profileToDisplay.number_of_follower
												: 0
										}),
										profileToDisplay.number_of_follower
									)}
								</TouchableOpacity>
								<TouchableOpacity
									style={styles.connectedMemberCountView}
									disabled={isUserInfoAvailable}
									onPress={() =>
										navigate("UserListView", { isFollowing: true })
									}>
									{this._renderUserActivityLabel(
										I18n.t("com_following"),
										profileToDisplay.number_of_following
									)}
								</TouchableOpacity>
							</View>
							{this.userInfo ? (
								<About
									profileToDisplay={profileToDisplay}
									navigation={this.props.navigation}
								/>
							) : (
								<AboutAndPosts
									ref={this.tabRef}
									profileToDisplay={profileToDisplay}
									isOtherUser={isUserInfoAvailable}
									navigation={this.props.navigation}
								/>
							)}
						</View>
					) : (
						<View style={styles.communityLoader}>
							<CommunityLoader />
						</View>
					)}
				</UserProfileViewComponent>
				{this._renderFindConnections()}
			</View>
		);
	}

	_renderSiteName(profileToDisplay) {
		return profileToDisplay.current_organisation ? (
			<View style={styles.labelTextBox}>
				<CommunityTextMedium
					color={grey.shade1}
					numberOfLines={1}
					size={USER_INFO_TEXT_HEIGHT}>
					{profileToDisplay.site_info.name}
				</CommunityTextMedium>
			</View>
		) : null;
	}

	_renderOrganisationName(profileToDisplay) {
		return profileToDisplay.current_organisation ? (
			<View style={styles.labelTextBox}>
				<CommunityTextMedium
					color={grey.shade1}
					numberOfLines={1}
					size={USER_INFO_TEXT_HEIGHT}>
					{profileToDisplay.current_organisation}
				</CommunityTextMedium>
			</View>
		) : null;
	}

	_renderUserName(profileToDisplay) {
		return (
			<CommunityTextBold
				color={BLACK_COLOR}
				numberOfLines={1}
				style={styles.userNameText}>
				{profileToDisplay.name}
			</CommunityTextBold>
		);
	}

	_renderBackButton() {
		return (
			<TouchableOpacity
				style={styles.backButton}
				onPress={() => this.handleGoBack()}>
				<Icon
					iconType={"feather"}
					iconSize={width / 18}
					iconName={"arrow-left"}
					iconColor={BLACK_COLOR}
				/>
			</TouchableOpacity>
		);
	}

	_renderFindConnections() {
		return !this.userInfo ? (
			<TouchableOpacity
				style={styles.findConnection}
				onPress={() => this.handleMexconnectPress()}>
				<CommunityTextMedium color={WHITE_COLOR} size={height / 50}>
					{I18n.t("com_find_connections")}
				</CommunityTextMedium>
			</TouchableOpacity>
		) : null;
	}

	_renderUserActivityLabel(label, totalActivities) {
		return (
			<>
				<CommunityTextBold>{totalActivities}</CommunityTextBold>
				<CommunityTextMedium
					color={grey.shade3}
					numberOfLines={1}
					size={USER_ACTIVITIES_TEXT_HIEGHT}>
					{label}
				</CommunityTextMedium>
			</>
		);
	}

	_renderEditOrFollowButton(profileToDisplay) {
		return this.userInfo ? (
			<TouchableOpacity
				style={styles.editButton}
				onPress={() => this.followUnfollowUser(profileToDisplay)}>
				<CommunityTextBold
					color={grey.shade4}
					style={styles.followText}
					numberOfLines={1}
					size={height / 55}>
					{this.state.isFollowing
						? I18n.t("com_following")
						: I18n.t("com_follow")}
				</CommunityTextBold>
			</TouchableOpacity>
		) : (
			<TouchableOpacity style={styles.editIcon} onPress={this.handleNavigate}>
				<Icon
					iconType={"feather"}
					iconSize={width / 18}
					iconName={"edit"}
					iconColor={SKY_BLUE_COLOR}
				/>
			</TouchableOpacity>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: _.get(state, "appToken.token", ""),
		error: _.get(state, "community.error", {}),
		myProfile: _.get(state, "community.profile.myProfile", {}),
		secondUserProfile: _.get(state, "community.profile.secondUserProfile", {})
	};
}

export default connect(mapStateToProps)(UserProfile);
