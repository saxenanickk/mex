import React, { Component } from "react";
import { View, Dimensions, FlatList, Alert } from "react-native";
import { connect } from "react-redux";
import PostCard from "../../../Components/PostCard";
import CommunityLoader from "../../../Components/CommunityLoader";
import { EmptyView } from "../../../../../Components";
import I18n from "../../../Assets/Strings/i18n";
import {
	communityGetMyPost,
	communityRefreshMyPost
} from "../../EditProfile/Saga";
import { styles } from "./style";
import { communityResetError } from "../../../Saga";

const { width } = Dimensions.get("window");
class Posts extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true
		};
	}

	shouldComponentUpdate(props, state) {
		if (
			props.error &&
			props.error !== this.props.error &&
			props.error.isError &&
			props.error.errorScreen === "UserProfile"
		) {
			Alert.alert(I18n.t("error"), "Something went wrong");
			this.props.dispatch(communityResetError());
			this.setState({ isLoading: false });
		}
		if (props.myPosts && props.myPosts !== this.props.myPosts) {
			this.setState({ isLoading: false });
		}
		if (props.myPosts === null && props.myPosts !== this.props.myPosts) {
			this.setState({ isLoading: true });
		}
		return true;
	}

	componentWillUnmount() {
		this.props.dispatch(communityRefreshMyPost(null));
	}

	_renderItem = ({ item, index }) => {
		if (item !== null) {
			return (
				<PostCard
					postDetail={item}
					isClubPost={false}
					isMyPost={true}
					navigation={this.props.navigation}
				/>
			);
		} else {
			return null;
		}
	};

	_listEmptyComponent = () => (
		<EmptyView
			noRecordFoundMesage={I18n.t("com_no_post_found")}
			sorryMessage={I18n.t("com_sorry")}
			refreshButton={styles.refreshButton}
			onRefresh={() => {
				this.props.dispatch(
					communityGetMyPost({
						appToken: this.props.appToken
					})
				);
				this.setState({ isLoading: true });
			}}
			containerStyle={styles.emptyViewContainer}
		/>
	);

	handleRefresh = () => {
		this.props.dispatch(
			communityGetMyPost({
				appToken: this.props.appToken,
				isRefresh: true
			})
		);
		this.setState({ isLoading: true });
	};

	handleOnEndReached = () => {
		if (this.props.myPostNextOffset !== null) {
			this.props.dispatch(
				communityGetMyPost({
					appToken: this.props.appToken,
					nextOffset: this.props.myPostNextOffset
				})
			);
		}
	};

	renderFooterComponent = () => {
		if (this.props.myPostNextOffset === null) {
			// Write proper text for posts end reached
			return null;
		}
		return <CommunityLoader />;
	};
	render() {
		const { myPosts } = this.props;
		return (
			<View style={styles.postContainer}>
				{myPosts !== null ? (
					<FlatList
						data={myPosts}
						contentContainerStyle={styles.flatlistContentContainer}
						showsVerticalScrollIndicator={false}
						style={styles.listGap}
						keyExtractor={(item, index) => index.toString()}
						extraData={this.state}
						renderItem={this._renderItem}
						ListEmptyComponent={this._listEmptyComponent}
						ListFooterComponent={this.renderFooterComponent}
						onRefresh={this.handleRefresh}
						refreshing={false}
						onEndReachedThreshold={0.8}
						onEndReached={this.handleOnEndReached}
					/>
				) : (
					<View style={styles.progressContainer}>
						{this.state.isLoading && <CommunityLoader size={width / 15} />}
					</View>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		error: state.community && state.community.error,
		myPosts: state.community && state.community.myProfileReducer.myPosts,
		myPostNextOffset:
			state.community && state.community.myProfileReducer.myPostNextOffset,
		currentDisplayScreen:
			state.community && state.community.error.currentDisplayScreen
	};
}

export default connect(mapStateToProps)(Posts);
