import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	Animated,
	Easing,
	Modal,
	Platform
} from "react-native";
import { styles } from "./style";
import { CommunityTextBold } from "../../../Components/CommunityText";
import { QRCodeReader, Icon } from "../../../../../Components";
import CommunityLoader from "../../../Components/CommunityLoader";
const { height, width } = Dimensions.get("window");

export default class QRScanner extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return (
			<Modal
				visible={this.props.openScanner}
				onRequestClose={() => this.props.closeScanner()}>
				<View style={{ flex: 1 }}>
					{this.props.otpFetching ? (
						<View
							style={{
								height: height,
								justifyContent: "center",
								alignItems: "center"
							}}>
							<CommunityLoader />
						</View>
					) : (
						<View
							style={{
								...Platform.select({
									ios: {
										position: "absolute",
										top: 0,
										bottom: 0,
										left: 0,
										right: 0
									},
									android: { flex: 1 }
								})
							}}>
							<QRCodeReader onRead={this.props.handleBarCodeRead} />
							<TouchableOpacity
								onPress={() => this.props.closeScanner()}
								style={[
									styles.closeCameraButton,
									{
										position: "absolute",
										top: height / 1.2,
										alignSelf: "center"
									}
								]}>
								<CommunityTextBold color={"#fff"}>{"CLOSE"}</CommunityTextBold>
							</TouchableOpacity>
						</View>
					)}
				</View>
			</Modal>
		);
	}
}
