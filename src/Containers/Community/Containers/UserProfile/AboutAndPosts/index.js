import React, { Component } from "react";
import { View, Dimensions } from "react-native";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { Icon } from "../../../../../Components";
import About from "./About";
import Posts from "./Posts";
import Setting from "./Setting";
import { styles, FOCUSED_COLOR, UNFOCUSED_COLOR } from "./style";

import { communitySaveCurrentDisplayScreen } from "../../../Saga";
import {
	communityGetMyPost,
	communityRefreshMyPost
} from "../../EditProfile/Saga";
import { CommunityTextBold } from "../../../Components/CommunityText";
import { connect } from "react-redux";

const SCREEN = {
	About: "About",
	Setting: "Setting",
	Posts: "Posts"
};

const { width, height } = Dimensions.get("window");

class AboutAndPosts extends Component {
	state = {
		index: 0,
		routes: this.props.isOtherUser
			? [{ key: SCREEN.About }]
			: [{ key: SCREEN.About }, { key: SCREEN.Posts }, { key: SCREEN.Setting }]
	};

	_handleIndexChange = index => {
		if (index === 1) {
			this.props.dispatch(
				communityGetMyPost({
					appToken: this.props.appToken
				})
			);
			if (this.props.currentDisplayScreen !== "UserProfile") {
				this.props.dispatch(communitySaveCurrentDisplayScreen("UserProfile"));
			}
		} else {
			this.props.dispatch(communitySaveCurrentDisplayScreen(null));
			this.props.dispatch(communityRefreshMyPost(null));
		}
		this.setState({ index });
	};

	_renderIcon = props => {
		const { route } = props;
		const { index, routes } = this.state;
		const selectedRoute = routes[index];
		const color =
			selectedRoute.key === route.key ? FOCUSED_COLOR : UNFOCUSED_COLOR;

		switch (route.key) {
			case SCREEN.About:
				return (
					<View style={[styles.tabHeaderContainer(color)]}>
						<Icon
							iconType={"icomoon"}
							iconName={"profile"}
							iconSize={height / 40}
							iconColor={color}
							style={styles.tabHeaderImage(color)}
						/>
						<CommunityTextBold color={color}>Profile</CommunityTextBold>
					</View>
				);
			case SCREEN.Posts:
				return (
					<View style={styles.tabHeaderContainer(color)}>
						<Icon
							iconType={"ionicon"}
							iconName={"ios-images"}
							iconSize={height / 40}
							iconColor={color}
							style={styles.tabHeaderImage(color)}
						/>
						<CommunityTextBold color={color}>Posts</CommunityTextBold>
					</View>
				);
			case SCREEN.Setting:
				return (
					<View style={[styles.tabHeaderContainer(color)]}>
						<Icon
							iconType={"ionicon"}
							iconName={"ios-link"}
							iconSize={height / 40}
							iconColor={color}
							style={styles.tabHeaderImage(color)}
						/>
						<CommunityTextBold color={color}>More</CommunityTextBold>
					</View>
				);
			default:
				return null;
		}
	};

	_renderHeader = props => (
		<TabBar
			{...props}
			layout={{
				width: width,
				height,
				measured: true
			}}
			renderIcon={this._renderIcon}
			style={styles.background}
			tabStyle={styles.tabStyle}
			indicatorStyle={{
				backgroundColor: FOCUSED_COLOR
			}}
		/>
	);

	changeTab = index => {
		this.setState({ index });
	};

	_renderScene = route => {
		switch (route.route.key) {
			case SCREEN.About:
				return (
					<About
						navigation={this.props.navigation}
						profileToDisplay={this.props.profileToDisplay}
					/>
				);
			case SCREEN.Posts:
				return <Posts navigation={this.props.navigation} />;
			case SCREEN.Setting:
				return (
					<Setting
						navigation={this.props.navigation}
						profileToDisplay={this.props.profileToDisplay}
					/>
				);
			default:
				return null;
		}
	};
	_renderSingleScene = SceneMap({
		About: () => <About profileToDisplay={this.props.profileToDisplay} />
	});

	render() {
		return (
			<TabView
				navigationState={this.state}
				renderScene={
					this.props.isOtherUser ? this._renderSingleScene : this._renderScene
				}
				renderTabBar={this._renderHeader}
				onIndexChange={this._handleIndexChange}
				initialLayout={{ width: width - 40, height }}
			/>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		currentDisplayScreen:
			state.community && state.community.error.currentDisplayScreen
	};
}

export default connect(
	mapStateToProps,
	null,
	null,
	{ forwardRef: true }
)(AboutAndPosts);
