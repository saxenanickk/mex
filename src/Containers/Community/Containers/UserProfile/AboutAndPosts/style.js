import { StyleSheet, Dimensions, Platform } from "react-native";
import { font_two } from "../../../../../Assets/styles.ios";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff"
	},
	navigatorButton: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	box: { paddingHorizontal: width / 30 },
	buttonStyle: {
		flex: 1,
		borderBottomWidth: 1,
		borderBottomColor: "#f5f5f5",
		justifyContent: "center",
		paddingVertical: height / 60,
		alignItems: "center"
	},
	selectedButtonStyle: {
		flex: 1,
		borderBottomWidth: 2,
		borderBottomColor: "#fb9e80",
		paddingVertical: height / 60,
		justifyContent: "center",
		alignItems: "center"
	},
	selectedText: {
		color: "#000"
	},
	unSelectedtext: {
		color: "#bdc0d0"
	},
	clubJoinedSection: {
		alignSelf: "center",
		justifyContent: "space-between",
		width: width / 1.1,
		paddingTop: height / 50,
		paddingHorizontal: width / 100
	},
	joinedClubSection: {
		flexDirection: "row",
		alignItems: "center",
		flexWrap: "wrap"
	},
	joinedClubSingleView: {
		width: width / 5,
		paddingVertical: height / 90,
		paddingLeft: 2,
		alignItems: "center"
	},
	joinClubButton: {
		borderWidth: 1,
		borderColor: "#8cb9e0"
	},
	manageEducationSection: {
		paddingTop: height / 50,
		marginHorizontal: width / 30
	},
	manageSkillsSection: {
		marginHorizontal: width / 30
	},
	educationLabelText: {
		paddingHorizontal: height / 90
	},
	educationText: {
		paddingHorizontal: height / 90,
		paddingVertical: height * 0.01
	},
	imageView: {
		width: width / 8,
		height: height / 8,
		backgroundColor: "#fff",
		justifyContent: "center",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 3 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.2
			}
		})
	},
	imageViewMatching: { borderWidth: 2, borderColor: "#5da4df" },
	imageViewNotMatching: { borderWidth: 2, borderColor: "#fff" },
	clubImage: {
		width: width / 5.6,
		height: height / 6.7
	},
	progressContainer: {
		height: height / 1.5,
		justifyContent: "center",
		alignItems: "center"
	},
	settingButton: {
		paddingVertical: height / 50,
		paddingHorizontal: width / 45,
		flexDirection: "row",
		alignItems: "center"
	},
	separateLines: {
		height: 1.2,
		backgroundColor: "#EFF1F4"
	},
	cameraContainer: {
		justifyContent: "center",
		alignItems: "center",
		position: "absolute",
		top: 0,
		bottom: 0,
		right: 0,
		left: 0,
		elevation: 4,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		backgroundColor: "#fff"
	},
	camera: {
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "transparent",
		width: width,
		height: height - height / 9
	},
	rectangleContainer: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "transparent"
	},
	barRecatangle: {
		position: "absolute",
		top: height / 4,
		borderColor: "#38AFFE",
		borderWidth: 1,
		width: width / 1.2,
		height: height / 2.4,
		alignItems: "center"
	},
	animatedLineStyle: {
		position: "absolute",
		borderWidth: 1,
		borderColor: "#38AFFE",
		width: width / 1.2
	},
	headerPrimaryText: {
		fontSize: height / 50,
		width: width / 1.5,
		textAlign: "center"
	},
	// Settings styles starts here
	settingsContainer: {
		flex: 1,
		margin: width / 100
	},
	settingsLabel: {
		fontSize: 22
	},
	connectedAccountsButton: {
		marginVertical: width / 100,
		borderTopWidth: 1,
		borderBottomWidth: 1,
		borderColor: "grey",
		paddingVertical: width / 100
	},
	connectedAccountsLabel: {
		fontSize: 18
	},
	closeCameraButton: {
		alignItems: "center",
		borderRadius: width / 50,
		...Platform.select({
			android: { elevation: 2 },
			ios: {
				shadowOffset: { width: 0, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		}),
		backgroundColor: "#4980E2",
		paddingVertical: height / 90,
		paddingHorizontal: width / 20
	},
	tabHeaderContainer: color => ({
		flexDirection: "row",
		color: color,
		padding: width * 0.01
	}),
	tabHeaderImage: color => ({
		width: width / 20,
		color,
		height: width / 20,
		marginRight: width / 50
	}),
	tabStyle: {
		alignItems: "center",
		justifyContent: "center",
		flexDirection: "row"
	},
	background: { backgroundColor: "#FFFFFF" },
	aboutContainer: {
		flex: 1
	},
	aboutText: {
		color: "#232132",
		fontFamily: Platform.OS === "ios" ? "Avenir" : font_two,
		fontWeight: Platform.OS === "ios" ? "400" : undefined,
		marginTop: 4,
		fontSize: 14
	},
	matchingGroupStyle: {
		borderWidth: 2,
		borderColor: "#5da4df",
		borderRadius: width * 0.03
	},
	unMactchingGrupStyle: {
		borderWidth: 2,
		borderColor: "#fff",
		borderRadius: width * 0.03
	},
	HR: {
		height: height / 90,
		backgroundColor: "#EFF1F4"
	},
	moreLabels: {
		marginLeft: width / 40
	},
	seprateLines: {
		height: 2,
		backgroundColor: "#EFF1F4",
		marginHorizontal: width / 20
	},
	postContainer: {
		flex: 1,
		alignSelf: "center"
	},
	flatlistContentContainer: {
		flexGrow: 1
	},
	emptyViewContainer: {
		height: "100%"
	},
	refreshButton: {
		backgroundColor: "#aaa",
		width: width * 0.4,
		paddingVertical: width * 0.02
	},
	listGap: {
		backgroundColor: "#edeff2"
	},
	socialPodIcon: { height: height / 25, width: width / 25 },
	clubNameGradient: {
		height: height / 7,
		paddingTop: height / 9,
		paddingHorizontal: width / 100,
		borderRadius: height / 80
	},
	clubNameText: {
		color: "#fff"
	}
});

export const FOCUSED_COLOR = "#63B2F4";
export const UNFOCUSED_COLOR = "#000";
export const grey = {
	shade1: "#000"
};
