import React, { Component } from "react";
import { View, Dimensions, TouchableOpacity, Alert } from "react-native";
import { connect } from "react-redux";
import { styles } from "./style";
import { CommunityTextMedium } from "../../../Components/CommunityText";

import Api from "../../../Api";
import QRScanner from "./QRScanner";
import { Icon } from "../../../../../Components";

const { height } = Dimensions.get("window");

const MEDIUM_GREY_COLOR = "rgba(0,0,0,0.5)";
const CANCEL = "CANCEL";
const STYLE_CANCEL = "cancel";
const OK = "OK";
const INFORMATION = "Information";
const OTP_TO_CONNECT_MASSAGE = "OTP to connect with social lobby is-";
const BECOME_CONNECT_USER_MESSAGE =
	"You need to become a connect user to connect with social lobby";
const UNABLE_TO_GENERATE_OTP = "Unable to generate OTP";
const ERROR = "Error";
class Setting extends Component {
	constructor(props) {
		super(props);
		this.state = {
			openScanner: false,
			otpFetching: false
		};
	}

	async getOtp(qrhash) {
		const onPress = () => this.closeScanner();
		const alertButtons = [
			{ text: CANCEL, onPress, style: STYLE_CANCEL },
			{ text: OK, onPress }
		];
		try {
			let response = await Api.getSocialLobbyOtp({
				appToken: this.props.appToken,
				qrhash
			});
			if (response.success === 1) {
				Alert.alert(
					INFORMATION,
					OTP_TO_CONNECT_MASSAGE + response.message.OTP,
					alertButtons,
					{ cancelable: false }
				);
			} else {
				throw new Error(UNABLE_TO_GENERATE_OTP);
			}
		} catch (error) {
			Alert.alert(ERROR, UNABLE_TO_GENERATE_OTP, alertButtons);
		}
	}

	handleBarCodeRead = barCodeData => {
		this.setState({ otpFetching: true });
		this.getOtp(barCodeData.data);
	};

	closeScanner = () => {
		this.setState({ openScanner: false, otpFetching: false });
	};

	openScanner = () => {
		if (this.props.profileToDisplay.linkedin) {
			this.setState({ openScanner: true });
		} else {
			Alert.alert(
				INFORMATION,
				BECOME_CONNECT_USER_MESSAGE,
				[
					{
						text: CANCEL,
						onPress: () => {}
					},
					{
						text: OK,
						onPress: () => this.navigateToMatchingConnection()
					}
				],
				{ cancelable: false }
			);
		}
	};

	navigateToMatchingConnection = () => {
		this.props.navigation.navigate("ConnectAuth");
	};

	render() {
		const { openScanner, otpFetching } = this.state;
		return (
			<View style={styles.container}>
				<View style={styles.box}>
					{this._renderSettingMenu(
						"icomoon",
						"connect_to_social",
						"Connect to SocialPod",
						this.openScanner
					)}
					<View style={styles.separateLines} />
					{this._renderSettingMenu(
						"icomoon",
						"connected-accounts",
						"Connected Accounts",
						this.navigateToMatchingConnection
					)}
					<View style={styles.separateLines} />

					<TouchableOpacity
						style={styles.settingButton}
						onPress={() =>
							this.props.navigation.navigate("LinkView", {
								url: "https://www.mexit.in/faqs/",
								title: "FAQs"
							})
						}>
						<Icon
							iconType="simple_line"
							iconSize={height / 50}
							iconName="question"
							iconColor={MEDIUM_GREY_COLOR}
						/>
						<CommunityTextMedium
							color={MEDIUM_GREY_COLOR}
							size={height / 55}
							style={styles.moreLabels}>
							{`FAQs`}
						</CommunityTextMedium>
					</TouchableOpacity>
					{openScanner && (
						<QRScanner
							openScanner={openScanner}
							otpFetching={otpFetching}
							closeScanner={this.closeScanner}
							handleBarCodeRead={this.handleBarCodeRead}
						/>
					)}
				</View>
			</View>
		);
	}
	_renderSettingMenu(iconType, iconName, label, onPress) {
		return (
			<TouchableOpacity style={styles.settingButton} onPress={onPress}>
				<Icon
					iconType={iconType}
					iconSize={height / 50}
					iconName={iconName}
					iconColor={MEDIUM_GREY_COLOR}
				/>
				<CommunityTextMedium
					color={MEDIUM_GREY_COLOR}
					size={height / 55}
					style={styles.moreLabels}>
					{label}
				</CommunityTextMedium>
			</TouchableOpacity>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		error: state.community && state.community.error,
		myProfile: state.community && state.community.profile.myProfile
	};
}

export default connect(mapStateToProps)(Setting);
