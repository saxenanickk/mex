import React, { Component } from "react";
import {
	View,
	ScrollView,
	TouchableOpacity,
	Dimensions,
	ImageBackground
} from "react-native";
import { connect } from "react-redux";
import _ from "lodash";
import I18n from "../../../Assets/Strings/i18n";
import MultipleItemSelector from "../../../../../Components/MultipleItemSelector";
import { styles, grey } from "./style";
import {
	CommunityTextLight,
	CommunityTextBold,
	CommunityTextMedium
} from "../../../Components/CommunityText";
import { replace } from "lodash";
import { NOIMAGE } from "../../../../../Assets/Img/Image";
import LinearGradient from "react-native-linear-gradient";
import {
	GoappTextBold,
	GoappTextRegular
} from "../../../../../Components/GoappText";
import { StackActions } from "@react-navigation/native";

const { width, height } = Dimensions.get("window");

const removeEmTags = items =>
	items.map(item => {
		item = replace(item, "<em>", "");
		item = replace(item, "</em>", "");
		return item;
	});
const isMatchingAttributes = (items, type, item) =>
	items.findIndex(value =>
		type === I18n.t("com_education") ? value === item.name : value === item
	) >= 0;
class About extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	renderAttributes = (userProfile, type) => {
		try {
			if (userProfile[type] && userProfile[type].length > 0) {
				let matchArray = [];
				if (type === I18n.t("com_education")) {
					matchArray = _.get(userProfile, "matched.education.name", []);
				} else {
					matchArray = _.get(userProfile, `matched.${type}`, []);
				}

				matchArray = removeEmTags(matchArray);
				let attributes = userProfile[type].map(item => {
					let obj = {};
					obj.value = type === I18n.t("com_education") ? item.name : item;
					obj.isMatching = isMatchingAttributes(matchArray, type, item);
					return obj;
				});
				return attributes;
			} else {
				return [];
			}
		} catch (error) {
			console.log("error is", error);
			return [];
		}
	};

	isMatchingClub = (userProfile, club_id) => {
		try {
			const club_ids = _.get(userProfile, "matched.club_ids", []);
			if (club_ids.length > 0) {
				let matchIdsCount = club_ids.filter(id => id === club_id).length;
				return matchIdsCount === 1;
			} else {
				return false;
			}
		} catch (error) {
			return false;
		}
	};

	getClubDtail = club => {
		this.props.navigation.dispatch(
			StackActions.push("ClubDetail", {
				clubId: club.id
			})
		);
	};

	render() {
		const { profileToDisplay } = this.props;

		return (
			<View style={styles.aboutContainer}>
				<ScrollView showsVerticalScrollIndicator={false}>
					{this._renderUserInfo(profileToDisplay.about, I18n.t("com_about"))}
					{!_.isEmpty(profileToDisplay.home_town) &&
						this._renderUserInfo(
							profileToDisplay.home_town,
							I18n.t("com_home_town")
						)}
					{!_.isEmpty(profileToDisplay.education) &&
						this._renderEducation(
							profileToDisplay,
							"education",
							I18n.t("com_Education")
						)}
					{!_.isEmpty(profileToDisplay.club_info) &&
						this._renderUserJoindGroups(profileToDisplay)}
					{!_.isEmpty(profileToDisplay.hobbies_interests) &&
						this._renderUserProfileInfo(
							profileToDisplay,
							"hobbies_interests",
							I18n.t("com_hobbies_and_interests")
						)}
					{!_.isEmpty(profileToDisplay.skills) && (
						<View style={styles.manageSkillsSection}>
							<MultipleItemSelector
								data={this.renderAttributes(profileToDisplay, "skills")}
								type={I18n.t("com_professional_skills")}
							/>
						</View>
					)}
					{!_.isEmpty(profileToDisplay.past_organisation) && (
						<View style={styles.manageSkillsSection}>
							<MultipleItemSelector
								data={this.renderAttributes(
									profileToDisplay,
									"past_organisation"
								)}
								type={I18n.t("com_past_organisations")}
							/>
						</View>
					)}
				</ScrollView>
			</View>
		);
	}

	_renderEducation = (profileToDisplay, type, dataLabel) => {
		return (
			<View style={styles.manageEducationSection}>
				<GoappTextBold style={styles.educationLabelText}>
					{dataLabel}
				</GoappTextBold>
				<View>
					{this.renderAttributes(profileToDisplay, type).map((item, index) => (
						<GoappTextRegular key={index} style={styles.educationText}>
							{item.value}
						</GoappTextRegular>
					))}
				</View>
			</View>
		);
	};

	_renderUserProfileInfo(profileToDisplay, type, dataLable) {
		return (
			<View style={styles.manageSkillsSection}>
				<MultipleItemSelector
					data={this.renderAttributes(profileToDisplay, type)}
					type={dataLable}
				/>
			</View>
		);
	}

	_renderUserJoindGroups(profileToDisplay) {
		return (
			<View style={styles.clubJoinedSection}>
				<CommunityTextBold color={grey.shade1}>
					{I18n.t("com_group_joined")}
				</CommunityTextBold>
				<View style={styles.joinedClubSection}>
					{profileToDisplay.club_info.map((club, index) => {
						return (
							<TouchableOpacity
								key={club.id}
								onPress={() => this.getClubDtail(club)}
								style={styles.joinedClubSingleView}>
								<ImageBackground
									defaultSource={NOIMAGE}
									source={
										club.icon !== null &&
										club.icon !== undefined &&
										club.icon.trim() !== ""
											? { uri: club.icon }
											: NOIMAGE
									}
									imageStyle={{
										borderRadius: width * 0.03
									}}
									style={[
										styles.clubImage,
										this.isMatchingClub(profileToDisplay, club.id)
											? styles.matchingGroupStyle
											: styles.unMactchingGrupStyle
									]}>
									<LinearGradient
										start={{ x: 0, y: 0 }}
										end={{ x: 0, y: 1 }}
										colors={["rgba(0, 0, 0 , 0)", "rgba(0, 0 , 0, 1)"]}
										style={styles.clubNameGradient}>
										<CommunityTextMedium
											numberOfLines={1}
											size={height / 68}
											color={"#000"}
											style={styles.clubNameText}>
											{_.capitalize(club.name) + " " + "Group"}
										</CommunityTextMedium>
									</LinearGradient>
								</ImageBackground>
							</TouchableOpacity>
						);
					})}
				</View>
			</View>
		);
	}

	_renderUserInfo(profileToDisplay, label) {
		return (
			<View style={styles.clubJoinedSection}>
				<CommunityTextBold color={grey.shade1}>{label}</CommunityTextBold>
				<CommunityTextLight style={styles.aboutText}>
					{profileToDisplay}
				</CommunityTextLight>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		secondUserProfile: _.get(state, "community.profile.secondUserProfile", {})
	};
}

export default connect(mapStateToProps)(About);
