import { StyleSheet, Dimensions, Platform } from "react-native";
const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	root: {
		flex: 1
	},
	container: {
		flex: 1,
		backgroundColor: "#fff",
		paddingVertical: height / 60
	},
	header: {
		width: width,
		height: height / 15,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	headerLeft: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center"
	},
	headerRight: {
		marginRight: width * 0.05,
		paddingHorizontal: width * 0.05,
		paddingVertical: width * 0.01,
		backgroundColor: "#2C98F0",
		borderRadius: width * 0.1
	},
	publishDisabled: {
		opacity: 0.3
	},
	backButton: {
		width: width / 5,
		height: height / 15,
		justifyContent: "center",
		paddingHorizontal: width / 20
	},
	inputField: {
		width: width * 0.9,
		alignSelf: "center",
		alignItems: "center",
		backgroundColor: "#f5f6f9",
		borderRadius: width / 25,
		paddingBottom: height * 0.01
	},
	imageText: {
		marginRight: width / 30,
		color: "#2c98f0"
	},
	editImage: {
		width: width / 20,
		height: width / 20
	},
	rowView: {
		flexDirection: "row",
		alignItems: "center"
	},
	textInput: {
		width: "100%",
		height: "100%",
		borderRadius: width / 25,
		paddingHorizontal: width * 0.05,
		paddingTop: height * 0.03,
		textAlignVertical: "top"
	},
	createPostSection: {
		width: 0.92 * width,
		paddingVertical: height / 50,
		borderRadius: width / 30,
		backgroundColor: "#fff",
		alignSelf: "center"
	},
	actionField: {
		width: "100%",
		flexDirection: "row",
		alignItems: "center",
		borderRadius: width / 30,
		marginTop: height / 30
	},
	action: {
		paddingVertical: height * 0.01,
		flexDirection: "row",
		alignItems: "center",
		backgroundColor: "#eaf4fc",
		marginHorizontal: width * 0.01,
		paddingHorizontal: width * 0.05,
		justifyContent: "space-around",
		borderRadius: width * 0.05
	},
	imageToUpload: {
		width: 0.5 * width,
		height: 0.3 * width,
		alignItems: "flex-end"
	},
	imageRemoveIcon: {
		width: width / 10,
		height: height / 25,
		alignItems: "center",
		justifyContent: "center"
	},
	lessOpacityBackground: {
		backgroundColor: "rgba(0,0,0,0.2)"
	},
	imageBorder: {
		borderRadius: 10
	},
	postText: {
		width: width / 5,
		alignItems: "flex-end",
		paddingVertical: height / 90
	},
	cancelText: {
		width: width / 5,
		paddingVertical: height / 90
	},
	progressScreen: {
		position: "absolute",
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		height: height,
		width: width,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "rgba(0,0,0,0.5)"
	},
	caretDownImage: {
		width: width / 25,
		height: width / 35
	},
	visiblityView: {
		flexDirection: "row",
		alignItems: "center",
		width: width / 7,
		justifyContent: "space-around"
	},
	visibleOptionView: {
		width: width / 1.8,
		backgroundColor: "#fff",
		position: "absolute",
		top: height / 2.7,
		right: width / 3.7,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	visiblityRowView: {
		width: width / 1.8,
		paddingVertical: height / 90,
		backgroundColor: "#f4f4f4",
		flexDirection: "row",
		justifyContent: "flex-start",
		paddingHorizontal: width / 40,
		borderBottomColor: "#fff",
		borderBottomWidth: 1,
		alignItems: "center"
	},
	visibilityHeader: {
		marginLeft: width / 30
	},
	visibilityHeaderText: {
		paddingVertical: height / 90,
		paddingHorizontal: width / 40
	},
	visibleText: {
		width: width / 2.3
	},
	mainImage: {
		width: width * 0.84,
		height: height / 5,
		borderRadius: width / 30,
		alignSelf: "center",
		marginTop: height / 50
	},
	outsideVideoView: {
		paddingTop: height / 60
	},
	videoView: {
		backgroundColor: "#fff",
		width: width * 0.83,
		justifyContent: "space-between",
		paddingTop: height / 200,
		paddingBottom: height / 200,
		marginTop: height / 40,
		borderRadius: 6,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	videoNameText: {
		width: width * 0.7
	},
	videoPadding: {
		paddingHorizontal: width / 30
	},
	progressBarView: {
		marginTop: height / 70
	},
	progressText: {
		width: width / 7
	},
	closeVideoButton: {
		position: "absolute",
		right: -3,
		top: 8,
		zIndex: 5,
		...Platform.select({
			android: { elevation: 10 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	bottomContent: {
		width: width * 0.83,
		paddingHorizontal: width / 30,
		marginTop: height / 90,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between"
	},
	videoUploadView: {
		width: width * 0.22,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between"
	},
	playButton: {
		justifyContent: "center",
		alignItems: "flex-start",
		width: width / 5
	},
	videoButton: {
		justifyContent: "center",
		alignItems: "center",
		width: width / 15,
		height: width / 15,
		borderRadius: width / 30,
		backgroundColor: "#f4f4f4"
	},
	urlPreviewView: {
		backgroundColor: "#f4f4f4",
		marginTop: height / 90,
		paddingVertical: height / 150,
		paddingHorizontal: width / 50,
		flexDirection: "row",
		borderRadius: width / 30
	},
	previewImage: {
		width: width / 4,
		height: height / 10
	},
	previewText: {
		width: width / 2
	},
	fullPreviewText: {
		width: width / 1.4
	},
	previewRemoveIcon: {
		position: "absolute",
		top: 0,
		right: 0
	},
	previewTextSection: {
		marginLeft: width / 60
	},
	progressView: {
		position: "absolute",
		top: 0,
		bottom: 0,
		backgroundColor: "#e8ebf1"
	}
});
