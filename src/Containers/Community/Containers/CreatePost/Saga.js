import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api";
import { communitySaveError } from "../../Saga";

export const COMMUNITY_GET_URL_PREVIEW = "COMMUNITY_GET_URL_PREVIEW";
export const COMMUNITY_SAVE_URL_PREVIEW = "COMMUNITY_SAVE_URL_PREVIEW";

export const communityGetUrlPreview = payload => ({
	type: COMMUNITY_GET_URL_PREVIEW,
	payload
});
export const communitySaveUrlPreview = payload => ({
	type: COMMUNITY_SAVE_URL_PREVIEW,
	payload
});
/**
 * Saga
 * @param {any} dispatch
 */
export function* communityCreatePostSaga(dispatch) {
	yield takeLatest(COMMUNITY_GET_URL_PREVIEW, handleCommunityGetUrlPreview);
}

function* handleCommunityGetUrlPreview(action) {
	try {
		let response = yield call(Api.getUrlPreview, action.payload);
		console.log("response of url preview", response);
		if (response.success === 1) {
			if (
				response.data.title ||
				response.data.appName ||
				response.data.description
			) {
				yield put(communitySaveUrlPreview(response.data));
			}
		} else {
			throw new Error("unable to fetch posts");
		}
	} catch (error) {
		console.log(error, "no url preview available");
	}
}
