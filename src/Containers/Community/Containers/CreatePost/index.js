import React, { Component } from "react";
import {
	View,
	Dimensions,
	Image,
	TextInput,
	TouchableOpacity,
	ImageBackground,
	Platform,
	Alert,
	NativeModules,
	LayoutAnimation,
	ScrollView,
	Keyboard
} from "react-native";
import { connect } from "react-redux";
import { check, PERMISSIONS, RESULTS, request } from "react-native-permissions";
import { ProgressBar, DialogModal } from "../../../../Components";
import { Icon, GoToast, GoappCamera } from "../../../../Components";
import { styles } from "./style";
import {
	CommunityTextMedium,
	CommunityTextBold,
	CommunityTextRegular
} from "../../Components/CommunityText";
import ImagePicker from "react-native-image-picker";
import {
	communityCreatePost,
	communityCreatePostResponse
} from "../Activity/Saga";
import { communityResetError } from "../../Saga";
import I18n from "../../Assets/Strings/i18n";
import CommunityLoader from "../../Components/CommunityLoader";
import Video from "react-native-video";
import {
	SPACE_KEY_CHAR_CODE,
	ENTER_KEY_CHAR_CODE,
	URL_MATCH_REGEX
} from "../../Constants";
import { communityGetUrlPreview, communitySaveUrlPreview } from "./Saga";
import { startsWith } from "lodash";
import UploadComponent, {
	EDIT_UPLOAD_PRESET
} from "../../../../Components/UploadComponent";
import Api from "../../Api";
import { GoappTextBold } from "../../../../Components/GoappText";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";

const { width, height } = Dimensions.get("window");
const { UIManager } = NativeModules;
if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}
// More info on all the options is below in the API Reference... just some common use cases shown here
const options = {
	storageOptions: {
		skipBackup: true,
		path: "images",
		cameraRoll: true,
		waitUntilSaved: true
	}
};

class CreatePost extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedImagePath: null,
			selectedVideoPath: null,
			postDescription: "",
			initialHeight: height / 15,
			isVisibleView: false,
			postVisibility: "public",
			videoPaused: true,
			uploadedVideoUrl: null,
			videoUploadStart: false,
			barProgress: 0,
			isModalVisible: false
		};
		this.changePostDescriptionText = this.changePostDescriptionText.bind(this);
		this.visiblityOption = [
			{
				iconName: "md-globe",
				title: "Public",
				value: "public",
				message: "Anyone in the community"
			},
			{
				iconName: "md-people",
				title: "Follower",
				value: "followers",
				message: "Your Followers on community"
			},
			{
				iconName: "md-lock",
				title: "Only me",
				value: "private",
				message: "Only me"
			}
		];
		this.openImagePicker = this.openImagePicker.bind(this);
		this.openVideoPicker = this.openVideoPicker.bind(this);
		this.validateVideoLength = this.validateVideoLength.bind(this);
		this.x = 0;
		this.y = 0;
		this.currentWord = "";
		this.cameraRef = React.createRef();
	}

	shouldComponentUpdate(props, state) {
		if (
			props.createPostResponse &&
			props.createPostResponse === true &&
			props.createPostResponse !== this.props.createPostResponse
		) {
			this.props.dispatch(communityCreatePostResponse(null));
			this.props.navigation.goBack();
		}
		if (
			props.error &&
			props.error !== this.props.error &&
			props.error.isError &&
			props.error.errorScreen === "CreatePost"
		) {
			GoToast.show(props.error.errorMessage, I18n.t("error"));
			this.props.dispatch(communityResetError());
			this.props.navigation.goBack();
		}
		return true;
	}
	/**
	 * handler function to handle the closing of club detail page
	 */
	async closeCreatePost() {
		this.props.navigation.goBack();
	}

	validateTypeOfImage = image => {
		try {
			let response = image;
			if (image.type === null || image.type === undefined) {
				let type = image.fileName.split(".");
				type = type[type.length - 1].toLowerCase();
				response = {
					...response,
					type: `image\/${type}`
				};
			}
			return response;
		} catch (error) {
			return image;
		}
	};
	async openImagePicker() {
		if (this.state.selectedVideoPath !== null) {
			Alert.alert(I18n.t("information"), "You need to remove the video.");
			return;
		}
		if (this.cameraRef) {
			this.cameraRef.handleCameraPermission();
		}
	}

	async openVideoPicker() {
		if (this.state.selectedImagePath !== null) {
			Alert.alert(I18n.t("information"), "You need to remove the image.");
			return;
		}
		if (this.state.videoUploadStart === true) {
			Alert.alert(I18n.t("information"), "Video Upload is in progress");
			return;
		}
		let videoOptions = {
			title: "Video Picker",
			takePhotoButtonTitle: null,
			mediaType: "video",
			videoQuality: "medium",
			chooseFromLibraryButtonTitle: I18n.t("com_choose_from_lib")
		};
		if (Platform.OS === "ios") {
			check(PERMISSIONS.IOS.PHOTO_LIBRARY)
				.then(result => {
					switch (result) {
						case RESULTS.UNAVAILABLE:
							console.log(
								"This feature is not available (on this device / in this context)"
							);
							break;
						case RESULTS.DENIED:
							request(PERMISSIONS.IOS.PHOTO_LIBRARY).then(res => {
								console.log(res);
							});
							break;
						case RESULTS.GRANTED:
							this.startVideo(videoOptions);
							break;
						case RESULTS.BLOCKED:
							console.log(
								"The permission is denied and not requestable anymore"
							);
							break;
					}
				})
				.catch(error => {
					console.log(error);
				});
		} else {
			this.startVideo(videoOptions);
		}
	}

	startVideo = async videoOptions => {
		await ImagePicker.showImagePicker(videoOptions, response => {
			if (response.didCancel) {
				console.log("User cancelled video picker");
			} else if (response.error) {
				console.log("ImagePicker Error: ", response.error);
			} else if (response.customButton) {
				console.log("User tapped custom button: ", response.customButton);
			} else {
				if (this.state.uploadedVideoUrl === null) {
					console.log("selected video is", response);
					this.setState({
						selectedVideoPath: response,
						selectedImagePath: null
					});
					if (this.props.urlPreview !== null) {
						this.props.dispatch(communitySaveUrlPreview(null));
					}
				} else {
					Alert.alert(
						I18n.t("information"),
						"Video has been uploaded,are you sure to remove the video",
						[
							{
								text: I18n.t("com_cancel"),
								onPress: () => {
									console.log("do nothing");
								},
								style: "cancel"
							},
							{
								text: I18n.t("com_ok"),
								onPress: () => {
									this.setState({
										selectedVideoPath: response,
										selectedImagePath: null,
										uploadedVideoUrl: null
									});
									if (this.props.urlPreview !== null) {
										this.props.dispatch(communitySaveUrlPreview(null));
									}
								}
							}
						],
						{ cancelable: false }
					);
				}
			}
		});
	};

	uploadPost = async () => {
		const { route } = this.props;
		const { clubId } = route.params ? route.params : {};

		Keyboard.dismiss();
		let urlPreview = null;
		if (this.state.videoUploadStart === true) {
			Alert.alert(I18n.t("information"), "Video upload is in progress");
			return;
		}
		if (
			this.state.postDescription.trim() !== "" ||
			this.state.selectedImagePath !== null ||
			this.state.selectedVideoPath !== null
		) {
			if (
				this.state.selectedImagePath === null &&
				this.state.selectedVideoPath === null &&
				this.props.urlPreview === null
			) {
				urlPreview = await this.fetchURLPreview(
					this.state.postDescription.trim()
				);
			}
			this.setState({ isLoading: true });
			GoAppAnalytics.trackWithProperties("New Community Post", {});
			this.props.dispatch(
				communityCreatePost({
					photo: this.state.selectedImagePath,
					video: this.state.uploadedVideoUrl,
					postType: "post",
					postTitle: "",
					postDescription: this.state.postDescription,
					appToken: this.props.appToken,
					postVisibility: this.state.postVisibility,
					clubId,
					urlPreview:
						this.props.urlPreview !== null
							? JSON.stringify(this.props.urlPreview)
							: urlPreview !== null
							? JSON.stringify(urlPreview)
							: null
				})
			);
			//this.props.navigation.goBack()
		} else {
			GoToast.show(I18n.t("com_empty_post_error"), I18n.t("error"));
		}
	};

	validateVideoLength = response => {
		console.log("valiate response", response);
		if (response.duration < 3 || response.duration > 60) {
			Alert.alert(
				I18n.t("com_video_length_out_of_limit_header"),
				I18n.t("com_video_length_out_of_limit")
			);
			this.setState({ selectedVideoPath: null, videoUploadStart: false });
		}
	};
	createVideoFormData = video => {
		const data = new FormData();
		if (video !== null) {
			data.append("file", {
				name: "video",
				type: Platform.OS === "android" ? "video/mp4" : "video/mov",
				uri:
					Platform.OS === "android"
						? video.uri
						: video.uri.replace("file://", "")
			});
			data.append("upload_preset", EDIT_UPLOAD_PRESET);
		}

		return data;
	};

	removeVideo = () => {
		if (this.state.videoUploadStart === true) {
			Alert.alert(
				I18n.t("information"),
				"Video upload is in progress,Do you want to cancel it",
				[
					{
						text: I18n.t("com_cancel"),
						onPress: () => {
							console.log("do nothing");
						},
						style: "cancel"
					},
					{
						text: I18n.t("com_ok"),
						onPress: () => {
							this.setState({
								selectedVideoPath: null,
								uploadedVideoUrl: null,
								videoUploadStart: false
							});
						}
					}
				],
				{ cancelable: false }
			);
		} else {
			if (this.state.uploadedVideoUrl === null) {
				this.setState({ selectedVideoPath: null });
			} else {
				Alert.alert(
					I18n.t("information"),
					"Video has been uploaded,are you sure to remove the video",
					[
						{
							text: I18n.t("com_cancel"),
							onPress: () => {
								console.log("do nothing");
							},
							style: "cancel"
						},
						{
							text: I18n.t("com_ok"),
							onPress: () => {
								this.setState({
									selectedVideoPath: null,
									uploadedVideoUrl: null,
									videoUploadStart: false
								});
							}
						}
					],
					{ cancelable: false }
				);
			}
		}
	};

	getVideoName = () => {
		try {
			if (Platform.OS === "android") {
				let path = this.state.selectedVideoPath.path;
				let nameArray = path.split("/");
				return nameArray[nameArray.length - 1];
			} else {
				return this.state.selectedVideoPath.fileName;
			}
		} catch (error) {
			return "temp";
		}
	};

	renderVideo = () => {
		try {
			return (
				<View style={styles.videoView}>
					<View
						style={[
							styles.progressView,
							{
								width: this.getVideoUploadProgressWidth()
							}
						]}
					/>
					<UploadComponent
						onUploadStart={() => this.setState({ videoUploadStart: true })}
						onUploadSuccess={response => {
							console.log("response on success", response);
							LayoutAnimation.linear();
							this.setState({
								videoUploadStart: false,
								barProgress: 0,
								uploadedVideoUrl: response.secure_url
							});
						}}
						onUploadFail={response => {
							Alert.alert(I18n.t("information"), "Unable to upload video");
							this.setState({
								videoUploadStart: false,
								barProgress: 0,
								selectedVideoPath: null
							});
						}}
						onProgressUpdate={response => {
							LayoutAnimation.linear();
							this.setState({ barProgress: response });
						}}
						blobBody={this.createVideoFormData(this.state.selectedVideoPath)}
						fileURI={this.state.selectedVideoPath.uri}
					/>
					<Video
						source={{
							uri: this.state.selectedVideoPath.uri
						}}
						ref={ref => {
							this.player = ref;
						}}
						style={{ width: 0, height: 0 }}
						paused={this.state.videoPaused}
						onLoad={this.validateVideoLength}
					/>
					<View style={styles.videoPadding}>
						<CommunityTextMedium
							color={"#4f4f4f"}
							numberOfLines={1}
							style={styles.videoNameText}>
							{this.getVideoName()}
						</CommunityTextMedium>
					</View>
					<View style={styles.bottomContent}>
						{this.state.videoUploadStart ? (
							<View>
								<CommunityTextMedium
									color={"#4f4f4f"}
									numberOfLines={1}
									style={styles.progressText}>
									{`${this.state.barProgress}%`}
								</CommunityTextMedium>
							</View>
						) : this.state.uploadedVideoUrl !== null ? (
							<View style={styles.videoButton}>
								<Icon
									iconType={"ionicon"}
									iconSize={height / 30}
									iconName={"ios-checkmark"}
									iconColor={"#439a72"}
								/>
							</View>
						) : (
							<View />
						)}
						<View style={styles.videoUploadView}>
							<TouchableOpacity
								style={styles.videoButton}
								onPress={() =>
									this.props.navigation.navigate("VideoPlayer", {
										videoUri: this.state.selectedVideoPath.uri
									})
								}>
								<Icon
									iconType={"ionicon"}
									iconSize={height / 40}
									iconName={"ios-play"}
									iconColor={"#8b8fa5"}
								/>
							</TouchableOpacity>
							<TouchableOpacity
								style={styles.videoButton}
								onPress={() => this.removeVideo()}>
								<Icon
									iconType={"ionicon"}
									iconSize={height / 30}
									iconName={"ios-close"}
									iconColor={"#8b8fa5"}
								/>
							</TouchableOpacity>
						</View>
					</View>
					{this.state.videoUploadStart ? (
						<View style={styles.progressBarView}>
							<ProgressBar
								width={width * 0.83}
								height={2}
								value={this.state.barProgress}
								backgroundColor="#7e74f7"
								barEasing={"linear"}
							/>
						</View>
					) : null}
				</View>
			);
		} catch (error) {
			console.log("error in video view", error);
			return null;
		}
	};

	fetchURLPreview = async text => {
		try {
			let url = text;
			let ar = URL_MATCH_REGEX.exec(url);
			if (ar !== null && ar instanceof Array && ar.length > 0) {
				url = ar[0];
			} else {
				if (this.props.urlPreview !== null) {
					this.props.dispatch(communitySaveUrlPreview(null));
				}
			}
			if (URL_MATCH_REGEX.test(url)) {
				if (
					startsWith(url.toLowerCase(), "https://") === false &&
					startsWith(url.toLowerCase(), "http://") === false
				) {
					url = "https://" + url;
				}
				if (
					this.props.urlPreview === null &&
					this.state.selectedImagePath === null &&
					this.state.selectedVideoPath === null
				) {
					let response = await Api.getUrlPreview({
						appToken: this.props.appToken,
						url: url
					});
					if (response.success === 1) {
						if (
							response.data.title ||
							response.data.appName ||
							response.data.description
						) {
							return response.data;
						}
					}
				}
			}
			return null;
		} catch (error) {
			return null;
		}
	};

	checkTextIsUrlOrNot = text => {
		try {
			let url = text;
			let ar = URL_MATCH_REGEX.exec(url);
			if (ar !== null && ar instanceof Array && ar.length > 0) {
				url = ar[0];
			} else {
				if (this.props.urlPreview !== null) {
					this.props.dispatch(communitySaveUrlPreview(null));
				}
			}
			if (URL_MATCH_REGEX.test(url)) {
				if (
					startsWith(url.toLowerCase(), "https://") === false &&
					startsWith(url.toLowerCase(), "http://") === false
				) {
					url = "https://" + url;
				}
				if (
					this.props.urlPreview === null &&
					this.state.selectedImagePath === null &&
					this.state.selectedVideoPath === null
				) {
					this.props.dispatch(
						communityGetUrlPreview({
							appToken: this.props.appToken,
							url: url
						})
					);
				}
			}
		} catch (error) {
			return;
		}
	};

	changePostDescriptionText = text => {
		let charCode = text.charCodeAt(text.length - 1);
		if (text.length > this.state.postDescription.length) {
			if (
				charCode === SPACE_KEY_CHAR_CODE ||
				charCode === ENTER_KEY_CHAR_CODE
			) {
				this.checkTextIsUrlOrNot(text);
			}
		}
		this.setState({ postDescription: text });
	};

	getVideoUploadProgressWidth = () => {
		if (this.state.barProgress !== 0) {
			return (width * 0.83 * this.state.barProgress) / 100;
		} else {
			return 0;
		}
	};

	renderPreviewImage = urlPreview => {
		try {
			let image = urlPreview.image;
			if (
				startsWith(image, "https://") === false &&
				startsWith(image, "http://") === false
			) {
				image = urlPreview.url + image;
			}
			return (
				<Image
					source={{ uri: image }}
					style={styles.previewImage}
					resizeMode={"cover"}
				/>
			);
		} catch (error) {
			return null;
		}
	};

	renderUrlPreview = () => {
		try {
			if (this.props.urlPreview !== null) {
				return (
					<TouchableOpacity
						onPress={() =>
							this.props.navigation.navigate("LinkView", {
								url: this.props.urlPreview.url
							})
						}
						activeOpacity={0.3}>
						<View style={styles.urlPreviewView}>
							{this.props.urlPreview.image
								? this.renderPreviewImage(this.props.urlPreview)
								: null}
							<View style={styles.previewTextSection}>
								{this.props.urlPreview.title ? (
									<CommunityTextBold
										numberOfLines={1}
										color={"#000"}
										style={styles.previewText}
										size={height / 55}>
										{this.props.urlPreview.title}
									</CommunityTextBold>
								) : null}
								{this.props.urlPreview.appName ? (
									<CommunityTextRegular
										numberOfLines={1}
										color={"#000"}
										style={styles.previewText}
										size={height / 55}>
										{this.props.urlPreview.appName}
									</CommunityTextRegular>
								) : null}
								{this.props.urlPreview.description ? (
									<CommunityTextRegular
										numberOfLines={3}
										color={"#000"}
										style={styles.previewText}
										size={height / 55}>
										{this.props.urlPreview.description}
									</CommunityTextRegular>
								) : null}
							</View>
						</View>
						<TouchableOpacity
							style={styles.previewRemoveIcon}
							onPress={() =>
								this.props.dispatch(communitySaveUrlPreview(null))
							}>
							<Icon
								iconType={"ionicon"}
								iconName={"ios-close-circle"}
								iconSize={width / 17}
								iconColor={"rgba(0,0,0,0.5)"}
							/>
						</TouchableOpacity>
					</TouchableOpacity>
				);
			} else {
				return null;
			}
		} catch (error) {
			return null;
		}
	};

	componentWillUnmount() {
		this.props.dispatch(communitySaveUrlPreview(null));
	}

	getImageStyle = () => {
		try {
			if (this.state.selectedImagePath) {
				let imageWidth = this.state.selectedImagePath.width;
				let imageHeight = this.state.selectedImagePath.height;
				let originalHeight = (width * 0.8 * imageHeight) / imageWidth;
				return {
					width: 0.8 * width,
					height: originalHeight,
					alignSelf: "center"
				};
			} else {
				return styles.imageToUpload;
			}
		} catch (error) {
			return styles.imageToUpload;
		}
	};

	isPublishDisabled = () => {
		return (
			this.state.postDescription.length > 0 ||
			this.state.selectedImagePath !== null ||
			this.state.uploadedVideoUrl !== null
		);
	};

	render() {
		return (
			<View style={styles.root}>
				<ScrollView
					style={styles.container}
					showsVerticalScrollIndicator={false}
					keyboardShouldPersistTaps={"always"}>
					<View style={styles.header}>
						<View style={styles.headerLeft}>
							<TouchableOpacity
								style={styles.backButton}
								onPress={() => this.props.navigation.goBack()}>
								<Icon
									iconType={"feather"}
									iconSize={width / 15}
									iconName={"arrow-left"}
									iconColor={"#000"}
								/>
							</TouchableOpacity>
							<CommunityTextBold color={"#4f4f4f"} size={height / 45}>
								{I18n.t("com_create_post")}
							</CommunityTextBold>
						</View>
						<TouchableOpacity
							style={[
								styles.headerRight,
								!this.isPublishDisabled() ? styles.publishDisabled : {}
							]}
							onPress={this.uploadPost}
							disabled={!this.isPublishDisabled()}>
							<GoappTextBold color={"#fff"}>Publish</GoappTextBold>
						</TouchableOpacity>
					</View>
					<View style={styles.createPostSection}>
						<View
							style={[
								styles.inputField,
								{
									height: height * 0.25
								}
							]}>
							<TextInput
								underlineColorAndroid={"transparent"}
								placeholder={I18n.t("com_write_something")}
								style={[styles.textInput]}
								multiline={true}
								value={this.state.postDescription}
								returnKeyType={"default"}
								onChangeText={this.changePostDescriptionText}
								selectTextOnFocus={true}
								selectionColor={"#c13d4a"}
								onContentSizeChange={event => {
									this.setState({
										initialHeight: event.nativeEvent.contentSize.height
									});
								}}
							/>
						</View>
						{this.renderUrlPreview()}
						{this.state.selectedImagePath ? (
							<ImageBackground
								source={{ uri: this.state.selectedImagePath.uri }}
								style={[styles.imageToUpload, { marginTop: height / 90 }]}
								imageStyle={styles.imageBorder}
								resizeMode={"cover"}>
								<View
									style={[
										styles.imageToUpload,
										styles.lessOpacityBackground,
										styles.imageBorder
									]}>
									<TouchableOpacity
										style={styles.imageRemoveIcon}
										onPress={() => this.setState({ selectedImagePath: null })}>
										<Icon
											iconType={"ionicon"}
											iconName={"ios-close-circle"}
											iconSize={width / 17}
											iconColor={"#fff"}
										/>
									</TouchableOpacity>
								</View>
							</ImageBackground>
						) : null}
						{this.state.selectedVideoPath && this.renderVideo()}
						<View style={styles.actionField}>
							<TouchableOpacity
								style={styles.action}
								onPress={this.openImagePicker}>
								<CommunityTextRegular
									style={styles.imageText}
									numberOfLines={1}>
									{I18n.t("com_image")}
								</CommunityTextRegular>
								<Icon
									iconName={"md-image"}
									iconType={"ionicon"}
									iconColor={"#2C98F0"}
									iconSize={18}
								/>
							</TouchableOpacity>
							<TouchableOpacity
								style={styles.action}
								onPress={this.openVideoPicker}>
								<CommunityTextRegular
									style={styles.imageText}
									numberOfLines={1}>
									{"Video"}
								</CommunityTextRegular>
								<Icon
									iconName={"video-camera"}
									iconType={"font_awesome"}
									iconColor={"#2C98F0"}
									iconSize={18}
								/>
							</TouchableOpacity>
						</View>
					</View>
					<DialogModal
						visible={this.state.isVisibleView}
						onRequestClose={() => this.setState({ isVisibleView: false })}>
						<View
							style={[
								styles.visibleOptionView,
								{
									top: this.x + height / 13,
									right: this.y - width / 1.8
								}
							]}>
							<View style={styles.visibilityHeaderText}>
								<CommunityTextRegular
									size={height / 50}
									numberOfLines={1}
									color={"#9d9d9d"}>
									{"Who should see this?"}
								</CommunityTextRegular>
							</View>
							{this.visiblityOption.map((item, index) => {
								return (
									<TouchableOpacity
										key={index}
										style={styles.visiblityRowView}
										onPress={() =>
											this.setState({ postVisibility: item.value })
										}>
										<Icon
											iconType={"ionicon"}
											iconName={item.iconName}
											iconSize={width / 15}
											iconColor={
												this.state.postVisibility === item.value
													? "#466cad"
													: "rgba(0,0,0,0.5)"
											}
										/>
										<View style={styles.visibilityHeader}>
											<CommunityTextBold
												size={height / 45}
												numberOfLines={1}
												style={styles.visibleText}>
												{item.title}
											</CommunityTextBold>
											<CommunityTextRegular
												size={height / 60}
												numberOfLines={1}
												color={"#9d9d9d"}
												style={styles.visibleText}>
												{item.message}
											</CommunityTextRegular>
										</View>
									</TouchableOpacity>
								);
							})}
						</View>
					</DialogModal>
				</ScrollView>
				{this.state.isLoading && (
					<View style={styles.progressScreen}>
						<CommunityLoader />
					</View>
				)}
				<GoappCamera
					ref={ref => (this.cameraRef = ref)}
					options={options}
					onImageSelect={response => {
						response = this.validateTypeOfImage(response);
						this.setState({
							selectedImagePath: response,
							selectedVideoPath: null
						});
						//link preview priority is too low,so if the user adds a image remove the link preview
						if (this.props.urlPreview !== null) {
							this.props.dispatch(communitySaveUrlPreview(null));
						}
					}}
				/>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		error: state.community && state.community.error,
		createPostResponse:
			state.community && state.community.postReducer.createPostResponse,
		urlPreview: state.community && state.community.createPostReducer.urlPreview
	};
}

export default connect(mapStateToProps)(CreatePost);
