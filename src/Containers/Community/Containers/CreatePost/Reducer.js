import { COMMUNITY_SAVE_URL_PREVIEW } from "./Saga";

const initialState = {
	urlPreview: null
};

/**
 * @param {{ type: string; payload: any; }} action
 */
export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case COMMUNITY_SAVE_URL_PREVIEW:
			return {
				...state,
				urlPreview: action.payload
			};
		default:
			return state;
	}
};
