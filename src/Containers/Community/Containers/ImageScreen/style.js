import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#000"
	},
	header: {
		position: "absolute",
		top: 0,
		height: height / 12,
		paddingHorizontal: width / 27.3,
		justifyContent: "center",
		borderWidth: 0,
		borderColor: "#fff",
		zIndex: 5,
		width,
		backgroundColor: "rgba(0,0,0,0.3)"
	},
	backButton: {
		width: width / 5,
		height: height / 13,
		justifyContent: "center"
	},
	imageContainer: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	likeCommentRow: {
		flexDirection: "row",
		borderWidth: 0,
		borderColor: "#efefef",
		alignItems: "center",
		height: height / 25,
		marginHorizontal: width / 27.3,
		borderBottomWidth: 1
	},
	mainImage: {
		width: width,
		height: height / 4,
		alignSelf: "center"
	},
	circle: {
		width: width / 90,
		height: width / 90,
		borderRadius: width / 180,
		backgroundColor: "#fff",
		marginHorizontal: width / 60
	},
	likeCommentSection: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		marginHorizontal: width / 27.3,
		borderColor: "#fff",
		height: height / 10 - height / 25
	},
	likeSection: {
		width: width / 2 - width / 27.3,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center"
	}
});
