import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	Easing,
	UIManager,
	Animated,
	Platform
} from "react-native";
import { connect } from "react-redux";

import ImageZoom from "react-native-image-pan-zoom";
import { styles } from "./style";
import I18n from "../../Assets/Strings/i18n";
import { Icon } from "../../../../Components";
import {
	communityChangeClubPostLikeLocal,
	communityChangePostLikeLocal
} from "../../Containers/Activity/Saga";
import {
	communityLikeOrUnlikePost,
	communitySaveSinglePostDetail,
	communitySaveSinglePostComment,
	communitySaveCommentOffset
} from "../../Containers/PostDetail/Saga";
import { CommunityTextMedium } from "../../Components/CommunityText";
import FastImage from "react-native-fast-image";
import { communityChangeMyPostLikeLocal } from "../EditProfile/Saga";

const { width, height } = Dimensions.get("window");

if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}
class ImageScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLiked: null
		};
		this.toggleBottomContent = this.toggleBottomContent.bind(this);
		this.animatedValue = new Animated.Value(0);
		this.isBottomVisible = false;
	}

	likeUnLike = (postDetail, isClubPost, isMyPost) => {
		let isLike = null;
		let likeCount = 0;
		if (this.state.isLiked === null) {
			likeCount = postDetail.hasLiked
				? postDetail.liked_count - 1
				: postDetail.liked_count + 1;
			this.setState({
				isLiked: !postDetail.hasLiked,
				likeCount
			});
			isLike = !postDetail.hasLiked;
		} else {
			likeCount = this.state.isLiked
				? this.state.likeCount - 1
				: this.state.likeCount + 1;
			this.setState({
				isLiked: !this.state.isLiked,
				likeCount
			});
			isLike = !this.state.isLiked;
		}
		this.props.dispatch(
			communityLikeOrUnlikePost({
				appToken: this.props.appToken,
				postId: postDetail.id,
				uniqueIdentifier: postDetail.uniqueIdentifier,
				isLike
			})
		);

		if (isClubPost) {
			this.props.dispatch(
				communityChangeClubPostLikeLocal({
					postId: postDetail.id,
					likeCount,
					isLike
				})
			);
		}
		if (isMyPost) {
			this.props.dispatch(
				communityChangeMyPostLikeLocal({
					postId: postDetail.id,
					likeCount,
					isLike
				})
			);
		}
		this.props.dispatch(
			communityChangePostLikeLocal({
				postId: postDetail.id,
				likeCount,
				isLike
			})
		);
	};

	toggleBottomContent = () => {
		Animated.timing(this.animatedValue, {
			toValue: this.isBottomVisible ? 0 : 1,
			duration: 300,
			easing: Easing.linear
		}).start();
		this.isBottomVisible = !this.isBottomVisible;
	};

	navigateToPostDetail = (postDetail, isClubPost, isMyPost) => {
		this.props.dispatch(communitySaveSinglePostComment(null));
		!(this.props.isClubPost && !this.props.isMember) &&
			this.props.navigation.navigate("PostDetail", {
				postId: postDetail.id,
				isClubPost: isClubPost,
				isMyPost: isMyPost
			});
	};

	renderPostImage = image => {
		try {
			if (image.startsWith("http")) {
				if (
					image.startsWith("http://res.cloudinary.com/") ||
					image.startsWith("https://res.cloudinary.com/")
				) {
					let imageArray = image.split("/upload/");
					image = `${imageArray[0]}/upload/q_50/${imageArray[1]}`;
				}
				return (
					<ImageZoom
						onClick={this.toggleBottomContent}
						cropWidth={width}
						cropHeight={height}
						imageWidth={width}
						imageHeight={height - height / 13}>
						<FastImage
							source={{ uri: image }}
							style={{ width: width, height: height - height / 13 }}
							resizeMode={Platform.OS === "ios" ? "contain" : "center"}
						/>
					</ImageZoom>
				);
			}
			return null;
		} catch (error) {
			return null;
		}
	};

	render() {
		const { route } = this.props;
		const { postDetail, isClubPost, isMember, isMyPost } = route.params
			? route.params
			: {};

		const bottomBarHeight = this.animatedValue.interpolate({
			inputRange: [0, 1],
			outputRange: [0, height / 10]
		});
		const usernameOpacity = this.animatedValue.interpolate({
			inputRange: [0, 1],
			outputRange: [0, 1]
		});

		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<TouchableOpacity
						style={styles.backButton}
						onPress={() => this.props.navigation.goBack()}>
						<Icon
							iconType={"material"}
							iconSize={width / 15}
							iconName={"close"}
							iconColor={"#fff"}
						/>
					</TouchableOpacity>
				</View>
				{postDetail.image !== null &&
					postDetail.image !== undefined &&
					postDetail.image.trim() !== "" &&
					postDetail.image.startsWith("http") &&
					this.renderPostImage(postDetail.image)}
				<Animated.View
					style={{
						position: "absolute",
						top: height / 12,
						paddingLeft: width / 26,
						opacity: usernameOpacity,
						width: width,
						backgroundColor: "rgba(0,0,0,0.3)"
					}}>
					<CommunityTextMedium color={"#fff"} size={height / 60}>
						{postDetail.user_info &&
							postDetail.user_info.name &&
							`Posted by: ${postDetail.user_info.name}`}
					</CommunityTextMedium>
				</Animated.View>
				<Animated.View
					style={{
						position: "absolute",
						bottom: 0,
						height: bottomBarHeight,
						width: width,
						backgroundColor: "rgba(0,0,0,0.3)"
					}}>
					<View style={styles.likeCommentRow}>
						<CommunityTextMedium color={"#fff"} size={height / 60}>
							{I18n.t("com_like", {
								count:
									this.state.isLiked === null
										? postDetail.liked_count
										: this.state.likeCount
							})}
						</CommunityTextMedium>
						<View style={styles.circle} />
						<CommunityTextMedium color={"#fff"} size={height / 60}>
							{I18n.t("com_comment", {
								count: postDetail.comment_count
							})}
						</CommunityTextMedium>
					</View>
					{!(isClubPost && !isMember) && (
						<View style={styles.likeCommentSection}>
							<TouchableOpacity
								onPress={() =>
									this.likeUnLike(postDetail, isClubPost, isMyPost)
								}
								style={styles.likeSection}>
								<Icon
									iconType={"ionicon"}
									iconSize={width / 25}
									iconName={"ios-heart"}
									iconColor={
										this.state.isLiked === null
											? postDetail.hasLiked
												? "#d75654"
												: "#fff"
											: this.state.isLiked
											? "#d75654"
											: "#fff"
									}
								/>
								<CommunityTextMedium color={"#fff"} size={height / 55}>
									{"  Like"}
								</CommunityTextMedium>
							</TouchableOpacity>
							<TouchableOpacity
								onPress={() => {
									this.navigateToPostDetail(postDetail, isClubPost, isMyPost);
								}}
								style={styles.likeSection}>
								<Icon
									iconType={"ionicon"}
									iconSize={width / 25}
									iconName={"ios-paper"}
									iconColor={"#fff"}
								/>
								<CommunityTextMedium color={"#fff"} size={height / 55}>
									{"  Comment"}
								</CommunityTextMedium>
							</TouchableOpacity>
						</View>
					)}
				</Animated.View>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		error: state.community && state.community.error,
		postDetail: state.community && state.community.postDetailReducer.postDetail
	};
}

export default connect(mapStateToProps)(ImageScreen);
