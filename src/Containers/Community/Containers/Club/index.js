import React, { Component } from "react";
import {
	View,
	Dimensions,
	ScrollView,
	TouchableOpacity,
	Alert
} from "react-native";
import { connect } from "react-redux";
import { styles } from "./style.js";
import I18n from "../../Assets/Strings/i18n";
import {
	communityGetClubs,
	communityModifyClubMemberShip,
	communitySaveClubs
} from "./Saga";
import { communityResetError } from "../../Saga";
import ClubCard from "../../Components/ClubCard";
import CommunityLoader from "../../Components/CommunityLoader";

const { width } = Dimensions.get("window");
class Club extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedClub: []
		};
	}

	componentDidMount() {
		this._unsubscribeFocus = this.props.navigation.addListener("focus", () => {
			!this.props.clubs.length &&
				this.props.dispatch(
					communityGetClubs({
						appToken: this.props.appToken
					})
				);
		});
	}

	showAlert(message, type) {
		Alert.alert(type, message);
	}

	shouldComponentUpdate(props, state) {
		if (
			props.error &&
			props.error !== this.props.error &&
			props.error.isError &&
			props.error.errorScreen === "Clubs"
		) {
			//@ts-ignore
			this.showAlert(I18n.t("error"), "Something went wrong");
			this.props.dispatch(communityResetError());
			this.props.navigation.navigate("Activities");
		}
		return true;
	}

	modifySelectedClub = club => {
		let obj = {
			appToken: this.props.appToken,
			clubId: club.id
		};
		if (club.isMember) {
			obj.url = "leaveClub";
		} else {
			obj.url = "joinClub";
		}
		this.props.dispatch(communityModifyClubMemberShip(obj));
	};

	componentWillUnmount() {
		this.props.dispatch(communitySaveClubs(null));
		this._unsubscribeFocus();
	}

	render() {
		return (
			<View style={styles.container}>
				<ScrollView
					showsVerticalScrollIndicator={false}
					scrollEventThrottle={10}>
					<View style={styles.clubsScrollView}>
						{this.props.clubs.length ? (
							this.props.clubs.map((club, index) => {
								return (
									<TouchableOpacity
										activeOpacity={1}
										onPress={() =>
											this.props.navigation.navigate("ClubDetail", {
												clubId: club.id
											})
										}
										key={index}
										style={styles.clubSection}>
										<ClubCard club={club} />
									</TouchableOpacity>
								);
							})
						) : (
							<View style={styles.progressScreen}>
								<CommunityLoader size={width / 15} />
							</View>
						)}
					</View>
				</ScrollView>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		clubs: state.community && state.community.clubReducer.clubs,
		appToken: state.appToken.token,
		error: state.community && state.community.error
	};
}

export default connect(mapStateToProps)(Club);
