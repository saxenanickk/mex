import React from "react";
import { StyleSheet, Dimensions, Platform } from "react-native";
const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	container: { flex: 1, backgroundColor: "#fff" },
	imageView: {
		width: width / 6,
		height: width / 6,
		borderRadius: width / 10,
		backgroundColor: "#fff",
		justifyContent: "center",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	clubsScrollView: {
		backgroundColor: "#fff",
		flexDirection: "row",
		flexWrap: "wrap",
		marginTop: height * 0.025
	},
	clubSection: {
		width: width / 2,
		justifyContent: "center",
		alignItems: "center"
	},
	leftBorder: {
		borderLeftWidth: 1,
		borderLeftColor: "#f2f2f2"
	},
	clubImage: {
		width: width / 13,
		height: width / 13
	},
	clubNameText: {
		marginTop: height / 60
	},
	clubButton: {
		width: width / 5,
		height: height / 25,
		backgroundColor: "#ececec",
		borderWidth: 0.5,
		borderColor: "#c3c3c3",
		borderRadius: width / 60,
		marginTop: height / 40,
		justifyContent: "center",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	progressScreen: {
		height: height / 1.5,
		justifyContent: "center",
		alignItems: "center",
		width: width
	}
});

/**
 * ...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3,
			},
		}),
 */
