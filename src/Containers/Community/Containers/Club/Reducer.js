// @ts-check
import { COMMUNITY_SAVE_CLUBS, COMMUNITY_SAVE_CLUB_DETAIL } from "./Saga";

const initialState = {
	clubs: [],
	clubDetail: null
};

/**
 * @param {{ type: string; payload: any; }} action
 */
export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case COMMUNITY_SAVE_CLUBS:
			return {
				...state,
				clubs: action.payload
			};
		case COMMUNITY_SAVE_CLUB_DETAIL:
			return {
				...state,
				clubDetail: action.payload
			};
		default:
			return state;
	}
};
