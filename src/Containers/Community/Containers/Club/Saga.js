// @ts-check
import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api";
import { communitySaveError } from "../../Saga";

// Constants
export const COMMUNITY_GET_CLUBS = "COMMUNITY_GET_CLUBS";
export const COMMUNITY_SAVE_CLUBS = "COMMUNITY_SAVE_CLUBS";
export const COMMUNITY_MODIFY_CLUB_MEMBERSHIP =
	"COMMUNITY_MODIFY_CLUB_MEMBERSHIP";
// Action Creators
export const COMMUNITY_GET_CLUB_DETAIL = "COMMUNITY_GET_CLUB_DETAIL";
export const COMMUNITY_SAVE_CLUB_DETAIL = "COMMUNITY_SAVE_CLUB_DETAIL";

export const communityGetClubs = payload => ({
	type: COMMUNITY_GET_CLUBS,
	payload
});
export const communitySaveClubs = payload => ({
	type: COMMUNITY_SAVE_CLUBS,
	payload
});
export const communityModifyClubMemberShip = payload => ({
	type: COMMUNITY_MODIFY_CLUB_MEMBERSHIP,
	payload
});
export const communitySaveClubDetail = payload => ({
	type: COMMUNITY_SAVE_CLUB_DETAIL,
	payload
});
export const communityGetClubDetail = payload => ({
	type: COMMUNITY_GET_CLUB_DETAIL,
	payload
});
/**
 * Saga
 * @param {any} dispatch
 */
export function* communityClubSaga(dispatch) {
	yield takeLatest(COMMUNITY_GET_CLUBS, handleCommunityGetClubs);
	yield takeEvery(
		COMMUNITY_MODIFY_CLUB_MEMBERSHIP,
		handleCommunityModifyClubMemberShip
	);
	yield takeLatest(COMMUNITY_GET_CLUB_DETAIL, handleCommunityGetClubDetail);
}

// Handlers

function* handleCommunityGetClubs(action) {
	try {
		let response = yield call(Api.getClubs, action.payload);
		console.log("response", response);
		if (response.success === 1) {
			yield put(communitySaveClubs(response.data));
		} else {
			yield put(
				communitySaveError({
					errorScreen: "Clubs",
					errorMessage: "Unable to fetch clubs",
					isError: true
				})
			);
		}
	} catch (error) {
		console.log(error, "error");
		yield put(
			communitySaveError({
				errorScreen: "Clubs",
				errorMessage: "Unable to fetch clubs",
				isError: true
			})
		);
	}
}
function* handleCommunityModifyClubMemberShip(action) {
	try {
		let response = yield call(Api.userModifyClubMemberShip, action.payload);
		console.log("response", response);
		if (response.success === 1) {
			yield put(
				communityGetClubs({
					appToken: action.payload.appToken
				})
			);
			yield put(
				communityGetClubDetail({
					appToken: action.payload.appToken,
					clubId: action.payload.clubId
				})
			);
		} else {
			yield put(
				communitySaveError({
					errorScreen: "Clubs",
					errorMessage:
						action.payload.url === "joinClub"
							? "Unable to join club"
							: "Unable to leave club",
					isError: true
				})
			);
		}
	} catch (error) {
		console.log(error, "error");
		yield put(
			communitySaveError({
				errorScreen: "Clubs",
				errorMessage:
					action.payload.url === "joinClub"
						? "Unable to join club"
						: "Unable to leave club",
				isError: true
			})
		);
	}
}
function* handleCommunityGetClubDetail(action) {
	try {
		let response = yield call(Api.getClubDetail, action.payload);
		console.log("response", response);
		if (response.success === 1) {
			yield put(communitySaveClubDetail(response.data));
		} else {
			yield put(
				communitySaveError({
					errorScreen: "ClubDetail",
					errorMessage: "Unable to fetch club detail",
					isError: true
				})
			);
		}
	} catch (error) {
		console.log(error, "error");
		yield put(
			communitySaveError({
				errorScreen: "ClubDetail",
				errorMessage: "Unable to fetch club detail",
				isError: true
			})
		);
	}
}
