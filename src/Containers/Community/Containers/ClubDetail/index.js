import React, { Component } from "react";
import {
	View,
	Dimensions,
	Image,
	Alert,
	SafeAreaView,
	TouchableOpacity,
	BackHandler,
	ImageBackground,
	FlatList
} from "react-native";
import { connect } from "react-redux";

import { Icon } from "../../../../Components";
import {
	communityGetClubDetail,
	communitySaveClubDetail,
	communityModifyClubMemberShip
} from "../Club/Saga";
import I18n from "../../Assets/Strings/i18n";
import {
	communityResetError,
	communitySaveCurrentDisplayScreen
} from "../../Saga";
import { styles } from "./style";
import { capitalize } from "lodash";
import {
	CommunityTextMedium,
	CommunityTextBold,
	CommunityTextRegular
} from "../../Components/CommunityText";
import { CARETDOWNCPG, CARETUPCPG } from "../../Assets/Img/Image";
import { NOIMAGE } from "../../../../Assets/Img/Image";
import { communityGetClubPost, communitySaveClubPost } from "../Activity/Saga";
import PostCard from "../../Components/PostCard";
import CommunityLoader from "../../Components/CommunityLoader";
import {
	CLUB_MEMBER,
	NOT_CLUB_MEMBER,
	PENDING_MEMBER_STATUS
} from "../../Constants";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";

const { width, height } = Dimensions.get("window");
class ClubDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isExpandedClubDetail: false
		};

		const { route } = props;
		const { clubId } = route.params ? route.params : {};

		this.clubId = clubId;

		this.closeClubDetail = this.closeClubDetail.bind(this);
		this.fetchClubDetailAndPost = this.fetchClubDetailAndPost.bind(this);
		BackHandler.addEventListener("hardwareBackPress", this.closeClubDetail);
	}

	fetchClubDetailAndPost() {
		const clubId = this.clubId || null;
		if (this.props.currentDisplayScreen !== "ClubDetail") {
			this.props.dispatch(communitySaveCurrentDisplayScreen("ClubDetail"));
		}
		this.props.dispatch(
			communityGetClubDetail({
				appToken: this.props.appToken,
				clubId
			})
		);
		this.props.dispatch(
			communityGetClubPost({
				appToken: this.props.appToken,
				isRefresh: true,
				clubId
			})
		);
	}
	shouldComponentUpdate(props, state) {
		if (props.clubDetail && props.clubDetail !== this.props.clubDetail) {
			this.setState({
				status: props.clubDetail.Status
			});
		}
		if (
			props.error &&
			props.error !== this.props.error &&
			props.error.isError &&
			props.error.errorScreen === "ClubDetail"
		) {
			//@ts-ignore
			Alert.alert(I18n.t("error"), props.error.errorMessage);
			this.props.navigation.goBack();
			this.props.dispatch(communityResetError());
		}
		return true;
	}

	componentDidMount() {
		this._unsubscribeFocus = this.props.navigation.addListener("focus", () => {
			this.fetchClubDetailAndPost();
		});

		this._unsubscribeBlur = this.props.navigation.addListener("blur", () => {
			this.props.dispatch(communitySaveClubDetail(null));
			this.props.dispatch(communitySaveClubPost(null));
			this.props.dispatch(communitySaveCurrentDisplayScreen(null));
		});
	}

	componentWillUnmount() {
		this.props.dispatch(communitySaveClubDetail(null));
		this.props.dispatch(communitySaveClubPost(null));
		this._unsubscribeFocus();
		this._unsubscribeBlur();
		BackHandler.removeEventListener("hardwareBackPress", this.closeClubDetail);
	}

	modifySelectedClub = club => {
		try {
			let obj = {
				appToken: this.props.appToken,
				clubId: club.id
			};
			let club_name = club.name;
			if (this.state.status === CLUB_MEMBER) {
				Alert.alert(
					I18n.t("com_sure_to_leave_club_header"),
					I18n.t("com_sure_to_leave_club"),
					[
						{
							text: "Cancel",
							onPress: () => {
								console.log("cancel pressed");
							},
							style: "cancel"
						},
						{
							text: "OK",
							onPress: () => {
								obj.url = "leaveClub";
								if (club_name) {
									GoAppAnalytics.trackWithProperties("Left Group", {
										group_name: club_name
									});
								}
								this.setState({
									status:
										this.state.status === NOT_CLUB_MEMBER
											? PENDING_MEMBER_STATUS
											: NOT_CLUB_MEMBER
								});
								this.props.dispatch(communityModifyClubMemberShip(obj));
							}
						}
					],
					{ cancelable: false }
				);
			} else if (this.state.status === NOT_CLUB_MEMBER) {
				obj.url = "joinClub";
				if (club_name) {
					GoAppAnalytics.trackWithProperties("Joined Group", {
						group_name: club_name
					});
				}
				this.setState({
					status:
						this.state.status === NOT_CLUB_MEMBER
							? PENDING_MEMBER_STATUS
							: NOT_CLUB_MEMBER
				});
				this.props.dispatch(communityModifyClubMemberShip(obj));
			}
		} catch (error) {}
	};

	/**
	 * handler function to handle the closing of club detail page
	 */
	async closeClubDetail() {
		this.props.navigation.goBack();
		return true;
	}

	getData = () => {
		let data = [];
		data[0] = { isFirstAvailable: true };
		try {
			if (
				this.props.clubPost !== null &&
				this.props.clubPost !== undefined &&
				this.state.isExpandedClubDetail === false
			) {
				data = data.concat(this.props.clubPost);
			}
			return data;
		} catch (error) {
			return data;
		}
	};

	renderFirstElement = () => {
		const { clubDetail } = this.props;
		try {
			if (this.state.isExpandedClubDetail) {
				return (
					<View style={styles.expandedClubContainer}>
						{clubDetail.images && clubDetail.images.length > 0 ? (
							<ImageBackground
								source={{ uri: clubDetail.images[0] }}
								style={styles.bannerView}
								resizeMode={"cover"}
							/>
						) : (
							<ImageBackground
								source={NOIMAGE}
								style={styles.bannerView}
								resizeMode={"cover"}>
								<CommunityTextBold size={height / 20} color={"#000"}>
									{clubDetail.name}
								</CommunityTextBold>
							</ImageBackground>
						)}
						<CommunityTextMedium
							size={height / 50}
							color={"#939393"}
							style={styles.clubDetailText}>
							{clubDetail.description}
						</CommunityTextMedium>
						<TouchableOpacity
							style={styles.expandButton}
							onPress={() => this.setState({ isExpandedClubDetail: false })}>
							<Image
								source={CARETUPCPG}
								style={styles.caretUpImage}
								resizeMode={"contain"}
							/>
						</TouchableOpacity>
					</View>
				);
			} else {
				return (
					<View style={styles.shrinkedClubContainer}>
						{clubDetail.images && clubDetail.images.length > 0 ? (
							<ImageBackground
								source={{ uri: clubDetail.images[0] }}
								style={styles.bannerView}
								resizeMode={"cover"}
							/>
						) : (
							<ImageBackground
								source={NOIMAGE}
								style={styles.bannerView}
								resizeMode={"cover"}>
								<CommunityTextBold size={height / 20} color={"#000"}>
									{clubDetail.name}
								</CommunityTextBold>
							</ImageBackground>
						)}
						<View style={styles.readMoreContainer}>
							<CommunityTextMedium size={height / 60} color={"#000"}>
								{I18n.t("com_read_more")}
							</CommunityTextMedium>
							<TouchableOpacity
								onPress={() => this.setState({ isExpandedClubDetail: true })}>
								<Image
									source={CARETDOWNCPG}
									style={styles.caretDownImage}
									resizeMode={"contain"}
								/>
							</TouchableOpacity>
						</View>
						{clubDetail.isMember ? (
							<View style={styles.inputField}>
								<TouchableOpacity
									activeOpacity={0.8}
									style={styles.rowView}
									onPress={() =>
										this.props.navigation.navigate("CreatePost", {
											clubId: this.clubId || null
										})
									}>
									<CommunityTextRegular style={styles.textInput}>
										{I18n.t("comments_write")}
									</CommunityTextRegular>
									<Icon
										iconType={"feather"}
										iconSize={width * 0.08}
										iconName={"plus"}
										iconColor={"#9A9A9A"}
										style={{ marginLeft: width / 3.1 }}
									/>
								</TouchableOpacity>
							</View>
						) : null}
					</View>
				);
			}
		} catch (error) {
			return null;
		}
	};

	render() {
		const { clubDetail } = this.props;
		const clubId = this.clubId || null;

		return (
			<SafeAreaView style={styles.container}>
				{clubDetail ? (
					<View style={styles.container}>
						<View style={styles.header}>
							<View style={styles.headerLeftPart}>
								<TouchableOpacity
									style={styles.backButton}
									onPress={() => this.props.navigation.goBack()}>
									<Icon
										iconType={"material"}
										iconSize={width / 18}
										iconName={"arrow-back"}
										iconColor={"#000"}
									/>
								</TouchableOpacity>
								<TouchableOpacity
									style={styles.headerText}
									onPress={() =>
										this.props.navigation.navigate("UserListView", {
											clubId: clubDetail.id
										})
									}>
									<CommunityTextMedium
										size={height / 43}
										color={"#000"}
										style={styles.clubHeaderText}
										numberOfLines={1}>
										{capitalize(clubDetail.name) + " " + "Group"}
									</CommunityTextMedium>
									<CommunityTextMedium
										size={height / 53}
										color={"#929292"}
										style={styles.clubHeaderText}
										numberOfLines={1}>
										{clubDetail.followers +
											" " +
											//@ts-ignore
											I18n.t("com_member", { count: clubDetail.followers })}
									</CommunityTextMedium>
								</TouchableOpacity>
							</View>
							<TouchableOpacity
								style={[styles.memberShipButton]}
								onPress={() =>
									this.state.status !== PENDING_MEMBER_STATUS
										? this.modifySelectedClub(clubDetail)
										: Alert.alert(
												I18n.t("error"),
												"Your membership status is pending"
										  )
								}>
								<CommunityTextMedium
									size={height / 45}
									color={"#414141"}
									style={styles.membershipTextStyle}
									numberOfLines={1}>
									{this.state.status === CLUB_MEMBER
										? I18n.t("com_leave_club")
										: this.state.status === PENDING_MEMBER_STATUS
										? I18n.t("com_pending_club")
										: I18n.t("com_join_club")}
								</CommunityTextMedium>
							</TouchableOpacity>
						</View>

						<FlatList
							data={this.getData()}
							showsVerticalScrollIndicator={false}
							keyExtractor={(item, index) => index.toString()}
							refreshing={false}
							onRefresh={() => {
								if (!this.state.isExpandedClubDetail) {
									this.props.dispatch(
										communityGetClubPost({
											appToken: this.props.appToken,
											isRefresh: true,
											clubId
										})
									);
								}
							}}
							onEndReachedThreshold={0.4}
							onEndReached={() => {
								if (
									!this.state.isExpandedClubDetail &&
									this.props.clubNextOffset !== null
								) {
									this.props.dispatch(
										communityGetClubPost({
											appToken: this.props.appToken,
											nextOffSet: this.props.clubNextOffset,
											isRefresh: false,
											clubId
										})
									);
								}
							}}
							renderItem={({ item, index }) => {
								if (index === 0) {
									return this.renderFirstElement();
								} else {
									return (
										<View style={styles.flatlistItemContainer}>
											<PostCard
												postDetail={item}
												key={index}
												isClubPost={true}
												navigation={this.props.navigation}
												isMember={clubDetail.isMember}
											/>
										</View>
									);
								}
							}}
						/>
					</View>
				) : (
					<View style={styles.progressScreen}>
						<CommunityLoader size={width / 15} />
					</View>
				)}
			</SafeAreaView>
		);
	}
}

function mapStateToProps(state) {
	return {
		clubDetail: state.community && state.community.clubReducer.clubDetail,
		appToken: state.appToken.token,
		error: state.community && state.community.error,
		clubPost: state.community && state.community.postReducer.clubPost,
		clubNextOffset:
			state.community && state.community.postReducer.clubNextOffset,
		currentDisplayScreen:
			state.community && state.community.error.currentDisplayScreen
	};
}

export default connect(mapStateToProps)(ClubDetail);
