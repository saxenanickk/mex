import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: { flex: 1, backgroundColor: "#eeeff5" },
	expandedClubContainer: {
		backgroundColor: "transparent",
		justifyContent: "space-between"
	},
	shrinkedClubContainer: { alignItems: "center" },
	readMoreContainer: {
		marginTop: width / 50,
		marginBottom: width / 50,
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center"
	},
	expandButton: { flex: 1, alignSelf: "center" },
	flatlistItemContainer: {
		borderWidth: 0
	},
	inputField: {
		marginBottom: height / 70,
		width: width,
		alignSelf: "center",
		justifyContent: "center",
		backgroundColor: "#fff",
		...Platform.select({
			android: { elevation: 1 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	progressScreen: {
		height: height,
		justifyContent: "center",
		alignItems: "center"
	},
	commentField: {
		width: width * 0.84,
		alignSelf: "center",
		marginTop: height / 50,
		height: height / 15,
		justifyContent: "center",
		paddingHorizontal: width / 30,
		backgroundColor: "#eeeff5",
		borderRadius: width / 50
	},
	clubDetailText: {
		padding: width / 30
	},
	editImage: {
		width: width / 20,
		height: width / 20
	},
	clubHeaderText: {
		// width: width / 3
	},
	rowView: {
		flexDirection: "row",
		alignItems: "center",
		height: height / 15,
		paddingHorizontal: width / 30
	},
	textInput: {
		width: width / 2,
		marginLeft: width / 50,
		color: "#9ca1ad"
	},
	imageView: {
		width: width / 7,
		height: width / 7,
		borderRadius: width / 14,
		marginLeft: width / 30,
		backgroundColor: "#fff",
		justifyContent: "center",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	header: {
		height: height / 8,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		backgroundColor: "#ffffff",
		...Platform.select({
			android: { elevation: 3 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.4
			}
		})
	},
	iconView: {
		width: width / 13,
		height: width / 13
	},
	headerLeftPart: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "flex-start",
		alignItems: "center"
	},
	headerText: {
		flex: 1,
		marginLeft: width / 30,
		height: height / 12,
		justifyContent: "center"
	},
	memberShipButton: {
		marginLeft: width / 50,
		marginRight: width / 50,
		width: width / 5,
		height: height / 25,
		justifyContent: "center",
		alignItems: "center",
		borderWidth: 1.5,
		borderRadius: width / 10,
		borderColor: "#414141"
	},
	membershipTextStyle: {
		width: width / 5,
		textAlign: "center"
	},
	bannerView: {
		width: width,
		height: width / 2,
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 2 },
			ios: {
				shadowOffset: { width: 2, height: 2 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	lessExpandableView: {
		width: width
	},
	scrollContentStyle: {
		justifyContent: "center",
		alignItems: "center"
	},
	expandableView: {
		width: width,
		height: height / 1.6,
		padding: width / 20
	},
	caretDownImage: {
		width: width / 15,
		height: width / 15
	},
	line: {
		width: width,
		height: height / 30,
		elevation: 5
	},
	caretUpImage: {
		width: width / 15,
		height: width / 15
	},
	backButton: {
		width: width / 8,
		height: height / 8,
		justifyContent: "center",
		alignItems: "center"
	}
});
