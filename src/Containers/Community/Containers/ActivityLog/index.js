import React, { Component } from "react";
import {
	View,
	SectionList,
	TouchableOpacity,
	Dimensions,
	Platform
} from "react-native";
import { connect } from "react-redux";

import { debounce } from "lodash";
import { styles } from "./styles";
import { communityGetActivityLog } from "./Saga";
import {
	CommunityTextBold,
	CommunityTextRegular,
	CommunityTextMedium
} from "../../Components/CommunityText";
import { Icon, EmptyView } from "../../../../Components";
import {
	communitySaveSinglePostDetail,
	communitySaveSinglePostComment
} from "../PostDetail/Saga";
import { getSince } from "../../../../utils/getSince";
import FastImage from "react-native-fast-image";
import { NOIMAGE } from "../../../../Assets/Img/Image";
import CommunityLoader from "../../Components/CommunityLoader";

const { width, height } = Dimensions.get("window");
const currentTimestamp = new Date().getTime();

class ActivityLog extends Component {
	isRefreshing = false;

	componentDidMount() {
		/**
		 * Check Activity log is fetched previously or not
		 */
		if (
			this.props.activityLog.Today.length === 0 &&
			this.props.activityLog["This Week"].length === 0 &&
			this.props.activityLog.Previous.length === 0
		) {
			this.props.dispatch(
				communityGetActivityLog({ appToken: this.props.appToken })
			);
		}
	}

	shouldComponentUpdate(nextProps, nextState) {
		console.log(nextProps);
		if (this.isRefreshing && !nextProps.isActivityLogLoading) {
			this.isRefreshing = false;
		}
		return true;
	}

	navigateToPostDetail = postId => {
		this.props.dispatch(communitySaveSinglePostDetail(null));
		this.props.dispatch(communitySaveSinglePostComment(null));

		this.props.navigation.navigate("PostDetail", {
			postId: postId
		});
	};

	navigateToUserProfile = userId => {
		if (userId !== this.props.userId) {
			const params = {
				userInfo: {
					user_id: userId
				}
			};
			this.props.navigation.navigate("UserProfile", params);
		}
	};

	_renderImage = url => {
		if (typeof url === "string" && url.startsWith("http")) {
			return (
				<FastImage
					style={{
						width: width / 8,
						height: width / 8,
						borderRadius: width / 16,
						justifyContent: "center",
						alignItems: "center"
					}}
					source={{ uri: url }}
				/>
			);
		}
		return (
			<FastImage
				style={{
					width: width / 8,
					height: width / 8,
					borderRadius: width / 16,
					justifyContent: "center",
					alignItems: "center"
				}}
				source={NOIMAGE}
			/>
		);
	};

	_renderItem = ({ item, index, section }) => {
		if (typeof item.additional_info === "string") {
			let additional_info = JSON.parse(item.additional_info);
			let imageString =
				additional_info.other_user_info &&
				additional_info.other_user_info.image;
			let isUserAndOtherUserSame =
				item.user_id === additional_info.other_user_id;
			let otherUserName;
			if (!isUserAndOtherUserSame) {
				otherUserName =
					additional_info &&
					additional_info.other_user_info &&
					additional_info.other_user_info.name
						? additional_info.other_user_info.name
						: "Community User";
			}
			if (item.activity_type === "COMMENTED_BY") {
				return (
					<View style={styles.activityLogItemContainer}>
						<View style={styles.activityLogItemImageContainer}>
							{this._renderImage(imageString)}
						</View>
						<View style={styles.activityLogItemTextContainer}>
							<View style={styles.activityLogItemTextDescriptionContainer}>
								{isUserAndOtherUserSame ? (
									<CommunityTextBold>{"You "}</CommunityTextBold>
								) : (
									<TouchableOpacity
										onPress={() =>
											this.navigateToUserProfile(additional_info.other_user_id)
										}>
										<CommunityTextBold>{`${otherUserName} `}</CommunityTextBold>
									</TouchableOpacity>
								)}
								<CommunityTextRegular>
									{"commented on your"}
								</CommunityTextRegular>
								<TouchableOpacity
									onPress={() =>
										this.navigateToPostDetail(additional_info.post_id)
									}>
									<CommunityTextBold>{" Post"}</CommunityTextBold>
								</TouchableOpacity>
							</View>
							<View>
								<CommunityTextBold color={"grey"} size={height / 80}>
									{getSince(
										currentTimestamp,
										new Date(item.created_at).getTime()
									)}
								</CommunityTextBold>
							</View>
						</View>
					</View>
				);
			}

			if (item.activity_type === "POST_LIKED_BY") {
				return (
					<View style={styles.activityLogItemContainer}>
						<View style={styles.activityLogItemImageContainer}>
							{this._renderImage(imageString)}
						</View>
						<View style={styles.activityLogItemTextContainer}>
							<View style={styles.activityLogItemTextDescriptionContainer}>
								{isUserAndOtherUserSame ? (
									<CommunityTextRegular>{"You "}</CommunityTextRegular>
								) : (
									<TouchableOpacity
										onPress={() =>
											this.navigateToUserProfile(additional_info.other_user_id)
										}>
										<CommunityTextBold>{`${otherUserName} `}</CommunityTextBold>
									</TouchableOpacity>
								)}
								<CommunityTextRegular>{"liked your "}</CommunityTextRegular>
								<TouchableOpacity
									onPress={() =>
										this.navigateToPostDetail(additional_info.post_id)
									}>
									<CommunityTextBold>{"Post"}</CommunityTextBold>
								</TouchableOpacity>
							</View>
							<View>
								<CommunityTextBold color={"grey"} size={height / 80}>
									{getSince(
										currentTimestamp,
										new Date(item.created_at).getTime()
									)}
								</CommunityTextBold>
							</View>
						</View>
					</View>
				);
			}

			if (item.activity_type === "COMMENTED") {
				return (
					<View style={styles.activityLogItemContainer}>
						<View style={styles.activityLogItemImageContainer}>
							{this._renderImage(imageString)}
						</View>
						<View style={styles.activityLogItemTextContainer}>
							<View style={styles.activityLogItemTextDescriptionContainer}>
								<CommunityTextRegular>
									{"You commented on "}
								</CommunityTextRegular>
								{isUserAndOtherUserSame ? (
									<CommunityTextRegular>{"your "}</CommunityTextRegular>
								) : (
									<TouchableOpacity
										onPress={() =>
											this.navigateToUserProfile(additional_info.other_user_id)
										}>
										<CommunityTextBold>
											{`${otherUserName}'s `}
										</CommunityTextBold>
									</TouchableOpacity>
								)}
								<TouchableOpacity
									onPress={() =>
										this.navigateToPostDetail(additional_info.post_id)
									}>
									<CommunityTextBold>{"Post"}</CommunityTextBold>
								</TouchableOpacity>
							</View>
							<View>
								<CommunityTextBold color={"grey"} size={height / 80}>
									{getSince(
										currentTimestamp,
										new Date(item.created_at).getTime()
									)}
								</CommunityTextBold>
							</View>
						</View>
					</View>
				);
			}

			if (item.activity_type === "POST_LIKED") {
				return (
					<View style={styles.activityLogItemContainer}>
						<View style={styles.activityLogItemImageContainer}>
							{this._renderImage(imageString)}
						</View>
						<View style={styles.activityLogItemTextContainer}>
							<View style={styles.activityLogItemTextDescriptionContainer}>
								<CommunityTextRegular>{"You liked "} </CommunityTextRegular>
								{isUserAndOtherUserSame ? (
									<CommunityTextRegular>{"your "}</CommunityTextRegular>
								) : (
									<TouchableOpacity
										onPress={() =>
											this.navigateToUserProfile(additional_info.other_user_id)
										}>
										<CommunityTextBold>
											{`${otherUserName}'s `}
										</CommunityTextBold>
									</TouchableOpacity>
								)}
								<TouchableOpacity
									onPress={() =>
										this.navigateToPostDetail(additional_info.post_id)
									}>
									<CommunityTextBold>{"Post"}</CommunityTextBold>
								</TouchableOpacity>
							</View>
							<View>
								<CommunityTextBold color={"grey"} size={height / 80}>
									{getSince(
										currentTimestamp,
										new Date(item.created_at).getTime()
									)}
								</CommunityTextBold>
							</View>
						</View>
					</View>
				);
			}

			if (item.activity_type === "FOLLOWING") {
				return (
					<View style={styles.activityLogItemContainer}>
						<View style={styles.activityLogItemImageContainer}>
							{this._renderImage(imageString)}
						</View>
						<View style={styles.activityLogItemTextContainer}>
							<View style={styles.activityLogItemTextDescriptionContainer}>
								<CommunityTextRegular>
									{"You started following "}
								</CommunityTextRegular>
								<TouchableOpacity
									onPress={() =>
										this.navigateToUserProfile(additional_info.other_user_id)
									}>
									<CommunityTextBold>{otherUserName}</CommunityTextBold>
								</TouchableOpacity>
							</View>
							<View>
								<CommunityTextBold color={"grey"} size={height / 80}>
									{getSince(
										currentTimestamp,
										new Date(item.created_at).getTime()
									)}
								</CommunityTextBold>
							</View>
						</View>
					</View>
				);
			}

			if (item.activity_type === "FOLLOWED_BY") {
				return (
					<View style={styles.activityLogItemContainer}>
						<View style={styles.activityLogItemImageContainer}>
							{this._renderImage(imageString)}
						</View>
						<View style={styles.activityLogItemTextContainer}>
							<View style={styles.activityLogItemTextDescriptionContainer}>
								<TouchableOpacity
									onPress={() =>
										this.navigateToUserProfile(additional_info.other_user_id)
									}>
									<CommunityTextBold>{`${otherUserName} `}</CommunityTextBold>
								</TouchableOpacity>
								<CommunityTextRegular>
									{"started following you"}
								</CommunityTextRegular>
							</View>
							<View>
								<CommunityTextBold color={"grey"} size={height / 80}>
									{getSince(
										currentTimestamp,
										new Date(item.created_at).getTime()
									)}
								</CommunityTextBold>
							</View>
						</View>
					</View>
				);
			}

			// Uncomment when below features are added
			// if (item.activity_type === "COMMENT_LIKED_BY") {
			// 	return null
			// }

			// if (item.activity_type === "COMMENT_LIKED") {
			// 	return null
			// }
		}

		return null;
	};

	_renderListEmptyComponent = () => {
		if (
			!this.props.isActivityLogLoading &&
			!this.props.isActivityLogError &&
			this.props.activityLog.Today.length === 0 &&
			this.props.activityLog["This Week"].length === 0 &&
			this.props.activityLog.Previous.length === 0
		) {
			return (
				<EmptyView
					showIcon={false}
					sorryText={{ color: "#606060", fontWeight: "bold" }}
					sorryMessage={"MEX. Community Activity Log"}
					noRecordText={{
						color: "#9a9a9a",
						fontWeight: "200",
						textAlign: "center",
						marginTop: width / 50
					}}
					noRecordFoundMesage={
						"Here you will see your notifications and activity related to community"
					}
					isRefresh={false}
				/>
			);
		}
		if (
			this.props.isActivityLogError &&
			this.props.activityLog.Today.length === 0 &&
			this.props.activityLog["This Week"].length === 0 &&
			this.props.activityLog.Previous.length === 0
		) {
			return (
				<EmptyView
					noSorry={true}
					noRecordFoundMesage={"Something went wrong!"}
					onRefresh={() =>
						this.props.dispatch(
							communityGetActivityLog({ appToken: this.props.appToken })
						)
					}
				/>
			);
		}

		return null;
	};

	_renderListFooterComponent = () => {
		if (this.props.isActivityLogLoading) {
			return <CommunityLoader />;
		}
		return null;
	};

	handleOnEndReached = debounce(info => {
		console.log("on end reached");
		if (info.distanceFromEnd >= -10 && this.props.offset !== null) {
			this.props.dispatch(
				communityGetActivityLog({
					appToken: this.props.appToken,
					offset: this.props.offset
				})
			);
		}
		this.onEndReachedCalledDuringMomentum = true;
	}, 500);

	handleRefresh = () => {
		this.isRefreshing = true;
		this.props.dispatch(
			communityGetActivityLog({
				appToken: this.props.appToken,
				isRefresh: true
			})
		);
	};

	render() {
		return (
			<View style={{ backgroundColor: "#fff", flex: 1 }}>
				<View
					style={{
						height: height / 12,
						alignItems: "center",
						justifyContent: "center",
						flexDirection: "row",
						elevation: 3,
						zIndex: 1,
						backgroundColor: "#fff",
						shadowOpacity: 0.2,
						shadowColor: "#000",
						shadowRadius: 2,
						shadowOffset: {
							height: 1,
							width: 1
						}
					}}>
					<TouchableOpacity
						onPress={() => this.props.navigation.goBack()}
						style={{
							left: 0,
							position: "absolute",
							justifyContent: "center",
							alignItems: "center",
							height: height / 12,
							aspectRatio: 1
						}}>
						<Icon
							iconType={"ionicon"}
							iconSize={height / 30}
							iconName={Platform.OS === "android" ? "md-close" : "ios-close"}
							iconColor={"#000"}
						/>
					</TouchableOpacity>
					<CommunityTextMedium
						style={{ fontSize: height / 40, color: "#161F3D" }}>
						Activity Log
					</CommunityTextMedium>
				</View>
				<SectionList
					onRefresh={this.handleRefresh}
					refreshing={this.isRefreshing}
					contentContainerStyle={{
						paddingHorizontal: width / 25,
						flexGrow: 1
					}}
					extraData={[
						this.props.activityLog,
						this.props.isActivityLogError,
						this.props.isActivityLogLoading
					]}
					stickySectionHeadersEnabled={true}
					onEndReachedThreshold={1}
					onEndReached={this.handleOnEndReached}
					renderItem={this._renderItem}
					renderSectionHeader={({ section: { title, data } }) => {
						if (data.length === 0) {
							return null;
						}
						return (
							<View
								style={{
									backgroundColor: "#ffffff",
									paddingVertical: height / 60
								}}>
								<CommunityTextRegular
									style={{
										fontSize: height / 50,
										color: "#000000"
									}}>
									{title}
								</CommunityTextRegular>
							</View>
						);
					}}
					sections={Object.keys(this.props.activityLog).map(
						(activity_section, index) => ({
							title: `${activity_section}`,
							data: this.props.activityLog[activity_section],
							index
						})
					)}
					ListHeaderComponent={this._renderListEmptyComponent}
					keyExtractor={(item, index) => item + index}
				/>
			</View>
		);
	}
}

const mapStateToProps = state => ({
	appToken: state.appToken.token,
	activityLog: state.community && state.community.activityLog.activityLog,
	isActivityLogLoading:
		state.community && state.community.activityLog.isActivityLogLoading,
	isActivityLogError:
		state.community && state.community.activityLog.isActivityLogError,
	offset: state.community && state.community.activityLog.offset,
	userId: state.community && state.community.profile.myProfile.user_id
});

export default connect(mapStateToProps)(ActivityLog);
