import { takeLatest, put, call } from "redux-saga/effects";
import Api from "../../Api";
import { getMonthText } from "../../../../utils/dateUtils";

// Constants
export const COMMUNITY_GET_ACTIVITY_LOG = "COMMUNITY_GET_ACTIVITY_LOG";
export const COMMUNITY_SAVE_ACTIVITY_LOG = "COMMUNITY_SAVE_ACTIVITY_LOG";
export const COMMUNITY_SAVE_ACTIVITY_LOG_ERROR =
	"COMMUNITY_SAVE_ACTIVITY_LOG_ERROR";

// Action Creators
export const communityGetActivityLog = payload => ({
	type: COMMUNITY_GET_ACTIVITY_LOG,
	payload
});

export const communitySaveActivityLog = payload => ({
	type: COMMUNITY_SAVE_ACTIVITY_LOG,
	payload
});

export const communitySaveActivityLogError = payload => ({
	type: COMMUNITY_SAVE_ACTIVITY_LOG_ERROR,
	payload
});

/**
 * Saga
 * @param {any} dispatch
 */

export function* communityActivityLogSaga(dispatch) {
	yield takeLatest(COMMUNITY_GET_ACTIVITY_LOG, handleCommunityGetActivityLog);
}

function* handleCommunityGetActivityLog(action) {
	try {
		let response = yield call(Api.getActivityLog, action.payload);
		let offsetCheck =
			typeof action.payload.offset === "string"
				? action.payload.offset !== response.offset
				: true;
		let isRefresh = action.payload.isRefresh;
		if (offsetCheck) {
			if (response.success === 1) {
				let activityLog = {
					Today: [],
					"This Week": [],
					Previous: []
				};
				response.data.length > 0 &&
					response.data.forEach(activity => {
						let today = new Date().getDate();
						let dayIndex = new Date(activity.created_at).getDate();

						if (dayIndex === today) {
							activityLog.Today.push(activity);
						} else if (today - dayIndex < 7) {
							activityLog["This Week"].push(activity);
						} else {
							activityLog.Previous.push(activity);
						}
					});
				yield put(
					communitySaveActivityLog({
						activityLog: activityLog,
						offset: response.offset,
						...(isRefresh ? { isRefresh } : {})
					})
				);
			} else {
				throw new Error("unable to fetch activity");
			}
		}
	} catch (error) {
		yield put(communitySaveActivityLogError(true));
	}
}
