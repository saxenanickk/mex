import {
	COMMUNITY_GET_ACTIVITY_LOG,
	COMMUNITY_SAVE_ACTIVITY_LOG,
	COMMUNITY_SAVE_ACTIVITY_LOG_ERROR
} from "./Saga";

const initialState = {
	isActivityLogLoading: false,
	isActivityLogError: false,
	activityLog: {
		Today: [],
		"This Week": [],
		Previous: []
	},
	offset: null
};

export const reducer = (state = initialState, { type, payload }) => {
	switch (type) {
		case COMMUNITY_GET_ACTIVITY_LOG:
			return {
				...state,
				isActivityLogLoading: true,
				isActivityLogError: false
			};

		case COMMUNITY_SAVE_ACTIVITY_LOG:
			let activityLog = { ...state.activityLog };

			if (!payload.isRefresh) {
				activityLog.Today = [
					...activityLog.Today,
					...payload.activityLog.Today
				];
				activityLog["This Week"] = [
					...activityLog["This Week"],
					...payload.activityLog["This Week"]
				];
				activityLog.Previous = [
					...activityLog.Previous,
					...payload.activityLog.Previous
				];
			} else {
				activityLog = payload.activityLog;
			}

			return {
				...state,
				isActivityLogLoading: false,
				activityLog: activityLog,
				offset: payload.offset
			};

		case COMMUNITY_SAVE_ACTIVITY_LOG_ERROR:
			return {
				...state,
				isActivityLogLoading: false,
				isActivityLogError: true
			};

		default:
			return state;
	}
};
