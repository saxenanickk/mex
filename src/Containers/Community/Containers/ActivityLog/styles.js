import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	activityLogItemContainer: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		borderBottomWidth: 0.5,
		borderBottomColor: "#e8e9eb"
	},
	activityLogItemImageContainer: {
		marginVertical: width / 30,
		alignItems: "center"
	},
	activityLogItemTextContainer: {
		flex: 1,
		marginLeft: width / 20,
		alignItems: "flex-start",
		justifyContent: "flex-start",
		flexWrap: "wrap"
	},
	activityLogItemTextDescriptionContainer: {
		flexWrap: "wrap",
		flexDirection: "row"
	}
});
