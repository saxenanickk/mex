import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
	container: { flex: 1 },
	headerBar: {
		width: width,
		backgroundColor: "#fff",
		justifyContent: "space-between",
		alignItems: "center",
		flexDirection: "row",
		height: height / 13.33,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "#000",
				shadowOpacity: 0.3
			}
		})
	},
	goBackButton: {
		padding: width / 30
	},
	centeredRow: { flexDirection: "row", alignItems: "center" },
	bookText: {
		fontSize: height / 50,
		width: width / 1.5,
		marginLeft: width / 26.3,
		color: "#000"
	}
});
