import React, { Component } from "react";
import { TouchableOpacity, View, Dimensions, BackHandler } from "react-native";
import { WebView } from "react-native-webview";
import { connect } from "react-redux";
import { Icon, ProgressBar } from "../../../../Components";
import styles from "./style";
import I18n from "../../Assets/Strings/i18n";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import { CommunityTextRegular } from "../../Components/CommunityText";

const { width, height } = Dimensions.get("window");
class LinkView extends Component {
	constructor(props) {
		super(props);
		this.state = {
			barProgress: 100
		};
		this.navigationStateChange = this.navigationStateChange.bind(this);
		this.webViewRef = null;
		this.goBackInWebView = this.goBackInWebView.bind(this);
		this.canGoBack = false;
		this.interval = null;
		this.loadingStarted = false;
		BackHandler.addEventListener("hardwareBackPress", this.goBackInWebView);
	}

	navigationStateChange = event => {
		this.currentUrl = event.url;
		console.log("navigation state change", event);
		if (event.loading === true) {
			if (this.loadingStarted === false) {
				this.setState({ barProgress: 0 });
				this.startProgress();
				this.loadingStarted = true;
			}
		} else {
			this.setState({ barProgress: 100 });
			clearInterval(this.interval);
			this.loadingStarted = false;
		}
		this.canGoBack = event.canGoBack;
	};

	async goBackInWebView() {
		try {
			if (this.webViewRef !== null) {
				if (this.canGoBack === false) {
					throw new Error("GoTo Community");
				} else {
					if (
						this.webViewRef.props.source.uri === "https://www.mexit.in/faqs/"
					) {
						this.props.navigation.goBack();
					} else {
						this.webViewRef.goBack();
						this.setState({ barProgress: 100 });
					}
				}
			}
			return true;
		} catch (error) {
			this.props.navigation.goBack();
			return true;
		}
	}

	componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.goBackInWebView);
		if (this.interval !== null) {
			clearInterval(this.interval);
		}
	}

	startProgress() {
		let self = this;
		this.interval = setInterval(() => {
			if (self.state.barProgress <= 100) {
				self.setState({ barProgress: self.state.barProgress + 5 });
			}
		}, 100);
	}
	render() {
		const { route } = this.props;
		const { title, url } = route.params ? route.params : {};

		return (
			<View style={styles.container}>
				<View style={styles.headerBar}>
					<View style={styles.centeredRow}>
						<TouchableOpacity
							style={styles.goBackButton}
							onPress={() => this.goBackInWebView()}>
							<Icon
								iconType={"ionicon"}
								iconName={"ios-arrow-back"}
								iconSize={width / 16}
								iconColor={"#000"}
							/>
						</TouchableOpacity>
						<CommunityTextRegular style={styles.bookText} numberOfLines={1}>
							{title ? title : url}
						</CommunityTextRegular>
					</View>
				</View>
				{this.state.barProgress !== 100 ? (
					<ProgressBar
						width={width / 5}
						height={height / 200}
						value={this.state.barProgress}
						backgroundColor="#000"
						barEasing={"linear"}
					/>
				) : null}

				<WebView
					source={{ uri: this.props.route.params.url }}
					onNavigationStateChange={this.navigationStateChange}
					thirdPartyCookiesEnabled={true}
					ref={ref => (this.webViewRef = ref)}
				/>
			</View>
		);
	}
}

function mapStateToProps(state) {
	console.log(state);
	return {};
}

export default connect(
	mapStateToProps,
	null,
	null
)(LinkView);
