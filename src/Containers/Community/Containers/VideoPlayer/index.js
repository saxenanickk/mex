import React, { Component } from "react";
import { Dimensions, StyleSheet } from "react-native";
import { connect } from "react-redux";
import { GoappVideoPlayer } from "../../../../Components";

const { height } = Dimensions.get("window");

class VideoPlayer extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		const { route } = props;
		const { videoUri } = route.params ? route.params : {};
		this.videoUri = videoUri;
	}

	render() {
		return (
			<GoappVideoPlayer
				source={{
					uri: this.videoUri,
					cache: true
				}}
				ref={ref => {
					this.player = ref;
				}}
				controls={true}
				style={styles.container}
				goBack={() => this.props.navigation.goBack()}
				onEnd={() => {
					if (this.player !== null && this.player !== undefined) {
						this.player.seekTo(0);
					}
				}}
			/>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		position: "absolute",
		top: 0,
		left: 0,
		bottom: height / 20,
		right: 0
	}
});

function mapStateToProps() {
	return {};
}

export default connect(mapStateToProps)(VideoPlayer);
