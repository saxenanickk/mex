import {
	COMMUNITY_SAVE_MATCHING_CONNECTION,
	COMMUNITY_UPDATE_MATCHING_CONNECTION,
	COMMUNITY_SAVE_MATCHING_CONNECTION_OFFSET,
	COMMUNITY_SAVE_CONNECT_FILTER,
	COMMUNITY_RESET_CONNECT_FILTER
} from "./Saga";
import { COMMUNITY_FOLLOW_UNFOLLOW_USER } from "../PostDetail/Saga";

const initialState = {
	matchingConnections: null,
	matchingConnectionOffset: null,
	selectedFilterAttribute: [],
	filterAttribute: [
		"hobbies_interests",
		"education",
		"current_organisation",
		"site_id",
		"home_town",
		"skills"
	]
};

/**
 * @param {{ type: string; payload: any; }} action
 */
export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case COMMUNITY_SAVE_MATCHING_CONNECTION:
			return {
				...state,
				matchingConnections: action.payload
			};
		case COMMUNITY_UPDATE_MATCHING_CONNECTION:
			let temp = state.matchingConnections.slice();
			temp.concat(action.payload);
			return {
				...state,
				matchingConnections: temp
			};
		case COMMUNITY_SAVE_MATCHING_CONNECTION_OFFSET:
			return {
				...state,
				matchingConnectionOffset: action.payload
			};
		case COMMUNITY_SAVE_CONNECT_FILTER:
			return {
				...state,
				selectedFilterAttribute: action.payload
			};
		case COMMUNITY_RESET_CONNECT_FILTER:
			return {
				...state,
				selectedFilterAttribute: []
			};
		case COMMUNITY_FOLLOW_UNFOLLOW_USER:
			console.log(action.payload);
			if (state.matchingConnections) {
				let updatedConnections = state.matchingConnections.map(connection => {
					if (connection.record.user_id === action.payload.userId) {
						connection.is_Following = !action.payload.isFollowing;
					}
					return connection;
				});
				return {
					...state,
					matchingConnections: updatedConnections
				};
			}
			return state;
		default:
			return state;
	}
};
