import React from "react";
import { StyleSheet, Dimensions, Platform } from "react-native";
const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff"
	},
	header: {
		flexDirection: "row",
		backgroundColor: "#fff",
		justifyContent: "space-between",
		paddingHorizontal: width / 30,
		width: width,
		height: height / 15,
		alignItems: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	backButton: {
		width: width / 10,
		alignItems: "flex-start",
		justifyContent: "center",
		height: height / 15
	},
	textSection: {
		flexDirection: "row",
		width: width * 0.83,
		height: height / 15,
		justifyContent: "space-between",
		alignItems: "center"
	},
	filterSection: {
		flexDirection: "row",
		width: width / 3,
		height: height / 15,
		justifyContent: "flex-end",
		alignItems: "center"
	},
	rotateIcon: {
		transform: [{ rotate: "90deg" }]
	},
	filterText: {
		marginLeft: width / 50
	},
	matchingList: {
		height: height,
		marginTop: height / 60
	},
	matchingConnectionSection: {
		width: width * 0.94,
		alignSelf: "center",
		backgroundColor: "#f1f0f7",
		paddingHorizontal: width / 30,
		paddingVertical: height / 60,
		borderRadius: width / 60,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between"
	},
	listSeparator: {
		width: width,
		height: height / 60
	},
	userImage: {
		width: width / 6,
		height: width / 6,
		borderRadius: width / 12,
		justifyContent: "center",
		alignItems: "center"
	},
	noUserImage: {
		width: width / 6,
		height: width / 6,
		borderRadius: width / 12,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#fff"
	},
	connectionInfoView: {
		width: width * 0.55
	},
	connectionInfoSection: {
		flexDirection: "row",
		alignItems: "center"
	},
	nameText: {
		width: width * 0.55
	},
	matchingPercentageView: {
		width: width * 0.55,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		marginTop: height / 150
	},
	matchingProgressSection: {
		width: width * 0.55,
		backgroundColor: "#fff",
		height: height / 120,
		marginTop: height / 100,
		borderRadius: width / 30
	},
	matchingProgressMarkSection: {
		width: 0,
		backgroundColor: "#000",
		height: height / 120,
		borderRadius: width / 30
	},
	editImage: {
		width: width / 20,
		height: width / 20
	},
	filterHeaderText: {
		paddingLeft: width / 30,
		paddingVertical: height / 60
	},
	filterOptionButton: {
		flexDirection: "row",
		justifyContent: "space-between",
		width: width * 0.93,
		height: height / 20,
		alignItems: "center"
	},
	separator: {
		width: width * 0.93,
		height: 1,
		backgroundColor: "rgba(0,0,0,0.3)"
	},
	applyButton: {
		width: width,
		position: "absolute",
		bottom: 0,
		backgroundColor: "#2C98F0",
		height: height / 15,
		justifyContent: "center",
		alignItems: "center"
	}
});
