import React, { Component } from "react";
import { View, Dimensions, TouchableOpacity, FlatList } from "react-native";
import { connect } from "react-redux";
import { Icon } from "../../../../../Components";
import { styles } from "./style";
import {
	CommunityTextMedium,
	CommunityTextRegular
} from "../../../Components/CommunityText";
import { remove } from "lodash";
import { communitySaveConnectFilter } from "../Saga";
const { width, height } = Dimensions.get("window");

const localFilterAttribute = {
	hobbies_interests: "Hobbies & Interests",
	education: "Education",
	current_organisation: "Current organizations",
	site_id: "Site",
	home_town: "Home town",
	skills: "Skills"
};
class Filter extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filterOptions: props.selectedFilterAttribute
		};
	}

	toggleFilterOption = (selectedFilterValue, index) => {
		try {
			let temp = this.state.filterOptions.slice();
			if (this.state.filterOptions.indexOf(selectedFilterValue) >= 0) {
				remove(temp, item => item === selectedFilterValue);
			} else {
				temp.push(selectedFilterValue);
			}
			this.setState({ filterOptions: temp });
		} catch (error) {
			return null;
		}
	};
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<TouchableOpacity
						style={styles.backButton}
						onPress={() => this.props.navigation.goBack()}>
						<Icon
							iconType={"feather"}
							iconSize={width / 18}
							iconName={"arrow-left"}
							iconColor={"#000"}
						/>
					</TouchableOpacity>
					<View style={styles.textSection}>
						<CommunityTextMedium size={height / 45}>
							{"Filter By Attributes"}
						</CommunityTextMedium>
					</View>
				</View>
				<View style={styles.filterHeaderText}>
					<FlatList
						extraData={this.state}
						style={{ height: height }}
						keyExtractor={(item, index) => index.toString()}
						data={this.props.filterAttribute}
						renderItem={({ item, index }) => {
							return (
								<TouchableOpacity
									style={styles.filterOptionButton}
									onPress={() => this.toggleFilterOption(item, index)}>
									<CommunityTextRegular size={height / 50}>
										{localFilterAttribute[item]}
									</CommunityTextRegular>
									{this.state.filterOptions.indexOf(item) >= 0 ? (
										<Icon
											iconType={"ionicon"}
											iconSize={width / 18}
											iconName={"ios-checkmark-circle"}
											iconColor={"#2C98F0"}
										/>
									) : (
										<Icon
											iconType={"ionicon"}
											iconSize={width / 18}
											iconName={"ios-radio-button-off"}
											iconColor={"#2C98F0"}
										/>
									)}
								</TouchableOpacity>
							);
						}}
					/>
				</View>
				<TouchableOpacity
					style={styles.applyButton}
					onPress={() => {
						this.props.dispatch(
							communitySaveConnectFilter(this.state.filterOptions)
						);
						this.props.navigation.goBack();
					}}>
					<CommunityTextMedium color={"#fff"} size={height / 50}>
						{"Apply"}
					</CommunityTextMedium>
				</TouchableOpacity>
			</View>
		);
	}
}

function mapStateToProps(state) {
	console.log("state is", state);
	return {
		appToken: state.appToken.token,
		error: state.community && state.community.error,
		selectedFilterAttribute:
			state.community &&
			state.community.connectionMatchReducer.selectedFilterAttribute,
		filterAttribute:
			state.community && state.community.connectionMatchReducer.filterAttribute
	};
}

export default connect(mapStateToProps)(Filter);
