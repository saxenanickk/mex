import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api";
import { communitySaveError } from "../../Saga";

export const COMMUNITY_GET_MATCHING_CONNECTION =
	"COMMUNITY_GET_MATCHING_CONNECTION";
export const COMMUNITY_SAVE_MATCHING_CONNECTION =
	"COMMUNITY_SAVE_MATCHING_CONNECTION";
export const COMMUNITY_UPDATE_MATCHING_CONNECTION =
	"COMMUNITY_UPDATE_MATCHING_CONNECTION";
export const COMMUNITY_SAVE_MATCHING_CONNECTION_OFFSET =
	"COMMUNITY_SAVE_MATCHING_CONNECTION_OFFSET";
export const COMMUNITY_SAVE_CONNECT_FILTER = "COMMUNITY_SAVE_CONNECT_FILTER";
export const COMMUNITY_RESET_CONNECT_FILTER = "COMMUNITY_RESET_CONNECT_FILTER";
export const COMMUNITY_GET_CONNECT_FILTER = "COMMUNITY_GET_CONNECT_FILTER";
export const COMMUNITY_UPDATE_FOLLOW_UNFOLLOW_LOCAL =
	"COMMUNITY_UPDATE_FOLLOW_UNFOLLOW_LOCAL";
export const COMMUNITY_FOLLOW_UNFOLLOW_CONNECT_USER =
	"COMMUNITY_FOLLOW_UNFOLLOW_CONNECT_USER";

export const communityGetMatchingConnection = payload => ({
	type: COMMUNITY_GET_MATCHING_CONNECTION,
	payload
});

export const communitySaveMatchingConnection = payload => ({
	type: COMMUNITY_SAVE_MATCHING_CONNECTION,
	payload
});

export const communityUpdateMatchingConnection = payload => ({
	type: COMMUNITY_UPDATE_MATCHING_CONNECTION,
	payload
});

export const communitySaveMatchingConnectionOffset = payload => ({
	type: COMMUNITY_SAVE_MATCHING_CONNECTION_OFFSET,
	payload
});

export const communitySaveConnectFilter = payload => ({
	type: COMMUNITY_SAVE_CONNECT_FILTER,
	payload
});

export const communityResetConnectFIlter = payload => ({
	type: COMMUNITY_RESET_CONNECT_FILTER,
	payload
});

export const communityGetConnectFilter = payload => ({
	type: COMMUNITY_GET_CONNECT_FILTER,
	payload
});

export const communityUpdateFollowUnfollowLocal = payload => ({
	type: COMMUNITY_UPDATE_FOLLOW_UNFOLLOW_LOCAL,
	payload
});

export const communityFollowUnfollowConnectUser = payload => ({
	type: COMMUNITY_FOLLOW_UNFOLLOW_CONNECT_USER,
	payload
});

/**
 * Saga
 * @param {any} dispatch
 */
export function* communityConnectionMatchSaga(dispatch) {
	yield takeLatest(
		COMMUNITY_GET_MATCHING_CONNECTION,
		handleCommunityGetMatchingConnection
	);
	yield takeLatest(
		COMMUNITY_GET_CONNECT_FILTER,
		handleCommunityGetConnectFilter
	);
	yield takeEvery(
		COMMUNITY_FOLLOW_UNFOLLOW_CONNECT_USER,
		handleCommunityFollowUnfollowConnectUser
	);
}

function* handleCommunityGetMatchingConnection(action) {
	try {
		let response = yield call(Api.getMatchingConnections, action.payload);
		console.log("response is", response);
		if (response.success && response.success === 1) {
			yield put(communitySaveMatchingConnection(response.data));
		} else {
			throw new Error("Unable to get Matching Connection");
		}
	} catch (error) {
		console.log("error is", error);
		yield put(
			communitySaveError({
				errorScreen: "ConnectionList",
				errorMessage: "Unable to get Matching Connection",
				isError: true
			})
		);
	}
}

function* handleCommunityGetConnectFilter(action) {
	try {
		let response = yield call(Api.getMatchingConnections, action.payload);
		console.log("response is", response);
		if (response.data && response.data.length > 0) {
			yield put(communitySaveMatchingConnection(response.data));
		} else {
			throw new Error("Unable to get Matching Connection");
		}
	} catch (error) {
		yield put(
			communitySaveError({
				errorScreen: "ConnectionList",
				errorMessage: "Unable to get Matching Connection",
				isError: true
			})
		);
	}
}

function* handleCommunityFollowUnfollowConnectUser(action) {
	try {
		let response = yield call(Api.followUnfollowUser, action.payload);
		if (response.success === 1) {
			yield put(communityGetMatchingConnection(action.payload));
		} else {
			throw new Error("Unable to follow the user");
		}
	} catch (error) {
		console.log("error is", error);
		yield put(
			communitySaveError({
				errorScreen: "ConnectionList",
				errorMessage: action.payload.isFollowing
					? "unable to unfollow " + action.payload.userName
					: "unable to follow " + action.payload.userName,
				isError: true
			})
		);
	}
}
