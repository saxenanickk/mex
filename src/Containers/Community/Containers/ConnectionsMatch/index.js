import React from "react";
import ConnectionList from "./ConnectionList";
import Filter from "./Filter";
import { Stack } from "../../../../utils/Navigators";

const ConnectionsMatch = () => (
	<Stack.Navigator headerMode={"none"}>
		<Stack.Screen name={"ConnectionList"} component={ConnectionList} />
		<Stack.Screen name={"Filter"} component={Filter} />
	</Stack.Navigator>
);

export default ConnectionsMatch;
