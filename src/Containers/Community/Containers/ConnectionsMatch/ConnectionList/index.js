import React, { Component } from "react";
import {
	View,
	Dimensions,
	Alert,
	TouchableOpacity,
	FlatList,
	TextInput
} from "react-native";
import { connect } from "react-redux";
import { StackActions } from "@react-navigation/native";
import { Icon } from "../../../../../Components";
import { styles } from "./style";
import { CommunityTextMedium } from "../../../Components/CommunityText";
import I18n from "../../../Assets/Strings/i18n";
import CommunityLoader from "../../../Components/CommunityLoader";
import {
	communityGetMatchingConnection,
	communitySaveMatchingConnection,
	communityFollowUnfollowConnectUser,
	communitySaveConnectFilter
} from "../Saga";
import { communityResetError } from "../../../Saga";
import MatchingConnectioncard from "../../../Components/MatchingConnectionCard";
import { EmptyView } from "../../../../../Components";

const { width, height } = Dimensions.get("window");

class ConnectionList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			searchQuery: ""
		};
		this.followUnfollowUser = this.followUnfollowUser.bind(this);
		this.onPressCard = this.onPressCard.bind(this);
	}

	componentDidMount() {
		this.props.dispatch(
			communityGetMatchingConnection({
				appToken: this.props.appToken
			})
		);
	}

	componentWillUnmount() {
		this.props.dispatch(communitySaveMatchingConnection(null));
		this.props.dispatch(communitySaveConnectFilter([]));
	}

	shouldComponentUpdate(props, state) {
		if (
			props.selectedFilterAttribute &&
			props.selectedFilterAttribute !== this.props.selectedFilterAttribute
		) {
			this.filterConnectionList(
				props.selectedFilterAttribute,
				this.state.searchQuery
			);
		}
		if (
			props.error &&
			props.error !== this.props.error &&
			props.error.isError &&
			props.error.errorScreen === "ConnectionList"
		) {
			Alert.alert(I18n.t("error"), props.error.errorMessage);
			this.props.dispatch(communityResetError());

			this.props.navigation.navigate("Home");
		}
		return true;
	}

	filterConnectionList = (filter, keyword) => {
		this.props.dispatch(communitySaveMatchingConnection(null));
		this.props.dispatch(
			communityGetMatchingConnection({
				appToken: this.props.appToken,
				filter: filter,
				keyword: keyword
			})
		);
	};

	getMatchingPercentage = item => {
		try {
			let numberOfMatchingAttribute = Object.keys(item.matched).length;
			let percentage = parseInt(
				(numberOfMatchingAttribute * 100) / this.props.filterAttribute.length
			);
			return percentage;
		} catch (error) {
			return 0;
		}
	};

	followUnfollowUser = item => {
		try {
			this.props.dispatch(
				communityFollowUnfollowConnectUser({
					appToken: this.props.appToken,
					isFollowing: item.is_Following,
					userId: item.record.user_id,
					userNme: item.record.name,
					filter: this.props.selectedFilterAttribute,
					keyword: this.state.searchQuery
				})
			);
		} catch (error) {
			console.log("error in following unfolowing is", error);
		}
	};

	onPressCard = item => {
		this.props.navigation.dispatch(
			StackActions.push("UserProfile", {
				userInfo: {
					user_id: item.record.user_id
				}
			})
		);
	};

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<TouchableOpacity
						style={styles.backButton}
						onPress={() => this.props.navigation.navigate("Home")}>
						<Icon
							iconType={"feather"}
							iconSize={width / 18}
							iconName={"arrow-left"}
							iconColor={"#000"}
						/>
					</TouchableOpacity>
					<View style={styles.textSection}>
						<CommunityTextMedium size={height / 45}>
							{"Connections Found"}
						</CommunityTextMedium>
						<TouchableOpacity
							style={styles.filterSection}
							onPress={() => this.props.navigation.navigate("Filter")}>
							<CommunityTextMedium
								size={height / 55}
								style={styles.filterText}
								color={"#000"}>
								{"Filter"}
							</CommunityTextMedium>
						</TouchableOpacity>
					</View>
				</View>
				<View style={styles.inputField}>
					<View style={styles.rowView}>
						<TextInput
							underlineColorAndroid={"transparent"}
							placeholder={"Search by name or email"}
							style={styles.textInput}
							selectionColor={"#000"}
							onChangeText={text => {
								this.setState({ searchQuery: text });
								this.filterConnectionList(
									this.props.selectedFilterAttribute,
									text
								);
							}}
						/>
						<Icon
							style={styles.searchIcon}
							iconType={"feather"}
							iconSize={width / 18}
							iconName={"search"}
							iconColor={"#83C2F6"}
						/>
					</View>
				</View>
				{this.props.matchingConnections === null ? (
					<View style={styles.loaderView}>
						<CommunityLoader />
					</View>
				) : (
					<FlatList
						style={styles.matchingList}
						data={this.props.matchingConnections}
						extraData={this.props.matchingConnections}
						keyboardShouldPersistTaps={"always"}
						keyExtractor={(item, index) => index.toString()}
						renderItem={({ item }) => (
							<MatchingConnectioncard
								item={item}
								followUnfollowUser={this.followUnfollowUser}
								onPressCard={this.onPressCard}
							/>
						)}
						ItemSeparatorComponent={() => <View style={styles.listSeparator} />}
						ListEmptyComponent={() => (
							<EmptyView
								noRecordFoundMesage={"No matching connections found"}
								sorryMessage={"Sorry"}
								isRefresh={false}
							/>
						)}
					/>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	console.log("state is", state);
	return {
		appToken: state.appToken.token,
		error: state.community && state.community.error,
		matchingConnections:
			state.community &&
			state.community.connectionMatchReducer.matchingConnections,
		matchingConnectionOffset:
			state.community &&
			state.community.connectionMatchReducer.matchingConnectionOffset,
		selectedFilterAttribute:
			state.community &&
			state.community.connectionMatchReducer.selectedFilterAttribute,
		filterAttribute:
			state.community && state.community.connectionMatchReducer.filterAttribute
	};
}

export default connect(mapStateToProps)(ConnectionList);
