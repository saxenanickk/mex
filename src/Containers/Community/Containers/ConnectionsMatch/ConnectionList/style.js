import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#F2F2F2"
	},
	header: {
		flexDirection: "row",
		backgroundColor: "#fff",
		justifyContent: "space-between",
		paddingHorizontal: width / 30,
		width: width,
		height: height / 15,
		alignItems: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	backButton: {
		width: width / 10,
		alignItems: "flex-start",
		justifyContent: "center",
		height: height / 15
	},
	textSection: {
		flexDirection: "row",
		width: width * 0.83,
		height: height / 15,
		justifyContent: "space-between",
		alignItems: "center"
	},
	filterSection: {
		flexDirection: "row",
		paddingHorizontal: width / 15,
		height: height / 25,
		borderColor: "#2C98F0",
		borderWidth: 1,
		borderRadius: width / 20,
		justifyContent: "center",
		alignItems: "center"
	},
	rotateIcon: {
		transform: [{ rotate: "90deg" }]
	},
	matchingList: {
		height: height,
		marginTop: height / 60
	},
	matchingConnectionSection: {
		width: width * 0.94,
		paddingTop: height / 60,
		paddingBottom: height / 40,
		alignSelf: "center",
		backgroundColor: "#f1f0f7",
		paddingHorizontal: width / 30,
		borderRadius: width / 60,
		flexDirection: "row",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	listSeparator: {
		width: width,
		height: height / 60
	},
	userImage: {
		width: width / 6,
		height: width / 6,
		borderRadius: width / 12,
		justifyContent: "center",
		alignItems: "center"
	},
	noUserImage: {
		width: width / 6,
		height: width / 6,
		borderRadius: width / 12,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#fff"
	},
	connectionInfoView: {
		width: width * 0.65,
		marginLeft: width / 30
	},
	connectionInfoSection: {
		flexDirection: "row",
		alignItems: "center"
	},
	nameText: {
		width: width * 0.65
	},
	matchingPercentageView: {
		width: width * 0.65,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		marginTop: height / 150
	},
	matchingProgressSection: {
		width: width * 0.65,
		backgroundColor: "#fff",
		height: height / 120,
		marginTop: height / 100,
		borderRadius: width / 30
	},
	matchingProgressMarkSection: {
		width: 0,
		backgroundColor: "#000",
		height: height / 120,
		borderRadius: width / 30
	},
	editImage: {
		width: width / 20,
		height: width / 20
	},
	matchingConnectionFirstSection: {
		width: width,
		alignSelf: "center",
		backgroundColor: "#fff",
		borderRadius: width / 60,
		paddingBottom: height / 30
	},
	lowerButtonSection: {
		width: width * 0.9,
		alignItems: "flex-end",
		marginTop: height / 60,
		position: "absolute",
		bottom: width / 30
	},
	unfollowButton: {
		width: width / 4.5,
		height: height / 30,
		borderWidth: 2,
		borderColor: "#000",
		backgroundColor: "#fff",
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 30,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	followButton: {
		width: width / 4.5,
		height: height / 30,
		backgroundColor: "#000",
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 30,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	inputField: {
		width: width * 0.92,
		marginTop: height / 50,
		alignSelf: "center",
		justifyContent: "center",
		backgroundColor: "#FFFFFF",
		borderRadius: width / 10,
		...Platform.select({
			android: { elevation: 2 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	commentField: {
		width: width * 0.84,
		alignSelf: "center",
		marginTop: height / 50,
		height: height / 15,
		justifyContent: "center",
		paddingHorizontal: width / 30,
		backgroundColor: "#eeeff5",
		borderRadius: width / 50
	},
	rowView: {
		flexDirection: "row",
		alignItems: "center",
		paddingHorizontal: width / 30,
		height: height / 16
	},
	textInput: {
		width: width / 1.2,
		marginLeft: width / 50,
		fontSize: height / 65
	},
	loaderView: {
		height: height * 0.9,
		justifyContent: "center",
		alignItems: "center"
	},
	searchIcon: {
		right: width / 15
	}
});
