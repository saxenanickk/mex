import React, { Component } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import FastImage from "react-native-fast-image";
import Activity from "../Activity";
import Club from "../Club";
import { Icon } from "../../../../Components";
import { styles } from "./styles";
import _ from "lodash";
import { CommunityTextBold } from "../../Components/CommunityText";
import { TopTab } from "../../../../utils/Navigators";
import { communitySaveCurrentDisplayScreen } from "../../Saga";
import { scale } from "../../../../utils/scaling";
import { GoappTextBold } from "../../../../Components/GoappText";

const { width, height } = Dimensions.get("window");
const ACTIVE_TAB_COLOR = "#2C98F0";
const INACTIVE_TAB_COLOR = "grey";
const TAB_BACKGROUND_COLOR = "#FFF";
const LIGHT_GREY_COLOR = "#00000080";

const TAB_BAR_OPTIONS = {
	showIcon: true,
	activeTintColor: ACTIVE_TAB_COLOR,
	inactiveTintColor: INACTIVE_TAB_COLOR,
	backgroundColor: TAB_BACKGROUND_COLOR,
	indicatorStyle: {
		backgroundColor: ACTIVE_TAB_COLOR
	},
	style: {
		backgroundColor: TAB_BACKGROUND_COLOR
	},
	tabStyle: {
		flexDirection: "row"
	},
	iconStyle: {
		width: width / 14
	}
};
const ICON_SIZE = width / 22;
const ICONS_SIZE = width / 18;

const TopTabNavigator = () => (
	<TopTab.Navigator
		lazy={true}
		tabBarOptions={TAB_BAR_OPTIONS}
		screenOptions={({ route }) => ({
			tabBarIcon: ({ color }) => {
				const iconStyles = { color: color };
				const { name } = route;
				if (name === "Posts") {
					return (
						<Icon
							iconType={"ionicon"}
							iconSize={ICONS_SIZE}
							iconName={"ios-images"}
							style={iconStyles}
						/>
					);
				} else if (name === "Groups") {
					return (
						<Icon
							iconType={"icomoon"}
							iconSize={ICON_SIZE}
							iconName={"groups-icon"}
							style={iconStyles}
						/>
					);
				}
			}
		})}>
		<TopTab.Screen
			name={"Posts"}
			component={Activity}
			options={{
				tabBarLabel: ({ color, focused }) => (
					<GoappTextBold color={color} size={scale(14)}>
						{"Posts"}
					</GoappTextBold>
				)
			}}
		/>
		<TopTab.Screen
			name={"Groups"}
			component={Club}
			options={{
				tabBarLabel: ({ color, focused }) => (
					<GoappTextBold color={color} size={scale(14)}>
						{"Groups"}
					</GoappTextBold>
				)
			}}
		/>
	</TopTab.Navigator>
);

class Home extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		const { navigation, currentDisplayScreen, dispatch } = this.props;

		this._unsubscribeFocus = navigation.addListener("focus", () => {
			if (currentDisplayScreen !== "Home") {
				dispatch(communitySaveCurrentDisplayScreen("Home"));
			}
		});

		this._unsubscribeBlur = navigation.addListener("blur", () => {
			dispatch(communitySaveCurrentDisplayScreen(null));
		});
	}

	renderUserImage = () => {
		try {
			if (this.hasProfileImageUrl()) {
				return (
					<FastImage
						source={{ uri: this.props.myProfile.image }}
						style={styles.userImage}
					/>
				);
			} else {
				throw new Error("User profile pic not found");
			}
		} catch (error) {
			return (
				<View style={styles.noUserImage}>
					<Icon
						iconType={"ionicon"}
						iconSize={width / 12}
						iconName={"ios-person"}
						iconColor={LIGHT_GREY_COLOR}
					/>
				</View>
			);
		}
	};

	checkValidityForConnect = () => {
		try {
			if (this.isHobbiesAndIntresetsEmpty()) {
				return false;
			} else if (this.isInfoAboutUserProfileEmpty()) {
				return false;
			} else if (this.isUserNameEmpty()) {
				return false;
			} else {
				return true;
			}
		} catch (error) {
			console.log("error while check valid", error);
			return false;
		}
	};

	handleMexconnectPress = () => {
		const { navigate } = this.props.navigation;
		if (this.props.myProfile.linkedin !== null) {
			if (this.checkValidityForConnect()) {
				navigate("ConnectionsMatch");
			} else {
				this.props.navigation.navigate("EditProfile", {
					isUseConnect: true
				});
			}
		} else {
			navigate("ConnectWelcome");
		}
	};

	hasProfileImageUrl = () => {
		return _.get(this.props, "myProfile.image", "")
			.trim()
			.startsWith("http");
	};

	isUserNameEmpty = () => {
		return _.get(this.props, "myProfile.name", "").trim() === "";
	};

	isInfoAboutUserProfileEmpty = () => {
		return _.get(this.props, "myProfile.about", "").trim().length <= 30;
	};

	isHobbiesAndIntresetsEmpty = () => {
		return _.get(this.props, "myProfile.hobbies_interests", []).length === 0;
	};

	componentWillUnmount() {
		this._unsubscribeFocus();
		this._unsubscribeBlur();
	}

	render() {
		const { navigation } = this.props;
		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<View style={styles.leftMenu}>
						<TouchableOpacity
							onPress={() => navigation.navigate("UserProfile")}>
							{this.renderUserImage()}
						</TouchableOpacity>
					</View>
					<View style={styles.rightMenu}>
						<View style={styles.innerRight}>
							<TouchableOpacity
								onPress={() => navigation.navigate("ActivityLog")}>
								<Icon
									iconType={"icomoon"}
									iconSize={height / 30}
									iconName={"notification"}
									iconColor={LIGHT_GREY_COLOR}
								/>
							</TouchableOpacity>
							<TouchableOpacity
								onPress={() => this.handleMexconnectPress()}
								style={styles.gradButton}>
								<CommunityTextBold color={"#fff"} size={13} numberOfLines={1}>
									{"MEX. Connect"}
								</CommunityTextBold>
							</TouchableOpacity>
						</View>
					</View>
				</View>
				<TopTabNavigator />
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		username: _.get(state, "appToken.user.name", ""),
		appToken: _.get(state, "appToken.token", ""),
		myProfile: _.get(state, "community.profile.myProfile", {}),
		currentDisplayScreen: _.get(
			state,
			"community.error.currentDisplayScreen",
			""
		)
	};
}

export default connect(mapStateToProps)(Home);
