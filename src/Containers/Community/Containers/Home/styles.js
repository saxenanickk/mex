import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: { flex: 1, backgroundColor: "#fff" },
	header: {
		flexDirection: "row",
		width: width,
		paddingLeft: width / 17,
		paddingTop: height / 70,
		alignItems: "center",
		justifyContent: "space-between",
		backgroundColor: "#fff"
	},
	userContainer: {
		flexDirection: "row",
		paddingHorizontal: width / 25,
		paddingVertical: height / 50,
		alignItems: "center",
		backgroundColor: "#fff"
	},
	userImage: {
		width: width / 8,
		height: width / 8,
		borderRadius: width / 16
	},
	userInfo: {
		marginTop: height / 90,
		marginLeft: width / 90,
		paddingHorizontal: width / 25
	},
	noUserImage: {
		width: width / 6,
		height: width / 6,
		borderRadius: width / 12,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#eeeff5"
	},
	mexconnectContainer: {
		backgroundColor: "#7eb1e5",
		paddingHorizontal: width / 30,
		paddingVertical: width / 50,
		borderRadius: width / 20
	},
	mexConnectText: {
		color: "#fff",
		fontSize: 18
	},
	connectButton: {
		flexDirection: "row",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		borderRadius: width / 20,
		alignItems: "center",
		height: height / 20
	},
	activityImage: {
		width: height / 30,
		height: height / 30,
		marginTop: height / 200
	},
	leftMenu: { flexDirection: "row", alignItems: "center", flex: 1 },
	rightMenu: {
		height: height / 11.3,
		flexDirection: "row",
		justifyContent: "space-between",
		borderWidth: 0,
		width: width / 2.4,
		paddingLeft: 0
	},
	connectText: { textDecorationLine: "underline" },
	gradButton: {
		paddingVertical: height / 120,
		paddingHorizontal: width / 40,
		borderRadius: width / 10,
		alignItems: "center",
		backgroundColor: "#2C98F0",
		marginLeft: width * 0.015
	},
	innerRight: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "flex-end",
		alignItems: "center",
		marginRight: width * 0.03
	},
	postImage: {
		height: height / 22,
		width: width / 22
	}
});

/**
 * ...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3,
			},
		}),
 */
