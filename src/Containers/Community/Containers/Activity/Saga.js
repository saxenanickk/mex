// @ts-check
import { takeLatest, put, call } from "redux-saga/effects";
import Api from "../../Api";
import { communitySaveError } from "../../Saga";
import {
	communityGetSinglePostComment,
	communityGetSinglePostDetail
} from "../PostDetail/Saga";

// Constants
export const COMMUNITY_CREATE_POST = "COMMUNITY_CREATE_POST";
export const COMMUNITY_CREATE_POST_RESPONSE = "COMMUNITY_CREATE_POST_RESPONSE";
export const COMMUNITY_SAVE_POST = "COMMUNITY_SAVE_POST";
export const COMMUNITY_GET_POST = "COMMUNITY_GET_POST";
export const COMMUNITY_SAVE_OFFSET = "COMMUNITY_SAVE_OFFSET";
export const COMMUNITY_REFRESH_POST = "COMMUNITY_REFRESH_POST";
export const COMMUNITY_GET_CLUB_POST = "COMMUNITY_GET_CLUB_POST";
export const COMMUNITY_SAVE_CLUB_POST = "COMMUNITY_SAVE_CLUB_POST";
export const COMMUNITY_SAVE_CLUB_OFFSET = "COMMUNITY_SAVE_CLUB_OFFSET";
export const COMMUNITY_CREATE_COMMENT_RESPONSE =
	"COMMUNITY_CREATE_COMMENT_RESPONSE";
export const COMMUNITY_CHANGE_POST_LIKE_LOCAL =
	"COMMUNITY_CHANGE_POST_LIKE_LOCAL";
export const COMMUNITY_CHANGE_POST_COMMENT_COUNT_LOCAL =
	"COMMUNITY_CHANGE_POST_COMMENT_COUNT_LOCAL";
export const COMMUNITY_CHANGE_CLUB_POST_LIKE_LOCAL =
	"COMMUNITY_CHANGE_CLUB_POST_LIKE_LOCAL";
export const COMMUNITY_CHANGE_CLUB_POST_COMMENT_COUNT_LOCAL =
	"COMMUNITY_CHANGE_CLUB_POST_COMMENT_COUNT_LOCAL";
export const COMMUNITY_GET_LIKED_MEMBERS = "COMMUNITY_GET_LIKED_MEMBERS";
export const COMMUNITY_SAVE_LIKED_MEMBERS = "COMMUNITY_SAVE_LIKED_MEMBERS";

//	Action creators
export const communityCreatePost = payload => ({
	type: COMMUNITY_CREATE_POST,
	payload
});
export const communityCreatePostResponse = payload => ({
	type: COMMUNITY_CREATE_POST_RESPONSE,
	payload
});
export const communityGetPost = payload => ({
	type: COMMUNITY_GET_POST,
	payload
});
export const communitySavePost = payload => ({
	type: COMMUNITY_SAVE_POST,
	payload
});
export const communitySaveOffset = payload => ({
	type: COMMUNITY_SAVE_OFFSET,
	payload
});
export const communityRefreshPost = payload => ({
	type: COMMUNITY_REFRESH_POST,
	payload
});
export const communityCreateCommentResponse = payload => ({
	type: COMMUNITY_CREATE_COMMENT_RESPONSE,
	payload
});
export const communityGetClubPost = payload => ({
	type: COMMUNITY_GET_CLUB_POST,
	payload
});
export const communitySaveClubPost = payload => ({
	type: COMMUNITY_SAVE_CLUB_POST,
	payload
});
export const communitySaveClubOffset = payload => ({
	type: COMMUNITY_SAVE_CLUB_OFFSET,
	payload
});
export const communityChangePostLikeLocal = payload => ({
	type: COMMUNITY_CHANGE_POST_LIKE_LOCAL,
	payload
});
export const communityChangePostCommentCountLocal = payload => ({
	type: COMMUNITY_CHANGE_POST_COMMENT_COUNT_LOCAL,
	payload
});
export const communityChangeClubPostLikeLocal = payload => ({
	type: COMMUNITY_CHANGE_CLUB_POST_LIKE_LOCAL,
	payload
});
export const communityChangeClubPostCommentCountLocal = payload => ({
	type: COMMUNITY_CHANGE_CLUB_POST_COMMENT_COUNT_LOCAL,
	payload
});
export const communityGetLikedMembders = payload => ({
	type: COMMUNITY_GET_LIKED_MEMBERS,
	payload
});

export const communitySaveLikedMembders = payload => ({
	type: COMMUNITY_SAVE_LIKED_MEMBERS,
	payload
});

/**
 * Saga
 * @param {any} dispatch
 */
export function* communityPostSaga(dispatch) {
	yield takeLatest(COMMUNITY_CREATE_POST, handleCommunityCreatePost);
	yield takeLatest(COMMUNITY_GET_POST, handleCommunityGetPost);
	yield takeLatest(COMMUNITY_GET_CLUB_POST, handleCommunityGetClubPost);
	yield takeLatest(COMMUNITY_GET_LIKED_MEMBERS, handleCommunityGetLikedMembers);
}

//	Handlers
function* handleCommunityCreatePost(action) {
	try {
		let photoUploadResponse = null;
		if (action.payload.photo !== null && action.payload.photo !== undefined) {
			photoUploadResponse = yield call(Api.uploadImage, action.payload);
			if (photoUploadResponse.success === 0) {
				throw new Error("unable to upload photo");
			}
		}
		let response = yield call(Api.createPost, {
			...action.payload,
			postImage:
				photoUploadResponse !== null && photoUploadResponse !== undefined
					? photoUploadResponse.url
					: "",
			postVideo: action.payload.video
		});
		console.log("response of image upload", response);
		if (response.success === 1) {
			if (action.payload.postType === "post") {
				yield put(communityCreatePostResponse(true));
				if (
					action.payload.clubId !== null &&
					action.payload.clubId !== undefined
				) {
					yield put(
						communityGetClubPost({ ...action.payload, isRefresh: true })
					);
				}
				yield put(
					communityGetPost({
						appToken: action.payload.appToken,
						isRefresh: true
					})
				);
			} else if (action.payload.postType === "comment") {
				yield put(communityCreateCommentResponse(true));
				yield put(
					communityGetSinglePostDetail({
						appToken: action.payload.appToken,
						postId: action.payload.parentId
					})
				);
				yield put(
					communityGetSinglePostComment({
						appToken: action.payload.appToken,
						postId: action.payload.parentId,
						isRefresh: true
					})
				);
			}
		} else {
			throw new Error("unable to save post");
		}
	} catch (error) {
		console.log(error, "error");
		if (action.payload.postType === "post") {
			yield put(communityCreatePostResponse(false));
		} else if (action.payload.postType === "comment") {
			yield put(communityCreateCommentResponse(false));
		}
		yield put(
			communitySaveError({
				errorScreen: "CreatePost",
				errorMessage: "Unable to save post",
				isError: true
			})
		);
	}
}

function* handleCommunityGetPost(action) {
	try {
		let response = yield call(Api.getPosts, action.payload);
		console.log("response", response);
		if (response.success === 1) {
			if (
				action.payload.isRefresh !== undefined &&
				action.payload.isRefresh === true
			) {
				yield put(communityRefreshPost(response.data));
			} else {
				yield put(communitySavePost(response.data));
			}
			if (response.offset !== null && response.offset !== undefined) {
				yield put(communitySaveOffset(response.offset));
			}
		} else {
			throw new Error("unable to fetch posts");
		}
	} catch (error) {
		console.log(error, "error");
		yield put(
			communitySaveError({
				errorScreen: "Activity",
				errorMessage: "Unable to fetch posts At this moment",
				isError: true
			})
		);
	}
}

function* handleCommunityGetClubPost(action) {
	try {
		let response = yield call(Api.getPosts, action.payload);
		console.log("response", response);
		if (response.success === 1) {
			yield put(
				communitySaveClubPost({
					posts: response.data,
					isRefresh: action.payload.isRefresh
				})
			);
			yield put(communitySaveClubOffset(response.offset));
		} else {
			throw new Error("unable to fetch posts related to club");
		}
	} catch (error) {
		console.log(error, "error");
		yield put(
			communitySaveError({
				errorScreen: "ClubDetail",
				errorMessage: "Unable to fetch posts related to club",
				isError: true
			})
		);
	}
}

function* handleCommunityGetLikedMembers(action) {
	try {
		let response = yield call(Api.getLikedMembers, action.payload);
		console.log("response", response);
		if (response.success === 1) {
			yield put(communitySaveLikedMembders(response.data));
		} else {
			throw new Error("unable to get liked members");
		}
	} catch (error) {
		console.log(error, "error");
		yield put(
			communitySaveError({
				errorScreen: "LikedMember",
				errorMessage: "Unable to get Liked Members",
				isError: true
			})
		);
	}
}
