import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#eeeff5"
	},
	progressContainer: {
		height: height / 1.5,
		justifyContent: "center",
		alignItems: "center"
	},
	inputField: {
		width: "100%",
		alignSelf: "center",
		justifyContent: "center",
		backgroundColor: "#fff",
		marginVertical: width / 50,
		// borderRadius: width / 50,
		...Platform.select({
			android: { elevation: 1 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	commentField: {
		width: width * 0.84,
		alignSelf: "center",
		marginTop: height / 50,
		height: height / 15,
		justifyContent: "center",
		paddingHorizontal: width / 30,
		backgroundColor: "#eeeff5",
		borderRadius: width / 50
	},
	editImage: {
		width: width / 20,
		height: width / 20
	},
	rowView: {
		flexDirection: "row",
		alignItems: "center",
		height: height / 15,
		paddingHorizontal: width / 30
	},
	textInput: {
		width: width / 2,
		marginLeft: width / 50,
		color: "#9ca1ad"
	},
	plusIcon: {
		marginLeft: width / 2.92,
		height: height / 23,
		width: width / 23
	},
	commentTextInput: {
		width: width / 1.4
	},
	postSection: {
		alignItems: "center",
		flex: 1
	},
	flatListStyle: {
		width: "100%"
	},
	flatListContainerStyle: {
		flexGrow: 1
	},
	postCard: {
		width: width * 0.92,
		paddingVertical: height / 50,
		backgroundColor: "#fff",
		borderRadius: width / 30,
		marginTop: height / 50
	},
	userInfo: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: height / 60,
		alignItems: "center"
	},
	userImageSection: {
		flexDirection: "row",
		width: width / 2.2,
		justifyContent: "space-between",
		alignItems: "center"
	},
	userImageView: {
		width: width / 8,
		height: width / 8,
		borderRadius: width / 12,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#ff0000",
		borderWidth: 1
	},
	imageIcon: {
		width: width / 6.5,
		height: width / 6.5,
		borderRadius: width / 13
	},
	userPostDescription: {
		paddingHorizontal: width / 30,
		paddingVertical: height / 60
	},
	mainImage: {
		width: width * 0.84,
		height: height / 5,
		borderRadius: width / 30,
		alignSelf: "center"
	},
	mainImageSection: {
		width: width * 0.84,
		height: height / 5,
		borderRadius: width / 30,
		alignSelf: "center",
		elevation: 5,
		backgroundColor: "rgba(0,0,0,0.5)"
	}
});
