import _ from "lodash";
import {
	COMMUNITY_SAVE_POST,
	COMMUNITY_CREATE_POST_RESPONSE,
	COMMUNITY_SAVE_OFFSET,
	COMMUNITY_REFRESH_POST,
	COMMUNITY_CREATE_COMMENT_RESPONSE,
	COMMUNITY_SAVE_CLUB_POST,
	COMMUNITY_SAVE_CLUB_OFFSET,
	COMMUNITY_CHANGE_POST_LIKE_LOCAL,
	COMMUNITY_CHANGE_POST_COMMENT_COUNT_LOCAL,
	COMMUNITY_CHANGE_CLUB_POST_LIKE_LOCAL,
	COMMUNITY_CHANGE_CLUB_POST_COMMENT_COUNT_LOCAL,
	COMMUNITY_SAVE_LIKED_MEMBERS
} from "./Saga";

const initialState = {
	posts: null,
	createPostResponse: null,
	nextOffSet: null,
	createCommentResponse: null,
	clubPost: null,
	clubNextOffset: null,
	likedMembers: null,
	userProfile: null
};

/**
 * @param {{ type: string; payload: any; }} action
 */
export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case COMMUNITY_SAVE_POST:
			return {
				...state,
				posts:
					state.posts === null
						? action.payload
						: state.posts.slice().concat(action.payload)
			};
		case COMMUNITY_CREATE_POST_RESPONSE:
			return {
				...state,
				createPostResponse: action.payload
			};
		case COMMUNITY_SAVE_OFFSET:
			return {
				...state,
				nextOffSet:
					action.payload.user !== null || action.payload.club !== null
						? action.payload
						: null
			};
		case COMMUNITY_REFRESH_POST:
			return {
				...state,
				posts: action.payload
			};
		case COMMUNITY_CREATE_COMMENT_RESPONSE:
			return {
				...state,
				createCommentResponse: action.payload
			};
		case COMMUNITY_SAVE_CLUB_POST:
			return {
				...state,
				clubPost:
					action.payload === null
						? action.payload
						: state.clubPost === null || action.payload.isRefresh
						? action.payload.posts
						: state.clubPost.slice().concat(action.payload.posts)
			};
		case COMMUNITY_SAVE_CLUB_OFFSET:
			return {
				...state,
				clubNextOffset:
					action.payload.user !== null ? action.payload : state.clubNextOffset
			};
		case COMMUNITY_CHANGE_POST_LIKE_LOCAL:
			let temp = state.posts !== null ? state.posts.slice() : state.posts;
			if (temp !== null) {
				let index = _.findIndex(state.posts, ["id", action.payload.postId]);
				if (temp[index]) {
					temp[index].hasLiked = action.payload.isLike;
					temp[index].liked_count = action.payload.likeCount;
				}
			}
			return {
				...state,
				posts: temp
			};
		case COMMUNITY_CHANGE_POST_COMMENT_COUNT_LOCAL:
			temp = state.posts !== null ? state.posts.slice() : state.posts;
			if (temp !== null) {
				let index = _.findIndex(state.posts, ["id", action.payload.postId]);
				if (temp[index]) {
					temp[index].comment_count = action.payload.commentCount;
				}
			}
			return {
				...state,
				posts: temp
			};
		case COMMUNITY_CHANGE_CLUB_POST_LIKE_LOCAL:
			temp = state.clubPost !== null ? state.clubPost.slice() : state.clubPost;
			if (temp !== null) {
				let index = _.findIndex(state.clubPost, ["id", action.payload.postId]);
				if (temp[index]) {
					temp[index].hasLiked = action.payload.isLike;
					temp[index].liked_count = action.payload.likeCount;
				}
			}
			return {
				...state,
				clubPost: temp
			};
		case COMMUNITY_CHANGE_CLUB_POST_COMMENT_COUNT_LOCAL:
			temp = state.clubPost !== null ? state.clubPost.slice() : state.clubPost;
			if (temp !== null) {
				let index = _.findIndex(state.clubPost, ["id", action.payload.postId]);
				if (temp[index]) {
					temp[index].comment_count = action.payload.commentCount;
				}
			}
			return {
				...state,
				clubPost: temp
			};
		case COMMUNITY_SAVE_LIKED_MEMBERS:
			return {
				...state,
				likedMembers: action.payload
			};
		default:
			return state;
	}
};
