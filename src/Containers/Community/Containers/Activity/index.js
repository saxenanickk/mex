import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	FlatList,
	Keyboard,
	Alert
} from "react-native";
import { connect } from "react-redux";

import { styles } from "./style";
import I18n from "../../Assets/Strings/i18n";
import {
	communityGetPost,
	communityCreatePost,
	communityRefreshPost
} from "./Saga";
import PostCard from "../../Components/PostCard";
import CommunityLoader from "../../Components/CommunityLoader";
import {
	communityResetError,
	communitySaveCurrentDisplayScreen
} from "../../Saga";
import { CommunityTextRegular } from "../../Components/CommunityText";
import { EmptyView, Icon } from "../../../../Components";

const { width } = Dimensions.get("window");
class Activity extends Component {
	constructor(props) {
		super(props);
		this.commentIndex = [];
		this.state = {
			isLoading: true,
			isImageView: false,
			isModalOpen: false,
			imagePostInfo: null,
			pause: false
		};
		this.videoPlayOffset = 0;
		this.videoPlayStatus = "PAUSED";
		this.offsetBeforeVideoPlay = 0;
		this.offsetAfterVideoPlay = 0;
		this.saveComment = this.saveComment.bind(this);
	}

	componentDidMount() {
		this._unsubscribeFocus = this.props.navigation.addListener("focus", () => {
			if (this.props.currentDisplayScreen !== "Home") {
				this.props.dispatch(communitySaveCurrentDisplayScreen("Home"));
			}
		});

		this._unsubscribeBlur = this.props.navigation.addListener("blur", () => {
			this.props.dispatch(communitySaveCurrentDisplayScreen(null));
		});

		this.props.dispatch(
			communityGetPost({
				appToken: this.props.appToken
			})
		);
	}

	componentWillUnmount() {
		this.props.dispatch(communityRefreshPost(null));
		this._unsubscribeFocus();
		this._unsubscribeBlur();
	}

	saveComment = (item, index) => {
		Keyboard.dismiss();
		if (
			this.commentIndex[index]._lastNativeText !== undefined &&
			this.commentIndex[index]._lastNativeText.trim() !== ""
		) {
			this.props.dispatch(
				communityCreatePost({
					postType: "comment",
					postDescription: this.commentIndex[index]._lastNativeText,
					appToken: this.props.appToken,
					parentId: item.id,
					uniqueIdentifier: item.uniqueIdentifier
				})
			);
			this.commentIndex[index].clear();
		}
	};

	shouldComponentUpdate(props, state) {
		if (
			props.error &&
			props.error !== this.props.error &&
			props.error.isError &&
			props.error.errorScreen === "Activity"
		) {
			Alert.alert(I18n.t("error"), "Something went wrong");
			this.props.dispatch(communityResetError());
			this.setState({ isLoading: false });
		}
		return true;
	}

	setScrollOffset = cb => {
		this.videoPlayStatus = "PLAYING";
		this.videoPlayOffset = this.offsetBeforeVideoPlay;

		if (this.state.pause) this.setState({ pause: false });

		cb();
	};

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.inputField}>
					<TouchableOpacity
						activeOpacity={0.8}
						style={styles.rowView}
						onPress={() => this.props.navigation.navigate("CreatePost")}>
						<CommunityTextRegular style={styles.textInput}>
							{I18n.t("comments_write")}
						</CommunityTextRegular>
						<Icon
							iconType={"feather"}
							iconSize={width * 0.08}
							iconName={"plus"}
							iconColor={"#9A9A9A"}
							style={{ marginLeft: width / 3.1 }}
						/>
					</TouchableOpacity>
				</View>
				<View style={styles.postSection}>
					{this.props.posts !== null ? (
						<FlatList
							data={this.props.posts}
							showsVerticalScrollIndicator={false}
							keyboardShouldPersistTaps={"always"}
							refreshing={false}
							contentContainerStyle={styles.flatListContainerStyle}
							style={styles.flatListStyle}
							onRefresh={() => {
								this.props.dispatch(
									communityGetPost({
										appToken: this.props.appToken,
										isRefresh: true
									})
								);
							}}
							onScroll={({ nativeEvent }) => {
								if (this.videoPlayStatus === "PAUSED") {
									this.offsetBeforeVideoPlay = nativeEvent.contentOffset.y;
								} else {
									this.offsetAfterVideoPlay = nativeEvent.contentOffset.y;

									if (
										Math.abs(this.offsetAfterVideoPlay - this.videoPlayOffset) >
										40
									) {
										this.setState({ pause: true });
									}
								}
							}}
							onEndReachedThreshold={0.4}
							onEndReached={() => {
								if (this.props.nextOffSet !== null) {
									this.props.dispatch(
										communityGetPost({
											appToken: this.props.appToken,
											nextOffSet: this.props.nextOffSet
										})
									);
								}
							}}
							keyExtractor={(item, index) => index.toString()}
							extraData={this.state}
							renderItem={({ item, index }) => {
								if (item !== null) {
									return (
										<PostCard
											pause={this.state.pause}
											setScrollOffset={this.setScrollOffset}
											postDetail={item}
											isClubPost={false}
											navigation={this.props.navigation}
										/>
									);
								} else {
									return null;
								}
							}}
							ListEmptyComponent={() => (
								<EmptyView
									noRecordFoundMesage={I18n.t("com_no_post_found")}
									sorryMessage={I18n.t("com_sorry")}
									onRefresh={() => {
										this.props.dispatch(
											communityGetPost({
												appToken: this.props.appToken
											})
										);
									}}
								/>
							)}
						/>
					) : (
						<View style={styles.progressContainer}>
							{this.state.isLoading && <CommunityLoader size={width / 15} />}
						</View>
					)}
				</View>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		posts: state.community && state.community.postReducer.posts,
		nextOffSet: state.community && state.community.postReducer.nextOffSet,
		appToken: state.appToken.token,
		error: state.community && state.community.error,
		createPostResponse:
			state.community && state.community.postReducer.createPostResponse,
		currentDisplayScreen:
			state.community && state.community.error.currentDisplayScreen
	};
}

export default connect(mapStateToProps)(Activity);
