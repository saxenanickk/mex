import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 10
	},
	welcomeTextContainer: {
		alignItems: "center",
		marginTop: height / 8
	},
	welcomeText: {
		fontSize: 17
	},
	welcomeTitleText: {
		fontSize: 18
	},
	onboardLoadingImage: {
		alignItems: "center"
	},
	descriptionContainer: {
		alignItems: "center"
	},
	descriptionText: {
		textAlign: "center",
		fontSize: height / 50,
		marginTop: height / 55
	},
	proceedButton: {
		alignItems: "center",
		borderRadius: width / 10,
		marginHorizontal: "15%",
		marginVertical: height * 0.03,
		backgroundColor: "#2C98F0",
		paddingVertical: width / 30
	},
	proceedText: {
		fontSize: height / 50
	},
	connectImage: { width: width / 1.2, height: width / 1.2 },
	header: {
		position: "absolute",
		top: 0
	}
});
