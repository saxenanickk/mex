import React, { Component } from "react";
import { View, Image, TouchableOpacity } from "react-native";
import { styles } from "./style";
import { connect } from "react-redux";
import {
	CommunityTextBold,
	CommunityTextLight,
	CommunityTextMedium
} from "../../../Components/CommunityText";
import { CONNECTLOGO } from "../../../Assets/Img/Image";
import CommunityHeader from "../../../Components/Header";
import { GoappTextBold } from "../../../../../Components/GoappText";

class Welcome extends Component {
	handleBackbuttonPress = () => {
		this.props.navigation.goBack();
	};

	handlerProceed = () => {
		this.props.navigation.navigate("ConnectAuth");
	};
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<CommunityHeader onPress={() => this.handleBackbuttonPress()} />
				</View>
				<View style={styles.welcomeTextContainer}>
					<CommunityTextMedium color="#389EF2" style={styles.welcomeText}>
						{"MEX. Connect!"}
					</CommunityTextMedium>
				</View>
				<View style={styles.onboardLoadingImage}>
					<Image source={CONNECTLOGO} style={styles.connectImage} />
				</View>
				<View style={styles.descriptionContainer}>
					<CommunityTextLight style={styles.descriptionText} color="#6B6B6B">
						{
							"Find and connect with people who share similar interests from the community!"
						}
					</CommunityTextLight>
				</View>
				<TouchableOpacity
					onPress={this.handlerProceed}
					style={styles.proceedButton}>
					<GoappTextBold style={styles.proceedText} color={"#fff"}>
						{"Proceed"}
					</GoappTextBold>
				</TouchableOpacity>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state && state.appToken && state.appToken.token,
		myProfile: state.community && state.community.profile.myProfile
	};
}

export default connect(mapStateToProps)(Welcome);
