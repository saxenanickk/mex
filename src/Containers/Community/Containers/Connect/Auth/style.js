import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
		paddingHorizontal: width / 30
	},
	helloTextContainer: {
		alignItems: "center",
		marginTop: height / 20
	},
	titleText: {
		fontSize: height / 35,
		textAlign: "center"
	},
	descriptionText: {
		textAlign: "center",
		fontSize: height / 50,
		marginHorizontal: width / 10
	},
	signinContainer: {
		alignItems: "center",
		marginTop: height / 15
	},
	signinText: {
		marginBottom: height / 30,
		fontSize: height / 33
	},
	linkedInButton: {
		marginTop: height / 30,
		borderRadius: width / 10,
		backgroundColor: "#FFFFFF",
		paddingVertical: width / 30,
		alignItems: "center",
		...Platform.select({
			android: { elevation: 3 },
			ios: {
				shadowOffset: { width: 0, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	linkedInText: {
		color: "#4B5874",
		fontSize: 16,
		paddingLeft: width / 30
	},
	facebookButton: {
		marginTop: height / 30,
		borderRadius: width / 10,
		backgroundColor: "#FFFFFF",
		paddingVertical: width / 30,
		alignItems: "center",
		...Platform.select({
			android: { elevation: 3 },
			ios: {
				shadowOffset: { width: 0, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	facebookText: {
		color: "#4B5874",
		fontSize: 16,
		paddingLeft: width / 30,
		paddingRight: width / 90
	},
	signinButtons: {
		width: "80%"
	},
	proceedContainer: {
		flex: 1,
		justifyContent: "flex-end",
		marginBottom: height / 10
	},
	proceedButton: {
		alignItems: "center",
		borderRadius: width / 10,
		...Platform.select({
			android: { elevation: 2 },
			ios: {
				shadowOffset: { width: 0, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		}),
		marginHorizontal: "16%",
		backgroundColor: "#2C98F0",
		paddingVertical: width / 30
	},
	proceedText: {
		fontSize: height / 50,
		color: "#F4FAFE"
	},
	closeButton: {
		marginTop: height / 25,
		marginLeft: width / 20
	},
	disconnectButtonLabel: {
		color: "#d32b28",
		fontSize: height / 80,
		marginTop: height / 100
	},
	disconnectButton: {
		backgroundColor: "#ffffff",
		top: -width / 60,
		zIndex: -1,
		borderBottomStartRadius: 5,
		borderBottomEndRadius: 5,
		padding: 4,
		justifyContent: "flex-end",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 2 },
			ios: {
				shadowOffset: { width: 0.1, height: 0.5 },
				shadowColor: "black",
				shadowOpacity: 0.1
			}
		})
	},
	loadingContainer: {
		position: "absolute",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "#000",
		opacity: 0.6,
		zIndex: 10,
		elevation: 10
	},
	verifyIcon: {
		marginLeft: width / 7
	},
	requiredFields: {
		marginLeft: width / 100,
		marginBottom: height / 70
	}
});
