import React, { Component } from "react";
import { View, TouchableOpacity, Alert, Dimensions } from "react-native";
import LinkedInModal from "react-native-linkedin";
import { LoginManager, AccessToken } from "react-native-fbsdk";
import { styles } from "./style";
import {
	CommunityTextBold,
	CommunityTextLight,
	CommunityTextMedium
} from "../../../Components/CommunityText";
import { Icon, GoToast } from "../../../../../Components";
import CommunityLoader from "../../../Components/CommunityLoader";
import { connect } from "react-redux";
import {
	communityUpdateUserProfile,
	communityUpdateUserSuccess
} from "../../UserProfile/Saga";
import GoAppAnalytics from "../../../../../utils/GoAppAnalytics";
import Config from "react-native-config";

const { SERVER_BASE_URL_LINKEDIN_REDIRECT } = Config;

const { width, height } = Dimensions.get("window");

const FacebookButton = ({ onPress, token }) => (
	<TouchableOpacity
		style={styles.facebookButton}
		activeOpacity={1}
		onPress={onPress}>
		<View
			style={{
				flexDirection: "row",
				alignItems: "center",
				justifyContent: "flex-start",
				width: "65%"
			}}>
			<Icon
				iconType={"entypo"}
				iconName={"facebook"}
				iconSize={width / 15}
				iconColor={"#475993"}
			/>
			<CommunityTextMedium style={styles.facebookText} numberOfLines={1}>
				{`Facebook`}
			</CommunityTextMedium>
			<CommunityTextLight
				size={height / 65}
				color="#D1D3D7"
				numberOfLines={1}>{`(optional)`}</CommunityTextLight>
		</View>
	</TouchableOpacity>
);

const linkedInButton = (token = null) => (
	<View style={styles.linkedInButton}>
		<View
			style={{
				flexDirection: "row",
				alignItems: "center",
				justifyContent: "flex-start",
				width: "65%"
			}}>
			<Icon
				iconType={"font_awesome"}
				iconName={"linkedin-square"}
				iconSize={width / 15}
				iconColor={"#0077B7"}
			/>
			<CommunityTextMedium style={styles.linkedInText}>
				{"Linkedin"}
			</CommunityTextMedium>
			<Icon
				style={styles.requiredFields}
				iconType={"font_awesome"}
				iconName={"asterisk"}
				iconSize={width / 58}
				iconColor={"#E75252"}
			/>
			{token !== null ? (
				<Icon
					style={styles.verifyIcon}
					iconType={"feather"}
					iconName={"check-circle"}
					iconSize={height / 35}
					iconColor={"#58af61"}
				/>
			) : null}
		</View>
	</View>
);

class Auth extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: false,
			fbTokenDetails: this.props.myProfile
				? this.props.myProfile.facebook
				: null,
			linkedInTokenDetails: this.props.myProfile
				? this.props.myProfile.linkedin
				: null
		};
	}

	handleGoBack = () => {
		if (this.props.myProfile.linkedin) {
			this.props.navigation.goBack();
		} else {
			Alert.alert("Warning", "If you close, you cannot use connect feature", [
				{ text: "Cancel", onPress: () => null, style: "cancel" },
				{ text: "OK", onPress: () => this.props.navigation.goBack() }
			]);
		}
	};

	/**
	 * @param {object} tokenDetails
	 * @param {string} token.access_token
	 * @param {string} token.expires_in
	 */
	handleLinkedInAccessToken = async ({ access_token, expires_in }) => {
		try {
			let expiryDate = new Date();
			expiryDate.setSeconds(expiryDate.getSeconds() + expires_in);
			GoAppAnalytics.trackWithProperties("community_linkedin_connect", {
				data: "Linkedin login successful"
			});

			const obj = {
				token: access_token,
				expirydate: expiryDate.getTime().toString()
			};
			this.setState({
				linkedInTokenDetails: obj
			});
		} catch (error) {
			console.log("erorr in login", error);
		}
	};

	handleLinkedInOnError = () => {
		GoAppAnalytics.trackWithProperties("community_linkedin_connect_error", {
			data: "Linkedin login failed"
		});
		Alert.alert("Info", "LinkedIn Login failed");
	};

	/**
	 * Facebook Flow
	 * 1. handle facebook login
	 * 2. (login success) fetch access token, expiration time and userId
	 * 3. get user data and send to our servers
	 */
	handleFacebookLogin = async () => {
		if (this.state.fbTokenDetails === null) {
			try {
				let fbLoginResult = await LoginManager.logInWithPermissions([
					"public_profile",
					"email"
				]);
				if (fbLoginResult.isCancelled) {
					GoToast.show("Login Cancelled", "Info");
				} else {
					const {
						accessToken,
						expirationTime
					} = await AccessToken.getCurrentAccessToken();
					GoAppAnalytics.trackWithProperties("community_facebook_connect", {
						data: "Facebook login successful"
					});
					this.setState({
						fbTokenDetails: {
							token: accessToken,
							expirydate: expirationTime.toString()
						}
					});
				}
			} catch (error) {
				GoToast.show("Oops! unable to link facebook");
				GoAppAnalytics.trackWithProperties("community_facebook_connect_error", {
					data: error ? error : "Facebook login failed"
				});
				console.log("Login fail with error: " + error);
			}
		}
	};

	openDisconnectAlert = socialAccountType => () => {
		console.log(socialAccountType);
		Alert.alert("Warning", "Do you really want to disconnect", [
			{ text: "No", onPress: () => null, style: "cancel" },
			{ text: "Yes", onPress: () => this.handleDisconnect(socialAccountType) }
		]);
	};

	handleDisconnect = socialAccountType => {
		console.log(socialAccountType);
		if (socialAccountType === "facebook") {
			// logout of facebook
			LoginManager.logOut();
		}
		this.setState({
			isLoading: true
		});
		this.props.dispatch(
			communityUpdateUserProfile({
				appToken: this.props.appToken,
				profile: {
					[socialAccountType]: null,
					site_id: this.props.myProfile.site_id
				}
			})
		);
		this.setState({
			linkedInTokenDetails: null,
			fbTokenDetails: null
		});
	};

	handleProceed = () => {
		if (this.state.linkedInTokenDetails !== null) {
			if (this.props.myProfile.linkedin !== null) {
				this.props.navigation.navigate("ConnectionsMatch");
			} else {
				let params = {
					linkedInTokenDetails: this.state.linkedInTokenDetails,
					...(this.state.fbTokenDetails && {
						fbTokenDetails: this.state.fbTokenDetails
					})
				};
				this.props.navigation.navigate("EditProfile", params);
			}
		} else {
			GoToast.show("Please login to LinkedIn", "Info");
		}
	};

	shouldComponentUpdate(props, state) {
		if (
			props.isProfileUpdateSuccess &&
			this.props.isProfileUpdateSuccess !== props.isProfileUpdateSuccess
		) {
			this.setState({ isLoading: false });
			this.props.dispatch(communityUpdateUserSuccess(null));
		}

		if (
			props.error &&
			props.error !== this.props.error &&
			props.error.isError &&
			props.error.errorScreen === "EditProfile"
		) {
			//@ts-ignore
			this.setState({ isLoading: false });
			Alert.alert("Error", "Unable to disconnect");
		}
		return true;
	}
	render() {
		console.log("state is", this.state);
		return (
			<View style={styles.container}>
				<TouchableOpacity
					style={styles.closeButton}
					onPress={this.handleGoBack}>
					<Icon
						iconType={"ionicon"}
						iconName={"ios-close"}
						iconColor={"#3B3B3B"}
						iconSize={30}
					/>
				</TouchableOpacity>
				<View style={styles.helloTextContainer}>
					<CommunityTextBold style={styles.titleText} color="#313131">
						{`Hey ${this.props.myProfile.name}`}
					</CommunityTextBold>
					<CommunityTextLight style={styles.descriptionText} color="#A6A6A6">
						{`Please help us know more about you to find good connects. Connecting these accounts would be a good way to start.`}
					</CommunityTextLight>
				</View>
				<View style={styles.signinContainer}>
					<CommunityTextMedium style={styles.signinText}>
						{"Sign in with"}
					</CommunityTextMedium>
					<View style={styles.signinButtons}>
						{this.state.linkedInTokenDetails === null ? (
							<LinkedInModal
								renderButton={() => linkedInButton()}
								clientID={"8107ixgwvxj0az"}
								clientSecret={"DRqfw9DMnezJDmlk"}
								redirectUri={SERVER_BASE_URL_LINKEDIN_REDIRECT}
								permissions={["r_liteprofile", "r_emailaddress"]}
								onSuccess={token => this.handleLinkedInAccessToken(token)}
								onError={this.handleLinkedInOnError}
							/>
						) : (
							linkedInButton(this.state.linkedInTokenDetails)
						)}
						{this.props.myProfile.linkedin !== null ? (
							<TouchableOpacity
								style={styles.disconnectButton}
								onPress={this.openDisconnectAlert("linkedin")}>
								<CommunityTextBold style={styles.disconnectButtonLabel}>
									{"Disconnect"}
								</CommunityTextBold>
							</TouchableOpacity>
						) : null}
						<FacebookButton
							onPress={this.handleFacebookLogin}
							token={this.state.fbTokenDetails}
						/>
						{this.props.myProfile.facebook !== null ? (
							<TouchableOpacity
								style={styles.disconnectButton}
								onPress={this.openDisconnectAlert("facebook")}>
								<CommunityTextBold style={styles.disconnectButtonLabel}>
									{"Disconnect"}
								</CommunityTextBold>
							</TouchableOpacity>
						) : null}
					</View>
				</View>
				<View style={styles.proceedContainer}>
					<TouchableOpacity
						style={styles.proceedButton}
						onPress={this.handleProceed}>
						<CommunityTextBold style={styles.proceedText}>
							{`Proceed`}
						</CommunityTextBold>
					</TouchableOpacity>
				</View>
				{this.state.isLoading ? (
					<View style={styles.loadingContainer}>
						<CommunityLoader />
					</View>
				) : null}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state && state.appToken && state.appToken.token,
		myProfile: state.community && state.community.profile.myProfile,
		isMyProfileLoading: state.community && state.community.profile.myProfile,
		error: state.community && state.community.error,
		isProfileUpdateSuccess:
			state.community && state.community.profile.isProfileUpdateSuccess
	};
}

export default connect(mapStateToProps)(Auth);
