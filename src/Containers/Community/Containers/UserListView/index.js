import React, { Component } from "react";
import {
	View,
	Dimensions,
	Image,
	TouchableOpacity,
	Alert,
	FlatList
} from "react-native";
import { connect } from "react-redux";
import { styles } from "./style";
import I18n from "../../Assets/Strings/i18n";
import { Icon, EmptyView } from "../../../../Components";
import { CommunityTextMedium } from "../../Components/CommunityText";
import CommunityLoader from "../../Components/CommunityLoader";
import {
	communityGetAllClubMembers,
	communitySaveAllClubMembers
} from "../EditProfile/Saga";
import { communityResetError } from "../../Saga";
import {
	communityGetAllFollower,
	communitySaveAllFollower,
	communityGetAllFollowing,
	communitySaveAllFollowing
} from "../UserProfile/Saga";
const { width, height } = Dimensions.get("window");

class UserListView extends Component {
	constructor(props) {
		super(props);
		this.state = {};

		const { route } = props;
		const { isFollowing = null, clubId = null } = route.params
			? route.params
			: {};

		this.isFollowing = isFollowing;
		this.clubId = clubId;
	}

	componentDidMount() {
		this.callUserListApi();
	}

	componentWillUnmount() {
		this.flushUserListData();
	}

	callUserListApi = () => {
		if (this.isFollowing !== null && this.isFollowing !== undefined) {
			if (this.isFollowing) {
				this.props.dispatch(
					communityGetAllFollowing({
						appToken: this.props.appToken
					})
				);
			} else {
				this.props.dispatch(
					communityGetAllFollower({
						appToken: this.props.appToken
					})
				);
			}
		} else {
			this.props.dispatch(
				communityGetAllClubMembers({
					appToken: this.props.appToken,
					clubId: this.clubId
				})
			);
		}
	};

	flushUserListData = () => {
		if (this.isFollowing !== null && this.isFollowing !== undefined) {
			if (this.isFollowing) {
				this.props.dispatch(communitySaveAllFollowing(null));
			} else {
				this.props.dispatch(communitySaveAllFollower(null));
			}
		} else {
			this.props.dispatch(communitySaveAllClubMembers(null));
		}
	};

	checkIsInLoadingState() {
		if (this.isFollowing !== null && this.isFollowing !== undefined) {
			if (this.isFollowing) {
				return (
					this.props.following === null || this.props.following === undefined
				);
			} else {
				return (
					this.props.followers === null || this.props.followers === undefined
				);
			}
		} else {
			return (
				this.props.clubMembers === null || this.props.clubMembers === undefined
			);
		}
	}

	renderUserImage = userProfile => {
		try {
			let image = userProfile.image;
			if (
				image !== null &&
				image !== undefined &&
				image.trim() !== "" &&
				image.startsWith("http")
			) {
				return <Image source={{ uri: image }} style={styles.userImage} />;
			} else {
				throw new Error("User profile pic not found");
			}
		} catch (error) {
			return (
				<View style={styles.noUserImage}>
					<Icon
						iconType={"ionicon"}
						iconSize={width / 12}
						iconName={"ios-person"}
						iconColor={"rgba(0,0,0,0.5)"}
					/>
				</View>
			);
		}
	};

	getData = () => {
		if (this.isFollowing !== null && this.isFollowing !== undefined) {
			if (this.isFollowing) {
				let data = this.props.following;
				return data;
			} else {
				let data = this.props.followers;
				return data;
			}
		} else {
			return this.props.clubMembers;
		}
	};

	getHeader = () => {
		if (this.isFollowing !== null && this.isFollowing !== undefined) {
			if (this.isFollowing) {
				return I18n.t("com_following");
			} else {
				return I18n.t("com_follower").other;
			}
		} else {
			return I18n.t("com_group_member");
		}
	};

	shouldComponentUpdate(props, state) {
		if (
			props.error &&
			props.error !== this.props.error &&
			props.error.isError &&
			props.error.errorScreen === "UserListView"
		) {
			Alert.alert("Error", props.error.errorMessage);
			this.props.dispatch(communityResetError());
			this.props.navigation.goBack();
		}
		return true;
	}

	getUserListType = () => {
		if (this.isFollowing !== null && this.isFollowing !== undefined) {
			if (this.isFollowing) {
				return 1;
			} else {
				return 2;
			}
		} else {
			return 3;
		}
	};

	getEmptyMessage = () => {
		if (this.isFollowing !== null && this.isFollowing !== undefined) {
			if (this.isFollowing) {
				return I18n.t("com_following_no_one");
			} else {
				return I18n.t("com_no_followers");
			}
		} else {
			return I18n.t("com_no_club_members");
		}
	};

	handleNavigationToUserProfile = (item, clubId) => {
		if (this.props.myProfile.user_id === item.user_id) {
			this.props.navigation.navigate("UserProfile");
		} else {
			this.props.navigation.push("UserProfile", {
				userInfo: item,
				needToRefresh: true,
				pageType: this.getUserListType(),
				clubId
			});
		}
	};

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<TouchableOpacity
						style={styles.backButton}
						onPress={() => this.props.navigation.goBack()}>
						<Icon
							iconType={"material"}
							iconSize={width / 18}
							iconName={"arrow-back"}
							iconColor={"#000"}
						/>
					</TouchableOpacity>
					<CommunityTextMedium
						size={height / 45}
						numberOfLines={1}
						style={styles.headerText}>
						{this.getHeader()}
					</CommunityTextMedium>
				</View>
				{this.checkIsInLoadingState() ? (
					<View style={styles.progressScreenView}>
						<CommunityLoader size={width / 15} />
					</View>
				) : (
					<FlatList
						data={this.getData()}
						keyExtractor={(item, index) => index.toString()}
						renderItem={({ item, index }) => {
							console.log("item to render is", item);
							return (
								<TouchableOpacity
									activeOpacity={1}
									style={styles.userList}
									onPress={() =>
										this.handleNavigationToUserProfile(item, this.clubId)
									}>
									{this.renderUserImage(item)}
									<View style={styles.userInfoSection}>
										<CommunityTextMedium size={height / 45} numberOfLines={1}>
											{item.name}
										</CommunityTextMedium>
									</View>
								</TouchableOpacity>
							);
						}}
						ListEmptyComponent={() => (
							<EmptyView
								noRecordFoundMesage={this.getEmptyMessage()}
								sorryMessage={I18n.t("com_sorry")}
								onRefresh={() => {
									this.flushUserListData();
									this.callUserListApi();
								}}
							/>
						)}
					/>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		error: state.community && state.community.error,
		myProfile: state.community && state.community.profile.myProfile,
		myPosts: state.community && state.community.myProfileReducer.myPosts,
		followers: state.community && state.community.profile.myProfile.followers,
		following: state.community && state.community.profile.myProfile.following,
		clubMembers: state.community && state.community.myProfileReducer.clubMembers
	};
}

export default connect(mapStateToProps)(UserListView);
