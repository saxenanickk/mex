import React from "react";
import { StyleSheet, Dimensions, Platform } from "react-native";
const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff"
	},
	header: {
		flexDirection: "row",
		backgroundColor: "#fff",
		paddingHorizontal: width / 30,
		width: width,
		height: height / 15,
		alignItems: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	headerText: {
		marginLeft: width / 30,
		width: width / 1.5
	},
	progressScreenView: {
		marginTop: height / 5,
		justifyContent: "center",
		alignItems: "center"
	},
	userImage: {
		width: width / 8,
		height: width / 8,
		borderRadius: width / 16
	},
	noUserImage: {
		width: width / 8,
		height: width / 8,
		borderRadius: width / 16,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#eeeff5"
	},
	userList: {
		flexDirection: "row",
		paddingHorizontal: width / 30,
		alignItems: "center",
		height: height / 10
	},
	userInfoSection: {
		borderBottomWidth: 1,
		borderBottomColor: "#eeeff5",
		marginLeft: width / 30,
		width: width / 1.3,
		height: height / 10,
		justifyContent: "center"
	}
});
