import React, { Component } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { CommonActions } from "@react-navigation/native";
import { connect } from "react-redux";
import CommunityLoader from "../../Components/CommunityLoader";
import { communityResetError } from "../../Saga";
import { CommunityTextMedium } from "../../Components/CommunityText";
import Freshchat from "../../../../CustomModules/Freshchat";
import { communityGetMyProfile } from "../UserProfile/Saga";

const { width, height } = Dimensions.get("window");
class Splash extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isError: false
		};
	}

	isNavigated = false;

	// Name, Campus, Pic, Organisation, About

	componentDidMount() {
		this.checkAppToken(this.props);
	}

	shouldComponentUpdate(props, state) {
		if (props.appToken !== null && props.appToken !== this.props.appToken) {
			if (props.user.site_id && props.user.tenant_id) {
				this.checkAppToken(props);
			} else {
				this.setState({ isError: true });
			}
		}
		if (props.myProfile !== null && !this.isNavigated) {
			this.isNavigated = true;
			let navigationParams = this.navigateToScreen();
			// If navigation params are not available go to home screen
			if (navigationParams === null) {
				this.props.navigation.dispatch(
					CommonActions.reset({
						index: 0,
						routes: [{ name: "Home" }]
					})
				);
			}
			// or navigate to home screen and then navigate to deep linked screen
			// so that back actions will work properly
			else {
				this.props.navigation.dispatch(
					CommonActions.reset({
						index: 1,
						routes: [{ name: "Home" }, navigationParams]
					})
				);
			}
		}
		if (
			props.error &&
			props.error !== this.props.error &&
			props.error.isError &&
			props.error.errorScreen === "Splash"
		) {
			this.props.dispatch(communityResetError());
			this.setState({ isError: true });
		}
		return true;
	}

	checkAppToken(props) {
		if (props.appToken !== null) {
			this.props.dispatch(
				communityGetMyProfile({
					appToken: props.appToken
				})
			);
		}
	}

	navigateToScreen = () => {
		try {
			if (this.props.deeplinkParams) {
				let data = {};
				switch (this.props.deeplinkParams.type) {
					case "socialinvite":
						data = {
							name: "SocialInvite",
							params: {
								inviteId: this.props.deeplinkParams.invite_id
							}
						};
						return data;
					case "like":
					case "comment":
						data = {
							name: "PostDetail",
							params: {
								postId: this.props.deeplinkParams.post_id
							}
						};
						return data;
					case "new follower":
						data = {
							name: "UserProfile",
							params: {
								userInfo: {
									user_id: this.props.deeplinkParams.user_id
								}
							}
						};
						return data;
				}
			}
			return null;
		} catch (error) {
			return null;
		}
	};

	render() {
		return (
			<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
				{this.state.isError === false ? (
					<CommunityLoader />
				) : (
					<View
						style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
						<CommunityTextMedium
							color={"#000"}
							size={height / 45}
							style={{ textAlign: "center" }}>
							{
								"Sorry, you are not an existing user or you can check your corporate login profile"
							}
						</CommunityTextMedium>
						<CommunityTextMedium
							color={"#000"}
							size={height / 50}
							style={{ textAlign: "center" }}>
							{"Please Contact our support desk"}
						</CommunityTextMedium>
						<TouchableOpacity
							onPress={() => Freshchat.launchSupportChat()}
							style={{
								alignSelf: "center",
								marginTop: height / 50,
								paddingHorizontal: width / 20,
								paddingVertical: height / 80,
								backgroundColor: "#000",
								borderRadius: width / 30
							}}>
							<CommunityTextMedium color={"#fff"} size={height / 50}>
								{"Support"}
							</CommunityTextMedium>
						</TouchableOpacity>
					</View>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	console.log("state is", state);
	return {
		appToken: state.appToken.token,
		user: state.appToken.user,
		myProfile: state.community && state.community.profile.myProfile,
		error: state.community && state.community.error,
		deeplinkParams: state.community && state.community.error.deeplinkParams
	};
}

export default connect(mapStateToProps)(Splash);
