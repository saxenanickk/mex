// @ts-check
import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api";
import { communitySaveError } from "../../Saga";

//  Constants
export const COMMUNITY_GET_INVITE_DETAILS = "COMMUNITY_GET_INVITE_DETAILS";
export const COMMUNITY_SAVE_INVITE_DETAILS = "COMMUNITY_SAVE_INVITE_DETAILS";
export const COMMUNITY_REJECT_ACCEPT_INVITE = "COMMUNITY_REJECT_ACCEPT_INVITE";
export const COMMUNITY_BLOCK_INVITE = "COMMUNITY_BLOCK_INVITE";
export const COMMUNITY_SAVE_INVITATION_RESPONSE =
	"COMMUNITY_SAVE_INVITATION_RESPONSE";
//  Action Creators
export const communityGetInviteDetails = payload => ({
	type: COMMUNITY_GET_INVITE_DETAILS,
	payload
});
export const communitySaveInviteDetails = payload => ({
	type: COMMUNITY_SAVE_INVITE_DETAILS,
	payload
});
export const communityRejectAcceptInvite = payload => ({
	type: COMMUNITY_REJECT_ACCEPT_INVITE,
	payload
});
export const communityBlockInvite = payload => ({
	type: COMMUNITY_BLOCK_INVITE,
	payload
});
export const communitySaveInvitationResponse = payload => ({
	type: COMMUNITY_SAVE_INVITATION_RESPONSE,
	payload
});

//  Saga
export function* communitySocialInviteSaga(dispatch) {
	yield takeLatest(COMMUNITY_GET_INVITE_DETAILS, handleCommunityInviteDetails);
	yield takeEvery(
		COMMUNITY_REJECT_ACCEPT_INVITE,
		handleCommunityRejectAcceptInvite
	);
	yield takeLatest(COMMUNITY_BLOCK_INVITE, handleCommunityBlockInvite);
}

//  Handlers
function* handleCommunityInviteDetails(action) {
	try {
		let inviteResponse = yield call(Api.getInviteDetails, action.payload);
		if (inviteResponse.success === 1) {
			inviteResponse = inviteResponse.data[0];
			let invitedUserResponse = yield call(Api.getUserProfile, {
				...action.payload,
				userId: inviteResponse.invited_by_user_id
			});
			if (invitedUserResponse.success === 1) {
				yield put(
					communitySaveInviteDetails({
						inviteDetail: inviteResponse,
						invitedUserDetail: invitedUserResponse.data
					})
				);
			} else {
				throw new Error("Unable to get Invite Details");
			}
		} else {
			throw new Error("Unable to get Invite Details");
		}
	} catch (error) {
		console.log(error, "error");
		yield put(
			communitySaveError({
				errorScreen: "SocialInvite",
				errorMessage: "Unable to get Invite Details",
				isError: true
			})
		);
	}
}

function* handleCommunityRejectAcceptInvite(action) {
	try {
		let response = yield call(Api.rejectAndAcceptInvite, action.payload);
		console.log("response for invitation is", response);
		if (response.success === 1) {
			yield put(
				communitySaveInvitationResponse({
					success: 1,
					message: action.payload.isAccept
						? "Invitation accepted"
						: "Invitation rejected"
				})
			);
		} else {
			throw new Error("Unable to process request");
		}
	} catch (error) {
		console.log(error, "error");
		yield put(
			communitySaveInvitationResponse({
				success: 0,
				message: "Unable to Process your request, Please try again"
			})
		);
	}
}

function* handleCommunityBlockInvite(action) {
	try {
		let response = yield call(Api.blockInvite, action.payload);
		console.log("response for invitation to block is", response);
		if (response.success === 1) {
			yield put(
				communitySaveInvitationResponse({
					success: 1,
					message: "User is blocked from sending more invitation"
				})
			);
		} else {
			throw new Error("Unable to process request");
		}
	} catch (error) {
		console.log(error, "error");
		yield put(
			communitySaveInvitationResponse({
				success: 0,
				message: "Unable to Process your request, Please try again"
			})
		);
	}
}
