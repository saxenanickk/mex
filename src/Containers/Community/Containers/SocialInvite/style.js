import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#eeeff5",
		padding: 10
	},
	header: {
		width: width,
		height: height / 12,
		backgroundColor: "#ffffff",
		paddingHorizontal: width / 30,
		flexDirection: "row",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	dataContainer: {
		backgroundColor: "#ffffff",
		borderRadius: 10,
		padding: 10,
		flex: 1,
		flexGrow: 1,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 0, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	userInfoText: {
		width: width / 1.1,
		textAlign: "center"
	},
	backButton: {
		width: width / 6,
		height: height / 12,
		justifyContent: "center"
	},
	userProfileView: {
		height: height / 15,
		justifyContent: "center",
		marginLeft: width / 30,
		borderBottomWidth: 1,
		borderBottomColor: "#ececec"
	},
	userImage: {
		width: width / 5,
		height: width / 5,
		borderRadius: width / 10
	},
	noUserImage: {
		width: width / 5,
		height: width / 5,
		borderRadius: width / 10,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#eeeff5"
	},
	userImageView: {
		justifyContent: "center",
		alignItems: "center"
	},
	userInfoSection: {
		marginTop: height / 20,
		alignItems: "center"
	},
	emailSection: {
		marginTop: height / 80
	},
	followUnfollowButton: {
		marginTop: height / 2.5,
		width: width / 3,
		paddingVertical: height / 90,
		borderRadius: width / 50,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "#f4f4f4"
	},
	followUnfollowText: {
		textAlign: "center"
	},
	userPrimaryInfoHeader: {
		flexDirection: "row",
		paddingBottom: height / 40,
		paddingHorizontal: width / 30
	},
	changeImage: {
		position: "absolute",
		bottom: -height / 90,
		left: width / 14,
		width: width / 15,
		height: width / 15,
		borderRadius: width / 30,
		backgroundColor: "#eeeff5",
		justifyContent: "center",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 2 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	professionalInfoSection: {
		flex: 1,
		paddingHorizontal: width / 30,
		justifyContent: "center"
	},
	connectedMemberCountSection: {
		borderRadius: width / 30,
		paddingVertical: height / 100,
		backgroundColor: "#f3f9ff",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		marginHorizontal: width / 60
	},
	connectedMemberCountView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	editButton: {
		width: width / 5,
		height: height / 25,
		borderColor: "#474747",
		borderWidth: 1.5,
		borderRadius: width / 60,
		justifyContent: "center",
		alignItems: "center"
	},
	clubJoinedSection: {
		width: width * 0.92,
		alignSelf: "center",
		paddingVertical: height / 60
	},
	imageView: {
		width: width / 7,
		height: width / 7,
		borderRadius: width / 14,
		backgroundColor: "#fff",
		justifyContent: "center",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 3 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.2
			}
		})
	},
	clubImage: {
		width: width / 12,
		height: width / 12
	},
	joinedClubSection: {
		width: width * 0.92,
		flexDirection: "row",
		alignItems: "center",
		flexWrap: "wrap"
	},
	joinedClubSingleView: {
		width: width * 0.23,
		paddingVertical: height / 90
	},
	joinClubButton: {
		borderWidth: 1,
		borderColor: "#8cb9e0"
	},
	manageSkillsSection: {
		width: width * 0.92,
		alignSelf: "center",
		marginTop: height / 90
	},
	lowerButtons: {
		flexDirection: "row",
		justifyContent: "space-evenly"
	},
	lowerButton: {
		padding: 10,
		flex: 1,
		borderWidth: 0.5,
		backgroundColor: "#ffffff",
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 5
	},
	progressScreen: {
		height: height,
		position: "absolute",
		top: 0,
		left: 0,
		bottom: 0,
		right: 0,
		backgroundColor: "rgba(0,0,0,0.5)",
		justifyContent: "center",
		alignItems: "center",
		elevation: 10,
		zIndex: 10
	}
});
