import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	Image,
	ScrollView,
	BackHandler,
	Alert
} from "react-native";
import { connect } from "react-redux";
import { Icon } from "../../../../Components";
import CommunityLoader from "../../Components/CommunityLoader";
import {
	CommunityTextMedium,
	CommunityTextBold,
	CommunityTextRegular
} from "../../Components/CommunityText";
import {
	communityGetInviteDetails,
	communitySaveInvitationResponse,
	communityBlockInvite,
	communityRejectAcceptInvite
} from "./Saga";
import { styles } from "./style";
import CommunityHeader from "../../Components/Header";
import MultipleItemSelector from "../../../../Components/MultipleItemSelector";
import { replace } from "lodash";
import { CommonActions } from "@react-navigation/native";

const { width, height } = Dimensions.get("window");
class SocialInvite extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: false,
			actionType: null,
			isActionSuccess: false
		};

		const { route } = props;
		const { inviteId = null } = route.params ? route.params : {};
		this.inviteId = inviteId;

		this.goBack = this.goBack.bind(this);
		this.backHandler = BackHandler.addEventListener(
			"hardwareBackPress",
			this.goBack
		);
		if (this.inviteId !== null) {
			this.props.dispatch(
				communityGetInviteDetails({
					appToken: this.props.appToken,
					inviteId: this.inviteId
				})
			);
		}
	}

	componentWillUnmount() {
		this.backHandler.remove();
	}

	shouldComponentUpdate(props, state) {
		if (
			props.invitationResponse &&
			props.invitationResponse !== this.props.invitationResponse
		) {
			Alert.alert("Information", props.invitationResponse.message);
			this.props.dispatch(communitySaveInvitationResponse(null));
			this.setState({
				isLoading: false,
				isActionSuccess: props.invitationResponse.success === 1 ? true : false
			});
		}
		return true;
	}
	renderUserImage = userProfile => {
		try {
			let image = userProfile.image;
			if (
				image !== null &&
				image !== undefined &&
				image.trim() !== "" &&
				image.startsWith("http")
			) {
				return (
					<View style={styles.userImageView}>
						<Image source={{ uri: image }} style={styles.userImage} />
					</View>
				);
			} else {
				throw new Error("User profile pic not found");
			}
		} catch (error) {
			console.log("error is", error);
			return (
				<View style={styles.noUserImage}>
					<Icon
						iconType={"ionicon"}
						iconSize={width / 10}
						iconName={"ios-person"}
						iconColor={"rgba(0,0,0,0.5)"}
					/>
				</View>
			);
		}
	};

	getGenderAndAge = userProfile => {
		try {
			let data = "";
			if (userProfile.gender) {
				data = data + userProfile.gender;
				if (userProfile.date_of_birth) {
					let age =
						new Date() - new Date(userProfile.date_of_birth).getFullYear();
					data = data + ", " + age;
				}
				return (
					<CommunityTextMedium color={"#727272"} numberOfLines={1}>
						{data}
					</CommunityTextMedium>
				);
			} else {
				return null;
			}
		} catch (error) {
			return null;
		}
	};

	actionOnInvitation = actionType => {
		this.setState({ isLoading: true, actionType: actionType });
		if (actionType === "block") {
			this.props.dispatch(
				communityBlockInvite({
					appToken: this.props.appToken,
					inviteId: this.props.inviteDetail.inviteDetail.id,
					invitedUserId: this.props.inviteDetail.inviteDetail.invited_by_user_id
				})
			);
		} else {
			const isAccept = actionType === "accept" ? true : false;
			this.props.dispatch(
				communityRejectAcceptInvite({
					appToken: this.props.appToken,
					inviteId: this.props.inviteDetail.inviteDetail.id,
					isAccept
				})
			);
		}
	};
	renderAttributes = (userProfile, type) => {
		try {
			if (userProfile[type] && userProfile[type].length > 0) {
				let matchArray =
					userProfile.matched && userProfile.matched[type]
						? userProfile.matched[type]
						: [];
				matchArray = matchArray.map(item => {
					item = replace(item, "<em>", "");
					item = replace(item, "</em>", "");
					return item;
				});
				let data = userProfile[type].map(item => {
					let obj = {};
					obj.value = item;
					obj.isMatching = matchArray.findIndex(value => value === item) >= 0;
					return obj;
				});
				return data;
			} else {
				return [];
			}
		} catch (error) {
			console.log("error is", error);
			return [];
		}
	};

	async goBack() {
		if (!this.props.navigation.goBack()) {
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "Home" }]
				})
			);
		}
	}
	render() {
		const profileToDisplay =
			this.props.inviteDetail && this.props.inviteDetail.invitedUserDetail
				? this.props.inviteDetail.invitedUserDetail
				: null;
		const inviteDetail =
			this.props.inviteDetail && this.props.inviteDetail.inviteDetail
				? this.props.inviteDetail.inviteDetail
				: null;
		return (
			<View style={styles.container}>
				{profileToDisplay !== null ? (
					<View style={styles.dataContainer}>
						<View style={{ flexDirection: "row", alignItems: "center" }}>
							<CommunityHeader
								style={{ width: width / 3.2 }}
								onPress={() => this.goBack()}
							/>
							<CommunityTextBold size={height / 40}>
								{"Invite Request"}
							</CommunityTextBold>
						</View>
						<ScrollView
							style={{ flex: 1 }}
							showsVerticalScrollIndicator={false}
							keyboardShouldPersistTaps={"always"}>
							<View style={styles.userPrimaryInfoHeader}>
								{this.renderUserImage(profileToDisplay)}
								<View style={styles.professionalInfoSection}>
									<CommunityTextBold color={"#000"} numberOfLines={1}>
										{profileToDisplay.name}
									</CommunityTextBold>
									{this.getGenderAndAge()}
									{profileToDisplay.current_designation && (
										<CommunityTextMedium color={"#3e3e3e"} numberOfLines={1}>
											{profileToDisplay.current_designation}
										</CommunityTextMedium>
									)}
									{profileToDisplay.current_organisation && (
										<CommunityTextMedium color={"#3e3e3e"} numberOfLines={1}>
											{profileToDisplay.current_organisation}
										</CommunityTextMedium>
									)}
								</View>
							</View>
							<View style={styles.connectedMemberCountSection}>
								<TouchableOpacity
									style={styles.connectedMemberCountView}
									onPress={this.handlePostsPress}
									activeOpacity={1}>
									<CommunityTextMedium>
										{profileToDisplay.number_of_post}
									</CommunityTextMedium>
									<CommunityTextRegular color={"#5e615e"}>
										{"POSTS"}
									</CommunityTextRegular>
								</TouchableOpacity>
								<View style={styles.connectedMemberCountView}>
									<CommunityTextMedium>
										{profileToDisplay.number_of_follower}
									</CommunityTextMedium>
									<CommunityTextRegular color={"#5e615e"}>
										{"FOLLOWERS"}
									</CommunityTextRegular>
								</View>
								<View style={styles.connectedMemberCountView}>
									<CommunityTextMedium>
										{profileToDisplay.number_of_following}
									</CommunityTextMedium>
									<CommunityTextRegular color={"#5e615e"}>
										{"FOLLOWING"}
									</CommunityTextRegular>
								</View>
							</View>
							{inviteDetail && inviteDetail.social_lobby_info ? (
								<View style={styles.manageSkillsSection}>
									<CommunityTextBold color={"#a3a3a3"}>
										{"Meet at"}
									</CommunityTextBold>
									<CommunityTextRegular
										style={{
											paddingLeft: width / 60
										}}>{`${inviteDetail.social_lobby_info.name}, ${
										inviteDetail.social_lobby_info.address
									}`}</CommunityTextRegular>
								</View>
							) : null}
							<View style={styles.manageSkillsSection}>
								<MultipleItemSelector
									data={this.renderAttributes(profileToDisplay, "skills")}
									type={"Skills"}
								/>
							</View>
							<View style={styles.manageSkillsSection}>
								<MultipleItemSelector
									data={this.renderAttributes(
										profileToDisplay,
										"hobbies_interests"
									)}
									type={"Interests"}
								/>
							</View>
						</ScrollView>
						{profileToDisplay ? (
							this.state.isActionSuccess ? (
								<View style={styles.lowerButtons}>
									<View
										style={[
											styles.lowerButton,
											{ backgroundColor: "#ffffff" }
										]}>
										<CommunityTextMedium
											color={
												this.state.actionType === "accept"
													? "#55A810"
													: this.state.actionType === "reject"
													? "#3b64e5"
													: "#DF535B"
											}
											size={height / 50}>
											{this.state.actionType === "accept"
												? "Invitation accepted"
												: this.state.actionType === "reject"
												? "Invitation rejected"
												: "User blocked"}
										</CommunityTextMedium>
									</View>
								</View>
							) : (
								<View style={styles.lowerButtons}>
									<TouchableOpacity
										onPress={() => this.actionOnInvitation("accept")}
										style={[
											styles.lowerButton,
											{ backgroundColor: "#ffffff" }
										]}>
										<CommunityTextMedium color={"#55A810"} size={height / 50}>
											{"Accept"}
										</CommunityTextMedium>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() => this.actionOnInvitation("reject")}
										style={[styles.lowerButton]}>
										<CommunityTextMedium color={"#3b64e5"} size={height / 50}>
											{"Reject"}
										</CommunityTextMedium>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() => this.actionOnInvitation("block")}
										style={[
											styles.lowerButton,
											{ backgroundColor: "#ffffff" }
										]}>
										<CommunityTextMedium color={"#DF535B"} size={height / 50}>
											{"Block"}
										</CommunityTextMedium>
									</TouchableOpacity>
								</View>
							)
						) : null}
					</View>
				) : (
					<View
						style={{
							height: height,
							justifyContent: "center",
							alignItems: "center"
						}}>
						<CommunityLoader />
					</View>
				)}

				{this.state.isLoading && (
					<View style={styles.progressScreen}>
						<CommunityLoader />
					</View>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	console.log("state is", state);
	return {
		appToken: state.appToken.token,
		error: state.community && state.community.error,
		inviteDetail:
			state.community && state.community.socialInviteReducer.inviteDetail,
		invitationResponse:
			state.community && state.community.socialInviteReducer.invitationResponse
	};
}

export default connect(mapStateToProps)(SocialInvite);
