import {
	COMMUNITY_SAVE_INVITE_DETAILS,
	COMMUNITY_SAVE_INVITATION_RESPONSE
} from "./Saga";

const initialState = {
	inviteDetail: null,
	invitationResponse: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case COMMUNITY_SAVE_INVITE_DETAILS:
			return {
				...state,
				inviteDetail: action.payload
			};
		case COMMUNITY_SAVE_INVITATION_RESPONSE:
			return {
				...state,
				invitationResponse: action.payload
			};
		default:
			return state;
	}
};
