import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	FlatList,
	Alert
} from "react-native";
import { connect } from "react-redux";
import { styles } from "./style";
import I18n from "../../Assets/Strings/i18n";
import { Icon } from "../../../../Components";
import FastImage from "react-native-fast-image";
import { CommunityTextMedium } from "../../Components/CommunityText";
import {
	communityGetLikedMembders,
	communitySaveLikedMembders
} from "../Activity/Saga";
import CommunityLoader from "../../Components/CommunityLoader";
import { communityResetError } from "../../Saga";
const { width, height } = Dimensions.get("window");

class LikedMember extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		const { route } = props;
		const { postId, likeCount, userId } = route.params ? route.params : {};
		this.postId = postId;
		this.likeCount = likeCount;
		this.userId = userId;
	}

	componentDidMount() {
		const postId = this.postId || null;
		this.props.dispatch(
			communityGetLikedMembders({
				postId: postId,
				appToken: this.props.appToken
			})
		);
	}

	shouldComponentUpdate(props, state) {
		if (
			props.error &&
			props.error !== this.props.error &&
			props.error.isError &&
			props.error.errorScreen === "LikedMember"
		) {
			Alert.alert(I18n.t("error"), props.error.errorMessage);
			this.props.dispatch(communityResetError());
			this.props.navigation.goBack();
		}
		return true;
	}

	componentWillUnmount() {
		this.props.dispatch(communitySaveLikedMembders(null));
	}
	render() {
		const likeCount = this.likeCount || null;
		const userId = this.userId || null;

		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<TouchableOpacity
						style={styles.backButton}
						onPress={() => this.props.navigation.goBack()}>
						<Icon
							iconType={"material"}
							iconSize={width / 18}
							iconName={"arrow-back"}
							iconColor={"#000"}
						/>
					</TouchableOpacity>
					{likeCount && (
						<CommunityTextMedium color={"#000"} size={height / 40}>
							{I18n.t("com_like", {
								count: likeCount
							})}
						</CommunityTextMedium>
					)}
				</View>
				{this.props.likedMembers !== null ? (
					<FlatList
						style={{ height: height }}
						keyExtractor={(item, index) => index.toString()}
						data={this.props.likedMembers}
						renderItem={({ item, index }) => {
							return (
								<TouchableOpacity
									style={styles.userProfileView}
									onPress={() => {
										console.log("navigate");
										if (userId === item.id) {
											this.props.navigation.navigate("UserProfile");
										} else {
											// TODO - fix userprofile screen to handle
											if (!item.hasOwnProperty("user_id")) {
												item.user_id = item.id;
											}
											this.props.navigation.navigate("UserProfile", {
												userInfo: item
											});
										}
									}}>
									{item.image ? (
										<FastImage
											source={{ uri: item.image }}
											style={styles.userImage}
										/>
									) : (
										<Icon
											style={styles.NoinageCommentedUser}
											iconType={"ionicon"}
											iconSize={height / 33}
											iconName={"ios-person"}
											iconColor={"#000"}
										/>
									)}
									<CommunityTextMedium color={"#000"} size={height / 45}>
										{item.name}
									</CommunityTextMedium>
								</TouchableOpacity>
							);
						}}
					/>
				) : (
					<View style={styles.loadingContainer}>
						<CommunityLoader size={width / 20} />
					</View>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		error: state.community && state.community.error,
		likedMembers: state.community && state.community.postReducer.likedMembers
	};
}

export default connect(mapStateToProps)(LikedMember);
