import React from "react";
import { StyleSheet, Dimensions, Platform } from "react-native";
const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff"
	},
	loadingContainer: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	header: {
		width: width,
		height: height / 12,
		backgroundColor: "#f4f4f4",
		paddingHorizontal: width / 30,
		flexDirection: "row",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	backButton: {
		width: width / 6,
		height: height / 12,
		justifyContent: "center"
	},
	userProfileView: {
		alignItems: "center",
		marginLeft: width / 30,
		paddingVertical: height / 35,
		borderBottomWidth: 1,
		borderBottomColor: "#ececec",
		flexDirection: "row"
	},
	userImage: {
		height: width / 10,
		width: width / 10,
		borderRadius: width / 20,
		marginRight: width / 80
	},
	NoinageCommentedUser: {
		color: "grey",
		backgroundColor: "#c3c3c3",
		paddingVertical: width / 60,
		paddingHorizontal: width / 40,
		borderRadius: width / 20,
		marginRight: width / 80
	}
});
