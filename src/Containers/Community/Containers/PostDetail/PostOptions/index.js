import React from "react";
import {
	View,
	Image,
	TouchableOpacity,
	Alert,
	Dimensions,
	Modal
} from "react-native";
import I18n from "../../../Assets/Strings/i18n";
import { styles, dialogStyle } from "./style";
import { CommunityTextMedium } from "../../../Components/CommunityText";
import {
	communityDeletePost,
	communityUserReportPost,
	communityUserHidePost,
	communityFollowUnfollowUser
} from "../Saga";
import { REPORT, HIDE, UNFOLLOW, DELETE } from "../../../Assets/Img/Image";
const { height } = Dimensions.get("window");
function PostOptions(props) {
	const {
		postDetail,
		openModal,
		setModal,
		setLoading,
		appToken,
		dispatch,
		measurements,
		isClubPost
	} = props;

	const followUnfollowUser = () => {
		dispatch(
			communityFollowUnfollowUser({
				appToken: appToken,
				postId: postDetail.id,
				userId: postDetail.user_id,
				isFollowing: postDetail.isFollowing,
				userName: postDetail.user_info.name,
				clubId: postDetail.club_id,
				isClubPost
			})
		);
		setModal(false);
		setLoading(true);
	};
	const reportPost = () => {
		const isMyPost = postDetail.isYourPost;
		Alert.alert(
			I18n.t("com_sure_to_report_post_header"),
			I18n.t("com_sure_to_report_post"),
			[
				{
					text: I18n.t("com_cancel"),
					onPress: () => {
						setModal(false);
					},
					style: "cancel"
				},
				{
					text: I18n.t("com_ok"),
					onPress: () => {
						dispatch(
							communityUserReportPost({
								appToken,
								postId: postDetail.id,
								uniqueIdentifier: postDetail.uniqueIdentifier,
								clubId: postDetail.club_id,
								isClubPost,
								isMyPost
							})
						);
						setModal(false);
						setLoading(true);
					}
				}
			],
			{ cancelable: false }
		);
	};
	const hidePost = () => {
		dispatch(
			communityUserHidePost({
				appToken,
				postId: postDetail.id,
				uniqueIdentifier: postDetail.uniqueIdentifier,
				clubId: postDetail.club_id,
				isClubPost
			})
		);
		setModal(false);
		setLoading(true);
	};
	const deletePost = () => {
		const isMyPost = postDetail.is_my_post;
		Alert.alert(
			I18n.t("information"),
			I18n.t("com_sure_to_delete_post"),
			[
				{
					text: I18n.t("com_cancel"),
					onPress: () => {
						setModal(false);
					},
					style: "cancel"
				},
				{
					text: I18n.t("com_ok"),
					onPress: () => {
						setModal(false);
						setLoading(true);
						dispatch(
							communityDeletePost({
								appToken,
								postId: postDetail.id,
								uniqueIdentifier: postDetail.uniqueIdentifier,
								postType: "post",
								clubId: postDetail.club_id,
								isClubPost,
								isMyPost
							})
						);
					}
				}
			],
			{ cancelable: false }
		);
	};
	const renderCommentFollowDialog = () => {
		return openModal ? (
			<Modal
				visible={true}
				onRequestClose={() => setModal(false)}
				transparent={true}>
				<View style={styles.container}>
					<TouchableOpacity
						style={styles.modalContainer}
						onPress={() => setModal(false)}>
						<View style={dialogStyle(measurements).optionContainer}>
							{!postDetail.isYourPost && (
								<TouchableOpacity
									style={styles.rowOptions}
									onPress={() => reportPost()}>
									<Image
										source={REPORT}
										style={styles.editImage}
										resizeMode={"contain"}
									/>
									<View style={styles.postOptionDescription}>
										<CommunityTextMedium color={"#000"} size={height / 50}>
											{I18n.t("com_report_this_post")}
										</CommunityTextMedium>
										<CommunityTextMedium color={"#6e6e6e"} size={height / 65}>
											{I18n.t("com_report_post_desc")}
										</CommunityTextMedium>
									</View>
								</TouchableOpacity>
							)}
							<View style={styles.rowOptions}>
								<Image
									source={HIDE}
									style={styles.editImage}
									resizeMode={"contain"}
								/>
								<TouchableOpacity
									style={styles.postOptionDescription}
									onPress={() => hidePost()}>
									<CommunityTextMedium color={"#000"} size={height / 50}>
										{I18n.t("com_hide_this_post")}
									</CommunityTextMedium>
									<CommunityTextMedium color={"#6e6e6e"} size={height / 65}>
										{I18n.t("com_see_few_post")}
									</CommunityTextMedium>
								</TouchableOpacity>
							</View>
							{!postDetail.isYourPost && postDetail.user_info !== null ? (
								<TouchableOpacity
									style={styles.rowOptions}
									onPress={() => followUnfollowUser()}>
									<Image
										source={UNFOLLOW}
										style={styles.editImage}
										resizeMode={"contain"}
									/>
									<View style={styles.postOptionDescription}>
										<CommunityTextMedium color={"#000"} size={height / 50}>
											{postDetail.isFollowing
												? `${I18n.t("com_unfollow")} ${
														postDetail.user_info.name
												  }`
												: `${I18n.t("com_follow")} ${
														postDetail.user_info.name
												  }`}
										</CommunityTextMedium>
										<CommunityTextMedium color={"#6e6e6e"} size={height / 65}>
											{postDetail.isFollowing
												? I18n.t("com_stop_see_post")
												: I18n.t("com_start_see_post")}
										</CommunityTextMedium>
									</View>
								</TouchableOpacity>
							) : null}
							{postDetail.isYourPost && (
								<TouchableOpacity
									style={styles.rowOptions}
									onPress={() => deletePost()}>
									<Image
										source={DELETE}
										style={styles.editImage}
										resizeMode={"contain"}
									/>
									<View style={styles.postOptionDescription}>
										<CommunityTextMedium color={"#000"} size={height / 50}>
											{I18n.t("com_delete_post")}
										</CommunityTextMedium>
										<CommunityTextMedium color={"#6e6e6e"} size={height / 65}>
											{I18n.t("com_delete_post_from_wall")}
										</CommunityTextMedium>
									</View>
								</TouchableOpacity>
							)}
						</View>
					</TouchableOpacity>
				</View>
			</Modal>
		) : null;
	};
	return renderCommentFollowDialog();
}
export default PostOptions;
