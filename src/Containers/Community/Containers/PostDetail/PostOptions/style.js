import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	modalContainer: {
		height: height,
		width: width,
		backgroundColor: "rgba(0,0,0,0.5)"
	},
	optionContainer: {
		width: width / 1.2,
		position: "absolute",
		right: width / 12,
		top: height / 8,
		backgroundColor: "#f4f4f4",
		borderRadius: width / 60,
		paddingHorizontal: width / 30
	},
	rowOptions: {
		borderBottomWidth: 0.5,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingVertical: height / 40,
		borderBottomColor: "#848484",
		paddingHorizontal: width / 60
	},
	postOptionDescription: {
		alignItems: "flex-start",
		width: width / 1.6
	},
	editImage: {
		width: width / 20,
		height: width / 20
	},
	reportText: {
		color: "#6e6e6e",
		fontSize: height / 50
	},
	hideText: {
		color: "#6e6e6e",
		fontSize: height / 65
	},
	container: {
		height: height,
		width: width,
		justifyContent: "center",
		alignItems: "center"
	}
});
export const dialogStyle = measurements => {
	const { pageY, height: iconHeight } = measurements;
	console.log("pageY", pageY);
	return StyleSheet.create({
		optionContainer: {
			width: width / 1.2,
			position: "absolute",
			right: width / 12,
			top:
				height - pageY > height / 2.5
					? pageY + iconHeight
					: pageY - height / 2.8,
			backgroundColor: "#f4f4f4",
			borderRadius: width / 60,
			paddingHorizontal: width / 30
		}
	});
};
