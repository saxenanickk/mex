// @ts-check
import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api";
import { communitySaveError } from "../../Saga";
import { communityGetPost, communityGetClubPost } from "../Activity/Saga";
import { communityGetMyPost } from "../EditProfile/Saga";
import {
	communityGetAllFollowing,
	communityGetAllFollower
} from "../UserProfile/Saga";

export const COMMUNITY_GET_SINGLE_POST_DETAIL =
	"COMMUNITY_GET_SINGLE_POST_DETAIL";
export const COMMUNITY_SAVE_SINGLE_POST_DETAIL =
	"COMMUNITY_SAVE_SINGLE_POST_DETAIL";
export const COMMUNITY_GET_SINGLE_POST_COMMENT =
	"COMMUNITY_GET_SINGLE_POST_COMMENT";
export const COMMUNITY_SAVE_SINGLE_POST_COMMENT =
	"COMMUNITY_SAVE_SINGLE_POST_COMMENT";
export const COMMUNITY_SAVE_COMMENT_OFFSET = "COMMUNITY_SAVE_COMMENT_OFFSET";
export const COMMUNITY_ADD_NEW_COMMENT = "COMMUNITY_ADD_NEW_COMMENT";
export const COMMUNITY_LIKE_UNLIKE_POST = "COMMUNITY_LIKE_POST";
export const COMMUNITY_DELETE_POST = "COMMUNITY_DELETE_POST";
export const COMMUNITY_USER_SAVE_POST = "COMMUNITY_USER_SAVE_POST";
export const COMMUNITY_USER_REPORT_POST = "COMMUNITY_USER_REPORT_POST";
export const COMMUNITY_USER_HIDE_POST = "COMMUNITY_USER_HIDE_POST";
export const COMMUNITY_FOLLOW_UNFOLLOW_USER = "COMMUNITY_FOLLOW_UNFOLLOW_USER";
export const COMMUNITY_POST_USER_ACTIVITY_SUCCESS =
	"COMMUNITY_POST_USER_ACTIVITY_SUCCESS";
export const COMMUNITY_DELETE_POST_COMMENT = "COMMUNITY_DELETE_POST_COMMENT";
export const COMMUNITY_LOCAL_DELETE_POST_COMMENT =
	"COMMUNITY_LOCAL_DELETE_POST_COMMENT";

//action creators

export const communityGetSinglePostDetail = payload => ({
	type: COMMUNITY_GET_SINGLE_POST_DETAIL,
	payload
});
export const communitySaveSinglePostDetail = payload => ({
	type: COMMUNITY_SAVE_SINGLE_POST_DETAIL,
	payload
});
export const communityGetSinglePostComment = payload => ({
	type: COMMUNITY_GET_SINGLE_POST_COMMENT,
	payload
});
export const communitySaveSinglePostComment = payload => ({
	type: COMMUNITY_SAVE_SINGLE_POST_COMMENT,
	payload
});
export const communitySaveCommentOffset = payload => ({
	type: COMMUNITY_SAVE_COMMENT_OFFSET,
	payload
});
export const communityLikeOrUnlikePost = payload => ({
	type: COMMUNITY_LIKE_UNLIKE_POST,
	payload
});
export const communityDeletePost = payload => ({
	type: COMMUNITY_DELETE_POST,
	payload
});
export const communityUserSavePost = payload => ({
	type: COMMUNITY_USER_SAVE_POST,
	payload
});
export const communityUserReportPost = payload => ({
	type: COMMUNITY_USER_REPORT_POST,
	payload
});
export const communityPostUserActivitySuccess = payload => ({
	type: COMMUNITY_POST_USER_ACTIVITY_SUCCESS,
	payload
});
export const communityUserHidePost = payload => ({
	type: COMMUNITY_USER_HIDE_POST,
	payload
});
export const communityFollowUnfollowUser = payload => ({
	type: COMMUNITY_FOLLOW_UNFOLLOW_USER,
	payload
});
export const communityAddNewComment = payload => ({
	type: COMMUNITY_ADD_NEW_COMMENT,
	payload
});
export const communityDeletePostComment = payload => ({
	type: COMMUNITY_DELETE_POST_COMMENT,
	payload
});
export const communityLocalDeletePostComment = payload => ({
	type: COMMUNITY_LOCAL_DELETE_POST_COMMENT,
	payload
});
/**
 * Saga
 * @param {any} dispatch
 */
export function* communityPostDetailSaga(dispatch) {
	yield takeLatest(
		COMMUNITY_GET_SINGLE_POST_COMMENT,
		handleCommunityGetSinglePostComment
	);
	yield takeLatest(
		COMMUNITY_GET_SINGLE_POST_DETAIL,
		handleCommunityGetSinglePostDetail
	);
	yield takeEvery(COMMUNITY_LIKE_UNLIKE_POST, handleCommunityLikeUnlikePost);
	yield takeEvery(COMMUNITY_DELETE_POST, handleCommunityDeletePost);
	yield takeEvery(COMMUNITY_USER_SAVE_POST, handleCommunitySavePost);
	yield takeEvery(COMMUNITY_USER_REPORT_POST, handleCommunityReportPost);
	yield takeEvery(COMMUNITY_USER_HIDE_POST, handleCommunityHidePost);
	yield takeEvery(
		COMMUNITY_FOLLOW_UNFOLLOW_USER,
		handleCommunityFollowUnfollowUser
	);
	yield takeEvery(
		COMMUNITY_DELETE_POST_COMMENT,
		handleCommunityDeletePostComment
	);
}

// Handlers

function* handleCommunityGetSinglePostComment(action) {
	try {
		let response = yield call(Api.getComments, action.payload);
		if (response.success === 1) {
			if (action.payload.isRefresh === true) {
				yield put(
					communitySaveSinglePostComment({
						comments: response.data.comments,
						isRefresh: true
					})
				);
				yield put(communitySaveCommentOffset({ isRefresh: true }));
			} else {
				console.log("comments are", response.data);
				yield put(communitySaveSinglePostComment(response.data.comments));
			}

			yield put(communitySaveCommentOffset(response.data.offset));
		} else {
			throw new Error("unable to fetch comments");
		}
	} catch (error) {
		yield put(
			communitySaveError({
				errorScreen: "PostDetail",
				errorMessage: "Unable to  get comments",
				isError: true
			})
		);
	}
}

function* handleCommunityGetSinglePostDetail(action) {
	try {
		let response = yield call(Api.getPostDetail, action.payload);
		console.log("response is", response);
		if (response.success === 1 && response.data.post.length > 0) {
			yield put(communitySaveSinglePostDetail(response.data.post[0]));
		} else {
			throw new Error("unable to fetch post detail");
		}
	} catch (error) {
		yield put(
			communitySaveError({
				errorScreen: "PostDetail",
				errorMessage: "Unable to get post detail",
				isError: true
			})
		);
	}
}

function* handleCommunityLikeUnlikePost(action) {
	try {
		let response = yield call(Api.likeOrUnlikePost, action.payload);
		// if (response.success === 1) {
		// 	yield put(communityGetSinglePostDetail(action.payload))
		// }
	} catch (error) {
		console.log("error is", error);
	}
}

function* handleCommunityDeletePost(action) {
	try {
		console.log("payload for delete is", action.payload);
		let response = yield call(Api.removePost, action.payload);
		if (response.success === 1) {
			yield put(
				communityPostUserActivitySuccess({
					status: true,
					message: "Post deleted"
				})
			);
			if (action.payload.isClubPost) {
				yield put(
					communityGetClubPost({
						appToken: action.payload.appToken,
						clubId: action.payload.clubId,
						isRefresh: true
					})
				);
			} else if (action.payload.isMyPost === true) {
				yield put(
					communityGetMyPost({
						appToken: action.payload.appToken,
						isRefresh: true
					})
				);
			}
			yield put(
				communityGetPost({
					appToken: action.payload.appToken,
					isRefresh: true
				})
			);
		} else {
			throw new Error("Unable to delete post");
		}
	} catch (error) {
		console.log("error is", error);
		yield put(
			communitySaveError({
				errorScreen: "PostDetail",
				errorMessage: "Unable to delete post",
				isError: true
			})
		);
	}
}
function* handleCommunitySavePost(action) {
	try {
		let response = yield call(Api.savePost, action.payload);
		if (response.success === 1) {
			yield put(
				communityPostUserActivitySuccess({
					status: true,
					message: "Post saved"
				})
			);
		} else {
			throw new Error("Unable to save post");
		}
	} catch (error) {
		console.log("error is", error);
		yield put(
			communitySaveError({
				errorScreen: "PostDetail",
				errorMessage: "Unable to save post",
				isError: true
			})
		);
	}
}
function* handleCommunityReportPost(action) {
	try {
		let response = yield call(Api.reportPost, action.payload);
		if (response.success === 1) {
			yield put(
				communityPostUserActivitySuccess({
					status: true,
					message: "Post reported"
				})
			);
			if (action.payload.isClubPost) {
				yield put(
					communityGetClubPost({
						appToken: action.payload.appToken,
						clubId: action.payload.clubId,
						isRefresh: true
					})
				);
			} else if (action.payload.isMyPost === true) {
				yield put(
					communityGetMyPost({
						appToken: action.payload.appToken,
						isRefresh: true
					})
				);
			}
			yield put(
				communityGetPost({
					appToken: action.payload.appToken,
					isRefresh: true
				})
			);
		} else {
			throw new Error("Unable to report the post");
		}
	} catch (error) {
		console.log("error is", error);
		yield put(
			communitySaveError({
				errorScreen: "PostDetail",
				errorMessage: "Unable to report post",
				isError: true
			})
		);
	}
}

function* handleCommunityHidePost(action) {
	try {
		let response = yield call(Api.hidePost, action.payload);
		if (response.success === 1) {
			yield put(
				communityPostUserActivitySuccess({
					status: true,
					message: "Post hidden from your wall"
				})
			);
			if (action.payload.isClubPost) {
				yield put(
					communityGetClubPost({
						appToken: action.payload.appToken,
						clubId: action.payload.clubId,
						isRefresh: true
					})
				);
			}
			yield put(
				communityGetPost({
					appToken: action.payload.appToken,
					isRefresh: true
				})
			);
		} else {
			throw new Error("Unable to hide the post");
		}
	} catch (error) {
		console.log("error is", error);
		yield put(
			communitySaveError({
				errorScreen: "PostDetail",
				errorMessage: "Unable to hide post",
				isError: true
			})
		);
	}
}

function* handleCommunityFollowUnfollowUser(action) {
	try {
		let response = yield call(Api.followUnfollowUser, action.payload);
		if (response.success === 1) {
			yield put(
				communityPostUserActivitySuccess({
					status: true,
					message: action.payload.isFollowing
						? "you have stopped following " + action.payload.userName
						: "you have started following " + action.payload.userName
				})
			);
			// yield put(
			// 	communityGetPost({
			// 		appToken: action.payload.appToken,
			// 		isRefresh: true,
			// 	}),
			// )
			if (action.payload.needToRefresh === true) {
				const { pageType, clubId } = action.payload;
				if (pageType === 1) {
					yield put(
						communityGetAllFollowing({
							appToken: action.payload.appToken
						})
					);
				} else if (pageType === 2) {
					yield put(
						communityGetAllFollower({
							appToken: action.payload.appToken
						})
					);
				}
			}
		} else {
			throw new Error("Unable to follow the user");
		}
	} catch (error) {
		console.log("error is", error);
		yield put(
			communitySaveError({
				errorScreen: "PostDetail",
				errorMessage: action.payload.isFollowing
					? "unable to unfollow " + action.payload.userName
					: "unable to follow " + action.payload.userName,
				isError: true
			})
		);
	}
}
function* handleCommunityDeletePostComment(action) {
	try {
		yield put(communityLocalDeletePostComment(action.payload));
		let response = yield call(Api.removeComment, action.payload);
		console.log("response is", response);
	} catch (error) {}
}
