import React, { Component } from "react";
import { TouchableOpacity, Alert, View, Dimensions } from "react-native";
import { styles } from "./style";
import { Icon } from "../../../../../Components";
import I18n from "../../../Assets/Strings/i18n";
import FastImage from "react-native-fast-image";
import { communityDeletePostComment } from "../Saga";
import {
	communityChangeClubPostCommentCountLocal,
	communityChangePostCommentCountLocal
} from "../../Activity/Saga";
import { communityChangeMyPostCommentLocal } from "../../EditProfile/Saga";
import {
	CommunityTextBold,
	CommunityTextMedium
} from "../../../Components/CommunityText";

const { width, height } = Dimensions.get("window");

class CommentsList extends Component {
	deleteComment = item => {
		const { route } = this.props;
		const { isClubPost = false, isMyPost = false } = route.params
			? route.params
			: {};

		Alert.alert(
			I18n.t("information"),
			I18n.t("com_sure_to_delete_comment"),
			[
				{
					text: I18n.t("com_cancel"),
					onPress: () => {
						console.log("cancel pressed");
					},
					style: "cancel"
				},
				{
					text: I18n.t("com_ok"),
					onPress: () => {
						this.props.dispatch(
							communityDeletePostComment({
								appToken: this.props.appToken,
								uniqueIdentifier: item.uniqueIdentifier,
								postId: item.id,
								postType: "comment",
								uniqueIdentifierParent: this.props.postDetail.uniqueIdentifier
							})
						);
						if (isClubPost) {
							this.props.dispatch(
								communityChangeClubPostCommentCountLocal({
									postId: this.props.postDetail.id,
									commentCount: this.props.postDetail.comment_count
								})
							);
						}
						if (isMyPost) {
							this.props.dispatch(
								communityChangeMyPostCommentLocal({
									postId: this.props.postDetail.id,
									commentCount: this.props.postDetail.comment_count
								})
							);
						}
						this.props.dispatch(
							communityChangePostCommentCountLocal({
								postId: this.props.postDetail.id,
								commentCount: this.props.postDetail.comment_count
							})
						);
					}
				}
			],
			{ cancelable: false }
		);
	};

	navigateToUserProfile = item => {
		if (item.isYourPost) {
			this.props.navigation.navigate("UserProfile");
		} else {
			this.props.navigation.navigate("UserProfile", {
				userInfo: item
			});
		}
	};

	render() {
		const { comments } = this.props;
		return comments
			? comments.map((item, index) => {
					return (
						<View style={styles.commentArea} key={index}>
							<TouchableOpacity
								onPress={() => this.navigateToUserProfile(item)}>
								{item.user_info.image ? (
									<FastImage
										source={{ uri: item.user_info.image }}
										style={styles.commentedUserImage}
									/>
								) : (
									<Icon
										style={styles.NoinageCommentedUser}
										iconType={"ionicon"}
										iconSize={height / 33}
										iconName={"ios-person"}
									/>
								)}
							</TouchableOpacity>
							<View style={styles.descriptionBox}>
								<View style={styles.descriptionMiniBox}>
									<TouchableOpacity
										onPress={() => this.navigateToUserProfile(item)}>
										<CommunityTextBold
											style={styles.commentUserNameText}
											numberOfLines={1}
											color={"#000"}
											size={height / 50}>
											{`${item.user_info.name}  `}
										</CommunityTextBold>
									</TouchableOpacity>
									<CommunityTextMedium
										color={"#8a8a8a"}
										size={height / 55}
										style={styles.commentText}>
										{item.description}
									</CommunityTextMedium>
								</View>
								{item.isYourPost && (
									<TouchableOpacity
										style={styles.deletebutton}
										onPress={() => this.deleteComment(item)}>
										<Icon
											iconType={"ionicon"}
											iconSize={width / 22}
											iconName={"ios-trash"}
											iconColor={"#000"}
										/>
									</TouchableOpacity>
								)}
							</View>
						</View>
					);
			  })
			: null;
	}
}

export default CommentsList;
