import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

export const styles = StyleSheet.create({
	commentArea: {
		flex: 1,
		width: width,
		alignSelf: "center",
		marginBottom: height / 70,
		paddingHorizontal: width / 25,
		paddingBottom: height / 90,
		flexDirection: "row"
	},
	userName: {
		color: "#000",
		fontSize: height / 50
	},
	descriptionText: {
		color: "#8a8a8a",
		fontSize: height / 55
	},
	deletebutton: {
		alignSelf: "flex-start",
		paddingHorizontal: width / 30
	},
	commentUserNameText: {
		flex: 1
	},
	commentText: {
		width: width / 1.7
	},
	commentedUserImage: {
		height: width / 10,
		width: width / 10,
		borderRadius: width / 20
	},
	NoinageCommentedUser: {
		color: "grey",
		backgroundColor: "#c3c3c3",
		paddingVertical: width / 60,
		paddingHorizontal: width / 40,
		borderRadius: width / 20
	},
	descriptionBox: {
		flex: 1,
		flexDirection: "row",
		alignItems: "center",
		paddingHorizontal: width / 70
	},
	descriptionMiniBox: { flex: 1 }
});
