import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	mainContainer: {
		flex: 1
	},
	container: {
		flex: 1,
		backgroundColor: "#ffffff"
	},
	progressScreen: {
		position: "absolute",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "rgba(0,0,0,0.5)"
	},
	editImage: {
		width: width / 20,
		height: width / 20
	},
	postCard: {
		marginTop: height / 50,
		marginBottom: height / 50,
		alignSelf: "center",
		width: "100%",
		paddingVertical: height / 50,
		backgroundColor: "#ffffff",
		// borderRadius: width / 30,
		...Platform.select({
			android: { elevation: 1 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	userInfo: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: height / 60,
		alignItems: "center"
	},
	userImageSection: {
		flexDirection: "row",
		alignItems: "center",
		flex: 1
	},
	userPostDescription: {
		paddingHorizontal: width / 30,
		paddingVertical: height / 90
	},
	header: {
		flexDirection: "row",
		backgroundColor: "#fff",
		justifyContent: "space-between",
		paddingHorizontal: width / 30,
		width: width,
		height: height / 15,
		alignItems: "center",
		...Platform.select({
			android: { elevation: 2 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	backButton: {
		width: width / 10,
		height: height / 15,
		justifyContent: "center"
	},
	optionButton: {
		borderWidth: 0,
		paddingHorizontal: width / 30,
		paddingVertical: height / 50
	},
	loader: {
		height: height,
		width: width,
		justifyContent: "center",
		alignItems: "center"
	},
	commentsMainView: {
		backgroundColor: "white",
		paddingBottom: width / 30
	},
	commentView: {
		backgroundColor: "#edeff2"
	}
});
