import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

export const styles = StyleSheet.create({
	urlPreviewView: {
		backgroundColor: "#f4f4f4",
		marginTop: height / 90,
		paddingVertical: height / 150,
		paddingHorizontal: width / 50,
		flexDirection: "row",
		borderRadius: width / 30,
		width: width * 0.84,
		alignSelf: "center"
	},
	previewTextSection: {
		flex: 1,
		marginLeft: width / 60
	},
	fullPreviewText: {
		width: width * 0.8
	}
});
