import React from "react";
import { TouchableOpacity, View, Dimensions, Image } from "react-native";
import {
	CommunityTextRegular,
	CommunityTextBold
} from "../../../Components/CommunityText";
import { styles } from "./style";
import { startsWith } from "lodash";
import MiniApp from "../../../../../CustomModules/MiniApp";
const { height } = Dimensions.get("window");
function UrlPreview(props) {
	const { postDetail, navigation } = props;

	const renderPreviewImage = urlPreview => {
		try {
			let image = urlPreview.image;
			if (
				startsWith(image, "https://") === false &&
				startsWith(image, "http://") === false
			) {
				image = urlPreview.url + image;
			}
			return (
				<Image
					source={{ uri: image }}
					style={styles.previewImage}
					resizeMode={"cover"}
				/>
			);
		} catch (error) {
			return null;
		}
	};
	const renderUrlPreview = () => {
		try {
			let urlPreview =
				postDetail.links !== null && typeof postDetail.links === "string"
					? JSON.parse(postDetail.links)
					: null;
			if (urlPreview !== null) {
				return (
					<TouchableOpacity
						onPress={() => {
							MiniApp.launchWebApp("Community", urlPreview.url);
							// navigation.navigate("LinkView", {
							// 	url: urlPreview.url
							// })
						}}
						activeOpacity={0.3}>
						<View style={styles.urlPreviewView}>
							{urlPreview.image ? renderPreviewImage(urlPreview) : null}
							<View style={styles.previewTextSection}>
								{urlPreview.title ? (
									<CommunityTextBold
										numberOfLines={1}
										color={"#000"}
										style={
											urlPreview.image
												? styles.previewText
												: styles.fullPreviewText
										}
										size={height / 55}>
										{urlPreview.title}
									</CommunityTextBold>
								) : null}
								{urlPreview.appName ? (
									<CommunityTextRegular
										numberOfLines={1}
										color={"#000"}
										style={
											urlPreview.image
												? styles.previewText
												: styles.fullPreviewText
										}
										size={height / 55}>
										{urlPreview.appName}
									</CommunityTextRegular>
								) : null}
								{urlPreview.description ? (
									<CommunityTextRegular
										numberOfLines={3}
										color={"#000"}
										style={
											urlPreview.image
												? styles.previewText
												: styles.fullPreviewText
										}
										size={height / 55}>
										{urlPreview.description}
									</CommunityTextRegular>
								) : null}
							</View>
						</View>
					</TouchableOpacity>
				);
			} else {
				return null;
			}
		} catch (error) {
			console.log("error is", error);
			return null;
		}
	};
	return renderUrlPreview();
}
export default UrlPreview;
