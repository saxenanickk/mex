import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	Keyboard,
	ScrollView,
	Alert,
	Platform,
	KeyboardAvoidingView
} from "react-native";
import { HeaderHeightContext } from "@react-navigation/stack";
import { connect } from "react-redux";
import { Icon, GoToast } from "../../../../Components";
import { styles } from "./style";
import {
	communityGetSinglePostDetail,
	communityGetSinglePostComment,
	communitySaveSinglePostDetail,
	communitySaveSinglePostComment,
	communitySaveCommentOffset,
	communityPostUserActivitySuccess
} from "./Saga";
import I18n from "../../Assets/Strings/i18n";
import { communityResetError } from "../../Saga";
import CommunityLoader from "../../Components/CommunityLoader";
import { POST_DETAIL_COMMENT_ERROR, POST_DETAIL_ERROR } from "../../Constants";
import PostCard from "../../Components/PostCard";
import CommentsList from "./CommentsList";
import PostOptions from "./PostOptions";
import PostDetailCommentInput from "./PostDetailCommentInput";
import { ifIphoneX } from "../../../../utils/iphonexHelper";
const { width } = Dimensions.get("window");

class PostDetail extends Component {
	constructor(props) {
		super(props);
		this.state = { openModal: false, isLoading: false, measurements: {} };
		this.scrollRef = null;
		this.scrollViewHeight = 0;
		this.keyboardDidShowListener = Keyboard.addListener(
			"keyboardDidShow",
			this._keyboardDidShow
		);
		this.keyboardDidHideListener = Keyboard.addListener(
			"keyboardDidHide",
			this._keyboardDidHide
		);
	}

	componentDidMount() {
		this._unsubscribeFocus = this.props.navigation.addListener("focus", () =>
			this.fetchPostDetail()
		);
	}

	openOptions = () => {
		if (this.optionsRef) {
			this.optionsRef.measure((x, y, height, pageX, pageY) => {
				this.setState({
					measurements: { x, y, width, height, pageX, pageY },
					openModal: true
				});
			});
		}
	};

	fetchPostDetail = () => {
		const { route } = this.props;
		const { postId } = route.params ? route.params : {};

		this.props.dispatch(
			communityGetSinglePostDetail({
				appToken: this.props.appToken,
				postId
			})
		);
		this.props.dispatch(
			communityGetSinglePostComment({
				appToken: this.props.appToken,
				postId,
				isRefresh: true
			})
		);
	};

	_keyboardDidShow = event => {
		try {
			if (Platform.OS === "ios") {
				if (this.scrollRef !== null && this.scrollRef !== undefined) {
					if (this.scrollViewHeight > event.endCoordinates.height) {
						this.scrollRef.scrollTo({
							x: 0,
							y: this.scrollViewHeight - event.endCoordinates.height,
							animated: true
						});
					}
				}
			}
		} catch (error) {
			console.log("do nothing");
		}
	};

	_keyboardDidHide = () => {
		try {
			if (Platform.OS === "ios") {
				if (this.scrollRef !== null && this.scrollRef !== undefined) {
					this.scrollRef.scrollTo({
						x: 0,
						y: 0,
						animated: true
					});
				}
			}
		} catch (error) {
			console.log("do nothing");
		}
	};

	showAlert(message, type) {
		Alert.alert(type, message);
	}

	shouldComponentUpdate(props, state) {
		const isUserActivityStatusChanged =
			this.props.navigation.isFocused &&
			props.userActivityStatus !== null &&
			props.userActivityStatus !== this.props.userActivityStatus;

		if (isUserActivityStatusChanged) {
			GoToast.show(props.userActivityStatus.message, I18n.t("information"));
			this.setState({ isLoading: false });
			this.props.dispatch(communityPostUserActivitySuccess(null));
			this.props.navigation.goBack();
		}
		if (
			props.error &&
			props.error !== this.props.error &&
			props.error.isError &&
			props.error.errorScreen === "PostDetail"
		) {
			//@ts-ignore
			this.setState({ isLoading: false });
			if (props.error.errorMessage !== POST_DETAIL_COMMENT_ERROR) {
				this.showAlert(props.error.errorMessage, I18n.t("error"));
			}
			if (props.error.errorMessage === POST_DETAIL_ERROR) {
				this.props.navigation.goBack();
			}
			this.props.dispatch(communityResetError());
		}
		return true;
	}

	componentWillUnmount() {
		this.props.dispatch(communitySaveSinglePostDetail(null));
		this.props.dispatch(communitySaveSinglePostComment(null));
		this.clearPostInformation();
		this._unsubscribeFocus();
	}

	clearPostInformation = () => {
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
		this.props.dispatch(
			communitySaveCommentOffset({
				isRefresh: true
			})
		);
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	};

	getCount = (postDetail, object) => {
		if (postDetail[object] === null || postDetail === undefined) {
			return 0;
		}
		return postDetail[object].length;
	};

	openUserProfile = postDetail => {
		try {
			if (!postDetail.isYourPost) {
				const params = postDetail.isYourPost
					? {}
					: {
							userInfo: {
								user_id: postDetail.user_id
							}
					  };
				this.props.navigation.navigate("UserProfile", params);
			}
		} catch (error) {
			console.log("error in navigating to user profile", error);
		}
	};

	render() {
		const { postDetail } = this.props;
		return (
			<HeaderHeightContext.Consumer>
				{headerHeight => (
					<KeyboardAvoidingView
						style={styles.mainContainer}
						{...(Platform.OS === "ios" ? { behavior: "padding" } : {})}
						keyboardVerticalOffset={Platform.select({
							ios: headerHeight + ifIphoneX(-20, 0),
							android: 0
						})}>
						{/* Header */}
						<View style={styles.header}>
							<TouchableOpacity
								style={styles.backButton}
								onPress={() => this.props.navigation.goBack()}>
								<Icon
									iconType={"material"}
									iconSize={width / 18}
									iconName={"arrow-back"}
									iconColor={"#000"}
								/>
							</TouchableOpacity>
						</View>
						<View style={styles.container}>
							<ScrollView
								bounces={false}
								style={styles.container}
								ref={ref => (this.scrollRef = ref)}
								keyboardShouldPersistTaps={"always"}
								scrollEventThrottle={10}
								onScrollEndDrag={() => {
									if (
										this.props.nextCommentOffSet &&
										this.props.nextCommentOffSet.comment !== null
									) {
										this.props.dispatch(
											communityGetSinglePostComment({
												appToken: this.props.appToken,
												nextOffSet: this.props.nextCommentOffSet,
												postId: this.props.postDetail.id
											})
										);
									}
								}}
								showsVerticalScrollIndicator={false}>
								{postDetail !== null ? (
									<View style={styles.commentsMainView}>
										<PostCard
											postDetail={postDetail}
											isClubPost={false}
											isPostDetail={true}
											navigation={this.props.navigation}
										/>
										<CommentsList
											comments={this.props.comments}
											postDetail={postDetail}
											dispatch={this.props.dispatch}
											appToken={this.props.appToken}
											navigation={this.props.navigation}
											route={this.props.route}
										/>
									</View>
								) : null}
								{/* Options Modal */}
								{this.state.openModal ? (
									<PostOptions
										postDetail={postDetail}
										openModal={this.state.openModal}
										setModal={val => this.setState({ openModal: val })}
										setLoading={val => this.setState({ isLoading: val })}
										appToken={this.props.appToken}
										dispatch={this.props.dispatch}
										navigation={this.props.navigation}
										measurements={this.state.measurements}
									/>
								) : null}
							</ScrollView>
							<View style={styles.commentView}>
								<PostDetailCommentInput
									postDetail={postDetail}
									navigation={this.props.navigation}
									route={this.props.route}
									dispatch={this.props.dispatch}
									myProfile={this.props.myProfile}
									appToken={this.props.appToken}
									createCommentResponse={this.props.createCommentResponse}
								/>
							</View>
							{/* Loading Indicator */}
							{this.state.isLoading || this.props.postDetail === null ? (
								<View style={styles.progressScreen}>
									<CommunityLoader />
								</View>
							) : null}
						</View>
					</KeyboardAvoidingView>
				)}
			</HeaderHeightContext.Consumer>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		error: state.community && state.community.error,
		username:
			state &&
			state.appToken &&
			state.appToken.user &&
			state.appToken.user.name,
		postDetail: state.community && state.community.postDetailReducer.postDetail,
		comments: state.community && state.community.postDetailReducer.comments,
		nextCommentOffSet:
			state.community && state.community.postDetailReducer.nextCommentOffSet,
		userActivityStatus:
			state.community && state.community.postDetailReducer.userActivityStatus,
		createCommentResponse:
			state.community && state.community.postReducer.createCommentResponse,
		myProfile: state.community && state.community.profile.myProfile
	};
}

export default connect(mapStateToProps)(PostDetail);
