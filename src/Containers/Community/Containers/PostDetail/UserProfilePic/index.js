import React from "react";
import { Image, View, Dimensions } from "react-native";
import { styles } from "./style";
import { Icon } from "../../../../../Components";

const { width } = Dimensions.get("window");
function UserProfilePic(props) {
	const { postDetail } = props;
	const renderUserImage = () => {
		try {
			let image = postDetail.user_info.image;
			if (
				image !== null &&
				image !== undefined &&
				image.trim() !== "" &&
				image.startsWith("http")
			) {
				return <Image source={{ uri: image }} style={styles.userImage} />;
			} else {
				throw new Error("User profile pic not found");
			}
		} catch (error) {
			return (
				<View style={styles.noUserImage}>
					<Icon
						iconType={"ionicon"}
						iconSize={width / 12}
						iconName={"ios-person"}
						iconColor={"rgba(0,0,0,0.5)"}
					/>
				</View>
			);
		}
	};
	return renderUserImage();
}
export default UserProfilePic;
