import { StyleSheet, Dimensions } from "react-native";

const { width } = Dimensions.get("window");

export const styles = StyleSheet.create({
	noUserImage: {
		width: width / 6,
		height: width / 6,
		borderRadius: width / 12,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#eeeff5"
	},
	userImage: {
		width: width / 9,
		height: width / 9,
		borderRadius: width / 12,
		justifyContent: "center",
		alignItems: "center"
	}
});
