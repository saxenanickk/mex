import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	commentField: {
		width: "100%",
		backgroundColor: "#fff",
		borderTopWidth: width / 50,
		borderTopColor: "#eff1f4"
	},
	editImage: {
		width: width / 20,
		height: width / 20
	},
	rowView: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		minHeight: 45
	},
	commentTextInput: {
		flex: 1,
		alignItems: "center",
		marginLeft: width / 39,
		...Platform.select({
			ios: {
				maxHeight: 60
			}
		})
	},
	addCommentView: {
		paddingHorizontal: width / 30,
		paddingVertical: height / 90,
		transform: [{ rotate: "315deg" }]
	}
});
