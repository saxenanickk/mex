import React from "react";
import {
	View,
	Image,
	TextInput,
	TouchableOpacity,
	Keyboard
} from "react-native";
import { styles } from "./style";
import I18n from "../../../Assets/Strings/i18n";
import {
	communityCreateCommentResponse,
	communityChangePostCommentCountLocal,
	communityChangeClubPostCommentCountLocal,
	communityCreatePost
} from "../../Activity/Saga";
import { SENDARROW } from "../../../Assets/Img/Image";
import { communityChangeMyPostCommentLocal } from "../../EditProfile/Saga";
import { communityAddNewComment } from "../Saga";

class PostDetailCommentInput extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			newCommentText: null,
			commentText: "",
			isNewCommentAdded: false
		};

		const { route } = props;
		const {
			isClubPost = false,
			isMyPost = false,
			autoFocus = false
		} = route.params ? route.params : {};

		this.isClubPost = isClubPost;
		this.isMyPost = isMyPost;
		this.autoFocus = autoFocus;

		this.saveComment = this.saveComment.bind(this);
	}

	shouldComponentUpdate(props, state) {
		const { createCommentResponse, dispatch } = this.props;
		if (
			props.createCommentResponse === true &&
			props.createCommentResponse !== createCommentResponse
		) {
			this.setState({ isNewCommentAdded: false, newCommentText: "" });
			dispatch(communityCreateCommentResponse(null));
		}
		return true;
	}

	saveComment = () => {
		const { postDetail, dispatch, myProfile, appToken } = this.props;
		const { commentText } = this.state;

		let count = postDetail.comment_count;
		Keyboard.dismiss();
		if (commentText !== undefined && commentText.trim() !== "") {
			dispatch(
				communityAddNewComment({
					post_type: "comment",
					description: commentText,
					user_info: {
						name: myProfile.name
					}
				})
			);
			dispatch(
				communityCreatePost({
					postType: "comment",
					postDescription: commentText,
					appToken: appToken,
					parentId: postDetail.id,
					uniqueIdentifier: postDetail.uniqueIdentifier
				})
			);

			if (this.isClubPost) {
				dispatch(
					communityChangeClubPostCommentCountLocal({
						postId: postDetail.id,
						commentCount: count + 1
					})
				);
			}
			if (this.isMyPost) {
				dispatch(
					communityChangeMyPostCommentLocal({
						postId: postDetail.id,
						commentCount: postDetail.comment_count
					})
				);
			}
			dispatch(
				communityChangePostCommentCountLocal({
					postId: postDetail.id,
					commentCount: count + 1
				})
			);
			this.setState({ commentText: "" });
		}
	};

	render() {
		return (
			<View style={styles.commentField}>
				<View style={styles.rowView}>
					<TextInput
						value={this.state.commentText}
						autoFocus={this.autoFocus}
						multiline={true}
						onChangeText={text => this.setState({ commentText: text })}
						underlineColorAndroid={"transparent"}
						placeholder={I18n.t("com_write_comment")}
						style={styles.commentTextInput}
						selectionColor={"#D9D8D8"}
						returnKeyType={"done"}
						blurOnSubmit={true}
						numberOfLines={3}
					/>
					<TouchableOpacity
						onPress={() => this.saveComment()}
						style={styles.addCommentView}>
						<Image
							source={SENDARROW}
							style={styles.editImage}
							resizeMode={"contain"}
						/>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}
export default PostDetailCommentInput;
