// @ts-check
import { remove } from "lodash";
import {
	COMMUNITY_SAVE_SINGLE_POST_DETAIL,
	COMMUNITY_SAVE_SINGLE_POST_COMMENT,
	COMMUNITY_SAVE_COMMENT_OFFSET,
	COMMUNITY_POST_USER_ACTIVITY_SUCCESS,
	COMMUNITY_ADD_NEW_COMMENT,
	COMMUNITY_LOCAL_DELETE_POST_COMMENT
} from "./Saga";

const initialState = {
	postDetail: null,
	nextCommentOffSet: null,
	comments: null,
	userActivityStatus: null
};

/**
 * @param {{ type: string; payload: any; }} action
 */
export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case COMMUNITY_SAVE_SINGLE_POST_COMMENT:
			let tempComment = null;
			if (action.payload !== null) {
				if (action.payload.isRefresh === true) {
					tempComment = action.payload.comments;
				} else {
					if (state.comments === null) {
						tempComment = action.payload;
					} else {
						tempComment = state.comments.slice().concat(action.payload);
					}
				}
			}
			return {
				...state,
				comments: tempComment
			};
		case COMMUNITY_SAVE_SINGLE_POST_DETAIL:
			return {
				...state,
				postDetail: action.payload
			};
		case COMMUNITY_SAVE_COMMENT_OFFSET:
			return {
				...state,
				nextCommentOffSet:
					action.payload.isRefresh === undefined ? action.payload : null
			};
		case COMMUNITY_POST_USER_ACTIVITY_SUCCESS:
			return {
				...state,
				userActivityStatus: action.payload
			};
		case COMMUNITY_ADD_NEW_COMMENT:
			let temp = [];
			temp[0] = action.payload;
			let tempPostDetail = state.postDetail;
			tempPostDetail.comment_count++;
			return {
				...state,
				comments: temp.concat(state.comments),
				postDetail: tempPostDetail
			};
		case COMMUNITY_LOCAL_DELETE_POST_COMMENT:
			temp = state.comments;
			tempPostDetail = state.postDetail;
			tempPostDetail.comment_count--;
			remove(temp, row => row.id === action.payload.postId);
			return {
				...state,
				comments: temp,
				postDetail: tempPostDetail
			};
		default:
			return state;
	}
};
