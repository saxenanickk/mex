import React from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { styles } from "./style";
import { Icon } from "../../../../../Components";
import { CommunityTextRegular } from "../../../Components/CommunityText";
const { width } = Dimensions.get("window");
import I18n from "../../../Assets/Strings/i18n";
import { communityLikeOrUnlikePost } from "../Saga";
import {
	communityChangeClubPostLikeLocal,
	communityChangePostLikeLocal
} from "../../Activity/Saga";
import { communityChangeMyPostLikeLocal } from "../../EditProfile/Saga";

class UserActivitySection extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isLiked: null
		};
	}
	shouldComponentUpdate(props, state) {
		if (
			props.postDetail !== null &&
			props.postDetail !== this.props.postDetail
		) {
			this.setState({ likeCount: props.postDetail.liked_count });
		}
		return true;
	}

	setIsLiked = isLiked => {
		this.setState({ isLiked });
	};
	setLikeCount = likeCount => {
		this.setState({ likeCount });
	};
	likeUnLike = () => {
		const { postDetail, dispatch, appToken } = this.props;
		const { isLiked, likeCount } = this.state;
		let isLike = null;
		let tempLikeCount = 0;

		const { route } = this.props;
		const { isClubPost = false, isMyPost = false } = route.params
			? route.params
			: {};

		if (isLiked === null) {
			tempLikeCount = postDetail.hasLiked
				? postDetail.liked_count - 1
				: postDetail.liked_count + 1;
			this.setIsLiked(!postDetail.hasLiked);
			this.setLikeCount(tempLikeCount);
			isLike = !postDetail.hasLiked;
		} else {
			tempLikeCount = isLiked ? likeCount - 1 : likeCount + 1;
			this.setIsLiked(!isLiked);
			this.setLikeCount(tempLikeCount);
			isLike = !isLiked;
		}
		dispatch(
			communityLikeOrUnlikePost({
				appToken: appToken,
				postId: postDetail.id,
				uniqueIdentifier: postDetail.uniqueIdentifier,
				isLike
			})
		);

		if (isClubPost) {
			dispatch(
				communityChangeClubPostLikeLocal({
					postId: postDetail.id,
					likeCount: tempLikeCount,
					isLike
				})
			);
		}
		if (isMyPost) {
			dispatch(
				communityChangeMyPostLikeLocal({
					postId: postDetail.id,
					likeCount: tempLikeCount,
					isLike
				})
			);
		}
		dispatch(
			communityChangePostLikeLocal({
				postId: postDetail.id,
				likeCount: tempLikeCount,
				isLike
			})
		);
	};
	navigateToLikedMember = () => {
		const { postDetail } = this.props;
		const { isLiked, likeCount } = this.state;

		this.props.navigation.navigate("LikedMember", {
			postId: postDetail.id,
			userId: postDetail.user_id,
			likeCount: isLiked === null ? postDetail.liked_count : likeCount
		});
	};

	render() {
		const { postDetail } = this.props;
		const { isLiked, likeCount } = this.state;
		return (
			<View style={styles.likeSection}>
				<TouchableOpacity
					onPress={() => this.likeUnLike()}
					style={styles.heartIcon}>
					<Icon
						iconType={"font_awesome"}
						iconSize={width / 18}
						iconName={"heart"}
						iconColor={
							isLiked === null
								? postDetail.hasLiked
									? "#d75654"
									: "#ABABAB"
								: isLiked
								? "#d75654"
								: "#787878"
						}
					/>
				</TouchableOpacity>
				<TouchableOpacity
					style={styles.likeCommentCount}
					activeOpacity={1}
					onPress={() =>
						isLiked === null
							? postDetail.liked_count !== 0 && this.navigateToLikedMember()
							: likeCount !== 0 && this.navigateToLikedMember()
					}>
					<CommunityTextRegular style={styles.likeText}>
						{I18n.t("com_like", {
							count: isLiked === null ? postDetail.liked_count : likeCount
						})}
					</CommunityTextRegular>
					<CommunityTextRegular
						style={[styles.commentMarginLeft, styles.likeText]}>
						{I18n.t("com_comment", {
							count: postDetail.comment_count
						})}
					</CommunityTextRegular>
				</TouchableOpacity>
			</View>
		);
	}
}

export default UserActivitySection;
