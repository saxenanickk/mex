import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	likeSection: {
		width: 0.84 * width,
		height: height / 14,
		alignItems: "center",
		flexDirection: "row",
		justifyContent: "space-between",
		alignSelf: "center"
	},
	heartImage: {
		width: width / 18,
		height: width / 18
	},
	likeCommentCount: {
		justifyContent: "space-between",
		flexDirection: "row"
	},
	commentMarginLeft: {
		marginLeft: width / 30
	},
	heartIcon: {
		width: width / 10,
		borderWidth: 0,
		paddingVertical: height / 90
	},
	likeText: {
		color: "#a8a8a8",
		fontSize: height / 50
	}
});
