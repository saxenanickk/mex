import React from "react";
import {
	View,
	Dimensions,
	Image,
	TouchableOpacity,
	Platform
} from "react-native";
const { width, height } = Dimensions.get("window");
import {
	GoappTextLight,
	GoappTextInputRegular,
	GoappTextRegular,
	GoappTextBold
} from "../../Components/GoappText";

class ContactUsEmailVerify extends React.Component {
	constructor(props) {
		super(props);
		this.props.setAnalytics("corporate_trouble_verify", {});
	}
	render() {
		return (
			<View
				style={{
					margin: height / 81.2,
					alignItems: "center"
				}}>
				<View
					style={{
						...Platform.select({
							android: { elevation: 5 },
							ios: {
								shadowOffset: { width: 1, height: 1 },
								shadowColor: "grey",
								shadowOpacity: 0.2
							}
						}),
						height: height / 7,
						width: width / 1.1,
						backgroundColor: "#fff",
						borderRadius: height / 90,
						padding: height / 65
					}}>
					<GoappTextBold>{"Send an email to info@mexit.in"}</GoappTextBold>
					<GoappTextRegular
						color={"#5d6271"}
						style={{
							lineHeight: height / 38,
							marginTop: height / 120
						}}>
						{
							'Please send an email from your corporate id with subject "Corporate Authentication" to info@mexit.in to get your email verified.'
						}
					</GoappTextRegular>
				</View>
				<View
					style={{
						height: height / 13.5,
						justifyContent: "center"
					}}>
					<GoappTextBold color={"#6f6f6f"}>{"or"}</GoappTextBold>
				</View>
				<TouchableOpacity
					onPress={() => this.props.navigation.navigate("ContactUs")}
					style={{
						height: height / 17,
						backgroundColor: "#fff",
						elevation: height / 162.4,
						width: width / 1.1,
						alignItems: "center",
						justifyContent: "center",
						borderRadius: height / 90
					}}>
					<GoappTextBold color="#359df2">{"Contact US"}</GoappTextBold>
				</TouchableOpacity>
			</View>
		);
	}
}

export default ContactUsEmailVerify;
