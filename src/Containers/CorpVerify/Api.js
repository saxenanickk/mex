import Config from "react-native-config";

const { MEX_LOGIN_BASE_URL } = Config;
export const CORPORATE_EMAIL_VALIDATION = "CORPORATE_EMAIL_VALIDATION";
/**
 * @typedef {Object} sendOtpParam
 * @property {string} appToken
 * @property {string} userId
 * @property {string} corpEmail
 */

/**
 * @typedef {Object} verifyOtpParam
 * @property {string} verificationCode
 */

class CorpVerifyApi {
	/**
	 * function to send otp to user
	 * @param {sendOtpParam} params
	 */
	sendOtpToTheUser(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${MEX_LOGIN_BASE_URL}sendVerificationCode`, {
					method: "POST",
					headers: {
						"x-access-token": params.appToken,
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						userId: params.userId,
						otpType: CORPORATE_EMAIL_VALIDATION,
						corpEmail: params.corpEmail
					})
				})
					.then(response => {
						response
							.json()
							.then(res => resolve(res))
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * function to verify the otp
	 * @param {sendOtpParam & verifyOtpParam} params
	 */
	verifyOtp(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${MEX_LOGIN_BASE_URL}verifyVerificationCode`, {
					method: "POST",
					headers: {
						"x-access-token": params.appToken,
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						userId: params.userId,
						corpEmail: params.corpEmail,
						verificationCode: params.verificationCode,
						otpType: CORPORATE_EMAIL_VALIDATION
					})
				})
					.then(response => {
						response
							.json()
							.then(res => resolve(res))
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				reject(error);
			}
		});
	}

	getMyProfile(params) {
		const X_ACCESS_TOKEN = params.token;
		return new Promise((resolve, reject) => {
			try {
				fetch(`${MEX_LOGIN_BASE_URL}getProfile`, {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": X_ACCESS_TOKEN
					}
				})
					.then(response =>
						response
							.json()
							.then(res => resolve(res))
							.catch(error => reject(error))
					)
					.catch(error => reject(error));
			} catch (error) {
				reject(error);
			}
		});
	}
}

export default new CorpVerifyApi();
