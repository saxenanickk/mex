import React from "react";
import { View, Dimensions, Image } from "react-native";
import { VERIFIED_CORPORATE } from "../../Assets/Img/Image";
import Icon from "../../Components/Icon";
const { width, height } = Dimensions.get("window");

import { GoappTextLight, GoappTextBold } from "../../Components/GoappText";

class CorpVerified extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<View style={{ padding: height / 26 }}>
				<View
					style={{
						height: height / 8,
						justifyContent: "center"
					}}>
					<GoappTextLight style={{ color: "#8c8f9a" }}>
						{"Company"}
					</GoappTextLight>
					<GoappTextLight
						style={{
							marginTop: height / 203
						}}>{`${this.props.company}`}</GoappTextLight>
				</View>

				<View
					style={{
						justifyContent: "center",
						alignItems: "center",
						height: height / 3.6
					}}>
					<Image source={VERIFIED_CORPORATE} />
					<GoappTextBold
						style={{
							marginTop: height / 54.1,
							fontSize: height / 50
						}}>
						{"Corporate Verified!"}
					</GoappTextBold>
				</View>
			</View>
		);
	}
}

export default CorpVerified;
