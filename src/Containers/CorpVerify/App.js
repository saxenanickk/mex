import React, { Component, Fragment } from "react";
import { StatusBar, StyleSheet, BackHandler } from "react-native";
import GoAppAnalytics from "../../utils/GoAppAnalytics";
import { GoappAlertContext } from "../../GoappAlertContext";
import EmailCorpVerify from "./EmailCorpVerify";
import CorpVerified from "./CorpVerified";
import ContactUsEmailVerify from "./ContactUsEmailVerify";
import { CommonActions } from "@react-navigation/native";
import { connect } from "react-redux";

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isVerified: false,
			alternateToVerifyMail: false
		};
		this.onClickAlternateToVerifyMail = this.onClickAlternateToVerifyMail.bind(
			this
		);
		this.onPressBack = this.onPressBack.bind(this);
		this.onVerificationComplete = this.onVerificationComplete.bind(this);
		this.setAnalytics = this.setAnalytics.bind(this);
		BackHandler.addEventListener("hardwareBackPress", this.onPressBack);
	}

	onPressBack = () => {
		try {
			if (this.state.alternateToVerifyMail === true) {
				this.setState({ alternateToVerifyMail: false });
				return true;
			} else {
				const parent = this.props.navigation.dangerouslyGetParent();
				let route = "Goapp";
				if (parent && parent.state && parent.state.routes) {
					let length = parent.state.routes.length;
					if (length >= 2) {
						route = parent.state.routes[length - 2].routeName;
					}
				}
				this.props.navigation.navigate(route);
				return true;
			}
		} catch (error) {
			this.props.navigation.navigate("Goapp");
			return true;
		}
	};

	componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.onPressBack);
	}

	onClickAlternateToVerifyMail() {
		this.setState({ alternateToVerifyMail: true });
	}

	checkIsCorporate() {
		try {
			if (this.props.userProfile && this.props.userProfile.tenantdto) {
				const tenantDto = JSON.parse(this.props.userProfile.tenantdto);
				let isCorpVerified =
					tenantDto.tenant_id !== null && tenantDto.tenant_id !== undefined;
				if (isCorpVerified) {
					GoAppAnalytics.setSuperProperties({
						tenant_id: tenantDto.tenant_id,
						tenant_name: tenantDto.name
					});
					GoAppAnalytics.setPeopleProperties({
						tenant_id: tenantDto.tenant_id,
						tenant_name: tenantDto.name
					});
				}
				return isCorpVerified;
			} else {
				return false;
			}
		} catch (error) {
			return false;
		}
	}

	onVerificationComplete() {
		const navigateTo = this.props.route.params.navigateTo || null;
		this.setState({ isVerified: true });
		if (navigateTo !== null) {
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "Goapp" }, { routeName: navigateTo }]
				})
			);
		}
	}

	setAnalytics = (event, property) => {
		GoAppAnalytics.trackWithProperties(event, property);
	};

	getCompanyName() {
		try {
			if (this.props.userProfile && this.props.userProfile.tenantdto) {
				const tenantDto = JSON.parse(this.props.userProfile.tenantdto);
				if (tenantDto.name !== null && tenantDto.name !== undefined) {
					return tenantDto.name;
				}
			}
		} catch (error) {
			return "";
		}
		return "";
	}

	render() {
		return (
			<Fragment>
				<StatusBar backgroundColor={"#fff"} barStyle={"dark-content"} />
				{this.checkIsCorporate() ? (
					<CorpVerified company={this.getCompanyName()} {...this.props} />
				) : this.state.alternateToVerifyMail ? (
					<ContactUsEmailVerify
						{...this.props}
						setAnalytics={this.setAnalytics}
					/>
				) : (
					<EmailCorpVerify
						onClickAlternate={this.onClickAlternateToVerifyMail}
						{...this.props}
						alert={this.context.current}
						onVerified={this.onVerificationComplete}
						setAnalytics={this.setAnalytics}
					/>
				)}
			</Fragment>
		);
	}
}

App.contextType = GoappAlertContext;

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		user: state.appToken.user,
		userProfile: state.appToken.userProfile
	};
}
export default connect(mapStateToProps)(App);
