import React from "react";
import { View, Dimensions, TouchableOpacity, Keyboard } from "react-native";
import { Icon, OtpValidation, ProgressScreen } from "../../Components";
import {
	GoappTextLight,
	GoappTextInputRegular,
	GoappTextRegular
} from "../../Components/GoappText";
const { width, height } = Dimensions.get("window");
import CorpVerifyApi from "./Api";
import { validateEmail } from "../../utils/validation";
import { styles } from "./style";
import {
	mexSaveUserProfile,
	saveAppToken
} from "../../CustomModules/ApplicationToken/Saga";
import AsyncStorage from "@react-native-community/async-storage";
import MiniApp from "../../CustomModules/MiniApp";
import { ERRORCLOUD } from "../../Assets/Img/Image";

class EmailCorpVerify extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			emailInputText: "",
			emailValidation: false,
			apologiesMsg: false,
			isOtpVerify: false,
			isVerified: false,
			isLoading: false,
			message: ""
		};
		this.sendOtp = this.sendOtp.bind(this);
		this.verifyOtp = this.verifyOtp.bind(this);
		this.sendCount = 0;
	}
	validateEmailFun = text => {
		let validateEmailVerify = validateEmail(text);
		if (validateEmailVerify) {
			this.setState({ emailInputText: text, emailValidation: true });
		} else {
			this.setState({ emailInputText: text, emailValidation: false });
		}
	};

	sendOtp = async () => {
		const { setAnalytics } = this.props;
		try {
			Keyboard.dismiss();
			this.sendCount = this.sendCount + 1;
			const { emailInputText } = this.state;
			setAnalytics("corporate_send_otp", {
				status: this.sendCount === 0 ? "initiate" : "resend"
			});
			this.setState({
				isLoading: true,
				message: "Sending OTP",
				isOtpVerify: false
			});
			const resp = await CorpVerifyApi.sendOtpToTheUser({
				appToken: this.props.appToken,
				corpEmail: emailInputText,
				userId: this.props.userProfile.userId
			});
			if (resp.success === 1) {
				setAnalytics("corporate_send_otp", { status: "success" });
				this.setState({ isOtpVerify: true });
			} else {
				throw new Error("Unable to send Otp");
			}
		} catch (error) {
			setAnalytics("corporate_send_otp", { status: "fail" });
			this.props.alert.openDialog({
				type: "Action",
				title: "Error",
				message:
					"Sorry, we are unable to send OTP, Check your corporate email.",
				rightPress: () => this.setState({ isOtpVerify: false }),
				icon: ERRORCLOUD
			});
		} finally {
			this.setState({ isLoading: false });
		}
	};

	getMiniUserInfo = userProfile => {
		return new Promise(function(resolve, reject) {
			try {
				let obj = {};
				obj.contact = userProfile.phoneNum;
				obj.email = userProfile.email;
				obj.name = userProfile.name;
				obj.site_id = Object.keys(userProfile.zonesMap)[0];
				obj.site_name = Object.values(userProfile.zonesMap)[0];
				obj.tenant_id = userProfile.tenantId;
				obj.tenant_name = null;
				resolve(obj);
			} catch (error) {
				resolve({
					contact: "",
					email: "",
					name: "",
					site_id: "",
					site_name: "",
					tenant_id: "",
					tenant_name: ""
				});
			}
		});
	};

	verifyOtp = async otp => {
		const { setAnalytics } = this.props;
		try {
			this.setState({
				isLoading: true,
				isOtpVerify: false,
				message: "Verifying OTP"
			});
			setAnalytics("corporate_verify_otp", { status: "initiate" });
			const resp = await CorpVerifyApi.verifyOtp({
				appToken: this.props.appToken,
				corpEmail: this.state.emailInputText,
				userId: this.props.userProfile.userId,
				verificationCode: otp
			});
			console.log("response of otp verify is", resp);
			if (resp.success === 1) {
				setAnalytics("corporate_verify_otp", { status: "success" });
				this.props.dispatch(saveAppToken({ token: resp.token, user: null }));
				AsyncStorage.setItem(
					"mex_app_token",
					JSON.stringify({ token: resp.token, user: null })
				);
				MiniApp.setSessionToken(resp.token);
				const profile = await CorpVerifyApi.getMyProfile({
					token: resp.token
				});
				if (profile.success === 1) {
					const user = await this.getMiniUserInfo(profile.data);
					this.props.dispatch(
						mexSaveUserProfile({ userProfile: profile.data, user })
					);
					this.props.onVerified();
				}
			} else {
				if (resp.message === "Invalid verification code") {
					this.props.alert.openDialog({
						type: "Confirmation",
						title: "Error!",
						message: "Incorrect OTP. Please try again.",
						leftPress: () => this.setState({ isOtpVerify: false }),
						rightTitle: "Retry",
						rightPress: () => this.setState({ isOtpVerify: true }),
						icon: ERRORCLOUD
					});
				} else {
					throw new Error("Unable to verify Otp");
				}
			}
		} catch (error) {
			console.log("error ", error);
			setAnalytics("corporate_verify_otp", { status: "fail" });
			this.setState({ apologiesMsg: true });
			this.props.alert.openDialog({
				type: "Action",
				title: "Error",
				message: "Sorry, we are unable to verify OTP.",
				rightTitle: "Cancel",
				rightPress: () => this.setState({ isOtpVerify: false }),
				icon: ERRORCLOUD
			});
		} finally {
			this.setState({ isLoading: false });
		}
	};
	render() {
		return (
			<View style={{ flex: 1 }}>
				<View style={styles.blueBox}>
					<View style={styles.flexRowDirection}>
						<Icon
							iconType={"ionicon"}
							iconName={"md-star"}
							iconSize={width / 18}
							iconColor="#ffffff"
						/>
						<GoappTextLight style={styles.verifyHeaderText}>
							{"Verify your corporate email id!"}
						</GoappTextLight>
					</View>
					<GoappTextLight style={styles.verifyDetailsText}>
						{
							"We have kept certain features exclusive for Verified Corporate Users working in RMZ campuses."
						}
					</GoappTextLight>
				</View>
				<View style={styles.inputEmailBox}>
					<GoappTextRegular style={styles.emailHeader}>
						{"Corporate Email"}
					</GoappTextRegular>
					<View style={styles.inputEmailView}>
						<GoappTextInputRegular
							color={"rgba(0,0,0,1)"}
							style={styles.emailInput}
							placeholder="Enter Email"
							autoFocus={true}
							keyboardType={"email-address"}
							value={this.state.emailInputText}
							onChangeText={text => this.validateEmailFun(text.trim())}
						/>
						{this.state.emailValidation ? (
							<TouchableOpacity
								onPress={this.sendOtp}
								style={styles.verifyButton}>
								<GoappTextRegular color={"#fff"} size={height / 60}>
									Verify
								</GoappTextRegular>
							</TouchableOpacity>
						) : null}
					</View>
				</View>
				{this.state.apologiesMsg ? (
					<View style={styles.apologiesView}>
						<GoappTextRegular style={styles.apologiesText}>
							{
								"Apologies! We are unable to verify your email ID. Kindly try these alternate ways to"
							}
						</GoappTextRegular>
						<TouchableOpacity onPress={this.props.onClickAlternate}>
							<GoappTextRegular color={"#45a4f2"} style={styles.apologiesText}>
								{"get your email verified!"}
							</GoappTextRegular>
						</TouchableOpacity>
					</View>
				) : null}
				{this.state.isOtpVerify ? (
					<OtpValidation
						open={this.state.isOtpVerify}
						handleClose={() =>
							this.setState({ isOtpVerify: false, isVerified: false })
						}
						heading={"Check your inbox!"}
						message={`We've sent an email to \n${
							this.state.emailInputText
						}.\nPlease enter the code below`}
						verificationMessage={"Corporate verification successful!"}
						isVerified={this.state.isVerified}
						onPressConfirm={this.verifyOtp}
						onPressResend={() => this.sendOtp()}
					/>
				) : null}
				{this.state.isLoading ? (
					<ProgressScreen
						indicatorColor={"#2c98f0"}
						indicatorSize={height / 45}
						isMessage={true}
						primaryMessage={"Hang On.."}
						message={this.state.message}
					/>
				) : null}
			</View>
		);
	}
}
export default EmailCorpVerify;
