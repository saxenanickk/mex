import React from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";

export default class CorpVerify extends React.Component {
	constructor(props) {
		super(props);
		mpAppLaunch({ name: "corp_verify" });
	}

	render() {
		return <App {...this.props} />;
	}
}

AppRegistry.registerComponent("CorpVerify", () => CorpVerify);
