import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
export const styles = StyleSheet.create({
	blueBox: {
		height: width / 3,
		width: width,
		backgroundColor: "#2c98f0",
		flexDirection: "column",
		justifyContent: "center",
		padding: width / 18
	},
	flexRowDirection: {
		flexDirection: "row"
	},
	verifyHeaderText: {
		paddingLeft: width / 37,
		fontSize: 15,
		color: "#ffffff"
	},
	verifyDetailsText: {
		fontSize: 14,
		color: "#ffffff",
		marginTop: width / 62
	},
	verifyButton: {
		backgroundColor: "#2c98f0",
		height: height / 25,
		width: width / 6,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 15
	},
	emailInput: {
		height: height / 20,
		width: width / 1.5,
		fontSize: height / 55,
		borderRadius: width / 15
	},
	emailHeader: {
		marginLeft: width / 25,
		color: "#8c8f9a"
	},
	inputEmailBox: {
		height: width / 3,
		justifyContent: "center",
		marginHorizontal: width / 18
	},
	apologiesView: {
		justifyContent: "center",
		padding: width / 10,
		alignItems: "center"
	},
	apologiesText: {
		textAlign: "center",
		fontSize: width / 24
	},
	inputEmailView: {
		marginTop: width / 80,
		height: height / 20,
		width: width / 1.1,
		fontSize: height / 55,
		backgroundColor: "#EFF1F4",
		alignItems: "center",
		justifyContent: "space-between",
		flexDirection: "row",
		borderRadius: width / 15,
		paddingLeft: width / 30,
		paddingRight: width / 50
	}
});
