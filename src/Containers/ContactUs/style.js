import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff"
	},
	header: {
		flexDirection: "row",
		height: height / 12,
		width: width,
		backgroundColor: "#fff",
		alignItems: "center",
		paddingHorizontal: width / 27,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	headerTopic: {
		marginLeft: width / 25,
		fontSize: height / 45
	},
	secondContainer: {
		flex: 1,
		marginTop: height / 40,
		backgroundColor: "#fff"
	},
	chatSection: {
		flexDirection: "row"
	},
	chatSectionText: {
		marginHorizontal: width / 25,
		fontSize: 16,
		alignSelf: "center",
		color: "#2C98F0"
	},
	callSection: {
		flexDirection: "row",
		height: height / 12,
		alignItems: "center"
	},
	callText: {
		marginHorizontal: width / 25,
		fontSize: height / 50,
		alignSelf: "center",
		color: "#2C98F0"
	},
	emailSection: {
		marginTop: height / 20,
		flexDirection: "row"
	},
	emailText: {
		marginHorizontal: width / 25,
		fontSize: 16,
		alignSelf: "center",
		color: "#2C98F0"
	}
});
