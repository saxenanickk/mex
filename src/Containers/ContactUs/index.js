import React from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";

export default class ContactUs extends React.Component {
	constructor(props) {
		super(props);
		mpAppLaunch({ name: "contact_us" });
	}

	componentWillUnmount() {}

	render() {
		return <App {...this.props} />;
	}
}

AppRegistry.registerComponent("ContactUs", () => ContactUs);
