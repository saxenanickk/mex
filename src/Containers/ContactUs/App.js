import React, { Component, Fragment } from "react";
import { StatusBar, View, Dimensions, Linking } from "react-native";
import Freshchat from "../../CustomModules/Freshchat";
import ProfileButton from "../../Components/ProfileButton";
import styles from "./style";

const { height, width } = Dimensions.get("window");

class App extends Component {
	render() {
		return (
			<Fragment>
				<StatusBar backgroundColor={"#fff"} barStyle={"dark-content"} />
				<View style={styles.container}>
					<View style={styles.secondContainer}>
						<View style={{ paddingHorizontal: width / 35 }}>
							<ProfileButton
								iconName={"chat_bubbles"}
								iconSize={height / 35}
								menuText={"Chat with Us"}
								onPress={() => Freshchat.launchSupportChat()}
							/>
						</View>
						<View style={{ paddingHorizontal: width / 35 }}>
							<ProfileButton
								iconName={"line_call"}
								iconSize={height / 40}
								menuText={"Give us a Call"}
								onPress={() => Linking.openURL(`tel:${"08047109999"}`)}
							/>
						</View>
						<View style={{ paddingHorizontal: width / 35 }}>
							<ProfileButton
								iconName={"sms_icon"}
								iconSize={height / 40}
								menuText={"Email us"}
								onPress={() => Linking.openURL("mailto:support@mexit.in")}
							/>
						</View>
					</View>
				</View>
			</Fragment>
		);
	}
}

export default App;
