import React, { Fragment } from "react";
import { StatusBar } from "react-native";
import RegisterScreen from "./RegisterScreen";
import GoAppAnalytics from "../../utils/GoAppAnalytics";
import { DrawerOpeningHack } from "../../Components";
import { connect } from "react-redux";
import { SafeAreaView } from "react-native-safe-area-context";

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			prevState: null,
			currState: null
		};
	}

	componentDidMount() {
		setTimeout(() => {
			StatusBar.setBackgroundColor("#fff");
			StatusBar.setBarStyle("dark-content");
		}, 200);
	}

	render() {
		return (
			<Fragment>
				<SafeAreaView style={{ flex: 1, backgroundColor: "#ffffff" }}>
					<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
					<RegisterScreen
						onNavigationStateChange={(prevState, currState) =>
							GoAppAnalytics.logChangeOfScreenInStack(
								"goapp",
								prevState,
								currState
							)
						}
					/>
					<DrawerOpeningHack />
				</SafeAreaView>
			</Fragment>
		);
	}
}

export default connect()(App);
