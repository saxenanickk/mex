import React from "react";

const TabNavigationContext = React.createContext();

export default TabNavigationContext;
