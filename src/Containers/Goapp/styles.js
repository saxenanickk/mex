import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: { flex: 1, backgroundColor: "#ffffff" },
	tabBar: {
		zIndex: 10,
		height: height / 12,
		position: "absolute",
		flexDirection: "row",
		justifyContent: "space-around",
		bottom: 0,
		width: width,
		alignSelf: "center",
		alignItems: "flex-end",
		backgroundColor: "#fff",
		...Platform.select({
			android: { elevation: 3 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.2
			}
		})
	},
	tab: {
		height: height / 12,
		width: width / 5,
		backgroundColor: "#ffffff",
		alignItems: "center",
		justifyContent: "center"
	},
	tabText: {
		fontSize: height / 75,
		color: "#8F96A3"
	},
	tabTextEnabled: {
		fontSize: height / 75,
		color: "#2C98F0"
	}
});

export default styles;
