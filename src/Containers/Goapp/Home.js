import React, { Component } from "react";
import {
	Platform,
	LayoutAnimation,
	UIManager,
	View,
	FlatList
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import firebase from "react-native-firebase";
import I18n from "../../Assets/strings/i18n";
import { connect } from "react-redux";
import MiniApp from "../../CustomModules/MiniApp";
import { AppList } from "./AppList";

import { GoToast } from "../../Components";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";
import Feed, { Tutorial } from "../../Components/Feed";
import { DeviceId } from "../../CustomModules/GoDeviceInfo";
import { mexSaveFcmToken } from "./Saga";
import noInternetDialog from "../../Components/NoInternetDialog";
import DialogContext from "../../DialogContext";

class Home extends Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "HOME" });
		this.state = {
			selectedTab: 3,
			snackBarVisibility: false,
			showPopup: false,
			params: null,
			isRefresh: null,
			showTutorial: false,
			images: []
		};
		if (Platform.OS === "android") {
			UIManager.setLayoutAnimationEnabledExperimental(true);
		}
		this.isProfileAvailableFlag = null;
		this.backButton = false;
		this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
		this.handleApplicationClick = this.handleApplicationClick.bind(this);
	}

	componentDidMount() {
		const channel = new firebase.notifications.Android.Channel(
			"mex",
			"General",
			firebase.notifications.Android.Importance.Max
		).setDescription("MEX notification channel.");
		const channel_community = new firebase.notifications.Android.Channel(
			"community",
			"Community",
			firebase.notifications.Android.Importance.Max
		).setDescription("MEX community notifications.");
		firebase.notifications().android.createChannel(channel);
		firebase.notifications().android.createChannel(channel_community);

		this.checkPermission();
		this.notificationListeners();
		this.getInitialNotification();
	}

	getInitialNotification = async () => {
		const notificationOpen = await firebase
			.notifications()
			.getInitialNotification();

		if (notificationOpen) {
			// App was opened by a notification
			// Get the action triggered by the notification being opened
			const action = notificationOpen.action;
			console.log("getInitialNotification action", action);
			// Get information about the notification that was opened
			const notification = notificationOpen.notification;
			const data = notification.data;
			this.handleNotificationClick(data);
		}
	};

	handleNotificationClick = data => {
		console.log("handleNotificationClick", data);
		if (data) {
			const appName = data.appname;
			if (appName) {
				this.handleNotificationNavigation(appName, data);
			}
		}
		//Else No Handling is needed
	};

	notificationListeners = async () => {
		this.notificationDisplayedListener = firebase
			.notifications()
			.onNotificationDisplayed(notification => {
				// Process your notification as required
				// ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
			});

		this.notificationOpenedListener = firebase
			.notifications()
			.onNotificationOpened(notificationOpen => {
				console.log("onNotificationOpened", notificationOpen);
				const { title, body, data } = notificationOpen.notification;
				this.handleNotificationClick(data);
			});

		this.notificationListener = firebase
			.notifications()
			.onNotification(notification => {
				if (Platform.OS === "android") {
					const localNotification = new firebase.notifications.Notification({
						sound: "default",
						show_in_foreground: true
					})
						.setNotificationId(notification.notificationId)
						.setTitle(notification.title)
						.setSubtitle(notification.subtitle)
						.setBody(notification.body)
						.setData(notification.data)
						.android.setChannelId(
							notification.android && notification.android.channelId
								? notification.android.channelId
								: "mex"
						)
						.android.setAutoCancel(true)
						.android.setColor("#1D4486") // you can set a color here
						.android.setSmallIcon("@drawable/ic_stat_name")
						.android.setBigText(notification.body)
						.android.setPriority(firebase.notifications.Android.Priority.High);

					firebase
						.notifications()
						.displayNotification(localNotification)
						.catch(err => console.error(err));
				} else if (Platform.OS === "ios") {
					console.log("onNotification", notification);
					const localNotification = new firebase.notifications.Notification()
						.setNotificationId(notification.notificationId)
						.setTitle(notification.title)
						.setSubtitle(notification.subtitle)
						.setBody(notification.body)
						.setData(notification.data)
						.ios.setBadge(notification.ios.badge);

					firebase
						.notifications()
						.displayNotification(localNotification)
						.catch(err => console.error(err));
				}
			});
	};

	shouldComponentUpdate(props, state) {
		// if (
		// 	props.location.status &&
		// 	props.location.status !== this.props.location.status
		// ) {
		// 	LocationApi.reverseGeocoding({
		// 		latitude: props.location.latitude,
		// 		longitude: props.location.longitude
		// 	}).then(data => {
		// 		this.setState({
		// 			addressTitle: data.results[0].address_components[3].short_name,
		// 			addressValue:
		// 				data.results[0].address_components[0].short_name +
		// 				", " +
		// 				data.results[0].address_components[2].short_name
		// 		});
		// 	});
		// } else if (
		// 	props.location.status === false &&
		// 	props.location.status !== this.props.location.status
		// ) {
		// 	this.setState({
		// 		addressTitle: I18n.t("unknown"),
		// 		addressValue: I18n.t("unknown")
		// 	});
		// }
		LayoutAnimation.easeInEaseOut();
		return true;
	}

	componentWillUnmount() {
		this.notificationDisplayedListener();
		this.notificationListener();
	}

	getToken = async () => {
		try {
			let fcmToken = await AsyncStorage.getItem("fcmToken");
			if (!fcmToken) {
				fcmToken = await firebase.messaging().getToken();
				if (fcmToken) {
					await AsyncStorage.setItem("fcmToken", fcmToken);
				}
			}
			console.log("fcm token in", fcmToken);
			let deviceId = await DeviceId();
			let obj = {};
			if (Platform.OS === "ios") {
				obj = {
					deviceId,
					voipToken: fcmToken,
					os: "ios"
				};
			} else {
				obj = {
					deviceId,
					fcmToken: fcmToken,
					os: "android"
				};
			}
			this.props.dispatch(
				mexSaveFcmToken({ body: obj, appToken: this.props.appToken })
			);
		} catch (error) {
			console.log("fcm token error is", error);
		}
	};

	checkPermission = async () => {
		try {
			const enabled = await firebase.messaging().hasPermission();
			if (enabled) {
				this.getToken();
			} else {
				this.requestPermission();
			}
		} catch (error) {
			console.log("Asking for persmission", error);
		}
	};

	requestPermission = async () => {
		try {
			await firebase.messaging().requestPermission();
			this.getToken();
		} catch (error) {
			console.log("permission rejected");
		}
	};

	handleNotificationNavigation = (appName, appData) => {
		// Navigate to app by passing data
		this.isProfileAvailableFlag = setInterval(() => {
			if (this.props.userProfile) {
				clearInterval(this.isProfileAvailableFlag);
				this.handleApplicationClick(appName, JSON.stringify(appData));
			}
		}, 500);
	};

	/**
	 * Back Button Handler
	 */
	handleBackButtonClick() {
		if (!this.backButton) {
			this.backButton = true;
			setTimeout(() => {
				this.backButton = false;
			}, 2000);
			GoToast.show(I18n.t("press_again_to_go_back"), I18n.t("information"));
			return true;
		} else {
			MiniApp.clearMiniApp();
			this.backButton = false;
			return false;
		}
	}

	handleApplicationClick(app_name, link = "") {
		try {
			let item = AppList.filter(item => item.name === app_name);
			if (item.length > 0) {
				switch (item[0].type) {
					case "PWA":
						if (!this.props.isInternetAvailable) noInternetDialog(this.context);
						else {
							//uncomment this code when trying to launch web app
							if (app_name === "Azgo" || app_name === "Hungerbox") {
								MiniApp.launchPhoneVerifiedApp(
									app_name,
									this.props.userProfile,
									this.props.navigation
								);
							} else {
								MiniApp.launchWebApp(app_name, item[0].url);
							}
						}
						break;
					case "SWIGGY":
						if (!this.props.isInternetAvailable) noInternetDialog(this.context);
						else MiniApp.launchNativeApp("swiggy");
						break;
					case "BBInstant":
						if (Platform.OS === "android") {
							MiniApp.launchPhoneVerifiedApp(
								"bbinstant",
								this.props.userProfile,
								this.props.navigation
							);
						} else {
							this.context.current.openDialog({
								type: "Information",
								title: "Information",
								message:
									"BBInstant coming soon on iOS. Currently available only on Android."
							});
						}
						break;
					case "NATIVE":
						if (!this.props.isInternetAvailable) noInternetDialog(this.context);
						else {
							if (
								app_name === "Community" ||
								app_name === "Vms" ||
								app_name === "Ignite"
							) {
								MiniApp.launchCorporateVerifyApp(
									app_name,
									this.props.userProfile,
									this.props.navigation,
									link
								);
							} else {
								if (link !== "") {
									this.props.navigation.navigate(app_name, link);
								} else {
									MiniApp.launchApp(this.props.navigation, app_name);
								}
							}
						}
						break;
					case "SERVICE":
						if (!this.props.isInternetAvailable) noInternetDialog(this.context);
						else MiniApp.launchApp(this.props.navigation, app_name);
						break;
					default:
						console.log("do nothing");
						break;
				}
			}
		} catch (error) {
			console.log("unable to open app", error);
		}
	}

	onProfileClick = () => {
		if (!this.props.isInternetAvailable) noInternetDialog(this.context);
		else this.props.navigation.navigate("Profile");
	};

	render() {
		return (
			<View style={{ flex: 1 }}>
				<FlatList
					data={[1]}
					keyExtractor={(item, index) => index.toString()}
					refreshing={false}
					onRefresh={() => {
						if (this.state.isRefresh === null) {
							this.setState({ isRefresh: true });
						} else {
							this.setState({ isRefresh: !this.state.isRefresh });
						}
					}}
					renderItem={({ item }) => (
						<Feed
							isRefresh={this.state.isRefresh}
							resetRefresh={() => this.setState({ isRefresh: false })}
							launchPhoneVerifiedApp={MiniApp.launchPhoneVerifiedApp}
							launchApp={app => this.handleApplicationClick(app)}
							launchService={app => this.handleApplicationClick(app)}
							launchWebApp={(app, link) => this.handleApplicationClick(app)}
							launchAppWithLink={(app, link) =>
								this.handleApplicationClick(app, link)
							}
							onProfileClick={this.onProfileClick}
							onVerifyNumberClick={() =>
								this.props.navigation.navigate("VerifyPhone")
							}
							showTutorial={images =>
								this.setState({ showTutorial: true, images: images })
							}
						/>
					)}
				/>
				{this.state.showTutorial ? (
					<Tutorial
						hideTutorial={() =>
							this.setState({ showTutorial: false, images: [] })
						}
						data={this.state.images}
					/>
				) : null}
			</View>
		);
	}
}

Home.contextType = DialogContext;

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		isInternetAvailable: state.netInfo.status,
		location: state.location,
		userProfile: state.appToken.userProfile
	};
}

export default connect(mapStateToProps)(Home);
