import React from "react";
import {
	View,
	TouchableOpacity,
	Dimensions,
	Platform,
	ActivityIndicator,
	LayoutAnimation,
	UIManager,
	Linking
} from "react-native";
import BluetoothStateManager from "react-native-bluetooth-state-manager";
import { Icon, TermSheet } from "../../Components";
import BottomNavigator from "./BottomNavigator";
import MiniApp from "../../CustomModules/MiniApp";
import { GoappTextRegular } from "../../Components/GoappText";
import Access from "../../CustomModules/Access";
import { connect } from "react-redux";
import { GoappAlertContext } from "../../GoappAlertContext";
import { ERRORCLOUD } from "../../Assets/Img/Image";
import LocationModule from "../../CustomModules/LocationModule";
import GoAppAnalytics, { mpAppLaunch } from "../../utils/GoAppAnalytics";
import OpenGate from "../Access/Containers/OpenGate";
import noInternetDialog from "../../Components/NoInternetDialog";
import OfflineMessage from "../../Components/OfflineMessage";
import styles from "./styles";
import { ACCESS_16_DIGIT_CODE } from "../../CustomModules/Offline";
import { SiteSelectionContext } from "../../Components/SiteSelection/SiteSelectionContext";
import AsyncStorage from "@react-native-community/async-storage";
import TabNavigationContext from "./Context/TabNavigationContext";
import DialogContext from "../../DialogContext";

const { height } = Dimensions.get("window");

const bottomTabs = [
	{
		name: "Home",
		screenName: "Home",
		icon: "Home"
	},
	{
		name: "Services",
		screenName: "Service",
		icon: "Services"
	},
	{
		name: "Access",
		screenName: "Access",
		icon: "key"
	},
	{
		name: "Partners",
		screenName: "Partners",
		icon: "Partners"
	},
	{
		name: "Community",
		screenName: "Community",
		icon: "Community"
	}
];

if (Platform.OS === "android") {
	if (UIManager.setLayoutAnimationEnabledExperimental) {
		UIManager.setLayoutAnimationEnabledExperimental(true);
	}
}
class Landing extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedTab: 0,
			showUnlock: false,
			accessStatus: "Connecting",
			locationModule: false,
			change: false,
			isTermsOpen: false
		};
	}

	checkTnC = async () => {
		let mex_is_tnc_accepted = await AsyncStorage.getItem("mex_is_tnc_accepted");
		if (!mex_is_tnc_accepted) {
			this.setState({ isTermsOpen: true });
		} else {
			this.checkSite();
		}
	};

	siteSelectionRef = null;

	componentDidMount() {
		this.checkTnC();
	}

	checkSite = () => {
		if (this.props.selectedSite === null) {
			this.siteSelectionRef.current.openSiteSelection();
		} else {
			console.log("super properties  " + this.props.selectedSite);
			//Set super properties in case it wasn't
			GoAppAnalytics.setSuperProperties({
				site_id: this.props.selectedSite.id,
				ref_site_id: this.props.selectedSite.ref_site_id,
				site_name: this.props.selectedSite.name
			});
			GoAppAnalytics.setPeopleProperties({
				site_id: this.props.selectedSite.id,
				ref_site_id: this.props.selectedSite.ref_site_id,
				site_name: this.props.selectedSite.name
			});
		}
	};

	checkPermission = async () => {
		try {
			const { access } = this.props.userProfile;
			let invitationCode = null;

			/**
			 * Check whether "access" key is available in User Profile.
			 */
			if (access) {
				invitationCode = JSON.parse(access).invitationCode;
			}
			/**
			 * Fetch last used access code from Local Storage
			 */
			const LAST_USED_ACCESS_CODE = await Access.getLastUsedAccessCode(
				ACCESS_16_DIGIT_CODE
			);
			/**
			 * Last used code and invitation code from access object must be present and equal.
			 */

			/*
			 * Logic for this. If the user has LAST_USED_ACCESS_CODE then check for invitation code is same or not.
			 * if the user doesn't have LAST_USED_ACCESS_CODE. Meaning he is old user or he logged in with a different account.
			 * in both cases allow code to check lockn init status. Because we make sure to reset all users without LAST_USED_ACCESS_CODE
			 * upon logout. - Ashish
			 */
			if (
				(LAST_USED_ACCESS_CODE &&
					invitationCode &&
					LAST_USED_ACCESS_CODE === invitationCode) ||
				!LAST_USED_ACCESS_CODE
			) {
				BluetoothStateManager.getState().then(bluetoothState => {
					switch (bluetoothState) {
						case "Unauthorized":
							GoAppAnalytics.trackWithProperties("access_error", {
								error: "no_bluetooth",
								status: bluetoothState
							});
							this.context.current.openDialog({
								type: "DismissableAction",
								icon: ERRORCLOUD,
								title: "Error",
								message: "Bluetooth permission is not given.",
								rightPress: () => BluetoothStateManager.openSettings()
							});
							break;
						case "PoweredOff":
							GoAppAnalytics.trackWithProperties("access_error", {
								error: "no_bluetooth",
								status: bluetoothState
							});
							this.context.current.openDialog({
								type: "DismissableAction",
								icon: ERRORCLOUD,
								title: "Error",
								message: "Bluetooth needs to be enabled for this service.",
								rightPress: () => BluetoothStateManager.openSettings()
							});
							break;
						case "PoweredOn":
							this.checkAccessStatus()
								.then(response => {
									/**
									 * Access already initialized
									 * Mixpanel App Launch
									 */
									mpAppLaunch({
										name: "access",
										access_enabled: true
									});
									LayoutAnimation.configureNext(
										LayoutAnimation.Presets.easeInEaseOut
									);
									this.setState({ showUnlock: true }, () => {
										Access.openLock(
											success => {
												console.log(success);
												GoAppAnalytics.trackWithProperties("access_status", {
													status: success
												});
												LayoutAnimation.configureNext(
													LayoutAnimation.Presets.easeInEaseOut
												);
												this.setState(
													{ accessStatus: success, change: true },
													() =>
														setTimeout(() => {
															LayoutAnimation.configureNext(
																LayoutAnimation.Presets.easeInEaseOut
															);
															this.setState({
																showUnlock: false,
																accessStatus: "Connecting",
																change: false
															});
														}, 1500)
												);
											},
											error => {
												console.log(error);
												GoAppAnalytics.trackWithProperties("access_status", {
													status: error
												});
												setTimeout(() => {
													LayoutAnimation.configureNext(
														LayoutAnimation.Presets.easeInEaseOut
													);
													this.setState({ accessStatus: error }, () =>
														setTimeout(() => {
															LayoutAnimation.configureNext(
																LayoutAnimation.Presets.easeInEaseOut
															);
															this.setState({
																showUnlock: false,
																accessStatus: "Connecting",
																change: false
															});
														}, 2000)
													);
												}, 500);
											}
										);
									});
								})
								.catch(error => {
									// Access not initialized
									Access.removeAccessCode(ACCESS_16_DIGIT_CODE);
									if (!this.props.isInternetAvailable)
										noInternetDialog(this.context);
									else this.props.navigation.navigate("Access");
								});
							break;
						default:
							GoAppAnalytics.trackWithProperties("access_error", {
								error: "no_bluetooth"
							});
							this.context.current.openDialog({
								type: "Information",
								icon: ERRORCLOUD,
								title: "Error",
								message: "Something went wrong."
							});
							break;
					}
				});
			} else {
				/**
				 * Reset the Access Module.
				 * Clear the last used access code.
				 * Navigate to Enter Code screen in Access Module.
				 */
				Access.factoryReset();
				Access.removeAccessCode(ACCESS_16_DIGIT_CODE);
				if (!this.props.isInternetAvailable) noInternetDialog(this.context);
				else this.props.navigation.navigate("Access");
			}
		} catch (error) {
			/**
			 * Show popup in case of any caught errors.
			 */
			console.log("error is", error);
			GoAppAnalytics.trackWithProperties("access_error", {
				error: "Something went wrong."
			});
			this.context.current.openDialog({
				type: "Information",
				icon: ERRORCLOUD,
				title: "Error",
				message: "Something went wrong."
			});
		}
	};

	checkAccessStatus = () => {
		return new Promise((resolve, reject) => {
			/**
			 * Check whether the Access module is already initialized.
			 */
			Access.isInitialized(data => {
				if (data) {
					resolve(data);
				} else {
					reject(data);
				}
			});
		});
	};

	navigationOfTab = screenName => {
		switch (screenName) {
			case "Home":
				this.props.navigation.navigate("Home");
				this.setState({ selectedTab: 0 });
				return;
			case "Service":
				this.props.navigation.navigate("Service");
				this.setState({ selectedTab: 1 });
				return;
			case "Access":
				// Check access status whether isInitialized or not
				this.setState({ locationModule: true });
				return;
			case "Partners":
				this.props.navigation.navigate("Partners");
				this.setState({ selectedTab: 3 });
				return;
			case "Community":
				if (!this.props.isInternetAvailable) noInternetDialog(this.context);
				else
					MiniApp.launchCorporateVerifyApp(
						"Community",
						this.props.userProfile,
						this.props.navigation
					);
				return;
			default:
				return;
		}
	};

	hideUnlock = () => this.setState({ showUnlock: false });

	showStatus = accessStatus => {
		return accessStatus;
	};

	render() {
		const { showUnlock, accessStatus, isTermsOpen } = this.state;
		const { userProfile } = this.props;

		if (isTermsOpen) {
			return (
				<TermSheet
					open={true}
					close={() => {
						this.setState({ isTermsOpen: false }, () => this.checkSite());
					}}
				/>
			);
		}

		return (
			<View style={styles.container}>
				{this.state.locationModule ? (
					<LocationModule
						onError={error => {
							GoAppAnalytics.trackWithProperties("access_error", {
								error: "no_location"
							});

							if (
								(Platform.OS === "android" &&
									error &&
									error === "no-permission-given") ||
								Platform.OS === "ios"
							) {
								/**
								 * When permission is denied by user then navigate him to settings.
								 */
								this.context.current.openDialog({
									type: "DismissableAction",
									icon: ERRORCLOUD,
									title: "Error",
									message: "Location Permission is off, kindly turn it on.",
									rightPress: () => {
										Linking.openSettings();
									}
								});
							}
							this.setState({
								locationModule: false
							});
						}}
						onLoad={() => {
							this.setState({
								locationModule: false
							});
							this.checkPermission();
						}}
					/>
				) : null}
				<SiteSelectionContext.Consumer>
					{siteRef => {
						this.siteSelectionRef = siteRef;
						return null;
					}}
				</SiteSelectionContext.Consumer>
				<TabNavigationContext.Provider
					value={{
						navigationOfTab: screenName => this.navigationOfTab(screenName)
					}}>
					<BottomNavigator />
				</TabNavigationContext.Provider>
				<OpenGate
					showUnlock={showUnlock}
					accessStatus={accessStatus}
					showStatus={this.showStatus}
					loop={!this.state.change}
					source={
						this.state.change
							? require("../../../tick.json")
							: require("../../../loader.json")
					}
				/>
				<OfflineMessage show={!this.props.isInternetAvailable} />
				<View style={styles.tabBar}>
					{bottomTabs.map((item, index) => (
						<TouchableOpacity
							key={index}
							disabled={
								showUnlock
									? true
									: index === 2 && this.checkAccessStatus()
									? false
									: userProfile
									? false
									: true
							}
							style={styles.tab}
							onPress={() => this.navigationOfTab(item.screenName)}>
							{index === 2 && showUnlock ? (
								<ActivityIndicator />
							) : (
								<Icon
									iconType={"icomoon"}
									iconName={item.icon}
									iconColor={
										this.state.selectedTab === index ? "#2C98F0" : "#8F96A3"
									}
									iconSize={height / 50}
								/>
							)}
							<GoappTextRegular
								style={
									this.state.selectedTab === index
										? styles.tabTextEnabled
										: styles.tabText
								}>
								{index === 2 && showUnlock ? "Accessing" : item.name}
							</GoappTextRegular>
						</TouchableOpacity>
					))}
				</View>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		user: state.appToken.user,
		isInternetAvailable: state.netInfo.status,
		userProfile: state.appToken.userProfile,
		selectedSite: state.appToken.selectedSite
	};
}
Landing.contextType = GoappAlertContext;
Landing.contextType = DialogContext;

export default connect(mapStateToProps)(Landing);
