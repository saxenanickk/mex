import React from "react";
import { BottomTab } from "../../utils/Navigators";
import Home from "./Home";
import Service from "../../Components/Service";
import Partners from "../../Components/Partners";
import Access from "../Access";

const TabNavigator = () => (
	<BottomTab.Navigator
		tabBarOptions={{
			style: { width: 0, height: 0 }
		}}
		backBehavior={"initialRoute"}>
		<BottomTab.Screen name="Home" component={Home} />
		<BottomTab.Screen name="Service" component={Service} />
		<BottomTab.Screen name="Access" component={Access} />
		<BottomTab.Screen name="Partners" component={Partners} />
	</BottomTab.Navigator>
);

const BottomTabNavigator = ({ navigation }) => <TabNavigator />;

export default BottomTabNavigator;
