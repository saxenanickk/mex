import Config from "react-native-config";
import { Platform } from "react-native";

const { SERVER_BASE_URL_COMMUNITY, MEX_LOGIN_BASE_URL } = Config;

const createFormData = params => {
	const data = new FormData();
	const photo = params.photo;
	const name = params.name;
	data.append("image", {
		name: photo.fileName,
		type: photo.type,
		uri:
			Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
	});
	data.append("name", name);
	data.append("phonenum", params.phoneNum);
	return data;
};

class GoappApi {
	constructor() {
		console.log("Goapp Api Instantiated.");
	}

	getAllSkills(params) {
		return new Promise(function(resolve, reject) {
			let url = params.searchQuery
				? "getSkills?name=" + params.searchQuery
				: "getSkills";
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + url, {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getEducationInstitute(params) {
		return new Promise(function(resolve, reject) {
			let url = params.searchQuery
				? "getEducations?name=" + params.searchQuery
				: "getEducations";
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + url, {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getHobbiesAndInterests(params) {
		return new Promise(function(resolve, reject) {
			let url = params.searchQuery
				? "getHobbiesAndInterests?name=" + params.searchQuery
				: "getHobbiesAndInterests";
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + url, {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response of hobby", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	uploadProfilePic(params) {
		let body =
			params.photo !== null &&
			params.photo !== undefined &&
			createFormData(params);
		console.log("body of phone num", body);
		return new Promise(function(resolve, reject) {
			try {
				fetch(MEX_LOGIN_BASE_URL + "uploadProfilePicture", {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"content-type": "multipart/form-data",
						"cache-control": "no-cache"
					},
					body: body
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => {
						console.log("error in profile pic upload", error);
						reject(error);
					});
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getMyProfile(params) {
		const X_ACCESS_TOKEN = params.appToken;
		return new Promise((resolve, reject) => {
			try {
				fetch(`${MEX_LOGIN_BASE_URL}getProfile`, {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": X_ACCESS_TOKEN
					}
				})
					.then(response =>
						response
							.json()
							.then(res => resolve(res))
							.catch(error => reject(error))
					)
					.catch(error => reject(error));
			} catch (error) {
				reject(error);
			}
		});
	}

	editMyProfile(params) {
		return new Promise((resolve, reject) => {
			let url = "editProfile";
			try {
				fetch(MEX_LOGIN_BASE_URL + url, {
					method: "POST",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					},
					body: JSON.stringify(params.body)
				})
					.then(response => {
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(err => reject(err));
					})
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	saveFCMToken(params) {
		return new Promise((resolve, reject) => {
			let url = "saveFcmToken";
			try {
				fetch(MEX_LOGIN_BASE_URL + url, {
					method: "POST",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					},
					body: JSON.stringify(params.body)
				})
					.then(response => {
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(err => reject(err));
					})
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}
}

export default new GoappApi();
