import React from "react";
import { AppRegistry, Platform, Alert } from "react-native";
import App from "./App";
import { connect } from "react-redux";
import MiniApp from "../../CustomModules/MiniApp";
class Goapp extends React.Component {
	constructor(props) {
		super(props);
		console.disableYellowBox = true;
	}

	onAppClick = (app, params) => {
		if (app.isPwa !== undefined) {
			if (app.isPwa === true) {
				if (app.appName === "Azgo" || app.appName === "Hungerbox") {
					MiniApp.launchPhoneVerifiedApp(
						app.appName,
						this.props.userProfile,
						this.props.navigation
					);
				} else {
					MiniApp.launchWebApp(app.appName, app.pwaUrl);
				}
			} else {
				MiniApp.launchApp(this.props.navigation, app.appName);
			}
			return;
		}
		try {
			switch (app.type) {
				case "PWA":
					//uncomment this code when trying to launch web app
					if (app.name === "Azgo" || app.name === "Hungerbox") {
						MiniApp.launchPhoneVerifiedApp(
							app.name,
							this.props.userProfile,
							this.props.navigation
						);
					} else {
						MiniApp.launchWebApp(app.name, app.pwaUrl);
					}
					break;
				case "SWIGGY":
					MiniApp.launchNativeApp(app.name);
					break;
				case "BBInstant":
					if (Platform.OS === "android") {
						MiniApp.launchNativeApp(app.name);
					} else {
						console.log("open alert");
						Alert.alert(
							"Info!",
							"BBInstant coming soon on iOS. Currently available only on Android."
						);
					}
					break;
				case "NATIVE":
					if (
						app.name === "Community" ||
						app.name === "Vms" ||
						app.name === "Ignite"
					) {
						MiniApp.launchCorporateVerifyApp(
							app.name,
							this.props.userProfile,
							this.props.navigation
						);
					} else {
						MiniApp.launchApp(this.props.navigation, app.name);
					}
					break;
				default:
					console.log("do nothing", app);
			}
		} catch (error) {
			console.log("unable to open app", error);
		}
	};

	render() {
		return <App {...this.props} />;
	}
}

const mapStateToProps = state => ({
	user: state.appToken.user,
	userProfile: state.appToken.userProfile
});

export default connect(mapStateToProps)(Goapp);
AppRegistry.registerComponent("Goapp", () => Goapp);
