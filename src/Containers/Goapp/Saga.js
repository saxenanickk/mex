import { takeLatest, call } from "redux-saga/effects";

import Api from "./Api";

export const MEX_SAVE_FCM_TOKEN = "MEX_SAVE_FCM_TOKEN";

export const mexSaveFcmToken = payload => ({
	type: MEX_SAVE_FCM_TOKEN,
	payload
});

export default function* goappSaga(dispatch) {
	yield takeLatest(MEX_SAVE_FCM_TOKEN, handleMexSaveFcmToken);
}

function* handleMexSaveFcmToken(action) {
	try {
		const resp = yield call(Api.saveFCMToken, action.payload);
		console.log("response of fcm tken", resp);
	} catch (error) {
		console.log("error while saving fcm token", error);
	}
}
