import React from "react";
import { Stack } from "../../utils/Navigators";
import Landing from "./Landing";
import Profile from "./Containers/Profile";

const RegisterScreen = props => {
	return (
		<Stack.Navigator headerMode="none">
			<Stack.Screen name="Landing" component={Landing} />
			<Stack.Screen name="Profile" component={Profile} />
		</Stack.Navigator>
	);
};

export default RegisterScreen;
