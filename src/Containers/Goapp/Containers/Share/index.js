import React, { Component } from "react";
import {
	View,
	Platform,
	Dimensions,
	TouchableOpacity,
	Linking,
	SafeAreaView,
	BackHandler
} from "react-native";
import FastImage from "react-native-fast-image";
import Branch from "react-native-branch";
import { connect } from "react-redux";
import Config from "react-native-config";
import { Icon, GoToast } from "../../../../Components";
import {
	GoappTextMedium,
	GoappTextRegular
} from "../../../../Components/GoappText";
import { SHARE } from "../../../../Assets/Img/Image";

const { width } = Dimensions.get("window");

export class Share extends Component {
	_buo = null;
	_shortUrl = null;
	componentDidMount() {
		this.createBranchUniversalObject();
		BackHandler.addEventListener("hardwareBackPress", this.handleGoBack);
	}
	componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.handleGoBack);
	}
	handleGoBack = () => {
		this.props.navigation.goBack();
		return true;
	};

	createBranchUniversalObject = async () => {
		try {
			let branchUniversalObject = await Branch.createBranchUniversalObject(
				"invite",
				{
					locallyIndex: true,
					contentMetadata: {
						customMetadata: {
							userId: this.props.userProfile.userid
								? this.props.userProfile.userid
								: null
						}
					},
					title: "MEX.",
					contentDescription:
						"Install MEX.! Experience the world of smart buildings and smart features."
				}
			);

			this._buo = branchUniversalObject;

			let linkProperties = {
				userId: this.props.userProfile.userid
					? this.props.userProfile.userid
					: null,
				feature: "share",
				channel: "App"
			};

			let controlParams = {
				$desktop_url: Config.BRANCH_DESKTOP_URL
			};

			let { url } = await branchUniversalObject.generateShortUrl(
				linkProperties,
				controlParams
			);

			this._shortUrl = url;
			this.forceUpdate();
		} catch (error) {
			console.log(error);
		}
	};

	showShare = async () => {
		let shareOptions = {
			messageHeader: "Invitation to join mex",
			messageBody:
				"Join me on MEX. An ultimate member experience app for people working in RMZ Campuses. Community, Events, Marketplace, Offers and a lot more."
		};
		let linkProperties = { feature: "share", channel: "RNApp" };
		let { channel, completed, error } = await this._buo.showShareSheet(
			shareOptions,
			linkProperties
		);
	};

	shareToWhatsapp = () => {
		const referText =
			"Join me on MEX. An ultimate member experience app for people working in RMZ Campuses. Community, Events, Marketplace, Offers and a lot more.";
		Linking.canOpenURL(`whatsapp://send?text=${referText} ${this._shortUrl}`)
			.then(supported => {
				if (!supported) {
					GoToast.show("Oops!", "Not able to share on whatsapp", 1000);
				} else {
					Linking.openURL(
						`whatsapp://send?text=${referText} ${this._shortUrl}`
					);
				}
			})
			.catch(err => {
				GoToast.show("Oops!", "Not able to share on whatsapp", 1000);
			});
	};

	render() {
		return (
			<SafeAreaView style={{ flex: 1 }}>
				<SafeAreaView style={{ flex: 0 }} />
				<View
					style={{
						flex: 1,
						margin: width / 20
					}}>
					<View
						style={{
							justifyContent: "space-around",
							flex: 1
						}}>
						<View
							style={{
								height: width / 4.6,
								paddingHorizontal: width / 20.8
							}}>
							<GoappTextRegular size={15} color={"#313131"}>
								Share this invite link with your friends and colleagues and help
								them get the unique experience of MEX.
							</GoappTextRegular>
							{/* Create a webpage and show */}
						</View>
						<FastImage
							source={SHARE}
							style={{
								width: undefined,
								height: undefined,
								flex: 0.6
							}}
							resizeMode={"contain"}
						/>
						<View
							style={{
								margin: width / 25
							}}>
							<GoappTextRegular color={"#404040"}>Invite link</GoappTextRegular>
							<View
								style={{
									borderWidth: 1,
									flexDirection: "row",
									justifyContent: "space-around",
									alignItems: "center",
									paddingVertical: 10,
									marginVertical: 10,
									borderRadius: 10
								}}>
								{this._shortUrl === null ? (
									<GoappTextRegular>Generating url</GoappTextRegular>
								) : (
									<GoappTextRegular size={14}>
										{this._shortUrl}
									</GoappTextRegular>
								)}
								<TouchableOpacity onPress={this.showShare}>
									<Icon
										iconType={"ionicon"}
										iconName={"md-share"}
										iconColor={"#2C98F0"}
										iconSize={width / 15.6}
									/>
								</TouchableOpacity>
							</View>
							{Platform.OS === "ios" ? null : (
								<TouchableOpacity
									onPress={this.shareToWhatsapp}
									style={{
										backgroundColor: "#2C98F0",
										alignItems: "center",
										paddingVertical: 10,
										borderRadius: 40
									}}>
									<GoappTextMedium color={"#fff"} size={15}>
										Share on Whatsapp
									</GoappTextMedium>
								</TouchableOpacity>
							)}
						</View>
					</View>
				</View>
			</SafeAreaView>
		);
	}
}

const mapStateToProps = state => ({
	userProfile: state.appToken.userProfile
});

export default connect(mapStateToProps)(Share);
