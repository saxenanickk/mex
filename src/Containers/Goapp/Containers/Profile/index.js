import React, { Component } from "react";
import {
	ScrollView,
	View,
	TouchableOpacity,
	Dimensions,
	ActivityIndicator
} from "react-native";
import {
	GoappTextRegular,
	GoappTextBold,
	GoappTextMedium
} from "../../../../Components/GoappText";
import { connect } from "react-redux";
import { CommonActions } from "@react-navigation/native";
import styles from "./style";
import {
	Icon,
	ImageViewer,
	ProfileButton,
	GoappCamera
} from "../../../../Components";
import { EMPTYIMAGE } from "../../../../Assets/Img/Image";
import { mexLogout } from "../../../LoginScreen/Saga";
import FastImage from "react-native-fast-image";
import Api from "../../Api";
import { signOut as GoogleSignOut } from "../../../../CustomModules/GoogleLogin";
import { FacebookLogout } from "../../../../CustomModules/FacebookLogin";
import Access from "../../../../CustomModules/Access";
import { ACCESS_16_DIGIT_CODE } from "../../../../CustomModules/Offline";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import { mexGetProfile } from "../../../../CustomModules/ApplicationToken/Saga";
import DialogContext from "../../../../DialogContext";
const { height, width } = Dimensions.get("window");

// Image Picker Options
const options = {
	storageOptions: {
		skipBackup: true,
		path: "images",
		cameraRoll: true,
		waitUntilSaved: true
	},
	maxHeight: 400,
	maxWidth: 400,
	quality: 0.5
};

const MexButton = props => (
	<TouchableOpacity
		style={[
			{
				borderRadius: width / 5,
				width: props.size,
				flexDirection: "row",
				backgroundColor: "#3398f0",
				justifyContent: "center",
				alignItems: "center",
				paddingVertical: height / 60,
				paddingHorizontal: width / 90
			},
			props.style
		]}
		onPress={() => props.onPress()}>
		<GoappTextMedium
			style={[{ fontSize: height / 45, color: "#fff" }, props.textStyle]}
			numberOfLines={1}>
			{props.buttonText}
		</GoappTextMedium>
		{props.iconName ? (
			<Icon
				style={{ marginLeft: width / 30 }}
				iconName={props.iconName}
				iconType={props.iconType}
				iconColor={props.iconColor}
				iconSize={props.iconSize}
			/>
		) : null}
	</TouchableOpacity>
);

MexButton.defaultProps = {
	onPress: () => console.log("pass function")
};
class Profile extends Component {
	constructor(props) {
		super(props);
		this.state = {
			open: false,
			image: null,
			isImageUploading: false
		};
		this.handleClose = this.handleClose.bind(this);
		this.openImagePicker = this.openImagePicker.bind(this);
		this.uploadImage = this.uploadImage.bind(this);
	}

	componentDidMount() {
		this.props.dispatch(mexGetProfile({ appToken: this.props.appToken }));
	}
	handleClose = () => {
		this.props.navigation.goBack();
	};

	openImagePicker = () => {
		this.setAnalyticsToChangePic("initiate");
		this.cameraRef.handleCameraPermission();
	};

	renderUserImage = userProfile => {
		try {
			let image = userProfile.image;
			//first check is image is getting uploaded
			if (
				image !== null &&
				image !== undefined &&
				image.trim() !== "" &&
				image.startsWith("http")
			) {
				return (
					<TouchableOpacity
						onPress={() => this.setState({ open: true, image })}>
						<FastImage source={{ uri: image }} style={styles.userAvatar} />
						{this.state.isImageUploading ? (
							<View style={styles.loadingImage}>
								<ActivityIndicator size={"small"} color={"#3398f0"} />
							</View>
						) : null}
						<TouchableOpacity
							style={styles.profileEdit}
							onPress={this.openImagePicker}>
							<Icon
								iconType={"material"}
								iconName={"edit"}
								iconSize={20}
								iconColor={"white"}
							/>
						</TouchableOpacity>
					</TouchableOpacity>
				);
			} else {
				throw new Error("User profile pic not found");
			}
		} catch (error) {
			return (
				<View>
					<FastImage source={EMPTYIMAGE} style={styles.userAvatar} />
					{this.state.isImageUploading ? (
						<View style={styles.loadingImage}>
							<ActivityIndicator size={"small"} color={"#3398f0"} />
						</View>
					) : null}
					<TouchableOpacity
						style={styles.profileEdit}
						onPress={this.openImagePicker}>
						<Icon
							iconType={"material"}
							iconName={"edit"}
							iconSize={20}
							iconColor={"white"}
						/>
					</TouchableOpacity>
				</View>
			);
		}
	};

	validateTypeOfImage = image => {
		try {
			let response = image;
			if (image.type === null || image.type === undefined) {
				let type = image.fileName.split(".");
				type = type[type.length - 1].toLowerCase();
				response = {
					...response,
					type: `image\/${type}`
				};
			}
			return response;
		} catch (error) {
			return image;
		}
	};

	uploadImage = async response => {
		try {
			this.setAnalyticsToChangePic("upload_start");
			this.setState({ isImageUploading: true });
			response = this.validateTypeOfImage(response);
			const resp = await Api.uploadProfilePic({
				appToken: this.props.appToken,
				photo: response,
				name: this.props.userProfile.name,
				phoneNum: this.props.userProfile.phonenum
			});
			if (resp.success === 1) {
				this.setAnalyticsToChangePic("upload_success");
				this.props.dispatch(mexGetProfile({ appToken: this.props.appToken }));
				let self = this;
				setTimeout(() => {
					self.setState({ isImageUploading: false });
				}, 1000);
			} else {
				throw new Error("Unable to upload image");
			}
		} catch (error) {
			this.setAnalyticsToChangePic("upload_fail");
			this.context.current.openDialog({
				type: "Information",
				title: "Error",
				message: "Unable to update your profile pic."
			});
			this.setState({ isImageUploading: false });
		}
	};

	checkIsCorporate() {
		try {
			if (this.props.userProfile && this.props.userProfile.tenantdto) {
				const tenantDto = JSON.parse(this.props.userProfile.tenantdto);
				return (
					tenantDto.tenant_id !== null && tenantDto.tenant_id !== undefined
				);
			} else {
				return false;
			}
		} catch (error) {
			return false;
		}
	}

	setAnalytics = section => {
		GoAppAnalytics.trackWithProperties("profile_section_click", { section });
	};

	setAnalyticsToChangePic = status => {
		GoAppAnalytics.trackWithProperties("change_profile_pic", { status });
	};

	disableAccess = async () => {
		const LAST_USED_ACCESS_CODE = await Access.getLastUsedAccessCode(
			ACCESS_16_DIGIT_CODE
		);
		if (!LAST_USED_ACCESS_CODE) {
			Access.factoryReset();
		}
	};

	render() {
		const { handleClose } = this;
		return (
			<View>
				<ScrollView>
					<View style={styles.header}>
						<TouchableOpacity onPress={handleClose} style={styles.backButton}>
							<Icon
								iconType={"ionicon"}
								iconName={"md-arrow-back"}
								iconSize={height / 25}
								iconColor={"#000"}
							/>
						</TouchableOpacity>
						<View
							style={{
								alignItems: "center"
							}}>
							{this.renderUserImage(this.props.userProfile)}
							<GoappTextBold style={styles.userName}>
								{this.props.userProfile.name}
							</GoappTextBold>
						</View>
						{this.checkIsCorporate() === false ? (
							<MexButton
								style={{ marginTop: height / 90 }}
								size={width / 1.5}
								buttonText={"Join MEX. Community Now!"}
								onPress={() => {
									this.setAnalytics("corp_verify");
									this.props.navigation.navigate("CorpVerify");
								}}
							/>
						) : null}
					</View>
					<View style={{ backgroundColor: "#EFF1F4" }}>
						{/** Normal Settings */}
						<View style={styles.appDetails}>
							<GoappTextBold style={styles.viewHeader}>
								{"Settings"}
							</GoappTextBold>
							<ProfileButton
								iconName={"profile_icon"}
								iconSize={height / 35}
								menuText={"Edit Profile Information"}
								onPress={() => {
									this.setAnalytics("edit_profile");
									this.props.navigation.navigate("EditProfile");
								}}
							/>
							<ProfileButton
								iconName={"corp_verify"}
								iconSize={height / 35}
								menuText={"Corporate Verification"}
								isVerified={this.checkIsCorporate()}
								onPress={() => {
									this.setAnalytics("corp_verify");
									this.props.navigation.navigate("CorpVerify");
								}}
							/>
							{/* <ProfileButton
								iconName={"bell_icon"}
								menuText={"Notifications"}
								iconSize={height / 35}
							/> */}
						</View>
						{/** In App  Section */}
						<View style={styles.appDetails}>
							<GoappTextBold style={styles.viewHeader}>
								{"In App"}
							</GoappTextBold>
							<ProfileButton
								iconName={"md-reorder"}
								iconSize={height / 35}
								iconType={"ionicon"}
								menuText={"Order History"}
								onPress={() => {
									this.setAnalytics("order_history"),
										this.props.navigation.navigate("Order");
								}}
							/>
							<ProfileButton
								onPress={() => {
									this.setAnalytics("invite_friend");
									this.props.navigation.navigate("Share");
								}}
								iconName={"people"}
								iconType={"simple_line"}
								iconSize={height / 37}
								menuText={"Invite Friends"}
							/>
						</View>
						{/** ABOUT  section */}
						<View style={styles.appDetails}>
							<GoappTextBold style={styles.viewHeader}>{"About"}</GoappTextBold>
							<ProfileButton
								iconName={"mex_new"}
								menuText={"About MEX."}
								iconSize={height / 55}
								onPress={() => {
									this.setAnalytics("about_mex");
									this.props.navigation.navigate("LinkView", {
										url: "https://www.mexit.in/",
										title: "About MEX."
									});
								}}
							/>
							<ProfileButton
								iconName={"ios-information-circle-outline"}
								iconType={"ionicon"}
								menuText={"Frequently asked Questions"}
								iconSize={height / 35}
								onPress={() => {
									this.setAnalytics("faq");
									this.props.navigation.navigate("LinkView", {
										url: "https://www.mexit.in/faqs",
										title: "FAQs"
									});
								}}
							/>
							<ProfileButton
								iconName={"md-paper"}
								iconType={"ionicon"}
								iconSize={height / 35}
								menuText={"Terms & Conditions"}
								onPress={() => {
									this.setAnalytics("terms_and_conditions");
									this.props.navigation.navigate("LinkView", {
										url: "https://www.mexit.in/service-terms",
										title: "Terms & Conditions"
									});
								}}
							/>
							<ProfileButton
								iconName={"md-copy"}
								iconType={"ionicon"}
								iconSize={height / 35}
								menuText={"Privacy Policy"}
								onPress={() => {
									this.setAnalytics("privacy_policy");
									this.props.navigation.navigate("LinkView", {
										url: "https://www.mexit.in/privacy-policy",
										title: "Privacy Policy"
									});
								}}
							/>
							<ProfileButton
								iconName={"headset"}
								iconType={"material_community_icon"}
								iconSize={height / 35}
								menuText={"Contact Us"}
								onPress={() => {
									this.setAnalytics("contact_us");
									this.props.navigation.navigate("ContactUs");
								}}
							/>
						</View>
						<View style={styles.appDetails}>
							<TouchableOpacity
								style={styles.logoutButton}
								onPress={() =>
									this.context.current.openDialog({
										type: "Confirmation",
										title: "Are you sure?",
										message: "Please confirm if you want to logout.",
										leftTitle: "No",
										leftPress: () => {},
										rightTitle: "Yes",
										rightPress: () => {
											this.setAnalytics("logout");
											GoAppAnalytics.reset();
											FacebookLogout();
											GoogleSignOut();
											this.disableAccess();
											this.props.dispatch(
												mexLogout({ token: this.props.appToken })
											);
											this.props.navigation.dispatch(
												CommonActions.reset({
													index: 0,
													routes: [{ name: "Login" }]
												})
											);
										}
									})
								}>
								<Icon
									iconName={"ios-log-out"}
									iconType={"ionicon"}
									iconColor={"#ff0000"}
									iconSize={height / 35}
								/>
								<GoappTextRegular style={styles.logoutText} numberOfLines={1}>
									{"Logout"}
								</GoappTextRegular>
							</TouchableOpacity>
						</View>
					</View>
				</ScrollView>
				{this.state.open ? (
					<ImageViewer
						open={this.state.open}
						image={this.state.image}
						close={() => this.setState({ open: false, image: "" })}
					/>
				) : null}
				<GoappCamera
					ref={ref => (this.cameraRef = ref)}
					options={options}
					quality={0.5}
					onImageSelect={this.uploadImage}
				/>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		user: state.appToken.user,
		userProfile: state.appToken.userProfile
	};
}

Profile.contextType = DialogContext;
export default connect(mapStateToProps)(Profile);
