import { Dimensions, StyleSheet } from "react-native";
const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
	header: {
		backgroundColor: "#fff",
		alignItems: "center",
		paddingHorizontal: width / 30,
		paddingVertical: height / 60
	},
	logoutButton: {
		backgroundColor: "#fff",
		flexDirection: "row",
		alignSelf: "center",
		width: width / 1.5,
		justifyContent: "center",
		alignItems: "center"
	},
	logoutText: {
		color: "#ff0000",
		marginLeft: width / 30
	},
	backButton: {
		width: width / 5,
		justifyContent: "center",
		height: height / 15,
		alignSelf: "flex-start"
	},
	backgroundImage: {
		height: height / 3,
		padding: height / 40
	},
	userAvatar: {
		height: width / 4,
		width: width / 4,
		borderRadius: width / 8
	},
	bottomSection: {
		flexDirection: "row",
		paddingHorizontal: width / 30,
		justifyContent: "space-between",
		paddingVertical: height / 60
	},
	profileEdit: {
		backgroundColor: "#3398f0",
		width: width / 12,
		height: width / 12,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 24,
		position: "absolute",
		bottom: -width / 24,
		left: width / 8 - width / 24
	},
	userName: {
		marginTop: height / 50,
		color: "#000",
		fontSize: height / 45
	},
	userActivitiesParent: {
		alignItems: "center"
	},
	userActivity: {
		flexDirection: "row",
		backgroundColor: "#ffffff",
		borderRadius: height / 30,
		marginTop: height / -25,
		height: height / 5,
		width: width / 1.2,
		justifyContent: "space-around",
		padding: height / 35
	},
	userActivitiesText: {
		fontSize: height / 48,
		color: "#000000"
	},
	userActivitiesPonits: {
		fontSize: height / 42,
		fontWeight: "bold",
		color: "#000"
	},
	hrView: {
		borderWidth: 1,
		borderColor: "#f3f3f3"
	},
	profileButtonStyle: {
		marginTop: height / -20,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		padding: width / 35,
		backgroundColor: "#2C98F0",
		color: "white",
		borderRadius: height / 10
	},
	profileButtonText: {
		color: "white",
		fontSize: height / 40,
		padding: height / 90
	},
	appDetails: {
		backgroundColor: "#ffffff",
		padding: height / 45,
		marginTop: height / 100
	},
	viewHeader: {
		color: "#000",
		fontSize: height / 45,
		marginBottom: height / 60
	},
	parent: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		padding: height / 35
	},
	child1: {
		flexDirection: "row"
	},
	featureTitle: {
		color: "#171717",
		fontSize: height / 48,
		paddingLeft: width / 22
	},
	featureTextWidth: {
		width: width / 1.7
	},
	featureLogout: {
		color: "#ed3833",
		fontSize: 20,
		paddingLeft: width / 22
	},
	liveSupportButton: {
		backgroundColor: "#3398f0",
		borderRadius: 100,
		flexDirection: "row",
		justifyContent: "space-around",
		padding: width / 13
	},
	logoutIcon: {
		height: height / 29,
		width: width / 24,
		marginRight: width / 18
	},
	notificationIcon: {
		height: height / 28,
		width: width / 20,
		marginRight: width / 17
	},
	UserIcon: {
		height: height / 29,
		width: width / 18,
		marginRight: width / 21
	},
	supportIcon: {
		height: height / 24,
		width: width / 16,
		marginRight: width / 27
	},
	corporateIcon: {
		height: height / 29,
		width: width / 16,
		marginRight: width / 27
	},
	friendsIcon: {
		height: height / 29,
		width: width / 13,
		marginRight: width / 33
	},
	alignIcon: {
		height: height / 29,
		width: width / 19,
		marginRight: width / 20
	},
	headset: {
		height: height / 29,
		width: width / 18,
		margin: width / 50
	},
	supportButtonText: {
		color: "white",
		fontSize: height / 40,
		padding: width / 50
	},
	liveSupportButtons: {
		width: width / 2.5,
		height: height / 13,
		backgroundColor: "#3398f0",
		flexDirection: "row",
		justifyContent: "space-around",
		borderRadius: width / 10,
		padding: width / 67
	},
	loadingImage: {
		position: "absolute",
		top: 0,
		height: width / 4,
		width: width / 4,
		borderRadius: width / 2,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "rgba(0,0,0,0.5)"
	}
});

export default styles;
