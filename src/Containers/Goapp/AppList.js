export const AppList = [
	{
		name: "1mg",
		type: "PWA",
		url: "https://www.1mg.com/?utm_source=RMZ&utm_medium=app&utm_campaign=mex"
	},
	{ name: "Azgo", type: "PWA", url: "https://rmz.azgo.app/?session=" },
	{ name: "Hungerbox", type: "PWA", url: "https://rmz.azgo.app/?session=" },
	{ name: "BBInstant", type: "BBInstant" },
	{ name: "Offers", type: "NATIVE" },
	{
		name: "Access",
		type: "NATIVE"
	},
	{ name: "Community", type: "NATIVE" },
	{ name: "Ignite", type: "NATIVE" },
	{ name: "Vms", type: "NATIVE" },
	{
		name: "Behrouz",
		type: "PWA",
		url:
			"https://www.behrouzbiryani.com/?utm_source=MEX&utm_medium=MEXmedia&utm_campaign=MEXoffers"
	},
	{ name: "BookMyShow", type: "PWA", url: "https://in.bookmyshow.com/" },
	{ name: "Cleartrip", type: "NATIVE" },
	{ name: "CleartripHotel", type: "NATIVE" },
	{ name: "Events", type: "NATIVE" },
	{
		name: "Drivezy",
		type: "PWA",
		url:
			"https://m.drivezy.com/?utm_source=RMZ&utm_medium=Referral&utm_campaign=PWAOnMex"
	},
	{
		name: "Faasos",
		type: "PWA",
		url:
			"https://order.faasos.io/?utm_source=MEX&utm_medium=MEXmedia&utm_campaign=MEXoffers"
	},
	{
		name: "Housejoy",
		type: "PWA",
		url:
			"https://www.housejoy.in/?utm_source=MEXRMZ&utm_medium=INapp&utm_campaign=promotions"
	},
	{
		name: "ICICI Auto Loan",
		type: "PWA",
		url:
			"https://loan.icicibank.com/asset-portal/auto-loan/apply-now?WT.mc_id=RMZ_Mex_ALoffer&utm_source=RMZ&utm_medium=Banner&utm_campaign=Mex_ALoffer"
	},
	{
		name: "ICICI Personal Loan",
		type: "PWA",
		url:
			"https://loan.icicibank.com/asset-portal/personal-loan/apply-now?WT.mc_id=RMZ_Mex_PLoffer&utm_source=RMZ&utm_medium=Banner&utm_campaign=Mex_PLoffer"
	},
	{
		name: "Licious",
		type: "PWA",
		url:
			"https://m.licious.in/?utm_source=RMZ&utm_medium=RMZ-MEX&utm_campaign=RMZ-May"
	},
	{
		name: "Medlife",
		type: "PWA",
		url:
			"https://m.medlife.com/?utm_source=RMZ_coworks&utm_medium=MEX_Marketplace#/root/home/HomeLandingOld"
	},
	{ name: "Ola", type: "NATIVE" },
	{
		name: "Oven Story",
		type: "PWA",
		url:
			"https://www.ovenstory.in/?utm_source=MEX&utm_medium=MEXmedia&utm_campaign=MEXoffers"
	},
	{
		name: "Oyo",
		type: "PWA",
		url: "https://www.oyorooms.com/microsite/partner/?utm_source=partner"
	},
	{ name: "Polling", type: "SERVICE" },
	{ name: "FreeWiFi", type: "SERVICE" },
	{
		name: "Portea",
		type: "PWA",
		url: "https://lp.portea.com/gnrc-ofr-rmz200/"
	},
	{ name: "RXDX", type: "PWA", url: "https://mex.smartenspaces.com/medical/" },
	{ name: "Redbus", type: "NATIVE" },
	{ name: "Samsung", type: "PWA", url: "https://www.samsung.com/in/store/rmz" },
	{ name: "Swiggy", type: "SWIGGY" },
	{
		name: "SwitchMe",
		type: "PWA",
		url: "https://www.switchme.in/partner-landing/utm_source=rmz"
	},
	{
		name: "The Good Bowl",
		type: "PWA",
		url:
			"https://www.thegoodbowl.co/?utm_source=MEX&utm_medium=MEXmedia&utm_campaign=MEXoffers"
	},
	{ name: "Shuttle", type: "SERVICE" },
	{ name: "Traffic", type: "SERVICE" },
	{ name: "VMS", type: "SERVICE" },
	{ name: "Hungerbox", type: "PWA" },
	{
		name: "Wakefit",
		type: "PWA",
		url:
			"https://www.wakefit.co/?utm_source=RMZEco&utm_medium=workplace&utm_campaign=Partnership"
	},
	{
		name: "Quikr",
		type: "PWA",
		url:
			"https://www.athomediva.com/?utm_source=RMZ&utm_medium=mex&utm_campaign=Coupon1000"
	},
	{
		name: "Instacar",
		type: "PWA",
		url: "http://bit.ly/InstacarRMZ1"
	},
	{
		name: "Savaari",
		type: "PWA",
		url:
			"https://www.savaari.com/?utm_source=rmzcorp&utm_campaign=rmzcorp&utm_medium=rmz_banner"
	},
	{
		name: "Rentomojo",
		type: "PWA",
		url: "https://ws3q.app.link/fNm9TyqSr1?%243p=a_custom_721331162924082765"
	},
	{
		name: "Docsapp",
		type: "PWA",
		url: "https://m.docsapp.in/"
	},
	{ name: "Access", type: "NATIVE" },
	{ name: "CampusAssist", type: "NATIVE" }
];
