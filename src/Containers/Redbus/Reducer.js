import { combineReducers } from "redux";
import { reducer as searchBus } from "./Containers/SearchBus/Reducer";
import { reducer as selectBus } from "./Containers/SelectBus/Reducer";
import { reducer as selectSeat } from "./Containers/SelectSeat/Reducer";
import { reducer as travellerDetail } from "./Containers/TravellerDetail/Reducer";
import { reducer as ticketDetail } from "./Containers/TicketDetail/Reducer";
import { REDBUS_NO_NETWORK } from "./Saga";

const initialState = {
	noNetworkError: null
};

const noNetwork = (state = initialState, action) => {
	switch (action.type) {
		case REDBUS_NO_NETWORK:
			return {
				...state,
				noNetworkError: action.payload
			};
		default:
			return state;
	}
};
const RedbusReducer = combineReducers({
	searchBus: searchBus,
	selectBus: selectBus,
	selectSeat: selectSeat,
	travellerDetail: travellerDetail,
	ticketDetail: ticketDetail,
	redbusNoNetwork: noNetwork
});

export default RedbusReducer;
