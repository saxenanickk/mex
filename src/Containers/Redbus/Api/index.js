import I18n from "i18n-js";
import {
	NO_NETWORK_ERROR,
	NO_NETWORK_FOUND,
	RESPONSE_NOT_OK
} from "../Constants";
import Config from "react-native-config";

const { SERVER_BASE_URL_REDBUS } = Config;

class RedbusApi {
	constructor() {
		console.log("Redbus Api instantiated");
	}

	getAutoCompleteQuery(params) {
		const APP_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_REDBUS + "/autoComplete?query=" + params.query, {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": APP_TOKEN,
						"Accept-Language": I18n.currentLocale()
					}
				})
					.then(response => {
						console.log("response of autocomplete is==>", response);
						if (response.ok) {
							response
								.json()
								.then(res => {
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						// no network error
						resolve({
							error: NO_NETWORK_FOUND,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	getAllAvailableBus(params) {
		let APP_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_REDBUS +
						"/availableTrips?source=" +
						params.source +
						"&destination=" +
						params.destination +
						"&doj=" +
						params.doj,
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": APP_TOKEN,
							"Accept-Language": I18n.currentLocale()
						}
					}
				)
					.then(response => {
						console.log("response of available bus is==>", response);
						if (response.ok) {
							response
								.json()
								.then(res => {
									console.log("response of available bus is==>", res);
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						// no network error
						resolve({
							error: NO_NETWORK_FOUND,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	getSeatLayout(params) {
		let APP_TOKEN = params.appToken;
		console.log("params in seat layout", params);
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_REDBUS + "/tripDetails?id=" + params.tripId, {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": APP_TOKEN,
						"Accept-Language": I18n.currentLocale()
					}
				})
					.then(response => {
						if (response.ok) {
							response
								.json()
								.then(res => {
									console.log("response of seat layout is==>", res);
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						// no network error
						resolve({
							error: NO_NETWORK_FOUND,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	getBoardingPoint(params) {
		let APP_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_REDBUS +
						"/boardingPoint?id=" +
						params.bpId +
						"&tripId=" +
						params.tripId,
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": APP_TOKEN,
							"Accept-Language": I18n.currentLocale()
						}
					}
				)
					.then(response => {
						if (response.ok) {
							response
								.json()
								.then(res => {
									console.log("response of boarding point is==>", res);
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						// no network error
						resolve({
							error: NO_NETWORK_FOUND,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	blockTicket(params) {
		let APP_TOKEN = params.appToken;
		console.log(
			"params in block ticket",
			JSON.stringify(params.blockTicketParams)
		);
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_REDBUS + "/blockTicket", {
					method: "POST",
					body: JSON.stringify(params.blockTicketParams),
					headers: {
						"X-ACCESS-TOKEN": APP_TOKEN,
						"Accept-Language": I18n.currentLocale(),
						"Content-Type": "application/json"
					}
				})
					.then(response => {
						console.log("response of block ticket is==>", response);
						if (response.ok) {
							response
								.json()
								.then(res => {
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						resolve({
							error: NO_NETWORK_FOUND,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	bookTicket(params) {
		let APP_TOKEN = params.appToken;
		console.log(
			"params in book ticket",
			JSON.stringify(params.bookTicketParams)
		);
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_REDBUS +
						"/bookTicket?blockKey=" +
						params.blockKey +
						"&payment_id=" +
						params.paymentId,
					{
						method: "POST",
						body: JSON.stringify(params.bookTicketParams),
						headers: {
							"X-ACCESS-TOKEN": APP_TOKEN,
							"Accept-Language": I18n.currentLocale(),
							"Content-Type": "application/json"
						}
					}
				)
					.then(response => {
						console.log("response of book ticket is==>", response);
						if (response.ok) {
							response
								.json()
								.then(res => {
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						resolve({
							error: NO_NETWORK_FOUND,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	getFareBreakup(params) {
		let APP_TOKEN = params.appToken;
		return new Promise(function(resolve, reject) {
			try {
				fetch(
					SERVER_BASE_URL_REDBUS +
						"/rtcfarebreakup?blockKey=" +
						params.blockKey,
					{
						method: "GET",
						headers: {
							"X-ACCESS-TOKEN": APP_TOKEN,
							"Accept-Language": I18n.currentLocale(),
							"Content-Type": "application/json"
						}
					}
				)
					.then(response => {
						console.log("response of fare breakup is==>", response);
						if (response.ok) {
							response
								.json()
								.then(res => {
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						reject(error);
					});
			} catch (error) {
				reject(error);
			}
		});
	}

	getTicketDetail(params) {
		let APP_TOKEN = params.appToken;
		console.log(
			"params in  ticket detail",
			JSON.stringify(params.bookTicketParams)
		);
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_REDBUS + "/ticket?tin=" + params.tin, {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": APP_TOKEN,
						"Accept-Language": I18n.currentLocale(),
						"Content-Type": "application/json"
					}
				})
					.then(response => {
						console.log("response of  ticket detail is==>", response);
						if (response.ok) {
							response
								.json()
								.then(res => {
									resolve(res);
								})
								.catch(error => {
									reject(error);
								});
						} else {
							reject(new Error(RESPONSE_NOT_OK));
						}
					})
					.catch(error => {
						resolve({
							error: NO_NETWORK_FOUND,
							errorCode: NO_NETWORK_ERROR
						});
					});
			} catch (error) {
				reject(error);
			}
		});
	}
}

export default new RedbusApi();
