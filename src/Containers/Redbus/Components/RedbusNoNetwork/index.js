import React from "react";
import { Modal, TouchableOpacity, Dimensions } from "react-native";
import { Icon } from "../../../../Components";
import { RedbusTextRegular, RedbusTextMedium } from "../RedbusText";
import I18n from "../../Assets/Strings/i18n";
import styles from "./style";

const { height } = Dimensions.get("window");

const RedbusNoNetwork = props => {
	return (
		<Modal
			transparent={true}
			visible={props.visible}
			hardwareAccelerated={true}
			onRequestClose={() => props.close()}>
			<TouchableOpacity style={styles.container} onPress={() => props.close()}>
				<TouchableOpacity style={styles.centerContainer} activeOpacity={1}>
					<Icon
						iconType={"material"}
						iconSize={height / 18}
						iconName={"signal-wifi-off"}
						iconColor={"#000"}
					/>
					<RedbusTextRegular style={styles.reconnectMessage}>
						{I18n.t("no_internet")}
					</RedbusTextRegular>
					<TouchableOpacity onPress={props.onRetryPress}>
						<RedbusTextMedium style={styles.retryMessage}>
							{props.retryMessage}
						</RedbusTextMedium>
					</TouchableOpacity>
				</TouchableOpacity>
			</TouchableOpacity>
		</Modal>
	);
};

RedbusNoNetwork.defaultProps = {
	retryMessage: "RETRY",
	close: function() {
		console.log("add close method");
	}
};

export default RedbusNoNetwork;
