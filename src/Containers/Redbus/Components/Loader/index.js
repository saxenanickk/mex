import React from "react";
import { View, Image } from "react-native";
import {
	MULTIMEDIA,
	RESTSTOP,
	LIVETRACK,
	REDDEALS
} from "../../Assets/Img/Image";
import styles from "./style";

export default class Loader extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			num: 1,
			image: LIVETRACK
		};
	}

	componentDidMount() {
		let self = this;
		this.interval = setInterval(function() {
			switch (self.state.num % 4) {
				case 0:
					self.setState({ num: self.state.num + 1, image: MULTIMEDIA });
					break;
				case 1:
					self.setState({ num: self.state.num + 1, image: RESTSTOP });
					break;
				case 2:
					self.setState({ num: self.state.num + 1, image: REDDEALS });
					break;
				case 3:
					self.setState({ num: self.state.num + 1, image: LIVETRACK });
					break;
				default:
					self.setState({ num: self.state.num + 1, image: MULTIMEDIA });
					break;
			}
		}, 400);
	}

	shouldComponentUpdate(props, state) {
		return true;
	}
	componentWillUnmount() {
		console.log("clear interval");
		clearInterval(this.interval);
	}
	render() {
		return (
			<View style={styles.container}>
				<Image
					style={styles.image}
					source={this.state.image}
					resizeMode={"contain"}
				/>
			</View>
		);
	}
}
