import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: { justifyContent: "center", alignItems: "center", flex: 1 },
	image: {
		width: width / 4,
		height: height / 4
	}
});

export default styles;
