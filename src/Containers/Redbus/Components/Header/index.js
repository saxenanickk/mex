import React from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import Icon from "../../../../Components/Icon";
import styles from "./style";

const { width, height } = Dimensions.get("window");
export default class Header extends React.Component {
	render() {
		return (
			<View style={[styles.header, this.props.styles]}>
				{this.props.goBack && (
					<TouchableOpacity
						style={[styles.goBack, this.props.goBackStyle]}
						onPress={() => this.props.callOnBack()}>
						<Icon
							iconType={"material"}
							iconSize={this.props.iconHeight}
							iconName={"arrow-back"}
							iconColor={"#ffffff"}
						/>
					</TouchableOpacity>
				)}
				{this.props.children}
			</View>
		);
	}
}

Header.defaultProps = {
	goBack: false,
	iconHeight: height / 24,
	callOnBack: function() {
		console.log("implement go back");
	}
};
