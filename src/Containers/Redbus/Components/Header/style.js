import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	header: {
		width: width,
		height: height / 13.33,
		flexDirection: "row",
		justifyContent: "space-between",
		backgroundColor: "#C13D4A",
		paddingHorizontal: width / 26.3,
		alignItems: "center",
		elevation: 5
	},
	goBack: {
		width: width / 8,
		height: height / 13.3,
		justifyContent: "center",
		alignItems: "flex-start"
	}
});

export default styles;
