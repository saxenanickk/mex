import React from "react";
import { View } from "react-native";
import Icon from "../../../../Components/Icon";
import styles from "./style";

const CheckBox = props => {
	return (
		<View
			style={[
				styles.container,
				{
					borderColor: props.value !== true ? "black" : props.checkColor,
					backgroundColor: props.value !== true ? "#fff" : props.checkColor,
					borderWidth: props.width / 10,
					width: props.width,
					height: props.height,
					borderRadius: props.borderRadius
				}
			]}>
			{props.value === true && (
				<Icon
					iconType={"ionicon"}
					iconName={"ios-checkmark"}
					iconSize={2 * props.width}
					iconColor={"#fff"}
				/>
			)}
		</View>
	);
};

export default CheckBox;
