import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	counter: {
		width: width / 4,
		paddingHorizontal: width / 40,
		paddingVertical: width / 40,
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center"
	},
	text: {
		fontSize: height / 50,
		color: "#fff"
	}
});

export default styles;
