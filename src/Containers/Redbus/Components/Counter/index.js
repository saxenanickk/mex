/* eslint-disable radix */
import React from "react";
import PropTypes from "prop-types";
import { View } from "react-native";
import { RedbusTextRegular } from "../RedbusText";
import styles from "./style";

export default class Counter extends React.Component {
	/**
	 * maxTime an minTime are in seconds
	 */
	static propTypes = {
		maxTime: PropTypes.number,
		minTime: PropTypes.number
	};

	static defaultProps = {
		maxTime: 60,
		minTime: 0
	};
	constructor(props) {
		super(props);
		this.state = {
			maxTime: props.maxTime
		};
	}
	componentDidMount() {
		let self = this;
		this.interval = setInterval(function() {
			if (self.props.minTime < self.state.maxTime) {
				self.setState({
					maxTime: self.state.maxTime - 1
				});
			}
		}, 1000);
	}
	getMinute() {
		let minute = this.state.maxTime / 60;
		if (minute < 10) {
			return "0" + parseInt(minute);
		} else {
			return parseInt(minute);
		}
	}

	getSeconds() {
		let seconds = this.state.maxTime % 60;
		if (seconds < 10) {
			return "0" + seconds;
		} else {
			return seconds;
		}
	}

	componentWillUnmount() {
		clearInterval(this.interval);
	}
	render() {
		return (
			<View style={[styles.counter, this.props.counterStyle]}>
				<RedbusTextRegular style={[styles.text, this.props.counterTextStyles]}>
					{this.getMinute()}
				</RedbusTextRegular>
				<RedbusTextRegular style={[styles.text, this.props.counterTextStyles]}>
					{":"}
				</RedbusTextRegular>
				<RedbusTextRegular style={[styles.text, this.props.counterTextStyles]}>
					{this.getSeconds()}
				</RedbusTextRegular>
			</View>
		);
	}
}
