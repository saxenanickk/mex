export const REDBUSSPLASH = require("./red_bus.png");
export const REDBUSICON = require("./red_bus_icon.png");
export const VERTSWAP = require("./vert_swap.png");
export const MULTIMEDIA = require("./multimedia.png");
export const REDDEALS = require("./red_deals.png");
export const RESTSTOP = require("./rest_stop.png");
export const LIVETRACK = require("./live_tracking.png");
export const SERVERERROR = require("./server_error.png");
export const FILTER = require("./filter.png");
export const BUSICON = require("./bus_icon.png");
export const CITYICON = require("./city_icon.png");
export const CITYSEARCH = require("./city_search.png");
export const FILTERBUS = require("./filter_bus.png");
export const RED_DEALS = require("./red_deals.png");
export const CANCELLABLE = require("./cancellable.png");
export const MTICKET = require("./mticket.png");
export const MORNING = require("./morning.png");
export const AFTERNOON = require("./afternoon.png");
export const NIGHT = require("./night.png");
export const DAWN = require("./dawn.png");
export const SEATER = require("./seater.png");
export const SEATERF = require("./seater_f.png");
export const SLEEPER = require("./sleeper.png");
export const SLEEPERF = require("./sleeper_f.png");
export const HSLEEPER = require("./hsleeper.png");
export const HSLEEPERF = require("./hsleeper_f.png");
export const DRIVER = require("./driver.png");
export const TRACK = require("./track.png");
export const RATING = require("./rating.png");
export const FILTERSELECTED = require("./filter_selected.png");
