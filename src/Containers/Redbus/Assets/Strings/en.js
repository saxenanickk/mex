import * as I18n from "../../../../Assets/strings/i18n";

export default {
	/**
	 * Redbus App Strings
	 */
	...I18n.default.translations.en,
	go_back_while_booking:
		"We have already blocked the selected seats for you." +
		"You need to select different seats if you go back.\n\n" +
		"Note:Previously selected seats would be available after a while.",
	time_out_while_booking:
		"Your booking time has expired. Please select " +
		"your seats again and complete your booking " +
		"within alloted time",
	out_of_time: "You ran out of time!",
	ok: "Ok",
	unable_to_book_ticket: "We are unable to book this ticket, please try again",
	to: "To",
	want_to_go_back: "Do you really want to go back",
	cancel: "Cancel",
	cancelled: "Cancelled",
	user_cancel_payment: "Payment cancelled by user.",
	hang_on: "Hang On",
	blocking_ticket: "Blocking ticket for you",
	booking_ticket: "Booking ticket for you",
	at: "at",
	boarding_point: "BOARDING POINT",
	dropping_point: "DROPPING POINT",
	passenger_details: "PASSENGER DETAILS",
	years: "years",
	fare_breakup: "FARE BREAKUP",
	base_fare: "Base Fare",
	bus_gst: "Bus Operator GST",
	total_fare: "Total Fare",
	total_pay: "Total Payable",
	book_ticket: "Book Ticket",
	error_primary_message: "Oops, there is an eror in booking ticket",
	error_secondary_message: "Please, try again later",
	go_home: "Go To Home",
	enter_city: "Enter City",
	from: "From",
	journey_date: "Journey Date",
	enter_city_name: "Enter a city name",
	no_result: "No Results Found",
	next: "Next",
	boarding: "BOARDING",
	dropping: "DROPPING",
	add_location: "Add Location",
	last_row_message: "Seats in the last row may not recline",
	ladies_seat_message:
		"You have selected a ladies seat. Please Ensure that you are booking for a female passenger.",
	unable_to_show_seat: "Unable to show seats, please try again",
	lower: "Lower",
	upper: "Upper",
	all: "All",
	seats: "Seats",
	rb_done: "DONE",
	fetching_ticket_detail: "Fetching ticket detail",
	confirmed: "Confirmed",
	boards: "BOARDS",
	travel: "TRAVEL",
	passengers: "PASSENGERS",
	seat: "SEAT",
	boarding_point_detail: "BOARDING POINT DETAILS",
	ticket_number: "TICKET NUMBER",
	total: "TOTAL",
	pnr_number: "PNR NUMBER",
	error_in_fetching_ticket_detail:
		"Oops, there is an eror in fetching ticket detail",
	reload: "Reload",
	primary_passenger: "Primary Passenger",
	co_passenger: "Co-Passenger",
	name: "Name",
	gender: "Gender",
	only_ladies_seat: "This is only ladies seat",
	male: "Male",
	female: "Female",
	rb_age: "Age",
	email_id: "Email Id",
	phone: "Phone",
	continue: "Continue",
	server_error: "Unable to retrieve trips, Try after sometime",
	operator_service_charge: "Operator Service Charge",
	today: "Today",
	tomorrow: "Tomorrow",
	enter_city_error: "Please enter City",
	sort_by: "SORT BY",
	ratings: "Ratings",
	departure: "Departure",
	fare: "Fare",
	bus_type: "BUS TYPE",
	ac: "AC",
	non_ac: "NON AC",
	seater: "SEATER",
	sleeper: "SLEEPER",
	travels: "Travels",
	boarding_points: "Boarding points",
	dropping_points: "Dropping points",
	amenities: "AMENITIES",
	live_tracking: "Live Tracking",
	red_deals: "Red Deals",
	high_rated_bus: "High Rated Buses",
	cancellable: "Cancellable",
	m_ticket: "M-ticket",
	am: "AM",
	pm: "PM",
	filter_by: "FILTER BY",
	only_show: "ONLY SHOW",
	filter_by_dep_time: "FILTER BY DEPARTURE TIME",
	apply: "APPLY",
	hours_before_travel: "hour(s) before travel",
	last: "Last",
	full_cancellation_charge: "hours(s) 100% cancellation charge",
	cancellation_fare_calculated:
		"Cancellation fare calculated on seat fare of ₹",
	hours: "hours",
	cancel_anytime_before: "Cancel anytime before",
	time_before_travel: "Time Before Travel",
	deductions: "Deductions",
	cancellation_rule_one:
		"Refundable amount is calculated based on scheduled bus departure time from the boarding time",
	cancellation_rule_two:
		"Partial Cancellation allowed for this ticket. Ticket cannot be cancelled after us departure time",
	cancellation_rule_three:
		"Ticket cannot be cancelled after scheduled bus departure time from the boarding point",
	enter_travels: "Enter a travels name",
	enter_boardingPoints: "Enter a boarding point",
	enter_droppingPoints: "Enter a dropping point",
	no_service_found: "No services Found",
	try_another_date: "Try another date",
	enter_origin: "Enter Origin",
	enter_destination: "Enter Destination",
	error: "Error",
	success: "Success",
	seat_count: {
		one: "Seat",
		other: "Seats"
	},

	/* new additions */
	no_internet: "You don't seem to have an active internet connection",
	travel_details: "TRAVEL DETAILS",
	filters: "Filters",
	clr_filters: "Clear Filters",
	ticket_details: "Ticket Details",
	bus_found: "Buses Found",
	max: "maximum",
	invalid_input: "Invalid input"
};
