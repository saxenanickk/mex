import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

export const indexStyles = StyleSheet.create({
	imageStyle: {
		width: width,
		height: height,
		justifyContent: "flex-end",
		paddingVertical: height / 30
	}
});
