import React, { Component } from "react";
import {
	View,
	Dimensions,
	StatusBar,
	ImageBackground,
	ProgressBarAndroid
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";

import {
	redbusSaveTravelDate,
	redbusSaveSource,
	redbusSaveDestination
} from "../SearchBus/Saga";
import { REDBUSSPLASH } from "../../Assets/Img/Image";
import { indexStyles as styles } from "./style";
import { CommonActions } from "@react-navigation/native";

const { width } = Dimensions.get("window");

class Splash extends Component {
	constructor(props) {
		super(props);
	}

	/**
	 * get source and destination from the async storage and store it in redux
	 */
	async getItemFromStore() {
		try {
			let value = JSON.parse(await AsyncStorage.getItem("redBus"));
			let currentDate = new Date();
			if (value !== null) {
				console.log("value is", value);
				this.props.dispatch(redbusSaveSource(value.source));
				this.props.dispatch(redbusSaveDestination(value.destination));
				if (new Date(value.date).getTime() < currentDate.getTime()) {
					this.props.dispatch(redbusSaveTravelDate(currentDate));
				} else {
					this.props.dispatch(redbusSaveTravelDate(new Date(value.date)));
				}
			} else {
				this.props.dispatch(redbusSaveTravelDate(currentDate));
			}
		} catch (error) {
			console.log("error:", error);
		}
	}

	componentDidMount() {
		this.getItemFromStore();
		setTimeout(() => {
			this.props.navigation.dispatch(
				CommonActions.reset({
					index: 0,
					routes: [{ name: "SearchBus" }]
				})
			);
		}, 2000);
	}

	render() {
		return (
			<View>
				<StatusBar backgroundColor="#9e3e47" barStyle="light-content" />
				<ImageBackground style={styles.imageStyle} source={REDBUSSPLASH}>
					<ProgressBarAndroid
						color={"#fff"}
						styleAttr={"Horizontal"}
						width={width}
					/>
				</ImageBackground>
			</View>
		);
	}
}

function mapStateToProps(state) {
	console.log(state);
	return {
		travelDate: state.redbus && state.redbus.searchBus.travelDate
	};
}

export default connect(
	mapStateToProps,
	null,
	null
)(Splash);

/**
 * layout.map(row => {
if(row.zIndex === "0"){
	if(lower[row.column] === undefined){

    lower[row.column] = []}
	lower[row.column].push(row.length)}
if(row.zIndex === "1"){
if(upper[row.column]=== undefined){
upper[row.column]=[]}
upper[row.column].push(row.length)}
})
 */
