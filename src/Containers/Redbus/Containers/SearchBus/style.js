import { StyleSheet, Dimensions, Platform } from "react-native";
import { font_two } from "../../../../Assets/styles";

const { width, height } = Dimensions.get("window");

export const indexStyles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#e9e9e9"
	},

	image: {
		width: width / 5,
		height: height / 15,
		justifyContent: "flex-start"
	},
	placeContainer: {
		width: width / 1.05,
		height: height / 4.6,
		backgroundColor: "#fff",
		justifyContent: "space-between",
		alignSelf: "center",
		marginTop: height / 50,
		paddingVertical: height / 120,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		borderRadius: 2
	},
	supportTextButton: {
		backgroundColor: "#ffffff",
		flexDirection: "row",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		paddingVertical: height / 90,
		borderRadius: width / 20,
		justifyContent: "flex-start",
		paddingHorizontal: width / 50,
		alignItems: "center",
		marginRight: width / 26.3
	},
	supportText: {
		color: "#c13d4a",
		width: width / 7
	},
	placeHolder: {
		width: width / 1.4,
		height: height / 12,
		// borderWidth: 1,
		// borderColor: "#000",
		paddingHorizontal: width / 50
	},
	secondPlaceHolder: {
		width: width / 1.2,
		height: height / 12,
		// borderWidth: 1,
		// borderColor: "#000",
		paddingHorizontal: width / 50,
		marginTop: -height / 100
	},
	primaryText: {
		fontSize: height / 55,
		color: "rgba(0,0,0,0.6)",
		marginLeft: width / 48
	},
	journeyDateText: {
		fontSize: height / 55,
		color: "rgba(0,0,0,0.6)"
	},
	secondaryText: {
		fontFamily: font_two,
		fontSize: height / 30,
		marginLeft: width / 40,
		color: "rgba(0,0,0,0.7)",
		width: width / 1.4
	},
	travel: {
		flexDirection: "row",
		height: height / 17.5,
		alignItems: "flex-end",
		width: width / 1.2,
		borderWidth: 0,
		borderColor: "#000"
	},
	middleBar: {
		flexDirection: "row",
		width: width / 1.1,
		height: height / 25,
		paddingHorizontal: width / 50,
		alignItems: "center",
		borderWidth: 0,
		borderColor: "#000"
	},
	swap: {
		width: width / 10,
		height: height / 15
	},
	dateContainer: {
		marginTop: height / 40,
		width: width / 1.05,
		height: height / 8,
		backgroundColor: "#fff",
		justifyContent: "space-between",
		flexDirection: "row",
		alignItems: "center",
		alignSelf: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		paddingHorizontal: width / 20,
		borderRadius: 2
	},
	button: {
		marginTop: height / 20,
		width: width / 5.8,
		height: width / 5.8,
		borderRadius: width / 10.6,
		backgroundColor: "#C13D4A",
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	date: {
		width: width / 2.3,
		height: height / 9,
		borderWidth: 0
	},
	dateType: {
		width: width / 3,
		height: height / 9
	},
	calendar: {
		flexDirection: "row",
		height: height / 11,
		alignItems: "center"
	},
	dateText: {
		fontSize: height / 25,
		marginLeft: width / 40,
		color: "rgba(0,0,0,0.9)"
	},
	miniInfo: {
		width: width / 10,
		alignItems: "center"
	},
	dayText: {
		fontSize: height / 68,
		color: "rgba(0,0,0,0.9)"
	},
	imageCity: {
		width: width / 10,
		height: width / 10
	},

	duration: {
		fontSize: height / 30,
		color: "#000"
	},
	durationView: {
		marginTop: height / 50
	}
});

/**
 * style for Search.js
 */

export const searchStyles = StyleSheet.create({
	noResult: {
		fontSize: height / 40,
		color: "#000",
		alignSelf: "center",
		marginTop: 0
	},
	text: {
		fontSize: height / 55,
		marginLeft: width / 30
	},
	list: {
		width: width,
		backgroundColor: "#fff",
		paddingHorizontal: width / 26.3,
		height: height / 15,
		flexDirection: "row",
		justifyContent: "flex-start",
		alignItems: "center"
	},
	header: {
		flex: 1,
		height: height / 20,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		alignContent: "center",
		borderBottomWidth: 1,
		borderBottomColor: "rgba(255,255,255,0.7)"
	},
	searchIcon: {
		paddingRight: width / 60
	},
	textInput: {
		flex: 1,
		height: height / 13.3,
		fontFamily: font_two,
		fontSize: height / 45,
		color: "rgba(255,255,255,0.7)"
	},
	imageCity: {
		width: width / 10,
		height: height / 25
	},
	headerView: {
		alignItems: "center",
		justifyContent: "space-between",
		alignContent: "center"
	},
	fullFlex: { flex: 1 },
	listEmptyView: {
		height: height / 2,
		alignItems: "center",
		justifyContent: "center"
	},
	listEmptyImage: {
		width: width / 2,
		height: height / 3,
		alignItems: "center",
		justifyContent: "center"
	},
	closeButton: {
		width: width / 15,
		height: height / 13.33,
		justifyContent: "center",
		alignItems: "flex-end"
	}
});
