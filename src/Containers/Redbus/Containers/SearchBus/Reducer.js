import {
	REDBUS_SAVE_AUTO_COMPLETE,
	REDBUS_SAVE_DESTINATION,
	REDBUS_SAVE_SOURCE,
	REDBUS_SWAP_CITY,
	REDBUS_SAVE_TRAVEL_DATE,
	REDBUS_SAVE_ALL_TRIP
} from "./Saga";
const initialState = {
	source: null,
	destination: null,
	travelDate: null,
	searchError: false,
	locations: null,
	today: "Today"
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case REDBUS_SAVE_AUTO_COMPLETE:
			return {
				...state,
				locations: action.payload
			};
		case REDBUS_SAVE_SOURCE:
			return {
				...state,
				source: action.payload
			};
		case REDBUS_SAVE_DESTINATION:
			return {
				...state,
				destination: action.payload
			};
		case REDBUS_SWAP_CITY:
			return {
				...state,
				destination: state.source,
				source: state.destination
			};
		case REDBUS_SAVE_TRAVEL_DATE:
			return {
				...state,
				travelDate: action.payload
			};
		default:
			return state;
	}
};
