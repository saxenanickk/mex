import React from "react";
import {
	View,
	Image,
	Dimensions,
	TouchableOpacity,
	TextInput,
	FlatList,
	Keyboard
} from "react-native";
import { connect } from "react-redux";
import { Header } from "../../Components";
import { CITYSEARCH, SERVERERROR } from "../../Assets/Img/Image";
import { Icon } from "../../../../Components";
import {
	redbusGetAutoComplete,
	redbusSaveBusAutoComplete,
	redbusSaveDestination,
	redbusSaveSource
} from "./Saga";
import { RedbusTextRegular, RedbusTextBold } from "../../Components/RedbusText";
import I18n from "../../Assets/Strings/i18n";
import { searchStyles as styles } from "./style";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";

const { width, height } = Dimensions.get("window");

/**
 * Component used to search city where redbus services are available
 */
class Search extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			searchText: ""
		};
	}

	/**
	 * call autocomplete query from the server
	 * @param  {[String]} text [text enetered by user to check redbus service]
	 */
	getAutoCompleteQuery = text => {
		GoAppAnalytics.trackWithProperties("search_city", { query: text });
		this.props.dispatch(
			redbusGetAutoComplete({
				appToken: this.props.appToken,
				query: text
			})
		);
		this.setState({
			searchText: text
		});
	};

	/**
	 * reset the enetered text to empty string
	 */
	resetText = () => {
		this.setState({ text: "", searchText: "" });
		this.props.dispatch(redbusSaveBusAutoComplete(null));
	};

	/**
	 * save the source city in the redux store
	 * @param  {[Object]} item [City information]
	 */
	saveSourceCity = item => {
		this.props.dispatch(redbusSaveSource(item));
	};

	/**
	 * save the destination city in the redux store
	 * @param  {[Object]} item [City information]
	 */
	saveDestinationCity = item => {
		this.props.dispatch(redbusSaveDestination(item));
	};

	componentWillUnmount() {
		this.props.dispatch(redbusSaveBusAutoComplete(null));
	}

	render() {
		return (
			<View style={{ flex: 1 }}>
				<Header
					style={styles.headerView}
					goBack={true}
					goBackStyle={{ width: width / 13 }}
					iconHeight={height / 38}
					callOnBack={() => this.props.navigation.goBack()}>
					<View style={styles.header}>
						<Icon
							style={styles.searchIcon}
							iconType={"material"}
							iconSize={height / 35}
							iconName={"search"}
							iconColor={"rgba(255,255,255,0.5)"}
						/>
						<TextInput
							underlineColorAndroid={"transparent"}
							style={styles.textInput}
							autoFocus={true}
							placeholder={I18n.t("enter_city_name")}
							placeholderTextColor={"rgba(255,255,255,0.5)"}
							onChangeText={text => this.getAutoCompleteQuery(text)}
							value={this.state.searchText}
							selectionColor={"rgba(255,255,255,0.5)"}
						/>
					</View>
					<TouchableOpacity
						onPress={() => this.resetText()}
						style={styles.closeButton}>
						{this.state.searchText.trim() !== "" && (
							<Icon
								iconType={"ionicon"}
								iconSize={height / 38}
								iconName={"ios-close-circle"}
								iconColor={"#fff"}
							/>
						)}
					</TouchableOpacity>
				</Header>
				{/* body section*/}
				<View style={styles.fullFlex}>
					{/* don't show list if there is no locations available*/}
					{this.props.locations !== null ? (
						<FlatList
							data={this.props.locations}
							keyExtractor={(item, index) => index.toString()}
							keyboardShouldPersistTaps={"always"}
							renderItem={({ item, index }) => {
								return (
									<TouchableOpacity
										style={styles.list}
										onPress={() => {
											Keyboard.dismiss();
											this.props.route.params.type === "source"
												? this.saveSourceCity(item)
												: this.saveDestinationCity(item);
											this.props.navigation.goBack();
										}}>
										<Image
											style={styles.imageCity}
											source={CITYSEARCH}
											resizeMode={"contain"}
										/>
										<RedbusTextRegular style={styles.text}>
											{item.city_name}
										</RedbusTextRegular>
									</TouchableOpacity>
								);
							}}
							ListEmptyComponent={() => {
								return (
									<View style={styles.listEmptyView}>
										<Image
											style={styles.listEmptyImage}
											source={SERVERERROR}
											resizeMode={"contain"}
										/>

										<RedbusTextBold style={styles.noResult}>
											{I18n.t("no_result")}
										</RedbusTextBold>
									</View>
								);
							}}
						/>
					) : null}
				</View>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		locations: state.redbus && state.redbus.searchBus.locations
	};
}
export default connect(mapStateToProps)(Search);
