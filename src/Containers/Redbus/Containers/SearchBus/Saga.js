import { takeLatest, put, call } from "redux-saga/effects";
import Api from "../../Api/";
import { NO_NETWORK_ERROR } from "../../Constants";
import { redbusNoNetwork } from "../../Saga";
export const REDBUS_GET_AUTO_COMPLETE = "REDBUS_GET_AUTO_COMPLETE";
export const REDBUS_SAVE_AUTO_COMPLETE = "REDBUS_SAVE_AUTO_COMPLETE";
export const REDBUS_SAVE_SEARCH_ERROR = "REDBUS_SAVE_SEARCH_ERROR";
export const REDBUS_SAVE_SOURCE = "REDBUS_SAVE_SOURCE";
export const REDBUS_SAVE_DESTINATION = "REDBUS_SAVE_DESTINATION";
export const REDBUS_SWAP_CITY = "REDBUS_SWAP_CITY";
export const REDBUS_SAVE_TRAVEL_DATE = "REDBUS_SAVE_TRAVEL_DATE";

//call Api to get the autocomplete query
export const redbusGetAutoComplete = payload => ({
	type: REDBUS_GET_AUTO_COMPLETE,
	payload
});

//save the autocomplete query result to the store
export const redbusSaveBusAutoComplete = payload => ({
	type: REDBUS_SAVE_AUTO_COMPLETE,
	payload
});

//update the error status while querying
export const redbusSaveSearchError = payload => ({
	type: REDBUS_SAVE_SEARCH_ERROR,
	payload
});

//save source city to the store
export const redbusSaveSource = payload => ({
	type: REDBUS_SAVE_SOURCE,
	payload
});

//save destination city to the store
export const redbusSaveDestination = payload => ({
	type: REDBUS_SAVE_DESTINATION,
	payload
});

//save the source and destination city
export const redbusSwapCity = payload => ({
	type: REDBUS_SWAP_CITY,
	payload
});

//save travel date to the store
export const redbusSaveTravelDate = payload => ({
	type: REDBUS_SAVE_TRAVEL_DATE,
	payload
});

export function* searchBusSaga(dispatch) {
	yield takeLatest(REDBUS_GET_AUTO_COMPLETE, handleGetBusAutoComplete);
}

//handles the autocomplete query saga
function* handleGetBusAutoComplete(action) {
	try {
		let response = yield call(Api.getAutoCompleteQuery, action.payload);
		console.log("response of auto complete", response);
		if (response.errorCode && response.errorCode === NO_NETWORK_ERROR) {
			// if there is no network error then show the error message
			yield put(
				redbusNoNetwork({
					actionType: action.type,
					payload: action.payload
				})
			);
			return;
		} else {
			if (response.success && response.success === 1) {
				//store the succesfully fetched auto complete query
				yield put(redbusSaveBusAutoComplete(response.data.cities));
			} else {
				//update the error status for auto complete query
				yield put(redbusSaveSearchError(true));
			}
		}
	} catch (error) {
		console.log("error=>", error);
		yield put(redbusSaveSearchError(true));
	}
}
