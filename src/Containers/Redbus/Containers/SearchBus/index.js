import React from "react";
import {
	View,
	Image,
	Dimensions,
	StatusBar,
	TouchableOpacity,
	Animated,
	Easing,
	BackHandler
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import { Header } from "../../Components";
import { REDBUSICON, VERTSWAP, CITYICON } from "../../Assets/Img/Image";
import {
	RedbusTextRegular,
	RedbusTextBold,
	RedbusTextMedium
} from "../../Components/RedbusText";
import { Icon, Seperator, GoToast, DatePicker } from "../../../../Components";
import { redbusSwapCity, redbusSaveTravelDate } from "./Saga";
import { DATE, DAY, MONTH, LOW_BLACK } from "../../Constants";
import I18n from "../../Assets/Strings/i18n";
import { indexStyles as styles } from "./style";
import Freshchat from "../../../../CustomModules/Freshchat";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";

const { width, height } = Dimensions.get("window");

/**
 * SearchBus Screen
 * functionality :
 * 1.search bus between source destination
 * 2.change date of the trip
 * 3.select source and destinaiton
 */
class SearchBus extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			rotateIcon: false,
			showDatePicker: false
		};
		this.spinValue = new Animated.Value(0);
		BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
	}

	onBackPress = () => {
		this.props.navigation.goBack();
	};

	/**
	 * swap the source and destination city
	 */
	swapElements() {
		this.setState({
			rotateIcon: !this.state.rotateIcon
		});
		this.props.dispatch(redbusSwapCity());
	}

	shouldComponentUpdate(props, state) {
		//if rotateIcon value changed the rotate te swap icon
		if (state.rotateIcon !== this.state.rotateIcon) {
			Animated.timing(this.spinValue, {
				toValue: state.rotateIcon === true ? 1 : 0,
				duration: 500,
				easing: Easing.linear,
				useNativeDriver: true
			}).start();
			return true;
		}
		return true;
	}

	/**
	 * calculate duration between selected date of trip and current date
	 * @return {[String]}
	 */
	getDuration = () => {
		let selected = new Date(this.props.travelDate.toDateString()).getTime();
		let now = new Date();
		let today = new Date(now.toDateString()).getTime();
		if (selected - today == 0) {
			return I18n.t("today");
		} else if (selected - today >= 86400000 && selected - today < 172800000) {
			return I18n.t("tomorrow");
		} else if (selected - today > 172800000) {
			return "";
		}
	};

	/**
	 * get month,day,year from the date according to the index
	 * @param  {[Integer]} index [index of which date information is to be fetched]
	 * @return {[String]}       [$DD or $MM or $YYYY]
	 */
	getDateDetail = index => {
		let date = this.props.travelDate.toDateString().split(" ");
		return date[index];
	};

	/**
	 * navigates to SelectBus Screen
	 */
	searchBus = () => {
		//source or destination city is null then don't navigate
		if (this.props.source === null || this.props.destination === null) {
			GoToast.show(I18n.t("enter_city_error"), I18n.t("error"), "LONG");
		} else {
			this.props.navigation.navigate("SelectBus");
		}
	};

	componentWillUnmount() {
		AsyncStorage.setItem(
			"redBus",
			JSON.stringify({
				source: this.props.source,
				destination: this.props.destination,
				date: this.props.travelDate
			})
		);
	}
	render() {
		//handles the spin animation of the rotate icon
		const spin = this.spinValue.interpolate({
			inputRange: [0, 0.5, 1],
			outputRange: ["0deg", "90deg", "180deg"]
		});
		//handles the opacity of source and destination city text
		const opacityValue = this.spinValue.interpolate({
			inputRange: [0, 0.5, 1],
			outputRange: [1, 0, 1]
		});
		return (
			<View style={styles.container}>
				<StatusBar backgroundColor="#9e3e47" barStyle="light-content" />
				<Header goBack={true} callOnBack={this.onBackPress}>
					<Image
						style={styles.image}
						source={REDBUSICON}
						resizeMode={"contain"}
					/>
					<TouchableOpacity
						style={styles.supportTextButton}
						onPress={() => Freshchat.launchSupportChat()}>
						<RedbusTextMedium style={styles.supportText} numberOfLines={1}>
							{I18n.t("app_support")}
						</RedbusTextMedium>
						<Icon
							iconType={"material"}
							iconSize={height / 40}
							iconName={"headset"}
							iconColor={"#c13d4a"}
						/>
					</TouchableOpacity>
				</Header>
				{/* body Section*/}
				<View>
					<View style={styles.placeContainer}>
						<TouchableOpacity
							style={styles.placeHolder}
							onPress={() =>
								this.props.navigation.navigate("SearchCity", {
									type: "source"
								})
							}>
							<RedbusTextBold style={styles.primaryText}>
								{I18n.t("from")}
							</RedbusTextBold>
							<View style={styles.travel}>
								<Image
									style={styles.imageCity}
									source={CITYICON}
									resizeMode={"contain"}
								/>
								<Animated.Text
									numberOfLines={1}
									style={[styles.secondaryText, { opacity: opacityValue }]}>
									{this.props.source === null
										? I18n.t("enter_origin")
										: this.props.source.city_name}
								</Animated.Text>
							</View>
						</TouchableOpacity>
						<View style={styles.middleBar}>
							<Seperator
								width={width / 1.35}
								height={height / 700}
								color={"#cccccc"}
							/>
							<TouchableOpacity onPress={() => this.swapElements()}>
								<Animated.Image
									style={[styles.swap, { transform: [{ rotate: spin }] }]}
									source={VERTSWAP}
									resizeMode={"contain"}
								/>
							</TouchableOpacity>
							<Seperator
								width={width / 13}
								height={height / 700}
								color={"#cccccc"}
							/>
						</View>
						<TouchableOpacity
							style={styles.secondPlaceHolder}
							onPress={() =>
								this.props.navigation.navigate("SearchCity", {
									type: "destination"
								})
							}>
							<RedbusTextBold style={styles.primaryText}>
								{I18n.t("to")}
							</RedbusTextBold>
							<View style={styles.travel}>
								<Image
									style={styles.imageCity}
									source={CITYICON}
									resizeMode={"contain"}
								/>
								<Animated.Text
									numberOfLines={1}
									style={[styles.secondaryText, { opacity: opacityValue }]}>
									{this.props.destination === null
										? I18n.t("enter_destination")
										: this.props.destination.city_name}
								</Animated.Text>
							</View>
						</TouchableOpacity>
					</View>
					<TouchableOpacity
						style={styles.dateContainer}
						onPress={() => this.setState({ showDatePicker: true })}>
						<View style={styles.date}>
							<RedbusTextBold style={styles.journeyDateText}>
								{I18n.t("journey_date")}
							</RedbusTextBold>
							<View style={styles.calendar}>
								<Icon
									iconType={"font_awesome"}
									iconSize={height / 33}
									iconName={"calendar"}
									iconColor={LOW_BLACK}
								/>
								<RedbusTextRegular style={styles.dateText}>
									{this.getDateDetail(DATE)}
								</RedbusTextRegular>
								<View style={styles.miniInfo}>
									<RedbusTextRegular style={styles.dayText}>
										{this.getDateDetail(DAY)}
									</RedbusTextRegular>
									<RedbusTextRegular style={styles.dayText}>
										{this.getDateDetail(MONTH)}
									</RedbusTextRegular>
								</View>
							</View>
						</View>
						<View style={styles.durationView}>
							<RedbusTextMedium style={styles.duration}>
								{this.getDuration()}
							</RedbusTextMedium>
						</View>
					</TouchableOpacity>
					<TouchableOpacity
						style={styles.button}
						onPress={() => this.searchBus()}>
						<Icon
							iconType={"font_awesome"}
							iconSize={height / 15}
							iconName={"angle-right"}
							iconColor={"#fff"}
						/>
					</TouchableOpacity>
				</View>
				{this.state.showDatePicker ? (
					<DatePicker
						date={new Date()}
						minimumDate={new Date()}
						onDateChange={date => {
							console.log(date);
							if (date) {
								const { month, day, year } = date;
								let selectedDate = new Date(month + 1 + "/" + day + "/" + year);
								GoAppAnalytics.trackWithProperties("select_journey_date", {
									date: selectedDate.toDateString()
								});
								this.props.dispatch(redbusSaveTravelDate(selectedDate));
							}
							this.setState({ showDatePicker: false });
						}}
					/>
				) : null}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		source: state.redbus && state.redbus.searchBus.source,
		destination: state.redbus && state.redbus.searchBus.destination,
		travelDate: state.redbus && state.redbus.searchBus.travelDate
	};
}
export default connect(mapStateToProps)(SearchBus);
