/* eslint-disable radix */
import React from "react";
import {
	View,
	Image,
	Dimensions,
	ScrollView,
	TouchableOpacity,
	LayoutAnimation,
	NativeModules,
	FlatList,
	Alert,
	BackHandler,
	Platform
} from "react-native";
import { connect } from "react-redux";
import {
	RedbusTextRegular,
	RedbusTextMedium
} from "../../Components/RedbusText";
import { Header, Counter } from "../../Components";

import { SERVERERROR } from "../../Assets/Img/Image";
import { Icon, ProgressScreen, GoToast } from "../../../../Components";
import {
	redbusSaveSelectedSeat,
	redbusSaveSeatLayout,
	redbusGetSeatLayout
} from "../SelectSeat/Saga";
import {
	redbusGetBlockTicket,
	redbusSaveBlockTicket,
	redbusSaveBlockTicketError,
	redbusGetBookTicket,
	redbusSaveBookTicket,
	redbusSaveBookTicketError
} from "../TravellerDetail/Saga";
import { DATE, MONTH, LOW_BLACK } from "../../Constants";
import I18n from "../../Assets/Strings/i18n";
import Razorpay from "../../../../CustomModules/RazorPayModule";
import { indexStyles as styles } from "./style";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import { StackActions, CommonActions } from "@react-navigation/native";

const { width, height } = Dimensions.get("window");
const { UIManager } = NativeModules;
const GO_BACK_MESSAGE = I18n.t("go_back_while_booking");
const TIME_OUT_MESSAGE = I18n.t("time_out_while_booking");

const BLOCK_TICKCET = 1;
const BOOK_TICKET = 2;
const NO_LOADING = 0;

/**
 * Screen for ticket booking confirmation
 */
class BookTicket extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isOpen: false,
			isLoading: BLOCK_TICKCET
		};
	}

	componentDidMount() {
		if (Platform.OS === "android") {
			UIManager.setLayoutAnimationEnabledExperimental &&
				UIManager.setLayoutAnimationEnabledExperimental(true);
		}

		this.props.dispatch(
			redbusGetBlockTicket({
				appToken: this.props.appToken,
				blockTicketParams: {
					availableTripId: this.props.selectedBus.id,
					boardingPointId: this.props.selectedBoardingPoint.bpId,
					source: this.props.source.redbus_city_id,
					destination: this.props.destination.redbus_city_id,
					inventoryItems: this.props.traveller,
					bpdpseatlayout: this.props.selectedBus.bpDpSeatLayout
				}
			})
		);

		/**
		 * Sets an interval of 7 minute to book the ticket
		 * The ticket is blocked for 7 minute after then timeput
		 * message is displayed
		 */
		this.timeOut = setTimeout(() => {
			Alert.alert(
				I18n.t("out_of_time"),
				TIME_OUT_MESSAGE,
				[
					{
						text: I18n.t("ok"),
						onPress: () => {
							GoAppAnalytics.trackWithProperties("cancel_ticket", {
								blockKey: this.props.blockKey,
								cancelUser: false,
								timeout: true
							});
							this.goBackToSeat();
						}
					}
				],
				{ cancelable: false }
			);
		}, 420000);

		this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
			this.closeBookTicket();
			return true;
		});
	}
	shouldComponentUpdate(props, state) {
		//display error message if there is no block key
		if (
			props.blockKeyError === true &&
			props.blockKeyError !== this.props.blockKeyError
		) {
			GoToast.show(I18n.t("unable_to_book_ticket"), I18n.t("error"), "LONG");
			this.goBackToSeat();
		}

		//don't show progress screen if there is block key
		if (props.blockKey !== null && props.blockKey !== this.props.blockKey) {
			this.setState({ isLoading: NO_LOADING });
		}

		//navigate to ticket detail page if tinNUmber is not null
		if (props.tinNumber !== null && props.tinNumber !== this.props.tinNumber) {
			this.setState({ isLoading: NO_LOADING });
			clearTimeout(this.timeOut);
			this.props.navigation.navigate("TicketDetail");
		}

		if (
			props.bookTicketError !== null &&
			props.bookTicketError === true &&
			this.props.bookTicketError !== props.bookTicketError
		) {
			this.setState({ isLoading: NO_LOADING });
		}
		return true;
	}

	/**
	 * Get the name of source to destination
	 * @return {[String]} [$source to $destination]
	 */
	getTravelCity = () => {
		return (
			this.props.source.city_name +
			" " +
			I18n.t("to") +
			" " +
			this.props.destination.city_name
		);
	};

	componentWillUnmount() {
		this.props.dispatch(redbusSaveBlockTicket(null));
		this.props.dispatch(redbusSaveBookTicketError(false));
		this.props.dispatch(redbusSaveBlockTicketError(false));
		this.props.dispatch(redbusSaveBookTicket(null));
		clearTimeout(this.timeOut);
		this.backHandler.remove();
	}

	/**
	 * function executed while going back due to timeout or due to user
	 * go back to SelectSeat page
	 */
	async goBackToSeat() {
		try {
			await this.props.dispatch(redbusSaveSeatLayout(null));
			this.props.dispatch(redbusSaveSelectedSeat(null));
			this.props.dispatch(
				redbusGetSeatLayout({
					appToken: this.props.appToken,
					tripId: this.props.selectedBus.id
				})
			);
			this.props.navigation.dispatch(StackActions.pop(3));
		} catch (error) {
			console.log(error);
		}
	}

	/**
	 * Shows alert box when user presses the back button
	 */
	closeBookTicket = () => {
		Alert.alert(
			I18n.t("want_to_go_back"),
			GO_BACK_MESSAGE,
			[
				{
					text: I18n.t("cancel"),
					onPress: () => {
						console.log("cancel pressed");
					},
					style: "cancel"
				},
				{
					text: I18n.t("ok"),
					onPress: () => {
						GoAppAnalytics.trackWithProperties("cancel_ticket", {
							blockKey: this.props.blockKey,
							cancelUser: true,
							timeout: false
						});
						this.goBackToSeat();
					}
				}
			],
			{ cancelable: false }
		);
	};

	/**
	 * open RazorPay for payment of the ticket
	 * if successfull payment then fetch ticket detail
	 * else go back to SelectSeat page
	 */
	makePayment = () => {
		Razorpay({
			color: "#C13D4a",
			description: this.getTravelCity(),
			amount: this.getTotalFare()
		})
			.then(response => {
				response.razorpay_payment_id;
				console.log("payment id==>", response.razorpay_payment_id);
				this.setState({ isLoading: BOOK_TICKET });
				try {
					GoAppAnalytics.trackChargeWithProperties(
						parseInt(this.getTotalFare()),
						response.razorpay_payment_id
					);
				} catch (error) {
					console.log("Error GoAppAnalytics.trackChargeWithProperties", error);
				}
				GoAppAnalytics.trackWithProperties("book_ticket", {
					blockKey: this.props.blockKey,
					paymentId: response.razorpay_payment_id
				});
				this.props.dispatch(
					redbusGetBookTicket({
						appToken: this.props.appToken,
						blockKey: this.props.blockKey,
						paymentId: response.razorpay_payment_id,
						bookTicketParams: {
							availableTripId: this.props.selectedBus.id,
							boardingPointId: this.props.selectedBoardingPoint.bpId,
							source: this.props.source.redbus_city_id,
							destination: this.props.destination.redbus_city_id,
							inventoryItems: this.props.traveller
						}
					})
				);
			})
			.catch(error => {
				console.log("RazorPay Error: ", error);
				this.goBackToSeat();
				GoToast.show(error.description, I18n.t("error"));
				if (error.code === 0) {
					Alert.alert(
						I18n.t("cancelled"),
						I18n.t("user_cancel_payment"),
						[
							{
								text: I18n.t("ok"),
								onPress: () => console.log("payment cancelled by user")
							}
						],
						{ cancelable: false }
					);
				}
			});
	};

	/**
	 * get the total fare for a
	 * @return {[Number]} [$totalFare]
	 */
	getTotalFare = () => {
		let fare = 0;
		this.props.traveller.map(passenger => {
			fare += parseInt(passenger.fare);
		});
		return fare;
	};
	/**
	 * render the base Fare section
	 * @return {[Component]} [renders the base fare]
	 */
	renderFare = () => {
		let fare = 0;
		this.props.traveller.map(passenger => {
			fare += parseInt(passenger.baseFare);
		});
		return (
			<View style={styles.fareBreakup}>
				<RedbusTextRegular style={styles.secondaryText}>
					{I18n.t("base_fare")}
				</RedbusTextRegular>
				<RedbusTextRegular style={styles.secondaryText}>
					{"₹" + I18n.toNumber(fare, { precision: 0 })}
				</RedbusTextRegular>
			</View>
		);
	};

	/**
	 * render the GST added on the trip
	 * @return {[Component]} [renders the GST]
	 */
	renderGST = () => {
		let gst = 0;
		this.props.traveller.map(passenger => {
			gst += parseInt(passenger.serviceTaxAbsolute);
		});
		if (gst > 0) {
			return (
				<View style={styles.fareBreakup}>
					<RedbusTextRegular style={styles.secondaryText}>
						{I18n.t("bus_gst")}
					</RedbusTextRegular>
					<RedbusTextRegular style={styles.secondaryText}>
						{"₹" + gst}
					</RedbusTextRegular>
				</View>
			);
		}
	};
	/**
	 * renders the operator service charge
	 * @return {[Number]}
	 */
	renderOperatorService = () => {
		let operatorServiceCharge = 0;
		this.props.traveller.map(passenger => {
			operatorServiceCharge += parseInt(
				passenger.operatorServiceChargeAbsolute
			);
		});
		if (operatorServiceCharge > 0) {
			return (
				<View style={styles.fareBreakup}>
					<RedbusTextRegular style={styles.secondaryText}>
						{I18n.t("operator_service_charge")}
					</RedbusTextRegular>
					<RedbusTextRegular style={styles.secondaryText}>
						{"₹" + I18n.toNumber(operatorServiceCharge, { precision: 0 })}
					</RedbusTextRegular>
				</View>
			);
		}
	};

	/**
	 * converts the string time into normal time format
	 * @param  {[String]} stringTime [redbus time]
	 * @return {[String]}            [$hour:$minute]
	 */
	getTime = stringTime => {
		let time = "";
		let integerTime = parseInt(stringTime);
		let hour = parseInt(integerTime / 60);
		let minute = parseInt(integerTime % 60);
		hour = hour >= 24 ? parseInt(hour / 24) : hour;
		time += hour + ":";
		time += minute < 10 ? "0" + minute : minute;
		return time;
	};

	/**
	 * convert the string date into 'DD MM' format
	 * @param  {[Date]} stringDate
	 * @return {[String]}            [$DD $MM]
	 */
	getDate = stringDate => {
		let date = new Date(stringDate).toDateString().split(" ");
		return date[DATE] + " " + date[MONTH];
	};

	render() {
		return (
			<View style={styles.container}>
				<Header
					goBack={true}
					iconHeight={height / 38}
					callOnBack={() => this.closeBookTicket()}>
					<View style={styles.subHeader}>
						<RedbusTextRegular style={styles.headerText}>
							{this.getTravelCity()}
						</RedbusTextRegular>
						<Counter
							maxTime={420}
							counterTextStyles={styles.counterTextStyle}
						/>
					</View>
				</Header>
				{this.state.isLoading !== NO_LOADING && (
					<ProgressScreen
						isMessage={true}
						message={
							this.state.isLoading === BLOCK_TICKCET
								? I18n.t("blocking_ticket")
								: I18n.t("booking_ticket")
						}
						indicatorColor={"#C13D4A"}
						indicatorSize={width / 15}
						primaryMessage={I18n.t("hang_on")}
					/>
				)}
				{!this.props.bookTicketError ? (
					<View style={styles.fullFlexView}>
						<ScrollView style={styles.bookTicketErrorView}>
							<View style={[styles.passengerInfo]}>
								{this.state.isOpen ? (
									<View>
										<RedbusTextMedium style={styles.infoHeader}>
											{I18n.t("travel_details")}
										</RedbusTextMedium>
										<View style={styles.travelDetail}>
											<RedbusTextRegular style={styles.secondaryText}>
												{this.props.selectedBus.travels}
											</RedbusTextRegular>
											<RedbusTextRegular style={styles.secondaryText}>
												{this.getDate(this.props.selectedBus.doj) +
													" " +
													I18n.t("at") +
													" " +
													this.getTime(this.props.selectedBus.departureTime)}
											</RedbusTextRegular>
											<RedbusTextRegular style={styles.secondaryText}>
												{this.props.selectedBus.busType}
											</RedbusTextRegular>
											<RedbusTextRegular style={styles.primaryText}>
												{I18n.t("boarding_point") + ": "}
												<RedbusTextRegular style={styles.secondaryText}>
													{this.props.selectedBoardingPoint.location}
												</RedbusTextRegular>
											</RedbusTextRegular>
											<RedbusTextRegular style={styles.primaryText}>
												{I18n.t("dropping_point") + ": "}
												<RedbusTextRegular style={styles.secondaryText}>
													{this.props.selectedDroppingPoint.location}
												</RedbusTextRegular>
											</RedbusTextRegular>
										</View>
										<FlatList
											style={styles.passengerDetailList}
											data={this.props.traveller}
											ListHeaderComponent={() => (
												<RedbusTextMedium style={styles.infoHeader}>
													{I18n.t("passenger_details")}
												</RedbusTextMedium>
											)}
											renderItem={({ item, index }) => {
												return (
													<View style={styles.passengerList}>
														<View>
															<RedbusTextRegular style={styles.secondaryText}>
																{item.passenger.name}
															</RedbusTextRegular>
															<RedbusTextRegular style={styles.secondaryText}>
																{item.passenger.gender}
																{", "}
																{item.passenger.age + " " + I18n.t("years")}
															</RedbusTextRegular>
														</View>
														<RedbusTextRegular style={styles.secondaryText}>
															{item.seatName}
														</RedbusTextRegular>
													</View>
												);
											}}
										/>
										<RedbusTextRegular style={styles.infoHeader}>
											{I18n.t("fare_breakup")}
										</RedbusTextRegular>
										{this.renderFare()}
										{this.renderOperatorService()}
										{this.renderGST()}

										<View style={styles.fareBreakup}>
											<RedbusTextRegular style={styles.secondaryText}>
												{I18n.t("total_fare")}
											</RedbusTextRegular>
											<RedbusTextRegular style={styles.secondaryText}>
												{"₹" + this.getTotalFare()}
											</RedbusTextRegular>
										</View>
										<View style={styles.fareBreakup}>
											<RedbusTextRegular style={styles.totalFare}>
												{I18n.t("total_pay")}
											</RedbusTextRegular>
											<RedbusTextRegular style={styles.totalFare}>
												{"₹" + this.getTotalFare()}
											</RedbusTextRegular>
										</View>
									</View>
								) : (
									<View style={styles.busSourceView}>
										<View style={styles.busSourceInnerView}>
											<RedbusTextRegular style={styles.smallSectionText}>
												{this.getDate(this.props.selectedBus.doj) +
													" " +
													I18n.t("at") +
													" " +
													this.getTime(this.props.selectedBus.departureTime)}
											</RedbusTextRegular>
											<RedbusTextRegular style={styles.smallSectionText}>
												{"₹" + this.getTotalFare()}
											</RedbusTextRegular>
										</View>
										<RedbusTextRegular style={styles.secondaryText}>
											{this.props.selectedBus.travels}
										</RedbusTextRegular>
										<RedbusTextRegular style={styles.primaryText}>
											{I18n.t("boarding_point") + ": "}
											<RedbusTextRegular style={styles.secondaryText}>
												{this.props.selectedBoardingPoint.bpName}
											</RedbusTextRegular>
										</RedbusTextRegular>
									</View>
								)}

								<TouchableOpacity
									style={styles.toggleButton}
									onPress={() => {
										// FIXME: when any of below issues closed
										// https://github.com/facebook/react-native/issues/25832
										// https://github.com/facebook/react-native/issues/26020
										if (Platform.OS === "ios") {
											LayoutAnimation.configureNext(
												LayoutAnimation.Presets.easeInEaseOut
											);
										}
										this.setState({ isOpen: !this.state.isOpen });
									}}>
									<Icon
										iconType={"ionicon"}
										iconSize={height / 28}
										iconName={
											this.state.isOpen ? "ios-arrow-up" : "ios-arrow-down"
										}
										iconColor={LOW_BLACK}
									/>
								</TouchableOpacity>
							</View>
						</ScrollView>
						<TouchableOpacity
							style={styles.button}
							onPress={() => this.makePayment()}>
							<RedbusTextRegular style={styles.headerText}>
								{I18n.t("book_ticket")}
							</RedbusTextRegular>
						</TouchableOpacity>
					</View>
				) : (
					<View style={styles.serverErrorView}>
						<Image
							style={styles.errorImage}
							source={SERVERERROR}
							resizeMode={"contain"}
						/>
						<RedbusTextRegular style={styles.primaryText}>
							{I18n.t("error_primary_message")}
						</RedbusTextRegular>
						<RedbusTextRegular style={styles.primaryText}>
							{I18n.t("error_secondary_message")}
						</RedbusTextRegular>
						<TouchableOpacity
							style={styles.goHomeButton}
							onPress={() => {
								this.props.navigation.dispatch(
									CommonActions.reset({
										index: 0,
										routes: [{ name: "SearchBus" }]
									})
								);
							}}>
							<RedbusTextRegular style={styles.headerText}>
								{I18n.t("go_home")}
							</RedbusTextRegular>
						</TouchableOpacity>
					</View>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		source: state.redbus && state.redbus.searchBus.source,
		destination: state.redbus && state.redbus.searchBus.destination,
		travelDate: state.redbus && state.redbus.searchBus.travelDate,
		seatLayout: state.redbus && state.redbus.selectSeat.seatLayout,
		selectedBus: state.redbus && state.redbus.selectSeat.selectedBus,
		selectedSeat: state.redbus && state.redbus.selectSeat.selectedSeat,
		traveller: state.redbus && state.redbus.selectSeat.traveller,
		selectedBoardingPoint:
			state.redbus && state.redbus.selectSeat.selectedBoardingPoint,
		selectedDroppingPoint:
			state.redbus && state.redbus.selectSeat.selectedDroppingPoint,
		blockKey: state.redbus && state.redbus.travellerDetail.blockKey,
		blockKeyError: state.redbus && state.redbus.travellerDetail.blockKeyError,
		bookTicketError:
			state.redbus && state.redbus.travellerDetail.bookTicketError,
		tinNumber: state.redbus && state.redbus.travellerDetail.tinNumber
	};
}
export default connect(
	mapStateToProps,
	null,
	null
)(BookTicket);
