import { StyleSheet, Dimensions, Platform } from "react-native";
import { font_three, font_two, font_four } from "../../../../Assets/styles";
import { LOW_BLACK } from "../../Constants";

const { width, height } = Dimensions.get("window");

export const indexStyles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#e9e9e9"
	},
	headerText: {
		color: "#fff",
		fontSize: height / 40
	},
	subHeader: {
		width: width / 1.2,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	counterTextStyle: {
		fontFamily: font_three,
		fontSize: height / 45,
		color: "#fff"
	},
	passengerInfo: {
		marginTop: height / 100,
		width: width / 1.03,
		paddingVertical: height / 30,
		backgroundColor: "#fff",
		alignSelf: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		borderRadius: 3
	},
	infoHeader: {
		fontSize: height / 40,
		color: LOW_BLACK,
		alignSelf: "center"
	},
	travelDetail: {
		padding: width / 30
	},
	primaryText: {
		fontFamily: font_two,
		fontSize: height / 42,
		color: LOW_BLACK,
		marginTop: height / 180
	},
	secondaryText: {
		fontSize: height / 45,
		color: LOW_BLACK,
		marginTop: height / 180
	},
	passengerList: {
		flexDirection: "row",
		justifyContent: "space-between",
		marginTop: height / 100
	},
	fareBreakup: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: width / 30
	},
	toggleButton: {
		position: "absolute",
		bottom: 0,
		width: width / 4,
		height: height / 15,
		borderWidth: 0,
		borderColor: "#000",
		justifyContent: "flex-end",
		alignItems: "center",
		alignSelf: "center"
	},
	totalFare: {
		fontFamily: font_three,
		fontSize: height / 45,
		color: "#000",
		marginTop: height / 180
	},
	smallSectionText: {
		fontFamily: font_four,
		fontSize: height / 42,
		color: "#000"
	},
	button: {
		width: width,
		height: height / 15,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "#C13D4A"
	},
	errorImage: {
		width: width,
		height: height / 2
	},
	goHomeButton: {
		width: width / 1.5,
		height: height / 15,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		borderRadius: 5,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "#C13D4A"
	},
	fullFlexView: { flex: 1 },
	bookTicketErrorView: { marginBottom: height / 15 },
	passengerDetailList: { padding: width / 30 },
	busSourceView: {
		paddingBottom: width / 30,
		paddingHorizontal: width / 30
	},
	busSourceInnerView: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	serverErrorView: { flex: 1, justifyContent: "center", alignItems: "center" }
});
