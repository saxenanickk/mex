import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api/";
import { NO_NETWORK_ERROR } from "../../Constants";
import { redbusNoNetwork } from "../../Saga";
export const REDBUS_GET_BLOCK_TICKET = "REDBUS_GET_BLOCK_TICKET";
export const REDBUS_SAVE_BLOCK_TICKET = "REDBUS_SAVE_BLOCK_TICKET";
export const REDBUS_BLOCK_TICKET_ERROR = "REDBUS_BLOCK_TICKET_ERROR";
export const REDBUS_GET_BOOK_TICKET = "REDBUS_GET_BOOK_TICKET";
export const REDBUS_SAVE_BOOK_TICKET = "REDBUS_SAVE_BOOK_TICKET";
export const REDBUS_SAVE_BOOK_TICKET_ERROR = "REDBUS_SAVE_BOOK_TICKET_ERROR";
export const REDBUS_GET_FARE_BREAKUP = "REDBUS_GET_FARE_BREAKUP";
export const REDBUS_SAVE_FARE_BREAKUP = "REDBUS_SAVE_FARE_BREAKUP";
/**
 * call Api to block the ticket
 * @return {String}         [blockKey]
 */
export const redbusGetBlockTicket = payload => ({
	type: REDBUS_GET_BLOCK_TICKET,
	payload
});

/**
 * save the block ticket to the store
 */
export const redbusSaveBlockTicket = payload => ({
	type: REDBUS_SAVE_BLOCK_TICKET,
	payload
});

//if there is error update while blocking the seat then save error to the store
export const redbusSaveBlockTicketError = payload => ({
	type: REDBUS_BLOCK_TICKET_ERROR,
	payload
});

//call Api to book ticket
//save the returned tin number to the store to fetch ticket detail
export const redbusGetBookTicket = payload => ({
	type: REDBUS_GET_BOOK_TICKET,
	payload
});

//save tin number after ticket booking to the store
export const redbusSaveBookTicket = payload => ({
	type: REDBUS_SAVE_BOOK_TICKET,
	payload
});

//update error in the store if error comes while booking ticket
export const redbusSaveBookTicketError = payload => ({
	type: REDBUS_SAVE_BOOK_TICKET_ERROR,
	payload
});

//call Api to get the fare breakup for a blockkey
export const redbusGetFareBreakup = payload => ({
	type: REDBUS_GET_FARE_BREAKUP,
	payload
});

//save fare brakup to the store
export const redbusSaveFareBreakup = payload => ({
	type: REDBUS_SAVE_FARE_BREAKUP,
	payload
});

//invoke traveller detail Saga
//functionality
//1.block the ticket for the user
//2.book the ticket for the user
export function* travellerDetailSaga(dispatch) {
	yield takeLatest(REDBUS_GET_BLOCK_TICKET, handleGetBlockTicket);
	yield takeLatest(REDBUS_GET_BOOK_TICKET, handleGetBookTicket);
}

//take latest request for block a seat
function* handleGetBlockTicket(action) {
	try {
		let response = yield call(Api.blockTicket, action.payload);
		console.log("block ticket response=>", response);
		if (response.error && response.errorCode === NO_NETWORK_ERROR) {
			//show no network message if there is no network error
			yield put(
				redbusNoNetwork({
					actionType: action.type,
					payload: action.payload
				})
			);
			return;
		} else {
			if (response.success === 1) {
				//if successfully blocked the seat the check the farebreakup
				let fareBreakup = yield call(Api.getFareBreakup, {
					appToken: action.payload.appToken,
					blockKey: response.data
				});
				if (fareBreakup.success === 1) {
					//if farebreakup is success then save the block key to the store
					yield put(redbusSaveFareBreakup(fareBreakup.data));
					yield put(redbusSaveBlockTicket(response.data));
				} else {
					// farebreakup fails then update the error status to be true
					yield put(redbusSaveBlockTicketError(true));
				}
			} else {
				// blockkey fails then update the error status to be true
				yield put(redbusSaveBlockTicketError(true));
			}
		}
	} catch (error) {
		console.log("block ticket error=>", error);
		yield put(redbusSaveBlockTicketError(true));
	}
}

function* handleGetBookTicket(action) {
	try {
		let response = yield call(Api.bookTicket, action.payload);
		console.log("book ticket response=>", response);
		if (response.error && response.errorCode === NO_NETWORK_ERROR) {
			//if there is no network error then show the no network message
			yield put(
				redbusNoNetwork({
					actionType: action.type,
					payload: action.payload
				})
			);
			return;
		} else {
			if (response.success === 1) {
				// if succesfully booked the ticket the save the tin number to the store
				yield put(redbusSaveBookTicket(response.data.tin));
			} else {
				// if unable to book the ticket then update the error to be true
				yield put(redbusSaveBookTicketError(true));
			}
		}
	} catch (error) {
		console.log("book ticket error=>", error);
		yield put(redbusSaveBookTicketError(true));
	}
}
