import React from "react";
import {
	View,
	StyleSheet,
	Dimensions,
	TouchableOpacity,
	NativeModules,
	FlatList,
	KeyboardAvoidingView,
	ScrollView,
	Keyboard,
	Platform
} from "react-native";
import {
	RedbusTextRegular,
	RedbusTextMedium
} from "../../Components/RedbusText";
import { connect } from "react-redux";
import { Header } from "../../Components";
import { font_two } from "../../../../Assets/styles";
import { CustomInput, GoToast } from "../../../../Components";
import { redbusSaveTraveller } from "../SelectSeat/Saga";
import { MALE, FEMALE } from "../../Constants";
import I18n from "../../Assets/Strings/i18n";
import { indexStyles as styles } from "./style";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
const { width, height } = Dimensions.get("window");

const { UIManager } = NativeModules;
if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}

/**
 * TravllerDetailScreen:accept traveller detail info
 * functionality:
 * 1:enter travller detail information
 */
class TravellerDetail extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isValid: true,
			traveller: this.props.traveller
		};
		this.validArray = {};
		this._keyboardDidShow = this._keyboardDidShow.bind(this);
		this._keyboardDidHide = this._keyboardDidHide.bind(this);
		this.keyboardDidShowListener = Keyboard.addListener(
			"keyboardDidShow",
			this._keyboardDidShow
		);
		this.keyboardDidHideListener = Keyboard.addListener(
			"keyboardDidHide",
			this._keyboardDidHide
		);
		this.touchTextAreaIndex = 0;
		this.touchTextAreaYOffset = [];
	}

	_keyboardDidShow = event => {
		try {
			if (Platform.OS === "ios") {
				if (this.scrollRef !== null && this.scrollRef !== undefined) {
					let index = this.touchTextAreaIndex;
					this.keyBoardHeight = event.endCoordinates.height;
					if (this.touchTextAreaYOffset[index] > event.endCoordinates.height) {
						this.scrollRef.scrollTo({
							x: 0,
							y:
								height / 10 +
								this.touchTextAreaYOffset[index] -
								this.keyBoardHeight,
							animated: true
						});
					}
				}
				console.log("height of keyboard is", event.endCoordinates.height);
			}
		} catch (error) {
			console.log("do nothing", error);
		}
	};

	_keyboardDidHide() {
		try {
			if (Platform.OS === "ios") {
				if (this.scrollRef !== null && this.scrollRef !== undefined) {
					this.scrollRef.scrollTo({
						x: 0,
						y: 0,
						animated: true
					});
				}
			}
		} catch (error) {
			console.log("do nothing");
		}
	}

	componentDidMount() {}
	shouldComponentUpdate() {
		return true;
	}

	/**
	 * get the name of source and destination city
	 */
	getTravelCity = () => {
		return (
			this.props.source.city_name +
			" " +
			I18n.t("to") +
			" " +
			this.props.destination.city_name
		);
	};

	componentWillUnmount() {
		this.props.dispatch(redbusSaveTraveller(null));
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	}

	/**
	 * get the traveller information to be stored for analytics
	 */
	getTravellerDetailForAnalytics = () => {
		let obj = {};
		this.state.traveller.map((item, index) => {
			obj["passenger" + (index + 1)] =
				item.passenger.name +
				"," +
				item.passenger.gender +
				"," +
				item.passenger.age;
			if (item.passenger.primary === true) {
				obj.email = item.passenger.email;
				obj.mobile = item.passenger.mobile;
			}
		});
		obj.busId = this.props.selectedBus.id;
		return obj;
	};

	/**
	 * calls block ticket to block particular tickets
	 */
	async blockTicket() {
		try {
			await this.props.dispatch(redbusSaveTraveller(this.state.traveller));
			let analytic_prop = this.getTravellerDetailForAnalytics(
				this.state.traveller
			);
			GoAppAnalytics.trackWithProperties("add_traveller", analytic_prop);
			if (this.validatePassenger()) {
				this.props.navigation.navigate("BookTicket");
			} else {
				GoToast.show(I18n.t("invalid_input"), I18n.t("error"));
			}
		} catch (error) {
			console.log(error);
		}
	}

	/**
	 * select gender for a traveller
	 */
	selectGender = (value, index) => {
		let temp = this.state.traveller.slice();
		temp[index].passenger.gender = value;
		this.setState({ traveller: temp });
		this.props.dispatch(redbusSaveTraveller(temp));
	};

	/**
	 * validate the passenger information
	 */
	validatePassenger = () => {
		let value = true;
		console.log(this.state.traveller);
		this.state.traveller.map(item => {
			if (item.passenger.name === null || item.passenger.name.trim() === "") {
				value = false;
			}
			if (
				item.passenger.age === null ||
				item.passenger.age.trim() === "" ||
				isNaN(item.passenger.age)
			) {
				value = false;
			}
			if (item.passenger.primary === true) {
				if (
					item.passenger.email === null ||
					(item.passenger.email === "" ||
						!this.validateEmail(item.passenger.email.trim()))
				) {
					value = false;
				}
				if (
					item.passenger.mobile === null ||
					(item.passenger.mobile.trim() === "" || isNaN(item.passenger.mobile))
				) {
					value = false;
				}
			}
			if (
				item.passenger.gender === null ||
				item.passenger.gender.trim() === ""
			) {
				value = false;
			}
		});
		return value;
	};

	/**
	 * validate the email id according to the regex pattern
	 */
	validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}

	/**
	 * render the passenger view for passenger info input
	 * @param  {[Object]} item  [seat info]
	 * @param  {[Integer]} index [seat index]
	 * @return {[Component]}
	 */
	renderPassenger = (item, index) => {
		return (
			<View
				style={styles.contactField}
				key={index}
				accessible={true}
				onTouchStart={() => (this.touchTextAreaIndex = index)}
				onLayout={event => {
					console.log("for index", index, event.nativeEvent.layout.y);
					this.touchTextAreaYOffset[index] = event.nativeEvent.layout.y;
				}}>
				<View style={styles.passengerHeader}>
					<RedbusTextMedium style={styles.passengerHeaderText}>
						{index === 0 ? I18n.t("primary_passenger") : I18n.t("co_passenger")}
					</RedbusTextMedium>
					<RedbusTextMedium style={styles.passengerHeaderText}>
						{I18n.t("seat") + " " + item.name}
					</RedbusTextMedium>
				</View>
				<CustomInput
					label={I18n.t("name")}
					borderColor={"#C13D4A"}
					style={styles.inputField}
					labelStyle={styles.inputLabel}
					requiredTextStyle={styles.inputRequiredText}
					selectionColor={"#C13D4A"}
					onBlur={args =>
						this.props.dispatch(redbusSaveTraveller(this.state.traveller))
					}
					onChange={event => {
						let temp = this.state.traveller.slice();
						temp[index].passenger.name = event.nativeEvent.text;
						this.setState({ traveller: temp });
					}}
					checks={{ isRequired: true, minSize: 2, maxSize: 25 }}
					animationDuration={500}
				/>
				<View style={styles.genderSection}>
					<RedbusTextRegular style={styles.genderText}>
						{I18n.t("gender")}
					</RedbusTextRegular>
					<TouchableOpacity
						style={styles.radioButton}
						onPress={() => {
							if (item.ladiesSeat === "true") {
								GoToast.show(I18n.t("only_ladies_seat"), I18n.t("error"));
							} else {
								this.selectGender(MALE, index);
							}
						}}>
						<View style={styles.circle}>
							{this.state.traveller[index].passenger.gender === MALE && (
								<View style={styles.innerCircle} />
							)}
						</View>
						<RedbusTextRegular style={styles.genderText}>
							{I18n.t("male")}
						</RedbusTextRegular>
					</TouchableOpacity>
					<TouchableOpacity
						style={styles.radioButton}
						onPress={() => this.selectGender(FEMALE, index)}>
						<View style={styles.circle}>
							{this.state.traveller[index].passenger.gender === FEMALE && (
								<View style={styles.innerCircle} />
							)}
						</View>
						<RedbusTextRegular style={styles.genderText}>
							{I18n.t("female")}
						</RedbusTextRegular>
					</TouchableOpacity>
				</View>
				<CustomInput
					label={I18n.t("rb_age")}
					borderColor={"#C13D4A"}
					style={styles.inputField}
					labelStyle={styles.inputLabel}
					requiredTextStyle={styles.inputRequiredText}
					selectionColor={"#C13D4A"}
					onBlur={args =>
						this.props.dispatch(redbusSaveTraveller(this.state.traveller))
					}
					onChange={event => {
						let temp = this.state.traveller.slice();
						temp[index].passenger.age = event.nativeEvent.text;
						this.setState({ traveller: temp });
					}}
					checks={{ isRequired: true, isNumber: true, minSize: 1, maxSize: 3 }}
					animationDuration={500}
					keyboardType={"numeric"}
				/>
			</View>
		);
	};
	render() {
		return (
			<View style={styles.container}>
				<Header
					goBack={true}
					iconHeight={height / 38}
					callOnBack={() => {
						this.props.navigation.goBack();
					}}>
					<View style={styles.travelCityView}>
						<RedbusTextRegular style={styles.headerText}>
							{this.getTravelCity()}
						</RedbusTextRegular>
					</View>
				</Header>
				<ScrollView
					ref={ref => (this.scrollRef = ref)}
					style={styles.scrollContainer}
					keyboardShouldPersistTaps={"always"}>
					<View style={styles.contactField}>
						<CustomInput
							label={I18n.t("email_id")}
							borderColor={"#C13D4A"}
							style={styles.inputField}
							labelStyle={styles.inputLabel}
							requiredTextStyle={styles.inputRequiredText}
							selectionColor={"#C13D4A"}
							onBlur={args =>
								this.props.dispatch(redbusSaveTraveller(this.state.traveller))
							}
							onChange={event => {
								let temp = this.state.traveller.slice();
								temp[0].passenger.email = event.nativeEvent.text;
								this.setState({ traveller: temp });
							}}
							checks={{ isRequired: true, minSize: 2, isEmail: true }}
							animationDuration={700}
						/>
						<CustomInput
							label={I18n.t("phone")}
							borderColor={"#C13D4A"}
							style={styles.inputField}
							labelStyle={styles.inputLabel}
							requiredTextStyle={styles.inputRequiredText}
							selectionColor={"#C13D4A"}
							onBlur={args =>
								this.props.dispatch(redbusSaveTraveller(this.state.traveller))
							}
							onChange={event => {
								let temp = this.state.traveller.slice();
								temp[0].passenger.mobile = event.nativeEvent.text;
								this.setState({ traveller: temp });
							}}
							checks={{
								isRequired: true,
								minSize: 10,
								isNumber: true,
								maxSize: 10
							}}
							animationDuration={700}
							keyboardType={"numeric"}
						/>
					</View>
					{this.props.selectedSeat &&
						this.props.selectedSeat.length > 0 &&
						this.props.selectedSeat.map((item, index) => {
							return this.renderPassenger(item, index);
						})}
				</ScrollView>
				<TouchableOpacity
					style={styles.button}
					onPress={() => this.blockTicket()}>
					<RedbusTextRegular style={styles.headerText}>
						{I18n.t("continue")}
					</RedbusTextRegular>
				</TouchableOpacity>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		source: state.redbus && state.redbus.searchBus.source,
		destination: state.redbus && state.redbus.searchBus.destination,
		travelDate: state.redbus && state.redbus.searchBus.travelDate,
		seatLayout: state.redbus && state.redbus.selectSeat.seatLayout,
		selectedBus: state.redbus && state.redbus.selectSeat.selectedBus,
		selectedSeat: state.redbus && state.redbus.selectSeat.selectedSeat,
		traveller: state.redbus && state.redbus.selectSeat.traveller,
		selectedBoardingPoint:
			state.redbus && state.redbus.selectSeat.selectedBoardingPoint,
		selectedDroppingPoint:
			state.redbus && state.redbus.selectSeat.selectedDroppingPoint
	};
}
export default connect(
	mapStateToProps,
	null,
	null
)(TravellerDetail);
