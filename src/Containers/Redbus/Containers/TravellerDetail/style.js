import { StyleSheet, Dimensions } from "react-native";
import { LOW_BLACK } from "../../Constants";
import { font_two } from "../../../../Assets/styles";
const { width, height } = Dimensions.get("window");

export const indexStyles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#e9e9e9"
	},
	scrollContainer: {
		flex: 1,
		backgroundColor: "#e9e9e9",
		marginBottom: height / 15
	},
	headerText: {
		color: "#fff",
		fontSize: height / 40
	},
	contactField: {
		marginTop: width / 30,
		width: width / 1.03,
		backgroundColor: "#fff",
		alignSelf: "center"
	},
	inputField: {
		width: width / 1.1,
		alignSelf: "center"
	},
	inputLabel: {
		fontFamily: font_two,
		fontSize: height / 40
	},
	inputRequiredText: {
		fontFamily: font_two,
		fontSize: height / 60,
		color: "#EC6733"
	},
	passengerHeaderText: {
		color: "#000",
		fontSize: height / 55
	},
	passengerHeader: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: width / 30
	},
	genderSection: {
		flexDirection: "row",
		paddingHorizontal: width / 30
	},
	radioButton: {
		flexDirection: "row",
		marginLeft: width / 30,
		alignItems: "center"
	},
	circle: {
		width: width / 20,
		height: width / 20,
		borderRadius: width / 40,
		justifyContent: "center",
		alignItems: "center",
		borderWidth: 1,
		borderColor: "#DFDDE0"
	},
	innerCircle: {
		width: width / 30,
		height: width / 30,
		borderRadius: width / 60,
		backgroundColor: "#C13D4A"
	},
	genderText: {
		fontSize: height / 55,
		color: "#000",
		marginLeft: width / 50
	},
	button: {
		width: width,
		height: height / 15,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#C13D4A",
		position: "absolute",
		bottom: 0
	},
	travelCityView: { width: width / 1.2 }
});
