import {
	REDBUS_SAVE_BLOCK_TICKET,
	REDBUS_BLOCK_TICKET_ERROR,
	REDBUS_SAVE_BOOK_TICKET,
	REDBUS_SAVE_BOOK_TICKET_ERROR,
	REDBUS_SAVE_FARE_BREAKUP
} from "./Saga";

const initialState = {
	blockKey: null,
	blockKeyError: false,
	tinNumber: null,
	bookTicketError: false,
	fareBreakup: null
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case REDBUS_SAVE_BLOCK_TICKET:
			return {
				...state,
				blockKey: action.payload
			};
		case REDBUS_BLOCK_TICKET_ERROR:
			return {
				...state,
				blockKeyError: action.payload
			};
		case REDBUS_SAVE_BOOK_TICKET:
			return {
				...state,
				tinNumber: action.payload
			};
		case REDBUS_SAVE_BOOK_TICKET_ERROR:
			return {
				...state,
				bookTicketError: action.payload
			};
		case REDBUS_SAVE_FARE_BREAKUP:
			return {
				...state,
				fareBreakup: action.payload
			};
		default:
			return {
				...state
			};
	}
};
