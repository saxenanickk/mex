/* eslint-disable radix */
import {
	REDBUS_SAVE_ALL_TRIP,
	REDBUS_SAVE_SERVER_ERROR,
	REDBUS_SAVE_FILTER,
	REDBUS_RESET_FILTER,
	REDBUS_SAVE_SORT
} from "./Saga";
import { NO_SORT } from "../../Constants";
const initialState = {
	trips: null,
	tripError: false,
	filter: {
		AC: null,
		nonAC: null,
		seater: null,
		sleeper: null,
		morning: null,
		afternoon: null,
		evening: null,
		night: null,
		liveTracking: null,
		redDeals: null,
		highRatedBus: null,
		cancellable: null,
		mTicket: null,
		travels: null,
		boardingPoints: null,
		droppingPoints: null,
		travelsTemp: [],
		boardingPointsTemp: [],
		droppingPointsTemp: []
	},
	sort: {
		rating: NO_SORT,
		departure: NO_SORT,
		fare: NO_SORT
	}
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case REDBUS_SAVE_ALL_TRIP:
			let bus = null;
			if (action.payload !== null) {
				if (action.payload instanceof Array) {
					bus = action.payload;
				} else {
					bus = [];
					bus.push(action.payload);
				}
				bus = bus.filter(item => parseInt(item.availableSeats) > 0);
			}
			return {
				...state,
				trips: bus
			};
		case REDBUS_SAVE_SERVER_ERROR:
			return {
				...state,
				tripError: action.payload
			};
		case REDBUS_SAVE_FILTER:
			return {
				...state,
				filter: {
					...action.payload,
					travels:
						action.payload.travelsTemp.length !== 0
							? action.payload.travelsTemp
							: null,
					boardingPoints:
						action.payload.boardingPointsTemp.length !== 0
							? action.payload.boardingPointsTemp
							: null,
					droppingPoints:
						action.payload.droppingPointsTemp.length !== 0
							? action.payload.droppingPointsTemp
							: null
				}
			};
		case REDBUS_SAVE_SORT:
			return {
				...state,
				sort: action.payload
			};
		case REDBUS_RESET_FILTER:
			return {
				...state,
				filter: {
					AC: null,
					nonAC: null,
					seater: null,
					sleeper: null,
					morning: null,
					afternoon: null,
					evening: null,
					night: null,
					liveTracking: null,
					redDeals: null,
					highRatedBus: null,
					cancellable: null,
					mTicket: null,
					travels: null,
					boardingPoints: null,
					droppingPoints: null,
					travelsTemp: [],
					boardingPointsTemp: [],
					droppingPointsTemp: []
				},
				sort: {
					rating: NO_SORT,
					departure: NO_SORT,
					fare: NO_SORT
				}
			};
		default:
			return state;
	}
};
