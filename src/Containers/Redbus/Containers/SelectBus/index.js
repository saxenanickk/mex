/* eslint-disable radix */
import React from "react";
import {
	View,
	Image,
	Dimensions,
	TouchableOpacity,
	LayoutAnimation,
	FlatList,
	UIManager,
	Platform
} from "react-native";
import { connect } from "react-redux";
import { Header, Loader } from "../../Components/";
import { SERVERERROR, FILTER, BUSICON } from "../../Assets/Img/Image";
import {
	RedbusTextLight,
	RedbusTextRegular,
	RedbusTextBold
} from "../../Components/RedbusText";
import { Icon, GoToast } from "../../../../Components/";
import {
	redbusGetAllTrip,
	redbusSaveAllTrip,
	redbusResetFilter,
	redbusSaveServerError
} from "./Saga";
import {
	redbusSaveSelectedBus,
	redbusSaveSeatLayout
} from "../SelectSeat/Saga";
import { redbusSaveTravelDate } from "../SearchBus/Saga";
import {
	DATE,
	DAY,
	MONTH,
	YEAR,
	ONE_DAY_EPOCH_DURATION,
	NO_SORT,
	LOW_BLACK
} from "../../Constants";
import I18n from "../../Assets/Strings/i18n";
import Filter from "./Filter";
import { indexStyles as styles } from "./style";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
const { height } = Dimensions.get("window");

/**
 * SelectBus screen
 * functionality:
 * 1.list of trips available between source and destination
 * 2.filtering of trip
 * 3.change date of trip
 */
class SelectBus extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			filterModal: false,
			showFilterButton: true
		};
		this.data = [];
		this.offSet = 0;
		if (Platform.OS === "android") {
			UIManager.setLayoutAnimationEnabledExperimental(true);
		}
	}

	/**
	 * Life cycle method
	 */
	componentDidMount() {
		GoAppAnalytics.trackWithProperties("search_trips", {
			source: this.props.source.redbus_city_id,
			destination: this.props.destination.redbus_city_id,
			doj: this.getDateAsParameter()
		});
		this.props.dispatch(
			redbusGetAllTrip({
				appToken: this.props.appToken,
				source: this.props.source.redbus_city_id,
				destination: this.props.destination.redbus_city_id,
				doj: this.getDateAsParameter()
			})
		);
	}

	shouldComponentUpdate(props, state) {
		//show toast for newly fetched trips
		if (props.trips !== null && props.trips !== this.props.trips) {
			GoToast.show(
				props.trips.length + " " + I18n.t("bus_found"),
				I18n.t("success")
			);
		}
		//hide the filterModal if new filters applied
		if (props.filter !== null && props.filter !== this.props.filter) {
			setTimeout(() => this.setState({ filterModal: false }), 100);
		}
		if (props.sort !== null && props.sort !== this.props.sort) {
			setTimeout(() => this.setState({ filterModal: false }), 100);
		}
		return true;
	}

	componentWillUnmount() {
		this.props.dispatch(redbusSaveAllTrip(null));
		this.props.dispatch(redbusSaveSeatLayout(null));
		this.props.dispatch(redbusResetFilter());
		this.props.dispatch(redbusSaveServerError(false));
	}

	/**
	 * method to filter and sort the list of bus
	 */
	filteredSearch() {
		console.log("filter props are-=>", this.props.filter);
		this.data = [];
		this.data = this.checkForAc(this.props.trips);
		this.data = this.checkFilterBy(this.data);
		this.data = this.onlyShow(this.data);
		this.data = this.checkBusTime(this.data);
		this.data = this.sortList(this.data);
		return this.data;
	}

	/**
	 * sort the list of trip
	 * @param  {[Object]} filteredData [list of trips]
	 */
	sortList = filteredData => {
		//if sort type is selected for departure then sort
		if (this.props.sort.departure !== NO_SORT) {
			switch (this.props.sort.departure) {
				case 1:
					return filteredData.sort(
						(first, second) =>
							parseInt(second.departureTime) - parseInt(first.departureTime)
					);
				case 2:
					return filteredData.sort(
						(first, second) =>
							parseInt(first.departureTime) - parseInt(second.departureTime)
					);
			}
		}
		//if sort type is selected for fare then sort
		if (this.props.sort.fare !== NO_SORT) {
			switch (this.props.sort.fare) {
				case 1:
					return filteredData.sort((first, second) => {
						let firstFare = this.getFares(first.fareDetails);
						let secondFare = this.getFares(second.fareDetails);
						return (
							parseInt(secondFare[0].baseFare) - parseInt(firstFare[0].baseFare)
						);
					});
				case 2:
					return filteredData.sort((first, second) => {
						let firstFare = this.getFares(first.fareDetails);
						let secondFare = this.getFares(second.fareDetails);
						return (
							parseInt(firstFare[0].baseFare) - parseInt(secondFare[0].baseFare)
						);
					});
			}
		}
		return filteredData;
	};

	/**
	 * method to check whether filter is applied or not
	 */
	isFilterApplied = () => {
		let isFilter = false;
		let arrayKey = ["boardingPointsTemp", "droppingPointsTemp", "travelsTemp"];
		Object.keys(this.props.filter).map(filterKey => {
			if (arrayKey.indexOf(filterKey) > -1) {
				if (this.props.filter[filterKey].length > 0) {
					isFilter = true;
				}
			} else {
				if (this.props.filter[filterKey] !== null) {
					isFilter = true;
				}
			}
		});
		Object.keys(this.props.sort).map(sortKey => {
			if (this.props.sort[sortKey] !== NO_SORT) {
				isFilter = true;
			}
		});
		return isFilter;
	};

	/**
	 * helper methods to filter the list of bus to check whether bus is AC or not
	 */
	checkForAc = allTrips => {
		let busTypeData = [];
		busTypeData = allTrips.filter(trip => {
			let condition = true;
			//if filter according to AC and nonAC bus is selected
			if (this.props.filter.AC && this.props.filter.nonAC) {
				condition = condition && (trip.AC === "true" || trip.nonAC === "true");
			} else {
				if (this.props.filter.AC) {
					condition = condition && trip.AC === "true";
				}
				if (this.props.filter.nonAC) {
					condition = condition && trip.nonAC === "true";
				}
			}
			//if filter according to seater and sleeper is selected
			if (this.props.filter.seater && this.props.filter.sleeper) {
				condition =
					condition && (trip.seater === "true" || trip.sleeper === "true");
			} else {
				if (this.props.filter.sleeper) {
					condition = condition && trip.sleeper === "true";
				}
				if (this.props.filter.seater) {
					condition = condition && trip.seater === "true";
				}
			}
			return condition;
		});
		return busTypeData;
	};

	/*
	 * function to filter trips according to boarding point dropping point and travels
	 */
	checkFilterBy = filterData => {
		//filter trips according to selected travels
		if (this.props.filter.travels !== null) {
			filterData = filterData.filter(travelsItem => {
				if (this.props.filter.travels.indexOf(travelsItem.travels) > -1) {
					return true;
				}
			});
		}
		//filter trips according to boarding points
		if (this.props.filter.boardingPoints !== null) {
			filterData = filterData.filter(item => {
				let boardingTimes = [];
				if (item.boardingTimes instanceof Array) {
					boardingTimes = item.boardingTimes;
				} else {
					boardingTimes = new Array(item.boardingTimes);
				}
				return (
					boardingTimes.filter(
						row => this.props.filter.boardingPoints.indexOf(row.bpName) > -1
					).length > 0
				);
			});
		}
		//filter trips according tp dropping points
		if (this.props.filter.droppingPoints !== null) {
			filterData = filterData.filter(item => {
				let droppingTimes = null;
				if (item.droppingTimes instanceof Array) {
					droppingTimes = item.droppingTimes;
				} else {
					droppingTimes = new Array(item.droppingTimes);
				}
				return (
					droppingTimes.filter(
						row => this.props.filter.droppingPoints.indexOf(row.bpName) > -1
					).length > 0
				);
			});
		}
		return filterData;
	};

	onlyShow = filteredTrips => {
		let onlyShowData = filteredTrips.filter(trip => {
			let condition = false;
			let flag = 0;
			//filter according to live tracking available for buses
			if (this.props.filter.liveTracking) {
				flag = 1;
				condition = condition || trip.liveTrackingAvailable === "true";
			}
			//filter for all mTIcket buses available
			if (this.props.filter.mTicket) {
				flag = 1;
				condition = condition || trip.mTicketEnabled === "true";
			}
			//filter if the trips are cancellable
			if (this.props.filter.cancellable) {
				flag = 1;
				condition = condition || trip.partialCancellationAllowed === "true";
			}
			return flag === 1 ? condition : true;
		});
		return onlyShowData;
	};

	/**
	 * filter according to the bus time
	 * @return {[Array]} filtered trips according to the selected time interval
	 */
	checkBusTime = () => {
		let timeData = [];
		//filter trips in the morning
		if (this.props.filter.morning) {
			this.data.map(item => {
				let time = this.getNewTime(item.departureTime);
				let hour = time.slice(0, 2);
				if (hour >= 6 && hour < 12) {
					timeData.push(item);
				}
			});
		}
		//fileter trips in the afternoon
		if (this.props.filter.afternoon) {
			this.data.map(item => {
				let time = this.getNewTime(item.departureTime);
				let hour = time.slice(0, 2);
				if (hour >= 12 && hour < 18) {
					timeData.push(item);
				}
			});
		}
		//filter trips in the evening
		if (this.props.filter.evening) {
			this.data.map(item => {
				let time = this.getNewTime(item.departureTime);
				let hour = time.slice(0, 2);
				if (hour >= 18 && hour < 24) {
					timeData.push(item);
				}
			});
		}
		//filter trips in the night
		if (this.props.filter.night) {
			this.data.map(item => {
				let time = this.getNewTime(item.departureTime);
				let hour = time.slice(0, 2);
				if (hour < 6) {
					timeData.push(item);
				}
			});
		}
		//if filtered then change the list of trips
		if (timeData.length !== 0) {
			this.data = timeData;
		}
		return this.data;
	};

	/**
	 * helper method to retrieve the time in hrs and minutes
	 */
	getNewTime = stringTime => {
		let time = "";
		let integerTime = parseInt(stringTime);
		let hour = parseInt(integerTime / 60);
		let minute = parseInt(integerTime % 60);
		hour = hour >= 24 ? parseInt(hour / 24) : hour;
		time += hour < 10 ? "0" + hour + ":" : hour + ":";
		time += minute < 10 ? "0" + minute : minute;
		return time;
	};

	/**
	 * get the name of travel city
	 * @return {[String]} [$source to $destination]
	 */
	getTravelCity = () => {
		return (
			this.props.source.city_name +
			" " +
			I18n.t("to") +
			" " +
			this.props.destination.city_name
		);
	};

	/**
	 * get date in readbale fomat
	 * @return {[String]} [$DAY,$DATE $MONTH $YEAR]
	 */
	getTravelDate = () => {
		let date = this.props.travelDate.toDateString().split(" ");
		return date[DAY] + ", " + date[DATE] + " " + date[MONTH] + " " + date[YEAR];
	};

	/**
	 * check whether trips for previous date is available or not
	 * @return {[boolean]} [false if trip not avaialble else true]
	 */
	isPreviousDateVisible = () => {
		return new Date().getTime() < this.props.travelDate.getTime();
	};

	/**
	 * get date as parameter to be sent to the backend
	 * @param  {[Date]} newDate
	 * @return {[STring]}         [$YYYY-$MM-$DD]
	 */
	getDateAsParameter = (newDate = null) => {
		let date = newDate !== null ? newDate : this.props.travelDate;
		return (
			date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
		);
	};

	/**
	 * method to change the travel date
	 */
	changeDate = type => {
		let newDate = new Date();
		let travelDate = new Date(this.props.travelDate).getTime();
		if (type) {
			newDate = new Date(travelDate + ONE_DAY_EPOCH_DURATION);
		} else {
			newDate = new Date(travelDate - ONE_DAY_EPOCH_DURATION);
		}
		this.props.dispatch(redbusSaveTravelDate(newDate));
		this.props.dispatch(redbusSaveAllTrip(null));
		this.props.dispatch(redbusSaveServerError(false));
		this.props.dispatch(
			redbusGetAllTrip({
				appToken: this.props.appToken,
				source: this.props.source.redbus_city_id,
				destination: this.props.destination.redbus_city_id,
				doj: this.getDateAsParameter(newDate)
			})
		);
	};

	/**
	 * opens the filter modal
	 */
	filter = () => {
		LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
		this.setState({ filterModal: true });
	};

	/**
	 * convert the time from redbus format time to string format time
	 * @param  {[String]} stringTime [time in string format]
	 * @return {[String]}            [$HH $MM]
	 */
	getTime = stringTime => {
		let time = "";
		let timeType = "";
		let integerTime = parseInt(stringTime);
		let hour = parseInt(integerTime / 60);
		let minute = parseInt(integerTime % 60);
		hour = hour >= 24 ? parseInt(hour % 24) : hour;
		if (hour < 12) {
			timeType = "AM";
		} else if (hour >= 12) {
			hour = hour > 12 ? hour - 12 : hour;
			timeType = "PM";
		}
		time += hour < 10 ? "0" + hour + ":" : hour + ":";
		time += minute < 10 ? "0" + minute : minute;
		time += " " + timeType;
		return time;
	};

	/**
	 * calculate the duration of a single trip
	 * @param  {[integer]} arrvTime [arrival time in redbus specific format]
	 * @param  {[ineteger]} deptTime [departure time in redbus specific format]
	 * @return {[integer]}          [difference between arrival and departure time]
	 */
	getDuration = (arrvTime, deptTime) => {
		let duration = "";
		let time = parseInt(arrvTime) - parseInt(deptTime);
		let hour = parseInt(time / 60);
		let minute = parseInt(time % 60);
		let day = hour >= 24 ? parseInt(hour / 24) : 0;
		hour = hour >= 24 ? parseInt(hour % 24) : hour;
		duration += day > 0 ? day + " day," : "";
		duration += hour + ":";
		duration += minute > 10 ? minute + " hrs" : "0" + minute + " hrs";
		return duration;
	};

	/**
	 * find the highes fare from the fares of a trip
	 * @param  {[Array]} fares [list of fares for a trip]
	 * @return {[Object]}       [highest fare]
	 */
	getFares = fares => {
		let fare = [];
		if (fares instanceof Array) {
			fares = fares.sort((a, b) => parseInt(b.baseFare) - parseInt(a.baseFare));
			return fares;
		} else {
			fare.push(fares);
			fare = fare.sort((a, b) => parseInt(b.baseFare) - parseInt(a.baseFare));
			return fare;
		}
	};

	/**
	 * navigates to the Select seat screen
	 * @param  {[Object]} item [selected trip to be stored into the store]
	 */
	async goToSelectSeat(item) {
		try {
			GoAppAnalytics.trackWithProperties("select_trip", {
				id: item.id,
				travels: item.travels,
				source: this.props.source.redbus_city_id,
				destination: this.props.destination.redbus_city_id,
				doj: this.getDateAsParameter()
			});
			await this.props.dispatch(redbusSaveSelectedBus(item));
			this.props.navigation.navigate("SelectSeat");
		} catch (error) {
			console.log(error);
		}
	}

	/**
	 * function to render fares
	 */

	renderFare = item => {
		let fares = this.getFares(item.fareDetails);
		let msg = fares.length > 1 ? "from " : "";
		return (
			<RedbusTextBold style={styles.rate}>
				{msg +
					"₹" +
					I18n.toNumber(Math.round(fares[0].baseFare), { precision: 0 })}
			</RedbusTextBold>
		);
	};
	/**
	 * method to render each section of trip
	 */
	renderTrip = item => {
		return (
			<View style={styles.renderTripView}>
				<TouchableOpacity
					style={styles.listItem}
					onPress={() => this.goToSelectSeat(item)}>
					<Image style={styles.bus} source={BUSICON} resizeMode={"contain"} />
					<View style={styles.busInfo}>
						<RedbusTextRegular style={styles.travelAgency}>
							{item.travels}
						</RedbusTextRegular>
						<RedbusTextLight style={styles.busType} numberOfLines={1}>
							{item.busType.toUpperCase()}
						</RedbusTextLight>
						<View style={styles.seatBar}>
							<RedbusTextRegular style={styles.seatText}>
								{item.availableSeats + " " + I18n.t("seats")}
							</RedbusTextRegular>
							<View style={styles.dot} />
							<RedbusTextRegular style={styles.seatText}>
								{this.getDuration(item.arrivalTime, item.departureTime)}
							</RedbusTextRegular>
						</View>
					</View>
					<View style={styles.rateInfo}>
						<View style={styles.flexEndAlignment}>
							<RedbusTextRegular style={styles.departure}>
								{this.getTime(item.departureTime)}
							</RedbusTextRegular>
							<RedbusTextRegular style={styles.arrival}>
								{this.getTime(item.arrivalTime)}
							</RedbusTextRegular>
						</View>
						{this.renderFare(item)}
					</View>
				</TouchableOpacity>
			</View>
		);
	};

	render() {
		return (
			<View style={styles.container}>
				<Header
					goBack={true}
					iconHeight={height / 38}
					callOnBack={() => {
						this.props.navigation.goBack();
					}}>
					<View style={styles.subHeader}>
						<RedbusTextBold numberOfLines={1} style={styles.headerText}>
							{this.getTravelCity()}
						</RedbusTextBold>
						<TouchableOpacity
							onPress={this.filter}
							disabled={this.props.trips === null}>
							<Image
								style={styles.image}
								source={FILTER}
								resizeMode={"contain"}
							/>
						</TouchableOpacity>
					</View>
				</Header>
				<View style={styles.fullFlex}>
					{this.props.trips && (
						<View style={styles.dateBar}>
							<TouchableOpacity
								style={styles.dateButton}
								disabled={!this.isPreviousDateVisible()}
								onPress={() => this.changeDate(false)}>
								{this.isPreviousDateVisible() && (
									<Icon
										iconType={"font_awesome"}
										iconSize={height / 32}
										iconName={"angle-left"}
										iconColor={LOW_BLACK}
									/>
								)}
							</TouchableOpacity>
							<RedbusTextRegular style={styles.travelDate}>
								{this.getTravelDate()}
							</RedbusTextRegular>
							<TouchableOpacity
								style={styles.dateButton}
								onPress={() => this.changeDate(true)}>
								<Icon
									iconType={"font_awesome"}
									iconSize={height / 32}
									iconName={"angle-right"}
									iconColor={LOW_BLACK}
								/>
							</TouchableOpacity>
						</View>
					)}
					{this.props.tripError === true ? (
						<View style={styles.tripErrorView}>
							<Image
								style={styles.tripErrorImage}
								source={SERVERERROR}
								resizeMode={"contain"}
							/>

							<RedbusTextRegular style={styles.noResult}>
								{I18n.t("server_error")}
							</RedbusTextRegular>
						</View>
					) : this.props.trips === null ? (
						<Loader />
					) : (
						<FlatList
							removeClippedSubviews
							style={{ height: height }}
							data={this.filteredSearch()}
							keyExtractor={(item, index) => index.toString()}
							renderItem={({ item, index }) => this.renderTrip(item)}
							ItemSeparatorComponent={() => {
								return <View style={styles.separator} />;
							}}
							scrollEventThrottle={1}
							onScroll={event => {
								const currentOffset = event.nativeEvent.contentOffset.y;
								if (currentOffset > 0 && currentOffset > this.offSet) {
									LayoutAnimation.configureNext(
										LayoutAnimation.Presets.easeInEaseOut
									);
									if (this.state.showFilterButton) {
										this.setState({ showFilterButton: false });
									}
								} else {
									LayoutAnimation.configureNext(
										LayoutAnimation.Presets.easeInEaseOut
									);
									if (!this.state.showFilterButton) {
										this.setState({ showFilterButton: true });
									}
								}
								this.offSet = currentOffset;
							}}
							ListEmptyComponent={() => {
								return (
									<View style={styles.tripErrorView}>
										<Image
											style={styles.tripErrorImage}
											source={SERVERERROR}
											resizeMode={"contain"}
										/>

										<RedbusTextBold style={styles.noResult}>
											{I18n.t("no_service_found")}
										</RedbusTextBold>
										<RedbusTextRegular style={styles.noResult}>
											{I18n.t("try_another_date")}
										</RedbusTextRegular>
									</View>
								);
							}}
						/>
					)}
				</View>
				{this.isFilterApplied() &&
					this.state.showFilterButton &&
					this.props.trips &&
					this.props.trips.length > 0 && (
						<TouchableOpacity
							style={styles.resetOption}
							onPress={() => {
								GoAppAnalytics.trackWithProperties("clear_filter", {
									isClearFilter: true
								});
								this.props.dispatch(redbusResetFilter());
							}}>
							<RedbusTextRegular style={styles.bottomText}>
								{I18n.t("clr_filters")}
							</RedbusTextRegular>
						</TouchableOpacity>
					)}
				{this.state.filterModal && (
					<Filter
						close={() => {
							LayoutAnimation.configureNext(
								LayoutAnimation.Presets.easeInEaseOut
							);
							this.setState({ filterModal: false });
						}}
					/>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		source: state.redbus && state.redbus.searchBus.source,
		destination: state.redbus && state.redbus.searchBus.destination,
		travelDate: state.redbus && state.redbus.searchBus.travelDate,
		trips: state.redbus && state.redbus.selectBus.trips,
		tripError: state.redbus && state.redbus.selectBus.tripError,
		filter: state.redbus && state.redbus.selectBus.filter,
		sort: state.redbus && state.redbus.selectBus.sort
	};
}
export default connect(mapStateToProps)(SelectBus);
