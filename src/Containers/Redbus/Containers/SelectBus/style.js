import { StyleSheet, Dimensions, Platform } from "react-native";
import { font_two } from "../../../../Assets/styles";
import { LOW_BLACK } from "../../Constants";

const { width, height } = Dimensions.get("window");

export const indexStyles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#e9e9e9"
	},
	image: {
		width: height / 13,
		height: height / 14
	},
	subHeader: {
		width: width / 1.2,
		height: height / 13.33,
		justifyContent: "space-between",
		flexDirection: "row",
		alignItems: "center"
	},
	headerText: {
		fontSize: height / 35,
		color: "#fff",
		width: width / 1.5
	},
	dateBar: {
		flexDirection: "row",
		width: width / 1.05,
		height: height / 15,
		alignSelf: "center",
		alignItems: "center",
		justifyContent: "space-between"
	},
	dateButton: {
		width: width / 4,
		justifyContent: "center",
		alignItems: "center"
	},
	listItem: {
		width: width / 1.03,
		height: height / 6.2,
		backgroundColor: "#fff",
		...Platform.select({
			android: { elevation: 2 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "black",
				shadowOpacity: 0.1
			}
		}),
		alignSelf: "center",
		flexDirection: "row",
		justifyContent: "space-between",
		paddingLeft: width / 60,
		paddingRight: width / 60,
		borderRadius: 2
	},
	separator: {
		width: width / 1.05,
		height: height / 80,
		backgroundColor: "#e9e9e9"
	},
	bus: {
		width: width / 12,
		height: width / 12,
		marginTop: height / 160
	},
	busInfo: {
		width: width / 1.8,
		paddingVertical: height / 80
	},
	rateInfo: {
		width: width / 4,
		alignItems: "flex-end",
		justifyContent: "space-between",
		paddingVertical: height / 80
	},
	travelAgency: {
		fontSize: height / 37,
		color: "#000"
	},
	busType: {
		fontSize: height / 50,
		width: width / 1.85,
		marginTop: height / 120
	},
	seatBar: {
		flexDirection: "row",
		alignItems: "center",
		marginTop: height / 120
	},
	dot: {
		width: width / 70,
		height: width / 70,
		borderRadius: width / 140,
		backgroundColor: LOW_BLACK,
		marginHorizontal: width / 60
	},
	seatText: {
		fontSize: height / 55
	},
	departure: {
		fontSize: height / 45,
		color: "#000"
	},
	arrival: {
		fontSize: height / 50,
		color: LOW_BLACK,
		marginTop: height / 120
	},
	rate: {
		fontSize: height / 45,
		color: "#C13D4A"
	},
	resetOption: {
		position: "absolute",
		bottom: 0,
		height: height / 14,
		width: width,
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#898C8E"
	},
	bottomText: {
		fontSize: height / 55,
		color: "#fff"
	},
	travelDate: {
		fontSize: height / 47
	},
	renderTripView: {
		width: width / 1.03,
		height: height / 6.2,
		backgroundColor: "#c13d4a",
		borderRadius: width / 100,
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center"
	},
	flexEndAlignment: { alignItems: "flex-end" },
	fullFlex: { flex: 1 },
	tripErrorView: {
		height: height / 2,
		alignItems: "center",
		justifyContent: "center"
	},
	tripErrorImage: {
		width: width / 2,
		height: height / 3,
		alignItems: "center",
		justifyContent: "center"
	}
});

/**
 * styles for Filter.js
 */

export const filterStyles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#e9e9e9"
	},

	subHeader: {
		width: width / 1.2,
		height: height / 13.33,
		justifyContent: "space-between",
		flexDirection: "row",
		alignItems: "center"
	},
	filterByHeader: {
		flexDirection: "row",
		borderWidth: 0,
		borderColor: "#fff",
		width: width / 1.3,
		justifyContent: "flex-end",
		paddingRight: width / 22
	},
	textInput: {
		width: width / 1.5,
		height: height / 13.3,
		fontFamily: font_two,
		fontSize: height / 45,
		color: "#fff"
	},
	searchBar: {
		width: width / 1.4,
		height: height / 13.3,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		alignContent: "center"
	},
	headerText: {
		fontSize: height / 35,
		color: "#fff"
	},
	bar: {
		flexDirection: "row",
		width: width,
		height: height / 15,
		alignSelf: "center",
		alignItems: "center",
		justifyContent: "space-between",
		backgroundColor: "rgba(52, 52, 52, 0.2)"
	},

	barText: {
		fontSize: height / 52,
		marginLeft: width / 42,
		color: "#000"
	},

	iconBar: {
		flexDirection: "row",
		width: width,
		height: height / 10,
		alignItems: "center",
		justifyContent: "space-between"
	},

	icons: {
		alignItems: "center",
		justifyContent: "center",
		width: width / 3,
		height: height / 10,
		borderWidth: 0
	},

	busTypeBar: {
		flexDirection: "row",
		width: width,
		height: height / 12,
		alignItems: "center",
		justifyContent: "space-around",
		borderBottomWidth: 0.5,
		borderBottomColor: "rgba(52, 52, 52, 0.2)"
	},

	busTypeName: {
		alignItems: "center",
		alignSelf: "center",
		justifyContent: "space-around",
		borderWidth: 0,
		width: width / 2,
		height: height / 13
	},

	filterBy: {
		flexDirection: "row",
		width: width,
		height: height / 16,
		justifyContent: "space-between",
		paddingRight: 5,
		paddingLeft: 5,
		borderBottomWidth: 0.5,
		borderBottomColor: "rgba(52, 52, 52, 0.2)",
		alignItems: "center",
		alignSelf: "center"
	},

	bus: {
		width: width / 13,
		height: width / 13
	},

	onlyShow: {
		flexDirection: "row",
		width: width,
		height: height / 12,
		justifyContent: "space-between",
		paddingHorizontal: width / 40,
		borderBottomWidth: 0.5,
		borderBottomColor: "rgba(52, 52, 52, 0.2)",
		alignItems: "center"
	},
	onlyShowInner: {
		flexDirection: "row",
		alignItems: "center"
	},
	onlyShowText: {
		marginLeft: width / 40,
		fontSize: height / 50
	},
	showImage: {
		width: width / 14,
		height: width / 10,
		borderWidth: 0,
		borderColor: "#000"
	},

	footer: {
		justifyContent: "space-around",
		alignItems: "center",
		alignSelf: "center",
		width: width,
		height: height / 14,
		backgroundColor: "#c13d4a"
	},

	departureImage: {
		width: width / 9,
		height: width / 9
	},

	travels: {
		flex: 1,
		width: width,
		height: 50
	},
	arrow: {
		position: "absolute",
		top: height / 30,
		right: width / 30
	},
	filterHeaderText: {
		fontSize: height / 50,
		color: "#fff"
	},
	filterByText: {
		color: "rgba(0,0,0,0.6)",
		fontSize: height / 50,
		paddingTop: 5
	},
	mainView: {
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		position: "absolute",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0
	},
	fullFlex: { flex: 1 },
	rowView: { flexDirection: "row" },
	applyText: {
		color: "white",
		fontSize: height / 50
	},
	activityView: { flex: 1, justifyContent: "center", alignItems: "center" },
	subMainView: {
		position: "absolute",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		zIndex: 2,
		backgroundColor: "white"
	},
	itemTextView: { width: width / 1.3 },
	checkboxTextView: { width: width / 8, marginLeft: width / 20 },
	listTravel: {
		flexDirection: "row",
		width: width,
		height: height / 12,
		justifyContent: "space-between",
		paddingHorizontal: width / 26.3,
		alignItems: "center",
		alignSelf: "center",
		backgroundColor: "#fff",
		paddingLeft: width / 26,
		paddingVertical: width / 25,
		borderBottomWidth: 3,
		borderBottomColor: "rgba(0,0,0,0.2)"
	},
	travelText: {
		color: "#000",
		fontSize: height / 50
	}
});
