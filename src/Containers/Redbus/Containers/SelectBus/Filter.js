import React from "react";
import {
	View,
	Image,
	Dimensions,
	TouchableOpacity,
	FlatList,
	ScrollView,
	Platform,
	UIManager,
	LayoutAnimation,
	BackHandler,
	TextInput
} from "react-native";
import { Header } from "../../Components/";
import {
	FILTERBUS,
	RED_DEALS,
	CANCELLABLE,
	MTICKET,
	MORNING,
	AFTERNOON,
	NIGHT,
	DAWN,
	TRACK,
	RATING,
	FILTERSELECTED
} from "../../Assets/Img/Image";
import { RedbusTextRegular, RedbusTextBold } from "../../Components/RedbusText";
import { connect } from "react-redux";
import { Icon, Seperator, ProgressScreen } from "../../../../Components/";
import CheckBox from "../../Components/CheckBox";
import { NO_SORT, DESCENDING, ASCENDING } from "../../Constants";
import { redbusSaveFilter, redbusSaveSort } from "./Saga";
import { filterStyles as styles } from "./style";
import I18n from "../../Assets/Strings/i18n";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";

const { width, height } = Dimensions.get("window");
const LOW_WHITE = "rgba(255,255,255,0.5)";
const LOW_BLACK = "rgba(0,0,0,0.5)";
const EMPTY = "empty";

/**
 * Component which shows filter options for the trip
 */
class Filter extends React.Component {
	constructor(props) {
		super(props);
		const { filter, sort } = props;
		this.state = {
			filters: {
				...filter,
				travelsTemp: filter.travelsTemp.slice(),
				boardingPointsTemp: filter.boardingPointsTemp.slice(),
				droppingPointsTemp: filter.droppingPointsTemp.slice()
			},
			sort: sort,
			mode: EMPTY,
			showLoader: false,
			modeSearch: false,
			modeSearchText: ""
		};
		if (Platform.OS === "android") {
			UIManager.setLayoutAnimationEnabledExperimental(true);
		}
		this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
			//if another inner screen is not open close this modal
			if (this.state.mode === EMPTY) {
				this.props.close();
			} else {
				//close the search screen or another screen
				if (!this.state.modeSearch) {
					this.setState({ mode: EMPTY });
				} else {
					this.setState({ modeSearch: false });
				}
			}
			return true;
		});
	}

	/**
	 * parse the variables and get all required properties that should be sent to
	 * the mixpanel for goapp analytics
	 * @return {[Object]} [properties saved for analytics]
	 */
	getPropertyForGoAppAnalytics = () => {
		let obj = {};
		console.log("filter are", this.state.filters);
		Object.keys(this.state.filters).map(key => {
			if (
				key !== "travels" &&
				key !== "boardingPoints" &&
				key !== "droppingPoints" &&
				this.state.filters[key] !== null
			) {
				if (this.state.filters[key].length > 0) {
					obj[key] = this.state.filters[key];
				}
			}
		});
		Object.keys(this.state.sort).map(key => {
			if (this.state.sort[key] !== NO_SORT) {
				obj[key] = this.state.sort[key];
			}
		});
		return obj;
	};

	/**
	 * [componentDidUpdate lifecycle method called after a component updates]
	 * @param  {[Object]} prevProps
	 * @param  {[Object]} prevState
	 */
	componentDidUpdate(prevProps, prevState) {
		if (this.state.showLoader && !prevState.showLoader) {
			let filtered_props = this.getPropertyForGoAppAnalytics();
			GoAppAnalytics.trackWithProperties("apply_filter", filtered_props);
			this.props.dispatch(redbusSaveFilter(this.state.filters));
			this.props.dispatch(redbusSaveSort(this.state.sort));
		}
	}

	componentWillUnmount() {
		this.backHandler.remove();
	}
	/**
	 * find all available travels , boarding points or dropping points
	 * for a trip
	 */
	travelList() {
		let data = [];
		//find travles for trip
		if (this.state.mode == "travels") {
			this.props.trips.map(item => {
				if (data.indexOf(item.travels) > -1) {
				} else {
					if (item.travels.trim() !== "") {
						data.push(item.travels);
					}
				}
			});
		} else if (this.state.mode == "boardingPoints") {
			//find boarding points for trip
			this.props.trips.map(item => {
				if (item.boardingTimes) {
					if (item.boardingTimes instanceof Array) {
						item.boardingTimes.map(item => {
							if (data.indexOf(item.bpName) === -1) {
								if (item.bpName.trim() !== "") {
									data.push(item.bpName);
								}
							}
						});
					} else {
						if (data.indexOf(item.boardingTimes.bpName) === -1) {
							if (item.boardingTimes.bpName.trim() !== "") {
								data.push(item.boardingTimes.bpName);
							}
						}
					}
				}
			});
		} else if (this.state.mode == "droppingPoints") {
			//find dropping points for trip
			this.props.trips.map(item => {
				if (item.droppingTimes) {
					if (item.droppingTimes instanceof Array) {
						item.droppingTimes.map(item => {
							if (data.indexOf(item.bpName) === -1) {
								if (item.bpName.trim() !== "") {
									data.push(item.bpName);
								}
							}
						});
					} else if (!(item.droppingTimes instanceof Array)) {
						if (data.indexOf(item.droppingTimes.bpName) === -1) {
							if (item.droppingTimes.bpName.trim() !== "") {
								data.push(item.droppingTimes.bpName);
							}
						}
					}
				}
			});
		}
		//remove all trailing spaces from the filtered list
		if (this.state.modeSearchText.trim() !== "") {
			data = data.filter(
				row =>
					row
						.toLowerCase()
						.search(this.state.modeSearchText.toLowerCase().trim()) >= 0
			);
		}
		return data.sort((a, b) =>
			a
				.trim()
				.toLowerCase()
				.localeCompare(b.trim().toLowerCase())
		);
	}

	/**
	 * set sorting type for the trip
	 * @param  {[integer]} type [sorting type for the trips]
	 */
	setSorting = type => {
		if (this.state.sort[type] === NO_SORT) {
			return DESCENDING;
		} else if (this.state.sort[type] === DESCENDING) {
			return ASCENDING;
		} else {
			return NO_SORT;
		}
	};

	render() {
		return (
			<View style={styles.mainView}>
				{!this.state.showLoader ? (
					<View style={styles.container}>
						<Header
							goBack={true}
							callOnBack={this.props.close}
							iconHeight={height / 38}>
							<View style={styles.subHeader}>
								<RedbusTextBold style={styles.headerText}>
									{I18n.t("filters")}
								</RedbusTextBold>
							</View>
						</Header>
						<ScrollView style={styles.fullFlex}>
							<View style={styles.bar}>
								<RedbusTextBold style={styles.barText}>
									{I18n.t("sort_by")}
								</RedbusTextBold>
							</View>
							<View style={styles.iconBar}>
								<TouchableOpacity
									style={styles.icons}
									onPress={() =>
										this.setState({
											sort: {
												...this.state.sort,
												rating: this.setSorting("rating"),
												departure: NO_SORT,
												fare: NO_SORT
											}
										})
									}>
									<Icon
										iconType={"font_awesome"}
										iconSize={height / 30}
										iconName={
											this.state.sort.rating === NO_SORT ? "star-o" : "star"
										}
										iconColor={LOW_BLACK}
									/>
									<RedbusTextRegular
										style={[
											styles.sortText,
											{
												color:
													this.state.sort.rating !== NO_SORT ? "#c13d4a" : null
											}
										]}>
										{I18n.t("ratings")}
									</RedbusTextRegular>
									{this.state.sort.rating !== NO_SORT && (
										<View style={styles.arrow}>
											<Icon
												iconType={"ionicon"}
												iconSize={height / 40}
												iconName={
													this.state.sort.rating === DESCENDING
														? "ios-arrow-down"
														: "ios-arrow-up"
												}
												iconColor={LOW_BLACK}
											/>
										</View>
									)}
								</TouchableOpacity>
								<Seperator
									width={1}
									height={height / 10}
									color={"rgba(52, 52, 52, 0.2)"}
								/>
								<TouchableOpacity
									style={styles.icons}
									onPress={() =>
										this.setState({
											sort: {
												...this.state.sort,
												departure: this.setSorting("departure"),
												rating: NO_SORT,
												fare: NO_SORT
											}
										})
									}>
									<Icon
										iconType={
											this.state.sort.departure === NO_SORT
												? "font_awesome"
												: "ionicon"
										}
										iconSize={height / 30}
										iconName={
											this.state.sort.departure === NO_SORT
												? "clock-o"
												: "ios-time"
										}
										iconColor={LOW_BLACK}
									/>
									<RedbusTextRegular
										style={[
											styles.sortText,
											{
												color:
													this.state.sort.departure !== NO_SORT
														? "#c13d4a"
														: null
											}
										]}>
										{I18n.t("departure")}
									</RedbusTextRegular>
									{this.state.sort.departure !== NO_SORT && (
										<View style={styles.arrow}>
											<Icon
												iconType={"ionicon"}
												iconSize={height / 40}
												iconName={
													this.state.sort.departure === DESCENDING
														? "ios-arrow-down"
														: "ios-arrow-up"
												}
												iconColor={LOW_BLACK}
											/>
										</View>
									)}
								</TouchableOpacity>
								<Seperator
									width={1}
									height={height / 10}
									color={"rgba(52, 52, 52, 0.2)"}
								/>
								<TouchableOpacity
									style={styles.icons}
									onPress={() =>
										this.setState({
											sort: {
												...this.state.sort,
												fare: this.setSorting("fare"),
												departure: NO_SORT,
												rating: NO_SORT
											}
										})
									}>
									<Icon
										iconType={"font_awesome"}
										iconSize={height / 25}
										iconName={"money"}
										iconColor={LOW_BLACK}
									/>
									<RedbusTextRegular
										style={[
											styles.sortText,
											{
												color:
													this.state.sort.fare !== NO_SORT ? "#c13d4a" : null
											}
										]}>
										{I18n.t("fare")}
									</RedbusTextRegular>
									{this.state.sort.fare !== NO_SORT && (
										<View style={styles.arrow}>
											<Icon
												iconType={"ionicon"}
												iconSize={height / 40}
												iconName={
													this.state.sort.fare === DESCENDING
														? "ios-arrow-down"
														: "ios-arrow-up"
												}
												iconColor={LOW_BLACK}
											/>
										</View>
									)}
								</TouchableOpacity>
							</View>

							<View style={styles.bar}>
								<RedbusTextBold style={styles.barText}>
									{I18n.t("bus_type")}
								</RedbusTextBold>
							</View>

							<View style={styles.busTypeBar}>
								<TouchableOpacity
									style={styles.busTypeName}
									onPress={() => {
										if (this.state.filters.AC === null) {
											this.setState({
												filters: { ...this.state.filters, AC: true }
											});
										} else {
											this.setState({
												filters: {
													...this.state.filters,
													AC: null
												}
											});
										}
									}}>
									<RedbusTextRegular
										style={[
											{ color: this.state.filters.AC ? "#c13D4A" : LOW_BLACK }
										]}>
										{I18n.t("ac")}
									</RedbusTextRegular>
								</TouchableOpacity>
								<Seperator
									width={1}
									height={height / 20}
									color={"rgba(52, 52, 52, 0.2)"}
								/>
								<TouchableOpacity
									style={styles.busTypeName}
									onPress={() => {
										if (this.state.filters.nonAC === null) {
											this.setState({
												filters: { ...this.state.filters, nonAC: true }
											});
										} else {
											this.setState({
												filters: {
													...this.state.filters,
													nonAC: null
												}
											});
										}
									}}>
									<RedbusTextRegular
										style={[
											{
												color: this.state.filters.nonAC ? "#c13D4A" : LOW_BLACK
											}
										]}>
										{I18n.t("non_ac")}
									</RedbusTextRegular>
								</TouchableOpacity>
							</View>

							<View style={styles.busTypeBar}>
								<TouchableOpacity
									style={styles.busTypeName}
									onPress={() => {
										if (this.state.filters.seater === null) {
											this.setState({
												filters: { ...this.state.filters, seater: true }
											});
										} else {
											this.setState({
												filters: {
													...this.state.filters,
													seater: null
												}
											});
										}
									}}>
									<RedbusTextRegular
										style={[
											{
												color: this.state.filters.seater ? "#c13D4A" : LOW_BLACK
											}
										]}>
										{I18n.t("seater")}
									</RedbusTextRegular>
								</TouchableOpacity>
								<Seperator
									width={1}
									height={height / 20}
									color={"rgba(52, 52, 52, 0.2)"}
								/>
								<TouchableOpacity
									style={styles.busTypeName}
									onPress={() => {
										if (this.state.filters.sleeper === null) {
											this.setState({
												filters: { ...this.state.filters, sleeper: true }
											});
										} else {
											this.setState({
												filters: {
													...this.state.filters,
													sleeper: null
												}
											});
										}
									}}>
									<RedbusTextRegular
										style={[
											{
												color: this.state.filters.sleeper
													? "#c13D4A"
													: LOW_BLACK
											}
										]}>
										{I18n.t("sleeper")}
									</RedbusTextRegular>
								</TouchableOpacity>
							</View>

							<View style={styles.bar}>
								<RedbusTextBold style={styles.barText}>
									{I18n.t("filter_by")}
								</RedbusTextBold>
							</View>
							<TouchableOpacity
								style={styles.filterBy}
								onPress={() => {
									LayoutAnimation.configureNext(
										LayoutAnimation.Presets.easeInEaseOut
									);
									this.setState({
										mode: "travels"
									});
								}}>
								<View style={styles.rowView}>
									<Image
										style={styles.bus}
										source={
											this.state.filters.travelsTemp.length > 0
												? FILTERSELECTED
												: FILTERBUS
										}
									/>
									<RedbusTextRegular style={[styles.filterByText]}>
										{I18n.t("travels")}
									</RedbusTextRegular>
								</View>
								<View>
									<Icon
										iconType={"font_awesome"}
										iconSize={height / 25}
										iconName={"angle-down"}
										iconColor={LOW_BLACK}
									/>
								</View>
							</TouchableOpacity>

							<TouchableOpacity
								style={styles.filterBy}
								onPress={() => {
									LayoutAnimation.configureNext(
										LayoutAnimation.Presets.easeInEaseOut
									);
									this.setState({
										mode: "boardingPoints"
									});
								}}>
								<View style={styles.rowView}>
									<Icon
										iconType={"evilicon"}
										iconSize={height / 25}
										iconName={"location"}
										iconColor={
											this.state.filters.boardingPointsTemp.length > 0
												? "#71a200"
												: LOW_BLACK
										}
									/>
									<RedbusTextRegular style={[styles.filterByText]}>
										{I18n.t("boarding_points")}
									</RedbusTextRegular>
								</View>
								<View>
									<Icon
										iconType={"font_awesome"}
										iconSize={height / 25}
										iconName={"angle-down"}
										iconColor={LOW_BLACK}
									/>
								</View>
							</TouchableOpacity>

							<TouchableOpacity
								style={styles.filterBy}
								onPress={() => {
									LayoutAnimation.configureNext(
										LayoutAnimation.Presets.easeInEaseOut
									);
									this.setState({
										mode: "droppingPoints"
									});
								}}>
								<View style={styles.rowView}>
									<Icon
										iconType={"evilicon"}
										iconSize={height / 25}
										iconName={"location"}
										iconColor={
											this.state.filters.droppingPointsTemp.length > 0
												? "#71a200"
												: LOW_BLACK
										}
									/>
									<RedbusTextRegular style={[styles.filterByText]}>
										{I18n.t("dropping_points")}
									</RedbusTextRegular>
								</View>
								<View>
									<Icon
										iconType={"font_awesome"}
										iconSize={height / 25}
										iconName={"angle-down"}
										iconColor={LOW_BLACK}
									/>
								</View>
							</TouchableOpacity>

							<View style={styles.bar}>
								<RedbusTextBold style={styles.barText}>
									{I18n.t("only_show")}
								</RedbusTextBold>
							</View>

							<TouchableOpacity
								style={styles.onlyShow}
								onPress={() => {
									if (this.state.filters.liveTracking === null) {
										this.setState({
											filters: { ...this.state.filters, liveTracking: true }
										});
									} else {
										this.setState({
											filters: {
												...this.state.filters,
												liveTracking: null
											}
										});
									}
								}}>
								<View style={styles.onlyShowInner}>
									<Image
										style={styles.showImage}
										source={TRACK}
										resizeMode={"contain"}
									/>
									<RedbusTextRegular style={styles.onlyShowText}>
										{I18n.t("live_tracking")}
									</RedbusTextRegular>
								</View>
								<CheckBox
									width={width / 25}
									height={width / 25}
									borderRadius={width / 150}
									checkColor={"#c13d4a"}
									value={this.state.filters.liveTracking}
								/>
							</TouchableOpacity>
							<TouchableOpacity
								style={styles.onlyShow}
								onPress={() => {
									if (this.state.filters.redDeals === null) {
										this.setState({
											filters: { ...this.state.filters, redDeals: true }
										});
									} else {
										this.setState({
											filters: {
												...this.state.filters,
												redDeals: null
											}
										});
									}
								}}>
								<View style={styles.onlyShowInner}>
									<Image
										style={styles.showImage}
										source={RED_DEALS}
										resizeMode={"contain"}
									/>
									<RedbusTextRegular style={styles.onlyShowText}>
										{I18n.t("red_deals")}
									</RedbusTextRegular>
								</View>
								<CheckBox
									width={width / 25}
									height={width / 25}
									borderRadius={width / 150}
									checkColor={"#c13d4a"}
									value={this.state.filters.redDeals}
								/>
							</TouchableOpacity>
							<TouchableOpacity
								style={styles.onlyShow}
								onPress={() => {
									if (this.state.filters.highRatedBus === null) {
										this.setState({
											filters: { ...this.state.filters, highRatedBus: true }
										});
									} else {
										this.setState({
											filters: {
												...this.state.filters,
												highRatedBus: null
											}
										});
									}
								}}>
								<View style={styles.onlyShowInner}>
									<Image
										style={styles.showImage}
										source={RATING}
										resizeMode={"contain"}
									/>
									<RedbusTextRegular style={styles.onlyShowText}>
										{I18n.t("high_rated_bus")}
									</RedbusTextRegular>
								</View>
								<CheckBox
									width={width / 25}
									height={width / 25}
									borderRadius={width / 150}
									checkColor={"#c13d4a"}
									value={this.state.filters.highRatedBus}
								/>
							</TouchableOpacity>
							<TouchableOpacity
								style={styles.onlyShow}
								onPress={() => {
									if (this.state.filters.cancellable === null) {
										this.setState({
											filters: { ...this.state.filters, cancellable: true }
										});
									} else {
										this.setState({
											filters: {
												...this.state.filters,
												cancellable: null
											}
										});
									}
								}}>
								<View style={styles.onlyShowInner}>
									<Image
										style={styles.showImage}
										source={CANCELLABLE}
										resizeMode={"contain"}
									/>
									<RedbusTextRegular style={styles.onlyShowText}>
										{I18n.t("cancellable")}
									</RedbusTextRegular>
								</View>
								<CheckBox
									width={width / 25}
									height={width / 25}
									borderRadius={width / 150}
									checkColor={"#c13d4a"}
									value={this.state.filters.cancellable}
								/>
							</TouchableOpacity>
							<TouchableOpacity
								style={styles.onlyShow}
								onPress={() => {
									if (this.state.filters.mTicket === null) {
										this.setState({
											filters: { ...this.state.filters, mTicket: true }
										});
									} else {
										this.setState({
											filters: {
												...this.state.filters,
												mTicket: null
											}
										});
									}
								}}>
								<View style={styles.onlyShowInner}>
									<Image
										style={styles.showImage}
										source={MTICKET}
										resizeMode={"contain"}
									/>
									<RedbusTextRegular style={styles.onlyShowText}>
										{I18n.t("m_ticket")}
									</RedbusTextRegular>
								</View>
								<CheckBox
									width={width / 25}
									height={width / 25}
									borderRadius={width / 150}
									checkColor={"#c13d4a"}
									value={this.state.filters.mTicket}
								/>
							</TouchableOpacity>

							<View style={styles.bar}>
								<RedbusTextBold style={styles.barText}>
									{I18n.t("filter_by_dep_time")}
								</RedbusTextBold>
							</View>
							<View style={styles.busTypeBar}>
								<TouchableOpacity
									style={styles.busTypeName}
									onPress={() => {
										if (this.state.filters.morning === null) {
											this.setState({
												filters: { ...this.state.filters, morning: true }
											});
										} else {
											this.setState({
												filters: {
													...this.state.filters,
													morning: null
												}
											});
										}
									}}>
									<Image
										style={styles.departureImage}
										source={MORNING}
										resizeMode={"contain"}
									/>
									<RedbusTextRegular
										style={[
											{
												color: this.state.filters.morning
													? "#c13D4A"
													: LOW_BLACK
											}
										]}>
										{"06:00 " + I18n.t("am") + " - 12:00 " + I18n.t("pm")}
									</RedbusTextRegular>
								</TouchableOpacity>
								<Seperator
									width={1}
									height={height / 20}
									color={"rgba(52, 52, 52, 0.2)"}
								/>
								<TouchableOpacity
									style={styles.busTypeName}
									onPress={() => {
										if (this.state.filters.afternoon === null) {
											this.setState({
												filters: { ...this.state.filters, afternoon: true }
											});
										} else {
											this.setState({
												filters: {
													...this.state.filters,
													afternoon: null
												}
											});
										}
									}}>
									<Image
										style={styles.departureImage}
										source={AFTERNOON}
										resizeMode={"contain"}
									/>
									<RedbusTextRegular
										style={[
											{
												color: this.state.filters.afternoon
													? "#c13D4A"
													: LOW_BLACK
											}
										]}>
										{"12:00 " + I18n.t("pm") + " - 06:00 " + I18n.t("pm")}
									</RedbusTextRegular>
								</TouchableOpacity>
							</View>

							<View style={styles.busTypeBar}>
								<TouchableOpacity
									style={styles.busTypeName}
									onPress={() => {
										if (this.state.filters.evening === null) {
											this.setState({
												filters: { ...this.state.filters, evening: true }
											});
										} else {
											this.setState({
												filters: {
													...this.state.filters,
													evening: null
												}
											});
										}
									}}>
									<Image
										style={styles.departureImage}
										source={NIGHT}
										resizeMode={"contain"}
									/>
									<RedbusTextRegular
										style={[
											{
												color: this.state.filters.evening
													? "#c13D4A"
													: LOW_BLACK
											}
										]}>
										{"06:00 " + I18n.t("pm") + " - 12:00 " + I18n.t("am")}
									</RedbusTextRegular>
								</TouchableOpacity>
								<Seperator
									width={1}
									height={height / 20}
									color={"rgba(52, 52, 52, 0.2)"}
								/>
								<TouchableOpacity
									style={styles.busTypeName}
									onPress={() => {
										if (this.state.filters.night === null) {
											this.setState({
												filters: { ...this.state.filters, night: true }
											});
										} else {
											this.setState({
												filters: {
													...this.state.filters,
													night: null
												}
											});
										}
									}}>
									<Image
										style={styles.departureImage}
										source={DAWN}
										resizeMode={"contain"}
									/>
									<RedbusTextRegular
										style={[
											{
												color: this.state.filters.night ? "#c13D4A" : LOW_BLACK
											}
										]}>
										{"12:00 " + I18n.t("am") + " - 06:00 " + I18n.t("am")}
									</RedbusTextRegular>
								</TouchableOpacity>
							</View>
						</ScrollView>
						<TouchableOpacity
							style={styles.footer}
							onPress={() => {
								LayoutAnimation.configureNext(
									LayoutAnimation.Presets.easeInEaseOut
								);
								this.setState({ showLoader: true });
							}}>
							<RedbusTextBold style={styles.applyText}>
								{I18n.t("apply")}
							</RedbusTextBold>
						</TouchableOpacity>
					</View>
				) : (
					<View style={styles.activityView}>
						<ProgressScreen indicatorColor={"#C13D4A"} indicatorSize={50} />
					</View>
				)}
				{this.state.mode !== "empty" && (
					<View style={styles.subMainView}>
						<Header
							goBack={true}
							iconHeight={height / 38}
							callOnBack={() => {
								LayoutAnimation.configureNext(
									LayoutAnimation.Presets.easeInEaseOut
								);
								if (!this.state.modeSearch) {
									this.setState({ mode: "empty" });
								} else {
									this.setState({ modeSearch: false });
								}
							}}>
							<View style={styles.filterByHeader}>
								{this.state.modeSearch ? (
									<View style={styles.searchBar}>
										<Icon
											iconType={"material"}
											iconSize={height / 27}
											iconName={"search"}
											iconColor={"#fff"}
										/>
										<TextInput
											underlineColorAndroid={"#fff"}
											style={styles.textInput}
											autoFocus={true}
											placeholder={I18n.t("enter_" + this.state.mode)}
											placeholderTextColor={"#fff"}
											value={this.state.text}
											onChangeText={text =>
												this.setState({ modeSearchText: text })
											}
											selectionColor={LOW_WHITE}
										/>
									</View>
								) : (
									<TouchableOpacity
										onPress={() => this.setState({ modeSearch: true })}>
										<Icon
											iconType={"material"}
											iconSize={height / 27}
											iconName={"search"}
											iconColor={"#fff"}
										/>
									</TouchableOpacity>
								)}
							</View>
						</Header>
						<FlatList
							removeClippedSubviews
							style={{ height: height }}
							data={this.travelList()}
							extraData={this.state}
							keyExtractor={(item, index) => index.toString()}
							renderItem={({ item, index }) => {
								return (
									<TouchableOpacity
										style={styles.listTravel}
										onPress={() => {
											if (this.state.mode == "travels") {
												if (
													this.state.filters.travelsTemp.indexOf(item) == -1
												) {
													let temp = this.state.filters.travelsTemp;
													temp.push(item);
													this.setState({
														filters: {
															...this.state.filters,
															travelsTemp: temp
														}
													});
												} else if (
													this.state.filters.travelsTemp.indexOf(item) > -1
												) {
													let temp2 = this.state.filters.travelsTemp;
													for (var i = 0; i < temp2.length; i++) {
														if (temp2[i] === item) {
															temp2.splice(i, 1);
														}
													}
													this.setState({
														filters: {
															...this.state.filters,
															travelsTemp: temp2
														}
													});
												}
											}

											if (this.state.mode == "boardingPoints") {
												if (
													this.state.filters.boardingPointsTemp.indexOf(item) ==
													-1
												) {
													let temp = this.state.filters.boardingPointsTemp;
													temp.push(item);
													this.setState({
														filters: {
															...this.state.filters,
															boardingPointsTemp: temp
														}
													});
												} else if (
													this.state.filters.boardingPointsTemp.indexOf(item) >
													-1
												) {
													let temp2 = this.state.filters.boardingPointsTemp;

													for (var i = 0; i < temp2.length; i++) {
														if (temp2[i] === item) {
															temp2.splice(i, 1);
														}
													}
													this.setState({
														filters: {
															...this.state.filters,
															boardingPointsTemp: temp2
														}
													});
												}
											}

											if (this.state.mode == "droppingPoints") {
												if (
													this.state.filters.droppingPointsTemp.indexOf(item) ==
													-1
												) {
													let temp = this.state.filters.droppingPointsTemp;
													temp.push(item);
													this.setState({
														filters: {
															...this.state.filters,
															droppingPointsTemp: temp
														}
													});
												} else if (
													this.state.filters.droppingPointsTemp.indexOf(item) >
													-1
												) {
													let temp2 = this.state.filters.droppingPointsTemp;
													for (var i = 0; i < temp2.length; i++) {
														if (temp2[i] === item) {
															temp2.splice(i, 1);
														}
													}
													this.setState({
														filters: {
															...this.state.filters,
															droppingPointsTemp: temp2
														}
													});
												}
											}
										}}>
										<View style={styles.itemTextView}>
											<RedbusTextRegular style={styles.travelText}>
												{item}
											</RedbusTextRegular>
										</View>
										<View style={styles.checkboxTextView}>
											<CheckBox
												width={width / 25}
												height={width / 25}
												borderRadius={width / 150}
												checkColor={"#c13d4a"}
												value={
													this.state.filters.travelsTemp.indexOf(item) > -1 ||
													this.state.filters.boardingPointsTemp.indexOf(item) >
														-1 ||
													this.state.filters.droppingPointsTemp.indexOf(item) >
														-1
												}
											/>
										</View>
									</TouchableOpacity>
								);
							}}
						/>
					</View>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		trips: state.redbus && state.redbus.selectBus.trips,
		filter: state.redbus && state.redbus.selectBus.filter,
		sort: state.redbus && state.redbus.selectBus.sort
	};
}
export default connect(mapStateToProps)(Filter);
