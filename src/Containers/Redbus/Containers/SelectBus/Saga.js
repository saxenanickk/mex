import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api/";
import { NO_NETWORK_ERROR } from "../../Constants";
import { redbusNoNetwork } from "../../Saga";
export const REDBUS_GET_ALL_TRIP = "REDBUS_GET_ALL_TRIP";
export const REDBUS_SAVE_ALL_TRIP = "REDBUS_SAVE_ALL_TRIP";
export const REDBUS_SAVE_SERVER_ERROR = "REDBUS_SAVE_SERVER_ERROR";
export const REDBUS_SAVE_FILTER = "REDBUS_SAVE_FILTER";
export const REDBUS_RESET_FILTER = "REDBUS_RESET_FILTER";
export const REDBUS_SAVE_SORT = "REDBUS_SAVE_SORT";

//call api to fetch trips
export const redbusGetAllTrip = payload => ({
	type: REDBUS_GET_ALL_TRIP,
	payload
});

//save the trips to the store
export const redbusSaveAllTrip = payload => ({
	type: REDBUS_SAVE_ALL_TRIP,
	payload
});

//save sever error to the store
export const redbusSaveServerError = payload => ({
	type: REDBUS_SAVE_SERVER_ERROR,
	payload
});

//save filters to the store
export const redbusSaveFilter = payload => ({
	type: REDBUS_SAVE_FILTER,
	payload
});

//reset all the filters
export const redbusResetFilter = payload => ({
	type: REDBUS_RESET_FILTER,
	payload
});

//save sort selection to the store
export const redbusSaveSort = payload => ({
	type: REDBUS_SAVE_SORT,
	payload
});

//saga handls fetching of trip
export function* selectBusSaga(dispatch) {
	yield takeLatest(REDBUS_GET_ALL_TRIP, handleGetAllTrip);
}

//handles fetching the trip
function* handleGetAllTrip(action) {
	try {
		let response = yield call(Api.getAllAvailableBus, action.payload);
		if (response.error && response.errorCode === NO_NETWORK_ERROR) {
			//if there is network error then show no network message
			yield put(
				redbusNoNetwork({
					actionType: action.type,
					payload: action.payload
				})
			);
			return;
		} else {
			if (response.success && response.success === 1) {
				//fetch successfully fetched trip to store
				if (response.data !== null) {
					yield put(redbusSaveAllTrip(response.data.availableTrips));
				} else {
					yield put(redbusSaveAllTrip([]));
				}
			} else {
				//update the error status
				yield put(redbusSaveServerError(true));
			}
		}
	} catch (error) {
		console.log("error=>", error);
		yield put(redbusSaveServerError(true));
	}
}
