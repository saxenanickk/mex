import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const indexStyles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff"
	},
	headerText: {
		color: "#fff",
		fontSize: height / 40
	},
	button: {
		width: width / 2,
		height: height / 10,
		borderRightWidth: 1,
		borderRightColor: "#DFDDE0",
		borderBottomWidth: 1,
		borderBottomColor: "#DFDDE0",
		justifyContent: "space-between",
		alignItems: "center"
	},
	buttonText: {
		color: "#000",
		fontSize: height / 40
	},
	barWithBoarding: {
		width: width / 2,
		height: height / 150,
		backgroundColor: "#C13D4A"
	},
	barWithoutBoarding: {
		width: width / 2,
		height: height / 150,
		backgroundColor: "#fff"
	},
	location: {
		width: width,
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: width / 30,
		paddingVertical: width / 30,
		borderBottomWidth: 2,
		borderBottomColor: "#DFDDE0"
	},
	locationText: {
		marginLeft: width / 30,
		width: width / 1.7,
		fontSize: height / 60
	},
	circle: {
		width: width / 20,
		height: width / 20,
		borderRadius: width / 40,
		justifyContent: "center",
		alignItems: "center",
		borderWidth: 1,
		borderColor: "#DFDDE0"
	},
	innerCircle: {
		width: width / 30,
		height: width / 30,
		borderRadius: width / 60,
		backgroundColor: "#C13D4A"
	},
	locationtime: {
		marginLeft: width / 50,
		fontSize: height / 60,
		color: "rgba(0,0,0,0.7)",
		width: width / 4
	},
	subLocation: {
		flexDirection: "row",
		width: width / 1.5
	},
	miniLocationText: {
		fontSize: height / 53,
		color: "rgba(0,0,0,0.7)",
		marginLeft: width / 30,
		width: width / 1.8
	},
	subHeader: {
		width: width / 1.2,
		flexDirection: "row",
		justifyContent: "space-between"
	},
	nextButton: {
		width: width / 6
	},
	buttonLocationText: {
		fontSize: height / 50,
		color: "rgba(0,0,0,0.7)",
		textAlign: "center"
	},
	nextText: {
		color: "#fff",
		fontSize: height / 50,
		marginLeft: width / 20
	},
	rowView: { flexDirection: "row" },
	locationView: {
		width: width / 1.7
	}
});
