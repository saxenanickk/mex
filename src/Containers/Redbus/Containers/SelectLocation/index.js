/* eslint-disable radix */
import React from "react";
import {
	View,
	Dimensions,
	ScrollView,
	TouchableOpacity,
	LayoutAnimation,
	NativeModules,
	Platform
} from "react-native";
import { connect } from "react-redux";
import { Header } from "../../Components";
import {
	redbusSelectBoardingPoint,
	redbusSelectDroppingPoint
} from "../SelectSeat/Saga";
import { RedbusTextRegular } from "../../Components/RedbusText";
import { ONE_DAY_EPOCH_DURATION, DATE, MONTH } from "../../Constants";
import I18n from "../../Assets/Strings/i18n";
import { indexStyles as styles } from "./style";
import ViewPager from "@react-native-community/viewpager";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";

const { height } = Dimensions.get("window");

const { UIManager } = NativeModules;

if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}
/**
 * SelectLocation:screen to select the boarding and dropping point
 * functionality:
 * 1.select boarding point
 * 2.select dropping point
 */
class SelectLocation extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isBoarding: true
		};

		this.viewPager = React.createRef();
	}

	shouldComponentUpdate(props, state) {
		return true;
	}

	componentWillUnmount() {
		this.props.dispatch(redbusSelectBoardingPoint(null));
		this.props.dispatch(redbusSelectDroppingPoint(null));
	}

	/**
	 * get the name of source and destination as string
	 * @return {[String]} [$source to $destination]
	 */
	getTravelCity = () => {
		return (
			this.props.source.city_name + " to " + this.props.destination.city_name
		);
	};

	/**
	 * get all available boarding points for the trip
	 * @return {[Array]} [list of boarding point]
	 */
	getBoardingPoints = () => {
		let boardingPoint = [];
		//if boardingtimes is available then check available boarding time
		if (this.props.selectedBus.boardingTimes) {
			//check for array
			if (this.props.selectedBus.boardingTimes instanceof Array) {
				return this.props.selectedBus.boardingTimes;
			} else {
				//check for objecyt
				boardingPoint.push(this.props.selectedBus.boardingTimes);
				return boardingPoint;
			}
		}
		return boardingPoint;
	};

	/**
	 * get all available dropping points for a trip
	 * @return {[Array]} [lisrt of all dropping points]
	 */
	getDroppingPoint = () => {
		let droppingPoint = [];
		if (this.props.selectedBus.droppingTimes) {
			if (this.props.selectedBus.droppingTimes instanceof Array) {
				return this.props.selectedBus.droppingTimes;
			} else {
				droppingPoint.push(this.props.selectedBus.droppingTimes);
				return droppingPoint;
			}
		}
		return droppingPoint;
	};

	/**
	 * get the time in human readable format
	 * @param  {[String]} stringTime [time inredbus format]
	 * @return {[typString]}            [$HH:$MM]
	 */
	getTime = stringTime => {
		let time = "";
		let timeType = "";
		let integerTime = parseInt(stringTime);
		let hour = parseInt(integerTime / 60);
		let minute = parseInt(integerTime % 60);
		let day = parseInt(hour / 24);
		let date = "";
		if (day > 0) {
			let departDate = new Date(
				new Date(this.props.selectedBus.doj).getTime() +
					day * ONE_DAY_EPOCH_DURATION
			)
				.toDateString()
				.split(" ");
			date = " (" + departDate[DATE] + "-" + departDate[MONTH] + ")";
		}
		hour = hour >= 24 ? parseInt(hour % 24) : hour;
		if (hour < 12) {
			timeType = "AM";
		} else if (hour >= 12) {
			hour = hour > 12 ? hour - 12 : hour;
			timeType = "PM";
		}
		time += hour < 10 ? "0" + hour + ":" : hour + ":";
		time += minute < 10 ? "0" + minute : minute;
		time += " " + timeType;
		return time + date;
	};

	/**
	 * change index from boarding to dropping point
	 */
	changeSelectedIndex = index => {
		if (index === 0) {
			this.setState({ isBoarding: true });
		} else {
			this.setState({ isBoarding: false });
		}
	};

	/**
	 * [check whether dropping point is availble or not]
	 * @return {[boolean]} [true if dropping point available else false]
	 */
	checkDroppingPointAvailable = () => {
		console.log("==>", this.props.selectedBus.droppingTimes);
		if (this.props.selectedBus.droppingTimes === undefined) {
			return false;
		}
		return true;
	};
	render() {
		return (
			<View style={styles.container}>
				<Header
					goBack={true}
					iconHeight={height / 38}
					callOnBack={() => {
						this.props.navigation.goBack();
					}}>
					<View style={styles.subHeader}>
						<RedbusTextRegular style={styles.headerText}>
							{this.getTravelCity()}
						</RedbusTextRegular>
						{this.props.selectedBoardingPoint &&
							this.props.selectedDroppingPoint && (
								<TouchableOpacity
									style={styles.nextButton}
									onPress={() =>
										this.props.navigation.navigate("TravellerDetail")
									}>
									<RedbusTextRegular style={styles.nextText}>
										{I18n.t("next")}
									</RedbusTextRegular>
								</TouchableOpacity>
							)}
					</View>
				</Header>
				<View style={styles.rowView}>
					<TouchableOpacity
						style={styles.button}
						onPress={() => {
							LayoutAnimation.easeInEaseOut();
							this.viewPager.current.setPage(0);
						}}>
						<RedbusTextRegular style={styles.buttonText}>
							{I18n.t("boarding")}
						</RedbusTextRegular>
						<RedbusTextRegular style={styles.buttonLocationText}>
							{this.props.selectedBoardingPoint === null
								? I18n.t("add_location")
								: this.props.selectedBoardingPoint.bpName}
						</RedbusTextRegular>
						<View
							style={[
								styles.bar,
								this.state.isBoarding
									? styles.barWithBoarding
									: styles.barWithoutBoarding
							]}
						/>
					</TouchableOpacity>
					{/* only render the dropping point location is dropping points available for that particular trip*/}
					{this.checkDroppingPointAvailable() && (
						<TouchableOpacity
							style={styles.button}
							onPress={() => {
								LayoutAnimation.easeInEaseOut();
								this.viewPager.current.setPage(1);
							}}>
							<RedbusTextRegular style={styles.buttonText}>
								{I18n.t("dropping")}
							</RedbusTextRegular>
							<RedbusTextRegular style={styles.buttonLocationText}>
								{this.props.selectedDroppingPoint === null
									? I18n.t("add_location")
									: this.props.selectedDroppingPoint.bpName}
							</RedbusTextRegular>
							<View
								style={[
									styles.bar,
									!this.state.isBoarding
										? styles.barWithBoarding
										: styles.barWithoutBoarding
								]}
							/>
						</TouchableOpacity>
					)}
				</View>
				{this.props.selectedBus && (
					<ViewPager
						ref={this.viewPager}
						style={styles.container}
						initialPage={0}
						onPageSelected={event => {
							this.changeSelectedIndex(event.nativeEvent.position);
						}}>
						<ScrollView key={"1"}>
							{this.getBoardingPoints().map((item, index) => {
								return (
									<TouchableOpacity
										key={index}
										style={styles.location}
										onPress={() => {
											this.viewPager.current.setPage(1);
											GoAppAnalytics.trackWithProperties("select_seat", {
												bpId: item.bpId,
												address: item.address,
												time: item.time,
												isBoarding: true,
												busId: this.props.selectedBus.id
											});
											this.props.dispatch(redbusSelectBoardingPoint(item));
											if (
												this.props.redbusSelectBoardingPoint !== null &&
												this.checkDroppingPointAvailable() === false
											) {
												this.props.navigation.navigate("TravellerDetail");
												this.viewPager.current.setPage(0);
											}
										}}>
										<View style={styles.subLocation}>
											<View style={styles.circle}>
												{this.props.selectedBoardingPoint &&
													this.props.selectedBoardingPoint.bpId ===
														item.bpId && <View style={styles.innerCircle} />}
											</View>
											<View style={styles.locationView}>
												<RedbusTextRegular style={styles.miniLocationText}>
													{item.location}
												</RedbusTextRegular>
												<RedbusTextRegular style={styles.locationText}>
													{item.address}
												</RedbusTextRegular>
											</View>
										</View>
										<RedbusTextRegular style={styles.locationtime}>
											{this.getTime(item.time)}
										</RedbusTextRegular>
									</TouchableOpacity>
								);
							})}
						</ScrollView>
						{/* don't display the dropping point list if no dropping point is available*/}
						{this.checkDroppingPointAvailable() && (
							<ScrollView style={styles.screen} key={"2"}>
								{this.getDroppingPoint().map((item, index) => {
									return (
										<TouchableOpacity
											key={index}
											style={styles.location}
											onPress={() => {
												GoAppAnalytics.trackWithProperties("select_seat", {
													bpId: item.bpId,
													address: item.address,
													time: item.time,
													isBoarding: false,
													busId: this.props.selectedBus.id
												});
												this.props.dispatch(redbusSelectDroppingPoint(item));
												if (this.props.redbusSelectBoardingPoint !== null) {
													this.props.navigation.navigate("TravellerDetail");
												}
											}}>
											<View style={styles.subLocation}>
												<View style={styles.circle}>
													{this.props.selectedDroppingPoint &&
														this.props.selectedDroppingPoint.bpId ===
															item.bpId && <View style={styles.innerCircle} />}
												</View>
												<View style={styles.locationView}>
													<RedbusTextRegular style={styles.miniLocationText}>
														{item.location}
													</RedbusTextRegular>
													<RedbusTextRegular style={styles.locationText}>
														{item.address}
													</RedbusTextRegular>
												</View>
											</View>
											<RedbusTextRegular
												style={styles.locationtime}
												numberofLines={2}>
												{this.getTime(item.time)}
											</RedbusTextRegular>
										</TouchableOpacity>
									);
								})}
							</ScrollView>
						)}
					</ViewPager>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		source: state.redbus && state.redbus.searchBus.source,
		destination: state.redbus && state.redbus.searchBus.destination,
		travelDate: state.redbus && state.redbus.searchBus.travelDate,
		seatLayout: state.redbus && state.redbus.selectSeat.seatLayout,
		selectedBus: state.redbus && state.redbus.selectSeat.selectedBus,
		selectedBoardingPoint:
			state.redbus && state.redbus.selectSeat.selectedBoardingPoint,
		selectedDroppingPoint:
			state.redbus && state.redbus.selectSeat.selectedDroppingPoint
	};
}
export default connect(mapStateToProps)(SelectLocation);
