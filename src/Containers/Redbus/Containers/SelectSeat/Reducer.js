/* eslint-disable radix */
import {
	REDBUS_SAVE_BOARDING_POINT,
	REDBUS_SAVE_SEAT_LAYOUT,
	REDBUS_SAVE_SELECTED_BUS,
	REDBUS_SAVE_BOARDING_POINT_ERROR,
	REDBUS_SAVE_SEAT_ERROR,
	REDBUS_SAVE_SELECTED_SEAT,
	REDBUS_REMOVE_SELECTED_SEAT,
	REDBUS_SELECT_DROPPING_POINT,
	REDBUS_SELECT_BOARDING_POINT,
	REDBUS_SAVE_TRAVELLER
} from "./Saga";

const initialState = {
	selectedBus: null,
	seatLayout: null,
	boardingPoints: null,
	seatError: false,
	boardingPointError: false,
	selectedSeat: [],
	selectedBoardingPoint: null,
	selectedDroppingPoint: null,
	traveller: []
};
export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case REDBUS_SAVE_BOARDING_POINT:
			return {
				...state,
				boardingPoints: action.payload
			};
		case REDBUS_SELECT_BOARDING_POINT:
			return {
				...state,
				selectedBoardingPoint: action.payload
			};
		case REDBUS_SELECT_DROPPING_POINT:
			return {
				...state,
				selectedDroppingPoint: action.payload
			};
		case REDBUS_SAVE_SELECTED_BUS:
			return {
				...state,
				selectedBus: action.payload
			};
		case REDBUS_SAVE_BOARDING_POINT_ERROR:
			return {
				...state,
				boardingPointError: action.payload
			};
		case REDBUS_SAVE_SEAT_ERROR:
			return {
				...state,
				seatError: action.payload
			};
		case REDBUS_SAVE_SEAT_LAYOUT: {
			let seats = {};
			let lower = [];
			let upper = [];
			let maxRowLower = 0;
			let maxRowUpper = 0;
			if (action.payload !== null) {
				action.payload.seats.map(row => {
					if (row.zIndex === "0") {
						if (lower[row.column] === undefined) {
							lower[row.column] = [];
						}
						if (parseInt(row.row) > maxRowLower) {
							maxRowLower = parseInt(row.row);
						}
						lower[row.column].push(row);
					}
					if (row.zIndex === "1") {
						if (upper[row.column] === undefined) {
							upper[row.column] = [];
						}
						if (parseInt(row.row) > maxRowUpper) {
							maxRowUpper = parseInt(row.row);
						}
						upper[row.column].push(row);
					}
				});
				seats = {
					...seats,
					maxSeatsPerTicket: action.payload.maxSeatsPerTicket,
					lower,
					upper,
					maxRowUpper,
					maxRowLower
				};
				return {
					...state,
					seatLayout: seats
				};
			} else {
				return {
					...state,
					seatLayout: action.payload
				};
			}
		}
		case REDBUS_SAVE_SELECTED_SEAT: {
			if (action.payload !== null) {
				let isPrimary = state.traveller.length === 0;
				let newTraveller = {
					name: null,
					gender: null,
					age: null,
					primary: isPrimary
				};
				if (isPrimary === true) {
					newTraveller = {
						...newTraveller,
						email: null,
						mobile: null
					};
				}
				return {
					...state,
					selectedSeat: [...state.selectedSeat, action.payload],
					traveller: [
						...state.traveller,
						{
							seatName: action.payload.name,
							fare: action.payload.fare,
							baseFare: action.payload.baseFare,
							ladiesSeat: action.payload.ladiesSeat,
							passenger: newTraveller,
							operatorServiceChargeAbsolute:
								action.payload.operatorServiceChargeAbsolute,
							serviceTaxAbsolute: action.payload.serviceTaxAbsolute
						}
					]
				};
			} else {
				return {
					...state,
					selectedSeat: [],
					traveller: []
				};
			}
		}
		case REDBUS_REMOVE_SELECTED_SEAT: {
			let selectedIndex = state.selectedSeat.indexOf(action.payload);
			let size = state.selectedSeat.length;
			let temp = state.selectedSeat
				.slice(0, selectedIndex)
				.concat(state.selectedSeat.slice(selectedIndex + 1, size));
			let passenger = state.traveller.slice(0, state.traveller.length - 1);
			return {
				...state,
				selectedSeat: temp,
				traveller: passenger
			};
		}
		case REDBUS_SAVE_TRAVELLER: {
			if (action.payload !== null) {
				return {
					...state,
					traveller: action.payload
				};
			} else {
				let tempTraveller = [];
				state.selectedSeat.map((item, index) => {
					let isPrimary = index === 0;
					let newTraveller = {
						name: null,
						gender: null,
						age: null,
						primary: isPrimary
					};
					if (isPrimary === true) {
						newTraveller = {
							...newTraveller,
							email: null,
							mobile: null
						};
					}
					tempTraveller = [
						...tempTraveller,
						{
							seatName: item.name,
							fare: item.fare,
							baseFare: item.baseFare,
							ladiesSeat: item.ladiesSeat,
							passenger: newTraveller,
							operatorServiceChargeAbsolute: item.operatorServiceChargeAbsolute,
							serviceTaxAbsolute: item.serviceTaxAbsolute
						}
					];
				});
				return {
					...state,
					traveller: tempTraveller
				};
			}
		}
		default:
			return {
				...state
			};
	}
};
