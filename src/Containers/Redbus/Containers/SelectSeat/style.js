import { StyleSheet, Dimensions, Platform } from "react-native";
import { font_two, font_three, font_four } from "../../../../Assets/styles";
import { LOW_BLACK } from "../../Constants";

const { width, height } = Dimensions.get("window");
const HEADER_MAX_HEIGHT = height / 4.5;
export const indexStyles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
		justifyContent: "center",
		alignItems: "center"
	},
	policySection: {
		width: width,
		flexDirection: "row",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		zIndex: 5
	},
	imageCity: {
		width: width,
		height: width / 2
	},
	sleeper: {
		width: width / 10,
		height: height / 10,
		backgroundColor: "#fff",
		marginBottom: height / 70,
		marginLeft: width / 50
	},
	hsleeper: {
		height: height / 19,
		width: height / 10,
		backgroundColor: "#fff",
		marginBottom: height / 70,
		marginLeft: width / 50
	},
	seater: {
		width: width / 10,
		height: height / 19,
		backgroundColor: "#fff",
		marginBottom: height / 70,
		marginLeft: width / 50
	},
	leftSide: {
		flexDirection: "row"
		//borderWidth: 1,
	},
	rightSide: {
		marginLeft: 0,
		flexDirection: "row",
		alignSelf: "flex-end"
		//borderWidth: 1,
	},
	seatList: {
		borderWidth: 0.5,
		borderColor: LOW_BLACK,
		borderRadius: 3,
		paddingVertical: width / 40,
		paddingRight: width / 40,
		marginTop: height / 20
	},
	driver: {
		alignSelf: "flex-end",
		width: width / 10,
		height: height / 20
	},
	header: {
		width: width,
		height: height / 4.5,
		backgroundColor: "#C13D4A",
		paddingHorizontal: width / 26.3,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	goBack: {
		borderWidth: 0,
		borderColor: "#fff",
		width: width / 8,
		height: height / 13.33,
		justifyContent: "center",
		alignItems: "center"
	},
	infoSection: {
		flex: 1,
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center"
	},
	normalText: {
		fontSize: height / 48,
		color: "#fff"
	},
	bigText: {
		fontFamily: font_four,
		fontSize: height / 32,
		color: "#fff",
		textAlign: "center",
		width: width / 1.3
	},
	button: {
		flexDirection: "row",
		backgroundColor: "#e9e9e9",
		justifyContent: "space-between",
		width: width / 2,
		height: height / 20,
		marginTop: height / 40
	},
	activeButton: {
		width: width / 4.4,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#C13D4A"
	},
	unactiveButton: {
		width: width / 4.4,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#e9e9e9"
	},
	activeText: {
		fontSize: height / 60,
		color: "#fff"
	},
	unactiveText: {
		fontSize: height / 60,
		color: "#000"
	},
	chooseSeatsText: {
		paddingBottom: height / 60
	},
	fareRow: {
		flexDirection: "row"
	},
	fareSection: {
		width: width,
		height: height / 8,
		borderWidth: 1,
		flexDirection: "column",
		justifyContent: "center",
		alignItems: "center",
		borderColor: "#DFDDE0",
		marginTop: height / 40,
		...Platform.select({
			android: { elevation: 2 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "black",
				shadowOpacity: 0.1
			}
		})
	},
	activeFare: {
		marginLeft: width / 30,
		borderRadius: width / 18,
		paddingHorizontal: width / 30,
		paddingVertical: width / 35,
		backgroundColor: "#C13D4A",
		justifyContent: "center",
		alignItems: "center"
	},
	unactiveFare: {
		marginLeft: width / 30,
		borderRadius: width / 18,
		paddingHorizontal: width / 30,
		paddingVertical: width / 35,
		backgroundColor: "#fff",
		borderColor: "#DFDDE0",
		borderWidth: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	bottomComponent: {
		position: "absolute",
		bottom: 0,
		width: width,
		backgroundColor: "#fff",
		borderTopColor: "#DFDDE0",
		borderTopWidth: 1,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		flexDirection: "row",
		justifyContent: "space-between"
	},
	bottomComponentOne: {
		height: height / 1.34
	},
	bottomComponentTwo: {
		height: height / 14
	},
	seatSelection: {
		width: width / 1.5,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingHorizontal: width / 40
	},
	doneButton: {
		width: width / 3,
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		height: height / 14,
		backgroundColor: "#C13D4A"
	},
	seatText: {
		fontSize: height / 50,
		color: "#000"
	},
	totalFare: {
		fontSize: height / 50,
		color: "#C13D4A"
	},
	snackBar: {
		position: "absolute",
		bottom: height / 18,
		width: width,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		flexDirection: "row",
		alignItems: "center",
		paddingHorizontal: width / 30,
		backgroundColor: "#FAA900"
	},
	snackBarOne: {
		height: 0
	},
	snackBarTwo: {
		height: height / 18
	},
	snackBarText: {
		fontSize: height / 65,
		color: "#fff"
	},
	animatedHeader: {
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		alignItems: "center",
		justifyContent: "center",
		zIndex: 5,
		backgroundColor: "#C13D4A",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	spaceBetweenRowView: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	flexEndRowView: {
		flexDirection: "row",
		justifyContent: "flex-end"
	},
	backButtonView: {
		position: "absolute",
		top: width / 350,
		left: width / 25,
		zIndex: 6,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		width: width / 6,
		height: height / 12
	},
	seatScrollView: { width, marginBottom: height / 14 },
	selectedBirthView: {
		borderWidth: 1,
		marginVertical: height / 50,
		borderColor: LOW_BLACK,
		borderRadius: width / 80
	},
	leftRowView: {
		flexDirection: "row",
		justifyContent: "space-between",
		marginTop: height / 50
	},
	rightRowViewOne: {
		marginRight: width / 50,
		marginLeft: width / 15
	},
	rightRowViewTwo: {
		marginRight: width / 50,
		marginLeft: 0
	},
	rowView: { flexDirection: "row" },
	interactableStyle: {
		position: "absolute",
		bottom: 0,
		width: width
	},
	scrollViewContentStyle: {
		paddingTop: HEADER_MAX_HEIGHT,
		alignItems: "center"
	}
});

/**
 * styles for cancelllation policy page
 */
export const canellationPolicyStyles = StyleSheet.create({
	container: {
		left: 0,
		right: 0,
		backgroundColor: "#e9e9e9"
	},
	headerSection: {
		backgroundColor: "#fff",
		alignItems: "center",
		justifyContent: "center",
		height: height / 14,
		borderTopWidth: 1,
		borderTopColor: "#cccccc"
	},
	infoSection: {
		width: width / 1.1,
		alignSelf: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		marginTop: height / 100,
		paddingVertical: height / 80,
		backgroundColor: "#fff",
		paddingHorizontal: width / 30
	},
	cancelList: {
		flexDirection: "row",
		justifyContent: "space-between",
		borderWidth: 0,
		borderColor: "#000",
		paddingVertical: height / 100
	},
	cancelMessage: {
		fontSize: height / 55
	},
	cancelMessageMargin: {
		marginTop: height / 60
	}
});
