import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api/";
import { NO_NETWORK_ERROR } from "../../Constants";
import { redbusNoNetwork } from "../../Saga";

export const REDBUS_SAVE_SELECTED_BUS = "REDBUS_SAVE_SELECTED_BUS";
export const REDBUS_GET_SEAT_LAYOUT = "REDBUS_GET_SEAT_LAYOUT";
export const REDBUS_SAVE_SEAT_LAYOUT = "REDBUS_SAVE_SEAT_LAYOUT";

export const REDBUS_GET_BOARDING_POINT = "REDBUS_GET_BOARDING_POINT";
export const REDBUS_SAVE_BOARDING_POINT = "REDBUS_SAVE_BOARDING_POINT";

export const REDBUS_SAVE_SEAT_ERROR = "REDBUS_SAVE_SEAT_ERROR";
export const REDBUS_SAVE_BOARDING_POINT_ERROR =
	"REDBUS_SAVE_BOARDING_POINT_ERROR";

export const REDBUS_SAVE_SELECTED_SEAT = "REDBUS_SAVE_SELECTED_SEAT";
export const REDBUS_REMOVE_SELECTED_SEAT = "REDBUS_REMOVE_SELECTED_SEAT";

export const REDBUS_SELECT_BOARDING_POINT = "REDBUS_SELECT_BOARDING_POINT";
export const REDBUS_SELECT_DROPPING_POINT = "REDBUS_SELECT_DROPPING_POINT";

export const REDBUS_SAVE_TRAVELLER = "REDBUS_SAVE_TRAVELLER";

//save the selected bus for trip by the user to the store
export const redbusSaveSelectedBus = payload => ({
	type: REDBUS_SAVE_SELECTED_BUS,
	payload
});

//call Api to get seat layout of selected bus
export const redbusGetSeatLayout = payload => ({
	type: REDBUS_GET_SEAT_LAYOUT,
	payload
});

//save the seat layout returned by the api to the store
export const redbusSaveSeatLayout = payload => ({
	type: REDBUS_SAVE_SEAT_LAYOUT,
	payload
});

//get boarding points of the selected bus for trip
export const redbusGetBoardingPoints = payload => ({
	type: REDBUS_GET_BOARDING_POINT,
	payload
});

//save boarding point for the selected bus for trip
export const redbusSaveBoardingPoints = payload => ({
	type: REDBUS_SAVE_BOARDING_POINT,
	payload
});

//update the error status for seat layout
export const redbusSaveSeatError = payload => ({
	type: REDBUS_SAVE_SEAT_ERROR,
	payload
});

//update the eror status for boarding point
export const redbusSaveBoardingPointError = payload => ({
	type: REDBUS_SAVE_BOARDING_POINT_ERROR,
	payload
});

//save selected seat to the store
export const redbusSaveSelectedSeat = payload => ({
	type: REDBUS_SAVE_SELECTED_SEAT,
	payload
});

//remove seats from the store
export const redbusRemoveSelectedSeat = payload => ({
	type: REDBUS_REMOVE_SELECTED_SEAT,
	payload
});

//save selected boarding point to the store
export const redbusSelectBoardingPoint = payload => ({
	type: REDBUS_SELECT_BOARDING_POINT,
	payload
});

//save selected dropping point to the store
export const redbusSelectDroppingPoint = payload => ({
	type: REDBUS_SELECT_DROPPING_POINT,
	payload
});

//save added traveller to the store
export const redbusSaveTraveller = payload => ({
	type: REDBUS_SAVE_TRAVELLER,
	payload
});

//saga handles the seat layout and boarding and dropping point selected
//1.handles seat layout
//2.handled boarding point selection
export function* selectSeatSaga(dispatch) {
	yield takeLatest(REDBUS_GET_SEAT_LAYOUT, handleGetSeatLayout);
	yield takeLatest(REDBUS_GET_BOARDING_POINT, handleGetBoardingPoint);
}

//this handler function calls the api to get the seat layout
//save the seat layout to the store
function* handleGetSeatLayout(action) {
	try {
		let seatLayout = yield call(Api.getSeatLayout, action.payload);
		console.log("seat layout is==>", seatLayout);
		if (seatLayout.error && seatLayout.errorCode === NO_NETWORK_ERROR) {
			//if there is no network while fetching seat layout show
			//no network message
			yield put(
				redbusNoNetwork({
					actionType: action.type,
					payload: action.payload
				})
			);
			return;
		} else {
			if (seatLayout.success === 1) {
				//save succesfully fetched seat layout to the store
				yield put(redbusSaveSeatLayout(seatLayout.data));
			} else {
				//update the error status
				yield put(redbusSaveSeatError(true));
			}
		}
	} catch (error) {
		yield put(redbusSaveSeatError(true));
	}
}

//handled fetching of boarding point for a trip
function* handleGetBoardingPoint(action) {
	try {
		let boardingPoints = yield call(Api.getBoardingPoint, action.payload);
		console.log("boarding point is==>", boardingPoints);
		if (boardingPoints.success === 1) {
			//save succesfully fetched boarding point to the store
			yield put(redbusSaveBoardingPoints(boardingPoints.data));
		} else {
			//update the error status
			yield put(redbusSaveBoardingPointError(true));
		}
	} catch (error) {
		yield put(redbusSaveBoardingPointError(true));
	}
}
