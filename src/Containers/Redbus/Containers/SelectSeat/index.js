/* eslint-disable radix */
import React, { Fragment } from "react";
import {
	View,
	Image,
	Dimensions,
	ScrollView,
	TouchableOpacity,
	LayoutAnimation,
	NativeModules,
	Animated,
	Platform
} from "react-native";
import { connect } from "react-redux";
import Interactable from "react-native-interactable";
import CancellationPolicy from "./CancellationPolicy";
import { Loader } from "../../Components";
import {
	SEATER,
	SEATERF,
	SLEEPER,
	SLEEPERF,
	HSLEEPER,
	HSLEEPERF,
	DRIVER
} from "../../Assets/Img/Image";
import {
	RedbusTextRegular,
	RedbusTextBold,
	RedbusTextMedium
} from "../../Components/RedbusText";
import { Icon, GoToast } from "../../../../Components";
import {
	redbusGetSeatLayout,
	redbusSaveSeatLayout,
	redbusSaveSelectedSeat,
	redbusRemoveSelectedSeat,
	redbusSaveSeatError,
	redbusSaveSelectedBus
} from "./Saga";
import { DATE, MONTH } from "../../Constants";
import I18n from "../../Assets/Strings/i18n";
import { indexStyles as styles } from "./style";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";

const { height } = Dimensions.get("window");
const SLEEPERTYPE = "2";
const AVAILABLE = "#fff";
const UNAVAILABLE = "#DED8DB";
const SELECTED = "#BCE395";
const UPPER = "upper";
const LOWER = "lower";
const ALLFARE = "All";
const LADIES_SEAT_MESSAGE = I18n.t("ladies_seat_message");
const HEADER_MAX_HEIGHT = height / 4.5;
const HEADER_MIN_HEIGHT = height / 12;

const { UIManager } = NativeModules;

if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}
class SelectSeat extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedBirth: LOWER,
			activeFare: ALLFARE,
			snackBarHeight: false,
			policy: false,
			isOpen: false
		};
		this.isAgain = false;
		this.AnimatedHeaderValue = new Animated.Value(0);
		this.message = "";
		this.policyRef = React.createRef();
	}

	componentWillMount() {
		if (this.props.selectedBus !== null) {
			this.props.dispatch(
				redbusGetSeatLayout({
					appToken: this.props.appToken,
					tripId: this.props.selectedBus.id
				})
			);
		}
	}

	componentWillUnmount() {
		this.props.dispatch(redbusSaveSeatLayout(null));
		this.props.dispatch(redbusSaveSeatError(false));
		this.props.dispatch(redbusSaveSelectedSeat(null));
		this.props.dispatch(redbusSaveSelectedBus(null));
	}
	shouldComponentUpdate(props, state) {
		if (props.seatError === true && props.seatError !== this.props.seatError) {
			GoToast.show(I18n.t("unable_to_show_seat"), I18n.t("error"));
			this.props.navigation.goBack();
		}
		return true;
	}

	getTime = stringTime => {
		let time = "";
		let timeType = "";
		let integerTime = parseInt(stringTime);
		let hour = parseInt(integerTime / 60);
		let minute = parseInt(integerTime % 60);
		hour = hour >= 24 ? parseInt(hour / 24) : hour;
		if (hour < 12) {
			timeType = "AM";
		} else if (hour >= 12) {
			hour = hour - 12;
			timeType = "PM";
		}
		time += hour + ":";
		time += minute < 10 ? "0" + minute : minute;
		time += " " + timeType;
		return time;
	};

	getDate = stringDate => {
		let date = new Date(stringDate).toDateString().split(" ");
		return date[DATE] + " " + date[MONTH];
	};
	getImage(item) {
		if (item.length === SLEEPERTYPE) {
			if (item.ladiesSeat === "true") {
				return SLEEPERF;
			}
			return SLEEPER;
		} else {
			if (item.width === SLEEPERTYPE) {
				if (item.ladiesSeat === "true") {
					return HSLEEPERF;
				}
				return HSLEEPER;
			}
			if (item.ladiesSeat === "true") {
				return SEATERF;
			}
			return SEATER;
		}
	}

	isSelectedSeat = item => {
		return this.props.selectedSeat.indexOf(item) > -1;
	};

	getBackgroundColor = item => {
		if (this.isSelectedSeat(item)) {
			return SELECTED;
		} else {
			if (item.available === "true") {
				if (
					this.state.activeFare !== ALLFARE &&
					item.baseFare !== this.state.activeFare.baseFare
				) {
					return "#F8F8F9";
				}
				return AVAILABLE;
			} else {
				return UNAVAILABLE;
			}
		}
	};

	selectSeat = seat => {
		LayoutAnimation.easeInEaseOut();
		if (this.props.selectedSeat.indexOf(seat) !== -1) {
			this.props.dispatch(redbusRemoveSelectedSeat(seat));
		} else {
			if (
				this.props.selectedSeat.length <
				parseInt(this.props.selectedBus.maxSeatsPerTicket)
			) {
				if (seat.ladiesSeat === "true") {
					this.message = LADIES_SEAT_MESSAGE;
					this.animateSnackBar();
				}
				GoAppAnalytics.trackWithProperties("select_seat", {
					fare: seat.fare,
					name: seat.name,
					row: seat.row,
					column: seat.column,
					length: seat.length,
					width: seat.width,
					busId: this.props.selectedBus.id
				});
				this.props.dispatch(redbusSaveSelectedSeat(seat));
			} else {
				GoToast.show(
					I18n.t("max") +
						" " +
						this.props.selectedBus.maxSeatsPerTicket +
						" seats are allowed",
					I18n.t("error")
				);
			}
		}
	};

	isDisabled = seat => {
		return seat.available === "false";
	};

	getFares = fares => {
		let fare = [];
		if (fares instanceof Array) {
			fares = fares.sort((a, b) => parseInt(b.baseFare) - parseInt(a.baseFare));
			return fares;
		} else {
			fare.push(fares);
			fare = fare.sort((a, b) => parseInt(b.baseFare) - parseInt(a.baseFare));
			return fare;
		}
	};

	getFareCount = type => {
		if (this.state.activeFare === ALLFARE) {
			return "";
		} else {
			let count = 0;
			this.props.seatLayout[type].map(item => {
				if (item !== undefined) {
					count += item.filter(
						seats => seats.baseFare === this.state.activeFare.baseFare
					).length;
				}
			});
			return "(" + count + ")";
		}
	};

	getAllSelectedSeats = () => {
		let seats = "";
		this.props.selectedSeat.map((obj, index) => {
			seats += obj.name;
			if (index < this.props.selectedSeat.length - 1) {
				seats += ",";
			}
		});
		return seats;
	};

	getTotalCost = () => {
		let cost = 0;
		this.props.selectedSeat.map((obj, index) => {
			cost += parseInt(obj.baseFare);
		});
		return "₹ " + I18n.toNumber(cost, { precision: 2 });
	};

	animateSnackBar = () => {
		LayoutAnimation.easeInEaseOut();
		this.setState({ snackBarHeight: true });
		let self = this;
		this.timeOut = setTimeout(function() {
			LayoutAnimation.easeInEaseOut();
			self.setState({ snackBarHeight: false });
		}, 1000);
	};
	renderSeat = (seat, index) => {
		return (
			<TouchableOpacity
				disabled={this.isDisabled(seat)}
				key={index}
				onPress={() => this.selectSeat(seat)}>
				<Image
					style={[
						seat.length === "1"
							? seat.width === "1"
								? styles.seater
								: styles.hsleeper
							: styles.sleeper,
						{
							backgroundColor: this.getBackgroundColor(seat)
						}
					]}
					source={this.getImage(seat)}
					resizeMode={"stretch"}
				/>
			</TouchableOpacity>
		);
	};
	checkPreviousRow = (currIndex, prevIndex) => {
		let seatArray = this.props.seatLayout[this.state.selectedBirth];
		if (
			seatArray[currIndex] !== undefined &&
			seatArray[prevIndex] !== undefined
		) {
			let isPrevious =
				seatArray[prevIndex].filter(seat => seat.row > 2 && seat.length === 1)
					.length > 0;
			let isCurrent =
				seatArray[currIndex].filter(seat => seat.row > 2 && seat.length === 1)
					.length > 0;
			if (isPrevious === false && isCurrent === true) {
				return true;
			}
		}
		return false;
	};
	renderEmptySeat = () => {
		return <View style={styles.seater} />;
	};

	renderLeftRow = (seats, seatIndex) => {
		this.isAgain = this.checkPreviousRow(seatIndex, seatIndex - 1);
		return (
			<React.Fragment key={seatIndex}>
				<View style={styles.spaceBetweenRowView}>
					<View style={styles.leftSide}>
						{seats.map((seat, index) => {
							if (seat.row > 2) {
								return this.isAgain
									? this.renderEmptySeat()
									: this.renderSeat(seat, index);
							}
						})}
					</View>
				</View>
				{this.isAgain && (
					<View style={styles.spaceBetweenRowView}>
						<View style={styles.leftSide}>
							{seats.map((seat, index) => {
								if (seat.row > 2) {
									return this.renderSeat(seat, index);
								}
							})}
						</View>
					</View>
				)}
			</React.Fragment>
		);
	};

	renderRightRow = (seats, seatIndex) => {
		return (
			<View key={seatIndex} style={styles.flexEndRowView}>
				<View style={styles.leftSide}>
					{seats.map((seat, index) => {
						if (seat.row < 3) {
							return this.renderSeat(seat, index);
						}
					})}
				</View>
			</View>
		);
	};

	render() {
		console.log("state==>", this.state);
		const animatedHeaderHeight = this.AnimatedHeaderValue.interpolate({
			inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
			outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
			extrapolate: "clamp"
		});

		const animatedFontSize = this.AnimatedHeaderValue.interpolate({
			inputRange: [
				0,
				(HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT) / 2 - 1,
				(HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT) / 2
			],
			outputRange: [height / 48, height / 48, 0],
			extrapolate: "clamp"
		});

		const travelsAnimatedFont = this.AnimatedHeaderValue.interpolate({
			inputRange: [
				0,
				(HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT) / 2 - 1,
				(HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT) / 2
			],
			outputRange: [height / 40, height / 40, height / 50],
			extrapolate: "clamp"
		});
		const busTypeAnimatedFont = this.AnimatedHeaderValue.interpolate({
			inputRange: [
				0,
				(HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT) / 2 - 1,
				(HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT) / 2
			],
			outputRange: [height / 48, height / 48, height / 55],
			extrapolate: "clamp"
		});
		const animatedCancellationPolicyHeight = this.AnimatedHeaderValue.interpolate(
			{
				inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
				outputRange: [
					height - HEADER_MAX_HEIGHT - height / 35,
					height - HEADER_MIN_HEIGHT - height / 35
				],
				extrapolate: "clamp"
			}
		);
		return (
			<View style={styles.container}>
				{/* Back Button */}
				<View style={styles.backButtonView}>
					<TouchableOpacity
						style={styles.goBack}
						onPress={() => this.props.navigation.goBack()}>
						<Icon
							iconType={"material"}
							iconSize={height / 38}
							iconName={"arrow-back"}
							iconColor={"#ffffff"}
						/>
					</TouchableOpacity>
				</View>
				{/* Back Button End */}
				<Animated.View
					style={[
						{
							height: animatedHeaderHeight
						},
						styles.animatedHeader
					]}>
					{this.props.selectedBus && (
						<View style={styles.infoSection}>
							<Animated.Text
								style={[styles.normalText, { fontSize: animatedFontSize }]}>
								{this.getDate(this.props.selectedBus.doj) +
									"-" +
									this.getTime(this.props.selectedBus.departureTime)}
							</Animated.Text>
							<Animated.Text
								style={[styles.bigText, { fontSize: travelsAnimatedFont }]}>
								{this.props.selectedBus.travels}
							</Animated.Text>
							<Animated.Text
								style={[styles.normalText, { fontSize: busTypeAnimatedFont }]}>
								{this.props.selectedBus.busType}
							</Animated.Text>
						</View>
					)}
				</Animated.View>
				<ScrollView
					scrollEventThrottle={16}
					style={styles.seatScrollView}
					showsVerticalScrollIndicator={false}
					contentContainerStyle={styles.scrollViewContentStyle}
					onScroll={Animated.event([
						{ nativeEvent: { contentOffset: { y: this.AnimatedHeaderValue } } }
					])}>
					{this.props.seatLayout === null ? (
						<Loader />
					) : (
						<Fragment>
							{this.props.seatLayout.upper.length > 0 && (
								<View style={styles.button}>
									<TouchableOpacity
										onPress={() => this.setState({ selectedBirth: LOWER })}
										style={[
											this.state.selectedBirth === LOWER
												? styles.activeButton
												: styles.unactiveButton
										]}>
										<RedbusTextBold
											style={[
												this.state.selectedBirth === LOWER
													? styles.activeText
													: styles.unactiveText
											]}>
											{I18n.t("lower")}
											{this.getFareCount(LOWER)}
										</RedbusTextBold>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() => this.setState({ selectedBirth: UPPER })}
										style={[
											this.state.selectedBirth === UPPER
												? styles.activeButton
												: styles.unactiveButton
										]}>
										<RedbusTextBold
											style={[
												this.state.selectedBirth === UPPER
													? styles.activeText
													: styles.unactiveText
											]}>
											{I18n.t("upper")}
											{this.getFareCount(UPPER)}
										</RedbusTextBold>
									</TouchableOpacity>
								</View>
							)}

							{this.getFares(this.props.selectedBus.fareDetails).length > 1 && (
								<View style={styles.fareSection}>
									<View style={styles.chooseSeatsText}>
										<RedbusTextMedium>
											Choose seats based on fares
										</RedbusTextMedium>
									</View>
									<View style={styles.fareRow}>
										<TouchableOpacity
											onPress={() => this.setState({ activeFare: ALLFARE })}
											style={
												this.state.activeFare === ALLFARE
													? styles.activeFare
													: styles.unactiveFare
											}>
											<RedbusTextBold
												style={
													this.state.activeFare === ALLFARE
														? styles.activeText
														: styles.unactiveText
												}>
												{I18n.t("all")}
											</RedbusTextBold>
										</TouchableOpacity>
										{this.getFares(this.props.selectedBus.fareDetails).map(
											(item, index) => {
												return (
													<TouchableOpacity
														onPress={() => this.setState({ activeFare: item })}
														style={
															this.state.activeFare === item
																? styles.activeFare
																: styles.unactiveFare
														}
														key={index}>
														<RedbusTextBold
															style={
																this.state.activeFare === item
																	? styles.activeText
																	: styles.unactiveText
															}>
															{"₹" +
																I18n.toNumber(parseInt(item.baseFare), {
																	precision: 0
																})}
														</RedbusTextBold>
													</TouchableOpacity>
												);
											}
										)}
									</View>
								</View>
							)}
							<View style={styles.selectedBirthView}>
								<View>
									{this.state.selectedBirth === LOWER && (
										<Image
											style={styles.driver}
											source={DRIVER}
											resizeMode={"contain"}
										/>
									)}
								</View>
								<View style={styles.leftRowView}>
									<View>
										{this.props.seatLayout[this.state.selectedBirth].map(
											(item, index) => {
												if (item !== undefined) {
													return this.renderLeftRow(item, index);
												}
											}
										)}
									</View>
									<View
										style={
											this.props.seatLayout[this.state.selectedBirth][
												this.props.seatLayout[this.state.selectedBirth].length -
													1
											].length < 5
												? styles.rightRowViewOne
												: styles.rightRowViewTwo
										}>
										{this.props.seatLayout[this.state.selectedBirth].map(
											(item, index) => {
												if (item !== undefined) {
													return this.renderRightRow(item, index);
												}
											}
										)}
									</View>
								</View>
							</View>
						</Fragment>
					)}
				</ScrollView>

				{this.props.seatLayout && this.props.selectedSeat.length > 0 && (
					<View
						style={[
							styles.bottomComponent,
							this.state.policy
								? styles.bottomComponentOne
								: styles.bottomComponentTwo
						]}>
						<View style={styles.rowView}>
							<View style={styles.seatSelection}>
								<RedbusTextMedium style={styles.seatText}>
									{I18n.t("seat_count", {
										count: this.props.selectedSeat.length
									}) + ":"}
									{this.getAllSelectedSeats()}
								</RedbusTextMedium>
								<RedbusTextMedium style={styles.totalFare}>
									{this.getTotalCost()}
								</RedbusTextMedium>
							</View>
							<TouchableOpacity
								style={styles.doneButton}
								onPress={() =>
									this.props.navigation.navigate("SelectLocation")
								}>
								<RedbusTextRegular style={styles.normalText}>
									{I18n.t("rb_done")}
								</RedbusTextRegular>
							</TouchableOpacity>
						</View>
					</View>
				)}

				{this.props.seatLayout && this.props.selectedSeat.length === 0 && (
					<Interactable.View
						ref={this.policyRef}
						style={styles.interactableStyle}
						verticalOnly={true}
						initialPosition={{ y: height / 1.48 }}
						onSnap={event => {
							if (event.nativeEvent.index === 0) {
								this.setState({ isOpen: true });
							} else {
								this.setState({ isOpen: false });
							}
						}}
						boundaries={{ top: height / 125, bottom: height / 1.48 }}
						snapPoints={[{ y: height / 125 }, { y: height / 1.48 }]}>
						<CancellationPolicy
							onPress={() => this.policyRef.current.snapTo({ index: 0 })}
							animatedHeight={animatedCancellationPolicyHeight}
							isOpen={this.state.isOpen}
						/>
					</Interactable.View>
				)}
				<View
					style={[
						styles.snackBar,
						this.state.snackBarHeight === false
							? styles.snackBarOne
							: styles.snackBarTwo
					]}>
					<RedbusTextRegular style={styles.snackBarText}>
						{this.message}
					</RedbusTextRegular>
				</View>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		source: state.redbus && state.redbus.searchBus.source,
		destination: state.redbus && state.redbus.searchBus.destination,
		travelDate: state.redbus && state.redbus.searchBus.travelDate,
		seatLayout: state.redbus && state.redbus.selectSeat.seatLayout,
		selectedBus: state.redbus && state.redbus.selectSeat.selectedBus,
		selectedSeat: state.redbus && state.redbus.selectSeat.selectedSeat,
		seatError: state.redbus && state.redbus.selectSeat.seatError
	};
}
export default connect(mapStateToProps)(SelectSeat);
