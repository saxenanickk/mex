import React from "react";
import {
	View,
	StyleSheet,
	Dimensions,
	TouchableOpacity,
	Animated
} from "react-native";
import { connect } from "react-redux";
import { Seperator } from "../../../../Components";
import { RedbusTextRegular } from "../../Components/RedbusText";
import { canellationPolicyStyles as styles } from "./style";
import I18n from "../../Assets/Strings/i18n";
const { width, height } = Dimensions.get("window");

/**
 * Component which renders the cancellation policy for a trip
 */
class CancellationPolicy extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	/**
	 * [get the cancellation policy from the redbus decoded fomat]
	 */
	getCancellationPolicy = () => {
		let policy = this.props.selectedBus.cancellationPolicy.split(";");
		return policy.reverse();
	};

	/**
	 * use grammar to translate message according to hindi or englisht
	 * @param  {[number]} hour1 [starting cancellationm hour]
	 * @param  {[number]} hour2 [ending cancellation hour]
	 * @return {[string]}       [message displayed for a cancellation rule]
	 */
	getPrimaryMessage = (hour1, hour2) => {
		if (hour2 === "-1") {
			return I18n.currentLocale() === "hi-IN"
				? hour1 + " " + I18n.t("cancel_anytime_before")
				: I18n.t("cancel_anytime_before") + " " + hour1 + " " + I18n.t("hours");
		} else {
			if (hour1 === "0") {
				return (
					I18n.t("last") +
					" " +
					hour2 +
					" " +
					I18n.t("full_cancellation_charge")
				);
			}
			return hour2 + " " + I18n.t("hours_before_travel");
		}
	};

	/**
	 * get the secondary message
	 * @param  {[number]} percentage [amount deducted percenatge]
	 * @return {[number]}            [amount that should be deducted from fare in number]
	 */
	getSecondaryMessage = percentage => {
		if (this.props.selectedBus.fares instanceof Array) {
			return Math.round(this.props.selectedBus.fares[0] * (percentage / 100));
		} else {
			return Math.round(this.props.selectedBus.fares * (percentage / 100));
		}
	};

	/**
	 * get total fare for a trip
	 */
	getFare = () => {
		if (this.props.selectedBus.fares instanceof Array) {
			return this.props.selectedBus.fares[0];
		} else {
			return this.props.selectedBus.fares;
		}
	};
	render() {
		const AnimatedTouchable = Animated.createAnimatedComponent(
			TouchableOpacity
		);
		return (
			<AnimatedTouchable
				style={[
					styles.container,
					{
						height: this.props.isOpen
							? this.props.animatedHeight
							: height / 1.34
					}
				]}
				activeOpacity={1}
				onPress={this.props.onPress}>
				<View style={styles.headerSection}>
					<RedbusTextRegular>POLICIES</RedbusTextRegular>
				</View>
				<View style={styles.infoSection}>
					<View style={styles.cancelList}>
						<RedbusTextRegular>
							{I18n.t("time_before_travel")}
						</RedbusTextRegular>
						<RedbusTextRegular>{I18n.t("deductions")}</RedbusTextRegular>
					</View>
					<Seperator height={1} width={width / 1.18} color={"#000"} />
					{this.getCancellationPolicy().map((item, index) => {
						let allItems = item.split(":");
						return (
							<View key={index} style={styles.cancelList}>
								<RedbusTextRegular style={styles.cancelMessage}>
									{this.getPrimaryMessage(allItems[0], allItems[1])}
								</RedbusTextRegular>
								<RedbusTextRegular style={styles.cancelMessage}>
									{"₹" + this.getSecondaryMessage(allItems[2])}
								</RedbusTextRegular>
							</View>
						);
					})}
					<RedbusTextRegular
						style={[styles.cancelMessage, styles.cancelMessageMargin]}>
						{I18n.currentLocale() === "hi-IN"
							? I18n.t("cancellation_fare_calculated_hindi_one") +
							  " ₹" +
							  this.getFare() +
							  " " +
							  I18n.t("cancellation_fare_calculated_hindi_two")
							: I18n.t("cancellation_fare_calculated") + this.getFare()}
					</RedbusTextRegular>
					<RedbusTextRegular
						style={[styles.cancelMessage, styles.cancelMessageMargin]}>
						{I18n.t("cancellation_rule_one")}
					</RedbusTextRegular>
					{this.props.selectedBus.partialCancellationAllowed === "true" && (
						<RedbusTextRegular
							style={[styles.cancelMessage, styles.cancelMessageMargin]}>
							{I18n.t("cancellation_rule_two")}
						</RedbusTextRegular>
					)}
					<RedbusTextRegular
						style={[styles.cancelMessage, styles.cancelMessageMargin]}>
						{I18n.t("cancellation_rule_three")}
					</RedbusTextRegular>
				</View>
			</AnimatedTouchable>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		selectedBus: state.redbus && state.redbus.selectSeat.selectedBus
	};
}
export default connect(mapStateToProps)(CancellationPolicy);
