import {
	REDBUS_SAVE_TICKET_DETAIL,
	REDBUS_SAVE_TICKET_DETAIL_ERROR
} from "./Saga";

const initialState = {
	ticketDetail: null,
	ticketDetailError: false
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case REDBUS_SAVE_TICKET_DETAIL:
			return {
				...state,
				ticketDetail: action.payload
			};
		case REDBUS_SAVE_TICKET_DETAIL_ERROR:
			return {
				...state,
				ticketDetailError: action.payload
			};
		default:
			return {
				...state
			};
	}
};
