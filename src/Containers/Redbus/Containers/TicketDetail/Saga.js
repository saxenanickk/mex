import { takeLatest, takeEvery, put, call } from "redux-saga/effects";
import Api from "../../Api/";
import { NO_NETWORK_ERROR } from "../../Constants";
import { redbusNoNetwork } from "../../Saga";
export const REDBUS_GET_TICKET_DETAIL = "REDBUS_GET_TICKET_DETAIL";
export const REDBUS_SAVE_TICKET_DETAIL = "REDBUS_SAVE_TICKET_DETAIL";
export const REDBUS_SAVE_TICKET_DETAIL_ERROR =
	"REDBUS_SAVE_TICKET_DETAIL_ERROR";
//call Api to get the ticket detail of a tin
export const redbusGetTicketDetail = payload => ({
	type: REDBUS_GET_TICKET_DETAIL,
	payload
});

//stores ticket detail into redux store
export const redbusSaveTicketDetail = payload => ({
	type: REDBUS_SAVE_TICKET_DETAIL,
	payload
});

//if there is error while fetching ticket detail then update the error status
export const redbusSaveTicketDetailError = payload => ({
	type: REDBUS_SAVE_TICKET_DETAIL_ERROR,
	payload
});

//this saga used to fetch ticket detail
//1.fetch ticket detail
export function* ticketDetailSaga(dispatch) {
	yield takeLatest(REDBUS_GET_TICKET_DETAIL, handleGetTicketDetail);
}

//handler function to fetch ticket detail
function* handleGetTicketDetail(action) {
	try {
		let ticket = yield call(Api.getTicketDetail, action.payload);
		if (ticket.error && ticket.errorCode === NO_NETWORK_ERROR) {
			// if there is network while fetching ticket detail show no network message
			yield put(
				redbusNoNetwork({
					actionType: action.type,
					payload: action.payload
				})
			);
			return;
		} else {
			if (ticket.success === 1) {
				//successfully fetched ticket then store it to redux
				yield put(redbusSaveTicketDetail(ticket.data));
			} else {
				//unable to fetch ticket the update the error status
				yield put(redbusSaveTicketDetailError(true));
			}
		}
	} catch (error) {
		yield put(redbusSaveTicketDetailError(true));
	}
}
