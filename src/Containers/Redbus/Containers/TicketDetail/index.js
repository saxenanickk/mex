/* eslint-disable radix */
import React from "react";
import {
	View,
	Image,
	Dimensions,
	ScrollView,
	TouchableOpacity,
	NativeModules,
	BackHandler,
	Platform
} from "react-native";
import { connect } from "react-redux";
import { Header } from "../../Components";

import { SERVERERROR } from "../../Assets/Img/Image";
import { Icon, Seperator, ProgressScreen } from "../../../../Components";
import {
	redbusSaveTicketDetail,
	redbusGetTicketDetail,
	redbusSaveTicketDetailError
} from "./Saga";
import { RedbusTextRegular, RedbusTextBold } from "../../Components/RedbusText";
import { DATE, MONTH, LOW_BLACK, YEAR, DAY } from "../../Constants";
import I18n from "../../Assets/Strings/i18n";
import { indexStyles as styles } from "./style";
import GoAppAnalytics from "../../../../utils/GoAppAnalytics";
import { CommonActions } from "@react-navigation/native";

const { width, height } = Dimensions.get("window");
const { UIManager } = NativeModules;

if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}
class TicketDetail extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	shouldComponentUpdate(props, state) {
		return true;
	}

	componentDidMount() {
		GoAppAnalytics.trackWithProperties("see_ticket", {
			tin: this.props.tinNumber
		});
		this.props.dispatch(
			redbusGetTicketDetail({
				appToken: this.props.appToken,
				tin: this.props.tinNumber
			})
		);
		this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
			this.closeTicketDetail();
			return true;
		});
	}
	componentWillUnmount() {
		this.props.dispatch(redbusSaveTicketDetail(null));
		this.props.dispatch(redbusSaveTicketDetailError(false));
		this.backHandler.remove();
	}

	/**
	 * get the time in human readable format
	 * @param  {[String]} stringTime [time inredbus format]
	 * @return {[typString]}            [$HH:$MM]
	 */
	getTime = stringTime => {
		let time = "";
		let integerTime = parseInt(stringTime);
		let hour = parseInt(integerTime / 60);
		let minute = parseInt(integerTime % 60);
		hour = hour >= 24 ? parseInt(hour / 24) : hour;
		time += hour + ":";
		time += minute < 10 ? "0" + minute : minute;
		return time;
	};

	/**
	 * get month,day,year from the date according to the index
	 * @param  {[Integer]} index [index of which date information is to be fetched]
	 * @return {[String]}       [$DD or $MM or $YYYY]
	 */
	getDate = stringDate => {
		let date = new Date(stringDate).toDateString().split(" ");
		return (
			date[DAY] + ", " + date[DATE] + " " + date[MONTH] + " ," + date[YEAR]
		);
	};

	/**
	 * close the ticket detail screen and go back to search bus page
	 */
	closeTicketDetail = () => {
		this.props.navigation.dispatch(
			CommonActions.reset({
				index: 0,
				routes: [{ name: "SearchBus" }]
			})
		);
	};

	/**
	 * get the contact number of the travellers
	 */
	getContactNumber = () => {
		let contactNumber = [];
		if (this.props.ticketDetail.pickUpContactNo instanceof Array) {
			return this.props.ticketDetail.pickUpContactNo;
		} else {
			contactNumber.push(this.props.ticketDetail.pickUpContactNo);
			return contactNumber;
		}
	};

	/**
	 * get list of passengers in a trip
	 * @return {[Array]} [list of passengers]
	 */
	getInventory = () => {
		let inventory = [];
		if (this.props.ticketDetail.inventoryItems instanceof Array) {
			return this.props.ticketDetail.inventoryItems;
		} else {
			inventory.push(this.props.ticketDetail.inventoryItems);
			return inventory;
		}
	};

	/**
	 * get total fare for a trip
	 * @return {[Array]} [list of fare]
	 */
	getFare = () => {
		let fare = 0;
		if (this.props.ticketDetail.inventoryItems instanceof Array) {
			this.props.ticketDetail.inventoryItems.map(item => {
				fare += parseInt(item.fare);
			});
			return fare;
		} else {
			return this.props.ticketDetail.inventoryItems.fare;
		}
	};
	render() {
		return (
			<View style={styles.container}>
				<Header
					goBack={true}
					callOnBack={() => this.closeTicketDetail()}
					iconHeight={height / 38}>
					<View style={styles.subHeader}>
						<RedbusTextRegular style={styles.headerText}>
							{I18n.t("ticket_details")}
						</RedbusTextRegular>
					</View>
				</Header>
				{!this.props.ticketDetailError ? (
					<View style={styles.fullFlex}>
						{this.props.ticketDetail === null ? (
							<ProgressScreen
								isMessage={true}
								message={I18n.t("fetching_ticket_detail")}
								indicatorColor={"#C13D4A"}
								indicatorSize={width / 15}
								primaryMessage={I18n.t("hang_on")}
							/>
						) : (
							<ScrollView style={styles.fullFlex}>
								<View style={styles.confirmSection}>
									<View style={styles.confirmView}>
										<RedbusTextBold style={styles.boldText}>
											{I18n.t("confirmed")}
										</RedbusTextBold>
										<RedbusTextBold
											style={[styles.semiBoldText, styles.semiBoldMargin]}>
											{this.getDate(this.props.ticketDetail.doj)}
										</RedbusTextBold>
									</View>
									<Seperator width={width / 1.2} color={"rgba(0,0,0,0.3)"} />
									<View style={styles.locationSection}>
										<View>
											<RedbusTextBold style={styles.semiBoldText}>
												{I18n.t("from").toUpperCase()}
											</RedbusTextBold>
											<RedbusTextBold style={styles.boldText}>
												{this.props.ticketDetail.sourceCity}
											</RedbusTextBold>
										</View>
										<Icon
											iconType={"ionicon"}
											iconSize={height / 26}
											iconName={"ios-arrow-forward"}
											iconColor={LOW_BLACK}
										/>
										<View>
											<RedbusTextBold style={styles.semiBoldText}>
												{I18n.t("to").toUpperCase()}
											</RedbusTextBold>
											<RedbusTextBold style={styles.boldText}>
												{this.props.ticketDetail.destinationCity}
											</RedbusTextBold>
										</View>
									</View>
								</View>
								<View style={styles.passengerDetailSection}>
									<View style={styles.locationSection}>
										<View>
											<RedbusTextRegular style={styles.semiBoldText}>
												{I18n.t("boarding_point")}
											</RedbusTextRegular>
											<RedbusTextRegular style={styles.normalText}>
												{this.props.ticketDetail.pickupLocation.trim()}
											</RedbusTextRegular>
										</View>
										<View>
											<RedbusTextBold style={styles.semiBoldText}>
												{I18n.t("boards")}
											</RedbusTextBold>
											<RedbusTextRegular style={styles.normalText}>
												{this.getTime(this.props.ticketDetail.pickupTime)}
											</RedbusTextRegular>
										</View>
									</View>
									<Seperator width={width / 1.2} color={"rgba(0,0,0,0.3)"} />
									<View style={styles.travelSection}>
										<View>
											<RedbusTextBold style={styles.semiBoldText}>
												{I18n.t("travel")}
											</RedbusTextBold>
											<RedbusTextRegular style={styles.normalText}>
												{this.props.ticketDetail.travels}
											</RedbusTextRegular>
											<RedbusTextRegular>
												{this.props.ticketDetail.busType}
											</RedbusTextRegular>
										</View>
										<View style={styles.passengerSection}>
											<RedbusTextBold style={styles.semiBoldText}>
												{I18n.t("passengers")}
											</RedbusTextBold>
											<RedbusTextBold
												style={[styles.semiBoldText, styles.semiBoldAlign]}>
												{I18n.t("seat")}
											</RedbusTextBold>
										</View>
										{this.getInventory().map((item, index) => {
											return (
												<View style={styles.listSection} key={index}>
													<RedbusTextRegular style={styles.listText}>
														{item.passenger.name}
													</RedbusTextRegular>
													<RedbusTextRegular
														style={[styles.listText, styles.semiBoldAlign]}>
														{item.seatName}
													</RedbusTextRegular>
												</View>
											);
										})}
									</View>
									<Seperator width={width / 1.2} color={"rgba(0,0,0,0.3)"} />
									<View style={styles.travelSection}>
										<RedbusTextBold style={styles.semiBoldText}>
											{I18n.t("boarding_point_detail")}
										</RedbusTextBold>
										<RedbusTextRegular style={styles.normalText}>
											{this.props.ticketDetail.pickupLocationLandmark}
											{","}
											{this.props.ticketDetail.pickUpLocationAddress}
										</RedbusTextRegular>
										<RedbusTextRegular style={styles.normalText}>
											{this.props.ticketDetail.pickupLocation}
										</RedbusTextRegular>
										{this.getContactNumber().map((phone, index) => {
											return (
												<RedbusTextRegular key={index} style={styles.contact}>
													{phone}
												</RedbusTextRegular>
											);
										})}
									</View>
								</View>
								<View style={styles.ticketSection}>
									<View style={styles.locationSection}>
										<View>
											<RedbusTextBold style={styles.semiBoldText}>
												{I18n.t("ticket_number")}
											</RedbusTextBold>
											<RedbusTextRegular>
												{this.props.ticketDetail.tin}
											</RedbusTextRegular>
										</View>
										<View>
											<RedbusTextBold style={styles.semiBoldText}>
												{I18n.t("total")}
											</RedbusTextBold>
											<RedbusTextRegular>{this.getFare()}</RedbusTextRegular>
										</View>
									</View>
									<Seperator width={width / 1.2} color={"rgba(0,0,0,0.3)"} />
									<View style={styles.locationSection}>
										<View>
											<RedbusTextBold style={styles.semiBoldText}>
												{I18n.t("pnr_number")}
											</RedbusTextBold>
											<RedbusTextRegular>
												{this.props.ticketDetail.pnr}
											</RedbusTextRegular>
										</View>
									</View>
								</View>
							</ScrollView>
						)}
					</View>
				) : (
					<View style={styles.errorContainer}>
						<Image
							style={styles.errorImage}
							source={SERVERERROR}
							resizeMode={"contain"}
						/>
						<RedbusTextRegular style={styles.primaryText}>
							{I18n.t("error_in_fetching_ticket_detail")}
						</RedbusTextRegular>
						<RedbusTextRegular style={styles.primaryText}>
							{I18n.t("error_secondary_message")}
						</RedbusTextRegular>
						<TouchableOpacity
							style={styles.goHomeButton}
							onPress={() => {
								this.props.dispatch(redbusSaveTicketDetailError(false));
								this.props.dispatch(
									redbusGetTicketDetail({
										appToken: this.props.appToken,
										tin: this.props.tinNumber
									})
								);
							}}>
							<RedbusTextRegular style={styles.headerText}>
								{I18n.t("reload")}
							</RedbusTextRegular>
						</TouchableOpacity>
					</View>
				)}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken:
			state.appToken && state.appToken.token ? state.appToken.token : null,
		tinNumber: state.redbus && state.redbus.travellerDetail.tinNumber,
		ticketDetail: state.redbus && state.redbus.ticketDetail.ticketDetail,
		ticketDetailError:
			state.redbus && state.redbus.ticketDetail.ticketDetailError
	};
}
export default connect(mapStateToProps)(TicketDetail);
