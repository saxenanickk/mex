import { StyleSheet, Dimensions } from "react-native";
import { LOW_BLACK } from "../../Constants";
const { width, height } = Dimensions.get("window");

export const indexStyles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#e9e9e9"
	},
	headerText: {
		color: "#fff",
		fontSize: height / 40
	},
	subHeader: {
		width: width / 1.2,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	primaryText: {
		fontSize: height / 42,
		color: LOW_BLACK,
		marginTop: height / 180
	},
	errorImage: {
		width: width,
		height: height / 2
	},
	travelSection: {
		width: width / 1.1,
		backgroundColor: "#fff",
		marginTop: height / 50
	},
	boldText: {
		color: "#000",
		fontSize: height / 50
	},
	semiBoldText: {
		fontSize: height / 55
	},
	confirmSection: {
		marginTop: height / 40,
		paddingVertical: height / 70,
		backgroundColor: "#cacac7",
		width: width / 1.1,
		alignSelf: "center",
		paddingHorizontal: width / 30
	},
	locationSection: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingVertical: height / 80
	},
	passengerDetailSection: {
		paddingVertical: height / 80,
		backgroundColor: "#fff",
		width: width / 1.1,
		alignSelf: "center",
		paddingHorizontal: width / 30
	},
	normalText: {
		fontSize: height / 55,
		marginTop: height / 100
	},
	passengerSection: {
		flexDirection: "row",
		width: width / 1.2,
		paddingVertical: height / 80,
		justifyContent: "space-between"
	},
	ticketSection: {
		width: width / 1.1,
		alignSelf: "center",
		backgroundColor: "#fff",
		paddingHorizontal: width / 30,
		marginTop: height / 70
	},
	listSection: {
		flexDirection: "row",
		width: width / 1.2,
		justifyContent: "space-between",
		marginBottom: height / 80
	},
	listText: {
		marginTop: height / 80,
		fontSize: height / 55
	},
	contact: {
		fontSize: height / 50,
		marginTop: height / 100
	},
	semiBoldMargin: { marginLeft: width / 30 },
	semiBoldAlign: { alignSelf: "flex-end" },
	errorContainer: { flex: 1, justifyContent: "center", alignItems: "center" },
	fullFlex: { flex: 1 },
	confirmView: {
		flexDirection: "row",
		paddingVertical: height / 80
	}
});
