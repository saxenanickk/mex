import React from "react";
import { Stack } from "../../utils/Navigators";
import Splash from "./Containers/Splash";
import SearchBus from "./Containers/SearchBus";
import SelectBus from "./Containers/SelectBus";
import SelectSeat from "./Containers/SelectSeat";
import SelectLocation from "./Containers/SelectLocation";
import TravellerDetail from "./Containers/TravellerDetail";
import BookTicket from "./Containers/BookTicket";
import TicketDetail from "./Containers/TicketDetail";
import Search from "./Containers/SearchBus/Search";

const Navigator = () => (
	<Stack.Navigator headerMode={"none"}>
		<Stack.Screen name={"Splash"} component={Splash} />
		<Stack.Screen name={"SearchBus"} component={SearchBus} />
		<Stack.Screen name={"SelectBus"} component={SelectBus} />
		<Stack.Screen name={"SelectSeat"} component={SelectSeat} />
		<Stack.Screen name={"SelectLocation"} component={SelectLocation} />
		<Stack.Screen name={"TravellerDetail"} component={TravellerDetail} />
		<Stack.Screen name={"BookTicker"} component={BookTicket} />
		<Stack.Screen name={"TicketDetail"} component={TicketDetail} />
		<Stack.Screen name={"SearchCity"} component={Search} />
	</Stack.Navigator>
);

export default Navigator;
