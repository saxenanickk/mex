import App from "./App";
import React from "react";
import { AppRegistry } from "react-native";
import reducer from "./Reducer";
import { getNewReducer, removeExistingReducer } from "../../../App";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";

export default class Redbus extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "redbus" });

		this.state = {
			doneLoading: false
		};
		getNewReducer({ name: "redbus", reducer: reducer });
		console.disableYellowBox = true;
	}

	componentWillUnmount() {
		removeExistingReducer("redbus");
	}

	render() {
		return <App />;
	}
}

AppRegistry.registerComponent("Redbus", () => Redbus);
