import React, { Fragment } from "react";
import { SafeAreaView } from "react-native";
import RegisterScreen from "./RegisterScreen";
import { connect } from "react-redux";
import { RedbusNoNetwork } from "./Components";
import GoAppAnalytics from "../../utils/GoAppAnalytics";
import { redbusNoNetwork, redbusHandleRetry } from "./Saga";

class App extends React.Component {
	componentWillUnmount() {
		this.props.dispatch(redbusNoNetwork(null));
	}

	handleRetryPress = () => {
		this.props.dispatch(
			redbusHandleRetry({
				actionType: this.props.noNetworkError.actionType,
				payload: this.props.noNetworkError.payload
			})
		);
	};

	render() {
		return (
			<Fragment>
				{this.props.noNetworkError && (
					<RedbusNoNetwork
						close={() => this.props.dispatch(redbusNoNetwork(null))}
						onRetryPress={this.handleRetryPress}
					/>
				)}
				<SafeAreaView style={{ flex: 0, backgroundColor: "#9e3e47" }} />
				<SafeAreaView style={{ flex: 1 }}>
					<RegisterScreen
						navigation={this.props.navigation}
						onNavigationStateChange={(prevState, currState) =>
							GoAppAnalytics.logChangeOfScreenInStack(
								"redbus",
								prevState,
								currState
							)
						}
					/>
				</SafeAreaView>
			</Fragment>
		);
	}
}

function mapStateToProps(state) {
	return {
		noNetworkError:
			state.redbus && state.redbus.redbusNoNetwork
				? state.redbus.redbusNoNetwork.noNetworkError
				: null
	};
}
export default connect(mapStateToProps)(App);
