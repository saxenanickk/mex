import { all, takeLatest, put } from "redux-saga/effects";
import { searchBusSaga } from "./Containers/SearchBus/Saga";
import { selectBusSaga } from "./Containers/SelectBus/Saga";
import { selectSeatSaga } from "./Containers/SelectSeat/Saga";
import { travellerDetailSaga } from "./Containers/TravellerDetail/Saga";
import { ticketDetailSaga } from "./Containers/TicketDetail/Saga";

/**
 * action to store no network cases in redbus
 */
export const REDBUS_NO_NETWORK = "REDBUS_NO_NETWORK";
export const REDBUS_HANDLE_RETRY = "REDBUS_HANDLE_RETRY";

export const redbusNoNetwork = payload => ({
	type: REDBUS_NO_NETWORK,
	payload
});

export const redbusHandleRetry = payload => ({
	type: REDBUS_HANDLE_RETRY,
	payload
});

function* redbusRetrySaga(dispatch) {
	yield takeLatest(REDBUS_HANDLE_RETRY, handleRetry);
}

function* handleRetry(action) {
	console.log(action);
	if (action.payload) {
		/**
		 * to add parameter for retry search while network request fails
		 */

		yield put(redbusNoNetwork(null));
		yield put({
			type: action.payload.actionType,
			payload: action.payload.payload
		});
	} else {
		/**
		 * helper method to close the no network screen
		 */
		yield put(redbusNoNetwork(null));
	}
}

export default function* redbusSaga(dispatch) {
	yield all([
		redbusRetrySaga(dispatch),
		searchBusSaga(dispatch),
		selectBusSaga(dispatch),
		selectSeatSaga(dispatch),
		travellerDetailSaga(dispatch),
		ticketDetailSaga(dispatch)
	]);
}
