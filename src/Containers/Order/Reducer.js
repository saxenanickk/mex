import { combineReducers } from "redux";
// Import Different Reducers Here
import { reducer as homeReducer } from "./Containers/Home/Reducer";
import global_reducer from "../../Reducer";

const initialState = {
	errorScreen: null,
	errorMessage: null,
	isError: false
};
export const reducer = (state = initialState, action) => {
	switch (action.type) {
		default:
			return {
				...state
			};
	}
};
const orderReducer = combineReducers({
	error: reducer,
	home: homeReducer
});

export default orderReducer;
