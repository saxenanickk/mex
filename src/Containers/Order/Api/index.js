import Config from "react-native-config";

const {
	SERVER_BASE_URL_CUSTOMER,
	SERVER_BASE_URL_REDBUS,
	SERVER_BASE_URL_CLIENT_DASHBOARD
} = Config;
class OrderApi {
	/**
	 * Orders API
	 */
	getOrders(params = {}) {
		let SEARCH_QUERY = "";
		let X_ACCESS_TOKEN = params.appToken;

		delete params.appToken;
		/**
		 * Dynamic Search Query Generation
		 */
		Object.keys(params).map(item => {
			if (SEARCH_QUERY.indexOf("?") < 0) {
				SEARCH_QUERY = `${SEARCH_QUERY}?${item}=${params[item]}`;
			} else {
				SEARCH_QUERY = `${SEARCH_QUERY}&${item}=${params[item]}`;
			}
			return true;
		});

		return new Promise(function(resolve, reject) {
			fetch(`${SERVER_BASE_URL_CUSTOMER}/history${SEARCH_QUERY}`, {
				method: "GET",
				headers: {
					"X-ACCESS-TOKEN": X_ACCESS_TOKEN
				}
			})
				.then(res => {
					res.status === 200
						? res.json().then(res => {
								resolve(res);
						  })
						: res.json().then(res => {
								reject(res);
						  });
				})
				.catch(error => {
					reject(error);
				});
		});
	}

	/**
	 * Cancellation details - Requesting cancellation data
	 * Provides details whether order can be cancelled (partially/completely)
	 */

	getCancellationData(params = {}) {
		let X_ACCESS_TOKEN = params.appToken;

		delete params.appToken;

		return new Promise(function(resolve, reject) {
			fetch(`${SERVER_BASE_URL_REDBUS}/cancellationData?tin=${params.tin}`, {
				method: "GET",
				headers: {
					"X-ACCESS-TOKEN": X_ACCESS_TOKEN
				}
			})
				.then(result => {
					result.json().then(res => {
						if (result.status !== 200) {
							reject(res);
						} else {
							resolve(res);
						}
					});
				})
				.catch(error => {
					reject(error);
				});
		});
	}

	/**
	 * Request Cancellation
	 */
	requestCancellation(params = {}) {
		let X_ACCESS_TOKEN = params.appToken;

		delete params.appToken;

		let order_id = params.order_id;
		let seatsToCancel = params.seatsToCancel;
		let cancellationType = params.cancellationType;

		return new Promise(function(resolve, reject) {
			fetch(`${SERVER_BASE_URL_CLIENT_DASHBOARD}/cancellation`, {
				method: "POST",
				headers: {
					"X-ACCESS-TOKEN": X_ACCESS_TOKEN,
					"Content-Type": "application/json"
				},
				body: JSON.stringify({ order_id, seatsToCancel, cancellationType })
			})
				.then(result =>
					result.json().then(res => {
						if (result.status === 200) {
							resolve(res);
						} else {
							reject(res);
						}
					})
				)
				.catch(error => {
					console.log(error);
				});
		});
	}
}
export default new OrderApi();
