import {
	ORDER_SAVE_ALL_ORDERS,
	ORDER_ADD_MORE_ORDERS,
	ORDER_SAVE_CANCELLATION_DATA,
	ORDER_FETCH_CANCELLATION_DATA,
	ORDER_SAVE_CANCELLATION_DATA_ERROR,
	ORDER_REQUEST_CANCELLATION,
	ORDER_SAVE_CANCELLATION,
	ORDER_SAVE_CANCELLATION_ERROR,
	ORDER_SAVE_OFFSET,
	ORDER_FETCH_ALL_ORDERS,
	ORDER_SAVE_ALL_ORDERS_ERROR
} from "./Saga";
const initialState = {
	orders: [],
	isOrdersLoading: false,
	isOrdersError: false,
	// Getting cancellation data
	isOrderCancelDataLoading: false,
	orderCancelData: null,
	isOrderCancelDataLoadingError: false,
	// Requesting to cancel
	isOrderCancelLoading: false,
	orderCancel: null,
	isOrderCancelError: false,
	offset: null
};

/**
 * @param {{ type: string; payload: any; }} action
 */
export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case ORDER_FETCH_ALL_ORDERS:
			return {
				...state,
				isOrdersLoading: true,
				isOrdersError: false
			};
		case ORDER_SAVE_ALL_ORDERS:
			return {
				...state,
				orders: action.payload,
				isOrdersLoading: false
			};
		case ORDER_SAVE_ALL_ORDERS_ERROR:
			return {
				...state,
				isOrdersLoading: false,
				isOrdersError: true
			};
		case ORDER_ADD_MORE_ORDERS:
			return {
				...state,
				orders: state.orders.concat(action.payload)
			};
		case ORDER_FETCH_CANCELLATION_DATA:
			return {
				...state,
				isOrderCancelDataLoading: true,
				orderCancelData: null,
				isOrderCancelDataLoadingError: false
			};
		case ORDER_SAVE_CANCELLATION_DATA:
			return {
				...state,
				isOrderCancelDataLoading: false,
				orderCancelData: action.payload
			};
		case ORDER_SAVE_CANCELLATION_DATA_ERROR:
			return {
				...state,
				isOrderCancelDataLoading: false,
				isOrderCancelDataLoadingError: action.payload
			};
		case ORDER_REQUEST_CANCELLATION:
			return {
				...state,
				isOrderCancelLoading: true,
				orderCancel: null,
				isOrderCancelError: false
			};
		case ORDER_SAVE_CANCELLATION:
			return {
				...state,
				isOrderCancelLoading: false,
				orderCancel: action.payload
			};
		case ORDER_SAVE_CANCELLATION_ERROR:
			return {
				...state,
				isOrderCancelLoading: false,
				isOrderCancelError: action.payload
			};
		case ORDER_SAVE_OFFSET:
			return {
				...state,
				offset: action.payload
			};
		default:
			return state;
	}
};
