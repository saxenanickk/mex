import React, { Component } from "react";
import {
	ActivityIndicator,
	View,
	Dimensions,
	FlatList,
	TouchableOpacity,
	TextInput,
	Keyboard,
	Platform,
	BackHandler,
	NativeModules
} from "react-native";
import { connect } from "react-redux";
import { styles } from "./style";
import {
	orderFetchAllOrders,
	orderFetchMoreOrders,
	orderFetchCancellationData
} from "./Saga";
import { Icon, DialogModal, EmptyView } from "../../../../Components";
import OrderItem from "./OrderItem";
import CancelOrder from "./CancelOrder";
import { OrderTextRegular, OrderTextBold } from "../../Components/OrderText";
import { get } from "lodash";

const { UIManager } = NativeModules;
if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}

const { width, height } = Dimensions.get("window");
class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			openFilter: false,
			selectedApp: null,
			searchQuery: "",
			isSearchedData: false,
			selectedOrderData: null,
			selectedSeatsToCancel: [],
			isCancelModalOpen: false
		};
		this.apps = {
			azgo: "AZGO",
			cleartrip: "CLEARTRIP",
			cleartrip_hotels: "CLEARTRIP HOTELS",
			events: "EVENTS",
			hungerbox: "MEX. CAFETERIA",
			mex_offers: "OFFERS",
			ola: "OLA",
			redbus: "REDBUS",
			swiggy: "SWIGGY"
		};
		this.handleBackPress = this.handleBackPress.bind(this);
		BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
	}

	handleBackPress() {
		if (this.state.isCancelModalOpen) {
			this.setState({ isCancelModalOpen: false });
		}
	}

	componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
	}

	componentDidMount() {
		// Fetch orders
		this.props.dispatch(
			orderFetchAllOrders({
				appToken: this.props.appToken,
				limit: 10
			})
		);
	}

	static getDerivedStateFromProps(props, state) {
		let nextState = {};
		if (
			props.isOrderCancelDataLoading ||
			props.isOrderCancelDataLoadingError ||
			(props.orderCancelData !== null &&
				props.orderCancelData.cancellable === "true")
		) {
			nextState = {
				...nextState,
				isCancelModalOpen: true
			};
		} else {
			nextState = {
				...nextState,
				isCancelModalOpen: false
			};
		}
		return nextState;
	}

	shouldComponentUpdate(props, state) {
		// Fetch orders based on selected app
		if (
			state.selectedApp !== null &&
			state.selectedApp !== this.state.selectedApp
		) {
			this.props.dispatch(
				orderFetchAllOrders({
					appToken: this.props.appToken,
					limit: 10,
					appid: state.selectedApp
				})
			);
		}
		// Fetch all orders
		if (
			state.selectedApp === null &&
			state.selectedApp !== this.state.selectedApp
		) {
			this.props.dispatch(
				orderFetchAllOrders({
					appToken: this.props.appToken,
					limit: 10
				})
			);
		}

		return true;
	}

	handleRefresh = () => {
		let params = {};
		params.limit = 10;
		params.appToken = this.props.appToken;

		if (this.state.selectedApp !== null) {
			params.appid = this.state.selectedApp;
		}
		this.props.dispatch(orderFetchAllOrders(params));

		this.setState({ searchQuery: "" });
	};

	handleOnEndReached = () => {
		if (this.state.searchQuery.trim() === "") {
			let params = {};

			params.limit = 10;
			params.offset = this.props.offset;
			params.appToken = this.props.appToken;

			if (this.state.selectedApp !== null) {
				params.appid = this.state.selectedApp;
			}

			this.props.dispatch(orderFetchMoreOrders(params));
		}
	};

	renderFilter = () => (
		<DialogModal
			visible={this.state.openFilter}
			onRequestClose={() => this.setState({ openFilter: false })}>
			<TouchableOpacity
				style={styles.modalContainer}
				activeOpacity={1}
				onPress={() => this.setState({ openFilter: false })}>
				<View style={styles.filterModal}>
					<TouchableOpacity
						style={styles.appName}
						onPress={() => {
							if (this.state.selectedApp !== null) {
								this.setState({ selectedApp: null, openFilter: false });
							}
						}}>
						<OrderTextBold
							style={{
								letterSpacing: 0.5,
								fontSize: 14
							}}>
							{"All"}
						</OrderTextBold>
						<Icon
							iconType={"ionicon"}
							iconName={
								this.state.selectedApp === null
									? "ios-radio-button-on"
									: "ios-radio-button-off"
							}
							iconSize={height / 35}
							iconColor={"#2C98F0"}
						/>
					</TouchableOpacity>
					{Object.keys(this.apps).map((key, index) => {
						return (
							<TouchableOpacity
								style={styles.appName}
								key={index}
								onPress={() => {
									if (this.state.selectedApp !== key) {
										this.setState({ selectedApp: key, openFilter: false });
									}
								}}>
								<OrderTextBold
									style={{
										letterSpacing: 0.5,
										fontSize: 14
									}}>
									{this.apps[key]}
								</OrderTextBold>
								<Icon
									iconType={"ionicon"}
									iconName={
										this.state.selectedApp === key
											? "ios-radio-button-on"
											: "ios-radio-button-off"
									}
									iconSize={height / 35}
									iconColor={"#2C98F0"}
								/>
							</TouchableOpacity>
						);
					})}
				</View>
			</TouchableOpacity>
		</DialogModal>
	);

	renderItem = data => {
		return (
			<OrderItem
				{...data}
				length={this.props.orders.length}
				handleOrderClick={this.handleOrderClick}
			/>
		);
	};

	/**
	 * @param {Object} data
	 * @param {string} data.app - App name
	 * @param {Object} data.item - Order details for the app
	 */
	handleOrderClick = ({ app, item }) => {
		console.log(app, item);
		this.setState({ selectedOrderData: item });
		this.props.dispatch(
			orderFetchCancellationData({
				appToken: this.props.appToken,
				tin: item.ref_booking_id
			})
		);
	};

	_renderListEmptyComponent = () => {
		if (this.props.isOrdersLoading) {
			return <ActivityIndicator />;
		}
		if (this.props.isOrdersEror) {
			return <OrderTextRegular>Something went wrong</OrderTextRegular>;
		}
		return (
			<EmptyView
				noSorry={true}
				isRefresh={false}
				noRecordFoundMesage={"No orders found"}
				sorryMessage={""}
				onRefresh={() => {
					this.flushUserListData();
					this.callUserListApi();
				}}
			/>
		);
	};

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.commentField}>
					<View style={styles.rowCommentView}>
						{!this.props.orders.length && !this.state.searchQuery.length ? (
							<OrderTextRegular
								style={{ paddingLeft: width / 30, color: "#6D7278", flex: 1 }}>
								{"Search by Order Id"}
							</OrderTextRegular>
						) : (
							<TextInput
								value={this.state.searchQuery}
								onChangeText={text => {
									if (text === "") {
										this.props.dispatch(
											orderFetchAllOrders({
												appToken: this.props.appToken,
												limit: 10
											})
										);
									}
									this.setState({ searchQuery: text });
								}}
								underlineColorAndroid={"transparent"}
								placeholder={"Search by Order Id"}
								style={styles.commentTextInput}
								selectionColor={"#6D7278"}
								editable={true}
								placeholderTextColor={"#6D7278"}
							/>
						)}
						<TouchableOpacity
							onPress={() => {
								if (this.state.searchQuery.trim() !== "") {
									Keyboard.dismiss();
									this.props.dispatch(
										orderFetchAllOrders({
											appToken: this.props.appToken,
											limit: 10,
											order_id: this.state.searchQuery.trim()
										})
									);
								} else {
									this.props.dispatch(
										orderFetchAllOrders({
											appToken: this.props.appToken,
											limit: 10
										})
									);
								}
							}}
							style={styles.searchButton}>
							<Icon
								iconType={"evilicon"}
								iconName={"search"}
								iconSize={height / 35}
								iconColor={"#fff"}
							/>
						</TouchableOpacity>
					</View>
				</View>
				<FlatList
					contentContainerStyle={{}}
					style={{ flex: 1 }}
					data={this.props.orders}
					showsVerticalScrollIndicator={false}
					refreshing={false}
					onRefresh={this.handleRefresh}
					onEndReachedThreshold={0.4}
					onEndReached={this.handleOnEndReached}
					keyExtractor={(item, index) => index.toString()}
					renderItem={this.renderItem}
					ListEmptyComponent={this._renderListEmptyComponent}
				/>
				<TouchableOpacity
					style={styles.button}
					activeOpacity={0.8}
					onPress={() => this.setState({ openFilter: !this.state.openFilter })}>
					{this.state.openFilter ? (
						<Icon
							iconType={"ionicon"}
							iconName={"ios-close"}
							iconSize={25}
							iconColor={"#fff"}
						/>
					) : (
						<Icon
							iconType={"ionicon"}
							iconName={"ios-funnel"}
							iconSize={20}
							iconColor={"#fff"}
						/>
					)}
				</TouchableOpacity>
				{this.renderFilter()}
				{/* Cancel Views */}
				{this.state.isCancelModalOpen ? (
					<CancelOrder selectedOrder={this.state.selectedOrderData} />
				) : null}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		orders: get(state, "order.home.orders", []),
		isOrdersLoading: get(state, "order.home.isOrdersLoading", false),
		isOrdersEror: get(state, "order.home.isOrdersEror", false),
		appToken: state.appToken.token,
		isOrderCancelDataLoading: get(
			state,
			"order.home.isOrderCancelDataLoading",
			false
		),
		isOrderCancelDataLoadingError: get(
			state,
			"order.home.isOrderCancelDataLoadingError",
			false
		),
		orderCancelData: get(state, "order.home.orderCancelData", null),
		offset: get(state, "order.home.offset", null)
	};
}

export default connect(mapStateToProps)(Home);
