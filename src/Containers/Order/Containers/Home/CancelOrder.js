import React, { Component } from "react";
import {
	View,
	TouchableOpacity,
	Dimensions,
	ActivityIndicator,
	Alert
} from "react-native";
import { connect } from "react-redux";
import { DialogModal, Icon, GoToast } from "../../../../Components";
import { styles } from "./style";
import {
	OrderTextBold,
	OrderTextMedium,
	OrderTextRegular
} from "../../Components/OrderText";
import {
	orderSaveCancellationData,
	orderRequestCancellation,
	orderSaveCancellation,
	orderSaveCancellationDataError,
	orderSaveCancellationError
} from "./Saga";
import { get } from "lodash";

const { width, height } = Dimensions.get("window");

class CancelOrder extends Component {
	state = {
		selectedSeatsToCancel: []
	};

	isFirstTimeCancellableFalse = false;

	static getDerivedStateFromProps(props, state) {
		let nextstate = {};
		// single ticket
		if (
			props.orderCancelData !== null &&
			props.orderCancelData.cancellable === "true" &&
			!Array.isArray(props.orderCancelData.cancellationCharges.entry)
		) {
			nextstate = {
				...nextstate,
				selectedSeatsToCancel: [
					props.orderCancelData.cancellationCharges.entry.key
				]
			};
		}

		// Multiple Tickets
		if (
			props.orderCancelData !== null &&
			props.orderCancelData.cancellable === "true" &&
			Array.isArray(props.orderCancelData.cancellationCharges.entry)
		) {
			// If partial cancel is false select all tickets
			if (props.orderCancelData.partiallyCancellable === "false") {
				let selectedSeatsToCancel = props.orderCancelData.cancellationCharges.entry.map(
					seat => seat.key
				);
				nextstate = {
					...nextstate,
					selectedSeatsToCancel
				};
			}
		}

		return nextstate;
	}

	shouldComponentUpdate(nextProps, nextState) {
		if (
			nextProps.orderCancelData !== null &&
			nextProps.orderCancelData.cancellable === "false" &&
			!this.isFirstTimeCancellableFalse
		) {
			this.isFirstTimeCancellableFalse = true;
			Alert.alert("Info", "Cannot cancel the ticket");
		}
		if (
			nextProps.orderCancelData === null &&
			this.props.orderCancelData !== null
		) {
			this.isFirstTimeCancellableFalse = false;
		}
		return true;
	}

	handleSingleRedbusTicketSelection = () => {
		let { selectedSeatsToCancel } = this.state;
		let index = selectedSeatsToCancel.indexOf(
			this.props.orderCancelData.cancellationCharges.entry.key
		);
		if (index > -1) {
			Alert.alert("Info", "Cannot deselect ticket");
			// selectedSeatsToCancel.splice(index, 1)
			// this.setState({ selectedSeatsToCancel })
		} else {
			selectedSeatsToCancel = [
				...selectedSeatsToCancel,
				this.props.orderCancelData.cancellationCharges.entry.key
			];
			this.setState({ selectedSeatsToCancel });
		}
	};

	handleMultipleRedbusTicketSelection = item => {
		if (this.props.orderCancelData.partiallyCancellable === "true") {
			let { selectedSeatsToCancel } = this.state;
			let index = selectedSeatsToCancel.indexOf(item.key);
			if (index > -1) {
				selectedSeatsToCancel.splice(index, 1);
				this.setState({ selectedSeatsToCancel });
			} else {
				selectedSeatsToCancel = [...selectedSeatsToCancel, item.key];
				this.setState({ selectedSeatsToCancel });
			}
		} else {
			Alert.alert("Info", "Partial cancellation is not available");
		}
	};

	handleRedbusTicketCancellation = () => {
		// Multiple tickets
		if (Array.isArray(this.props.orderCancelData.cancellationCharges.entry)) {
			let cancellationType =
				this.state.selectedSeatsToCancel.length ===
				this.props.orderCancelData.cancellationCharges.entry.length
					? "full"
					: "partial";
			// isPartial cancellable
			if (
				this.props.orderCancelData.partiallyCancellable === "true" &&
				this.state.selectedSeatsToCancel.length > 0
			) {
				this.props.dispatch(
					orderRequestCancellation({
						appToken: this.props.appToken,
						order_id: this.props.selectedOrder.order_id,
						seatsToCancel: this.state.selectedSeatsToCancel,
						cancellationType
					})
				);
			}
			// not partially cancellable
			else if (
				this.props.orderCancelData.partiallyCancellable === "false" &&
				this.state.selectedSeatsToCancel.length ===
					this.props.orderCancelData.cancellationCharges.entry.length
			) {
				this.props.dispatch(
					orderRequestCancellation({
						appToken: this.props.appToken,
						order_id: this.props.selectedOrder.order_id,
						seatsToCancel: this.state.selectedSeatsToCancel,
						cancellationType: "full"
					})
				);
			} else {
				GoToast.show(
					"Please select atleast one ticket to cancel",
					"Info",
					"LONG"
				);
			}
		}
		// Single Ticket
		else {
			if (this.state.selectedSeatsToCancel.length !== 1) {
				GoToast.show("Please select seats to cancel", "Info", "LONG");
			} else {
				this.props.dispatch(
					orderRequestCancellation({
						appToken: this.props.appToken,
						order_id: this.props.selectedOrder.order_id,
						seatsToCancel: this.state.selectedSeatsToCancel,
						cancellationType: "full"
					})
				);
			}
		}
	};

	closeDialogModal = () => {
		this.setState({ selectedSeatsToCancel: [] });
		this.props.dispatch(orderSaveCancellationData(null));
		this.props.dispatch(orderSaveCancellation(null));
		this.props.dispatch(orderSaveCancellationDataError(false));
		this.props.dispatch(orderSaveCancellationError(false));
	};

	render() {
		console.log(this.state, "selected seats");
		return (
			<DialogModal
				onRequestClose={() => this.closeDialogModal()}
				visible={
					this.props.isOrderCancelDataLoading ||
					this.props.isOrderCancelDataLoadingError ||
					(this.props.orderCancelData !== null &&
						this.props.orderCancelData.cancellable === "true")
				}>
				<TouchableOpacity
					onPress={() => null}
					activeOpacity={1}
					style={[
						styles.modalContainer,
						{ justifyContent: "center", alignItems: "center" }
					]}>
					<View style={styles.cancelContainer}>
						{/* Cancel Loading Indicator */}
						{this.props.isOrderCancelDataLoading ? (
							<View style={styles.loadingContainer}>
								<ActivityIndicator size={"large"} />
								<OrderTextBold style={styles.loadingText}>
									Fetching refund details, please wait
								</OrderTextBold>
							</View>
						) : null}
						{this.props.isOrderCancelLoading ? (
							<View style={styles.loadingContainer}>
								<ActivityIndicator size={"large"} />
								<OrderTextBold style={styles.loadingText}>
									Trying to cancel your ticket
								</OrderTextBold>
							</View>
						) : null}
						{/* Cancel Loading Details */}
						{this.props.orderCancelData !== null &&
						!this.props.isOrderCancelLoading &&
						this.props.orderCancelData.cancellable !== "false" &&
						this.props.orderCancel === null ? (
							<View style={styles.cancelDataContainer}>
								<OrderTextMedium
									style={{
										fontSize: 24,
										marginBottom: width / 20,
										alignSelf: "center"
									}}>
									Select seats to cancel
								</OrderTextMedium>
								{Array.isArray(
									this.props.orderCancelData.cancellationCharges.entry
								) ? (
									this.props.orderCancelData.cancellationCharges.entry.map(
										item => (
											<TouchableOpacity
												key={item.key}
												activeOpacity={1}
												onPress={this.handleMultipleRedbusTicketSelection.bind(
													this,
													item
												)}
												style={{
													flexDirection: "row",
													alignItems: "center",
													justifyContent: "space-around"
												}}>
												<Icon
													iconType={"font_awesome"}
													iconName={
														this.state.selectedSeatsToCancel.includes(item.key)
															? "check-square"
															: "square-o"
													}
													iconSize={height / 35}
													iconColor={"#00AEEF"}
												/>
												<View style={{ alignItems: "center" }}>
													<OrderTextBold>Seat No</OrderTextBold>
													<OrderTextRegular style={{ fontSize: 18 }}>
														{item.key}
													</OrderTextRegular>
												</View>
												<View style={{ alignItems: "center" }}>
													<OrderTextBold>Cancellation Fee</OrderTextBold>
													<OrderTextRegular style={{ fontSize: 18 }}>
														{item.value}
													</OrderTextRegular>
												</View>
											</TouchableOpacity>
										)
									)
								) : (
									<TouchableOpacity
										activeOpacity={1}
										onPress={this.handleSingleRedbusTicketSelection}
										style={{
											flexDirection: "row",
											alignItems: "center",
											justifyContent: "space-around"
										}}>
										<Icon
											iconType={"font_awesome"}
											iconName={
												this.state.selectedSeatsToCancel.includes(
													this.props.orderCancelData.cancellationCharges.entry
														.key
												)
													? "check-square"
													: "square-o"
											}
											iconSize={height / 35}
											iconColor={"#00AEEF"}
										/>
										<View style={{ alignItems: "center" }}>
											<OrderTextRegular style={{ fontSize: 16 }}>
												Seat No
											</OrderTextRegular>
											<OrderTextRegular style={{ fontSize: 18 }}>
												{
													this.props.orderCancelData.cancellationCharges.entry
														.key
												}
											</OrderTextRegular>
										</View>
										<View style={{ alignItems: "center" }}>
											<OrderTextRegular style={{ fontSize: 16 }}>
												Cancellation Fee
											</OrderTextRegular>
											<OrderTextRegular style={{ fontSize: 18 }}>
												{
													this.props.orderCancelData.cancellationCharges.entry
														.value
												}
											</OrderTextRegular>
										</View>
									</TouchableOpacity>
								)}
								<View
									style={{
										flexDirection: "row",
										justifyContent: "space-around",
										paddingTop: width / 20
									}}>
									<TouchableOpacity
										onPress={() => {
											this.setState({ selectedSeatsToCancel: [] });
											this.props.dispatch(orderSaveCancellationData(null));
											this.isFirstTimeCancellableFalse = false;
										}}
										style={{
											backgroundColor: "#000",
											width: "47%",
											alignItems: "center",
											paddingVertical: width / 50
										}}>
										<OrderTextMedium style={{ color: "#fff" }}>
											Don't Cancel
										</OrderTextMedium>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={this.handleRedbusTicketCancellation}
										style={{
											backgroundColor: "#000",
											width: "47%",
											alignItems: "center",
											paddingVertical: width / 50
										}}>
										<OrderTextMedium style={{ color: "#fff" }}>
											Cancel
										</OrderTextMedium>
									</TouchableOpacity>
								</View>
							</View>
						) : null}
						{this.props.orderCancel !== null ? (
							<View style={{ alignItems: "center" }}>
								<OrderTextBold style={{ fontSize: 18 }}>
									Ticket cancelled successfully!!
								</OrderTextBold>
								<TouchableOpacity
									style={{
										backgroundColor: "#000",
										marginTop: width / 20,
										paddingVertical: width / 40,
										paddingHorizontal: width / 20
									}}
									activeOpacity={1}
									onPress={this.closeDialogModal}>
									<OrderTextRegular style={{ color: "#fff" }}>
										CLOSE
									</OrderTextRegular>
								</TouchableOpacity>
							</View>
						) : null}
						{/* Cancel Error */}
						{this.props.isOrderCancelDataLoadingError ||
						this.props.isOrderCancelError ? (
							<View style={{ alignItems: "center" }}>
								<OrderTextBold style={{ textAlign: "center", fontSize: 18 }}>
									Something went Wrong!! Please try after sometime
								</OrderTextBold>
								<TouchableOpacity
									style={{
										backgroundColor: "#000",
										marginTop: width / 20,
										paddingVertical: width / 40,
										paddingHorizontal: width / 20
									}}
									activeOpacity={1}
									onPress={this.closeDialogModal}>
									<OrderTextRegular style={{ color: "#fff" }}>
										CLOSE
									</OrderTextRegular>
								</TouchableOpacity>
							</View>
						) : null}
					</View>
				</TouchableOpacity>
			</DialogModal>
		);
	}
}

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		// Cancel data fetching
		isOrderCancelDataLoading: get(
			state,
			"order.home.isOrderCancelDataLoading",
			false
		),
		isOrderCancelDataLoadingError: get(
			state,
			"order.home.isOrderCancelDataLoadingError",
			false
		),
		orderCancelData: get(state, "order.home.orderCancelData", null),
		// Request ticket to cancel
		isOrderCancelLoading: get(state, "order.home.isOrderCancelLoading", false),
		isOrderCancelError: get(state, "order.home.isOrderCancelError", false),
		orderCancel: get(state, "order.home.orderCancel", null)
	};
}

export default connect(mapStateToProps)(CancelOrder);
