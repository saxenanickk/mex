import { takeLatest, put, call } from "redux-saga/effects";
import Api from "../../Api";
export const ORDER_FETCH_ALL_ORDERS = "ORDER_FETCH_ALL_ORDERS";
export const ORDER_SAVE_ALL_ORDERS = "ORDER_SAVE_ALL_ORDERS";
export const ORDER_SAVE_OFFSET = "ORDER_SAVE_OFFSET";
export const ORDER_SAVE_ALL_ORDERS_ERROR = "ORDER_SAVE_ALL_ORDERS_ERROR";
export const ORDER_FETCH_MORE_ORDERS = "ORDER_FETCH_MORE_ORDERS";
export const ORDER_ADD_MORE_ORDERS = "ORDER_ADD_MORE_ORDERS";
export const ORDER_FETCH_CANCELLATION_DATA = "ORDER_FETCH_CANCELLATION_DATA";
export const ORDER_SAVE_CANCELLATION_DATA = "ORDER_SAVE_CANCELLATION_DATA";
export const ORDER_SAVE_CANCELLATION_DATA_ERROR =
	"ORDER_SAVE_CANCELLATION_DATA_ERROR";
export const ORDER_REQUEST_CANCELLATION = "ORDER_REQUEST_CANCELLATION";
export const ORDER_SAVE_CANCELLATION = "ORDER_SAVE_CANCELLATION";
export const ORDER_SAVE_CANCELLATION_ERROR = "ORDER_SAVE_CANCELLATION_ERROR";

export const orderFetchAllOrders = payload => ({
	type: ORDER_FETCH_ALL_ORDERS,
	payload
});
export const orderSaveAllOrders = payload => ({
	type: ORDER_SAVE_ALL_ORDERS,
	payload
});
export const orderSaveOffset = payload => ({
	type: ORDER_SAVE_OFFSET,
	payload
});
export const orderSaveAllOrdersError = payload => ({
	type: ORDER_SAVE_ALL_ORDERS_ERROR,
	payload
});
export const orderFetchMoreOrders = payload => ({
	type: ORDER_FETCH_MORE_ORDERS,
	payload
});
export const orderAddMoreOrders = payload => ({
	type: ORDER_ADD_MORE_ORDERS,
	payload
});

export const orderFetchCancellationData = payload => ({
	type: ORDER_FETCH_CANCELLATION_DATA,
	payload
});

export const orderSaveCancellationData = payload => ({
	type: ORDER_SAVE_CANCELLATION_DATA,
	payload
});

export const orderSaveCancellationDataError = payload => ({
	type: ORDER_SAVE_CANCELLATION_DATA_ERROR,
	payload
});

export const orderRequestCancellation = payload => ({
	type: ORDER_REQUEST_CANCELLATION,
	payload
});

export const orderSaveCancellation = payload => ({
	type: ORDER_SAVE_CANCELLATION,
	payload
});

export const orderSaveCancellationError = payload => ({
	type: ORDER_SAVE_CANCELLATION_ERROR,
	payload
});

export function* orderHomeSaga(dispatch) {
	yield takeLatest(ORDER_FETCH_ALL_ORDERS, handleOrderFetchAllOrders);
	yield takeLatest(ORDER_FETCH_MORE_ORDERS, handleFetchMoreOrders);
	yield takeLatest(ORDER_FETCH_CANCELLATION_DATA, handleFetchCancellationData);
	yield takeLatest(ORDER_REQUEST_CANCELLATION, handlerRequestCancellation);
}

function* handleOrderFetchAllOrders(action) {
	try {
		let response = yield call(Api.getOrders, action.payload);
		console.log("response is", response);
		if (response.success === 1) {
			if (
				response.data !== null &&
				response.data !== undefined &&
				response.data instanceof Array
			) {
				yield put(orderSaveOffset(response.offset));
				yield put(orderSaveAllOrders(response.data));
			}
		}
	} catch (error) {
		yield put(orderSaveAllOrdersError(true));
		console.log("error is", error);
	}
}

function* handleFetchMoreOrders(action) {
	try {
		console.log("fetch more orders");
		let response = yield call(Api.getOrders, action.payload);
		console.log("response is", response);
		if (response.success === 1) {
			if (
				response.data !== null &&
				response.data !== undefined &&
				response.data instanceof Array
			) {
				yield put(orderSaveOffset(response.offset));
				yield put(orderAddMoreOrders(response.data));
			}
		}
	} catch (error) {
		console.log("error is", error);
	}
}

function* handleFetchCancellationData(action) {
	try {
		let cancellationData = yield call(Api.getCancellationData, action.payload);
		if (cancellationData.success === 1) {
			yield put(orderSaveCancellationData(cancellationData.data));
		} else {
			yield put(orderSaveCancellationDataError(true));
		}
	} catch (error) {
		yield put(orderSaveCancellationDataError(true));
	}
}

function* handlerRequestCancellation(action) {
	let appToken = action.payload.appToken;
	try {
		let cancellation = yield call(Api.requestCancellation, action.payload);
		if (cancellation.success === 1) {
			yield put(orderSaveCancellation(cancellation.message));
			// Refreshing orders data to show cancelled items
			yield put(orderFetchAllOrders({ appToken, limit: 10 }));
		} else {
			yield put(orderSaveCancellationError(true));
		}
	} catch (error) {
		yield put(orderSaveCancellationError(true));
	}
}
