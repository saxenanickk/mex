import { StyleSheet, Dimensions, Platform } from "react-native";
import { font_two } from "../../../../Assets/styles";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	header: {
		width: width,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		backgroundColor: "#fff",
		paddingRight: width / 23,
		marginTop: width / 40
	},
	backButton: {
		height: height / 10,
		width: width / 7,
		justifyContent: "center",
		alignItems: "center"
	},
	rowView: {
		flexDirection: "row",
		alignItems: "center"
	},
	appIcon: {
		width: width / 15,
		height: width / 15,
		borderRadius: width / 30
	},
	appIconContainer: {
		borderWidth: 1,
		borderColor: "#e5e5e5",
		padding: width / 120,
		borderRadius: width / 15
	},
	button: {
		position: "absolute",
		width: width / 8,
		height: width / 8,
		borderRadius: width / 16,
		bottom: height / 20,
		right: width / 30,
		backgroundColor: "#2C98F0",
		justifyContent: "center",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		})
	},
	filterModal: {
		width: width / 1.5,
		position: "absolute",
		bottom: height / 6,
		right: width / 25,
		backgroundColor: "#fff",
		borderRadius: width / 50,
		paddingVertical: height / 90,
		paddingHorizontal: width / 30
	},
	modalContainer: {
		height: height,
		width: width,
		backgroundColor: "rgba(0,0,0,0.5)"
	},
	appName: {
		height: height / 20,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		borderBottomWidth: 1,
		borderBottomColor: "#ececec"
	},
	commentField: {
		width: width / 1.1,
		alignSelf: "center",
		margin: height / 80,
		height: height / 18,
		justifyContent: "center",
		backgroundColor: "#ffffff",
		borderRadius: width / 10,
		...Platform.select({
			android: { elevation: 3 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		}),
		paddingHorizontal: width / 50
	},
	editImage: {
		width: width / 20,
		height: width / 20
	},
	rowCommentView: {
		flex: 1,
		flexDirection: "row",
		alignItems: "center"
	},
	textInput: {
		width: width / 2,
		marginLeft: width / 50
	},
	commentTextInput: {
		flex: 1,
		color: "#000",
		fontFamily: font_two,
		paddingLeft: width / 30
	},
	searchButton: {
		width: height / 30,
		height: height / 30,
		borderRadius: height / 60,
		backgroundColor: "#2C98F0",
		justifyContent: "center",
		alignItems: "center"
	},
	container: {
		flex: 1,
		backgroundColor: "#fff"
	},
	renderItemContainer: {
		flex: 1,
		backgroundColor: "#fff",
		marginVertical: 5,
		marginHorizontal: 15,
		elevation: 3,
		shadowOffset: { width: 4, height: 4 },
		shadowColor: "#e5e5e5",
		shadowOpacity: 1,
		shadowRadius: 8,
		borderRadius: width / 40
	},
	itemContainer: {
		flex: 1,
		backgroundColor: "#ffffff",
		elevation: 3,
		borderRadius: width / 25,
		shadowOffset: { width: 1, height: 1 },
		shadowColor: "grey",
		shadowOpacity: 0.2
	},
	informationContainer: {
		flex: 1,
		flexDirection: "row",
		alignItems: "center",
		padding: 13
	},
	actionContainer: {
		flex: 1,
		padding: 10,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	actionButton: {
		padding: 10,
		borderWidth: 0,
		borderRadius: 10,
		backgroundColor: "transparent",
		borderColor: "#b9b9b9"
	},
	actionText: {
		color: "#00AEEF",
		fontFamily: Platform.OS === "ios" ? "Avenir" : "LatoRegular",
		letterSpacing: 0.5,
		fontSize: 13,
		fontWeight: "bold"
	},
	loadingContainer: {
		padding: width / 50,
		justifyContent: "center"
	},
	cancelDataContainer: {
		padding: width / 50
	},
	loadingText: { textAlign: "center", fontSize: 16, marginTop: width / 30 },
	cancelContainer: {
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "#fff",
		width: width / 1.1,
		height: height / 1.2,
		paddingVertical: height / 90,
		paddingHorizontal: width / 30
	},
	top: {
		flex: 1,
		flexDirection: "row",
		alignItems: "center",
		padding: 15
	},
	bottom: {
		flex: 1,
		flexDirection: "row",
		padding: 15,
		justifyContent: "space-between"
	},
	boldText: {
		letterSpacing: 0.5,
		fontSize: width / 26
	}
});
