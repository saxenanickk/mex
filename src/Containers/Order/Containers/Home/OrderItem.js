import React, { Component } from "react";
import { Animated, View, Dimensions } from "react-native";
import FastImage from "react-native-fast-image";
import { get, startCase, toLower } from "lodash";
import { styles } from "./style";
import {
	CLEARTRIP,
	OLA,
	REDBUS,
	SWIGGY,
	EVENTS,
	OFFERS,
	HUNGERBOX
} from "../../../../Assets/Img/Image";
import { OrderTextBold, OrderTextRegular } from "../../Components/OrderText";
import { _24to12 } from "../../../../utils/dateUtils";

const { width } = Dimensions.get("window");

export default class OrderItem extends Component {
	_defaultTransition = 250;

	constructor() {
		super();
		this.state = { showAction: false };
		this._rowOpacity = new Animated.Value(0);
		this.keyPair = {
			amount: "Amount",
			appid: "Application",
			created_at: "Time",
			fulfilment_status: "Fulfillment Status",
			order_id: "Order Id",
			order_status: "Status"
		};
	}

	componentDidMount() {
		let DURATION = 0;
		if (this.props.index < 5) {
			DURATION = this._defaultTransition * this.props.index;
		} else {
			DURATION = this._defaultTransition * 5;
		}

		Animated.timing(this._rowOpacity, {
			toValue: 1,
			duration: DURATION
		}).start();
	}

	getApplication(data = [], item) {
		if (!data.length) {
			if (item.information.hasOwnProperty("calendar")) {
				return "calendar";
			} else if (item.information.hasOwnProperty("event_details")) {
				return "events";
			}
		} else if (data.indexOf("ola") > -1) {
			return "ola";
		} else if (data.indexOf("swiggy") > -1) {
			return "swiggy";
		} else if (data.indexOf("cleartrip") > -1) {
			return "cleartrip";
		} else if (data.indexOf("cleartrip_hotel") > -1) {
			return "cleartrip_hotel";
		} else if (data.indexOf("events") > -1) {
			return "events";
		} else if (data.indexOf("calendar") > -1) {
			return "calendar";
		} else if (data.indexOf("redbus") > -1) {
			return "redbus";
		} else if (data.indexOf("mex_offers") > -1) {
			return "offers";
		} else if (data.indexOf("hungerbox") > -1) {
			return "mex_cafeteria";
		}
		return null;
	}

	getIcon(app = null) {
		if (app === "ola") {
			return OLA;
		} else if (app === "swiggy") {
			return SWIGGY;
		} else if (app === "cleartrip" || app === "cleartrip_hotel") {
			return CLEARTRIP;
		} else if (app === "events") {
			return EVENTS;
		} else if (app === "calendar") {
			return EVENTS;
		} else if (app === "redbus") {
			return REDBUS;
		} else if (app === "offers") {
			return OFFERS;
		} else if (app === "mex_cafeteria") {
			return HUNGERBOX;
		} else {
			return EVENTS;
		}
	}

	getOrderStatus(item) {
		// Do the similar thing for other apps to fetch order status
		if (item.appid === "hungerbox") {
			return get(
				item,
				"product.params.data.status",
				item.fulfilment_status
			).toUpperCase();
		}
		return item.order_status;
	}

	getOrderId(item) {
		// Do the similar thing for other apps to fetch order id
		if (item.appid === "hungerbox") {
			return get(item, "ref_booking_id", item.order_id);
		}
		return item.order_id;
	}

	render() {
		const { item, index } = this.props;
		const _left = this._rowOpacity.interpolate({
			inputRange: [0.5, 0.7, 1],
			outputRange: [-300, -10, 0]
		});
		const app = this.getApplication(item.appid, item);
		if (app === null) {
			return null;
		}
		const orderTime = new Date(item.created_at);
		const title =
			item.appid === "events"
				? get(item, "product.request.product.event_name", "Events")
				: startCase(toLower(app.replace("_", " ")));

		return (
			<Animated.View
				style={{
					opacity: this._rowOpacity,
					left: _left
				}}>
				<View style={styles.renderItemContainer}>
					<View style={styles.top}>
						<View style={styles.appIconContainer}>
							<FastImage source={this.getIcon(app)} style={styles.appIcon} />
						</View>
						<View style={{ marginLeft: width / 25 }}>
							<OrderTextBold style={styles.boldText}>{title}</OrderTextBold>
							<OrderTextRegular
								style={{
									color: "#5a5656"
								}}>{`On ${orderTime.toDateString()} ${_24to12(
								orderTime
							)}`}</OrderTextRegular>
						</View>
					</View>
					<View
						style={{
							borderBottomWidth: 0.5,
							width: "100%",
							borderBottomColor: "#D2D2D2"
						}}
					/>
					<View style={styles.bottom}>
						<View>
							<OrderTextRegular
								style={{
									color: "#6D7278",
									letterSpacing: 0.5,
									fontSize: width / 32,
									marginBottom: width / 120
								}}>
								{`${this.keyPair.order_status}`}
							</OrderTextRegular>
							<OrderTextBold
								style={{
									color: "#525252",
									letterSpacing: 0.5,
									fontSize: width / 30
								}}>
								{this.getOrderStatus(item)}
							</OrderTextBold>
						</View>
						<View style={{ marginLeft: width / 20, alignItems: "flex-end" }}>
							<OrderTextRegular
								style={{
									color: "#6D7278",
									letterSpacing: 0.5,
									fontSize: width / 32,
									marginBottom: width / 120
								}}>
								{`${this.keyPair.order_id}`}
							</OrderTextRegular>
							<OrderTextBold
								style={{
									color: "#525252",
									letterSpacing: 0.5,
									fontSize: width / 30
								}}>
								{this.getOrderId(item)}
							</OrderTextBold>
						</View>
					</View>
				</View>
			</Animated.View>
		);
	}
}
