import React from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import reducer from "./Reducer";
import { mpAppLaunch } from "../../utils/GoAppAnalytics";
import { getNewReducer, removeExistingReducer } from "../../../App";

export default class Order extends React.Component {
	constructor(props) {
		super(props);
		// Mixpanel App Launch
		mpAppLaunch({ name: "order" });
		getNewReducer({ name: "order", reducer: reducer });
	}

	componentWillUnmount() {
		removeExistingReducer("order");
	}

	render() {
		//Deeplink Params
		return <App {...this.props} />;
	}
}

AppRegistry.registerComponent("Order", () => Order);
