import React from "react";
import { Text } from "react-native";
import {
	font_one,
	font_two,
	font_three,
	font_four
} from "../../../Assets/styles";

export const OrderTextLight = props => (
	<Text
		style={[
			{ fontFamily: font_one, fontSize: props.size, color: props.color },
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

export const OrderTextRegular = props => (
	<Text
		style={[
			{ fontFamily: font_two, fontSize: props.size, color: props.color },
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

export const OrderTextMedium = props => (
	<Text
		style={[
			{ fontFamily: font_three, fontSize: props.size, color: props.color },
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

export const OrderTextBold = props => (
	<Text
		style={[
			{ fontFamily: font_four, fontSize: props.size, color: props.color },
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

const defaultProps = {
	size: null,
	color: "#000"
};

OrderTextBold.defaultProps = defaultProps;
OrderTextLight.defaultProps = defaultProps;
OrderTextMedium.defaultProps = defaultProps;
OrderTextRegular.defaultProps = defaultProps;
