import { all } from "redux-saga/effects";

import { orderHomeSaga } from "./Containers/Home/Saga";

export default function* orderSaga() {
	yield all([orderHomeSaga()]);
}
