import React, { Fragment } from "react";
import { StatusBar } from "react-native";
import Home from "./Containers/Home";

import { DrawerOpeningHack } from "../../Components";
import ApplicationToken from "../../CustomModules/ApplicationToken";

export default class App extends React.Component {
	render() {
		return (
			<Fragment>
				<ApplicationToken />
				<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
				<Home />
				<DrawerOpeningHack />
			</Fragment>
		);
	}
}
