import React, { Component } from "react";
import {
	View,
	ScrollView,
	TouchableOpacity,
	BackHandler,
	Dimensions,
	Image,
	Keyboard
} from "react-native";
import { remove } from "lodash";
import {
	GoappTextRegular,
	GoappTextInputRegular
} from "../../Components/GoappText";
import { styles } from "./style";
import MultipleItemSelector from "../../Components/MultipleItemSelector";
import { ProgressScreen } from "../../Components";
import ItemSelection from "./ItemSelection";
import Api from "./Api";
import { connect } from "react-redux";
import { communityGetMyProfile } from "../Community/Containers/UserProfile/Saga";
import DialogContext from "../../DialogContext";
import { ERRORCLOUD } from "../../Assets/Img/Image";
import GoAppAnalytics from "../../utils/GoAppAnalytics";
import { mexGetProfile } from "../../CustomModules/ApplicationToken/Saga";

const { height } = Dimensions.get("window");
const linkedInBaseApi = "https://api.linkedin.com/v2/";

class CommunityProfile extends Component {
	constructor(props) {
		super(props);
		this.state = {
			education: this.props.userProfile.education
				? this.props.userProfile.education.slice()
				: [],
			skills: this.props.userProfile.skills
				? this.props.userProfile.skills.slice()
				: [],
			hobbies_interests: this.props.userProfile.hobbies_interests
				? this.props.userProfile.hobbies_interests.slice()
				: [],
			about: this.props.userProfile.about ? this.props.userProfile.about : "",
			homeTown: this.props.userProfile.home_town
				? this.props.userProfile.home_town
				: "",
			itemSelector: null,
			isLoading: false
		};
		this.handleGoBack = this.handleGoBack.bind(this);
		this.isLoadingCommunity = false;
	}

	componentDidMount() {
		const { route } = this.props;

		const { isCommunity, linkedInTokenDetails } = route.params
			? route.params
			: {};

		this.routeName = (isCommunity && isCommunity) || false;
		this.routeName
			? (this.isLoadingCommunity = true)
			: (this.isLoadingCommunity = false);

		if (linkedInTokenDetails || this.routeName) {
			BackHandler.addEventListener("hardwareBackPress", this.handleGoBack);
		}
		this.isScreenLoadedFromConnect();
	}

	componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.handleGoBack);
	}

	isScreenLoadedFromConnect = () => {
		const { route } = this.props;

		const { linkedInTokenDetails } = route.params ? route.params : {};

		if (linkedInTokenDetails) {
			this.addLinkedInDataToProfile(linkedInTokenDetails);
			return true;
		}

		return false;
	};

	addLinkedInDataToProfile = async tokenDetails => {
		try {
			let linkedInProfile = {};
			const profileResp = fetch(
				`${linkedInBaseApi}me?projection=(id,localizedFirstName,localizedLastName,vanityName,localizedHeadline,profilePicture(displayImage~:playableStreams))`,
				{
					method: "GET",
					headers: {
						Authorization: `Bearer ${tokenDetails.token}`
					}
				}
			).then(res => {
				if (res.status === 200) {
					return res.json();
				}
			});

			const emailResponse = fetch(
				`${linkedInBaseApi}emailAddress?q=members&projection=(elements*(handle~))`,
				{
					method: "GET",
					headers: {
						Authorization: `Bearer ${tokenDetails.token}`
					}
				}
			).then(res => {
				if (res.status === 200) {
					return res.json();
				}
			});

			const [profile, email] = await Promise.all([profileResp, emailResponse]);
			if (profile) {
				linkedInProfile = {
					...linkedInProfile,
					...(profile.profilePicture && {
						profilePicture:
							profile.profilePicture["displayImage~"].elements[
								profile.profilePicture["displayImage~"].elements.length - 1
							].identifiers[0].identifier
					})
				};
			}
			this.setState(
				(state, props) => {
					return {
						profile: {
							...state.profile,
							linkedin: tokenDetails
						}
					};
				},
				() => {
					const { route } = this.props;

					const { fbTokenDetails } = route.params ? route.params : {};

					if (fbTokenDetails) {
						this.addFacebookDataToProfile();
					} else {
						this.setState({ isLoading: false });
					}
				}
			);
		} catch (error) {
			console.log("error of adding linkedin data is", error);
			this.setState({ isLoading: false });
		}
	};

	addFacebookDataToProfile = () => {
		const { route } = this.props;

		const { fbTokenDetails } = route.params ? route.params : {};

		this.setState({
			isLoading: false,
			profile: {
				...this.state.profile,
				facebook: fbTokenDetails
			}
		});
	};

	checkProfileValidity = () => {
		try {
			if (
				this.state.hobbies_interests === null ||
				this.state.hobbies_interests === undefined ||
				this.state.hobbies_interests.length === 0
			) {
				return false;
			} else if (
				!this.state.about ||
				this.state.about.trim() === "" ||
				this.state.about.length <= 30
			) {
				return false;
			} else {
				return true;
			}
		} catch (error) {
			console.log("error while check valid", error);
			return false;
		}
	};

	async handleGoBack() {
		if (this.state.itemSelector !== null) {
			this.setState({ itemSelector: null });
		} else {
			const { route } = this.props;

			const { isUseConnect, linkedInTokenDetails } = route.params
				? route.params
				: {};

			if (
				(isUseConnect || linkedInTokenDetails || this.isLoadingCommunity) &&
				this.checkProfileValidity() === false
			) {
				this.context.current.openDialog({
					type: "Confirmation",
					title: "Warning",
					message:
						"If you go Back,you can not use connect feature without adding about yourself and at least one hobby in your profile",

					rightPress: () => this.props.navigation.navigate("Community")
				});
			} else {
				this.props.navigation.goBack();

				this.isLoadingCommunity
					? this.props.navigation.goBack()
					: this.props.navigation.navigate("Community");
				this.isLoadingCommunity = false;
			}
		}
		return true;
	}

	handleTextChangeWithMaxLength = (type, text, maxLength) => {
		if (text.length === maxLength) {
			this.alert(
				"Info",
				"Sorry to interrupt, please keep your description short and sweet (140 characters)"
			);
		} else {
			this.handleTextChange(type, text);
		}
	};

	handleSave = () => {
		const { route } = this.props;

		const { isUseConnect, linkedInTokenDetails } = route.params
			? route.params
			: {};

		if (
			(isUseConnect || linkedInTokenDetails) &&
			(!this.state.hobbies_interests ||
				this.state.hobbies_interests.length === 0)
		) {
			this.alert(
				"Information",
				"You need to add your hobbies and interests to use connect feature."
			);
			return;
		}
		if (
			(isUseConnect || linkedInTokenDetails) &&
			(!this.state.about ||
				this.state.about.trim() === "" ||
				this.state.about.length <= 30)
		) {
			this.alert(
				"Information",
				"You need to add something about yourself (Minimum 30 characters) to use connect feature."
			);
			return;
		}
		this.saveCommunityProfileOfUser();
	};

	async saveCommunityProfileOfUser() {
		try {
			const { route } = this.props;

			const { linkedInTokenDetails } = route.params ? route.params : {};

			this.setState({ isLoading: true });
			this.setAnalytics("initiate", linkedInTokenDetails !== null);
			const resp = await Api.updateCommunityProfile({
				appToken: this.props.appToken,
				profile: {
					...this.state.profile,
					education: this.state.education,
					skills: this.state.skills,
					hobbies_interests: this.state.hobbies_interests,
					about: this.state.about.trim(),
					home_town: this.state.homeTown.trim()
				}
			});
			if (resp.success == 1) {
				this.setAnalytics("success", linkedInTokenDetails !== null);
				this.props.dispatch(mexGetProfile({ appToken: this.props.appToken }));
				if (linkedInTokenDetails) {
					this.props.dispatch(
						communityGetMyProfile({ appToken: this.props.appToken })
					);
					this.props.navigation.navigate("Community");
				} else {
					this.alert("Success!", "Profile Updated successfully");
				}
			} else {
				throw new Error("Unable to update Profile");
			}
			console.log("response of updating profile is", resp);
		} catch (error) {
			const { route } = this.props;

			const { linkedInTokenDetails } = route.params ? route.params : {};

			// Todo this is confusing
			this.setAnalytics("fail", linkedInTokenDetails !== null);
			console.log("error is", error);
			this.alert("Error", "Unable to update Profile");
		} finally {
			this.setState({ isLoading: false });
		}
	}

	alert = (title, message) => {
		this.context.current.openDialog({
			type: "Information",
			title: title,
			message: message
		});
	};

	removeItem = (data, type) => {
		if (type === "skills") {
			let temp = this.state.skills.slice();
			remove(temp, item => item === data);
			this.setState((state, props) => {
				return {
					skills: temp,
					itemSelector: null
				};
			});
		} else if (type === "education") {
			let temp = this.state.education.slice();
			remove(temp, item => item.name === data);
			this.setState((state, props) => {
				return {
					education: temp,
					itemSelector: null
				};
			});
		} else {
			let temp = this.state.hobbies_interests.slice();
			remove(temp, item => item === data);
			this.setState((state, props) => {
				return {
					itemSelector: null,
					hobbies_interests: temp
				};
			});
		}
	};

	addItem = (data, type) => {
		if (type === "skills") {
			this.setState((state, props) => {
				return {
					itemSelector: null,
					skills: data.map(item => (item.value ? item.value : item))
				};
			});
		} else if (type === "education") {
			this.setState((state, props) => {
				return {
					itemSelector: null,
					education: data.map(item =>
						item.value ? { name: item.value } : { name: item }
					)
				};
			});
		} else {
			this.setState((state, props) => {
				return {
					itemSelector: null,
					hobbies_interests: data.map(item => (item.value ? item.value : item))
				};
			});
		}
	};

	checkIsCorporate() {
		try {
			if (this.props.userProfile && this.props.userProfile.tenantdto) {
				const tenantDto = JSON.parse(this.props.userProfile.tenantdto);
				return (
					tenantDto.tenant_id !== null && tenantDto.tenant_id !== undefined
				);
			} else {
				return false;
			}
		} catch (error) {
			return false;
		}
	}

	setAnalytics(status, is_connect) {
		GoAppAnalytics.trackWithProperties("save_profile", {
			type: "corporate",
			status,
			is_connect
		});
	}
	render() {
		if (this.checkIsCorporate()) {
			return (
				<React.Fragment>
					<ScrollView
						style={styles.PIContainer}
						keyboardShouldPersistTaps={"always"}
						showsVerticalScrollIndicator={false}>
						<TouchableOpacity
							activeOpacity={1}
							onPress={() => Keyboard.dismiss()}>
							<View style={styles.PIInputBlock}>
								<GoappTextRegular style={styles.PIInputLabel}>
									About
								</GoappTextRegular>
								<GoappTextInputRegular
									placeholder={"Write something about yourself"}
									value={this.state.about}
									style={[
										styles.PIInput,
										{
											height: 100,
											textAlignVertical: "top",
											paddingVertical: height * 0.02
										}
									]}
									multiline={true}
									onChangeText={text => this.setState({ about: text })}
								/>
							</View>

							<View style={styles.PIInputBlock}>
								<GoappTextRegular style={styles.PIInputLabel}>
									Home Town
								</GoappTextRegular>
								<GoappTextInputRegular
									placeholder={"Enter Hometown"}
									value={this.state.homeTown}
									style={[styles.PIInput, { color: "#616675" }]}
									onChangeText={text => this.setState({ homeTown: text })}
								/>
							</View>
							<View style={styles.PIInputBlock}>
								<GoappTextRegular style={styles.PIInputLabel}>
									{"Education"}
								</GoappTextRegular>
								<MultipleItemSelector
									containerStyle={{ paddingHorizontal: 0 }}
									data={this.state.education.map(item => item.name)}
									type={""}
									isAddMore={true}
									addMore={selectionType => {
										this.setState({
											itemSelector: "Education"
										});
									}}
									isEditable={true}
									isShowType={false}
									removeItem={item => this.removeItem(item, "education")}
								/>
							</View>
							<View style={styles.PIInputBlock}>
								<GoappTextRegular style={styles.PIInputLabel}>
									{"Hobbies & Interests"}
								</GoappTextRegular>
								<MultipleItemSelector
									containerStyle={{ paddingHorizontal: 0 }}
									data={this.state.hobbies_interests.slice()}
									isAddMore={true}
									addMore={selectionType =>
										this.setState({
											itemSelector: "Hobbies & Interests"
										})
									}
									isEditable={true}
									isShowType={false}
									removeItem={item => this.removeItem(item, "hobby")}
								/>
							</View>
							<View style={styles.PIInputBlock}>
								<GoappTextRegular style={styles.PIInputLabel}>
									{"Professional Skills"}
								</GoappTextRegular>
								<MultipleItemSelector
									containerStyle={{ paddingHorizontal: 0 }}
									data={this.state.skills}
									isAddMore={true}
									addMore={selectionType => {
										this.setState({
											itemSelector: "Professional skills"
										});
									}}
									isEditable={true}
									isShowType={false}
									removeItem={item => this.removeItem(item, "skills")}
								/>
							</View>
							<View style={styles.PIActionsContainer}>
								<TouchableOpacity
									style={styles.PIActionCancel}
									onPress={() => {
										this.context.current.openDialog({
											type: "Confirmation",

											title: "Edit Profile",
											message: "Are you sure you want to discard the changes?",
											leftTitle: "No",
											rightTitle: "Yes",
											rightPress: () =>
												this.routeName === true
													? this.props.navigation.navigate("Community")
													: this.props.navigation.navigate("Goapp"),

											icon: ERRORCLOUD
										});
									}}>
									<GoappTextRegular style={styles.PILabelCancel}>
										Cancel
									</GoappTextRegular>
								</TouchableOpacity>
								<TouchableOpacity
									style={styles.PIActionSubmit}
									onPress={this.handleSave}>
									<GoappTextRegular style={styles.PILabelSubmit}>
										Save
									</GoappTextRegular>
								</TouchableOpacity>
							</View>
							{typeof this.state.itemSelector === "string" ? (
								<ItemSelection
									visible={true}
									type={this.state.itemSelector}
									selectedSkills={this.state.skills}
									selectedHobby={this.state.hobbies_interests}
									selectedEducation={this.state.education}
									onBackPress={() =>
										this.setState({
											itemSelector: null
										})
									}
									onDonePress={(data, type) => this.addItem(data, type)}
								/>
							) : null}
						</TouchableOpacity>
					</ScrollView>
					{this.state.isLoading ? (
						<ProgressScreen
							isMessage={true}
							primaryMessage={"Hang On..."}
							message={"Updating User Profile"}
							indicatorColor={"#2C98F0"}
							indicatorSize={height / 50}
						/>
					) : null}
				</React.Fragment>
			);
		} else {
			return (
				<View style={styles.errorContainer}>
					<Image source={ERRORCLOUD} resizeMode={"contain"} />
					<TouchableOpacity
						style={[styles.joinCommunityButton, { marginTop: height / 20 }]}>
						<GoappTextRegular
							style={{ color: "#fff" }}
							size={height / 50}
							onPress={() => this.props.navigation.navigate("CorpVerify")}>
							{"Join MEX. Community"}
						</GoappTextRegular>
					</TouchableOpacity>
				</View>
			);
		}
	}
}

function mapStateToProps(state) {
	return {
		appToken: state && state.appToken && state.appToken.token,
		userProfile: state.appToken.userProfile
	};
}

CommunityProfile.contextType = DialogContext;
export default connect(mapStateToProps)(CommunityProfile);
