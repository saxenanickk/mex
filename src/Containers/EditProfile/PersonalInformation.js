import React, { Component } from "react";
import { View, TouchableOpacity, Dimensions, ScrollView } from "react-native";
import {
	GoappTextRegular,
	GoappTextInputRegular
} from "../../Components/GoappText";
import { styles } from "./style";
import {
	GoPicker,
	ProgressScreen,
	GoToast,
	Icon,
	OtpValidation
} from "../../Components";
import Api from "./Api";
import { connect } from "react-redux";
import DialogContext from "../../DialogContext";
import { validatePhoneNumberDigit } from "../../utils/validation";
import { ERRORCLOUD } from "../../Assets/Img/Image";
import GoAppAnalytics from "../../utils/GoAppAnalytics";
import GoDateTimePicker from "../../Components/GoDateTimePicker";
import { mexGetProfile } from "../../CustomModules/ApplicationToken/Saga";

const { height } = Dimensions.get("window");

class PersonalInformation extends Component {
	constructor(props) {
		super(props);
		this.state = {
			fullName: this.props.userProfile.name,
			email: this.props.userProfile.email,
			phoneNumber: this.props.userProfile.phonenum
				? this.props.userProfile.phonenum
				: "",
			countryCode: "+91",
			phoneVerified: this.props.userProfile.phoneVerified,
			gender: this.props.userProfile.gender,
			dateOfBirth: this.props.userProfile.date_of_birth,
			isOtpVerify: false,
			isVerified: false,
			showDatePicker: false,
			message: "Updating User Profile"
		};
		this.verifyPhone = this.verifyPhone.bind(this);
		this.optSendCount = 0;
	}

	componentDidMount() {
		const { route } = this.props;

		const { isCommunity, linkedInTokenDetails } = route.params
			? route.params
			: {};

		this.routeName = (isCommunity && isCommunity) || false;
		this.routeName && this.props.navigation.navigate("CommunityProfile");

		if (linkedInTokenDetails || this.routeName) {
			this.props.navigation.navigate("CommunityProfile");
		}
		this.setNumberAndCode(this.props.userProfile.phonenum);
	}

	setNumberAndCode(phoneNum) {
		try {
			if (phoneNum) {
				let number = phoneNum.split("-");
				if (number.length === 2) {
					let countryCode = number[0];
					this.setState({ countryCode: countryCode, phoneNumber: number[1] });
				} else {
					this.setState({ phoneNumber: number[0] });
				}
			}
		} catch (error) {
			this.setState({ phoneNumber: phoneNum });
		}
	}

	handlePickerValue = ({ itemValue, itemIndex }) => {
		this.setState((state, props) => {
			return {
				gender: itemValue
			};
		});
	};

	handleTextChange = (text, inputKey) => {
		this.setState({ [inputKey]: text });
	};

	verifyPhone = () => {
		let number = "";
		if (this.state.countryCode[0] === "+") {
			number = `${this.state.countryCode}-${this.state.phoneNumber}`;
		} else {
			number = `+${this.state.countryCode}-${this.state.phoneNumber}`;
		}
		if (
			validatePhoneNumberDigit(number) &&
			this.state.countryCode.length > 0 &&
			this.state.phoneNumber.length > 0
		) {
			if (this.optSendCount === 0) {
				this.setAnalyticsVerify(
					"verify_phone",
					"otp_sent",
					this.state.phoneNumber
				);
			} else {
				this.setAnalyticsVerify(
					"verify_phone",
					"otp_resent",
					this.state.phoneNumber
				);
			}
			this.sendOtp(number);
			this.optSendCount = this.optSendCount + 1;
		} else {
			if (
				this.state.countryCode.length === 0 ||
				this.state.phoneNumber.length === 0
			) {
				GoToast.show("Phone number is mandatory.", "Error");
			} else {
				GoToast.show("Invalid phone number.", "Error");
			}
		}
	};

	closeOtpScreen = () => {
		this.setState({
			isOtpVerify: false,
			message: "Update User Profile",
			isVerified: false
		});
	};

	sendOtp = async number => {
		let message = "Sorry, we are unable to send OTP.";
		try {
			this.setState({ isLoading: true, message: "Sending OTP" });
			const resp = await Api.sendOtpToTheUser({
				appToken: this.props.appToken,
				phoneNum: number
			});
			if (resp.success === 1) {
				this.setState({ isOtpVerify: true });
			} else {
				message = resp.message;
				throw new Error("Unable to send Otp");
			}
		} catch (error) {
			this.context.current.openDialog({
				type: "Information",
				title: "Error",
				message: message
			});
			console.log("error is ", error);
		} finally {
			this.setState({ isLoading: false });
		}
	};

	verifyOtp = async otp => {
		let message = "Sorry, we are unable to verify OTP.";
		try {
			this.setState({
				isLoading: true,
				isOtpVerify: false,
				message: "Verifying OTP"
			});
			let number = "";
			if (this.state.countryCode[0] === "+") {
				number = `${this.state.countryCode}-${this.state.phoneNumber}`;
			} else {
				number = `+${this.state.countryCode}-${this.state.phoneNumber}`;
			}
			const resp = await Api.verifyOtp({
				appToken: this.props.appToken,
				phoneNum: number,
				verificationCode: otp
			});
			if (resp.success === 1) {
				this.setAnalyticsVerify(
					"verify_phone",
					"verify_success",
					this.state.phoneNumber
				);
				this.setState({ isVerified: true, isOtpVerify: false });
				this.context.current.openDialog({
					type: "Information",
					title: "Information",
					message: "Phone number verified."
				});
				this.props.dispatch(mexGetProfile({ appToken: this.props.appToken }));
			} else {
				if (resp.message === "Invalid verification code") {
					this.context.current.openDialog({
						type: "Confirmation",
						title: "Error!",
						message: "Incorrect OTP. Please try again.",

						leftPress: () => this.setState({ isOtpVerify: false }),
						rightTitle: "Retry",
						rightPress: () => this.setState({ isOtpVerify: true }),
						icon: ERRORCLOUD
					});
				} else {
					message = resp.message;
					throw new Error("Unable to verify Otp");
				}
			}
		} catch (error) {
			this.setAnalyticsVerify(
				"verify_phone",
				"verify_fail",
				this.state.phoneNumber
			);
			console.log("error is", error);
			this.context.current.openDialog({
				type: "Information",
				title: "Error",
				message: message,
				icon: ERRORCLOUD
			});
		} finally {
			this.setState({ isLoading: false });
		}
	};

	handlePhoneNumberChange = (type, text) => {
		this.setState({ [type]: text });
	};

	handleSave = async () => {
		try {
			if (this.state.fullName.trim() === "") {
				this.context.current.openDialog({
					type: "Information",
					title: "Error",
					message: "Name can't be empty"
				});
				return;
			}
			let number = "";
			if (this.state.countryCode[0] === "+") {
				number = `${this.state.countryCode}-${this.state.phoneNumber}`;
			} else {
				number = `+${this.state.countryCode}-${this.state.phoneNumber}`;
			}
			if (
				validatePhoneNumberDigit(number) &&
				this.state.countryCode.length > 0 &&
				this.state.phoneNumber.length > 0
			) {
				this.setState({ isSaveDisabled: true, isLoading: true });
				const body = {
					name: this.state.fullName.trim(),
					phonenum: number,
					gender: this.state.gender,
					date_of_birth: this.state.dateOfBirth
				};
				GoAppAnalytics.trackWithProperties("save_profile", {
					type: "personal",
					status: "initiate",
					body
				});
				const updateStatusResp = await Api.editMyProfile({
					appToken: this.props.appToken,
					body: body
				});
				if (updateStatusResp.success === 1) {
					GoAppAnalytics.trackWithProperties("save_profile", {
						type: "personal",
						status: "success"
					});
					this.setState({ isLoading: false });
					this.context.current.openDialog({
						type: "Information",
						title: "Success!",
						message: "Profile Updated successfully"
					});
					this.props.dispatch(mexGetProfile({ appToken: this.props.appToken }));
				} else {
					throw new Error("Failed to update profile");
				}
			} else {
				if (
					this.state.countryCode.length === 0 &&
					this.state.phoneNumber.length === 0
				) {
					GoToast.show("Phone number is mandatory.", "Error");
				} else {
					GoToast.show("Invalid phone number.", "Error");
				}
			}
		} catch (error) {
			GoAppAnalytics.trackWithProperties("save_profile", {
				type: "personal",
				status: "fail"
			});
			this.setState({ isSaveDisabled: false, isLoading: false });
			this.context.current.openDialog({
				type: "Information",
				title: "Error",
				message: "Unable to update Profile."
			});
		}
	};

	getParsedDate(dateOfBirth) {
		try {
			if (dateOfBirth && dateOfBirth !== "") {
				let dob = dateOfBirth.split("/");
				if (dob.length === 3) {
					return new Date(`${dob[1]}/${dob[0]}/${dob[2]}`);
				} else {
					return new Date();
				}
			} else {
				return new Date();
			}
		} catch (error) {
			return new Date();
		}
	}

	setAnalyticsVerify = (type, status, number) => {
		GoAppAnalytics.trackWithProperties(type, { status, number });
	};

	render() {
		return (
			<ScrollView
				style={styles.PIContainer}
				keyboardShouldPersistTaps={"always"}
				showsVerticalScrollIndicator={false}>
				<View style={styles.PIInputBlock}>
					<GoappTextRegular style={styles.PIInputLabel}>
						Full Name
					</GoappTextRegular>
					<GoappTextInputRegular
						placeholder={"Enter Full Name"}
						style={styles.PIInput}
						value={this.state.fullName}
						onChangeText={text => this.handleTextChange(text, "fullName")}
					/>
				</View>
				<View style={styles.PIInputBlock}>
					<GoappTextRegular style={styles.PIInputLabel}>Email</GoappTextRegular>
					<GoappTextInputRegular
						placeholder={"Enter Email Id"}
						style={[styles.PIInput, styles.PIInputEmail]}
						value={this.state.email}
						editable={false}
					/>
				</View>
				<View style={styles.PIInputBlock}>
					<GoappTextRegular style={styles.PIInputLabel}>
						{"Phone Number"}
						<GoappTextRegular color={"#ff0000"}>*</GoappTextRegular>
					</GoappTextRegular>
					<View style={styles.numberView}>
						<View style={styles.numberContainer}>
							<GoappTextInputRegular
								placeholder="XXX"
								maxLength={3}
								keyboardType="numeric"
								returnKeyType={"done"}
								style={styles.threeDigitPhoneNumber}
								value={this.state.countryCode}
								onChangeText={text =>
									this.handlePhoneNumberChange("countryCode", text)
								}
							/>
							<GoappTextRegular
								color={"rgba(0,0,0,0.5)"}
								style={styles.numberHyphen}>
								{"-"}
							</GoappTextRegular>
							<GoappTextInputRegular
								maxLength={10}
								placeholder="XXXXXXXXXX"
								keyboardType="numeric"
								style={styles.tenDigitPhoneNumber}
								value={this.state.phoneNumber}
								onChangeText={text =>
									this.handlePhoneNumberChange("phoneNumber", text)
								}
								returnKeyType={"done"}
							/>
						</View>
						{this.props.userProfile.phoneverified &&
						this.props.userProfile.phonenum ? (
							<Icon
								iconType={"feather"}
								iconName={"check-circle"}
								iconSize={height / 35}
								iconColor={"#58af61"}
							/>
						) : (
							<TouchableOpacity
								style={styles.PIverifyButton}
								onPress={this.verifyPhone}>
								<GoappTextRegular style={styles.PIverifyText} color={"#fff"}>
									{"Verify"}
								</GoappTextRegular>
							</TouchableOpacity>
						)}
					</View>
				</View>
				<View style={styles.PIInputBlock}>
					<GoappTextRegular style={styles.PIInputLabel}>
						Gender
					</GoappTextRegular>
					<View style={styles.goPickerView}>
						<GoPicker
							data={["Male", "Female", "Other"]}
							style={styles.goPicker}
							selectedValue={
								this.state.gender !== null ? this.state.gender : "Select"
							}
							updateSelectedValue={this.handlePickerValue}
							itemStyle={styles.dobText}
							textStyle={styles.dobText}
							textContainerStyle={[styles.goPickerText]}
						/>
					</View>
				</View>
				<View style={styles.PIInputBlock}>
					<GoappTextRegular style={styles.PIInputLabel}>
						Date Of Birth
					</GoappTextRegular>
					<TouchableOpacity
						style={styles.PIInput}
						onPress={() => this.setState({ showDatePicker: true })}>
						<GoappTextRegular style={styles.dobText}>
							{this.state.dateOfBirth ? this.state.dateOfBirth : "DD/MM/YYYY"}
						</GoappTextRegular>
					</TouchableOpacity>
				</View>
				<View style={styles.PIActionsContainer}>
					<TouchableOpacity
						style={styles.PIActionCancel}
						onPress={() => {
							this.context.current.openDialog({
								type: "Confirmation",
								title: "Edit Profile",
								message: "Are you sure you want to discard the changes?",
								leftTitle: "No",
								rightTitle: "Yes",
								rightPress: () => {
									GoAppAnalytics.trackWithProperties("save_profile", {
										type: "personal",
										status: "CANCELLED"
									});
									this.routeName
										? this.props.navigation.navigate("Community")
										: this.props.navigation.navigate("Goapp");
								},
								icon: ERRORCLOUD
							});
						}}>
						<GoappTextRegular style={styles.PILabelCancel}>
							Cancel
						</GoappTextRegular>
					</TouchableOpacity>
					<TouchableOpacity
						style={styles.PIActionSubmit}
						onPress={this.handleSave}>
						<GoappTextRegular style={styles.PILabelSubmit}>
							Save
						</GoappTextRegular>
					</TouchableOpacity>
				</View>
				{this.state.isOtpVerify ? (
					<OtpValidation
						open={this.state.isOtpVerify}
						handleClose={this.closeOtpScreen}
						heading={"Enter OTP"}
						message={`We've sent a text message to ${
							this.state.countryCode[0] === "+"
								? `${this.state.countryCode}-${this.state.phoneNumber}`
								: `+${this.state.countryCode}-${this.state.phoneNumber}`
						}`}
						verificationMessage={"Phone number verification successful!"}
						isVerified={false}
						onPressConfirm={this.verifyOtp}
						onPressResend={this.verifyPhone}
					/>
				) : null}
				{this.state.isLoading ? (
					<ProgressScreen
						isMessage={true}
						primaryMessage={"Hang On..."}
						message={this.state.message}
						indicatorColor={"#2C98F0"}
						indicatorSize={height / 50}
					/>
				) : null}
				{this.state.showDatePicker ? (
					<GoDateTimePicker
						isVisible={this.state.showDatePicker}
						onConfirm={date => {
							if (date !== null) {
								date = new Date(date);
								let dd = date.toString().split(" ")[2];
								let dob_picker = {
									dd,
									mm: date.getMonth() + 1,
									yyyy: date.getFullYear()
								};
								if ((dob_picker.mm + "").length === 1) {
									dob_picker.mm = "0" + dob_picker.mm;
								}
								if ((dob_picker.dd + "").length === 1) {
									dob_picker.dd = "0" + dob_picker.dd;
								}
								this.setState({
									dateOfBirth: `${dob_picker.dd}/${dob_picker.mm}/${
										dob_picker.yyyy
									}`,
									showDatePicker: false
								});
							} else {
								this.setState({
									showDatePicker: false
								});
							}
						}}
						onCancel={() => this.setState({ showDatePicker: false })}
						date={this.getParsedDate(this.state.dateOfBirth)}
						maximumDate={new Date()}
						minimumDate={new Date("01/01/1950")}
					/>
				) : null}
			</ScrollView>
		);
	}
}

PersonalInformation.contextType = DialogContext;
function mapStateToProps(state) {
	return {
		appToken: state && state.appToken && state.appToken.token,
		userProfile: state.appToken.userProfile
	};
}

export default connect(mapStateToProps)(PersonalInformation);
