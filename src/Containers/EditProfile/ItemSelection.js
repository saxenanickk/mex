import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	FlatList,
	Modal,
	TextInput,
	Keyboard
} from "react-native";
import { connect } from "react-redux";
import { remove, debounce } from "lodash";
import { Icon } from "../../Components";
import { styles } from "./style";
import { GoappTextMedium, GoappTextRegular } from "../../Components/GoappText";
import Api from "./Api";

const { width } = Dimensions.get("window");

class ItemSelection extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedItemList:
				this.props.type === "Professional skills"
					? this.props.selectedSkills
						? this.props.selectedSkills.slice()
						: []
					: this.props.type === "Education"
					? this.props.selectedEducation
						? this.props.selectedEducation.slice().map(item => item.name)
						: []
					: this.props.selectedHobby
					? this.props.selectedHobby.slice()
					: [],
			education: [],
			hobbies: [],
			skills: []
		};
	}

	toggleItemSelection = selectedItem => {
		Keyboard.dismiss();
		try {
			let temp = this.state.selectedItemList
				? this.state.selectedItemList.slice()
				: [];

			if (
				this.state.selectedItemList.findIndex(row => row === selectedItem) >= 0
			) {
				remove(temp, item => item === selectedItem);
				this.setState({ selectedItemList: temp });
			} else {
				temp.push(selectedItem);
				this.setState({ selectedItemList: temp });
			}
		} catch (error) {
			return null;
		}
	};

	getDataForItemSelection = () => {
		let data = [];
		if (this.props.type === "Professional skills") {
			data = this.state.skills;
		} else if (this.props.type === "Education") {
			data = this.state.education;
		} else {
			data = this.state.hobbies;
		}
		return data;
	};

	onDonePress = () => {
		if (this.props.type === "Professional skills") {
			this.props.onDonePress(this.state.selectedItemList, "skills");
		} else if (this.props.type === "Education") {
			this.props.onDonePress(this.state.selectedItemList, "education");
		} else {
			this.props.onDonePress(this.state.selectedItemList, "hobby");
		}
	};

	isItemSelected = value => {
		try {
			if (this.props.type === "Professional skills") {
				return (
					this.state.selectedItemList.findIndex(row =>
						typeof row === "string" ? row === value : row.value === value
					) >= 0
				);
			} else {
				return (
					this.state.selectedItemList.findIndex(row =>
						typeof row === "string" ? row === value : row.value === value
					) >= 0
				);
			}
		} catch (error) {
			return false;
		}
	};

	fetchEducation = async (appToken, searchQuery) => {
		const data = await Api.getEducationInstitute({
			appToken: this.props.appToken,
			searchQuery
		});
		if (data.success) {
			this.setState({ education: data.data });
		}
	};

	fetchSkills = async (appToken, searchQuery) => {
		const skills = await Api.getAllSkills({
			appToken: this.props.appToken,
			searchQuery
		});
		if (skills.success) {
			this.setState({ skills: skills.data });
		}
	};

	fetchHobbies = async (appToken, searchQuery) => {
		const hobbies = await Api.getHobbiesAndInterests({
			appToken: this.props.appToken,
			searchQuery
		});
		if (hobbies.success) {
			this.setState({ hobbies: hobbies.data });
		}
	};

	handleTextChange = debounce(text => {
		if (text.length > 0) {
			if (this.props.type === "Professional skills") {
				this.fetchSkills(this.props.appToken, text);
			} else if (this.props.type === "Education") {
				this.fetchEducation(this.props.appToken, text);
			} else {
				this.fetchHobbies(this.props.appToken, text);
			}
		} else {
			if (this.props.type === "Professional skills") {
				this.setState({ skills: [] });
			} else if (this.props.type === "Education") {
				this.setState({ education: [] });
			} else {
				this.setState({ hobbies: [] });
			}
		}
	}, 300);

	getPlaceholderText = () => {
		const { props } = this;
		return props.type === "Professional skills"
			? "Please enter your Skill"
			: props.type === "Hobbies & Interests"
			? "Please enter your Hobby"
			: "Please enter the name of your college";
	};
	render() {
		const { props } = this;
		return (
			<Modal
				visible={props.visible}
				// transparent={true}
				animationType={"slide"}
				onRequestClose={() => props.onBackPress()}>
				<View style={styles.container}>
					<View style={styles.dataContainer}>
						<View style={styles.header}>
							<TouchableOpacity
								style={styles.backButton}
								onPress={() => props.onBackPress()}>
								<GoappTextRegular style={{ marginLeft: 10 }} color={"#000000"}>
									{"Back"}
								</GoappTextRegular>
							</TouchableOpacity>
							<GoappTextMedium>{props.type}</GoappTextMedium>
							<TouchableOpacity
								style={styles.backButton}
								onPress={() => this.onDonePress()}>
								<GoappTextRegular style={{ marginRight: 10 }} color={"#000000"}>
									{"Done"}
								</GoappTextRegular>
							</TouchableOpacity>
						</View>
						<View style={styles.messageSection}>
							<TextInput
								style={styles.searchTextInput}
								placeholder={this.getPlaceholderText()}
								placeholderTextColor={"#000"}
								onChangeText={this.handleTextChange}
								underlineColorAndroid={"transparent"}
								selectionColor={"#000"}
							/>
						</View>
						<FlatList
							extraData={this.state}
							data={this.getDataForItemSelection()}
							style={styles.list}
							showsVerticalScrollIndicator={false}
							keyboardShouldPersistTaps={"always"}
							renderItem={({ item }) => {
								return (
									<TouchableOpacity
										style={styles.listItem}
										onPress={() => this.toggleItemSelection(item.name)}>
										<Icon
											iconType={"ionicon"}
											iconSize={width / 18}
											iconName={"ios-checkmark-circle"}
											iconColor={
												this.isItemSelected(item.name) ? "#5da4df" : "#eeeff5"
											}
										/>
										<GoappTextRegular style={styles.listItemName}>
											{item.name}
										</GoappTextRegular>
									</TouchableOpacity>
								);
							}}
							ItemSeparatorComponent={() => (
								<View style={styles.listItemSeparator} />
							)}
						/>
					</View>
				</View>
			</Modal>
		);
	}
}

const mapStateToProps = state => ({
	appToken: state.appToken.token
});

ItemSelection.defaultProps = {
	visible: false,
	onRequestClose: () => console.log("try to close the modal")
};

export default connect(mapStateToProps)(ItemSelection);
