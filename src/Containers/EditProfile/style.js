import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	header: {
		height: height / 12,
		backgroundColor: "transparent",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between"
	},
	backButton: {
		height: height / 12,
		justifyContent: "center",
		alignItems: "center",
		flexDirection: "row",
		paddingHorizontal: width / 30,
		paddingVertical: width / 60
	},
	PIContainer: {
		flex: 1,
		paddingHorizontal: 20,
		marginBottom: 20,
		backgroundColor: "#FFFFFF"
	},
	PIInputBlock: { paddingBottom: 10 },
	PIInputLabel: {
		paddingLeft: 10,
		paddingBottom: 10,
		color: "#616675",
		paddingTop: 20
	},
	PIInput: {
		backgroundColor: "#EFF1F4",
		color: "#000",
		borderRadius: 25,
		fontSize: height / 50,
		padding: 0,
		height: height / 15,
		justifyContent: "center",
		paddingHorizontal: width / 30
	},
	PIInputEmail: {
		color: "grey"
	},
	PIverifyButton: {
		backgroundColor: "#2c98f0",
		borderRadius: width / 15,
		height: height / 20,
		paddingHorizontal: width / 30,
		justifyContent: "center"
	},
	PIverifyText: { paddingHorizontal: 10 },
	PIActionsContainer: {
		flexDirection: "row",
		justifyContent: "flex-end",
		marginTop: 20
	},
	PIActionCancel: {
		marginRight: 10
	},
	PILabelCancel: {
		color: "#2C98F0",
		paddingHorizontal: 25,
		paddingVertical: 10
	},
	PIActionSubmit: {
		backgroundColor: "#2C98F0",
		height: height / 20,
		width: width / 4,
		borderRadius: width / 8,
		justifyContent: "center",
		alignItems: "center"
	},
	joinCommunityButton: {
		backgroundColor: "#2C98F0",
		height: height / 20,
		width: width / 2,
		borderRadius: width / 4,
		justifyContent: "center",
		alignItems: "center"
	},
	PILabelSubmit: {
		color: "#fff"
	},
	container: {
		flex: 1,
		backgroundColor: "rgba(0,0,0,0.5)",
		padding: 10
	},
	dataContainer: {
		backgroundColor: "#fff",
		borderRadius: 10,
		padding: 10,
		flex: 1,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 0, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	supportTextButton: {
		backgroundColor: "#000",
		flexDirection: "row",
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "black",
				shadowOpacity: 0.3
			}
		}),
		paddingVertical: height / 150,
		borderRadius: width / 20,
		justifyContent: "flex-start",
		paddingHorizontal: width / 50,
		alignItems: "center"
	},
	supportText: {
		color: "#fff",
		marginRight: width / 30
	},
	profileEditSection: {
		flex: 1,
		alignItems: "center",
		paddingVertical: height / 30
	},
	userImage: {
		width: width / 5,
		height: width / 5,
		borderRadius: width / 10
	},
	noUserImage: {
		width: width / 5,
		height: width / 5,
		borderRadius: width / 10,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#eeeff5"
	},
	changeImage: {
		position: "absolute",
		bottom: 1,
		left: width / 8.5,
		width: width / 10,
		height: width / 10,
		borderRadius: width / 20,
		backgroundColor: "#eeeff5",
		justifyContent: "center",
		alignItems: "center",
		...Platform.select({
			android: { elevation: 2 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.1
			}
		})
	},
	userProfileImage: {
		paddingVertical: 10
	},
	updateButton: {
		paddingHorizontal: width / 30,
		paddingVertical: height / 40,
		marginTop: height / 7,
		backgroundColor: "#eeeff5",
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 30
	},
	commentField: {
		width: width * 0.84,
		alignSelf: "center",
		marginTop: height / 50,
		height: height / 15,
		justifyContent: "center",
		paddingHorizontal: width / 30,
		backgroundColor: "#eeeff5",
		borderRadius: width / 50
	},
	rowView: {
		flexDirection: "row",
		alignItems: "center"
	},
	saveCommentButton: {
		borderWidth: 0,
		paddingVertical: height / 50,
		paddingHorizontal: width / 30
	},
	commentTextInput: {
		width: width / 1.5
	},
	genderSection: {
		width: width * 0.84,
		alignSelf: "center",
		marginTop: height / 50,
		flexDirection: "row",
		flexWrap: "wrap",
		height: height / 15,
		justifyContent: "space-between",
		alignItems: "center",
		paddingHorizontal: width / 30,
		borderRadius: width / 50
	},
	genderButton: {
		flexDirection: "row",
		alignItems: "center"
	},
	genderText: {
		marginLeft: width / 30
	},
	progressContainer: {
		height: height,
		justifyContent: "center",
		alignItems: "center",
		position: "absolute",
		top: 0,
		left: 0,
		bottom: 0,
		right: 0,
		backgroundColor: "rgba(0,0,0,0.5)"
	},
	profileViewSection: {
		width: width * 0.92,
		height: height / 4,
		alignSelf: "center",
		backgroundColor: "#fff",
		marginTop: height / 8,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.3
			}
		})
	},
	editProfileButtonView: {
		alignItems: "flex-end",
		width: width * 0.92,
		paddingHorizontal: width / 30,
		paddingVertical: height / 90
	},
	editProfileButton: {
		justifyContent: "flex-start",
		alignItems: "flex-end",
		width: width / 5,
		height: height / 20
	},
	profileInfoSection: {
		width: width * 0.92,
		alignItems: "center"
	},
	textStyle: {
		width: width / 1.2,
		textAlign: "center"
	},
	bottomButtonView: {
		position: "absolute",
		bottom: 0,
		width: width * 0.92,
		flexDirection: "row",
		justifyContent: "space-between"
	},
	followButton: {
		width: width * 0.46,
		height: height / 20,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		borderWidth: 1,
		borderColor: "#eeeff5"
	},
	followTextStyle: {
		marginLeft: width / 50,
		width: width / 5
	},
	messageSection: {
		paddingVertical: height / 90,
		paddingHorizontal: width / 40,
		backgroundColor: "#eeeff5",
		flexDirection: "row",
		borderRadius: width / 90,
		alignItems: "center"
	},
	searchTextInput: {
		width: width * 0.8,
		padding: 0,
		height: height / 20,
		color: "#000"
	},
	list: {
		height: height,
		backgroundColor: "#fff",
		marginTop: height / 90,
		borderRadius: width / 60
	},
	listItem: {
		paddingHorizontal: width / 30,
		paddingVertical: height / 60,
		flexDirection: "row",
		alignItems: "center"
	},
	listItemSeparator: {
		height: 1,
		backgroundColor: "#eeeff5"
	},
	listItemName: {
		marginLeft: width / 30,
		width: width / 1.3
	},
	userImageView: {
		justifyContent: "center",
		alignItems: "center"
	},
	errorContainer: {
		height: height / 2,
		justifyContent: "center",
		alignItems: "center"
	},
	goPickerView: {
		borderRadius: 25,
		backgroundColor: "#EFF1F4",
		width: width - width / 9,
		height: height / 15,
		justifyContent: "center",
		paddingHorizontal: width / 30
	},
	goPicker: {
		height: height / 15,
		width: width - width * 0.14,
		borderWidth: 1,
		fontSize: height / 50
	},
	goPickerText: {
		justifyContent: "space-between"
	},
	tenDigitPhoneNumber: {
		height: height / 23,
		width: width / 2.3,
		fontSize: height / 50,
		color: "#000",
		padding: 0
	},
	threeDigitPhoneNumber: {
		height: height / 23,
		width: width / 12,
		fontSize: height / 50,
		color: "#000",
		textAlign: "right"
	},
	dobText: { fontSize: height / 50 },
	numberView: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		backgroundColor: "#EFF1F4",
		borderRadius: 25,
		color: "#000",
		fontSize: height / 50,
		height: height / 15,
		paddingHorizontal: width / 30
	},
	numberContainer: {
		flexDirection: "row",
		alignItems: "center"
	},
	numberHyphen: {
		marginHorizontal: 4
	}
});
