import React, { Component } from "react";
import { View, BackHandler, Platform } from "react-native";
import { TopTab } from "../../utils/Navigators";
import PersonalInformation from "./PersonalInformation";
import CommunityProfile from "./CommunityProfile";

const TabNavigation = () => (
	// Todo hardcoded values
	<TopTab.Navigator
		tabBarOptions={{
			activeTintColor: "#2C98F0",
			inactiveTintColor: "#B2B7C0",
			labelStyle: {
				fontSize: 12
			},
			indicatorStyle: {
				backgroundColor: "#2C98F0"
			},
			style: {
				backgroundColor: "#fff",
				...Platform.select({
					android: {
						elevation: 0,
						borderBottomWidth: 0.5,
						borderColor: "#B2B7C0"
					}
				})
			},
			upperCaseLabel: false
		}}>
		<TopTab.Screen
			options={{ title: "Personal Information" }}
			name={"PersonalInformation"}
			component={PersonalInformation}
		/>
		<TopTab.Screen
			options={{ title: "Community Profile" }}
			name={"CommunityProfile"}
			component={CommunityProfile}
		/>
	</TopTab.Navigator>
);

export default class EditProfile extends Component {
	constructor(props) {
		super(props);
		this.handleBackPressed = this.handleBackPressed.bind(this);
		BackHandler.addEventListener("hardwareBackPress", this.handleBackPressed);
	}

	handleBackPressed = () => {
		this.props.navigation.navigate("Goapp");
		return true;
	};

	componentWillUnmount() {
		BackHandler.removeEventListener(
			"hardwareBackPress",
			this.handleBackPressed
		);
	}
	render() {
		return (
			<View style={{ flex: 1 }}>
				<TabNavigation screenProps={this.props.navigation} />
			</View>
		);
	}
}
