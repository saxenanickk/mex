import Config from "react-native-config";

const { SERVER_BASE_URL_COMMUNITY, MEX_LOGIN_BASE_URL } = Config;

export const MOBILE_NUMBER_VERIFICATION = "MOBILE NUMBER VERIFICATION";
/**
 * @typedef {Object} sendOtpParam
 * @property {string} appToken
 * @property {string} userId
 * @property {string} phoneNum
 */

/**
 * @typedef {Object} verifyOtpParam
 * @property {string} verificationCode
 */
const updateProfileParams = [
	"image",
	"gender",
	"email",
	"current_designation",
	"current_organisation",
	"education",
	"site_id",
	"past_organisation",
	"home_town",
	"date_of_birth",
	"hobbies_interests",
	"skills",
	"album",
	"name",
	"about",
	"linkedin",
	"facebook"
];

class EditProfileApi {
	getAllSkills(params) {
		return new Promise(function(resolve, reject) {
			let url = params.searchQuery
				? "getSkills?name=" + params.searchQuery
				: "getSkills";
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + url, {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getEducationInstitute(params) {
		return new Promise(function(resolve, reject) {
			let url = params.searchQuery
				? "getEducations?name=" + params.searchQuery
				: "getEducations";
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + url, {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	getHobbiesAndInterests(params) {
		return new Promise(function(resolve, reject) {
			let url = params.searchQuery
				? "getHobbiesAndInterests?name=" + params.searchQuery
				: "getHobbiesAndInterests";
			try {
				fetch(SERVER_BASE_URL_COMMUNITY + url, {
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response of hobby", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	updateCommunityProfile(params) {
		let body = {};
		if (params.profile) {
			updateProfileParams.map(item => {
				if (item === "image") {
					if (params.profile[item] && params.profile[item].startsWith("http")) {
						body[item] = params.profile[item];
					}
				} else if (
					item === "home_town" ||
					item === "current_designation" ||
					item === "name" ||
					item === "about"
				) {
					if (params.profile[item] !== undefined) {
						body[item] = params.profile[item]
							? params.profile[item].trim()
							: params.profile[item];
					}
				} else {
					if (params.profile[item] !== undefined) {
						body[item] = params.profile[item];
					}
				}
			});
		} else {
			body = {
				name: params.name,
				email: params.email,
				gender: params.gender,
				image: params.image
			};
		}
		console.log("body to edit user community profile is", body);
		return new Promise(function(resolve, reject) {
			try {
				fetch(MEX_LOGIN_BASE_URL + "editProfile", {
					method: "POST",
					headers: {
						"X-ACCESS-TOKEN": params.appToken,
						"content-type": "application/json"
					},
					body: JSON.stringify(body)
				})
					.then(response => {
						console.log("response is", response);
						response
							.json()
							.then(res => {
								console.log("response is", res);
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				console.log("ERROR: ", error);
				reject(error);
			}
		});
	}

	editMyProfile(params) {
		return new Promise((resolve, reject) => {
			let url = "editProfile";
			try {
				fetch(MEX_LOGIN_BASE_URL + url, {
					method: "POST",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": params.appToken
					},
					body: JSON.stringify(params.body)
				})
					.then(response => {
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(err => reject(err));
					})
					.catch(err => reject(err));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * function to send otp to user
	 * @param {sendOtpParam} params
	 */
	sendOtpToTheUser(params) {
		console.log("params to send the otp is", params);
		return new Promise((resolve, reject) => {
			try {
				fetch(`${MEX_LOGIN_BASE_URL}sendVerificationCode`, {
					method: "POST",
					headers: {
						"x-access-token": params.appToken,
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						otpType: MOBILE_NUMBER_VERIFICATION,
						phoneNum: params.phoneNum
					})
				})
					.then(response => {
						response
							.json()
							.then(res => resolve(res))
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				reject(error);
			}
		});
	}

	/**
	 * function to verify the otp
	 * @param {sendOtpParam & verifyOtpParam} params
	 */
	verifyOtp(params) {
		return new Promise((resolve, reject) => {
			try {
				fetch(`${MEX_LOGIN_BASE_URL}verifyVerificationCode`, {
					method: "POST",
					headers: {
						"x-access-token": params.appToken,
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						phoneNum: params.phoneNum,
						verificationCode: params.verificationCode,
						otpType: MOBILE_NUMBER_VERIFICATION
					})
				})
					.then(response => {
						response
							.json()
							.then(res => resolve(res))
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				reject(error);
			}
		});
	}
}

export default new EditProfileApi();
