import React from "react";
// Navigation Imports
import { NavigationContainer } from "@react-navigation/native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { Stack } from "./utils/Navigators";

// Route Imports
import Goapp from "./Containers/Goapp";
import CorpVerify from "./Containers/CorpVerify";
import Ola from "./Containers/Ola";
import Redbus from "./Containers/Redbus";
import Cleartrip from "./Containers/Cleartrip";
import CleartripHotel from "./Containers/CleartripHotel";
import Events from "./Containers/Events";
import Ignite from "./Containers/Ignite";
import Order from "./Containers/Order";
import Community from "./Containers/Community";
import Offers from "./Containers/Offers";
import FreeWiFi from "./Containers/FreeWiFi";
// import QuickRide from "./Containers/QuickRide";
import LoginScreen from "./Containers/LoginScreen";
import OnBoarding from "./Containers/OnBoarding";
import VerifyPhone from "./Containers/VerifyPhone";
import ContactUs from "./Containers/ContactUs";
import Traffic from "./Containers/Traffic";
import Shuttle from "./Containers/Shuttle";
import Share from "./Containers/Goapp/Containers/Share";
import Splash from "./Containers/Splash";
import Access from "./Containers/Access";
import EditProfile from "./Containers/EditProfile";
import { GoappAlertContext } from "./GoappAlertContext";
import GoAppAlertPopup from "./Components/GoAppAlertPopup";
import Vms from "./Containers/Vms";
import Polling from "./Containers/Polling";
import LinkView from "./Containers/LinkView";
import CampusAssist from "./Containers/CampusAssist";
import NetInfoModule from "./CustomModules/NetInfoModule";
import SiteSelection from "./Components/SiteSelection";
import { SiteSelectionContext } from "./Components/SiteSelection/SiteSelectionContext";
import GoAppAnalytics from "./utils/GoAppAnalytics";
import DialogContext from "./DialogContext";
import Dialog from "./Components/Dialog";

let data = null;

class App extends React.Component {
	constructor(props) {
		super(props);
		data = props;
		this.dialog = React.createRef();
		this.alertRef = React.createRef();
		this.siteSelectionRef = React.createRef();
	}

	prevRouteName = null;

	getActiveRouteName = state => {
		const route = state.routes[state.index];
		if (route.state) {
			return this.getActiveRouteName(route.state);
		}
		return route.name;
	};

	getActiveAppName = state => {
		console.log(state, "stateee");
		console.log(state.routes[state.index].name, "stateee");
		return state.routes[state.index].name;
	};

	render() {
		return (
			<React.Fragment>
				<NetInfoModule />
				<SiteSelectionContext.Provider value={this.siteSelectionRef}>
					<DialogContext.Provider value={this.dialog}>
						<GoappAlertContext.Provider value={this.alertRef}>
							<SafeAreaProvider>
								<NavigationContainer
									onStateChange={state => {
										const appName = this.getActiveAppName(state);
										const prevRouteName = this.prevRouteName;
										const currentRouteName = this.getActiveRouteName(state);
										if (prevRouteName !== currentRouteName) {
											GoAppAnalytics.logChangeOfScreen(
												appName,
												currentRouteName
											);
										}
										this.prevRouteName = currentRouteName;
									}}>
									<Stack.Navigator
										screenOptions={{
											headerBackTitle: "Back",
											gestureEnabled: true
										}}
										headerMode="float">
										<Stack.Screen
											options={{
												headerShown: false
											}}
											name="Splash"
											component={Splash}
										/>
										<Stack.Screen name="Shuttle" component={Shuttle} />
										<Stack.Screen
											options={{
												title: "MEX. WiFi"
											}}
											name="FreeWiFi"
											component={FreeWiFi}
										/>
										<Stack.Screen
											options={{
												headerShown: false
											}}
											name="Ignite"
											component={Ignite}
										/>
										<Stack.Screen
											options={{
												headerShown: false
											}}
											name="Events"
											component={Events}
										/>
										<Stack.Screen
											options={{
												headerShown: false
											}}
											name="Cleartrip"
											component={Cleartrip}
										/>
										<Stack.Screen
											options={{
												headerShown: false
											}}
											name="CleartripHotel"
											component={CleartripHotel}
										/>
										<Stack.Screen
											options={{
												headerShown: false
											}}
											name="Redbus"
											component={Redbus}
										/>
										<Stack.Screen
											options={{
												headerShown: false
											}}
											name="Goapp"
											component={Goapp}
										/>
										<Stack.Screen
											options={{
												headerShown: false
											}}
											name="Polling"
											component={Polling}
										/>
										<Stack.Screen
											options={{
												headerShown: false
											}}
											name="Ola"
											component={props => <Ola {...data} {...props} />}
										/>
										<Stack.Screen
											options={{
												headerShown: false
											}}
											name="Offers"
											component={Offers}
										/>
										<Stack.Screen
											options={{
												headerShown: false
											}}
											name="Vms"
											component={Vms}
										/>
										<Stack.Screen
											options={{ title: "Edit Profile Information" }}
											name={"EditProfile"}
											component={EditProfile}
										/>
										<Stack.Screen
											options={{
												headerShown: false
											}}
											name="CampusAssist"
											component={CampusAssist}
										/>
										<Stack.Screen
											options={{
												title: "Orders"
											}}
											name="Order"
											component={Order}
										/>
										<Stack.Screen
											options={{
												title: "Live Traffic"
											}}
											name="Traffic"
											component={Traffic}
										/>
										<Stack.Screen
											options={{ title: "Contact Us" }}
											name="ContactUs"
											component={ContactUs}
										/>
										<Stack.Screen
											options={{ title: "Corporate Verification" }}
											name="CorpVerify"
											component={CorpVerify}
										/>
										<Stack.Screen
											options={{
												headerShown: false
											}}
											name="Login"
											component={LoginScreen}
										/>
										<Stack.Screen
											options={{
												headerShown: false
											}}
											name="OnBoarding"
											component={OnBoarding}
										/>
										<Stack.Screen
											options={{
												headerShown: false
											}}
											name="Community"
											component={Community}
										/>
										<Stack.Screen
											options={{
												title: "Invite Friends"
											}}
											name="Share"
											component={Share}
										/>
										<Stack.Screen name="Access" component={Access} />
										<Stack.Screen
											options={{ title: "Verify Phone Number" }}
											name="VerifyPhone"
											component={VerifyPhone}
										/>
										<Stack.Screen
											options={({ route }) => ({ title: route.params.title })}
											name="LinkView"
											component={LinkView}
										/>
									</Stack.Navigator>
								</NavigationContainer>
							</SafeAreaProvider>
						</GoappAlertContext.Provider>
					</DialogContext.Provider>
				</SiteSelectionContext.Provider>
				<SiteSelection ref={this.siteSelectionRef} />
				<GoAppAlertPopup ref={this.alertRef} />
				<Dialog ref={this.dialog} />
			</React.Fragment>
		);
	}
}

export default App;
