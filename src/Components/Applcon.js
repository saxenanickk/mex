import React from "react";
import {
	TouchableOpacity,
	Image,
	Dimensions,
	StyleSheet,
	View
} from "react-native";
import { GoappTextRegular } from "./GoappText";

const { width, height } = Dimensions.get("window");

const AppIcon = props => {
	return (
		<TouchableOpacity onPress={props.onPress} style={styles.container}>
			<Image source={props.icon} style={styles.iconStyle} />
			<View
				style={{
					height: height / 23.13,
					alignItems: "center",
					justifyContent: "center"
				}}>
				<GoappTextRegular
					style={styles.titleStyle}
					numberOfLines={1}
					ellipsizeMode={"tail"}>
					{props.title}
				</GoappTextRegular>
			</View>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	container: {
		width: width / 4.5,
		alignItems: "center",
		height: height / 8.42
	},
	titleStyle: {
		fontSize: height / 53.3,
		color: "#ffffff"
	},
	iconStyle: {
		borderColor: "#6f6f6f",
		borderWidth: 0,
		borderRadius: height / 56.2,
		width: height / 13.24,
		height: height / 13.24
	}
});

export default AppIcon;
