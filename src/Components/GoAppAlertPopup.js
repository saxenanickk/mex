import React, { Component } from "react";

import { StyleSheet, Dimensions, Platform } from "react-native";
import FastImage from "react-native-fast-image";
import {
	GoappTextMedium,
	GoappTextBold,
	GoappTextInputRegular,
	GoappTextLight
} from "./GoappText";
import Alert from "./Alert";
const { width, height } = Dimensions.get("window");

export default class GoAppAlertPopup extends Component {
	constructor(props) {
		super(props);
		this.state = {
			alertType: null,
			title: "",
			message: "",
			visible: false,
			icon: null,
			cancelAction: null,
			okAction: null,
			inputText: "",
			inputError: false,
			isInputRequired: false,
			isCrossIconVisible: false
		};
	}
	handleOkClick = () => {
		const { okAction } = this.state;
		if (okAction && okAction.onPress) {
			if (this.state.isInputRequired) {
				if (this.state.inputText.trim().length === 0) {
					this.setState({ inputError: true });
					return;
				}
				okAction.onPress(this.state.inputText);
			} else {
				okAction.onPress();
			}
		}
		this.handleClose();
	};

	handleCrossClick = () => {
		const { cancelAction } = this.state;
		if (cancelAction && cancelAction.onPress) {
			cancelAction.onPress();
		}
		this.handleClose();
	};
	setInputText = value =>
		this.setState({ inputText: value, inputError: false });

	handleClose = () => {
		this.setState({
			alertType: null,
			title: "",
			message: "",
			visible: false,
			icon: null,
			cancelAction: null,
			okAction: null,
			inputText: "",
			inputError: false,
			isInputRequired: false,
			isCrossIconVisible: false
		});
	};

	openAlert = props => {
		let state = {};
		this.setState({
			...state,
			visible: true,
			title: props.title || "",
			message: props.message || "",
			icon: props.icon || null,
			isInputRequired: props.isInputRequired || false,
			alertType: props.alertType || "DEFAULT_ALERT",
			cancelAction: props.cancelAction || null,
			okAction: props.okAction || null,
			isCrossIconVisible: props.isCrossIconVisible || false,
			canCloseOnBackDropPress: props.canCloseOnBackDropPress || false
		});
	};
	renderImage = icon => {
		return icon ? (
			<FastImage source={icon} style={styles.imageStyle} resizeMode="contain" />
		) : null;
	};
	renderTitle = title => {
		return (
			<GoappTextBold
				size={height / 65}
				numberOfLines={1}
				style={styles.titleStyles}>
				{title}
			</GoappTextBold>
		);
	};
	renderMessage = message => {
		return (
			<GoappTextMedium size={height / 60} style={styles.massageStyles}>
				{message}
			</GoappTextMedium>
		);
	};
	renderInput = (inputText, inputErrorMessage) => {
		return this.state.isInputRequired ? (
			<>
				<GoappTextInputRegular
					value={inputText}
					style={styles.inputContainer(this.state.inputError)}
					onChangeText={text => this.setInputText(text)}
				/>
				{this.renderInputError(inputErrorMessage)}
			</>
		) : null;
	};
	renderInputError = errorMessage => {
		return (
			this.state.inputError && (
				<GoappTextLight
					size={height / 95}
					color={"red"}
					style={styles.warningText}>
					{errorMessage}
				</GoappTextLight>
			)
		);
	};
	render() {
		// const self = this;
		const commonMethods = {
			handleClose: this.handleClose,
			handleOkClick: this.handleOkClick,
			handleCrossClick: this.handleCrossClick
		};
		const alertComponents = {
			DEFAULT_ALERT: (
				<Alert
					{...this.state}
					{...commonMethods}
					renderImage={() => this.renderImage(this.state.icon)}
					renderTitle={() => this.renderTitle(this.state.title)}
					renderMessage={() => this.renderMessage(this.state.message)}
					renderInput={() =>
						this.renderInput(
							this.state.inputText,
							"Please enter your reason to reopen the ticket"
						)
					}
				/>
			)
		};
		if (this.state.visible && !this.state.alertType) {
			return alertComponents.DEFAULT_ALERT;
		}
		return this.state.visible && alertComponents[this.state.alertType];
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "rgba(0,0,0,0.5)",
		borderWidth: 2
	},
	imageStyle: {
		width: width / 2.95,
		height: height / 15.68,
		alignSelf: "center",
		marginVertical: height * 0.01
	},
	iconContainer: {
		position: "absolute",
		right: 0,
		top: 0,
		paddingHorizontal: width / 50,
		alignItems: "flex-end"
	},
	buttonContainer: cancelAction => ({
		width: width / 1.83,
		marginTop: height / 32,
		marginBottom: height * 0.01,
		flexDirection: "row",
		alignSelf: "center",
		justifyContent: cancelAction ? "space-between" : "center"
	}),
	cancelActionStyle: {
		backgroundColor: "#2C98F0",
		width: width / 4.1,
		height: height / 23.7,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 20,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.2
			}
		})
	},

	okActionStyle: cancelAction => ({
		backgroundColor: "#2C98F0",
		width: cancelAction ? width / 4.1 : width / 1.6,
		height: height / 23.7,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 20,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.2
			}
		})
	}),
	modalContainer: {
		width: width / 1.29,
		paddingVertical: height * 0.01,
		borderRadius: width / 50,
		backgroundColor: "#fff"
	},
	inputContainer: inputError => ({
		borderBottomWidth: 1,
		marginTop: width * 0.07,
		borderColor: inputError ? "red" : "#979797",
		width: "80%",
		alignSelf: "center"
	}),
	titleStyles: {
		width: width / 1.83,
		alignSelf: "center",
		textAlign: "center",
		marginVertical: height * 0.01
	},
	massageStyles: {
		width: width / 1.83,
		alignSelf: "center",
		textAlign: "center",
		color: "#4A4A4A",
		marginVertical: height / 80
	},
	warningText: { alignSelf: "flex-end", width: "70%" },
	iconStyle: { padding: width / 100 }
});
