import { Platform, ToastAndroid, Alert } from "react-native";
import I18n from "../Assets/strings/i18n";

/**
 * Custom Compoent for Goapp.
 * This will handle the tasks like showing
 * a small information as an Alert(iOS)
 * and Toast(Android).
 *
 *
 *	Usage:

 		import GoToast from "./Components"
		GoToast.show(MESSAGE, TITLE, DURATION)
 */

const GoToast = {
	show: (message, title = I18n.t("error"), duration = "SHORT") =>
		Platform.OS === "ios"
			? Alert.alert(title, message)
			: ToastAndroid.show(
					message,
					duration === "LONG" ? ToastAndroid.LONG : ToastAndroid.SHORT
			  )
};

export default GoToast;
