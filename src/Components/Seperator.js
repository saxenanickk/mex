import React from "react";
import { View } from "react-native";

const Seperator = props => {
	return (
		<View
			style={[
				{
					width: props.width || 1,
					height: props.height || 1,
					backgroundColor: props.color || "#777777"
				},
				props.style
			]}
		/>
	);
};

export default Seperator;
