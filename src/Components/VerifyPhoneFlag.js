import React from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import Icon from "./Icon";
import { GoappTextRegular, GoappTextMedium } from "./GoappText";
import PropTypes from "prop-types";

const { width, height } = Dimensions.get("window");
/**
 * Component to display verify phone number section
 */
export const VerifyPhoneFlag = props => (
	<View
		style={{
			width: width,
			paddingVertical: height / 60,
			backgroundColor: "#fff",
			marginTop: height / 30,
			paddingHorizontal: width / 22
		}}>
		<View style={{ flexDirection: "row" }}>
			<Icon
				iconType={"ionicon"}
				iconName={"ios-information-circle-outline"}
				iconColor={"#2C98F0"}
				iconSize={height / 28}
			/>
			<GoappTextRegular
				style={{
					marginLeft: width / 30,
					width: width / 1.3,
					fontSize: height / 45
				}}>
				{"To get the best experience of MEX. kindly verify your phone number"}
			</GoappTextRegular>
		</View>
		<TouchableOpacity
			style={{ alignSelf: "flex-end", marginTop: height / 90 }}
			onPress={() => props.onVerifyNumberClick()}>
			<GoappTextMedium
				style={{
					fontSize: height / 45,
					width: width / 1.3,
					textAlign: "right",
					color: "#2C98F0"
				}}>
				{"Verify Phone Number"}
			</GoappTextMedium>
		</TouchableOpacity>
	</View>
);

VerifyPhoneFlag.propTypes = {
	/**
	 *  called when clicked on verify phone number
	 */
	onVerifyNumberClick: PropTypes.func.isRequired
};
