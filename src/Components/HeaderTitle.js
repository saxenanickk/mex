import React from "react";
import { Dimensions } from "react-native";
import { GoappTextRegular } from "./GoappText";

const HeaderTitle = ({ title }) => (
	<GoappTextRegular size={Dimensions.get("window").height / 40}>
		{title}
	</GoappTextRegular>
);

export default HeaderTitle;
