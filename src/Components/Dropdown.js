import React, { Component } from "react";
import {
	TouchableOpacity,
	TouchableHighlight,
	StyleSheet,
	Dimensions,
	ScrollView
} from "react-native";
import DialogModal from "./DialogModal";
import { GoappTextRegular } from "./GoappText";

const { height, width } = Dimensions.get("window");

export default class Dropdown extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedValue: "",
			isModalVisible: false,
			isClicked: false
		};

		this.containerRef = React.createRef();
	}

	static getDerivedStateFromProps(nextProps, prevState) {
		if (
			nextProps.selectedValue &&
			nextProps.selectedValue !== prevState.selectedValue
		) {
			return { selectedValue: nextProps.selectedValue };
		}
		return null;
	}

	handleOnPress = item => {
		this.setState(
			{
				selectedValue: item,
				isClicked: true
			},
			() =>
				setTimeout(
					() =>
						this.setState({
							isClicked: false,
							isModalVisible: false
						}),
					10
				)
		);
		this.props.onPress(item);
	};

	onCloseHandler = () => {
		this.setState({ isModalVisible: false, isClicked: false });
	};

	render() {
		const { data, dataStyle } = this.props;
		const { selectedValue, isClicked } = this.state;

		return (
			<>
				<DialogModal
					dialogModalBackgroundStyle={styles.dialogModalBackgroundStyle}
					visible={this.state.isModalVisible}
					onRequestClose={() => this.onCloseHandler()}>
					<ScrollView
						ref={this.containerRef}
						onLayout={event => {
							const { height: h } = event.nativeEvent.layout;

							// Logic to align the container vertically center
							this.containerRef.current.setNativeProps({
								style: { marginVertical: (height - h) / 2 }
							});
						}}
						style={styles.classTypeView(data.length)}>
						{data
							? data.map((item, index) => (
									<TouchableHighlight
										key={index}
										activeOpacity={1}
										underlayColor={"#DDDDDD"}
										style={[
											styles.classTypeButton,
											isClicked && selectedValue === item
												? styles.backgroundColorAfterItemClicks
												: {}
										]}
										onPress={() => {
											this.handleOnPress(item.name);
										}}>
										<GoappTextRegular style={styles.classTypeText}>
											{item.name}
										</GoappTextRegular>
									</TouchableHighlight>
							  ))
							: null}
					</ScrollView>
				</DialogModal>
				<TouchableOpacity
					onPress={() => this.setState({ isModalVisible: true })}>
					<GoappTextRegular style={dataStyle}>
						{selectedValue ? selectedValue : "Select"}
					</GoappTextRegular>
				</TouchableOpacity>
			</>
		);
	}
}

const styles = StyleSheet.create({
	dialogModalBackgroundStyle: {
		backgroundColor: "#000000",
		opacity: 0.6
	},
	backgroundColorAfterItemClicks: { backgroundColor: "#eff0f1" },
	classTypeView: totalItems => ({
		zIndex: 1,
		width: width / 1.46,
		maxHeight: height / 2,
		borderRadius: height / 192,
		backgroundColor: "#ffffff",
		position: "absolute",
		alignSelf: "center"
	}),
	classTypeButton: {
		height: height / 15.7,
		flexDirection: "row",
		alignItems: "center",
		paddingHorizontal: width / 26.3
	},
	classTypeText: {
		fontSize: width / 25,
		color: "#000"
	},
	displayValueView: {
		borderBottomWidth: 2,
		borderBottomColor: "red"
	}
});
