import React, { Component } from "react";
import { Image } from "react-native";
import FastImage from "react-native-fast-image";
import { connect } from "react-redux";

class GoImage extends Component {
	state = {
		retryCount: 0,
		shouldRetry: false
	};

	static getDerivedStateFromProps(nextProps, prevState) {
		let nextState = {};
		if (nextProps.networkStatus && prevState.shouldRetry) {
			nextState.retryCount = prevState.retryCount + 1;
			nextState.shouldRetry = false;
		}
		return nextState;
	}

	handleRetry = () => {
		// IF there is no internet queue it to should retry
		if (!this.props.networkStatus) {
			this.setState({ shouldRetry: true });
			return;
		}
		if (this.state.retryCount < 3) {
			this.setState((state, props) => {
				return { retryCount: state.retryCount + 1 };
			});
		} else {
			// do nothing
		}
	};

	deriveImageKey = () => {
		if (this.props.source && typeof this.props.source === "object") {
			if (this.props.source.hasOwnProperty("uri")) {
				return this.props.source.uri;
			}
		}
		// generate random key
		return (
			Math.random()
				.toString(36)
				.substring(2, 15) +
			Math.random()
				.toString(36)
				.substring(2, 15)
		);
	};

	render() {
		console.log(this.props);
		if (this.props.useFastImage) {
			return (
				<FastImage
					key={`${this.props.uri}-${this.state.retryCount}`}
					{...this.props}
					onError={this.handleRetry}
				/>
			);
		}
		return (
			<Image
				key={`${this.props.uri}-${this.state.retryCount}`}
				{...this.props}
				onError={this.handleRetry}
			/>
		);
	}
}

const mapStateToProps = state => ({
	networkStatus: state.netInfo.status
});

GoImage.defaultProps = {
	useFastImage: false
};

export default connect(mapStateToProps)(GoImage);
