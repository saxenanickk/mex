import React from "react";
import { View, TouchableOpacity, Dimensions } from "react-native";
import { GoappTextBold, GoappTextMedium } from "./GoappText";

const { width, height } = Dimensions.get("window");
export const NoSiteView = props => {
	return (
		<View style={props.style}>
			<GoappTextBold>{props.message}</GoappTextBold>
			{props.isRefresh ? (
				<TouchableOpacity
					onPress={() => props.onRefresh()}
					style={{
						marginTop: height / 60,
						borderRadius: width / 5,
						width: width / 3,
						flexDirection: "row",
						backgroundColor: "#3398f0",
						justifyContent: "center",
						alignItems: "center",
						paddingVertical: height / 90,
						paddingHorizontal: width / 90
					}}>
					<GoappTextMedium
						style={{ fontSize: height / 45, color: "#fff" }}
						numberOfLines={1}>
						{"RETRY"}
					</GoappTextMedium>
				</TouchableOpacity>
			) : null}
		</View>
	);
};
