import React, { Component } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import Icon from "./Icon";

const { width, height } = Dimensions.get("window");

export default class Hamburger extends Component {
	render() {
		return (
			<View style={styles.hamburgerContainer}>
				<Icon
					iconName={"navicon"}
					iconType={"evilicon"}
					iconColor={"#000000"}
					iconSize={width / 10}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	hamburgerContainer: {
		width: width / 10,
		height: height / 24,
		alignItems: "center",
		justifyContent: "space-around"
	}
});
