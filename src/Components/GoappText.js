import React, { FunctionComponent } from "react";
import {
	Text,
	Platform,
	TextInput,
	TextProps,
	StyleProp,
	TextStyle
} from "react-native";
import {
	font_one,
	font_two,
	font_three,
	font_four,
	font_ten
} from "../Assets/styles";

/**
 * @typedef {Object}GoappTextProps
 * @property {StyleProp<TextStyle>} [style]
 * @property {Number} [size]
 * @property {String} [color]
 */

/**
 * @type {FunctionComponent<GoappTextProps & TextProps>}
 */
export const GoappTextLight = ({
	size,
	color,
	style,
	children,
	...restProps
}) => (
	<Text
		{...restProps}
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_one,
				fontWeight: Platform.OS === "ios" ? "300" : undefined,
				fontSize: size,
				color: color
			},
			style
		]}>
		{children}
	</Text>
);

/**
 * @type {FunctionComponent<TextProps & {size?: Number, color?: String, style?: StyleProp<TextStyle>}>}
 */
export const GoappTextRegular = props => (
	<Text
		{...props}
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_two,
				fontWeight: Platform.OS === "ios" ? "400" : undefined,
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}>
		{props.children}
	</Text>
);

/**
 * @type {FunctionComponent<GoappTextProps & TextProps>}
 */
export const GoappTextMedium = props => (
	<Text
		{...props}
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_three,
				fontWeight: Platform.OS === "ios" ? "500" : undefined,
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}>
		{props.children}
	</Text>
);

/**
 * @type {FunctionComponent<GoappTextProps & TextProps>}
 */
export const GoappTextBold = props => (
	<Text
		{...props}
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_four,
				fontWeight: Platform.OS === "ios" ? "700" : undefined,
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

/**
 * @type {FunctionComponent<OffersTextProps & TextProps>}
 */
export const GoappTextThin = props => (
	<Text
		{...props}
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_ten,
				fontWeight: Platform.OS === "ios" ? "700" : undefined,
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

export const GoappTextInputRegular = ({
	size,
	color = "#000",
	style,
	placeholder,
	value,
	onChangeText,
	multiline,
	numberOfLines,
	ellipsizeMode,
	editable,
	inputRef,
	...restProps
}) => (
	<TextInput
		{...restProps}
		ref={r => {
			inputRef && inputRef(r);
		}}
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_two,
				fontWeight: Platform.OS === "ios" ? "400" : undefined,
				fontSize: size,
				color: color,
				margin: 0,
				padding: 0
			},
			style
		]}
		underlineColorAndroid={"transparent"}
		placeholder={placeholder}
		value={value}
		onChangeText={onChangeText}
		multiline={multiline}
		numberOfLines={numberOfLines}
		ellipsizeMode={ellipsizeMode}
		editable={editable}
	/>
);

export const GoappTextInputBold = ({
	size,
	color = "#000",
	style,
	placeholder,
	value,
	onChangeText,
	multiline,
	numberOfLines,
	ellipsizeMode,
	editable,
	inputRef,
	...restProps
}) => (
	<TextInput
		{...restProps}
		ref={r => {
			inputRef && inputRef(r);
		}}
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : font_four,
				fontWeight: Platform.OS === "ios" ? "700" : undefined,
				fontSize: size,
				color: color,
				margin: 0,
				padding: 0
			},
			style
		]}
		underlineColorAndroid={"transparent"}
		placeholder={placeholder}
		value={value}
		onChangeText={onChangeText}
		multiline={multiline}
		numberOfLines={numberOfLines}
		ellipsizeMode={ellipsizeMode}
		editable={editable}
	/>
);
