import React from "react";
import Svg, { Path } from "react-native-svg";

const CommentSvg = props => (
	<Svg width="17px" height="17px" viewBox="0 0 17 17" {...props}>
		<Path
			d="M1 16l1.29-3.87a7.37 7.37 0 112.91 2.765L1 16z"
			stroke="#2C98F0"
			fill="none"
		/>
	</Svg>
);

export default CommentSvg;
