import React from "react";

import Svg, { Defs, Circle, G, Mask, Use, Path } from "react-native-svg";

const NewMexIcon = props => (
	<Svg width="28px" height="28px" viewBox="0 0 22 22" {...props}>
		<Defs>
			<Circle id="a" cx={11} cy={11} r={11} />
		</Defs>
		<G
			transform="translate(-17 -338) translate(17 338)"
			stroke="none"
			strokeWidth={1}
			fill="none"
			fillRule="evenodd">
			<Mask id="b" fill="#fff">
				<Use xlinkHref="#a" />
			</Mask>
			<Use fill="#E4EAF4" xlinkHref="#a" />
			<G mask="url(#b)" fill="#1D4486" fillRule="nonzero">
				<G transform="translate(5.48 5.937)">
					<Path d="M9.60000029 9.514286L7.40556361 9.514286 7.40556361 3.98244287 4.92328867 8.01919326 4.67638153 8.01919326 2.19542695 3.98244287 2.19542695 9.514286 1.77635684e-15 9.514286 1.77635684e-15 2.92293997e-13 2.19542695 2.92293997e-13 4.7998351 4.22715389 7.40556361 2.90434343e-13 9.60000029 2.90434343e-13z" />
					<Path d="M10.457 8.229a1.286 1.286 0 111.285 1.285 1.294 1.294 0 01-1.285-1.285z" />
				</G>
			</G>
		</G>
	</Svg>
);

export default NewMexIcon;
