import React from "react";
import Svg, { Defs, Circle, G, Mask, Use, Path } from "react-native-svg";

const SupportIcon = props => (
	<Svg width="26px" height="26px" viewBox="0 0 22 22" {...props}>
		<Defs>
			<Circle id="a" cx={11} cy={11} r={11} />
		</Defs>
		<G
			transform="translate(-17 -523) translate(17 523)"
			stroke="none"
			strokeWidth={1}
			fill="none"
			fillRule="evenodd">
			<Mask id="b" fill="#fff">
				<Use xlinkHref="#a" />
			</Mask>
			<Use fill="#E4EAF4" xlinkHref="#a" />
			<G mask="url(#b)" fill="#1D4486" fillRule="nonzero">
				<Path
					d="M13.371 6.746h-.04v-.99a5.756 5.756 0 10-11.513 0v.99h-.242c-.889 0-1.575.788-1.575 1.697v2.524a1.596 1.596 0 001.575 1.636h1.495a.384.384 0 00.363-.424V7.231c0-.243-.141-.485-.363-.485h-.445v-.99a4.948 4.948 0 019.897 0v.99h-.444c-.222 0-.364.242-.364.485v4.948a.384.384 0 00.364.424h.464l-.04.06a3.191 3.191 0 01-2.565 1.273 2 2 0 00-3.959.384 2.02 2.02 0 002.02 2 2.08 2.08 0 001.454-.626c.259-.265.428-.604.485-.97a4 4 0 003.211-1.596l.384-.565c.868-.06 1.414-.606 1.414-1.394V8.645c0-.869-.646-1.899-1.576-1.899zm-10.745 5.05h-1.05a.788.788 0 01-.768-.829V8.443c0-.465.324-.89.768-.89h1.05v4.242zm6.241 3.332a1.192 1.192 0 01-.868.384 1.232 1.232 0 01-1.212-1.192 1.192 1.192 0 112.383 0c.017.3-.093.593-.303.808zm5.272-3.959c0 .546-.525.626-.768.626h-.848V7.554h.848c.445 0 .768.626.768 1.09v2.525z"
					transform="translate(4 3)"
				/>
			</G>
		</G>
	</Svg>
);

export default SupportIcon;
