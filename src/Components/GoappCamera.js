import React, { Component } from "react";
import {
	View,
	Text,
	TouchableOpacity,
	Dimensions,
	StyleSheet,
	Modal,
	Alert,
	Platform,
	PermissionsAndroid
} from "react-native";
import { check, PERMISSIONS, RESULTS, request } from "react-native-permissions";
import Icon from "./Icon";
import { CameraKitCamera } from "react-native-camera-kit";
import ImagePicker from "react-native-image-picker";
import { font_three, font_two } from "../Assets/styles";
import { WHITE, DARK_GREY } from "../Constants/color";
import PropTypes from "prop-types";

const { width, height } = Dimensions.get("window");

class GoappCamera extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isModalVisible: false,
			isCameraOpen: false,
			isCameraBack: true
		};
		this.isCameraAuthorized = 0;
	}

	openImagePicker = () => {
		if (Platform.OS === "android") {
			this.setState({ isModalVisible: true });
		} else {
			check(PERMISSIONS.IOS.PHOTO_LIBRARY)
				.then(result => {
					switch (result) {
						case RESULTS.UNAVAILABLE:
							console.log(
								"This feature is not available (on this device / in this context)"
							);
							break;
						case RESULTS.DENIED:
							request(PERMISSIONS.IOS.PHOTO_LIBRARY).then(res => {
								console.log(res);
								this.openImagePickerForIos();
							});
							break;
						case RESULTS.GRANTED:
							this.openImagePickerForIos();
							break;
						case RESULTS.BLOCKED:
							console.log(
								"The permission is denied and not requestable anymore"
							);
							break;
					}
				})
				.catch(error => {
					console.log(error);
				});
		}
	};

	openImagePickerForIos = () => {
		const { options } = this.props;
		this.closeCamera();
		ImagePicker.showImagePicker(options, response => {
			if (response.didCancel) {
				return;
			} else if (response.error) {
				console.log("ImagePicker Error: ", response.error);
			} else if (response.customButton) {
				return;
			} else {
				this.props.onImageSelect(response);
			}
		});
	};

	openImageLibrary = () => {
		const { options } = this.props;
		this.closeCamera();
		ImagePicker.launchImageLibrary(options, response => {
			if (response.didCancel) {
				return;
			} else if (response.error) {
				console.log("ImagePicker Error: ", response.error);
			} else if (response.customButton) {
				return;
			} else {
				this.props.onImageSelect(response);
			}
		});
	};

	handleCameraPermission = async () => {
		if (Platform.OS === "android") {
			this.openImagePicker();
		} else {
			try {
				const isCameraAuthorized = await CameraKitCamera.checkDeviceCameraAuthorizationStatus();
				this.isCameraAuthorized = isCameraAuthorized;
				if (isCameraAuthorized > 0) {
					this.openImagePicker();
				} else {
					const status = await CameraKitCamera.requestDeviceCameraAuthorization();
					console.log(status);
					if (status) {
						this.openImagePicker();
					} else {
						Alert.alert(
							"Error",
							"Unable to open camera,try to give permissions from settings"
						);
					}
				}
			} catch (error) {
				Alert.alert(
					"Error",
					"Unable to open camera,try to give permissions from settings"
				);
			}
		}
	};

	/**
	 * Request permission if not granted or open camera
	 * if permission granted
	 */
	handleCameraPermissionAndroid = async () => {
		try {
			const granted = await PermissionsAndroid.check(
				PermissionsAndroid.PERMISSIONS.CAMERA
			);
			if (granted) {
				// open camera
				this.setState({ isCameraOpen: true });
			} else {
				// request camera permission
				const requestStatus = await PermissionsAndroid.request(
					PermissionsAndroid.PERMISSIONS.CAMERA
				);
				if (requestStatus === PermissionsAndroid.RESULTS.GRANTED) {
					// open camera
					this.setState({ isCameraOpen: true });
				} else {
					Alert.alert(
						"Error",
						"Unable to open camera,try to give permissions from settings"
					);
				}
			}
		} catch (e) {
			Alert.alert(
				"Error",
				"Unable to open camera,try to give permissions from settings"
			);
		}
	};

	handleGalleryPermissionAndroid = async () => {
		try {
			const readGranted = await PermissionsAndroid.check(
				PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
			);
			const writeGranted = await PermissionsAndroid.check(
				PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
			);
			const granted = readGranted && writeGranted;
			if (granted) {
				// open image library
				this.openImageLibrary();
			} else {
				// request gallery permisssion
				const requestStatus = await PermissionsAndroid.requestMultiple([
					PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
					PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
				]);
				if (
					requestStatus["android.permission.READ_EXTERNAL_STORAGE"] ===
						"granted" &&
					requestStatus["android.permission.WRITE_EXTERNAL_STORAGE"] ===
						"granted"
				) {
					this.openImageLibrary();
				} else {
					Alert.alert(
						"Error",
						"Unable to open Gallery,try to give permissions from settings"
					);
				}
				console.log(requestStatus);
			}
		} catch (e) {
			Alert.alert(
				"Error",
				"Unable to open Gallery,try to give permissions from settings"
			);
		}
	};

	takePicture = async () => {
		try {
			if (this.camera) {
				const data = await this.camera.capture(false);
				data.fileName = data.name;
				this.setState({ isModalVisible: false, isCameraOpen: false });
				this.props.onImageSelect(data);
			}
		} catch (error) {
			console.log("error in clicking", error);
		}
	};

	closeCamera = () => {
		this.setState({ isModalVisible: false, isCameraOpen: false });
	};

	changeCamera = async () => {
		if (this.camera) {
			this.camera.changeCamera();
		}
	};
	render() {
		return (
			<Modal
				visible={this.state.isModalVisible}
				transparent={true}
				onRequestClose={this.closeCamera}>
				{this.state.isCameraOpen ? (
					<View
						style={{
							flex: 1
						}}>
						<CameraKitCamera
							ref={cam => (this.camera = cam)}
							style={{
								flex: 1,
								backgroundColor: "#000"
							}}
							cameraOptions={{
								flashMode: "auto", // on/off/auto(default)
								focusMode: "on", // off/on(default)
								zoomMode: "on", // off/on(default)
								ratioOverlay: "1:1", // optional, ratio overlay on the camera and crop the image seamlessly
								ratioOverlayColor: DARK_GREY // optional
							}}
						/>
						<View style={styles.mainCamera}>
							<TouchableOpacity
								style={styles.flipButton}
								onPress={() => this.changeCamera()}>
								<Icon
									iconType={"ionicon"}
									iconSize={height / 20}
									iconName={"ios-reverse-camera"}
									iconColor={WHITE}
								/>
							</TouchableOpacity>
							<TouchableOpacity
								onPress={this.takePicture.bind(this)}
								style={styles.capture}>
								<Icon
									iconType={"ionicon"}
									iconSize={height / 20}
									iconName={"ios-camera"}
									iconColor={WHITE}
								/>
							</TouchableOpacity>
							<TouchableOpacity
								onPress={this.closeCamera}
								style={styles.closeCameraButton}>
								<Icon
									iconType={"ionicon"}
									iconSize={height / 18}
									iconName={"ios-close"}
									iconColor={WHITE}
								/>
							</TouchableOpacity>
						</View>
					</View>
				) : (
					<View style={styles.cameraOptionContainer}>
						<View style={styles.cameraOptionSubView}>
							<Text style={styles.selectImageText}>{"Select Image"}</Text>
							<TouchableOpacity
								style={styles.actionButton}
								onPress={this.handleCameraPermissionAndroid}>
								<Text style={styles.actionText}>{"Take Photo"}</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={styles.actionButton}
								onPress={this.handleGalleryPermissionAndroid}>
								<Text style={styles.actionText}>{"Choose From Library"}</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={styles.cancelButton}
								onPress={this.closeCamera}>
								<Text style={styles.cancelText}>{"CANCEL"}</Text>
							</TouchableOpacity>
						</View>
					</View>
				)}
			</Modal>
		);
	}
}

const styles = StyleSheet.create({
	mainCamera: {
		position: "absolute",
		bottom: 0,
		width: width,
		backgroundColor: "#000",
		flexDirection: "row",
		paddingHorizontal: width / 20,
		justifyContent: "space-between"
	},
	cameraOptionContainer: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "rgba(0,0,0,0.5)"
	},
	cameraOptionSubView: {
		width: width / 1.2,
		backgroundColor: "#fff",
		padding: 20
	},
	selectImageText: {
		fontFamily: font_three,
		fontSize: height / 45,
		color: "#000"
	},
	actionButton: {
		marginTop: height / 35,
		width: width / 1.4
	},
	cancelButton: {
		marginTop: height / 35,
		width: width / 1.4,
		flexDirection: "row",
		justifyContent: "flex-end"
	},
	cancelText: {
		fontFamily: font_two,
		fontSize: height / 50,
		color: "#000"
	},
	actionText: {
		fontFamily: font_two,
		fontSize: height / 45,
		color: "rgba(0,0,0,0.7)"
	},
	capture: {
		width: width / 5,
		paddingVertical: height / 100,
		position: "absolute",
		left: width / 2 - width / 10,
		alignItems: "center"
	},
	flipButton: {
		paddingVertical: height / 100,
		width: width / 5
	},
	closeCameraButton: {
		width: width / 5,
		alignItems: "flex-end"
	}
});

GoappCamera.propTypes = {
	//action performed after selecting or capturing the image
	onImageSelect: PropTypes.func.isRequired,
	//set options for the image picker library,match with react-native-image-picker
	options: PropTypes.object,
	//set quality for the image captured
	quality: PropTypes.number
};
GoappCamera.defaultProps = {
	options: {},
	quality: 0.6,
	onImageSelect: response =>
		console.log("response on image select is", response)
};
export default GoappCamera;
