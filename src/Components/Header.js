import React from "react";
import {
	View,
	StyleSheet,
	Dimensions,
	TextStyle,
	ViewStyle
} from "react-native";
import { GoappTextRegular } from "./GoappText";

const { width, height } = Dimensions.get("window");

/**
 * @param {Object} props
 * @param {String} props.title
 * @param {TextStyle} props.titleStyle
 * @param {ViewStyle} props.headerStyle
 */
const Header = props => {
	return (
		<View style={[styles.headerStyle, props.headerStyle]}>
			<GoappTextRegular style={[styles.titleStyle, props.titleStyle]}>
				{props.title}
			</GoappTextRegular>
		</View>
	);
};

const styles = StyleSheet.create({
	headerStyle: {
		height: height / 10,
		backgroundColor: "#000000"
	},
	titleStyle: {
		color: "#ffffff",
		fontSize: width / 20,
		marginLeft: width / 20
	}
});

export default Header;
