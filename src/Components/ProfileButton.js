import React from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import Icon from "./Icon";
import { GoappTextRegular, GoappTextMedium } from "./GoappText";
import PropTypes from "prop-types";
const { width, height } = Dimensions.get("window");

/**
 *component creates button for profile
 */

const ProfileButton = props => (
	<TouchableOpacity
		onPress={() => props.onPress()}
		style={{
			width: width - width / 12,
			paddingHorizontal: width / 45,
			height: height / 15,
			flexDirection: "row",
			justifyContent: "space-between",
			alignItems: "center"
		}}>
		<View
			style={{
				width: width / 1.4,
				height: height / 15,
				flexDirection: "row",
				alignItems: "center"
			}}>
			<View
				style={{
					width: width / 10,
					height: height / 15,
					justifyContent: "center",
					alignItems: "flex-start"
				}}>
				<Icon
					iconType={props.iconType}
					iconName={props.iconName}
					iconColor={"#000"}
					iconSize={props.iconSize}
				/>
			</View>
			<GoappTextRegular
				numberOfLines={1}
				style={{
					marginRight: width / 50
				}}>
				{props.menuText}
			</GoappTextRegular>
			{props.isVerified !== null ? (
				<Icon
					iconType={props.isVerified ? "feather" : "ionicon"}
					iconName={
						props.isVerified ? "check-circle" : "ios-information-circle-outline"
					}
					iconSize={height / 35}
					iconColor={props.isVerified ? "#58af61" : "#e02020"}
				/>
			) : null}
		</View>

		<Icon
			iconType={"ionicon"}
			iconName={"ios-arrow-round-forward"}
			iconSize={height / 25}
			iconColor={"rgba(0,0,0,0.6)"}
		/>
	</TouchableOpacity>
);

ProfileButton.propTypes = {
	/**
	 * function performed when clicked on button
	 */
	onPress: PropTypes.func,
	/**
	 * name of icon to be displayed in left side
	 */
	iconName: PropTypes.string.isRequired,
	/**
	 * text to displayed on the button
	 */
	menuText: PropTypes.string.isRequired,
	/**
	 * size of icon
	 */
	iconSize: PropTypes.number,
	/**
	 * type if icon
	 */
	iconType: PropTypes.string,
	/**
	 * is verified or not
	 */
	isVerified: PropTypes.bool
};

ProfileButton.defaultProps = {
	onPress: () => console.log("action on click"),
	iconSize: height / 30,
	iconType: "icomoon",
	isVerified: null
};
export default ProfileButton;
