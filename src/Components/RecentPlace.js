import React from "react";
import { View, Dimensions, StyleSheet, TouchableOpacity } from "react-native";
import Icon from "./Icon";
import { GoappTextRegular } from "./GoappText";

const { width, height } = Dimensions.get("window");

const RecentPlace = props => {
	return (
		<TouchableOpacity
			style={[styles.container, props.recentPlaceStyle]}
			onPress={props.onPress}>
			<View style={[styles.placeStyle, props.placeStyle]}>
				<Icon
					iconName={props.iconName}
					iconSize={props.iconSize}
					iconColor={props.iconColor}
					iconType={props.iconType}
				/>
			</View>
			<GoappTextRegular style={[styles.placeNameStyle, props.placeNameStyle]}>
				{props.placeName || "Unknown"}
			</GoappTextRegular>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	container: {
		width: width / 4,
		height: width / 4,
		borderRadius: width / 8,
		justifyContent: "center",
		alignItems: "center"
	},
	placeStyle: {
		elevation: 5,
		width: width / 6,
		height: width / 6,
		borderRadius: width / 12,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#ffffff"
	},
	placeNameStyle: {
		fontWeight: "bold",
		color: "#000000",
		fontSize: 12
	}
});

export default RecentPlace;
