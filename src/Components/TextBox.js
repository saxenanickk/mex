import React from "react";
import { TextInput, Dimensions, StyleSheet } from "react-native";
import { fontFamily } from "../Assets/styles";

const { width, height } = Dimensions.get("window");

export default class TextBox extends React.Component {
	static defaultProps = {
		onFocus: () => {}
	};
	render() {
		return (
			<TextInput
				style={[styles.container, this.props.textboxStyle]}
				onChangeText={this.props.onChangeText}
				numberOfLines={1}
				ellipsizeMode={"tail"}
				value={this.props.value}
				autoFocus={this.props.autoFocus}
				underlineColorAndroid={"transparent"}
				placeholder={this.props.placeholder}
				onFocus={this.props.onFocus}
			/>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: "#dfdfdf",
		elevation: 0,
		width: width / 1.06,
		height: height / 15,
		fontFamily: fontFamily,
		fontSize: width / 25,
		padding: width / 35,
		textAlignVertical: "top"
	}
});
