import React from "react";
import { View, Text, TouchableOpacity, Dimensions } from "react-native";
import {
	fontFamily_monserrat_semi_bold,
	fontFamily_monserrat_medium
} from "../Assets/styles";
import I18n from "../Assets/strings/i18n";

const { width, height } = Dimensions.get("window");

const HelpButton = props => {
	return (
		<TouchableOpacity
			activeOpacity={1}
			style={{
				backgroundColor: "transparent",
				alignItems: "center",
				justifyContent: "center",
				paddingHorizontal: width / 35,
				paddingVertical: height / 100,
				flexDirection: "row",
				marginRight: width / 250
			}}
			onPress={evt => {
				props.openHelp(evt.nativeEvent.pageX, evt.nativeEvent.pageY);
			}}>
			<View
				style={{
					position: "absolute",
					backgroundColor: "#ffffff",
					width: width / 5.8,
					borderRadius: width / 20,
					height: height / 35,
					elevation: 5,
					justifyContent: "space-around",
					paddingHorizontal: width / 35,
					paddingVertical: height / 60
				}}
			/>
			<Text
				style={{
					elevation: 5,
					fontFamily: fontFamily_monserrat_semi_bold,
					fontSize: width / 30,
					color: "rgb(68,68,68)"
				}}
				allowFontScaling={false}>
				{`${I18n.t("help")} `}
			</Text>
			<View
				style={{
					elevation: 5,
					borderRadius: 20,
					backgroundColor: "rgb(68,68,68)",
					width: width / 24,
					height: width / 24
				}}>
				<Text
					style={{
						color: "#fff",
						textAlign: "center",
						textAlignVertical: "center",
						fontSize: width / 35,
						opacity: 0.6,
						fontFamily: fontFamily_monserrat_medium
					}}>
					{"?"}
				</Text>
			</View>
		</TouchableOpacity>
	);
};

export default HelpButton;
