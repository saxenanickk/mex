import React from "react";
import {
	Text,
	View,
	Dimensions,
	Image,
	TouchableOpacity,
	StyleSheet
} from "react-native";
import { fontFamily_monserrat_semi_bold } from "../Assets/styles";
import { GoappTextRegular } from "./GoappText";

const { width, height } = Dimensions.get("window");

const AvatarPanel = props => {
	return (
		<View style={[styles.container, props.avatarStyle]}>
			<TouchableOpacity onPress={props.onClickAvatar}>
				{typeof props.imageName === "number" ? (
					<Image
						style={[styles.image, props.imageStyle]}
						source={props.imageName}
					/>
				) : (
					<Image
						style={[styles.image, props.imageStyle]}
						source={{ uri: props.imageName }}
					/>
				)}
			</TouchableOpacity>
			<View style={styles.infoContainer}>
				<GoappTextRegular
					style={[styles.username, props.textStyle]}
					ellipsizeMode={"tail"}
					numberOfLines={1}>
					{props.username}
				</GoappTextRegular>
				<GoappTextRegular
					style={[styles.username, props.textStyle]}
					ellipsizeMode={"tail"}
					numberOfLines={1}>
					{props.email}
				</GoappTextRegular>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		height: height / 4,
		padding: width / 40,
		backgroundColor: "#000000",
		flexDirection: "row",
		justifyContent: "flex-start",
		alignItems: "center"
	},
	infoContainer: {
		flex: 1
	},
	username: {
		color: "#ffffff",
		fontSize: width / 23
	},
	image: {
		width: width / 5,
		height: width / 5,
		borderRadius: width / 10
	}
});

export default AvatarPanel;
