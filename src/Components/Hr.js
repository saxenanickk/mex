import React from "react";
import { View } from "react-native";

const Hr = props => {
	return (
		<View
			style={[
				{
					borderBottomColor: "grey",
					borderBottomWidth: 1
				},
				props.style
			]}
		/>
	);
};

export default Hr;
