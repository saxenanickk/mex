import React from "react";
import { View, Dimensions, StyleSheet } from "react-native";
import Icon from "./Icon";
import { GoappTextRegular } from "./GoappText";

const { width, height } = Dimensions.get("window");

const InformationFooter = props => {
	return (
		<View style={[styles.informationFooterStyle, props.informationFooterStyle]}>
			<Icon
				iconType={"ionicon"}
				iconName={"ios-alert"}
				iconSize={width / 23}
				iconColor={"rgba(255,255,255,0.8)"}
			/>
			<GoappTextRegular
				style={[
					styles.informationFooterTextStyle,
					props.informationFooterTextStyle
				]}>
				{props.informationFooterText}
			</GoappTextRegular>
		</View>
	);
};

const styles = StyleSheet.create({
	informationFooterStyle: {
		position: "absolute",
		width: width,
		bottom: 0,
		backgroundColor: "#CCE500",
		alignItems: "center",
		paddingHorizontal: width / 35,
		height: height / 20,
		flexDirection: "row"
	},
	informationFooterTextStyle: {
		marginLeft: width / 30,
		fontSize: width / 27,
		color: "#000000"
	}
});

export default InformationFooter;
