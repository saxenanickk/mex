import React from "react";
import PropTypes from "prop-types";
import {
	Text,
	TextInput,
	View,
	StyleSheet,
	Dimensions,
	Platform
} from "react-native";
import { fontFamily } from "../Assets/styles";
import BaseInput from "./BaseInput";

const { width, height } = Dimensions.get("window");
const PADDING = width / 20;

export default class CustomInput extends BaseInput {
	static propTypes = {
		borderColor: PropTypes.string,
		isRequired: PropTypes.bool,
		/*
		 * this is used to set backgroundColor of label mask.
		 * this should be replaced if we can find a better way to mask label animation.
		 */
		maskColor: PropTypes.string,
		height: PropTypes.number
	};

	static defaultProps = {
		borderColor: "#000",
		height: height / 12,
		isRequired: false,
		selectionColor: "#000",
		returnKeyType: Platform.OS === "ios" ? "done" : "none",
		checks: {
			isRequired: true,
			minSize: 0,
			maxSize: 1000,
			isEmail: false,
			isNumber: false
		},
		keyboardType: "default"
	};

	render() {
		const {
			label,
			style: containerStyle,
			inputStyle,
			labelStyle,
			maskColor,
			borderColor,
			height: inputHeight,
			selectionColor,
			requiredTextStyle,
			keyboardType,
			returnKeyType
		} = this.props;
		const { width, focusedAnim, value, textAnim } = this.state;
		const flatStyles = StyleSheet.flatten(containerStyle) || {};
		const containerWidth = flatStyles.width || width;

		return (
			<View
				style={[
					styles.container,
					containerStyle,
					{
						height: inputHeight,
						width: containerWidth,
						borderWidth: 0
					}
				]}
				onLayout={this._onLayout}>
				<TextInput
					ref="input"
					style={[
						inputStyle,
						{
							width,
							height: height / 18,
							borderBottomWidth: 1,
							borderBottomColor: borderColor,
							padding: 0
						}
					]}
					value={value}
					onBlur={this._onBlur}
					onChange={this._onChange}
					onFocus={this._onFocus}
					underlineColorAndroid={"transparent"}
					selectionColor={selectionColor}
					placeholder={label}
					keyboardType={keyboardType}
					maxLength={
						this.props.checks.maxSize ? this.props.checks.maxSize : null
					}
					returnKeyType={returnKeyType}
				/>
				{/* <TouchableWithoutFeedback onPress={this.focus}>
					<Animated.View
						style={[
							styles.labelContainer,
							{
								opacity: focusedAnim.interpolate({
									inputRange: [0, 0.5, 1],
									outputRange: [1, 0, 1],
								}),
								top: focusedAnim.interpolate({
									inputRange: [0, 1],
									outputRange: [height / 35, height / 600],
								}),
							},
						]}>
						<Animated.Text
							style={[
								styles.label,
								labelStyle,
								{
									fontSize: textAnim.interpolate({
										inputRange: [0, 1],
										outputRange: [height / 40, height / 60],
									}),
								},
							]}>
							{label}
						</Animated.Text>
					</Animated.View>
				</TouchableWithoutFeedback> */}
				<View style={[styles.labelMask, { backgroundColor: maskColor }]} />
				{(this.state.required || this.state.invalid) && (
					<Text style={[styles.requiredText, requiredTextStyle]}>
						{label}
						{" is not valid"}
					</Text>
				)}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		borderWidth: 0,
		borderColor: "#b9c1ca",
		flexDirection: "row",
		justifyContent: "space-between"
	},
	labelContainer: {
		position: "absolute"
	},
	label: {
		fontSize: height / 45,
		color: "#6a7989"
	},
	textInput: {
		position: "absolute",
		bottom: height / 50,
		left: PADDING,
		padding: 0,
		color: "#6a7989",
		fontSize: height / 50,
		fontWeight: "bold",
		borderWidth: 0,
		borderColor: "#000"
	},
	labelMask: {
		height: 24,
		width: PADDING
	},
	border: {
		position: "absolute",
		bottom: height / 50,
		left: 0,
		right: 0,
		width: width
	},
	requiredText: {
		position: "absolute",
		bottom: 0,
		fontFamily: fontFamily
	}
});
