import { StyleSheet, Dimensions, Platform } from "react-native";
import { font_four, font_two } from "../../Assets/styles.android";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
	mandatoryContainer: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		width: width / 1.2,
		paddingVertical: height / 90
	},
	isMandatoryBox: { flexDirection: "row" },
	lableType: {
		fontFamily: Platform.OS === "ios" ? "Avenir" : font_four,
		fontWeight: Platform.OS === "ios" ? "700" : undefined
	},
	mandatoryIcon: { lineHeight: height / 40, color: "#ff0000" },
	container: {
		flexDirection: "row",
		flexWrap: "wrap",
		justifyContent: "flex-start"
	},
	educationConatainer: {
		alignItems: "center",
		borderRadius: width / 10,
		paddingVertical: height / 150,
		flexDirection: "row",
		justifyContent: "space-between"
	},
	itemText: isEditable => ({
		maxWidth: width / 1.5,
		marginRight: isEditable ? width / 40 : null
	}),
	itemTexts: {
		fontFamily: Platform.OS === "ios" ? "Avenir" : font_two,
		fontWeight: Platform.OS === "ios" ? "400" : undefined,
		alignSelf: "center"
	},
	chipColor: isMatching => ({
		color: isMatching ? "#fff" : "#2C98F0"
	}),
	addMore: {
		flexDirection: "row",
		backgroundColor: "#2c98f0",
		borderRadius: 15,
		alignItems: "center",
		paddingVertical: height / 150,
		paddingHorizontal: width / 30,
		margin: width / 50,
		justifyContent: "space-between"
	},
	dataContainer: isMatching => ({
		backgroundColor: isMatching ? "#2C98F0" : "#EAF4FC",
		alignItems: "center",
		borderRadius: width / 10,
		paddingVertical: height / 150,
		paddingHorizontal: width / 30,
		margin: width / 50,
		flexDirection: "row",
		justifyContent: "space-between"
	}),
	itemContainer: { paddingHorizontal: width / 50 },
	educationConatainers: { flexDirection: "row", alignItems: "center" }
});
