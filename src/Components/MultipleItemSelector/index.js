import React from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity, Dimensions } from "react-native";
import Icon from "../Icon";
import { GoappTextRegular, GoappTextBold } from "../GoappText";
import { styles } from "./styles";

const { height, width } = Dimensions.get("window");

/**
 * @typedef {Object} DataItem
 * @property {String} DataItem.value
 * @property {Boolean} DataItem.isMatching
 * @property {Boolean} DataItem.isEditable
 */

/**
 * Remove an item
 * @callback RemoveItem
 * @param {(DataItem|String)} Item An Item to remove
 */

/**
 * AddMore Items tells parent component the type
 * @callback AddMore
 * @param {String} type
 */

/**
 *
 * @param {Object} Props
 * @param {DataItem[]|String[]} Props.data
 * @param {ReactNode} Props.type
 * @param {RemoveItem} Props.removeItem
 * @param {Boolean} Props.isAddMore
 * @param {AddMore} Props.addMore
 * @param {Boolean} Props.isEditable
 * @param {Boolean} Props.isMandatory
 * @param {ViewStyle} props.containerStyle
 */
const MultipleItemSelector = ({
	data,
	type,
	removeItem,
	isAddMore,
	addMore,
	isEditable,
	isMandatory,
	isShowType,
	containerStyle
}) => {
	return (
		<View style={[styles.itemContainer, containerStyle]}>
			{isShowType ? (
				<View style={styles.mandatoryContainer}>
					<View style={styles.isMandatoryBox}>
						<GoappTextBold style={styles.lableType}>{type}</GoappTextBold>
						{isMandatory && (
							<GoappTextRegular style={styles.mandatoryIcon}>
								*
							</GoappTextRegular>
						)}
					</View>
				</View>
			) : null}
			<View style={styles.container}>
				{data.map((item, index) => (
					<TouchableOpacity key={index} activeOpacity={1}>
						<View style={styles.dataContainer(item.isMatching)}>
							<GoappTextRegular
								style={[
									styles.itemText(isEditable),
									styles.chipColor(item.isMatching)
								]}>
								{item.value ? item.value : item}
							</GoappTextRegular>
							{item.isEditable ||
								(isEditable && (
									<TouchableOpacity
										activeOpacity={1}
										onPress={() => removeItem(item)}>
										<Icon
											iconType={"ionicon"}
											iconSize={height / 40}
											iconName={"ios-close-circle"}
											iconColor={"rgba(0,0,0,0.5)"}
										/>
									</TouchableOpacity>
								))}
						</View>
					</TouchableOpacity>
				))}
				{isAddMore && (
					<TouchableOpacity
						onPress={() => addMore(type)}
						style={styles.addMore}>
						<GoappTextRegular color={"#fff"}>Add</GoappTextRegular>
						<Icon
							iconType={"feather"}
							iconSize={height / 40}
							iconName={"plus"}
							iconColor={"#fff"}
						/>
					</TouchableOpacity>
				)}
			</View>
		</View>
	);
};

MultipleItemSelector.propTypes = {
	type: PropTypes.string.isRequired,
	data: PropTypes.array.isRequired,
	isAddMore: PropTypes.bool
};

MultipleItemSelector.defaultProps = {
	data: [],
	type: "None",
	isAddMore: false,
	removeItem: (item = null) => console.log("remove this item"),
	addMore: () => console.log("add more to the list"),
	isMandatory: false,
	isShowType: true
};

export default MultipleItemSelector;
