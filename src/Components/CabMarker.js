import React from "react";
import { View, Text } from "react-native";
import { Marker } from "react-native-maps";

export default class CabMarker extends React.Component {
	static defaultProps = {
		rotation: 0
	};

	render() {
		return (
			<Marker.Animated
				style={{ transform: [{ rotateZ: `${this.props.rotation}deg` }] }}
				coordinate={this.props.coordinates}
				image={this.props.img}
				ref={ref => (this.markerRef = ref)}>
				<View>
					<Text>{""}</Text>
				</View>
			</Marker.Animated>
		);
	}
}
