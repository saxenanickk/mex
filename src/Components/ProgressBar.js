import React from "react";
import { View, Animated, Easing } from "react-native";
class ProgressBar extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			progress: props.value
		};

		this.widthAnimation = new Animated.Value(0);
		this.backgroundAnimation = new Animated.Value(0);
	}

	componentDidMount() {
		if (this.state.progress > 0) {
			this.animateWidth();
		}
	}

	componentWillReceiveProps(props) {
		if (props.value !== this.state.progress) {
			if (props.value >= 0 && props.value <= this.props.maxValue) {
				this.setState({ progress: props.value }, () => {
					if (this.state.progress === this.props.maxValue) {
						const callback = this.props.onComplete;
						if (callback) {
							setTimeout(callback, this.props.barAnimationDuration);
						}
					}
				});
			}
		}
	}

	componentDidUpdate(prevProps) {
		if (this.props.value !== prevProps.value) {
			this.animateWidth();
		}
	}

	animateWidth() {
		const toValue =
			(this.props.width * this.state.progress) / 100 -
			this.props.borderWidth * 2;

		Animated.timing(this.widthAnimation, {
			easing: Easing[this.props.barEasing],
			toValue: toValue > 0 ? toValue : 0,
			duration: 500
		}).start();
	}

	render() {
		return (
			<View
				style={[
					{
						width: this.props.width,
						height: this.props.height,
						borderWidth: this.props.borderWidth,
						borderColor: this.props.borderColor,
						borderRadius: this.props.borderRadius
					}
				]}>
				<Animated.View
					style={[
						{
							height: this.props.height - this.props.borderWidth * 2,
							width: this.widthAnimation,
							backgroundColor: this.props.backgroundColor,
							borderRadius: this.props.borderRadius
						}
					]}
				/>
			</View>
		);
	}
}

ProgressBar.defaultProps = {
	value: 0,
	maxValue: 100,

	barEasing: "linear",
	barAnimationDuration: 500,
	backgroundAnimationDuration: 2500,

	height: 0,

	backgroundColor: "#148cF0",
	backgroundColorOnComplete: null,

	borderWidth: 0,
	borderColor: "#C8CCCE",
	borderRadius: 0,

	onComplete: null
};

export default ProgressBar;
