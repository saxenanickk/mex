import React, { Component, ReactText } from "react";
import {
	Text,
	View,
	Platform,
	Dimensions,
	TouchableOpacity,
	FlatList,
	ColorPropType,
	ViewStyle
} from "react-native";
import Icon from "./Icon";
import DialogModal from "./DialogModal";
import { GoappTextRegular } from "./GoappText";
import { Picker } from "@react-native-community/picker";
const { width, height } = Dimensions.get("window");

/**
 * @callback UpdateSelectedValue
 * @param {{itemValue: Object, itemIndex: number}} params
 *
 */

/**
 * @typedef {Object} Props
 * @property {('dialog' | 'dropdown')} mode
 * @property {ColorPropType} selectedValueColor
 * @property {ViewStyle} style
 * @property {*} selectedValue
 * @property {Array} data
 * @property {UpdateSelectedValue} updateSelectedValue
 * @property {ReactText} pickerKey
 * @property {String} label
 * @property {String} labelPrefix
 * @property {String} labelSuffix
 * @property {ViewStyle} textContainerStyle
 *
 * @extends {Component<Props, {}>}
 */
class GoPicker extends Component {
	static defaultProps = {
		selectedValueColor: "#000",
		dialogStyle: {},
		mode: "dropdown",
		labelPrefix: "",
		labelSuffix: "",
		updateSelectedValue: ({ itemValue, itemIndex }) => null,
		selectedValue: null,
		style: { width: width / 3 },
		enabled: true,
		prompt: "",
		itemStyle: null,
		data: null,
		textStyle: {},
		selectedTextFontSize: width / 22
	};

	state = {
		visible: false
	};

	render() {
		if (Platform.OS === "android") {
			return (
				<Picker
					mode={this.props.mode}
					style={[
						{
							width: width / 3,
							color: this.props.selectedValueColor
						},
						this.props.style
					]}
					selectedValue={this.props.selectedValue}
					onValueChange={(itemValue, itemIndex) => {
						if (itemValue !== "select") {
							this.props.updateSelectedValue({
								itemValue,
								itemIndex: itemIndex - 1
							});
						}
					}}>
					<Picker.Item
						label={"Select"}
						value={"select"}
						itemStyle={this.props.itemStyle}
					/>
					{this.props.data.map((item, index) => (
						<Picker.Item
							key={
								typeof this.props.pickerKey === "undefined"
									? index
									: item[this.props.pickerKey]
							}
							label={
								typeof this.props.label === "undefined"
									? item.toString()
									: this.props.labelPrefix +
									  item[this.props.label] +
									  this.props.labelSuffix
							}
							value={
								typeof this.props.value === "undefined"
									? item
									: item[this.props.value]
							}
							itemStyle={this.props.itemStyle}
						/>
					))}
				</Picker>
			);
		} else {
			if (
				this.props.data !== null &&
				this.props.data !== undefined &&
				this.props.data.length !== 0 &&
				this.props.selectedValue !== null
			) {
				return (
					<React.Fragment>
						<TouchableOpacity
							style={[
								{
									flexDirection: "row",
									padding: 5,
									alignItems: "center",
									justifyContent: "center"
								},
								this.props.textContainerStyle
							]}
							onPress={() => this.setState({ visible: true })}>
							<GoappTextRegular
								style={[
									{
										fontSize: this.props.selectedTextFontSize,
										paddingRight: width / 60,
										color: this.props.selectedValueColor
									},
									this.props.textStyle
								]}>
								{this.props.labelPrefix +
									this.props.selectedValue +
									this.props.labelSuffix}
							</GoappTextRegular>
							<Icon
								iconType={"font_awesome"}
								iconName={this.state.visible ? "caret-up" : "caret-down"}
								iconSize={width / 16}
								iconColor={this.props.selectedValueColor}
							/>
						</TouchableOpacity>
						<DialogModal
							dialogModalBackgroundStyle={{
								backgroundColor: "#000",
								opacity: 0.2
							}}
							visible={this.state.visible}
							onRequestClose={() => this.setState({ visible: false })}>
							<View
								style={[
									{
										borderRadius: height / 200,
										borderColor: "#fff",
										elevation: 5,
										position: "absolute",
										width,
										top: height * 0.7,
										bottom: 0,
										backgroundColor: "#ffffff",
										justifyContent: "space-between",
										shadowOffset: {
											width: 2,
											height: 2
										},
										shadowOpacity: 0.2,
										shadowColor: "#000"
									},
									this.props.dialogStyle
								]}>
								<FlatList
									contentContainerStyle={{
										padding: width / 30
									}}
									data={this.props.data}
									renderItem={({ item, index }) => (
										<TouchableOpacity
											style={{
												alignItems: "center",
												padding: width / 35
											}}
											onPress={() => {
												this.props.updateSelectedValue({
													itemValue:
														this.props.value === undefined
															? item
															: item[this.props.value],
													itemIndex: index
												});
												this.setState({ visible: false });
											}}>
											<GoappTextRegular>
												{typeof this.props.value === "undefined"
													? item
													: this.props.labelPrefix +
													  item[this.props.value] +
													  this.props.labelSuffix}
											</GoappTextRegular>
										</TouchableOpacity>
									)}
									keyExtractor={(item, index) => index.toString()}
									extraData={this.props.booths}
									ItemSeparatorComponent={() => (
										<View
											style={{
												width: width * 0.8,
												borderWidth: 1,
												alignSelf: "center"
											}}
										/>
									)}
								/>
							</View>
						</DialogModal>
					</React.Fragment>
				);
			}
		}
		return <View style={{ height: height / 20 }} />;
	}
}

export default GoPicker;
