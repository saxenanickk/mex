import React, { Component } from "react";

import { View } from "react-native";

import Icon from "../Icon";

import { GoappTextRegular, GoappTextBold } from "../GoappText";
import styles from "./style";

class UserActivities extends Component {
	render() {
		return (
			<View style={styles.userActivitiesParent}>
				<View style={styles.userActivity}>
					<View style={styles.userActivitiesParent}>
						<GoappTextRegular style={styles.userActivitiesPonits}>
							{/* {this.props.userProfile.number_of_follower} */}
							42
						</GoappTextRegular>
						<GoappTextRegular style={styles.userActivitiesText}>
							Followers
						</GoappTextRegular>
					</View>
					<View style={styles.hrView} />
					<View style={styles.userActivitiesParent}>
						<GoappTextRegular style={styles.userActivitiesPonits}>
							{/* {this.props.userProfile.number_of_following} */}
							45
						</GoappTextRegular>
						<GoappTextRegular style={styles.userActivitiesText}>
							Followings
						</GoappTextRegular>
					</View>
					<View style={styles.hrView} />
					<View style={styles.userActivitiesParent}>
						<GoappTextRegular style={styles.userActivitiesPonits}>
							{/* {this.props.userProfile.number_of_post} */}
							55
						</GoappTextRegular>
						<GoappTextRegular style={styles.userActivitiesText}>
							Posts
						</GoappTextRegular>
					</View>
				</View>
				<View style={styles.profileButtonStyle}>
					<GoappTextBold style={styles.profileButtonText}>
						Join MEX. Community Now!
					</GoappTextBold>
				</View>
			</View>
		);
	}
}

export default UserActivities;
