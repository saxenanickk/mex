import { Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
const styles = {
	userActivitiesParent: {
		alignItems: "center"
	},
	userActivity: {
		flexDirection: "row",
		backgroundColor: "#ffffff",
		borderRadius: height / 30,
		marginTop: height / -25,
		height: height / 6,
		width: width / 1.2,
		justifyContent: "space-around",
		padding: height / 35
	},
	userActivitiesPonits: {
		fontSize: height / 42,
		fontWeight: "bold",
		color: "#000"
	},
	userActivitiesText: {
		fontSize: height / 48,
		color: "#000000"
	},
	hrView: {
		borderWidth: 1,
		borderColor: "#f3f3f3",
		height: height / 15
	},
	profileButtonStyle: {
		marginTop: height / -20,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		padding: width / 30,
		backgroundColor: "#2C98F0",
		color: "white",
		borderRadius: height / 10
	},
	profileButtonText: {
		color: "white",
		fontSize: height / 50
	}
};

export default styles;
