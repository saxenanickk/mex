// @ts-check
import React from "react";
import {
	TimePickerAndroid,
	DatePickerIOS,
	Platform,
	View,
	LayoutAnimation,
	TouchableOpacity,
	Dimensions
} from "react-native";
import { CleartripTextRegular } from "../Containers/Cleartrip/Components/CleartripText";

const { width, height } = Dimensions.get("window");

/**
 *
 * @param {object} props
 * @param {Date} props.date
 * @param {() => void} props.onDateChange
 */

export default class TimePicker extends React.Component {
	SELECTED_TIME = null;
	constructor(props) {
		super(props);
		this.state = {
			showTimePickerIos: false
		};
	}

	renderTimePickerIos() {
		return (
			<View
				style={{
					position: "absolute",
					alignSelf: "center",
					backgroundColor: "#ffffff",
					top: this.state.showTimePickerIos ? height / 1.8 : height,
					bottom: this.state.showTimePickerIos ? 0 : -height / 2.2,
					left: 0,
					right: 0,
					shadowOffset: { width: 2, height: 2 },
					shadowColor: "black",
					shadowOpacity: 0.2
				}}>
				<View
					style={{
						width: width,
						height: height / 20,
						backgroundColor: "transparent",
						borderWidth: 0.4,
						borderColor: "grey",
						flexDirection: "row",
						alignItems: "center",
						justifyContent: "space-between"
					}}>
					<TouchableOpacity
						onPress={() => {
							LayoutAnimation.spring();
							this.setState({ showTimePickerIos: false }, () =>
								setTimeout(() => this.props.onTimeChange(null), 100)
							);
						}}
						style={{ flex: 1, paddingHorizontal: width / 25 }}>
						<CleartripTextRegular style={{ color: "#1f3d7a" }}>
							{"Cancel"}
						</CleartripTextRegular>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={() => {
							LayoutAnimation.spring();
							this.setState({ showTimePickerIos: false }, () =>
								setTimeout(() => {
									if (!this.SELECTED_TIME) {
										this.SELECTED_TIME = {
											hour: this.props.date.getHours(),
											minute: this.props.date.getMinutes()
										};
									}
									this.props.onTimeChange(this.SELECTED_TIME);
								}, 100)
							);
						}}
						style={{
							flex: 1,
							paddingHorizontal: width / 25,
							alignItems: "flex-end"
						}}>
						<CleartripTextRegular style={{ color: "#1f3d7a" }}>
							{"Done"}
						</CleartripTextRegular>
					</TouchableOpacity>
				</View>
				<View style={{ flex: 1 }}>
					<DatePickerIOS
						style={{ flex: 1 }}
						mode={"time"}
						date={this.props.date}
						onDateChange={time => {
							let timeObject = new Date(time);
							this.SELECTED_TIME = {
								hour: timeObject.getHours(),
								minute: timeObject.getMinutes()
							};
						}}
					/>
				</View>
			</View>
		);
	}

	async componentDidMount() {
		if (Platform.OS === "android") {
			try {
				const time = await TimePickerAndroid.open({});
				if (time.action !== TimePickerAndroid.dismissedAction) {
					console.log("Android Responce: ", time);
					this.props.onTimeChange({ hour: time.hour, minute: time.minute });
				} else {
					this.props.onTimeChange(null);
				}
			} catch ({ code, message }) {
				console.warn("Cannot open time picker", message);
			}
		} else {
			setTimeout(() => {
				LayoutAnimation.spring();
				this.setState({ showTimePickerIos: true });
			}, 100);
		}
	}

	render() {
		return Platform.OS === "ios" ? this.renderTimePickerIos() : null;
	}
}

TimePicker.defaultProps = {
	date: new Date(),
	onDateChange: () => {}
};
