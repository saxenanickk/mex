import React from "react";
import { View, Image, Dimensions, StyleSheet } from "react-native";

const { width, height } = Dimensions.get("window");

const LocationPointer = props => {
	return (
		<View
			pointerEvents="none"
			style={[styles.pointerStyle, props.pointerStyle]}>
			<Image
				style={[styles.pointerIconStyle, props.pointerIconStyle]}
				pointerEvents="none"
				source={props.pointerIcon}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	pointerStyle: {
		position: "absolute",
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "transparent"
	},
	pointerIconStyle: {
		width: width / 18,
		height: height / 8
	}
});

export default LocationPointer;
