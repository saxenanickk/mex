import React, { Component } from "react";
import {
	View,
	Dimensions,
	Image,
	TouchableOpacity,
	StyleSheet,
	Platform,
	Modal,
	Keyboard
} from "react-native";
import {
	GoappTextBold,
	GoappTextLight,
	GoappTextMedium,
	GoappTextRegular,
	GoappTextInputRegular
} from "./GoappText";
import Icon from "./Icon";
import PropTypes from "prop-types";
import { VERIFIED_CORPORATE } from "../Assets/Img/Image";
const { width, height } = Dimensions.get("window");

class OtpValidation extends Component {
	state = {
		otps: ["", "", "", "", "", ""],
		time: 59
	};
	constructor(props) {
		super(props);
		this.startClock = this.startClock.bind(this);
		this.interval = null;
		this.inputRef = {};
	}

	componentDidMount() {
		this.interval = setInterval(this.startClock, 1000);
		if (this.inputRef && this.inputRef.textInput0) {
			this.inputRef.textInput0.focus();
		}
	}

	componentWillUnmount() {
		if (this.interval) {
			clearInterval(this.interval);
		}
	}

	startClock = () => {
		if (this.state.time > 0) {
			this.setState({ time: this.state.time - 1 });
		} else {
			clearInterval(this.interval);
		}
	};

	changeInput(index, val) {
		const { otps } = this.state;
		if (index === 5) {
			Keyboard.dismiss();
		}
		if (val && index < otps.length - 1) {
			this.inputRef["textInput" + (index + 1)].focus();
		}
		otps[index] = val[val.length - 1];
		this.setState({ otps });
	}

	submitOtp() {
		let otp = this.state.otps.join("");
		this.inputRef.textInput1.focus();
		this.props.onPressConfirm(otp);
	}

	checkOtp() {
		let otp = this.state.otps.join("");
		return otp.trim().length !== 6;
	}

	render() {
		const { otps } = this.state;
		const {
			open,
			handleClose,
			heading,
			message,
			isVerified,
			verificationMessage
		} = this.props;
		return (
			<Modal visible={open} transparent={true} onRequestClose={handleClose}>
				<View style={style.mainContainer}>
					<View style={style.container}>
						<TouchableOpacity style={style.closeButton} onPress={handleClose}>
							<Icon
								iconType={"ionicon"}
								iconName={"ios-close"}
								iconColor={"#2C98F0"}
								iconSize={height / 20}
							/>
						</TouchableOpacity>
						{isVerified ? (
							<View style={style.verifiedSection}>
								<Image
									source={VERIFIED_CORPORATE}
									resizeMode={"contain"}
									style={style.verifiedImage}
								/>
								<GoappTextBold style={style.textBottomMsg}>
									{verificationMessage}
								</GoappTextBold>
							</View>
						) : (
							<View style={style.otpSectionView}>
								<GoappTextBold style={style.textMsg}>{heading}</GoappTextBold>
								<GoappTextLight style={style.textBottomMsg}>
									{message}
								</GoappTextLight>
								<View style={style.textContainer}>
									{otps.map((value, index) => (
										<TouchableOpacity key={index}>
											<View style={style.textInput}>
												<GoappTextInputRegular
													color={"rgba(0,0,0,1)"}
													style={style.textStyle}
													keyboardType="numeric"
													returnKeyType={"done"}
													type="button"
													value={value}
													selectTextOnFocus={true}
													onChangeText={text => this.changeInput(index, text)}
													inputRef={ref =>
														(this.inputRef["textInput" + index] = ref)
													}
													onKeyPress={({ nativeEvent }) => {
														if (nativeEvent.key === "Backspace" && index > 0) {
															this.inputRef["textInput" + (index - 1)].focus();
														}
													}}
												/>
											</View>
										</TouchableOpacity>
									))}
								</View>
								<TouchableOpacity
									style={style.resendButton}
									onPress={() => {
										this.props.onPressResend();
										this.interval = setInterval(this.startClock, 1000);
										this.setState({ time: 59 });
									}}
									disabled={this.state.time > 0}>
									{this.state.time > 0 ? (
										<GoappTextRegular color={"#2C98F0"} size={height / 50}>
											{`00:${
												this.state.time > 10
													? this.state.time
													: "0" + this.state.time
											}`}
										</GoappTextRegular>
									) : null}
									<GoappTextRegular
										style={{ marginLeft: width / 60 }}
										color={this.state.time > 0 ? "#cbcbcb" : "#2C98F0"}
										size={height / 50}>
										{"Resend OTP"}
									</GoappTextRegular>
								</TouchableOpacity>
								<TouchableOpacity
									disabled={this.checkOtp()}
									style={[
										style.confirmButton,
										{
											backgroundColor: this.checkOtp()
												? "rgba(0,0,0,0.5)"
												: "#2c98f0"
										}
									]}
									onPress={() => this.submitOtp()}>
									<GoappTextMedium color={"#fff"} size={height / 50}>
										{"Confirm"}
									</GoappTextMedium>
								</TouchableOpacity>
							</View>
						)}
					</View>
				</View>
			</Modal>
		);
	}
}

OtpValidation.propTypes = {
	/**
	 * variable to open the otp validation screen
	 */
	open: PropTypes.bool.isRequired,
	/**
	 * function called when trying to close the otp validation screen
	 */
	handleClose: PropTypes.func.isRequired,
	/**
	 * variable to show the heading on the otp validation screen
	 */
	heading: PropTypes.string,
	/**
	 * variable to show message on the otp validation screen
	 */
	message: PropTypes.string,
	/**
	 * function called when clicked on resend button
	 */
	onPressResend: PropTypes.func,
	/**
	 * function called when clicked on confirm
	 */
	onPressConfirm: PropTypes.func,
	/**
	 * variable to check whether validation otp status
	 */
	isValidating: PropTypes.bool,
	/**
	 * variable to check whether otp is verified
	 */
	isVerified: PropTypes.bool,
	/**
	 * variable to display when otp has been verified
	 */
	verificationMessage: PropTypes.string
};

OtpValidation.defaultProps = {
	heading: "Enter Code",
	message: "Check For sent OTP",
	onPressConfirm: otp => console.log("eneterd otp is", otp)
};

export default OtpValidation;

const style = StyleSheet.create({
	mainContainer: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "rgba(0,0,0,0.5)"
	},
	container: {
		backgroundColor: "#fff",
		width: width / 1.1,
		paddingVertical: height / 90,
		paddingHorizontal: width / 30,
		borderRadius: width / 30
	},
	verifiedSection: {
		height: height / 3,
		alignItems: "center",
		justifyContent: "space-around"
	},
	verifiedImage: {
		width: width / 2,
		height: height / 4
	},
	resendButton: {
		flexDirection: "row",
		marginTop: height / 60
	},
	confirmButton: {
		width: width / 1.3,
		paddingVertical: height / 90,
		marginBottom: height / 20,
		backgroundColor: "#2C98F0",
		justifyContent: "center",
		alignItems: "center",
		marginTop: height / 50,
		borderRadius: width / 20,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 3, height: 3 },
				shadowColor: "grey",
				shadowOpacity: 0.2
			}
		})
	},
	profileView: {
		width: width / 3,
		height: height / 5,
		// padding: 10,
		alignItems: "center",
		justifyContent: "center"
	},
	profileImage: {
		width: width / 6,
		height: width / 6,
		borderRadius: width
		// padding: 10,
	},
	textMsg: {
		color: "#313131",
		textAlign: "center",
		fontSize: height / 40,
		width: width / 1.3
	},
	textBottomMsg: {
		marginTop: height / 60,
		color: "#8d919b",
		fontSize: height / 50,
		width: width / 1.3,
		textAlign: "center"
	},
	textContainer: {
		paddingHorizontal: width / 4,
		paddingTop: width / 14,
		paddingBottom: width / 22,
		flexDirection: "row"
		// justifyContent: "space-between",
	},
	textInput: {
		width: width / 7.8,
		height: height / 15,
		justifyContent: "center",
		alignItems: "center"
	},
	textStyle: {
		padding: 0,
		height: height / 15,
		width: width / 10,
		borderBottomWidth: 1,
		borderBottomColor: "#cbcbcb",
		textAlign: "center"
	},
	sendOTP: {
		textAlign: "center",
		color: "#2C98F0",
		fontSize: width / 33.5
	},
	buttonContainer: {
		alignItems: "center",
		marginTop: height / 4.2
	},
	buttonStyle: {
		borderRadius: width / 17.25,
		backgroundColor: "#2C98F0",
		width: width / 1.14,
		height: height / 16.44,
		justifyContent: "center"
	},
	buttonText: {
		color: "#FFFFFF",
		textAlign: "center",
		fontSize: width / 25.87
	},
	closeButton: {
		alignSelf: "flex-end",
		width: width / 5,
		alignItems: "flex-end"
	},
	otpSectionView: {
		alignItems: "center"
	}
});
