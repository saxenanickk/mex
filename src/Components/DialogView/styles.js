const styles = {
	container: {
		position: "absolute",
		top: 0,
		bottom: 0,
		right: 0,
		left: 0,
		backgroundColor: "transparent",
		justifyContent: "center",
		alignItems: "center"
	},
	discardBackground: {
		position: "absolute",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		backgroundColor: "#000000",
		opacity: 0.9
	}
};

export default styles;
