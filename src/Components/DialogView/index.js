import React from "react";
import { View, TouchableWithoutFeedback } from "react-native";
import styles from "./styles";

const DialogView = props => {
	return (
		<View style={styles.container}>
			<TouchableWithoutFeedback onPress={props.onRequestClose}>
				<View style={[styles.discardBackground, props.discardBackground]} />
			</TouchableWithoutFeedback>
			{props.children}
		</View>
	);
};

export default DialogView;
