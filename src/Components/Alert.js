import React from "react";

import {
	TouchableOpacity,
	Modal,
	View,
	StyleSheet,
	Dimensions,
	Platform
} from "react-native";
import { GoappTextMedium } from "./GoappText";
import Icon from "./Icon";
const { width, height } = Dimensions.get("window");

export default function Alert({
	visible,
	cancelAction,
	okAction,
	isCrossIconVisible,
	handleClose,
	handleOkClick,
	handleCrossClick,
	renderImage,
	renderTitle,
	renderMessage,
	renderInput
}) {
	return (
		<Modal
			visible={visible}
			onRequestClose={() => handleClose()}
			transparent={true}>
			<View style={styles.container}>
				<View style={styles.modalContainer}>
					<View style={styles.iconContainer}>
						{isCrossIconVisible ? (
							<TouchableOpacity
								style={styles.iconStyle}
								onPress={() => handleClose()}>
								<Icon
									iconType="material_community_icon"
									iconName="close"
									iconSize={height / 35}
									iconColor="#2C98F0"
								/>
							</TouchableOpacity>
						) : null}
					</View>
					{renderImage()}
					{renderTitle()}
					{renderMessage()}
					{renderInput()}
					<View style={styles.buttonContainer(cancelAction)}>
						{cancelAction && (
							<TouchableOpacity
								onPress={handleCrossClick}
								style={[
									styles.cancelActionStyle,
									(cancelAction && cancelAction.buttonStyle) || {}
								]}>
								<GoappTextMedium
									size={height / 60}
									color={"#000000"}
									style={(cancelAction && cancelAction.textStyle) || {}}
									numberOfLines={1}>
									{(cancelAction && cancelAction.text) || "Cancel"}
								</GoappTextMedium>
							</TouchableOpacity>
						)}
						<TouchableOpacity
							onPress={handleOkClick}
							style={[
								styles.okActionStyle(cancelAction),
								(okAction && okAction.buttonStyle) || {}
							]}>
							<GoappTextMedium
								size={height / 60}
								color={"#fff"}
								style={(okAction && okAction.textStyle) || {}}
								numberOfLines={1}>
								{(okAction && okAction.text) || "Okay"}
							</GoappTextMedium>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		</Modal>
	);
}

Alert.defaultProps = {
	renderImage: () => <></>,
	renderMessage: () => <></>,
	renderTitle: () => <></>,
	renderInput: () => <></>,
	cancelAction: {},
	okAction: {},
	visible: false
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "rgba(0,0,0,0.5)",
		borderWidth: 2
	},
	imageStyle: {
		width: width / 2.95,
		height: height / 15.68,
		alignSelf: "center",
		marginVertical: height * 0.01
	},
	iconContainer: {
		position: "absolute",
		right: 0,
		top: 0,
		paddingHorizontal: width / 50,
		alignItems: "flex-end"
	},
	buttonContainer: isCancelAction => ({
		width: width / 1.83,
		marginTop: height / 32,
		marginBottom: height * 0.01,
		flexDirection: "row",
		alignSelf: "center",
		justifyContent: isCancelAction ? "space-between" : "center"
	}),
	cancelActionStyle: {
		backgroundColor: "#2C98F0",
		width: width / 4.1,
		height: height / 23.7,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 20,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.2
			}
		})
	},

	okActionStyle: isCancelAction => ({
		backgroundColor: "#2C98F0",
		width: isCancelAction ? width / 4.1 : width / 1.6,
		height: height / 23.7,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 20,
		...Platform.select({
			android: { elevation: 5 },
			ios: {
				shadowOffset: { width: 1, height: 1 },
				shadowColor: "grey",
				shadowOpacity: 0.2
			}
		})
	}),
	modalContainer: {
		width: width / 1.29,
		paddingVertical: height * 0.01,
		borderRadius: width / 50,
		backgroundColor: "#fff"
	},
	inputContainer: inputError => ({
		borderBottomWidth: 1,
		marginTop: width * 0.07,
		borderColor: inputError ? "red" : "#979797",
		width: "80%",
		alignSelf: "center"
	}),
	titleStyles: {
		width: width / 1.83,
		alignSelf: "center",
		textAlign: "center",
		marginVertical: height * 0.01
	},
	massageStyles: {
		width: width / 1.83,
		alignSelf: "center",
		textAlign: "center",
		color: "#4A4A4A",
		marginVertical: height / 80
	},
	warningText: { alignSelf: "flex-end", width: "70%" },
	iconStyle: { padding: width / 100 }
});
