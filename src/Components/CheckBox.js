import React from "react";
import { View } from "react-native";
import Icon from "./Icon";

const CheckBox = props => {
	return (
		<View
			style={{
				borderWidth: props.width / 10,
				borderColor: props.value === false ? "#C0C4C7" : props.checkColor,
				backgroundColor: props.value === false ? "#fff" : props.checkColor,
				width: props.width,
				height: props.height,
				justifyContent: "center",
				alignItems: "center",
				borderRadius: props.borderRadius
			}}>
			{props.value === true && (
				<Icon
					iconType={"ionicon"}
					iconName={"ios-checkmark"}
					iconSize={2 * props.width}
					iconColor={"#fff"}
				/>
			)}
		</View>
	);
};

export default CheckBox;
