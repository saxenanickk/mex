import React from "react";
import {
	View,
	Dimensions,
	StyleSheet,
	ActivityIndicator,
	Platform,
	ColorPropType,
	ViewStyle
} from "react-native";
import { GoappTextBold } from "./GoappText";

const { width, height } = Dimensions.get("window");

/**
 * @typedef {Object} Props
 * @property {ColorPropType} indicatorColor
 * @property {Boolean} isMessage
 * @property {String} message
 * @property {Number} indicatorSize
 * @property {String} primaryMessage
 * @property {Number} numberOfLines
 * @property {ViewStyle} progressScreen
 */

/**
 * @param {Props} props
 */
const ProgressScreen = props => {
	return (
		<View style={[styles.container, props.progressScreen]}>
			{Platform.OS === "ios" ? (
				props.message ? (
					<View style={[styles.messageBar, props.messageBar]}>
						<ActivityIndicator
							size={props.indicatorSize}
							color={props.indicatorColor}
						/>
						<View style={{ marginLeft: width / 30 }}>
							<GoappTextBold
								numberOfLines={1}
								style={[styles.primaryMessage, props.primaryMessageStyles]}>
								{props.primaryMessage}
							</GoappTextBold>
							<GoappTextBold
								style={[styles.message, props.messageStyle]}
								numberOfLines={props.numberOfLines}>
								{props.message}
							</GoappTextBold>
						</View>
					</View>
				) : (
					<ActivityIndicator color={props.indicatorColor} />
				)
			) : props.isMessage ? (
				<View style={[styles.messageBar, props.messageBar]}>
					<ActivityIndicator
						size={props.indicatorSize}
						color={props.indicatorColor}
					/>
					<View style={{ marginLeft: width / 30 }}>
						<GoappTextBold
							numberOfLines={1}
							style={[styles.primaryMessage, props.primaryMessageStyles]}>
							{props.primaryMessage}
						</GoappTextBold>
						<GoappTextBold
							style={[styles.message, props.messageStyle]}
							numberOfLines={props.numberOfLines}>
							{props.message}
						</GoappTextBold>
					</View>
				</View>
			) : (
				<ActivityIndicator
					size={props.indicatorSize}
					color={props.indicatorColor}
				/>
			)}
		</View>
	);
};

ProgressScreen.defaultProps = {
	indicatorColor: "#3faad3",
	isMessage: false,
	message: "Loading...",
	indicatorSize: width / 20,
	primaryMessage: "Hang On",
	numberOfLines: 2,
	progressScreen: {}
};

const styles = StyleSheet.create({
	container: {
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		position: "absolute",
		backgroundColor: "transparent",
		justifyContent: "center",
		alignItems: "center",
		zIndex: 10,
		opacity: 1
	},
	messageBar: {
		width: width / 2,
		elevation: 5,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: "black",
		shadowOpacity: 0.2,
		paddingHorizontal: width / 30,
		height: height / 10,
		backgroundColor: "#fff",
		justifyContent: "space-around",
		alignItems: "center",
		flexDirection: "row",
		borderRadius: width / 70
	},
	message: {
		color: "rgba(0,0,0,0.6)",
		fontSize: height / 55,
		width: width / 2.7
	},
	primaryMessage: {
		color: "#000",
		fontSize: height / 50
	}
});

export default ProgressScreen;
