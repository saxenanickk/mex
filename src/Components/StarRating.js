import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import FontAwesome from "react-native-vector-icons/FontAwesome";

const createStars = numStars => {
	let stars = [];
	for (let index = 0; index < 5; index++) {
		if (numStars > index) {
			stars.push(<FontAwesome name={"star"} key={index.toString()} />);
		} else {
			stars.push(<FontAwesome name={"star-o"} key={index.toString()} />);
		}
	}
	return stars;
};

export default class StarRating extends Component {
	static propTypes = {
		numStars: PropTypes.number.isRequired
	};
	render() {
		return <Fragment>{createStars(this.props.numStars)}</Fragment>;
	}
}
