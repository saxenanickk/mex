import { ERRORCLOUD } from "../Assets/Img/Image";

const noInternetDialog = context =>
	context.current.openDialog({
		type: "Information",
		title: "No Internet Connection",
		message: "BBInstant and Access can be used without the internet.",
		icon: ERRORCLOUD
	});

export default noInternetDialog;
