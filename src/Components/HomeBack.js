import React from "react";
import { View, TouchableOpacity, Dimensions, StyleSheet } from "react-native";
import Icon from "./Icon";
import PropTypes from "prop-types";
import { GoappTextMedium } from "./GoappText";

const { width, height } = Dimensions.get("window");

const HomeBack = props => (
	<View
		style={[
			style.backButtonSection,
			{ height: props.headerHeight },
			props.style
		]}>
		<TouchableOpacity
			style={style.goBack}
			onPress={() => props.navigation.navigate("Goapp")}>
			<Icon
				iconType={"material"}
				iconSize={height / 30}
				iconName={"arrow-back"}
				iconColor={props.iconColor}
			/>
		</TouchableOpacity>
		<GoappTextMedium style={[props.headerTextStyle, style.headerTextStyle]}>
			{props.headerText}
		</GoappTextMedium>
	</View>
);

const style = StyleSheet.create({
	goBack: {
		paddingVertical: height / 60
	},
	backButtonSection: {
		flexDirection: "row",
		alignItems: "center",
		width: width / 2,
		paddingHorizontal: width / 50
	},
	headerTextStyle: {
		color: "#fff",
		marginLeft: width / 30
	}
});
HomeBack.propTypes = {
	/**
	 * name of text displayed on header
	 */
	headerText: PropTypes.string,
	/**
	 * color of back icon
	 */
	iconColor: PropTypes.string,
	/**
	 *style of headerText
	 */
	headerTextStyle: PropTypes.object
};

HomeBack.defaultProps = {
	iconColor: "#fff"
};

export default HomeBack;
