import React from "react";
import { StyleSheet } from "react-native";
import MapView, { Polyline, PROVIDER_GOOGLE } from "react-native-maps";

export default class Map extends React.Component {
	static defaultProps = {
		showPath: false,
		coords: [],
		coordinatesToFit: [],
		loadingEnabled: false,
		isMapReady: () => {},
		onMapReady: () => console.log("nothing to do")
	};
	constructor(props) {
		super(props);
		this.fitToCoordinate = this.fitToCoordinate.bind(this);
		this.animateRegionChange = this.animateRegionChange.bind(this);
	}

	animateRegionChange(region) {
		if (
			typeof region !== "undefined" &&
			region !== null &&
			region.latitude &&
			region.longitude &&
			region.latitudeDelta &&
			region.longitudeDelta
		) {
			console.log(region);
			this.mapRef.animateToRegion(region, 500);
		}
	}

	fitToCoordinate(coordinatesToFit = []) {
		const coordinatesToFitted =
			coordinatesToFit !== null &&
			this.props.coords !== null &&
			this.props.showPath
				? [...coordinatesToFit, ...this.props.coords]
				: coordinatesToFit !== null
				? [...coordinatesToFit]
				: this.props.showPath && this.props.coords !== null
				? [...this.props.coords]
				: [];

		if (coordinatesToFitted.length > 0) {
			this.mapRef.fitToCoordinates(coordinatesToFitted, {
				edgePadding: {
					top: this.props.edgePaddingTop ? this.props.edgePaddingTop : 120,
					right: this.props.edgePaddingRight
						? this.props.edgePaddingRight
						: 120,
					bottom: this.props.edgePaddingBottom
						? this.props.edgePaddingBottom
						: 120,
					left: this.props.edgePaddingLeft ? this.props.edgePaddingLeft : 120
				},
				animated: true
			});
		}
	}

	render() {
		return (
			<MapView
				provider={PROVIDER_GOOGLE}
				ref={ref => {
					this.mapRef = ref;
				}}
				onLayout={() => {
					if (this.props.onLayout) {
						this.fitToCoordinate();
					}
				}}
				scrollEnabled={this.props.scrollEnabled}
				onMapReady={this.props.onMapReady}
				loadingEnabled={this.props.loadingEnabled}
				customMapStyle={this.props.mapLayer}
				style={[styles.map, this.props.mapStyle]}
				showsCompass={false}
				showsMyLocationButton={false}
				onPanDrag={this.props.onPanDrag}
				rotateEnabled={false}
				showsUserLocation={this.props.showsUserLocation}
				followUserLocation={this.props.followUserLocation}
				initialRegion={this.props.initialRegion}
				onRegionChange={this.props.onRegionChange}
				onRegionChangeComplete={this.props.onRegionChangeComplete}
				edgePadding={this.props.mapPadding}>
				{this.props.children}
				{this.props.showPath ? (
					<Polyline
						coordinates={this.props.coords}
						strokeWidth={4}
						strokeColor={this.props.polylineColor}
					/>
				) : null}
			</MapView>
		);
	}
}

const styles = StyleSheet.create({
	map: {
		borderWidth: 0,
		position: "absolute",
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		zIndex: -10
	}
});
