import React, { Component } from "react";
import { Text, View } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { DeviceId } from "../CustomModules/GoDeviceInfo";

export default class ErrorBoundary extends Component {
	constructor(props) {
		super(props);
		this.state = {
			hasError: false,
			deviceId: null
		};
	}

	componentDidMount = () => {
		DeviceId()
			.then(id => {
				this.setState({ deviceId: id });
			})
			.catch(error => {
				this.setState({ deviceId: "Unable to Set device Id" });
			});
	};

	componentDidCatch(error, info) {
		this.setState({ hasError: true });
		const errorObject = {
			error,
			info,
			deviceId: this.state.deviceId
		};
	}

	render() {
		if (this.state.hasError) {
			return (
				<View
					style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
					<Icon name="mood-bad" backgroundColor="#f00" size={24} />
					{this.props.shortText && <Text>Oops!</Text>}
					{this.props.longText && <Text>Oops! Something went wrong</Text>}
				</View>
			);
		}
		return this.props.children;
	}
}
