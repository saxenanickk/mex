import React from "react";
import {
	View,
	Modal,
	Dimensions,
	TouchableWithoutFeedback,
	ViewStyle
} from "react-native";

/**
 * Use children's root style = {{ position: "absolute" }}
 */

/**
 * DialogModal
 * @param {object} props
 * @param {boolean} props.visible
 * @param {ViewStyle} props.dialogModalBackgroundStyle
 */
const DialogModal = props => {
	return (
		<Modal
			transparent={props.transparent}
			visible={props.visible}
			animationType={"fade"}
			hardwareAccelerated={true}
			onRequestClose={props.onRequestClose}>
			<TouchableWithoutFeedback onPress={props.onRequestClose}>
				<View
					style={[
						{
							flex: 1,
							backgroundColor: "#000000",
							opacity: 0.8
						},
						props.dialogModalBackgroundStyle
					]}
				/>
			</TouchableWithoutFeedback>
			{props.children}
		</Modal>
	);
};

DialogModal.defaultProps = {
	visible: false,
	onRequestClose: () => null,
	dialogModalBackgroundStyle: {},
	transparent: true
};

export default DialogModal;
