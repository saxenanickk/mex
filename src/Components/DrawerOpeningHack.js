import React from "react";
import { View, Dimensions } from "react-native";

const DrawerOpeningHack = () => (
	<View
		style={{
			left: 0,
			height: Dimensions.get("window").height,
			width: 10,
			backgroundColor: "transparent",
			position: "absolute"
		}}
	/>
);

export default DrawerOpeningHack;
