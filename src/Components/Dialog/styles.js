import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: {
		position: "absolute",
		alignSelf: "center",
		width: width / 1.3,
		backgroundColor: "#ffffff",
		borderRadius: 15,
		padding: width / 25,
		justifyContent: "center",
		alignItems: "center"
	},
	cross: { position: "absolute", right: 0, top: 0, padding: width / 40 },
	icon: {
		width: width / 2.95,
		height: height / 15.68,
		alignSelf: "center",
		marginVertical: height * 0.01
	},
	title: {
		width: "80%",
		alignSelf: "center",
		textAlign: "center",
		marginVertical: height * 0.01
	},
	message: {
		width: "80%",
		alignSelf: "center",
		textAlign: "center",
		color: "#4A4A4A",
		marginVertical: height / 80
	},
	actions: {
		left: 0,
		right: 0,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		marginTop: height / 32
	},
	error: { alignSelf: "flex-end", width: "70%" },
	inputContainer: inputError => ({
		borderBottomWidth: 1,
		marginTop: width * 0.07,
		borderColor: inputError ? "red" : "#979797",
		width: "90%",
		alignSelf: "center"
	})
});

export default styles;
