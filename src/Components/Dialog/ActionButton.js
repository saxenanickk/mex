import React from "react";
import { TouchableOpacity, StyleSheet, Dimensions } from "react-native";
import { GoappTextMedium } from "../GoappText";

const { width, height } = Dimensions.get("window");

const ActionButton = ({ onPress, title, color, titleColor, style }) => (
	<TouchableOpacity onPress={onPress} style={[styles.container(color), style]}>
		<GoappTextMedium
			size={height / 60}
			color={titleColor || "#FFFFFF"}
			numberOfLines={1}>
			{title}
		</GoappTextMedium>
	</TouchableOpacity>
);

const styles = StyleSheet.create({
	container: backgroundColor => ({
		flex: 1,
		backgroundColor: backgroundColor,
		borderWidth: 0.5,
		borderColor: "#AFAFAF",
		justifyContent: "center",
		alignItems: "center",
		borderRadius: width / 20,
		padding: width / 50,
		marginHorizontal: width / 50
	})
});

export default ActionButton;
