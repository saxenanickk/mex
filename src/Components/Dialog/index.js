import React from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import DialogModal from "../DialogModal";
import {
	GoappTextBold,
	GoappTextMedium,
	GoappTextInputRegular,
	GoappTextLight
} from "../GoappText";
import FastImage from "react-native-fast-image";
import ActionButton from "./ActionButton";
import Icon from "../Icon";
import styles from "./styles";

const { height } = Dimensions.get("window");

class Dialog extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isVisible: false,
			requiredInput: ""
		};
		this.containerRef = React.createRef();
	}

	static getDerivedStateFromProps(nextProps, prevState) {
		return null;
	}

	openDialog = ({
		type = "Information",
		title = null,
		message = null,
		icon = null,
		error = null,
		leftTitle = "Cancel",
		leftPress = null,
		leftStyle = {},
		rightTitle = "Okay",
		rightPress = () => {},
		rightStyle = {}
	}) => {
		switch (type) {
			case "DismissableAction": {
				this.crossPress = () => {
					this.closeDialog();
				};
				this.icon = icon;
				break;
			}
			case "Action": {
				this.icon = icon;
				break;
			}
			case "Confirmation": {
				this.icon = icon;
				this.leftTitle = leftTitle;
				this.leftPress = () => {
					leftPress && leftPress();
					this.closeDialog();
				};
				this.leftStyle = leftStyle;
				break;
			}
			case "RequiredInput": {
				this.crossPress = () => {
					this.closeDialog();
				};
				this.title = title;
				this.input = true;
				this.error = error;
				break;
			}
			case "Information":
			default: {
				this.icon = icon;
			}
		}
		this.type = type;
		this.title = title;
		this.message = message;
		this.rightTitle = rightTitle;
		this.rightPress = () => {
			if (this.input) {
				if (this.state.requiredInput.trim().length === 0) {
					this.setState({ inputError: true });
					return;
				}
				rightPress(this.state.requiredInput);
			} else {
				rightPress();
			}

			this.closeDialog();
		};
		this.rightStyle = rightStyle;

		this.setState({ isVisible: true });
	};

	closeDialog = () => {
		this.type = "Information";
		this.title = null;
		this.message = null;
		this.icon = null;
		this.error = null;
		this.leftTitle = "Cancel";
		this.leftPress = null;
		this.leftStyle = {};
		this.rightTitle = "Okay";
		this.rightPress = () => {};
		this.rightStyle = {};

		this.setState({ isVisible: false });
	};

	setInputText = value =>
		this.setState({ requiredInput: value, inputError: false });

	renderInputError = errorMessage => {
		return (
			this.state.inputError && (
				<GoappTextLight size={height / 95} color={"red"} style={styles.error}>
					{errorMessage}
				</GoappTextLight>
			)
		);
	};

	render() {
		const {
			state: { isVisible, requiredInput },
			type,
			crossPress,
			title,
			message,
			icon,
			input,
			error,
			leftTitle,
			leftPress,
			leftStyle,
			rightTitle,
			rightPress,
			rightStyle
		} = this;

		return (
			<DialogModal
				visible={isVisible}
				onRequestClose={type === "Information" ? this.closeDialog : undefined}>
				<View
					onLayout={event => {
						const { height: h } = event.nativeEvent.layout;

						this.containerRef.current.setNativeProps({
							style: { marginVertical: (height - h) / 2 }
						});
					}}
					ref={this.containerRef}
					style={styles.container}>
					{crossPress ? (
						<TouchableOpacity style={styles.cross} onPress={crossPress}>
							<Icon
								iconType="material_community_icon"
								iconName="close"
								iconSize={height / 35}
								iconColor="#2C98F0"
							/>
						</TouchableOpacity>
					) : null}
					{icon ? (
						<FastImage source={icon} style={styles.icon} resizeMode="contain" />
					) : null}
					{title ? (
						<GoappTextBold
							color={"#000000"}
							size={height / 60}
							numberOfLines={1}
							style={styles.title}>
							{title}
						</GoappTextBold>
					) : null}
					{message ? (
						<GoappTextMedium
							color={"#4A4A4A"}
							size={height / 60}
							style={styles.message}>
							{message}
						</GoappTextMedium>
					) : null}
					{input ? (
						<>
							<GoappTextInputRegular
								multiline
								value={requiredInput}
								style={styles.inputContainer(this.state.inputError)}
								onChangeText={text => this.setInputText(text)}
							/>
							{this.renderInputError(error)}
						</>
					) : null}
					<View style={styles.actions}>
						{leftPress ? (
							<ActionButton
								title={leftTitle}
								onPress={leftPress}
								color={"#FFFFFF"}
								titleColor={"#6D7278"}
								style={leftStyle}
							/>
						) : null}
						<ActionButton
							title={rightTitle}
							onPress={rightPress}
							color={"#2C98F0"}
							style={rightStyle}
						/>
					</View>
				</View>
			</DialogModal>
		);
	}
}

export default Dialog;
