import React from "react";
import { View } from "react-native";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
import SimpleLineIcon from "react-native-vector-icons/SimpleLineIcons";
import Ionicon from "react-native-vector-icons/Ionicons";
import Entypo from "react-native-vector-icons/Entypo";
import EvilIcon from "react-native-vector-icons/EvilIcons";
import Feather from "react-native-vector-icons/Feather";
import Octicons from "react-native-vector-icons/Octicons";
import { createIconSetFromIcoMoon } from "react-native-vector-icons";
import icoMoonConfig from "../Assets/Icons/icoMoon.json";
import { icon_moon_font } from "../Assets/styles";

const CustomIcon = createIconSetFromIcoMoon(icoMoonConfig, icon_moon_font);

/**
 *
 * @typedef {Object} Props
 * @property {("font_awesome" | "material" | "simple_line" | "ionicon" | "entypo" | "evilicon" | "material_community_icon" | "feather" | "icomoon" | "octicons")} iconType
 * @property {number} iconSize
 * @property {string} iconColor
 * @property {string} iconName
 * @property {ViewStyle} [style]
 *
 * @param {Props} props
 */

const Icon = props => {
	return (
		<View>
			{props.iconType === "font_awesome" ? (
				<FontAwesomeIcon
					style={props.style}
					name={props.iconName}
					size={props.iconSize}
					color={props.iconColor}
				/>
			) : props.iconType === "material" ? (
				<MaterialIcon
					style={props.style}
					name={props.iconName}
					size={props.iconSize}
					color={props.iconColor}
				/>
			) : props.iconType === "simple_line" ? (
				<SimpleLineIcon
					style={props.style}
					name={props.iconName}
					size={props.iconSize}
					color={props.iconColor}
				/>
			) : props.iconType === "ionicon" ? (
				<Ionicon
					style={props.style}
					name={props.iconName}
					size={props.iconSize}
					color={props.iconColor}
				/>
			) : props.iconType === "entypo" ? (
				<Entypo
					style={props.style}
					name={props.iconName}
					size={props.iconSize}
					color={props.iconColor}
				/>
			) : props.iconType === "evilicon" ? (
				<EvilIcon
					style={props.style}
					name={props.iconName}
					size={props.iconSize}
					color={props.iconColor}
				/>
			) : props.iconType === "material_community_icon" ? (
				<MaterialCommunityIcon
					style={props.style}
					name={props.iconName}
					size={props.iconSize}
					color={props.iconColor}
				/>
			) : props.iconType === "feather" ? (
				<Feather
					style={props.style}
					name={props.iconName}
					size={props.iconSize}
					color={props.iconColor}
				/>
			) : props.iconType === "octicons" ? (
				<Octicons
					style={props.style}
					name={props.iconName}
					size={props.iconSize}
					color={props.iconColor}
				/>
			) : props.iconType === "icomoon" ? (
				<CustomIcon
					style={props.style}
					name={props.iconName}
					size={props.iconSize}
					color={props.iconColor}
				/>
			) : null}
		</View>
	);
};

export default Icon;
