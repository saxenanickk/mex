import React from "react";
import { View, Dimensions, StyleSheet, TouchableOpacity } from "react-native";
import { fontFamily } from "../Assets/styles";
import I18n from "../Assets/strings/i18n";
import Icon from "./Icon";
import Button from "./Button";
import { GoappTextRegular } from "./GoappText";

const { width, height } = Dimensions.get("window");

const Favorites = props => {
	return (
		<View style={styles.container}>
			<Button onPress={props.onPress} style={{ flex: 1 }}>
				<View style={styles.rowContainer}>
					<View
						style={{
							width: width / 18,
							justifyContent: "center",
							alignItems: "center"
						}}>
						<Icon
							iconType={props.iconType}
							iconName={props.iconName}
							iconSize={props.iconSize}
							iconColor={props.iconColor}
						/>
					</View>
					<View style={{ marginLeft: width / 40, width: width / 1.6 }}>
						<GoappTextRegular style={styles.placeHeader}>
							{props.name}
						</GoappTextRegular>
						{props.value == null || props.value === "" ? null : (
							<GoappTextRegular
								style={styles.placeValue}
								numberOfLines={1}
								ellipsizeMode={"tail"}>
								{props.value}
							</GoappTextRegular>
						)}
					</View>
				</View>
			</Button>
			{props.value === null || props.value === "" ? null : (
				<TouchableOpacity onPress={props.onChange}>
					<GoappTextRegular style={{ color: "#000", fontSize: width / 35 }}>
						{I18n.t("change")}
					</GoappTextRegular>
				</TouchableOpacity>
			)}
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		paddingRight: width / 35,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between"
	},
	rowContainer: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between"
	},
	placeHeader: {
		color: "#000000",
		fontSize: width / 30
	},
	placeValue: {
		fontSize: width / 28
	}
});

export default Favorites;
