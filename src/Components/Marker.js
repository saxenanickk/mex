import React from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import MapView from "react-native-maps";
import Icon from "./Icon";
import { GoappTextRegular } from "./GoappText";

const { width, height } = Dimensions.get("window");

const Marker = props => {
	return (
		<MapView.Marker
			onPress={() => props.onPress()}
			coordinate={props.latlng}
			onDragEnd={props.onDrag}>
			{props.type === "destination" ? (
				<View style={styles.container}>
					{props.locationText && (
						<View
							style={[
								styles.destinationTextMarker,
								props.styleDestinationTextMarker
							]}>
							<GoappTextRegular
								style={[styles.destinationText, props.styleDestinationText]}
								numberOfLines={1}
								ellipsizeMode={"tail"}>
								{props.locationText}
							</GoappTextRegular>
						</View>
					)}
					<Icon
						iconType="icomoon"
						iconName={"uber_square"}
						iconSize={width / 25}
						iconColor={"#000"}
					/>
				</View>
			) : (
				<View style={styles.container}>
					{(props.eta !== "" || props.locationText !== "") && (
						<View style={[styles.originTextMarker]}>
							{props.eta && (
								<View style={[styles.etaContainer]}>
									<GoappTextRegular
										style={{ color: "#ffffff", textAlign: "center" }}>
										{isNaN(props.eta) ? props.eta : props.eta + " min"}
									</GoappTextRegular>
								</View>
							)}
							{props.locationText && (
								<GoappTextRegular
									style={[styles.originText]}
									numberOfLines={1}
									ellipsizeMode={"tail"}>
									{props.locationText}
								</GoappTextRegular>
							)}
						</View>
					)}
					<Icon
						iconType="icomoon"
						iconName={"uber_circle"}
						iconSize={width / 25}
						iconColor={"#000"}
					/>
				</View>
			)}
		</MapView.Marker>
	);
};

Marker.defaultProps = {
	onPress: () => {},
	locationText: "",
	eta: ""
};

export default Marker;

const styles = StyleSheet.create({
	container: {
		alignItems: "center",
		justifyContent: "center"
	},
	originTextMarker: {
		flexDirection: "row",
		alignItems: "center",
		borderWidth: 0,
		borderTopLeftRadius: 2,
		borderBottomLeftRadius: 2,
		elevation: 5,
		width: width / 3,
		backgroundColor: "#ffffff"
	},
	destinationTextMarker: {
		padding: 5,
		justifyContent: "center",
		alignItems: "center",
		borderWidth: 0,
		borderRadius: 2,
		elevation: 10,
		width: width / 4,
		backgroundColor: "#ffffff"
	},
	etaContainer: {
		borderTopLeftRadius: 2,
		borderBottomLeftRadius: 2,
		justifyContent: "center",
		alignItems: "center",
		width: width / 8,
		backgroundColor: "#000000",
		height: "100%"
	},
	originText: {
		color: "#000000",
		fontSize: height / 50,
		marginLeft: 5,
		marginRight: 5,
		width: (4.5 * width) / 24
	},
	destinationText: {
		color: "#000000",
		fontSize: height / 50,
		width: width / 4.5
	}
});
