import React from "react";
import { View, Dimensions } from "react-native";
import styles from "./styles";
import { GoappTextRegular } from "../GoappText";

const { height } = Dimensions.get("window");

const OfflineMessage = ({ show }) =>
	show ? (
		<View style={styles.container}>
			<GoappTextRegular
				color={"#ffffff"}
				size={height / 70}>{`Offline Mode`}</GoappTextRegular>
		</View>
	) : null;

export default OfflineMessage;
