import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: {
		zIndex: 10,
		paddingHorizontal: width / 20,
		paddingVertical: height / 200,
		position: "absolute",
		bottom: height / 12,
		width: width,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "grey"
	}
});

export default styles;
