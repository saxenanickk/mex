// @ts-check
import React from "react";
import {
	DatePickerAndroid,
	DatePickerIOS,
	Platform,
	View,
	LayoutAnimation,
	TouchableOpacity,
	Dimensions
} from "react-native";
import { CleartripTextRegular } from "../Containers/Cleartrip/Components/CleartripText";

const { width, height } = Dimensions.get("window");
const EPOCH_ONE_YEAR_DURATION = 31536000000;
const ONE_DAY_EPOCH_DURATION = 86400000;

/**
 *
 * @param {object} props
 * @param {Date} props.date
 * @param {Date} props.minimumDate
 * @param {Date} props.maximumDate
 * @param {() => void} props.onDateChange
 */

export default class DatePicker extends React.Component {
	SELECTED_DATE = null;
	constructor(props) {
		super(props);
		this.state = {
			showDatePickerIos: false
		};
	}

	renderDatePickerIos() {
		return (
			<View
				style={{
					position: "absolute",
					alignSelf: "center",
					backgroundColor: "#ffffff",
					top: this.state.showDatePickerIos ? height / 1.8 : height,
					bottom: this.state.showDatePickerIos ? 0 : -height / 2.2,
					left: 0,
					right: 0,
					shadowOffset: { width: 2, height: 2 },
					shadowColor: "black",
					shadowOpacity: 0.2
				}}>
				<View
					style={{
						width: width,
						height: height / 20,
						backgroundColor: "transparent",
						borderWidth: 0.4,
						borderColor: "grey",
						flexDirection: "row",
						alignItems: "center",
						justifyContent: "space-between"
					}}>
					<TouchableOpacity
						onPress={() => {
							LayoutAnimation.spring();
							this.setState({ showDatePickerIos: false }, () =>
								setTimeout(() => this.props.onDateChange(null), 100)
							);
						}}
						style={{ flex: 1, paddingHorizontal: width / 25 }}>
						<CleartripTextRegular style={{ color: "#1f3d7a" }}>
							{"Cancel"}
						</CleartripTextRegular>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={() => {
							LayoutAnimation.spring();
							this.setState({ showDatePickerIos: false }, () =>
								setTimeout(() => {
									if (!this.SELECTED_DATE) {
										this.SELECTED_DATE = {
											day: this.props.date.getDate(),
											month: this.props.date.getMonth(),
											year: this.props.date.getFullYear()
										};
									}
									this.props.onDateChange(this.SELECTED_DATE);
								}, 100)
							);
						}}
						style={{
							flex: 1,
							paddingHorizontal: width / 25,
							alignItems: "flex-end"
						}}>
						<CleartripTextRegular style={{ color: "#1f3d7a" }}>
							{"Done"}
						</CleartripTextRegular>
					</TouchableOpacity>
				</View>
				<View style={{ flex: 1 }}>
					<DatePickerIOS
						style={{ flex: 1 }}
						mode={"date"}
						date={this.props.date}
						minimumDate={this.props.minimumDate}
						maximumDate={this.props.maximumDate}
						onDateChange={date => {
							let dateObject = new Date(date);
							console.log(dateObject);
							this.SELECTED_DATE = {
								day: dateObject.getDate(),
								month: dateObject.getMonth(),
								year: dateObject.getFullYear()
							};
						}}
					/>
				</View>
			</View>
		);
	}

	async componentDidMount() {
		if (Platform.OS === "android") {
			try {
				const date = await DatePickerAndroid.open({
					date: this.props.date,
					minDate: this.props.minimumDate,
					maxDate: this.props.maximumDate
				});
				if (date.action !== DatePickerAndroid.dismissedAction) {
					console.log("Android Responce: ", date);
					this.props.onDateChange(date);
				} else {
					this.props.onDateChange(null);
				}
			} catch ({ code, message }) {
				console.warn("Cannot open date picker", message);
			}
		} else {
			setTimeout(() => {
				LayoutAnimation.spring();
				this.setState({ showDatePickerIos: true });
			}, 100);
		}
	}

	render() {
		return Platform.OS === "ios" ? this.renderDatePickerIos() : null;
	}
}

DatePicker.defaultProps = {
	date: new Date(),
	minimumDate: new Date(),
	maximumDate: new Date(new Date().getTime() + EPOCH_ONE_YEAR_DURATION),
	onDateChange: () => {}
};
