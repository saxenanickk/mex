import React from "react";
import { Dimensions, StyleSheet, TouchableOpacity } from "react-native";
import { GoappTextRegular } from "./GoappText";

const { width, height } = Dimensions.get("window");

const Button = props => {
	return (
		<TouchableOpacity
			style={[styles.container, props.style]}
			onPress={props.onPress}>
			{props.children}
			<GoappTextRegular style={[styles.title_style, props.title_style]}>
				{props.title}
			</GoappTextRegular>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	container: {
		flexDirection: "row",
		backgroundColor: "#ffffff",
		width: width / 1.5,
		height: height / 11,
		paddingLeft: width / 35,
		alignItems: "center"
	},
	title_style: {
		color: "#000000",
		fontSize: width / 18
	}
});

export default Button;
