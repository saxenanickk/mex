import { Component } from "react";
import { Animated, Text, View, ViewPropTypes } from "react-native";
import PropTypes from "prop-types";

export default class BaseInput extends Component {
	static propTypes = {
		label: PropTypes.string,
		value: PropTypes.string,
		defaultValue: PropTypes.string,
		style: ViewPropTypes ? ViewPropTypes.style : View.propTypes.style,
		inputStyle: Text.propTypes.style,
		labelStyle: Text.propTypes.style,
		easing: PropTypes.func,
		animationDuration: PropTypes.number,
		useNativeDriver: PropTypes.bool,

		editable: PropTypes.bool,

		/* these are TextInput props which are overridden
		 * so, i'm calling them manually
		 */
		onBlur: PropTypes.func,
		onFocus: PropTypes.func,
		onChange: PropTypes.func,
		onChangeText: PropTypes.func
	};

	constructor(props, context) {
		super(props, context);

		this._onLayout = this._onLayout.bind(this);
		this._onChange = this._onChange.bind(this);
		this._onBlur = this._onBlur.bind(this);
		this._onFocus = this._onFocus.bind(this);
		this.focus = this.focus.bind(this);

		const value = props.value || props.defaultValue;

		this.state = {
			value,
			focusedAnim: new Animated.Value(value ? 1 : 0),
			textAnim: new Animated.Value(value ? 1 : 0),
			required: false
		};
	}

	componentWillReceiveProps(newProps) {
		const newValue = newProps.value;
		if (newProps.hasOwnProperty("value") && newValue !== this.state.value) {
			this.setState({
				value: newValue
			});

			// animate input if it's active state has changed with the new value
			// and input is not focused currently.
			const isFocused = this.refs.input.isFocused();
			if (!isFocused) {
				const isActive = Boolean(newValue);
				if (isActive !== this.isActive) {
					this._toggle(isActive);
				}
			}
		}
	}

	_onLayout(event) {
		this.setState({
			width: event.nativeEvent.layout.width
		});
	}

	_onChange(event) {
		this.setState({
			value: event.nativeEvent.text
		});
		const onChange = this.props.onChange;
		if (onChange) {
			onChange(event);
		}
	}

	validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}
	_onBlur(event) {
		const onBlur = this.props.onBlur;
		if (!this.state.value) {
			this._toggle(false);
			if (this.props.checks.isRequired) {
				this.setState({ required: true });
			}
		} else {
			let invalid = false;
			if (
				this.props.checks.minSize &&
				this.props.checks.minSize > this.state.value.trim().length
			) {
				invalid = invalid || true;
			}
			if (
				this.props.checks.maxSize &&
				this.props.checks.maxSize < this.state.value.trim().length
			) {
				invalid = invalid || true;
			}
			if (
				this.props.checks.isEmail &&
				!this.validateEmail(this.state.value.trim())
			) {
				invalid = invalid || true;
			}
			if (this.props.checks.isNumber && isNaN(this.state.value.trim())) {
				invalid = invalid || true;
			}
			this.setState({ invalid, required: false });
		}
		if (onBlur) {
			onBlur(true);
		}
	}

	_onFocus(event) {
		this._toggle(true);

		const onFocus = this.props.onFocus;
		if (onFocus) {
			onFocus(event);
		}
	}

	_toggle(isActive) {
		const { animationDuration, easing, useNativeDriver } = this.props;
		this.isActive = isActive;
		if (isActive) {
			Animated.parallel([
				Animated.timing(this.state.focusedAnim, {
					toValue: isActive ? 1 : 0,
					duration: animationDuration,
					easing,
					useNativeDriver
				}),
				Animated.timing(this.state.textAnim, {
					toValue: isActive ? 1 : 0,
					duration: animationDuration / 2,
					easing,
					useNativeDriver
				})
			]).start();
		}
	}

	// public methods

	inputRef() {
		return this.refs.input;
	}

	focus() {
		if (this.props.editable !== false) {
			this.inputRef().focus();
		}
	}

	blur() {
		this.inputRef().blur();
	}

	isFocused() {
		return this.inputRef().isFocused();
	}

	clear() {
		this.inputRef().clear();
	}
}
