import { takeLatest, put, call } from "redux-saga/effects";
import Api from "./Api";
import { PARTNERS, saveData, clearData } from "../../CustomModules/Offline";

/**
 * Constants
 */
export const MEX_GET_PARTNERS = "MEX_GET_PARTNERS";
export const MEX_SAVE_PARTNERS = "MEX_SAVE_PARTNERS";
export const MEX_PARTNERS_LOADING = "MEX_PARTNERS_LOADING";

/**
 * Action Creators
 */
export const mexGetPartners = payload => ({
	type: MEX_GET_PARTNERS,
	payload
});

export const mexSavePartners = payload => ({
	type: MEX_SAVE_PARTNERS,
	payload
});

export const mexPartnersLoading = payload => ({
	type: MEX_PARTNERS_LOADING,
	payload
});

/**
 * Root Saga
 */
export default function* partnersSaga(dispatch) {
	yield takeLatest(MEX_GET_PARTNERS, handleMexGetPartners);
}

/**
 * Handlers
 */
function* handleMexGetPartners(action) {
	try {
		const partners = yield call(Api.getPartnerApps, action.payload);
		console.log("MexGetPartners: ", partners);
		if (partners.success === 1) {
			// Save Partners to AsyncStorage
			saveData(PARTNERS, partners.data);
			yield put(mexSavePartners(partners.data));
		} else {
			clearData(PARTNERS);
		}
	} catch (error) {
		console.log("Error in MexGetPartners: ", error.message);
		// TODO
	} finally {
		yield put(mexPartnersLoading(false));
	}
}
