import React, { Component } from "react";
import { View, Image, Dimensions, TouchableOpacity } from "react-native";
import { GoappTextMedium, GoappTextRegular } from "../GoappText";

const { width, height } = Dimensions.get("window");

const Apps = ({ apps, launchApp, azgoLoading }) => (
	<View style={styles.appContainer}>
		{apps.map((appDetails, index) => {
			return (
				<TouchableOpacity
					activeOpacity={1}
					onPress={() => launchApp(appDetails)}
					key={index}
					style={styles.app}>
					<Image
						style={styles.image}
						source={{ uri: appDetails.image }}
						resizeMode={"cover"}
					/>
					<GoappTextRegular style={styles.appText}>
						{azgoLoading && appDetails.name === "Azgo"
							? `Loading ${appDetails.name}`
							: appDetails.name}
					</GoappTextRegular>
				</TouchableOpacity>
			);
		})}
	</View>
);

class AppBox extends Component {
	render() {
		const { title, apps, launchApp, azgoLoading } = this.props;
		if (this.props.apps.length > 0) {
			return (
				<View style={styles.container}>
					<GoappTextMedium
						size={height / 55}
						color={"#171717"}
						style={{ marginBottom: height / 60 }}>
						{title}
					</GoappTextMedium>
					<Apps apps={apps} launchApp={launchApp} azgoLoading={azgoLoading} />
					{this.props.isShowBorder ? <View style={styles.separator} /> : null}
				</View>
			);
		} else {
			return null;
		}
	}
}

const styles = {
	container: {
		width: "100%",
		backgroundColor: "#fff",
		paddingHorizontal: width / 20
	},
	appContainer: {
		flexDirection: "row",
		flexWrap: "wrap",
		flex: 1
	},
	app: {
		width: width / 4.655,
		paddingTop: height / 100,
		alignItems: "center"
	},
	image: { width: width / 8, height: width / 8 },
	appText: {
		marginTop: width / 50,
		width: width / 6,
		textAlign: "center",
		fontSize: height / 68
	},
	separator: {
		width: "100%",
		alignSelf: "center",
		backgroundColor: "#eff1f4",
		height: 1,
		marginVertical: height / 30
	}
};

export default AppBox;
