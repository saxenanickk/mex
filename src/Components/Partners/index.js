import React, { Component } from "react";
import {
	View,
	Dimensions,
	NativeModules,
	LayoutAnimation,
	Platform,
	Keyboard,
	ScrollView,
	BackHandler
} from "react-native";
import styles from "./style";
import { GoappTextBold, GoappTextRegular } from "../GoappText";
import { connect } from "react-redux";
import MarketPlace from "./MarketPlace";
import ProgressScreen from "../ProgressScreen";
import { mexGetPartners } from "./Saga";
import { withOfflineCapability } from "../../CustomModules/Offline/Saga";
import TabNavigationContext from "../../Containers/Goapp/Context/TabNavigationContext";

const { width, height } = Dimensions.get("window");
const { UIManager } = NativeModules;

if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}

class Partners extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isEditing: false,
			searchText: "",
			selectedSite: null
		};
		this.toggleEditing = this.toggleEditing.bind(this);
		this.changeSearchText = this.changeSearchText.bind(this);
		this.getPartnerApps = this.getPartnerApps.bind(this);
	}

	handleBack = () => {
		const { navigationOfTab } = this.context;
		navigationOfTab("Home");
		return true;
	};

	componentDidMount() {
		if (this.props.selectedSite) {
			this.getPartnerApps();
		}
		this._focusunsubscribe = this.props.navigation.addListener("focus", () => {
			BackHandler.addEventListener("hardwareBackPress", this.handleBack);
		});
		this._blurunsubscribe = this.props.navigation.addListener("blur", () => {
			BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
		});
	}

	componentWillUnmount() {
		this._focusunsubscribe();
		this._blurunsubscribe();
	}

	shouldComponentUpdate(props, state) {
		if (props.selectedSite && props.selectedSite !== this.props.selectedSite) {
			this.getPartnerApps(props);
		}
		return true;
	}

	toggleEditing = () => {
		if (this.state.isEditing) {
			this.inputRef && this.inputRef.blur();
			Keyboard.dismiss();
		}
		setTimeout(() => {
			LayoutAnimation.easeInEaseOut();
			this.setState({ isEditing: !this.state.isEditing, searchText: "" });
		}, 100);
	};

	changeSearchText = text => {
		this.setState({ searchText: text });
	};

	getPartnerApps = async (props = this.props) => {
		this.props.dispatch(
			withOfflineCapability(
				mexGetPartners({
					siteId: props.selectedSite.id,
					appToken: props.appToken
				})
			)
		);
	};

	render() {
		return (
			<ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
				<View style={styles.header}>
					<View style={styles.subHeader}>
						<GoappTextBold
							color={"#273D52"}
							numberOfLines={1}
							style={styles.headerText}
							size={height / 35}>
							{"Partners"}
						</GoappTextBold>
						<View style={styles.siteSelectionButton}>
							<GoappTextRegular
								color={"#777b80"}
								numberOfLines={1}
								style={styles.siteText}
								size={height / 55}>
								{this.props.selectedSite ? this.props.selectedSite.name : ""}
							</GoappTextRegular>
						</View>
					</View>
				</View>
				{this.props.isLoading ? (
					<View style={styles.loadingView}>
						<ProgressScreen
							isMessage={true}
							primaryMessage={"Hang On"}
							message={"Getting Partner apps"}
							indicatorSize={height / 30}
							indicatorColor={"#2C98F0"}
							messageStyle={{ fontSize: height / 65, width: width / 2.7 }}
							primaryMessageStyles={{
								fontSize: height / 60,
								width: width / 2.7
							}}
						/>
					</View>
				) : (
					<MarketPlace
						navigation={this.props.navigation}
						configApps={this.props.apps}
						onRefresh={() => {
							if (this.props.selectedSite) {
								this.getPartnerApps();
							}
						}}
					/>
				)}
			</ScrollView>
		);
	}
}

Partners.contextType = TabNavigationContext;

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		selectedSite: state.appToken.selectedSite,
		apps: state.partners.apps,
		isLoading: state.partners.isLoading
	};
}
export default connect(mapStateToProps)(Partners);
