import {
	MEX_SAVE_PARTNERS,
	MEX_PARTNERS_LOADING,
	MEX_GET_PARTNERS
} from "./Saga";

const initialState = {
	apps: [],
	isLoading: true
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case MEX_GET_PARTNERS:
			return { ...state, apps: [], isLoading: true };
		case MEX_SAVE_PARTNERS:
			return { ...state, apps: action.payload };
		case MEX_PARTNERS_LOADING:
			return {
				...state,
				isLoading: action.payload
			};
		default:
			return state;
	}
};
