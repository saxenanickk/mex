import Config from "react-native-config";
const { SERVER_BASE_URL_PARTNERS, APP_VERSION } = Config;

class Api {
	getPartnerApps(params) {
		const SITE_ID = params.siteId;
		const APP_TOKEN = params.appToken;

		return new Promise(function(resolve, reject) {
			fetch(
				`${SERVER_BASE_URL_PARTNERS}getPartnerApps?site_id=${SITE_ID}&version=${APP_VERSION}`,
				{
					method: "GET",
					headers: {
						"Content-type": "application/json",
						"X-ACCESS-TOKEN": APP_TOKEN
					}
				}
			)
				.then(response => {
					response
						.json()
						.then(resp => {
							resolve(resp);
						})
						.catch(error => {
							reject(error);
						});
				})
				.catch(error => {
					reject(error);
				});
		});
	}
}

export default new Api();
