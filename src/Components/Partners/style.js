import { StyleSheet, Dimensions, Platform } from "react-native";
import { font_two } from "../../Assets/styles";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff"
	},
	header: {
		width: width,
		backgroundColor: "#fff",
		paddingVertical: height / 36,
		paddingHorizontal: width / 18.75,
		borderBottomWidth: 0.5,
		borderColor: "#cfcfcf"
	},
	inputSection: {
		height: height / 18,
		paddingHorizontal: width / 28,
		flexDirection: "row",
		alignItems: "center",
		backgroundColor: "#EBEBEB",
		borderRadius: width / 50
	},
	textInput: {
		width: width / 1.8,
		padding: 0,
		height: height / 21.16,
		marginLeft: width / 30,
		fontSize: height / 50,
		color: "#424446",
		fontFamily: Platform.OS === "ios" ? "Avenir" : font_two,
		fontWeight: Platform.OS === "ios" ? "400" : undefined
	},
	inputRow: {
		marginTop: height / 29,
		width: width / 1.12,
		height: height / 18,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between"
	},
	subHeader: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	headerText: {
		width: width / 3
	},
	siteText: {
		marginRight: width / 50
	},
	siteSelectionButton: {
		flexDirection: "row",
		alignItems: "center"
	},
	favList: {
		backgroundColor: "#fff",
		paddingVertical: height / 40,
		marginTop: height / 40
	},
	favHeader: {
		width: width / 2,
		marginLeft: width / 30,
		color: "#000"
	},
	loadingView: {
		height: height / 1.5,
		justifyContent: "center",
		alignItems: "center"
	}
});

export default styles;
