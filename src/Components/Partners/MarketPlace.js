import React from "react";
import {
	StyleSheet,
	View,
	Dimensions,
	Platform,
	TouchableOpacity,
	Image,
	ImageBackground
} from "react-native";
import { OFFERS, OFFERFRONT, OFFERPOINTER } from "./Img/Image";
import AppBox from "./AppBox";
import { connect } from "react-redux";
import MiniApp from "../../CustomModules/MiniApp";
import { GoappTextBold, GoappTextRegular } from "../GoappText";
import { NoSiteView } from "../NoSiteView";
import DialogContext from "../../DialogContext";
import noInternetDialog from "../NoInternetDialog";

const { width, height } = Dimensions.get("window");

const foodApps = [
	{
		name: "Swiggy",
		appName: "Swiggy",
		category: "Food",
		isPwa: false,
		pwaUrl: null
	},
	{
		name: "Licious",
		appName: "Licious",
		category: "Food",
		isPwa: true,
		pwaUrl:
			"https://m.licious.in/?utm_source=RMZ&utm_medium=RMZ-MEX&utm_campaign=RMZ-May"
	},
	{
		name: "Azgo",
		appName: "Azgo",
		category: "Food",
		isPwa: true,
		pwaUrl: "https://rmz.azgo.app/?session="
	},
	{
		name: "HungerBox",
		appName: "Hungerbox",
		category: "Food",
		isPwa: true,
		pwaUrl: "https://rmz.azgo.app/?session="
	},
	{
		name: "The Good Bowl",
		appName: "The Good Bowl",
		category: "Food",
		isPwa: true,
		pwaUrl:
			"https://www.thegoodbowl.co/?utm_source=MEX&utm_medium=MEXmedia&utm_campaign=MEXoffers"
	},
	{
		name: "BBInstant",
		appName: "BBInstant",
		category: "Food",
		isPwa: false,
		pwaUrl: null
	},
	{
		name: "Faasos",
		appName: "Faasos",
		category: "Food",
		isPwa: true,
		pwaUrl:
			"https://order.faasos.io/?utm_source=MEX&utm_medium=MEXmedia&utm_campaign=MEXoffers"
	},
	{
		name: "Behrouz",
		appName: "Behrouz",
		category: "Food",
		isPwa: true,
		pwaUrl:
			"https://www.behrouzbiryani.com/?utm_source=MEX&utm_medium=MEXmedia&utm_campaign=MEXoffers"
	},
	{
		name: "Oven Story",
		appName: "Oven Story",
		category: "Food",
		isPwa: true,
		pwaUrl:
			"https://www.ovenstory.in/?utm_source=MEX&utm_medium=MEXmedia&utm_campaign=MEXoffers"
	}
];

const commuteApps = [
	{
		name: "Ola",
		appName: "Ola",
		category: "Commute",
		isPwa: false,
		pwaUrl: null
	},
	{
		name: "Quick Ride",
		appName: "QuickRide",
		category: "Commute",
		isPwa: true,
		pwaUrl:
			"https://pwa.getquickride.com:443/#/login/?appToken=a176-c46edc76615"
	},
	{
		name: "Drivezy",
		appName: "Drivezy",
		category: "Commute",
		isPwa: true,
		pwaUrl:
			"https://m.drivezy.com/?utm_source=RMZ&utm_medium=Referral&utm_campaign=PWAOnMex"
	},
	{
		name: "Instacar",
		appName: "Instacar",
		category: "Others",
		isPwa: true,
		pwaUrl: "http://bit.ly/InstacarRMZ1"
	},
	{
		name: "Savaari",
		appName: "Savaari",
		category: "Others",
		isPwa: true,
		pwaUrl:
			"https://www.savaari.com/?utm_source=rmzcorp&utm_campaign=rmzcorp&utm_medium=rmz_banner"
	}
];

const travelApps = [
	{
		name: "Redbus",
		backgroundColor: "#e5f8f3",
		appName: "Redbus",
		category: "Travel",
		isPwa: false,
		pwaUrl: null
	},
	{
		name: "Cleartrip",
		backgroundColor: "#e5f8f3",
		appName: "Cleartrip",
		category: "Travel",
		isPwa: false,
		pwaUrl: null
	},
	{
		name: "Cleartrip Hotels",
		backgroundColor: "#e5f8f3",
		appName: "CleartripHotel",
		category: "Travel",
		isPwa: false,
		pwaUrl: null
	},
	{
		name: "Oyo",
		backgroundColor: "#e5f8f3",
		appName: "Oyo",
		category: "Travel",
		isPwa: true,
		pwaUrl: "https://www.oyorooms.com/microsite/partner/?utm_source=partner"
	}
];

const financialApps = [
	{
		name: "SwitchMe",
		backgroundColor: "#e5f8f3",
		appName: "SwitchMe",
		category: "Financial",
		isPwa: true,
		pwaUrl: "https://www.switchme.in/partner-landing/utm_source=rmz"
	},
	{
		name: "ICICI Bank Auto Loan",
		backgroundColor: "#e5f8f3",
		appName: "ICICI Auto Loan",
		category: "Financial",
		isPwa: true,
		pwaUrl:
			"https://loan.icicibank.com/asset-portal/auto-loan/apply-now?WT.mc_id=RMZ_Mex_ALoffer&utm_source=RMZ&utm_medium=Banner&utm_campaign=Mex_ALoffer"
	},
	{
		name: "ICICI Bank Personal Loan",
		backgroundColor: "#e5f8f3",
		appName: "ICICI Personal Loan",
		category: "Financial",
		isPwa: true,
		pwaUrl:
			"https://loan.icicibank.com/asset-portal/personal-loan/apply-now?WT.mc_id=RMZ_Mex_PLoffer&utm_source=RMZ&utm_medium=Banner&utm_campaign=Mex_PLoffer"
	}
];

const healthApps = [
	{
		name: "Portea",
		appName: "Portea",
		category: "Health",
		isPwa: true,
		pwaUrl: "https://lp.portea.com/gnrc-ofr-rmz200/"
	},
	{
		name: "1mg",
		appName: "1mg",
		category: "Health",
		isPwa: true,
		pwaUrl:
			"https://www.1mg.com/?utm_source=RMZ&utm_medium=app&utm_campaign=mex"
	},
	{
		name: "RxDX",
		appName: "RXDX",
		isPwa: true,
		pwaUrl: "https://mex.smartenspaces.com/medical/"
	},
	{
		name: "Medlife",
		appName: "Medlife",
		isPwa: true,
		pwaUrl:
			"https://m.medlife.com/?utm_source=RMZ_coworks&utm_medium=MEX_Marketplace#/root/home/HomeLandingOld"
	},
	{
		name: "Docsapp",
		appName: "Docsapp",
		category: "Others",
		isPwa: true,
		pwaUrl: "https://m.docsapp.in/"
	}
];

const otherApps = [
	{
		name: "BookMyShow",
		appName: "BookMyShow",
		category: "Others",
		isPwa: true,
		pwaUrl: "https://in.bookmyshow.com/"
	},
	{
		name: "Samsung",
		appName: "Samsung",
		category: "Others",
		isPwa: true,
		pwaUrl: "https://www.samsung.com/in/store/rmz"
	}
];

const lifeStyleApps = [
	{
		name: "Housejoy",
		appName: "Housejoy",
		category: "Others",
		isPwa: true,
		pwaUrl:
			"https://www.housejoy.in/?utm_source=MEXRMZ&utm_medium=INapp&utm_campaign=promotions"
	},
	{
		name: "Quikr",
		appName: "Quikr",
		category: "Others",
		isPwa: true,
		pwaUrl:
			"https://www.athomediva.com/?utm_source=RMZ&utm_medium=mex&utm_campaign=Coupon1000"
	},
	{
		name: "Rentomojo",
		appName: "Rentomojo",
		category: "Others",
		isPwa: true,
		pwaUrl: "https://ws3q.app.link/fNm9TyqSr1?%243p=a_custom_721331162924082765"
	},
	{
		name: "Wakefit",
		appName: "Wakefit",
		category: "Others",
		isPwa: true,
		pwaUrl:
			"https://www.wakefit.co/?utm_source=RMZEco&utm_medium=workplace&utm_campaign=Partnership"
	}
];
const AllApps = [
	{
		category: "Food",
		tagLine: "Feeling Hungry?",
		color: "#F0851D",
		apps: foodApps
	},
	{
		category: "Commute",
		tagLine: "Want to Commute?",
		color: "#444748",
		apps: commuteApps
	},
	{
		category: "Travel",
		tagLine: "Planning to Travel?",
		color: "#DB3C44",
		apps: travelApps
	},
	{
		category: "LifeStyle",
		tagLine: "LifeStyle",
		color: "#6DD400",
		apps: lifeStyleApps
	},
	{
		category: "Health",
		tagLine: "Want quick health services?",
		color: "#32C0BE",
		apps: healthApps
	},
	{
		category: "Financial",
		tagLine: "Need financial help?",
		color: "#3266C0",
		apps: financialApps
	},
	{
		category: "Others",
		tagLine: "Others",
		color: "#F4E258",
		apps: otherApps
	}
];

class MarketPlace extends React.Component {
	constructor() {
		super();
		this.state = {
			azgoLoading: false
		};
	}

	onAppClick = app => {
		try {
			if (app.isPwa !== undefined) {
				if (app.isPwa === true) {
					if (!this.props.isInternetAvailable) noInternetDialog(this.context);
					else {
						if (app.appName === "Azgo" || app.appName === "Hungerbox") {
							MiniApp.launchPhoneVerifiedApp(
								app.appName,
								this.props.userProfile,
								this.props.navigation
							);
						} else {
							MiniApp.launchWebApp(app.appName, app.pwaUrl);
						}
					}
				} else {
					switch (app.name) {
						case "Swiggy":
							if (this.props.isInternetAvailable)
								MiniApp.launchNativeApp("swiggy");
							else noInternetDialog(this.context);
							break;
						case "BBInstant":
							if (Platform.OS === "android") {
								MiniApp.launchPhoneVerifiedApp(
									app.appName,
									this.props.userProfile,
									this.props.navigation
								);
							} else {
								this.context.current.openDialog({
									type: "Information",
									title: "Info!",
									message:
										"BBInstant coming soon on iOS. Currently available only on Android."
								});
							}
							break;
						default:
							if (this.props.isInternetAvailable)
								MiniApp.launchApp(this.props.navigation, app.appName);
							else noInternetDialog(this.context);
					}
				}
			}
		} catch (error) {
			console.log("unable to open app", error);
		}
	};

	handleLaunchApp = async app => {
		this.onAppClick(app);
	};

	getApps = appGroup => {
		let temp = appGroup.apps.filter(item => {
			if (
				this.props.configApps.findIndex(app => app.app_name === item.appName) >=
				0
			) {
				item.image = this.props.configApps[
					this.props.configApps.findIndex(app => app.app_name === item.appName)
				].app_icon;
				console.log(item);
				return true;
			}
		});
		return temp;
	};

	checkIsOffersAvailable = () => {
		let isAvailable =
			this.props.configApps.findIndex(app => app.app_name === "Offers") >= 0;
		return isAvailable;
	};
	render() {
		return (
			<View
				style={styles.whiteContainer}
				onScroll={this.props.onScroll}
				bounces={false}>
				{this.props.configApps.length > 0 ? (
					<React.Fragment>
						{this.checkIsOffersAvailable() ? (
							<TouchableOpacity
								style={styles.offerButton}
								activeOpacity={1}
								onPress={() =>
									this.handleLaunchApp({
										name: "Offers",
										appName: "Offers",
										type: "NATIVE",
										isPwa: false
									})
								}>
								<View style={styles.offersContainer}>
									<ImageBackground
										source={OFFERFRONT}
										resizeMode={"contain"}
										style={styles.offersImageContainer}>
										<View style={styles.offersInnerContainer}>
											<Image
												source={OFFERS}
												style={{ width: width / 12, height: width / 12 }}
											/>
										</View>
										<TouchableOpacity
											onPress={() =>
												this.handleLaunchApp({
													name: "Offers",
													appName: "Offers",
													type: "NATIVE",
													isPwa: false
												})
											}
											style={styles.offerFullButton}>
											<GoappTextRegular color={"#fff"} size={height / 70}>
												{"Handpicked offers for MEX members"}
											</GoappTextRegular>
											<View style={styles.exploreSection}>
												<GoappTextBold color={"#fff"} size={height / 50}>
													{"Explore MEX. Offers"}
												</GoappTextBold>
												<Image
													source={OFFERPOINTER}
													style={styles.offerPointer}
												/>
											</View>
										</TouchableOpacity>
									</ImageBackground>
								</View>
							</TouchableOpacity>
						) : null}
						<View style={styles.separator} />
						<View style={styles.allPartnerView}>
							<GoappTextBold size={height / 45}>{"All Partners"}</GoappTextBold>
						</View>
						{this.props.AllApps.map((appGroup, index) => (
							<React.Fragment key={index}>
								<AppBox
									azgoLoading={this.state.azgoLoading}
									title={appGroup.tagLine}
									themeColor={appGroup.color}
									apps={this.getApps(appGroup)}
									launchApp={this.handleLaunchApp}
									isShowBorder={this.props.AllApps.length - 1 > index}
								/>
							</React.Fragment>
						))}
						<View style={styles.bottomPadding} />
					</React.Fragment>
				) : (
					<NoSiteView
						style={styles.noSiteView}
						message={"No Partner Apps Found"}
						isRefresh={true}
						onRefresh={() => this.props.onRefresh()}
					/>
				)}
			</View>
		);
	}
}

MarketPlace.contextType = DialogContext;
MarketPlace.defaultProps = {
	onScroll: () => console.log("Pass onScroll Prop to MarketPlace"),
	launchApp: () => console.log("name to launch the app"),
	launchWebApp: () => {},
	width: width,
	height: height,
	AllApps: AllApps
};

function mapStateToProps(state) {
	return {
		userProfile: state.appToken.userProfile,
		isInternetAvailable: state.netInfo.status
	};
}

export default connect(mapStateToProps)(MarketPlace);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		backgroundColor: "#fff"
	},
	offersContainer: {
		backgroundColor: "#2465e9",
		elevation: 3,
		borderRadius: width / 20,
		marginHorizontal: width / 27,
		alignItems: "center",
		flexDirection: "row"
	},
	offersImageContainer: {
		borderRadius: width / 20,
		paddingVertical: height / 50,
		alignItems: "center",
		flexDirection: "row",
		paddingHorizontal: width / 24
	},
	headerText: {
		fontSize: height / 40,
		color: "#000",
		fontWeight: "bold",
		alignSelf: "center",
		marginBottom: height / 50
	},
	scrollStyle: {
		flexDirection: "row",
		flexWrap: "wrap",
		width: width,
		paddingHorizontal: width / 12,
		justifyContent: "space-between",
		marginBottom: height / 20,
		marginTop: height / 60
	},
	allPartnerView: {
		backgroundColor: "#fff",
		width: width,
		paddingVertical: height / 50,
		paddingHorizontal: width / 20
	},
	separator: {
		height: height / 100,
		backgroundColor: "#eff1f4",
		width: width
	},
	offerButton: {
		backgroundColor: "#fff",
		marginVertical: height / 90,
		paddingVertical: height / 60
	},
	offersInnerContainer: {
		width: width / 6,
		height: width / 6,
		borderRadius: width / 12,
		backgroundColor: "#ffffff",
		shadowOffset: { width: 1, height: 2 },
		shadowColor: "grey",
		shadowOpacity: 0.2,
		elevation: 3,
		justifyContent: "center",
		alignItems: "center"
	},
	exploreSection: {
		borderTopWidth: 1,
		borderTopColor: "#eff1f4",
		paddingTop: 3,
		flexDirection: "row",
		alignItems: "center"
	},
	offerPointer: {
		width: width / 45,
		height: width / 45,
		marginLeft: width / 50,
		marginTop: 3
	},
	whiteContainer: { backgroundColor: "#fff" },
	noSiteView: {
		height: height / 1.2,
		justifyContent: "center",
		alignItems: "center"
	},
	bottomPadding: {
		height: height / 12,
		backgroundColor: "#eff1f4",
		marginTop: height / 60
	},
	offerFullButton: {
		justifyContent: "space-between",
		height: width / 8,
		marginLeft: 10,
		flex: 1
	}
});
