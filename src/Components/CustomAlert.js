import React, { Component } from "react";
import {
	Modal,
	View,
	Dimensions,
	TouchableOpacity,
	Image,
	Platform
} from "react-native";
import Icon from "./Icon";
import {
	GoappTextBold,
	GoappTextMedium,
	GoappTextInputRegular
} from "./GoappText";
import { connect } from "react-redux";

const INFORMATION = "Information";
const { width, height } = Dimensions.get("window");
class CustomAlert extends Component {
	constructor(props) {
		super(props);
		this.state = {
			open: false,
			title: "",
			type: "",
			message: "",
			icon: null,
			cancelAction: null,
			okAction: null,
			inputText: "",
			isInputRequired: false,
			inputError: false
		};
	}

	setOpen = value => this.setState({ open: value });
	setTitle = value => this.setState({ title: value });
	setType = value => this.setState({ type: value });
	setIcon = value => this.setState({ icon: value });
	setMessage = value => this.setState({ message: value });
	setCancelAction = value => this.setState({ cancelAction: value });
	setOkAction = value => this.setState({ okAction: value });
	setInputText = value =>
		this.setState({ inputText: value, inputError: false });
	setInputRequired = value => this.setState({ isInputRequired: value });

	handleClose = () => {
		this.setCancelAction(null);
		this.setOkAction(null);
		this.setOpen(false);
		this.setIcon(null);
		this.setInputText("");
		this.setInputRequired(false);
	};

	handleCancelClick = () => {
		const { cancelAction } = this.state;
		if (cancelAction && cancelAction.onPress) {
			cancelAction.onPress();
		}
		this.handleClose();
	};

	handleOkClick = () => {
		const { okAction } = this.state;
		if (okAction && okAction.onPress) {
			if (this.state.isInputRequired) {
				if (this.state.inputText.trim().length === 0) {
					this.setState({ inputError: true });
					return;
				} else {
					okAction.onPress(this.state.inputText);
				}
			} else {
				okAction.onPress();
			}
		}
		this.handleClose();
	};

	handleCrossClick = () => {
		const { cancelAction } = this.state;
		if (cancelAction && cancelAction.onPress) {
			cancelAction.onPress();
		}
		// cross click shouldn't pass the okAction

		// else if (okAction && okAction.onPress) {
		// 	okAction.onPress();
		// }
		this.handleClose();
	};

	openAlert(
		title = INFORMATION,
		message = "",
		actions = [],
		icon = null,
		isInputRequired
	) {
		if (actions.length >= 2) {
			actions.map((item, index) => {
				if (index === 0) {
					this.setCancelAction(item);
				} else if (index === 1) {
					this.setOkAction(item);
				}
			});
		} else if (actions.length === 1) {
			this.setOkAction(actions[0]);
		}
		this.setTitle(title);
		this.setIcon(icon);
		this.setMessage(message);
		this.setOpen(true);
		if (isInputRequired) {
			this.setInputRequired(true);
			this.setInputText("");
		}
	}

	render() {
		const {
			open,
			title,
			message,
			inputText,
			cancelAction,
			okAction,
			icon
		} = this.state;
		const { handleCrossClick } = this;
		return (
			<Modal
				visible={open}
				onRequestClose={handleCrossClick}
				transparent={true}>
				<View
					style={{
						flex: 1,
						justifyContent: "center",
						alignItems: "center",
						backgroundColor: "rgba(0,0,0,0.5)"
					}}>
					<View
						style={{
							width: width / 1.29,
							paddingVertical: height * 0.01,
							borderRadius: width / 50,
							backgroundColor: "#fff"
						}}>
						<View
							style={{
								position: "absolute",
								right: 0,
								top: 0,
								paddingHorizontal: width / 50,
								alignItems: "flex-end"
							}}>
							<TouchableOpacity
								style={{
									padding: width / 100
								}}
								onPress={handleCrossClick}>
								<Icon
									iconType={"material_community_icon"}
									iconName={"close"}
									iconSize={height / 35}
									iconColor={"#2C98F0"}
								/>
							</TouchableOpacity>
						</View>
						{icon ? (
							<Image
								source={icon}
								style={{
									width: width / 2.95,
									height: height / 15.68,
									alignSelf: "center",
									marginVertical: height * 0.01
								}}
								resizeMode={"contain"}
							/>
						) : null}
						{title.length ? (
							<GoappTextBold
								size={height / 55}
								numberOfLines={1}
								style={{
									width: width / 1.83,
									alignSelf: "center",
									textAlign: "center",
									marginVertical: height * 0.01
								}}>
								{title}
							</GoappTextBold>
						) : null}
						{message.length ? (
							<GoappTextMedium
								size={height / 60}
								style={{
									width: width / 1.83,
									alignSelf: "center",
									textAlign: "center",
									color: "#4A4A4A",
									marginVertical: height * 0.01
								}}>
								{message}
							</GoappTextMedium>
						) : null}
						{this.state.isInputRequired && (
							<GoappTextInputRegular
								value={inputText}
								style={{
									borderBottomWidth: 1,
									marginTop: width * 0.07,
									marginBottom: width * 0.1,
									borderColor: this.state.inputError ? "red" : "#979797",
									width: "80%",
									alignSelf: "center"
								}}
								onChangeText={text => this.setInputText(text)}
							/>
						)}
						<View
							style={{
								width: width / 1.83,
								marginTop: height / 32,
								marginBottom: height * 0.01,
								flexDirection: "row",
								alignSelf: "center",
								justifyContent: cancelAction ? "space-between" : "center"
							}}>
							{cancelAction && (
								<TouchableOpacity
									onPress={this.handleCancelClick}
									style={[
										{
											backgroundColor: "#15314C",
											width: width / 4.1,
											height: height / 23.7,
											justifyContent: "center",
											alignItems: "center",
											borderRadius: width / 20,
											...Platform.select({
												android: { elevation: 5 },
												ios: {
													shadowOffset: { width: 1, height: 1 },
													shadowColor: "grey",
													shadowOpacity: 0.2
												}
											})
										},
										cancelAction && cancelAction.buttonStyle
											? cancelAction.buttonStyle
											: {}
									]}>
									<GoappTextMedium
										size={height / 60}
										style={[
											{
												color: "#000000"
											},
											cancelAction && cancelAction.textStyle
												? cancelAction.textStyle
												: {}
										]}
										numberOfLines={1}>
										{cancelAction.text}
									</GoappTextMedium>
								</TouchableOpacity>
							)}
							<TouchableOpacity
								onPress={this.handleOkClick}
								style={[
									{
										backgroundColor: "#15314C",
										width: cancelAction ? width / 4.1 : width / 1.6,
										height: height / 23.7,
										justifyContent: "center",
										alignItems: "center",
										borderRadius: width / 20,
										...Platform.select({
											android: { elevation: 5 },
											ios: {
												shadowOffset: { width: 1, height: 1 },
												shadowColor: "grey",
												shadowOpacity: 0.2
											}
										})
									},
									okAction && okAction.buttonStyle ? okAction.buttonStyle : {}
								]}>
								<GoappTextMedium
									size={height / 60}
									style={[
										{
											color: "#fff"
										},
										okAction && okAction.textStyle ? okAction.textStyle : {}
									]}
									numberOfLines={1}>
									{okAction ? okAction.text : "OK"}
								</GoappTextMedium>
							</TouchableOpacity>
						</View>
					</View>
				</View>
			</Modal>
		);
	}
}

export default connect(
	null,
	null,
	null,
	{ forwardRef: true }
)(CustomAlert);
