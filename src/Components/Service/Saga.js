import { takeLatest, put, call } from "redux-saga/effects";
import Api from "./Api";
import { SERVICES, saveData, clearData } from "../../CustomModules/Offline";

/**
 * Constants
 */
export const MEX_GET_SERVICES = "MEX_GET_SERVICES";
export const MEX_SAVE_SERVICES = "MEX_SAVE_SERVICES";
export const MEX_SERVICES_LOADING = "MEX_SERVICES_LOADING";

/**
 * Action Creators
 */
export const mexGetServices = payload => ({
	type: MEX_GET_SERVICES,
	payload
});

export const mexSaveServices = payload => ({
	type: MEX_SAVE_SERVICES,
	payload
});

export const mexServicesLoading = payload => ({
	type: MEX_SERVICES_LOADING,
	payload
});

/**
 * Root Saga
 */
export default function* servicesSaga(dispatch) {
	yield takeLatest(MEX_GET_SERVICES, handleMexGetServices);
}

/**
 * Handlers
 */
function* handleMexGetServices(action) {
	try {
		const services = yield call(Api.getPartnerServices, action.payload);
		console.log("MexGetServices: ", services);
		if (services.success === 1) {
			// Save Services to AsyncStorage
			saveData(SERVICES, services.data);
			yield put(mexSaveServices(services.data));
		} else {
			clearData(SERVICES);
		}
	} catch (error) {
		console.log("Error in MexGetServices: ", error.message);
		// TODO
	} finally {
		yield put(mexServicesLoading(false));
	}
}
