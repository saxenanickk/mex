import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff"
	},
	header: {
		width: width,
		backgroundColor: "#fff",
		paddingVertical: height / 36,
		paddingHorizontal: width / 18.75,
		borderBottomWidth: 0.5,
		borderColor: "#cfcfcf"
	},
	subHeader: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	headerText: {
		width: width / 3
	},
	siteText: {
		marginRight: width / 50
	},
	siteSelectionButton: {
		flexDirection: "row",
		alignItems: "center"
	},
	loadingContainer: {
		height: height / 1.5,
		justifyContent: "center",
		alignItems: "center"
	},
	mainContainer: {
		flexDirection: "row",
		flexWrap: "wrap",
		marginTop: height / 40,
		marginBottom: height / 10
	},
	noSiteView: {
		height: height / 1.2,
		width: width,
		justifyContent: "center",
		alignItems: "center"
	},
	itemContainer: {
		width: "42%",
		marginVertical: width / 40,
		alignSelf: "flex-start"
	},
	itemContainerOdd: {
		marginLeft: "2%",
		marginRight: "2%"
	},
	itemContainerEven: {
		marginLeft: "5%",
		marginRight: "3%"
	},
	backgroundImage: {
		borderRadius: 10,
		aspectRatio: 76 / 100,
		justifyContent: "flex-end"
	},
	itemTitleContainer: {
		width: "100%",
		paddingVertical: height / 90,
		paddingHorizontal: width / 40,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	itemTitle: { color: "#fff", fontSize: height / 40, width: "80%" }
});

export default styles;
