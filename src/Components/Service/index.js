import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	NativeModules,
	LayoutAnimation,
	Platform,
	Keyboard,
	ScrollView,
	BackHandler
} from "react-native";
import styles from "./style";
import ProgressScreen from "../ProgressScreen";
import FastImage from "react-native-fast-image";
import { GoappTextBold, GoappTextRegular, GoappTextMedium } from "../GoappText";
import { connect } from "react-redux";
import { NoSiteView } from "../NoSiteView";
import MiniApp from "../../CustomModules/MiniApp";
import { mexGetServices } from "./Saga";
import { withOfflineCapability } from "../../CustomModules/Offline/Saga";
import noInternetDialog from "../NoInternetDialog";
import TabNavigationContext from "../../Containers/Goapp/Context/TabNavigationContext";
import DialogContext from "../../DialogContext";

const { width, height } = Dimensions.get("window");

const { UIManager } = NativeModules;

if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}

const App = props => (
	<TouchableOpacity
		activeOpacity={1}
		onPress={() => props.launchApp(props.app)}
		style={[
			styles.itemContainer,
			props.index % 2 === 0 ? styles.itemContainerEven : styles.itemContainerOdd
		]}>
		<FastImage
			source={{
				uri: props.app.image
			}}
			style={styles.backgroundImage}>
			<View style={styles.itemTitleContainer}>
				<GoappTextMedium style={styles.itemTitle} numberOfLines={2}>
					{displayName[props.app.name]}
				</GoappTextMedium>
			</View>
		</FastImage>
	</TouchableOpacity>
);

class Service extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isEditing: false,
			searchText: "",
			selectedSite: null
		};
		this.toggleEditing = this.toggleEditing.bind(this);
		this.changeSearchText = this.changeSearchText.bind(this);
	}

	componentDidMount() {
		if (this.props.selectedSite) {
			this.getServiceApps();
		}
		this._focusunsubscribe = this.props.navigation.addListener("focus", () => {
			BackHandler.addEventListener("hardwareBackPress", this.handleBack);
		});
		this._blurunsubscribe = this.props.navigation.addListener("blur", () => {
			BackHandler.removeEventListener("hardwareBackPress", this.handleBack);
		});
	}

	componentWillUnmount() {
		this._focusunsubscribe();
		this._blurunsubscribe();
	}

	shouldComponentUpdate(props, state) {
		if (props.selectedSite && props.selectedSite !== this.props.selectedSite) {
			this.getServiceApps(props);
		}
		return true;
	}

	handleBack = () => {
		const { navigationOfTab } = this.context;
		navigationOfTab("Home");
		return true;
	};

	getServiceApps = async (props = this.props) => {
		this.fetchServiceApps(props);
	};

	checkIsCorporate() {
		try {
			if (this.props.userProfile && this.props.userProfile.tenantdto) {
				const tenantDto = JSON.parse(this.props.userProfile.tenantdto);
				return (
					tenantDto.tenant_id !== null && tenantDto.tenant_id !== undefined
				);
			} else {
				return false;
			}
		} catch (error) {
			return false;
		}
	}

	fetchServiceApps = ({ appToken, selectedSite }) => {
		this.props.dispatch(
			withOfflineCapability(
				mexGetServices({ appToken, siteId: selectedSite.id })
			)
		);
	};

	toggleEditing = () => {
		if (this.state.isEditing) {
			this.inputRef && this.inputRef.blur();
			Keyboard.dismiss();
		}
		setTimeout(() => {
			LayoutAnimation.easeInEaseOut();
			this.setState({ isEditing: !this.state.isEditing, searchText: "" });
		}, 100);
	};

	changeSearchText = text => {
		this.setState({ searchText: text });
	};

	handleApplicationClick(app, context) {
		if (!this.props.isInternetAvailable) noInternetDialog(context);
		else {
			switch (app.name) {
				case "Hungerbox":
					MiniApp.launchPhoneVerifiedApp(
						app.name,
						this.props.userProfile,
						this.props.navigation
					);
					return;
				case "Ignite":
				case "Vms":
				case "CampusAssist":
					MiniApp.launchCorporateVerifyApp(
						app.name,
						this.props.userProfile,
						this.props.navigation
					);
					return;
				default:
					this.props.navigation.navigate(app.name);
			}
		}
	}

	renderServiceApps = () => {
		try {
			let temp = [];
			// eslint-disable-next-line consistent-this
			let self = this;
			AppList.map(function(item) {
				let index = self.props.configApps.findIndex(
					app => app.name === item.name
				);
				if (index >= 0) {
					temp.push(self.props.configApps[index]);
				}
			});
			if (temp.length > 0) {
				return temp.map((item, index) => (
					<DialogContext.Consumer>
						{context => (
							<App
								app={item}
								key={index}
								index={index}
								launchApp={app => this.handleApplicationClick(app, context)}
							/>
						)}
					</DialogContext.Consumer>
				));
			} else {
				throw new Error("No Service app");
			}
		} catch (error) {
			console.log("error in rendering service apps", error);
			return (
				<NoSiteView
					style={styles.noSiteView}
					message={"No Service Apps Found"}
					isRefresh={true}
					onRefresh={() => this.getServiceApps()}
				/>
			);
		}
	};

	render() {
		return (
			<ScrollView style={styles.container} keyboardShouldPersistTaps={"always"}>
				<View style={styles.header}>
					<View style={styles.subHeader}>
						<GoappTextBold
							color={"#273D52"}
							numberOfLines={1}
							style={styles.headerText}
							size={height / 35}>
							{"Services"}
						</GoappTextBold>
						<View style={styles.siteSelectionButton}>
							<GoappTextRegular
								color={"#777b80"}
								numberOfLines={1}
								style={styles.siteText}
								size={height / 55}>
								{this.props.selectedSite ? this.props.selectedSite.name : ""}
							</GoappTextRegular>
						</View>
					</View>
				</View>
				{this.props.isLoading ? (
					<View style={styles.loadingContainer}>
						<ProgressScreen
							isMessage={true}
							primaryMessage={"Hang On"}
							message={"Getting Service apps"}
							indicatorSize={height / 30}
							indicatorColor={"#2C98F0"}
							messageStyle={{ fontSize: height / 65, width: width / 2.7 }}
							primaryMessageStyles={{
								fontSize: height / 60,
								width: width / 2.7
							}}
						/>
					</View>
				) : (
					<View style={styles.mainContainer}>
						{this.props.configApps && this.props.configApps.length > 0 ? (
							<React.Fragment>{this.renderServiceApps()}</React.Fragment>
						) : (
							<NoSiteView
								style={styles.noSiteView}
								message={"No Service Apps Found"}
								isRefresh={true}
								onRefresh={() => this.getServiceApps()}
							/>
						)}
					</View>
				)}
			</ScrollView>
		);
	}
}

const AppList = [
	{ name: "Traffic", type: "NATIVE" },
	{ name: "FreeWiFi", type: "NATIVE" },
	{ name: "Events", type: "NATIVE" },
	{ name: "Hungerbox", type: "PWA" },
	{ name: "Shuttle", type: "NATIVE" },
	{ name: "Vms", type: "NATIVE" },
	{ name: "Polling", type: "NATIVE" },
	{ name: "Ignite", type: "NATIVE" },
	{ name: "CampusAssist", type: "NATIVE" }
];

const displayName = {
	Traffic: "Live Traffic",
	Shuttle: "Track Shuttle",
	Events: "Events",
	Ignite: "Ignite",
	FreeWiFi: "MEX. WiFi",
	Hungerbox: "MEX. Cafeteria",
	Vms: "Invite Visitors",
	Polling: "Polling",
	CampusAssist: "Campus Assist"
};

Service.contextType = TabNavigationContext;
// Service.contextType = DialogContext;

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		isInternetAvailable: state.netInfo.status,
		location: state.location,
		configApps: state.services.configApps,
		isLoading: state.services.isLoading,
		selectedSite: state.appToken.selectedSite,
		userProfile: state.appToken.userProfile
	};
}

export default connect(mapStateToProps)(Service);
