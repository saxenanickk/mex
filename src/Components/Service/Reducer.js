import {
	MEX_SAVE_SERVICES,
	MEX_SERVICES_LOADING,
	MEX_GET_SERVICES
} from "./Saga";

const initialState = {
	configApps: [],
	isLoading: true
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case MEX_GET_SERVICES:
			return { ...state, configApps: [], isLoading: true };
		case MEX_SAVE_SERVICES:
			return { ...state, configApps: action.payload };
		case MEX_SERVICES_LOADING:
			return {
				...state,
				isLoading: action.payload
			};
		default:
			return state;
	}
};
