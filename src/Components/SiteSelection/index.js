import React, { Component } from "react";
import {
	View,
	Dimensions,
	TouchableOpacity,
	Image,
	SafeAreaView,
	StatusBar,
	FlatList,
	BackHandler
} from "react-native";
import Icon from "../Icon";
import { GoappTextMedium, GoappTextRegular } from "../GoappText";
import ProgressScreen from "../ProgressScreen";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styles from "./style";
import { TICK } from "../../Assets/Img/Image";
import AsyncStorage from "@react-native-community/async-storage";
import { mexSaveSelectedSite } from "../../CustomModules/ApplicationToken/Saga";
import { NoSiteView } from "../NoSiteView";
import Config from "react-native-config";
import MiniApp from "../../CustomModules/MiniApp";
import Dialog from "../Dialog";
import DialogContext from "../../DialogContext";
const { SERVER_BASE_URL_SITES } = Config;
const { width, height } = Dimensions.get("window");

class SiteSelection extends Component {
	constructor(props) {
		super(props);
		this.state = {
			sites: null,
			open: false
		};

		this.handleClose = this.handleClose.bind(this);
		this.handleGoBack = this.handleGoBack.bind(this);
		this.alertRef = React.createRef();
	}

	componentDidMount() {
		BackHandler.addEventListener("hardwareBackPress", this.handleGoBack);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.handleGoBack);
	}

	openSiteSelection = () => {
		this.setState({ open: true, sites: null });
	};

	handleGoBack = () => {
		if (this.props.selectedSite === null) {
			this.alertRef.current.openDialog({
				type: "Information",
				title: "Error",
				message: "You can not use MEX. without selecting your campus."
			});
		} else {
			this.setState({ open: false, sites: null });
		}
		return true;
	};

	handleClose = () => {
		this.setState({ open: false, sites: null });
	};

	shouldComponentUpdate(props, state) {
		if (state.open && state.open !== this.state.open) {
			this.fetchSites();
		}
		return true;
	}
	titleCase = str => {
		return str
			.toLowerCase()
			.split(" ")
			.map(function(word) {
				return word.charAt(0).toUpperCase() + word.slice(1);
			})
			.join(" ");
	};

	getNameAndCity = (name, type) => {
		try {
			let item = name.split("-");
			return type === "city" ? item[0] : item[1];
		} catch (error) {
			return name;
		}
	};

	fetchSites = async () => {
		try {
			this.setState({ sites: null });
			const resp = await this.getAllSites({
				appToken: this.props.appToken
			});
			if (resp.success === 1) {
				let sortedSite = resp.data.sort((a, b) => (b.name > a.name ? -1 : 1));
				this.setState({
					sites: sortedSite
				});
			} else {
				throw new Error("No Sites Found");
			}
		} catch (error) {
			this.setState({ sites: [] });
		}
	};

	getAllSites(params) {
		return new Promise(function(resolve, reject) {
			try {
				fetch(SERVER_BASE_URL_SITES + "/getSites", {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": params.appToken
					}
				})
					.then(response => {
						response
							.json()
							.then(res => {
								resolve(res);
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} catch (error) {
				reject(error);
			}
		});
	}

	async setSelectedSite(site) {
		try {
			this.props.dispatch(mexSaveSelectedSite(site));
			MiniApp.setSiteInfo(site.id, site.ref_site_id, site.name);
			AsyncStorage.setItem("qc_selected_site", JSON.stringify(site));
			this.handleClose();
		} catch (error) {
			console.log("unable to set site", error);
		}
	}

	render() {
		const { open } = this.state;
		const { handleGoBack } = this;
		if (!open) return null;
		return (
			<View onRequestClose={handleGoBack} style={styles.container}>
				<SafeAreaView style={{ flex: 0, backgroundColor: "#ffffff" }} />
				{/** For the Bottom Color (Below the home touch) */}
				<SafeAreaView style={{ flex: 1 }}>
					<StatusBar backgroundColor={"#ffffff"} barStyle={"dark-content"} />
					<View style={styles.header}>
						<TouchableOpacity onPress={handleGoBack} style={styles.backButton}>
							<Icon
								iconColor={"#000"}
								iconType={"ionicon"}
								iconName={"md-arrow-back"}
								iconSize={width / 17}
							/>
						</TouchableOpacity>
						<GoappTextMedium style={styles.headerText}>
							{"Select Your Campus"}
						</GoappTextMedium>
					</View>
					{this.state.sites ? (
						<FlatList
							style={{ height: height }}
							data={this.state.sites}
							keyExtractor={(item, index) => index.toString()}
							ListEmptyComponent={() => (
								<NoSiteView
									style={{
										height: height,
										width: width,
										justifyContent: "center",
										alignItems: "center"
									}}
									isRefresh={true}
									onRefresh={() => this.fetchSites()}
									message={"Unable to get Sites"}
								/>
							)}
							renderItem={({ item }) => {
								return (
									<TouchableOpacity
										style={styles.listItem}
										onPress={() => this.setSelectedSite(item)}>
										{this.props.selectedSite &&
										this.props.selectedSite.id === item.id ? (
											<View style={styles.selectedRow}>
												<GoappTextMedium style={styles.selectedCampus}>
													{this.getNameAndCity(item.name, "campus")}
												</GoappTextMedium>
												<Image
													source={TICK}
													resizeMode={"contain"}
													style={styles.tickImage}
												/>
											</View>
										) : (
											<GoappTextRegular style={styles.campus}>
												{this.getNameAndCity(item.name, "campus")}
											</GoappTextRegular>
										)}
										<GoappTextMedium style={styles.city}>
											{this.getNameAndCity(item.name, "city")}
										</GoappTextMedium>
									</TouchableOpacity>
								);
							}}
						/>
					) : (
						<ProgressScreen
							isMessage={true}
							indicatorSize={height / 30}
							indicatorColor={"#14314c"}
							primaryMessage={"Hang On..."}
							message={"Loading Sites"}
						/>
					)}
				</SafeAreaView>
				<Dialog ref={this.alertRef} />
			</View>
		);
	}
}

SiteSelection.propTypes = {
	//function that should be invoked when selecting sites
	onSiteSelection: PropTypes.func,
	//site which is already selected
	selectedSite: PropTypes.object,
	//apptoken of the user
	appToken: PropTypes.string
};

SiteSelection.defaultProps = {
	selectedSite: null,
	onSiteSelection: site => console.log("selected site", site)
};

function mapStateToProps(state) {
	return {
		appToken: state.appToken.token,
		user: state.appToken.user,
		selectedSite: state.appToken.selectedSite
	};
}
SiteSelection.contextType = DialogContext;

export default connect(
	mapStateToProps,
	null,
	null,
	{ forwardRef: true }
)(SiteSelection);
