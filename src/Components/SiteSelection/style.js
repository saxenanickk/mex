import { Dimensions, StyleSheet } from "react-native";
const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
	container: {
		position: "absolute",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		zIndex: 20,
		flex: 1,
		backgroundColor: "#ffffff"
	},
	header: {
		height: height / 14.5,
		width: width,
		flexDirection: "row",
		alignItems: "center",
		backgroundColor: "#fff",
		paddingHorizontal: width / 17
	},
	backButton: {
		height: height / 14.5,
		justifyContent: "center",
		width: width / 10
	},
	headerText: {
		width: width / 2,
		fontSize: height / 40
	},
	listItem: {
		width: width,
		paddingHorizontal: width / 17,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		height: height / 16
	},
	campus: {
		fontSize: height / 45
	},
	city: {
		fontSize: height / 50,
		color: "#8F96A3"
	},
	selectedCampus: {
		fontSize: height / 45,
		color: "#2C98F0"
	},
	tickImage: {
		width: 15,
		height: 15,
		marginLeft: width / 50
	},
	selectedRow: {
		flexDirection: "row",
		alignItems: "center"
	}
});

export default styles;
