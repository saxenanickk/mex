import React from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import { GoappTextRegular } from "./GoappText";
import { WebView } from "react-native-webview";
import AsyncStorage from "@react-native-community/async-storage";

const { width, height } = Dimensions.get("window");

const TermSheet = props => (
	<View
		style={[
			{
				position: "absolute",
				top: props.open ? 0 : height,
				left: 0,
				right: 0,
				bottom: 0,
				height: height,
				justifyContent: "flex-start",
				alignItems: "center",
				backgroundColor: "rgba(0,0,0,0.5)"
			}
		]}>
		<View
			style={{
				marginTop: height / 20,
				width: width / 1.2,
				height: height / 1.2,
				borderRadius: width / 30,
				backgroundColor: "#fff",
				justifyContent: "space-between"
			}}>
			<View
				style={{
					width: width / 1.2,
					paddingVertical: height / 60,
					justifyContent: "center",
					alignItems: "center",
					backgroundColor: "#2C98F0",
					borderTopLeftRadius: width / 30,
					borderTopRightRadius: width / 30
				}}>
				<GoappTextRegular color={"#fff"}>{props.headerText}</GoappTextRegular>
			</View>
			<WebView
				showsVerticalScrollIndicator={false}
				source={{
					uri: props.uri
				}}
			/>
			<TouchableOpacity
				onPress={() => {
					AsyncStorage.setItem("mex_is_tnc_accepted", "true");
					props.close();
				}}
				style={{
					width: width / 1.2,
					paddingVertical: height / 60,
					justifyContent: "center",
					alignItems: "center",
					backgroundColor: "#2C98F0",
					borderBottomLeftRadius: width / 30,
					borderBottomRightRadius: width / 30
				}}>
				<GoappTextRegular color={"#fff"}>{"Accept & Close"}</GoappTextRegular>
			</TouchableOpacity>
		</View>
	</View>
);

TermSheet.defaultProps = {
	uri: "https://www.mexit.in/service-terms",
	headerText: "Terms & Conditions"
};
export default TermSheet;
