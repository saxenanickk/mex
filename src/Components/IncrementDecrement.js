import React from "react";
import { View, Dimensions, TouchableOpacity, StyleSheet } from "react-native";
import { GoappTextRegular } from "./GoappText";

const { width } = Dimensions.get("window");

const IncrementDecrement = props => {
	let data = props;
	return (
		<View style={styles.container}>
			<TouchableOpacity
				style={styles.button}
				onPress={() => {
					if (data.minValue !== data.value) {
						data.callbackFunction("decrement");
					}
				}}>
				<GoappTextRegular style={styles.operators}>{"-"}</GoappTextRegular>
			</TouchableOpacity>
			<View style={styles.valueContainer}>
				<GoappTextRegular style={styles.value}>{props.value}</GoappTextRegular>
			</View>
			<TouchableOpacity
				style={styles.button}
				onPress={() => {
					if (data.maxValue !== data.value) {
						data.callbackFunction("increment");
					}
				}}>
				<GoappTextRegular style={styles.operators}>{"+"}</GoappTextRegular>
			</TouchableOpacity>
		</View>
	);
};

export default IncrementDecrement;

const styles = StyleSheet.create({
	container: {
		flexDirection: "row",
		padding: width / 50,
		width: width,
		borderWidth: 0
	},
	button: {
		width: width / 8,
		backgroundColor: "#1d4098",
		borderWidth: 0,
		justifyContent: "center",
		alignItems: "center"
	},
	valueContainer: {
		width: width / 8,
		borderWidth: 1,
		borderColor: "#1d4098",
		justifyContent: "center",
		alignItems: "center"
	},
	operators: {
		padding: width / 50,
		color: "#ffffff",
		fontSize: width / 15
	},
	value: {
		borderColor: "#1d4098",
		color: "#1d4098",
		fontSize: width / 20
	}
});
