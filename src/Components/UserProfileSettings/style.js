import { Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
const styles = {
	parent: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		padding: height / 35
	},
	child1: {
		flexDirection: "row",
		alignItems: "center"
	},
	userAvatar: {
		height: height / 8,
		width: width / 4,
		borderRadius: height / 10
	},
	UserIcon: {
		height: height / 29,
		width: width / 18,
		marginRight: width / 21
	},
	featureTitle: {
		color: "#171717",
		fontSize: height / 48,
		paddingLeft: width / 22
	},
	featureTextWidth: {
		width: width / 1.7
	}
};

export default styles;
