import React, { Component } from "react";
import { View, Image } from "react-native";

import { USERAVATAR } from "../../Assets/Img/Image";
import Icon from "../Icon";
import { GoappTextRegular } from "../GoappText";
import styles from "./style";

const data = [
	{ title: "Corporate Verification" },
	{ title: "Notifications" },
	{ title: "Order History" },
	{ title: "Invite friends" },
	{ title: "About MEX." },
	{ title: "Frequently Asked Questions" },
	{ title: "Terms & Conditions" },
	{ title: "Privacy Policy" }
];

class UserProfileSettings extends Component {
	render() {
		console.log(data);
		return (
			<View style={styles.parent}>
				<View style={styles.child1}>
					<Image
						source={USERAVATAR}
						style={styles.UserIcon}
						resizeMode={"contain"}
					/>
					<GoappTextRegular
						numberOfLines={1}
						style={[styles.featureTitle, styles.featureTextWidth]}>
						Edit Personal Information1
					</GoappTextRegular>
				</View>
				<Icon
					iconType={"feather"}
					iconName={"arrow-right"}
					iconSize={30}
					iconColor={"#8F96A3"}
				/>
			</View>
		);
	}
}

export default UserProfileSettings;
