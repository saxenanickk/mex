// @ts-check
import React, { Component } from "react";
import {
	View,
	Dimensions,
	StyleSheet,
	TouchableOpacity,
	ViewStyle,
	TextStyle
} from "react-native";
import Icon from "./Icon";
import { GoappTextRegular, GoappTextMedium } from "./GoappText";
// @ts-ignore
import { font_three } from "../Assets/styles";
const { width, height } = Dimensions.get("window");

/**
 * @typedef {Object} Props
 * @property {Boolean} showIcon
 * @property {ViewStyle} containerStyle
 * @property {Boolean} noSorry
 * @property {String} sorryMessage
 * @property {TextStyle} [sorryText]
 * @property {Boolean} isRefresh
 * @property {String} noRecordFoundMesage
 * @property {TextStyle} [noRecordText]
 * @property {ViewStyle} refreshButton
 * @property {TextStyle} refreshText
 * @property {String} [refreshButtonText]
 * @property {Function} onRefresh
 *
 * @extends {Component<Props, {}>}
 */
export default class EmptyView extends Component {
	/**
	 * @param {Readonly<Props>} props
	 */
	constructor(props) {
		super(props);
	}

	render() {
		const { props } = this;
		return (
			<View style={[styles.container, props.containerStyle]}>
				{props.showIcon ? (
					<Icon
						iconType={"material_community_icon"}
						iconSize={height / 20}
						iconName={"lightbulb-on-outline"}
						iconColor={"#bdbdbd"}
					/>
				) : null}
				{props.noSorry ? null : (
					<GoappTextMedium
						size={height / 45}
						style={[styles.sorryText, props.sorryText]}
						numberOfLines={1}>
						{props.sorryMessage}
					</GoappTextMedium>
				)}
				<GoappTextMedium size={height / 55} style={props.noRecordText}>
					{props.noRecordFoundMesage}
				</GoappTextMedium>
				{props.isRefresh ? (
					<TouchableOpacity
						style={[styles.refreshButton, props.refreshButton]}
						onPress={() => props.onRefresh()}>
						<GoappTextRegular
							size={height / 55}
							style={[styles.refreshText, props.refreshText]}>
							{props.refreshButtonText}
						</GoappTextRegular>
					</TouchableOpacity>
				) : null}
			</View>
		);
	}
}

EmptyView.defaultProps = {
	noSorry: false,
	noRecordFoundMesage: "No Data Found",
	sorryMessage: "Sorry",
	onRefresh: () => console.log("refresh button pressed"),
	isRefresh: true,
	refreshButtonText: "REFRESH",
	showIcon: true
};
const styles = StyleSheet.create({
	container: {
		height: height - height / 4,
		backgroundColor: "#fff",
		justifyContent: "center",
		alignItems: "center",
		overflow: "hidden"
	},
	sorryText: {
		marginTop: height / 60
	},
	refreshButton: {
		width: width * 0.92,
		paddingVertical: height / 60,
		marginTop: height / 7,
		marginBottom: height / 15,
		backgroundColor: "#2C98F0",
		borderRadius: width / 10,
		justifyContent: "center",
		alignItems: "center"
	},
	refreshText: {
		fontFamily: font_three,
		color: "#fff"
	}
});
