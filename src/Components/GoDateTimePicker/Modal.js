import React, { Component } from "react";
import {
	Animated,
	DeviceEventEmitter,
	Dimensions,
	Easing,
	Modal as ReactNativeModal,
	StyleSheet,
	TouchableWithoutFeedback
} from "react-native";

export default class Modal extends Component {
	static defaultProps = {
		onBackdropPress: () => null,
		onHide: () => null,
		isVisible: false
	};
	state = {
		isVisible: this.props.isVisible,
		deviceWidth: Dimensions.get("window").width,
		deviceHeight: Dimensions.get("window").height
	};

	animVal = new Animated.Value(0);
	_isMounted = false;

	componentDidMount() {
		this._isMounted = true;
		if (this.state.isVisible) {
			this.show();
		}
		DeviceEventEmitter.addListener(
			"didUpdateDimensions",
			this.handleDimensionsUpdate
		);
	}

	componentWillUnmount() {
		DeviceEventEmitter.removeListener(
			"didUpdateDimensions",
			this.handleDimensionsUpdate
		);
		this._isMounted = false;
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.props.isVisible && !prevProps.isVisible) {
			this.show();
		} else if (!this.props.isVisible && prevProps.isVisible) {
			this.hide();
		}
	}

	handleDimensionsUpdate = dimensionsUpdate => {
		const deviceWidth = Dimensions.get("window").width;
		const deviceHeight = Dimensions.get("window").height;
		if (
			deviceWidth !== this.state.deviceWidth ||
			deviceHeight !== this.state.deviceHeight
		) {
			this.setState({ deviceWidth, deviceHeight });
		}
	};

	show = () => {
		this.setState({ isVisible: true });
		Animated.timing(this.animVal, {
			easing: Easing.inOut(Easing.quad),
			duration: 300,
			toValue: 1
		}).start();
	};

	hide = () => {
		Animated.timing(this.animVal, {
			easing: Easing.inOut(Easing.quad),
			duration: 300,
			toValue: 0
		}).start(() => {
			if (this._isMounted) {
				this.setState({ isVisible: false }, this.props.onHide);
			}
		});
	};

	render() {
		const { children, onBackdropPress, contentStyle } = this.props;
		const { deviceWidth, deviceHeight, isVisible } = this.state;
		const backdropAnimatedStyle = {
			opacity: this.animVal.interpolate({
				inputRange: [0, 1],
				outputRange: [0, 0.4]
			})
		};

		const contentAnimatedStyle = {
			transform: [
				{
					translateY: this.animVal.interpolate({
						inputRange: [0, 1],
						outputRange: [deviceHeight, 0],
						extrapolate: "clamp"
					})
				}
			]
		};
		return (
			<ReactNativeModal
				onDismiss={this.hide}
				transparent
				animationType="none"
				visible={isVisible}>
				<TouchableWithoutFeedback onPress={onBackdropPress}>
					<Animated.View
						style={[
							styles.backdrop,
							backdropAnimatedStyle,
							// Multiplied by 2 to make sure the backdrop covers the entire
							// screen even while changing orientation
							{ width: deviceWidth * 2, height: deviceHeight * 2 }
						]}
					/>
				</TouchableWithoutFeedback>
				{isVisible && (
					<Animated.View
						style={[styles.content, contentAnimatedStyle, contentStyle]}
						pointerEvents="box-none">
						{children}
					</Animated.View>
				)}
			</ReactNativeModal>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		position: "absolute",
		top: 0,
		left: 0,
		right: 0,
		bottom: 0
	},
	backdrop: {
		position: "absolute",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		backgroundColor: "black",
		opacity: 0
	},
	content: {
		flex: 1,
		justifyContent: "flex-end"
	}
});
