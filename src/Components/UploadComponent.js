/* eslint-disable radix */
import React from "react";
import { View, Dimensions } from "react-native";

export const CLOUDINARY_URL =
	"https://api.cloudinary.com/v1_1/quantified-community-services-private-limited";
export const API_KEY = "544273619848976";
export const API_SECRET = "NqxqnisdK9R3HFVUO0A2bs21NEk";
export const EDIT_UPLOAD_PRESET = "bbjibj1c";
export const CLOUD_NAME = "quantified-community-services-private-limited";

const { width, height } = Dimensions.get("window");

export default class UploadComponent extends React.Component {
	static defaultProps = {
		onFocus: () => {},
		uploadButtonStyle: {
			height: height / 20,
			justifyContent: "center",
			alignItems: "center",
			paddingHorizontal: width / 30
		}
	};
	constructor(props) {
		super(props);
		this.state = {
			fileSize: null,
			uploadedBytes: null,
			isUploading: false,
			isUploadDone: false
		};
		this.uploadBlob = this.uploadBlob.bind(this);
		this.xhr = null;
	}

	componentDidMount() {
		if (this.props.blobBody !== null) {
			this.uploadBlob();
		}
	}

	uploadBlob() {
		let self = this;
		try {
			self.props.onUploadStart();
			if (self.state.isUploading === false) {
				self.setState({ isUploading: true });
			}
			this.xhr = new XMLHttpRequest();
			this.xhr.open(
				"POST",
				`https://${API_KEY}:${API_SECRET}@api.cloudinary.com/v1_1/${CLOUD_NAME}/video/upload`
			);
			this.xhr.responseType = "json";
			this.xhr.send(self.props.blobBody);
			this.xhr.onload = function() {
				if (self.xhr.status !== 200) {
					console.log("status not ok", self.xhr);
					self.props.onUploadFail("Unable to upload the file");
				} else {
					let responseObj = self.xhr.response;
					console.log("response object is", responseObj);
					self.props.onUploadSuccess(responseObj);
					self.setState({ isUploadDone: true });
				}
			};
			this.xhr.upload.onprogress = function(event) {
				self.getPercentage(event.total, event.loaded);
			};
			this.xhr.onerror = function(error) {
				console.log("response is", error);
				self.props.onUploadFail("Unable to upload the file");
				self.setState({ isUploading: false });
			};
		} catch (error) {
			console.log("response of error is", error);
			self.props.onUploadFail("Unable to upload the file");
			self.setState({ isUploading: false });
		}
	}

	getPercentage = (fileSize, uploadedBytes) => {
		try {
			if (fileSize === null || uploadedBytes === null) {
				throw Error("INVALID FILE SIZE");
			}
			let per = (uploadedBytes * 100) / fileSize;
			this.props.onProgressUpdate(parseInt(per));
		} catch (error) {
			this.props.onProgressUpdate(0);
		}
	};

	shouldComponentUpdate(props, state) {
		if (props.fileURI && props.fileURI !== this.props.fileURI) {
			this.setState({
				fileSize: null,
				uploadedBytes: null,
				isUploading: false,
				isUploadDone: false
			});
			if (this.xhr) {
				this.xhr.abort();
			}
			if (props.blobBody) {
				this.uploadBlob();
			}
		}
		return true;
	}

	componentWillUnmount() {
		if (this.xhr) {
			this.xhr.abort();
		}
	}

	render() {
		return <View />;
	}
}
