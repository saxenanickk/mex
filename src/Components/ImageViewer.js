import React, { Component } from "react";
import { View, TouchableOpacity, Dimensions, Platform } from "react-native";
import FastImage from "react-native-fast-image";
import ImageZoom from "react-native-image-pan-zoom";
import Icon from "./Icon";
const { width, height } = Dimensions.get("window");

const ImageViewer = props =>
	props.open ? (
		<View
			style={{
				position: "absolute",
				top: 0,
				left: 0,
				bottom: 0,
				right: 0,
				height: height,
				backgroundColor: "rgba(0,0,0,0.5)",
				justifyContent: "center",
				alignItems: "center"
			}}>
			{props.isZoom ? (
				<ImageZoom
					cropWidth={width}
					cropHeight={height}
					imageWidth={width}
					imageHeight={height}>
					<FastImage
						source={{ uri: props.image }}
						style={{ width: width, height: height }}
						resizeMode={Platform.OS === "ios" ? "contain" : "center"}
					/>
				</ImageZoom>
			) : (
				<FastImage
					source={{ uri: props.image }}
					style={{ width: height / 2, height: height / 2 }}
					resizeMode={"contain"}
				/>
			)}

			<TouchableOpacity
				onPress={() => props.close()}
				style={{
					alignSelf: "center",
					zIndex: 100
				}}>
				<Icon
					iconName={"ios-close-circle"}
					iconType={"ionicon"}
					iconColor={"#fff"}
					iconSize={height / 20}
				/>
			</TouchableOpacity>
		</View>
	) : null;

export default ImageViewer;
