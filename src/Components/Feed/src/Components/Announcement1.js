import React, { Component } from "react";
import { TouchableOpacity, Image, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export default class Announcement1 extends Component {
	render() {
		const { onPress, announcements_1 } = this.props;

		return (
			<TouchableOpacity
				activeOpacity={1}
				onPress={onPress}
				style={{
					marginHorizontal: width / 20,
					marginVertical: height / (15.8 * 2.3) / 2.5
				}}>
				<Image
					defaultSource={require("../assets/default_image.png")}
					source={{
						uri: announcements_1[0].images[0]
					}}
					style={styles.offerImageStyle}
				/>
			</TouchableOpacity>
		);
	}
}

const styles = {
	offerImageStyle: {
		width: width * 0.9,
		aspectRatio: 1.9 / 1,
		borderRadius: 16
	}
};
