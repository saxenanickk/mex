import React, { Component } from "react";
import { View, Image } from "react-native";
import { FeedTextBold } from "./FeedText";
import { NOOFFER } from "../../../../Containers/Offers/Assets/Img";
import styles from "./styles";

export default class ErrorView extends Component {
	render() {
		return (
			<View style={styles.errorContainer}>
				<Image
					source={NOOFFER}
					style={styles.errorImage}
					resizeMode={"contain"}
				/>
				<View>
					<FeedTextBold style={styles.errorText}>
						{"Oops... No Feeds Found"}
					</FeedTextBold>
				</View>
			</View>
		);
	}
}
