import React from "react";
import { Text, Platform } from "react-native";

export const FeedTextLight = props => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : "LatoLight",
				fontWeight: Platform.OS === "ios" ? "300" : "normal",
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

export const FeedTextRegular = props => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : "LatoRegular",
				fontWeight: Platform.OS === "ios" ? "400" : "normal",
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

export const FeedTextMedium = props => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : "LatoMedium",
				fontWeight: Platform.OS === "ios" ? "500" : "normal",
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

export const FeedTextBold = props => (
	<Text
		style={[
			{
				fontFamily: Platform.OS === "ios" ? "Avenir" : "LatoBold",
				fontWeight: Platform.OS === "ios" ? "700" : "bold",
				fontSize: props.size,
				color: props.color
			},
			props.style
		]}
		numberOfLines={props.numberOfLines}
		ellipsizeMode={props.ellipsizeMode}>
		{props.children}
	</Text>
);

const defaultProps = {
	size: null,
	color: "#000",
	editable: true
};

FeedTextBold.defaultProps = defaultProps;
FeedTextLight.defaultProps = defaultProps;
FeedTextMedium.defaultProps = defaultProps;
FeedTextRegular.defaultProps = defaultProps;
