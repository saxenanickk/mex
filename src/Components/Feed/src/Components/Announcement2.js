import React, { Component } from "react";
import { View, Image, Dimensions, TouchableOpacity } from "react-native";

const { width, height } = Dimensions.get("window");

export default class Announcement2 extends Component {
	render() {
		const { announcements_2, onPress } = this.props;

		if (announcements_2.length) {
			return (
				<View
					style={{
						flexDirection: "row",
						marginVertical: height / (15.8 * 2.3) / 2.5 - width * 0.01,
						justifyContent: "center"
					}}>
					<View>
						<TouchableOpacity
							onPress={() =>
								onPress(
									announcements_2[0].banner_sub_type_name,
									announcements_2[0].additional_field
								)
							}>
							<Image
								style={{
									margin: width * 0.02,
									width: width * 0.43,
									aspectRatio: 0.64,
									borderRadius: 16
								}}
								source={{
									uri: announcements_2[0].images[0]
								}}
							/>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() =>
								onPress(
									announcements_2[1].banner_sub_type_name,
									announcements_2[1].additional_field
								)
							}>
							<Image
								style={{
									margin: width * 0.02,
									width: width * 0.43,
									aspectRatio: 1.28,
									borderRadius: 16
								}}
								source={{
									uri: announcements_2[1].images[0]
								}}
							/>
						</TouchableOpacity>
					</View>
					<View>
						<TouchableOpacity
							onPress={() =>
								onPress(
									announcements_2[3].banner_sub_type_name,
									announcements_2[3].additional_field
								)
							}>
							<Image
								style={{
									margin: width * 0.02,
									width: width * 0.43,
									aspectRatio: 1.28,
									borderRadius: 16
								}}
								source={{
									uri: announcements_2[3].images[0]
								}}
							/>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() =>
								onPress(
									announcements_2[2].banner_sub_type_name,
									announcements_2[2].additional_field
								)
							}>
							<Image
								style={{
									margin: width * 0.02,
									width: width * 0.43,
									aspectRatio: 0.64,
									borderRadius: 16
								}}
								source={{
									uri: announcements_2[2].images[0]
								}}
							/>
						</TouchableOpacity>
					</View>
				</View>
			);
		} else {
			return null;
		}
	}
}
