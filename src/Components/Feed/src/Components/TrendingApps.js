import React, { Component } from "react";
import { View, Dimensions, Platform } from "react-native";
import { FeedTextMedium } from "./FeedText";
import AppIcon from "./AppIcon";

const { width, height } = Dimensions.get("window");

export default class TrendingApps extends Component {
	render() {
		const { trendingApps, onPress } = this.props;

		if (trendingApps.length) {
			return (
				<View style={styles.trendingAppsSection}>
					<FeedTextMedium size={height / 60}>
						{"Most Popular Apps"}
					</FeedTextMedium>
					<View style={styles.trendingApps}>
						{trendingApps.map((item, index) =>
							// ToDo: Remove this line once BBInstant will up for iOS
							Platform.OS === "ios" && item.app_name === "BBInstant" ? null : (
								<AppIcon
									key={index}
									icon={{ uri: item.app_icon }}
									title={
										item.app_name === "Hungerbox" ? "MEX. Cafe" : item.app_name
									}
									titleStyle={{ color: "#000000" }}
									onPress={() => onPress(item)}
								/>
							)
						)}
					</View>
				</View>
			);
		} else {
			return null;
		}
	}
}

const styles = {
	trendingApps: {
		flex: 1,
		width: width * 0.85,
		alignSelf: "center",
		borderRadius: 16,
		backgroundColor: "#fff",
		flexDirection: "row",
		justifyContent: "flex-start",
		alignItems: "center"
	},
	trendingAppsSection: {
		width: width * 0.9,
		marginVertical: height / (15.8 * 2.3) / 2,
		borderRadius: 16,
		alignSelf: "center",
		paddingVertical: height / 50,
		backgroundColor: "#fff",
		borderWidth: 1,
		borderColor: "#E9E9E9",
		paddingHorizontal: width / 30,
		aspectRatio: 2.7 / 1
	}
};
