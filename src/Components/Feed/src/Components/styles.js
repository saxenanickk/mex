import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	errorContainer: {
		justifyContent: "center",
		alignItems: "center",
		flex: 1,
		backgroundColor: "#ffffff",
		height: height / 1.5
	},
	errorImage: { width: width / 2, height: width / 2 },
	errorText: {
		fontSize: height / 40,
		marginTop: 25,
		color: "#000"
	},
	loadingContainer: {
		justifyContent: "center",
		alignItems: "center",
		flex: 1,
		height: height / 1.35,
		backgroundColor: "#ffffff"
	},
	loadingImageContainer: {
		width: height / 6,
		height: height / 10,
		justifyContent: "flex-end",
		alignItems: "center",
		marginBottom: 5
	}
});

export default styles;
