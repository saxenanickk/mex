import React, { Component } from "react";
import {
	TouchableOpacity,
	Dimensions,
	ScrollView,
	Platform
} from "react-native";
import GoImage from "../../../GoImage";

const { width, height } = Dimensions.get("window");

export default class Benefit extends Component {
	constructor(props) {
		super(props);
		// To ensure that onScroll event gets fired only once
		this.onScrollFlag = true;
		// To hold the autoPlay interval
		this.autoPlayInterval = null;
		// To hold the scrollRefCheck interval
		this.scrollRefCheckInterval = null;
		this.autoPlayTimeout = null;
		// Current item in focus
		this.currentIndex = 1;
		// Offset on x - axis. sed for snapping one item
		this.scrollX = width - width * 0.025 - width * 0.05;
		this.scrollOffset = this.scrollX;
	}

	componentDidMount() {
		this.autoPlay();
	}

	autoPlay = () => {
		if (this.scrollRef && this.scrollRef.scrollTo) {
			clearInterval(this.scrollRefCheckInterval);
			this.autoPlayInterval = setInterval(() => {
				this.onScrollFlag = true;
				this.scrollRef.scrollTo(
					{
						x: this.scrollOffset,
						y: 0
					},
					true
				);
			}, 3000);
		} else {
			clearInterval(this.scrollRefCheckInterval);
			this.scrollRefCheckInterval = null;
			this.scrollRefCheckInterval = setInterval(() => {
				this.autoPlay();
			}, 2000);
		}
	};

	componentWillUnmount() {
		clearInterval(this.autoPlayInterval);
	}

	_onMomentumScrollEnd = event => {
		clearTimeout(this.autoPlayTimeout);

		let tempOffset = this.scrollX + event.nativeEvent.contentOffset.x;
		let tempIndex = Math.round(
			event.nativeEvent.contentOffset.x / this.scrollX
		);

		// Condition to go back to the first element
		if (
			tempIndex >= this.props.benefits.length - 1 ||
			this.currentIndex >= this.props.benefits.length - 1
		) {
			this.currentIndex = 0;
			this.scrollOffset = 0;
		} else {
			// Next Offset to Scroll and increase the currentIndex
			this.currentIndex = Math.round(tempOffset / this.scrollX);
			this.scrollOffset = tempOffset;
		}
		if (!this.autoPlayInterval) {
			this.autoPlayTimeout = setTimeout(() => this.autoPlay(), 3000);
		}
	};

	/**
	 * To cancel the autoplay when scrollview is touched
	 */
	_onTouchStart = () => {
		clearInterval(this.autoPlayInterval);
		this.autoPlayInterval = null;
	};

	/**
	 * Only for Android.
	 * onMomentumScrollEnd only gets fired when scrolling via touch.
	 */
	_onScroll =
		Platform.OS === "ios"
			? undefined
			: () => {
					if (this.onScrollFlag) {
						this.onScrollFlag = false;

						// Condition to go back to the first element
						if (this.currentIndex >= this.props.benefits.length - 1) {
							this.currentIndex = 0;
							this.scrollOffset = 0;
						} else {
							// Next Offset to Scroll and increase the currentIndex
							this.scrollOffset = this.scrollX * ++this.currentIndex;
						}
					}
			  };

	render() {
		const { benefits, handleItemClick } = this.props;

		if (benefits.length) {
			return (
				<ScrollView
					onLayout={() => console.log("Hello")}
					ref={ref => (this.scrollRef = ref)}
					onTouchStart={this._onTouchStart}
					onMomentumScrollEnd={this._onMomentumScrollEnd}
					disableIntervalMomentum={true}
					snapToStart={true}
					onScroll={this._onScroll}
					bounces={true}
					bouncesZoom={true}
					scrollEventThrottle={100}
					pagingEnabled={true}
					contentContainerStyle={styles.offerList}
					showsHorizontalScrollIndicator={false}
					snapToInterval={this.scrollX}
					horizontal={true}>
					{benefits.map((item, index) => (
						<TouchableOpacity
							style={[
								styles.bannerContainer,
								index === 0 ? styles.marginLeft : {},
								index === benefits.length - 1
									? styles.marginRightLast
									: styles.marginRight
							]}
							key={index}
							activeOpacity={1}
							onPress={() =>
								handleItemClick(
									item.banner_sub_type_name,
									item.additional_field
								)
							}>
							<GoImage
								useFastImage={true}
								defaultSource={require("../assets/default_image.png")}
								source={{ uri: item.images[0] }}
								style={styles.offerImageStyle}
								resizeMode="contain"
							/>
						</TouchableOpacity>
					))}
				</ScrollView>
			);
		} else {
			return null;
		}
	}
}

const styles = {
	offerList: {
		marginTop: height / (15.8 * 2.3),
		marginBottom: height / (15.8 * 2.3) / 2.5
	},
	bannerContainer: { alignSelf: "center" },
	marginLeft: { marginLeft: width * 0.05 },
	marginRightLast: { marginRight: width * 0.05 },
	marginRight: { marginRight: width * 0.025 },
	offerImageStyle: {
		width: width * 0.9,
		aspectRatio: 1.72 / 1,
		borderRadius: 16
	}
};
