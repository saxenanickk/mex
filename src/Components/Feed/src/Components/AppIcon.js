import React from "react";
import {
	TouchableOpacity,
	Image,
	Dimensions,
	StyleSheet,
	View
} from "react-native";
import { FeedTextRegular } from "./FeedText";

const { width, height } = Dimensions.get("window");

const AppIcon = props => {
	return (
		<TouchableOpacity onPress={props.onPress} style={styles.container}>
			<Image source={props.icon} style={styles.iconStyle} />
			<FeedTextRegular
				style={styles.titleStyle}
				numberOfLines={1}
				ellipsizeMode={"tail"}>
				{props.title}
			</FeedTextRegular>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	container: {
		width: width / 5,
		alignItems: "center",
		justifyContent: "center",
		aspectRatio: 1 / 1
	},
	titleStyle: {
		fontSize: height / 80,
		color: "#000000"
	},
	iconStyle: {
		borderColor: "#6f6f6f",
		borderRadius: height / 56.2,
		width: height / 16,
		aspectRatio: 1 / 1
	}
});

export default AppIcon;
