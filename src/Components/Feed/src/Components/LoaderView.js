import React, { Component } from "react";
import { Animated, View } from "react-native";
import { FeedTextMedium } from "./FeedText";
import DotsLoader from "./DotsLoader";
import styles from "./styles";

export default class LoaderView extends Component {
	constructor(props) {
		super(props);
		this.AnimatedValue = new Animated.Value(0);
	}

	componentDidMount() {
		this.expand();
	}

	expand = () => {
		Animated.timing(this.AnimatedValue, {
			toValue: 1,
			duration: 500
		}).start(() => {
			this.props.isLoading || this.props.isTick ? this.shrink() : null;
		});
	};

	shrink = () => {
		Animated.timing(this.AnimatedValue, {
			toValue: 0,
			duration: 500
		}).start(() => {
			this.props.isLoading || this.props.isTick ? this.expand() : null;
		});
	};

	render() {
		const animatedWidth = this.AnimatedValue.interpolate({
			inputRange: [0, 1],
			outputRange: [105, 120]
		});
		const animatedHeight = this.AnimatedValue.interpolate({
			inputRange: [0, 1],
			outputRange: [60, 70]
		});

		return (
			<View style={styles.loadingContainer}>
				{this.props.isLoading ? (
					<FeedTextMedium>{"Loading MEX."}</FeedTextMedium>
				) : (
					<FeedTextMedium>{""}</FeedTextMedium>
				)}
				<View style={styles.loadingImageContainer}>
					{this.props.isLoading ? (
						<Animated.Image
							style={{ width: animatedWidth, height: animatedHeight }}
							source={require("../assets/cloud.png")}
						/>
					) : (
						<Animated.Image
							style={{
								width: animatedWidth,
								height: animatedHeight
							}}
							source={require("../assets/cloud_tick.png")}
						/>
					)}
				</View>
				{this.props.isLoading ? (
					<DotsLoader color={"#1f455e"} />
				) : (
					<FeedTextMedium>{""}</FeedTextMedium>
				)}
			</View>
		);
	}
}
