import React from "react";
import {
	FlatList,
	SafeAreaView,
	View,
	Dimensions,
	Platform,
	LayoutAnimation,
	UIManager,
	Image,
	TouchableOpacity
} from "react-native";
import Geolocation from "@react-native-community/geolocation";
import styles from "./styles";
import FeedItem from "./FeedItem";
import FeedApi from "./Api";
import RNCalendarEvents from "react-native-calendar-events";
import { getPwaLink, getService } from "./Utils/Constants";
import Benefit from "./Components/Benefit";
import Announcement1 from "./Components/Announcement1";
import TrendingApps from "./Components/TrendingApps";
import Announcement2 from "./Components/Announcement2";
import Announcement3 from "./Components/Announcement3";
import LoaderView from "./Components/LoaderView";
import ErrorView from "./Components/ErrorView";
import Icon from "../../Icon";
import { connect } from "react-redux";
import {
	GoappTextRegular,
	GoappTextBold,
	GoappTextMedium
} from "../../GoappText";
import { VerifyPhoneFlag } from "../../VerifyPhoneFlag";
import ProgressScreen from "../../ProgressScreen";
import { EMPTYIMAGE } from "../../../Assets/Img/Image";
import { NoSiteView } from "../../NoSiteView";
import { GoappAlertContext } from "../../../GoappAlertContext";
import GoAppAnalytics from "../../../utils/GoAppAnalytics";
import Branch from "react-native-branch";
import { getUrlParams } from "../../../utils/getUrlParams";
import { AppList } from "../../../Containers/Goapp/AppList";
import {
	mexGetContext,
	mexGetBanners,
	mexGetTrendingApps,
	mexSaveBanners,
	mexSaveTrendingApps,
	mexSaveContext
} from "../Saga";
import { withOfflineCapability } from "../../../CustomModules/Offline/Saga";
import { mexGetProfile } from "../../../CustomModules/ApplicationToken/Saga";
import { SiteSelectionContext } from "../../SiteSelection/SiteSelectionContext";

const { width, height } = Dimensions.get("window");

if (Platform.OS === "android") {
	UIManager.setLayoutAnimationEnabledExperimental &&
		UIManager.setLayoutAnimationEnabledExperimental(true);
}

// eslint-disable-next-line no-unused-vars
let SITE_ID = null;

class Feed extends React.Component {
	pickup_lat = null;
	pickup_lng = null;
	constructor(props) {
		super(props);
		this.state = {
			currentItem: 0,
			isLoading: true,
			isTick: true,
			isError: false,
			isProfileFetchError: false,
			showTutorial: false,
			images: []
		};
		this.flag = 0;
		this.siteRef = null;
	}

	onViewableItemsChanged = ({ viewableItems }) => {
		if (viewableItems[0].index !== this.state.currentItem) {
			LayoutAnimation.easeInEaseOut();
			this.setState({ currentItem: viewableItems[0].index });
		}
	};

	// Get Context based Feed
	getContext = async (latitude = null, longitude = null, token = null) => {
		try {
			if (token) {
				let PARAMS = {};
				if (latitude && longitude) {
					PARAMS = {
						activity_lat: latitude,
						activity_lng: longitude
					};
				}
				PARAMS = { ...PARAMS, token };
				this.props.dispatch(withOfflineCapability(mexGetContext(PARAMS)));
			}
		} catch (error) {
			// Error case handling
			// ToDo
			console.log(error);
		}
	};

	// Calendar
	getCalendarAuthorizationStatus = async () => {
		try {
			let calendarAuthStatus = await RNCalendarEvents.authorizationStatus();
			if (calendarAuthStatus === "authorized") {
				this._fetchAllEvents();
			} else {
				this._requestCalendarPermissions();
			}
		} catch (error) {
			console.log("Failed to get Calendar Status");
		}
	};

	// Calendar
	_requestCalendarPermissions = async () => {
		try {
			let requestCalendarPermission = await RNCalendarEvents.authorizeEventStore();
			console.log(requestCalendarPermission);
			if (requestCalendarPermission === "authorized") {
				this._fetchAllEvents();
			}
		} catch (error) {
			console.log(error, "requesting error");
		}
	};

	// Calendar
	_fetchAllEvents = async () => {
		try {
			let date = new Date();
			let allEvents = await RNCalendarEvents.fetchAllEvents(
				date.toISOString(),
				new Date(date.getTime() + 7 * 24 * 60 * 60 * 1000).toISOString()
			);
			FeedApi.saveCalendarEvents({ token: this.props.token, data: allEvents });
		} catch (error) {}
	};

	componentDidMount() {
		this.getUserProfile();
		// Fetch Location
		Geolocation.getCurrentPosition(
			position => {
				this.pickup_lat = position.coords.latitude;
				this.pickup_lng = position.coords.longitude;
			},
			error => {
				console.log(error);
			}
		);
		this.getCalendarAuthorizationStatus();
		if (this.props.token) {
			this.getContext(this.pickup_lat, this.pickup_lng, this.props.token);
		}
		if (this.props.token && this.props.siteId) {
			SITE_ID = this.props.siteId;
			this.getFeedContent(this.props.token, this.props.siteId);
		}
	}

	getMiniUserInfo = userProfile => {
		return new Promise(function(resolve) {
			try {
				let obj = {};
				obj.contact = userProfile.phonenum;
				obj.email = userProfile.email;
				obj.name = userProfile.name;
				obj.site_id = userProfile.site_info
					? userProfile.site_info.site_id
					: "";
				obj.site_name = userProfile.site_info ? userProfile.site_info.name : "";
				obj.tenant_id = userProfile.tenant_id;
				obj.tenant_name = null;
				resolve(obj);
			} catch (error) {
				resolve({
					contact: "",
					email: "",
					name: "",
					site_id: "",
					site_name: "",
					tenant_id: "",
					tenant_name: ""
				});
			}
		});
	};

	getUserProfile = () => {
		if (!this.props.isLoading) {
			this.props.dispatch(
				withOfflineCapability(mexGetProfile({ appToken: this.props.token }))
			);
		}
	};

	subscribeToBranch = async () => {
		try {
			// Branch.initSessionTtl = 10000 // Set to 10 seconds
			this._unsubscribeFromBranch = Branch.subscribe(({ error, params }) => {
				if (error) {
					console.log("Error from Branch: " + error);
					return;
				}
				this.readBranchParams(params);
			});
		} catch (error) {
			console.log(error);
		}
	};

	readBranchParams = async params => {
		// let lastParams = await Branch.getLatestReferringParams() // params from last open
		// let installParams = await Branch.getFirstReferringParams() // params from original install
		// console.log(lastParams, installParams)
		console.log(params);

		if (params["+non_branch_link"]) {
			// Route non-Branch URL if appropriate.
			return;
		}

		if (!params["+clicked_branch_link"]) {
			// Indicates initialization success and some other conditions.
			// No link was opened.
			return;
		}

		// A Branch link was opened.
		// Route link based on data in params, e.g.

		// Get title and url for route
		const title = params.$og_title;
		const url = params.$canonical_url;
		const image = params.$og_image_url;
		console.log(title, url, image);
		if (params["~referring_link"]) {
			// Navigate based on params
			let queryParams = getUrlParams(params["~referring_link"]);
			if (queryParams.app) {
				let appIndex = AppList.findIndex(app => app.name === queryParams.app);
				if (appIndex > -1) {
					this.onAppClick(AppList[appIndex], queryParams);
				}
			}
			return;
		}
	};

	static getDerivedStateFromProps(nextProps, prevState) {
		if (
			(nextProps.availableFeed.length ||
				nextProps.benefits.length ||
				nextProps.announcements_1.length ||
				nextProps.announcements_2.length ||
				nextProps.announcements_3.length ||
				nextProps.trendingApps.length) &&
			(prevState.isLoading || !prevState.isError)
		) {
			return { isLoading: false, isTick: false };
		}
		if (nextProps.failure.length === 3 && !prevState.isError) {
			return { isLoading: false, isTick: false, isError: true };
		}
		return null;
	}

	shouldComponentUpdate(props, state) {
		if (props.userProfile && this.props.userProfile === null) {
			const { userdevices, email, phonenum, name } = props.userProfile;
			const userId = JSON.parse(userdevices)[0].userId;
			const userProps = { email, phone: phonenum, name: name };
			if (userId) {
				// GoAppAnalytics.init(`${userId}`, userProps);
				Branch.setIdentity(`${userId}`);
				this.subscribeToBranch();
			}
		}
		if (props.token && props.token !== this.props.token) {
			this.getContext(this.pickup_lat, this.pickup_lng, props.token);
		}
		if (props.token && props.token !== this.props.token && this.props.siteId) {
			SITE_ID = props.siteId;
			this.getFeedContent(props.token, props.siteId);
		} else if (this.props.token && props.siteId && this.props.siteId === null) {
			SITE_ID = props.siteId;
			this.getFeedContent(props.token, props.siteId);
		}
		if (
			this.props.token &&
			props.siteId &&
			this.props.siteId !== props.siteId
		) {
			SITE_ID = props.siteId;
			this.getFeedContent(props.token, props.siteId);
		}
		if (props.isRefresh && !this.state.isLoading) {
			this.refreshData(props);
			this.props.resetRefresh();
		}
		if (props.selectedSite && props.selectedSite !== this.props.selectedSite) {
			this.refreshData(props);
		}
		return true;
	}

	componentWillUnmount() {
		if (this._unsubscribeFromBranch) {
			this._unsubscribeFromBranch();
			this._unsubscribeFromBranch = null;
		}
	}

	refreshData(props = this.props) {
		try {
			this.props.dispatch(mexSaveContext([]));
			this.props.dispatch(
				mexSaveBanners({
					BENEFIT: [],
					ANNOUNCEMENT_4: [],
					ANNOUNCEMENT_2: [],
					ANNOUNCEMENT_3: []
				})
			);
			this.props.dispatch(mexSaveTrendingApps([]));
			this.getContext(this.pickup_lat, this.pickup_lng, props.token);
			this.getFeedContent(props.token, props.siteId);
		} catch (error) {
			console.log("error in refreshing ", error);
		}
	}

	renderItem = data => {
		return (
			<FeedItem
				{...data}
				length={this.state.availableFeed ? this.state.availableFeed.length : 0}
				handleFeedClick={this.handleFeedClick}
			/>
		);
	};

	createQueryParams(params) {
		let QUERY_PARAMS = "";
		Object.keys(params).map(item => {
			if (QUERY_PARAMS.indexOf("?") < 0) {
				QUERY_PARAMS = `${QUERY_PARAMS}?${item}=${params[item]}`;
			} else {
				QUERY_PARAMS = `${QUERY_PARAMS}&${item}=${params[item]}`;
			}
		});
		return QUERY_PARAMS;
	}

	createDeepLink(params) {
		return JSON.stringify(params);
	}

	handleFeedClick = (app, item) => {
		let PARAMS = {};
		if (app === "ola") {
			this.props.launchApp("Ola");
		} else if (app === "uber") {
			this.props.launchApp("Uber");
		} else if (app === "events") {
			item &&
				item.information &&
				item.information.event_details &&
				item.information.event_details.product &&
				item.information.event_details.product.event_id &&
				this.handleEventsBannerClick(
					item.information.event_details.product.event_id
				);
		} else if (app === "cleartrip") {
			let temp = new Date(item.information.trip_date_time);
			PARAMS = {
				type: "searchflight",
				destination: item.information.city_code,
				departDate: `${temp.getFullYear()}-${
					temp.getMonth() < 9
						? "0" + (temp.getMonth() + 1)
						: temp.getMonth() + 1
				}-${temp.getDate()}`
			};
			const deepLink = this.createDeepLink(PARAMS);
			this.props.launchAppWithLink("Cleartrip", deepLink);
		} else if (app === "cleartrip_hotel") {
			const deepLink = this.createDeepLink(PARAMS);
			this.props.launchAppWithLink("CleartripHotel", deepLink);
		} else if (app === "bookmyshow") {
			this.props.launchWebApp("BookMyShow", "https://in.bookmyshow.com/");
		} else if (app === "swiggy") {
			this.props.launchApp("Swiggy");
		}
	};

	handleEventsBannerClick = id => {
		let PARAMS = {
			event_id: id
		};
		const deepLink = this.createDeepLink(PARAMS);
		this.props.launchAppWithLink("Events", deepLink);
	};

	handleMexOffersBannerClick = id => {
		let PARAMS = {
			offer_id: id
		};
		const deepLink = this.createDeepLink(PARAMS);
		this.props.launchAppWithLink("Offers", deepLink);
	};

	viewabilityConfig = { viewAreaCoveragePercentThreshold: 50 };

	async getFeedContent(token, siteId) {
		try {
			this.setState({ isLoading: true, isError: false });
			if (siteId !== null) {
				this.props.dispatch(
					withOfflineCapability(
						mexGetBanners({
							site_id: siteId,
							token: token
						})
					)
				);
				this.props.dispatch(
					withOfflineCapability(
						mexGetTrendingApps({
							site_id: siteId,
							token: token
						})
					)
				);
			} else {
				throw new Error("Unable to get data");
			}
		} catch (error) {
			this.setState({ isError: true, isLoading: false, isTick: false });
		}
	}

	/**
	 *
	 * @param bannerSubType Enum
	 * @param additionalFields Object
	 */
	handleItemClick = (type, bannerSubType, additionalFields) => {
		if (additionalFields) {
			switch (bannerSubType) {
				case "COMMUNITY":
					this.setAnalytics("banner_click", { type, sub_type: "community" });
					this.props.launchApp("Community");
					break;
				case "EVENTS":
					this.setAnalytics("banner_click", { type, sub_type: "events" });
					this.handleEventsBannerClick(additionalFields.id);
					break;
				case "MARKET_PLACE":
					this.setAnalytics("banner_click", { type, sub_type: "market_place" });
					this.props.launchApp(additionalFields.app_name);
					break;
				case "MEX_OFFERS":
					this.setAnalytics("banner_click", { type, sub_type: "mex_offers" });
					this.handleMexOffersBannerClick(additionalFields.id);
					break;
				case "OFFERS":
					this.setAnalytics("banner_click", { type, sub_type: "offers" });
					this.handleMexOffersBannerClick(additionalFields.mex_offer_id);
					break;
				case "PWA":
					this.setAnalytics("banner_click", { type, sub_type: "pwa" });
					this.props.launchWebApp(
						additionalFields.app_name,
						getPwaLink(additionalFields.app_name)
					);
					break;
				case "SERVICE":
					this.setAnalytics("banner_click", { type, sub_type: "service" });
					this.props.launchService(getService(additionalFields.app_name));
					break;
				case "TUTORIAL":
					this.setAnalytics("banner_click", { type, sub_type: "tutorial" });
					this.props.showTutorial(additionalFields.tutorial_images);
					break;
				default:
					break;
			}
		}
	};

	/**
	 *
	 * @param app String
	 */
	handleTrendingApp = item => {
		try {
			if (item && item.type) {
				if (item.type === "Native") {
					this.props.launchApp(item.app_name);
				} else if (item.type === "PWA") {
					this.props.launchWebApp(item.app_name, getPwaLink(item.app_name));
				} else if (item.type === "SERVICE") {
					this.props.launchService(getService(item.app_name));
				}
			}
		} catch (error) {
			// Handle it properly
			console.log(error);
		}
	};

	renderUserImage = userProfile => {
		try {
			let image = userProfile.image;
			if (
				image !== null &&
				image !== undefined &&
				image.trim() !== "" &&
				image.startsWith("http")
			) {
				return (
					<Image
						source={{ uri: image }}
						style={{
							width: width / 7,
							height: width / 7,
							borderRadius: width / 14
						}}
					/>
				);
			} else {
				throw new Error("User profile pic not found");
			}
		} catch (error) {
			return (
				<Image
					source={EMPTYIMAGE}
					style={{
						width: width / 7,
						height: width / 7,
						borderRadius: width / 14
					}}
				/>
			);
		}
	};

	openSiteModal = () => {
		this.siteRef.current.openSiteSelection();
	};

	getSiteName = () => {
		try {
			const { selectedSite } = this.props;
			return selectedSite.name;
		} catch (error) {
			return "";
		}
	};

	setAnalytics = (event, property) => {
		GoAppAnalytics.trackWithProperties(event, property);
	};

	render() {
		const {
			isLoading,
			userProfile,
			style,
			onProfileClick,
			selectedSite,
			onVerifyNumberClick,
			benefits,
			announcements_1,
			announcements_2,
			announcements_3,
			trendingApps,
			availableFeed,
			onScroll
		} = this.props;

		if (userProfile) {
			return (
				<SafeAreaView style={[styles.container, style]}>
					<SiteSelectionContext.Consumer>
						{siteRef => {
							this.siteRef = siteRef;
							return null;
						}}
					</SiteSelectionContext.Consumer>
					<View style={styles.userProfileSection}>
						<TouchableOpacity onPress={onProfileClick}>
							{this.renderUserImage(userProfile)}
						</TouchableOpacity>
						<View style={styles.userNameSection}>
							<GoappTextBold
								size={height / 45}
								numberOfLines={1}
								style={{
									width: width / 1.5
								}}>{`Hello ${userProfile.name}`}</GoappTextBold>
							<TouchableOpacity
								style={styles.rowFlex}
								onPress={this.openSiteModal}>
								<Icon
									iconType={"ionicon"}
									iconName={"ios-pin"}
									iconColor={"#2C98F0"}
									iconSize={height / 45}
								/>
								<GoappTextRegular
									style={{ marginLeft: width / 100 }}
									color={"#2C98F0"}
									numberOfLines={1}
									size={height / 50}>
									{this.getSiteName()}
								</GoappTextRegular>
							</TouchableOpacity>
						</View>
					</View>
					{this.state.isLoading || this.state.isTick ? (
						<LoaderView
							isLoading={this.state.isLoading}
							isTick={this.state.isTick}
						/>
					) : this.state.isError ? (
						<ErrorView />
					) : selectedSite === null ? (
						<NoSiteView
							style={styles.noSiteView}
							isRefresh={true}
							onRefresh={this.openSiteModal}
							message={"No Site Selected"}
						/>
					) : (
						<View style={styles.scrollContainer}>
							{/** verify phone number section */}
							{userProfile.phoneverified === false ||
							userProfile.phonenum === null ? (
								<VerifyPhoneFlag
									onVerifyNumberClick={() => {
										this.setAnalytics("verify_phone_banner", {});
										onVerifyNumberClick();
									}}
								/>
							) : null}
							{/** Benefit */}

							{benefits && benefits.length ? (
								<Benefit
									benefits={benefits}
									handleItemClick={(bannerSubType, additionalFields) => {
										this.handleItemClick(
											"benefit",
											bannerSubType,
											additionalFields
										);
									}}
								/>
							) : null}
							{/** Announcement1 */}
							{announcements_1 && announcements_1.length ? (
								<Announcement1
									announcements_1={announcements_1}
									onPress={() => {
										this.handleItemClick(
											"announcements_1",
											announcements_1[0].banner_sub_type_name,
											announcements_1[0].additional_field
										);
									}}
								/>
							) : null}
							{/** Trending Apps */}
							<TrendingApps
								trendingApps={trendingApps}
								onPress={item => {
									GoAppAnalytics.trackWithProperties("section_click", {
										type: "trending_apps",
										app: item.app_name
									});
									this.handleTrendingApp(item);
								}}
							/>
							{/** feed section */}
							<FlatList
								bounces={false}
								style={styles.fullFlex}
								onViewableItemsChanged={this.onViewableItemsChanged}
								viewabilityConfig={this.viewabilityConfig}
								pagingEnabled={true}
								data={availableFeed}
								renderItem={this.renderItem}
								keyExtractor={(item, index) => index.toString()}
								onScroll={onScroll}
								showsHorizontalScrollIndicator={false}
								horizontal={true}
							/>
							{/** Announcement2 */}
							<Announcement2
								announcements_2={announcements_2}
								onPress={(bannerSubType, additionalFields) => {
									this.handleItemClick(
										"announcements_2",
										bannerSubType,
										additionalFields
									);
								}}
							/>
							{/** Announcement3 */}
							{announcements_3 && announcements_3.length ? (
								<Announcement3
									announcements_3={announcements_3}
									onPress={() => {
										this.handleItemClick(
											"announcements_3",
											announcements_3[0].banner_sub_type_name,
											announcements_3[0].additional_field
										);
									}}
								/>
							) : null}
							<View style={{ width: width, height: height / 8 }} />
						</View>
					)}
				</SafeAreaView>
			);
		} else {
			return (
				<SafeAreaView style={[styles.container, style]}>
					<View style={styles.userProfileSection}>
						<View>{this.renderUserImage(userProfile)}</View>
						<View style={styles.userNameSection} />
					</View>
					{isLoading ? (
						<View style={styles.noSiteView}>
							<ProgressScreen
								isMessage={true}
								primaryMessage={"Hang On"}
								message={"Getting User Profile"}
								indicatorSize={height / 30}
								indicatorColor={"#2C98F0"}
								messageStyle={{ fontSize: height / 65, width: width / 2.7 }}
								primaryMessageStyles={{
									fontSize: height / 60,
									width: width / 2.7
								}}
							/>
						</View>
					) : (
						<View style={styles.noSiteView}>
							<GoappTextBold>{"Something went wrong"}</GoappTextBold>
							<TouchableOpacity
								onPress={() => {
									this.getUserProfile();
									this.setState({ isProfileFetchError: false });
								}}
								style={styles.retryButton}>
								<GoappTextMedium
									size={height / 45}
									color={"#fff"}
									numberOfLines={1}>
									{"RETRY"}
								</GoappTextMedium>
							</TouchableOpacity>
						</View>
					)}
				</SafeAreaView>
			);
		}
	}
}

Feed.contextType = GoappAlertContext;

Feed.defaultProps = {
	launchApp: appName => console.log("launch the app", appName),
	launchWebApp: (appName, link) => console.log(appName, link),
	launchAppWithLink: (appName, deepLink) =>
		console.log("launch app with link", appName, deepLink),
	launchService: data => console.log("alunching", data),
	onOfferClick: (brand_id, category_id) =>
		console.log("offer click handled", brand_id, category_id),
	showTutorial: (images = []) => console.log(images),
	siteId: null
};

function mapStateToProps(state) {
	return {
		token: state.appToken.token,
		userProfile: state.appToken.userProfile,
		isLoading: state.appToken.isLoading,
		selectedSite: state.appToken.selectedSite,
		siteId:
			state.appToken && state.appToken.selectedSite
				? state.appToken.selectedSite.ref_site_id
				: null,
		availableFeed: state.feed.availableFeed,
		benefits: state.feed.benefits,
		announcements_1: state.feed.announcements_1,
		announcements_2: state.feed.announcements_2,
		announcements_3: state.feed.announcements_3,
		failure: state.feed.failure,
		trendingApps: state.feed.trendingApps
	};
}

export default connect(mapStateToProps)(Feed);
