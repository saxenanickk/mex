import { StyleSheet, Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
	container: {
		flex: 1,
		width: width,
		minHeight: height,
		backgroundColor: "#ffffff"
	},
	userProfileSection: {
		paddingHorizontal: width / 30,
		paddingVertical: height / 60,
		width: width,
		backgroundColor: "#fff",
		flexDirection: "row",
		alignItems: "center",
		borderBottomWidth: 0.5,
		borderColor: "#cfcfcf"
	},
	userNameSection: {
		height: width / 7,
		marginLeft: width / 30,
		justifyContent: "center"
	},
	rowFlex: {
		flexDirection: "row",
		alignItems: "center"
	},
	noSiteView: {
		height: height - width / 7,
		justifyContent: "center",
		alignItems: "center"
	},
	fullFlex: {
		flex: 1
	},
	scrollContainer: {
		flex: 1,
		backgroundColor: "#ffffff"
	},
	headerBar: {
		width: 0.9 * width,
		aspectRatio: 6.3 / 1,
		marginTop: height / (15.8 * 2.3),
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingHorizontal: width / 20,
		alignSelf: "center"
	},
	pinStyle: {
		width: width / 25,
		height: width / 25
	},
	headerLocationText: {
		flexDirection: "row",
		alignItems: "center"
	},
	offerList: {
		marginTop: height / (15.8 * 2.3)
		// height: height / 4
	},
	offerSeparator: {
		width: width / 60
	},
	offerImageStyle: {
		width: width * 0.9,
		aspectRatio: 1.72 / 1,
		borderRadius: 16
	},
	trendingApps: {
		flex: 1,
		width: width * 0.85,
		alignSelf: "center",
		borderRadius: 16,
		backgroundColor: "#fff",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingHorizontal: width / 30
	},
	trendingAppsSection: {
		width: width * 0.9,
		marginTop: height / (15.8 * 2.3),
		borderRadius: 16,
		alignSelf: "center",
		paddingVertical: height / 50,
		backgroundColor: "#fff",
		elevation: 5,
		shadowOpacity: 0.3,
		paddingHorizontal: width / 30,
		shadowRadius: 3,
		shadowOffset: {
			height: 0,
			width: 0
		},
		aspectRatio: 2.7 / 1
	},
	appImageView: {
		elevation: 3,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#ffffff",
		shadowOpacity: 0.3,
		shadowRadius: 3,
		shadowOffset: {
			height: 0,
			width: 0
		}
	},
	appImageStyle: {
		width: width / 8,
		aspectRatio: 1 / 1
	},
	aroundYou: {
		color: "#4e4e4e",
		fontFamily: Platform.OS === "ios" ? "Avenir" : "LatoRegular",
		letterSpacing: 0.5,
		fontSize: 15,
		marginLeft: 25,
		fontWeight: "bold"
	},
	renderItemContainer: {
		flex: 1,
		paddingVertical: height / (15.8 * 2.3) / 2.5 + height / (15.8 * 2.3) / 10,
		paddingHorizontal: width / 20,
		backgroundColor: "transparent"
	},
	itemContainer: {
		flex: 1,
		backgroundColor: "#ffffff",
		elevation: 5,
		shadowOpacity: 0.3,
		shadowRadius: 3,
		shadowOffset: {
			height: 0,
			width: 0
		},
		borderRadius: 16
	},
	informationContainer: {
		flex: 1,
		flexDirection: "row",
		alignItems: "center",
		padding: 13
	},
	actionContainer: {
		flex: 1,
		padding: 10,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	appIcon: { width: width / 9, height: width / 9, borderRadius: 10 },
	actionButton: {
		padding: 10,
		borderWidth: 0.5,
		borderRadius: 10,
		backgroundColor: "#f2f2f2",
		borderColor: "#b9b9b9"
	},
	actionText: {
		color: "#262626",
		fontFamily: Platform.OS === "ios" ? "Avenir" : "LatoRegular",
		letterSpacing: 0.5,
		fontSize: 13,
		fontWeight: "bold"
	},
	retryButton: {
		marginTop: height / 60,
		borderRadius: width / 5,
		width: width / 3,
		flexDirection: "row",
		backgroundColor: "#3398f0",
		justifyContent: "center",
		alignItems: "center",
		paddingVertical: height / 90,
		paddingHorizontal: width / 90
	}
});

export default styles;
