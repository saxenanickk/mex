import Config from "react-native-config";

const {
	MEX_LOGIN_BASE_URL,
	SERVER_BASE_URL_CONTEXT,
	SERVER_BASE_URL_EVENTS_AND_OFFERS,
	SERVER_BASE_URL_CALENDAR_EVENTS,
	SERVER_BASE_HOME_URL,
	APP_VERSION
} = Config;
class FeedApi {
	getContext(params) {
		let QUERY_PARAMS = "";
		const X_ACCESS_TOKEN = params.token;
		delete params.token;

		Object.keys(params).map(item => {
			if (QUERY_PARAMS.indexOf("?") < 0) {
				QUERY_PARAMS = `${QUERY_PARAMS}?${item}=${params[item]}`;
			} else {
				QUERY_PARAMS = `${QUERY_PARAMS}&${item}=${params[item]}`;
			}
		});

		return new Promise((resolve, reject) => {
			fetch(SERVER_BASE_URL_CONTEXT + `getContext` + QUERY_PARAMS, {
				method: "GET",
				headers: {
					"X-ACCESS-TOKEN": X_ACCESS_TOKEN
				}
			})
				.then(response =>
					response
						.json()
						.then(res => (res.success ? resolve(res.data) : reject(res.data)))
				)
				.catch(error => reject(error));
		});
	}

	getEventsAndOffers(params) {
		const X_ACCESS_TOKEN = params.token;

		return new Promise((resolve, reject) => {
			fetch(SERVER_BASE_URL_EVENTS_AND_OFFERS + params.zoneId, {
				method: "GET",
				headers: {
					"X-ACCESS-TOKEN": X_ACCESS_TOKEN
				}
			})
				.then(response =>
					response
						.json()
						.then(res =>
							res.success ? resolve(res.data) : reject(res.message)
						)
				)
				.catch(error => reject(error));
		});
	}

	saveCalendarEvents(params) {
		const X_ACCESS_TOKEN = params.token;
		return new Promise((resolve, reject) => {
			fetch(SERVER_BASE_URL_CALENDAR_EVENTS, {
				method: "POST",
				headers: {
					"X-ACCESS-TOKEN": X_ACCESS_TOKEN,
					"Content-Type": "application/json"
				},
				body: JSON.stringify({ data: params.data })
			});
		});
	}

	getBanners(params) {
		const SITE_ID = params.site_id;
		const X_ACCESS_TOKEN = params.token;

		return new Promise((resolve, reject) => {
			fetch(
				`${SERVER_BASE_HOME_URL}/getBanners?site_id=${SITE_ID}&version=v2&app_config_version=${APP_VERSION}`,
				{
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": X_ACCESS_TOKEN
					}
				}
			)
				.then(response =>
					response
						.json()
						.then(res =>
							res.success ? resolve(res.data) : reject(res.message)
						)
				)
				.catch(error => reject(error));
		});
	}

	getTrendingApps(params) {
		const SITE_ID = params.site_id;
		const X_ACCESS_TOKEN = params.token;

		return new Promise((resolve, reject) => {
			fetch(
				`${SERVER_BASE_HOME_URL}/getTrendingApps?site_id=${SITE_ID}&version=v2&app_config_version=${APP_VERSION}`,
				{
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": X_ACCESS_TOKEN
					}
				}
			)
				.then(response =>
					response
						.json()
						.then(res =>
							res.success ? resolve(res.data) : reject(res.message)
						)
				)
				.catch(error => reject(error));
		});
	}

	getMyProfile(params) {
		const X_ACCESS_TOKEN = params.token;
		return new Promise((resolve, reject) => {
			try {
				fetch(`${MEX_LOGIN_BASE_URL}getProfile`, {
					method: "GET",
					headers: {
						"X-ACCESS-TOKEN": X_ACCESS_TOKEN
					}
				})
					.then(response =>
						response
							.json()
							.then(res => resolve(res))
							.catch(error => reject(error))
					)
					.catch(error => reject(error));
			} catch (error) {
				reject(error);
			}
		});
	}
}

export default new FeedApi();
