import { Dimensions } from "react-native";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

const pwaApps = {
	BookMyShow: "https://in.bookmyshow.com/",
	RXDX: "https://mex.smartenspaces.com/medical/",
	"Hod.Life": "https://rmz.hod.life/",
	Portea: "https://lp.portea.com/gnrc-ofr-rmz200/",
	"1mg": "https://www.1mg.com/?utm_source=RMZ&utm_medium=app&utm_campaign=mex",
	SwitchMe: "https://www.switchme.in/partner-landing/utm_source=rmz",
	"ICICI Auto Loan":
		"https://loan.icicibank.com/asset-portal/auto-loan/apply-now?WT.mc_id=RMZ_Mex_ALoffer&utm_source=RMZ&utm_medium=Banner&utm_campaign=Mex_ALoffer",
	"ICICI Personal Loan":
		"https://loan.icicibank.com/asset-portal/personal-loan/apply-now?WT.mc_id=RMZ_Mex_PLoffer&utm_source=RMZ&utm_medium=Banner&utm_campaign=Mex_PLoffer",
	Licious:
		"https://m.licious.in/?utm_source=RMZ&utm_medium=RMZ-MEX&utm_campaign=RMZ-May",
	Azgo: "https://rmz.azgo.app/?session=",
	Faasos:
		"https://order.faasos.io/?utm_source=MEX&utm_medium=MEXmedia&utm_campaign=MEXoffers",
	Behrouz:
		"https://www.behrouzbiryani.com/?utm_source=MEX&utm_medium=MEXmedia&utm_campaign=MEXoffers",
	"Oven Story":
		"https://www.ovenstory.in/?utm_source=MEX&utm_medium=MEXmedia&utm_campaign=MEXoffers",
	"The Good Bowl":
		"https://www.thegoodbowl.co/?utm_source=MEX&utm_medium=MEXmedia&utm_campaign=MEXoffers",
	Drivezy:
		"https://m.drivezy.com/?utm_source=RMZ&utm_medium=Referral&utm_campaign=PWAOnMex",
	Oyo: "https://www.oyorooms.com/microsite/partner/?utm_source=partner",
	Medlife:
		"https://m.medlife.com/?utm_source=RMZ_coworks&utm_medium=MEX_Marketplace#/root/home/HomeLandingOld",
	Wakefit:
		"https://www.wakefit.co/?utm_source=RMZEco&utm_medium=workplace&utm_campaign=Partnership",
	Samsung: "https://www.samsung.com/in/store/rmz",
	Housejoy:
		"https://www.housejoy.in/?utm_source=MEXRMZ&utm_medium=INapp&utm_campaign=promotions"
};

function wp(percentage) {
	const value = (percentage * width) / 100;
	return Math.round(value);
}

export const slideHeight = height * 0.36;
export const slideWidth = wp(75);
export const itemHorizontalMargin = wp(2);

export const sliderWidth = width;
export const itemWidth = slideWidth + itemHorizontalMargin * 2;

export const DEEPLINK = {
	ola: "https://apps.mexit.in/Ola",
	uber: "https://apps.mexit.in/Uber",
	cleartrip: "https://apps.mexit.in/Cleartrip",
	bookmyshow: "https://apps.mexit.in/BookMyShow",
	events: "https://apps.mexit.in/Events",
	redbus: "https://apps.mexit.in/Redbus",
	cleartrip_hotel: "https://apps.mexit.in/CleartripHotel",
	ignite: "https://apps.mexit.in/Ignite",
	community: "https://apps.mexit.in/Community",
	swiggy: "https://www.swiggy.com/",
	offers: "https://apps.mexit.in/Offers"
};

export const getPwaLink = app => {
	return pwaApps[app];
};

const services = {
	Events: "Events",
	"Polling & Quizzes": "Polling",
	"Track Shuttle": "Shuttle",
	"Live Traffic": "Traffic",
	"Invite Visitors": "Vms",
	FreeWiFi: "FreeWiFi",
	Hungerbox: "Hungerbox",
	Ignite: "Ignite",
	"Campus Assist": "CampusAssist"
};

export const getService = name => {
	return services[name];
};
