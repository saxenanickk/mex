import React, { Component } from "react";
import {
	Text,
	View,
	TouchableOpacity,
	FlatList,
	Dimensions,
	Image,
	Platform
} from "react-native";

const { width } = Dimensions.get("window");

export default class EventsAndOffers extends Component {
	handleOnPress = ({ id, type, category_id }) => {
		// use this.props.siteId instead of hardcoded number
		if (type === "offers") {
			this.props.onOfferClick(id, category_id);
		} else {
			this.props.handleEventsBannerClick(id);
		}
	};

	_renderItem = ({ item, index }) => {
		const imageUrl =
			item.type === "offers" ? item.brand_image : item.event_icon;
		const id = item.type === "offers" ? item.brand_id : item.id;
		const name = item.type === "offers" ? item.brand_name : item.name;
		const category_id = item.type === "offers" ? item.category_id : null;

		if (imageUrl === null) {
			return null;
		}
		if (!name) {
			return "";
		}

		return (
			<TouchableOpacity
				activeOpacity={0.9}
				style={{
					paddingVertical: 15,
					paddingLeft: index > 0 ? 7.5 : 15,
					paddingRight: index < this.props.data.length - 1 ? 7.5 : 15,
					flex: 1
				}}
				onPress={() =>
					this.handleOnPress({
						type: item.type,
						category_id,
						id
					})
				}>
				<View
					style={{
						borderRadius: 16,
						elevation: 5,
						shadowOpacity: 0.3,
						shadowRadius: 3,
						shadowOffset: {
							height: 0,
							width: 0
						},
						backgroundColor: "#ffffff"
					}}>
					<Image
						source={{ uri: imageUrl }}
						resizeMode={"cover"}
						style={{
							width: width / 2,
							height: width / 4,
							borderRadius: 16
						}}
					/>
				</View>
				<Text
					style={{
						width: width / 2 - 20,
						color: "#4e4e4e",
						fontFamily: Platform.OS === "ios" ? "Avenir" : "LatoRegular",
						letterSpacing: 0.5,
						fontSize: 14,
						marginLeft: 10,
						marginTop: 5
					}}>
					{name}
				</Text>
			</TouchableOpacity>
		);
	};

	render() {
		return (
			<FlatList
				horizontal={true}
				data={this.props.data}
				extraData={this.props.data}
				renderItem={this._renderItem}
				showsHorizontalScrollIndicator={false}
				keyExtractor={(item, index) => index.toString()}
			/>
		);
	}
}
