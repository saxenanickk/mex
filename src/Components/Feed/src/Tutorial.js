import React, { Component } from "react";
import { View, TouchableOpacity, Image, Dimensions } from "react-native";
import Carousel from "react-native-snap-carousel";
import { sliderWidth } from "./Utils/Constants";

const { width, height } = Dimensions.get("window");

export default class Tutorial extends Component {
	render() {
		console.log("props are", this.props);
		if (this.props.data.length) {
			return (
				<View style={styles.container}>
					<View style={styles.translucentScreen} />
					<View style={{ alignSelf: "center" }}>
						<Carousel
							useScrollView={true}
							shouldOptimizeUpdates={true}
							data={this.props.data}
							containerCustomStyle={styles.offerList}
							renderItem={({ item, index }) => {
								return (
									<Image
										key={index}
										source={{ uri: item }}
										style={styles.offerImageStyle}
										resizeMode="contain"
									/>
								);
							}}
							sliderWidth={sliderWidth}
							itemWidth={width * 0.9}
							hasParallaxImages={true}
							inactiveSlideScale={0.95}
							inactiveSlideOpacity={0.8}
							loopClonesPerSide={2}
							autoplay={true}
							autoplayDelay={500}
							autoplayInterval={3000}
						/>
					</View>
					<TouchableOpacity
						onPress={this.props.hideTutorial}
						style={styles.crossContainer}>
						<Image
							style={{ flex: 1, height: 20, width: 20 }}
							source={require("./assets/cross.png")}
							resizeMode="contain"
						/>
					</TouchableOpacity>
				</View>
			);
		} else {
			return null;
		}
	}
}

const styles = {
	container: {
		position: "absolute",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		height: height,
		justifyContent: "center",
		alignItems: "center"
	},
	translucentScreen: {
		position: "absolute",
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		backgroundColor: "#000000",
		opacity: 0.9
	},
	crossContainer: {
		position: "absolute",
		top: height / 15,
		right: 0,
		padding: width / 30,
		borderColor: "white"
	},
	cross: { fontSize: 30, fontWeight: "bold", color: "#ffffff" },
	offerList: {
		flex: 1
	},
	offerImageStyle: { flex: 1, height: undefined, width: undefined }
};
