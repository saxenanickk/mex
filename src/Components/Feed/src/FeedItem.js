import React, { Component, Fragment } from "react";
import {
	Animated,
	View,
	Text,
	TouchableOpacity,
	Dimensions,
	Image,
	Platform
} from "react-native";
import styles from "./styles";

const { width, height } = Dimensions.get("window");

export default class FeedItem extends Component {
	_defaultTransition = 250;

	constructor() {
		super();
		this.state = { showAction: false };
		this._rowOpacity = new Animated.Value(0);
	}

	componentDidMount() {
		let DURATION = 0;
		if (this.props.index < 5) {
			DURATION = this._defaultTransition * this.props.index;
		} else {
			DURATION = this._defaultTransition * 5;
		}
		Animated.timing(this._rowOpacity, {
			toValue: 1,
			duration: DURATION
		}).start();
	}

	showInformation(data = "", fallback = "", app = "") {
		let information = [];

		const dotIndex = data.indexOf(".");
		const exclaimationIndex = data.indexOf("!");
		const questionIndex = data.indexOf("?");
		const commaIndex = data.indexOf(",");
		if (app === "calendar") {
			information = ["Reminder for the Event!", `${data}`];
		} else if (dotIndex > -1 && dotIndex < data.length - 1) {
			let temp = data.split(".");
			information = [`${temp[0]}.`, `${temp[1].trim()}`];
		} else if (questionIndex > -1 && questionIndex < data.length - 1) {
			let temp = data.split("?");
			information = [`${temp[0]}?`, `${temp[1].trim()}`];
		} else if (exclaimationIndex > -1 && exclaimationIndex < data.length - 1) {
			let temp = data.split("!");
			information = [`${temp[0]}!`, `${temp[1].trim()}`];
		} else if (commaIndex > -1 && commaIndex < data.length - 1) {
			let temp = data.split(",");
			information = [`${temp[0]}.`, `${temp[1].trim()}`];
		} else if (data) {
			information = ["Hey!", `${data}`];
		} else {
			information = ["Hey!", `${fallback}`];
		}

		return information;
	}

	getApplication(data = [], item) {
		if (!data.length) {
			if (item.information.hasOwnProperty("calendar")) {
				return "calendar";
			} else if (item.information.hasOwnProperty("event_details")) {
				return "events";
			}
		} else if (data.indexOf("ola") > -1) {
			return "ola";
		} else if (data.indexOf("uber") > -1) {
			return "uber";
		} else if (data.indexOf("swiggy") > -1) {
			return "swiggy";
		} else if (data.indexOf("cleartrip") > -1) {
			return "cleartrip";
		} else if (data.indexOf("cleartrip_hotel") > -1) {
			return "cleartrip_hotel";
		} else if (data.indexOf("bookmyshow") > -1) {
			return "bookmyshow";
		} else if (data.indexOf("events") > -1) {
			return "events";
		} else if (data.indexOf("calendar") > -1) {
			return "calendar";
		}
	}

	getBadgeColor(app = null) {
		if (app === "ola") {
			return "#cddc39";
		} else if (app === "uber") {
			return "#000000";
		} else if (app === "swiggy") {
			return "#fc8018";
		} else if (app === "cleartrip" || app === "cleartrip_hotel") {
			return "#f76a0b";
		} else if (app === "bookmyshow") {
			return "#d61820";
		} else if (app === "events") {
			return "#505eff";
		} else {
			return "#00aeef";
		}
	}

	getIcon(app = null) {
		if (app === "ola") {
			return require("./assets/ola.png");
		} else if (app === "uber") {
			return require("./assets/uber.png");
		} else if (app === "swiggy") {
			return require("./assets/swiggy.png");
		} else if (app === "cleartrip" || app === "cleartrip_hotel") {
			return require("./assets/cleartrip.jpeg");
		} else if (app === "bookmyshow") {
			return require("./assets/bookmyshow.png");
		} else if (app === "events") {
			return require("./assets/events.png");
		} else if (app === "calendar") {
			return require("./assets/events.png");
		} else {
			return require("./assets/ola.png");
		}
	}

	actionInformation(app = null, item) {
		try {
			if (app === "ola" && item.estimates.ola.eta !== -1) {
				return (
					<Fragment>
						<View>
							<Text
								style={{
									color: "#000000",
									fontFamily: Platform.OS === "ios" ? "Avenir" : "LatoRegular",
									letterSpacing: 0.5,
									fontSize: 12
								}}>
								{"ETA"}
							</Text>
							<Text
								style={{
									color: "#000000",
									fontFamily: Platform.OS === "ios" ? "Avenir" : "LatoRegular",
									letterSpacing: 0.5,
									fontSize: 12
								}}>
								{item.estimates.ola.eta}
							</Text>
						</View>
						<View>
							<Text
								style={{
									color: "#000000",
									fontFamily: Platform.OS === "ios" ? "Avenir" : "LatoRegular",
									letterSpacing: 0.5,
									fontSize: 12
								}}>
								{item.estimates.ola.ola_category}
							</Text>
							<Text
								style={{
									color: "#000000",
									fontFamily: Platform.OS === "ios" ? "Avenir" : "LatoRegular",
									letterSpacing: 0.5,
									fontSize: 12
								}}>
								{item.estimates.ola.ola_estimate}
							</Text>
						</View>
					</Fragment>
				);
			}
			return null;
		} catch (error) {
			return null;
		}
	}

	actionTitle(app = null) {
		if (app === "ola") {
			return "Book Now";
		} else if (app === "uber") {
			return "Book Now";
		} else if (app === "swiggy") {
			return "Order Now";
		} else if (app === "cleartrip" || app === "cleartrip_hotel") {
			return "Book Now";
		} else if (app === "bookmyshow") {
			return "Book Now";
		} else if (app === "events") {
			return "Show Details";
		} else {
			return "Book Now";
		}
	}

	render() {
		const { item, index } = this.props;
		const _left = this._rowOpacity.interpolate({
			inputRange: [0.5, 0.7, 1],
			outputRange: [-300, -10, 0]
		});

		const app = this.getApplication(item.information.suggested_apps, item);
		const information = this.showInformation(
			item.information.title,
			item.information.action,
			app
		);

		if (item.is_consumed) {
			return null;
		}

		return (
			<Animated.View
				style={{
					opacity: this._rowOpacity,
					left: _left,
					width: width
				}}>
				<View style={styles.renderItemContainer}>
					<View style={styles.itemContainer}>
						<View style={styles.informationContainer}>
							<View style={{ flex: 1 }}>
								<View
									style={{
										width: 6.3,
										height: height / 17,
										borderRadius: 5,
										backgroundColor: this.getBadgeColor(app)
									}}
								/>
							</View>
							<View style={{ flex: 19 }}>
								<Text
									style={{
										color: "#4e4e4e",
										fontFamily:
											Platform.OS === "ios" ? "Avenir" : "LatoRegular",
										letterSpacing: 0.5,
										fontSize: 12
									}}>
									{information[0]}
								</Text>
								<Text
									style={{
										color: "#000000",
										fontWeight: "bold",
										fontFamily:
											Platform.OS === "ios" ? "Avenir" : "LatoRegular",
										letterSpacing: 0.5,
										fontSize: 12
									}}>
									{information[1]}
								</Text>
							</View>
						</View>
						<View style={styles.actionContainer}>
							<Image source={this.getIcon(app)} style={styles.appIcon} />
							{this.actionInformation(app, item)}
							{app !== "calendar" ? (
								<TouchableOpacity
									onPress={() => this.props.handleFeedClick(app, item)}>
									<View style={styles.actionButton}>
										<Text style={styles.actionText}>
											{this.actionTitle(app)}
										</Text>
									</View>
								</TouchableOpacity>
							) : null}
						</View>
					</View>
				</View>
			</Animated.View>
		);
	}
}
