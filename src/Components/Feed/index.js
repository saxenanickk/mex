import React from "react";
import App from "./src/App";
import TutorialScreen from "./src/Tutorial";

const Feed = props => <App {...props} />;

export const Tutorial = props => <TutorialScreen {...props} />;

export default Feed;
