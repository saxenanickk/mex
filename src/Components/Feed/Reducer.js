import {
	MEX_SAVE_CONTEXT,
	MEX_SAVE_BANNERS,
	MEX_SAVE_TRENDING_APPS,
	MEX_HOME_PAGE_FAILURE
} from "./Saga";

const initialState = {
	availableFeed: [],
	benefits: [],
	announcements_1: [],
	announcements_2: [],
	announcements_3: [],
	trendingApps: [],
	failure: []
};

export const reducer = (state = initialState, action) => {
	switch (action.type) {
		case MEX_SAVE_CONTEXT:
			return {
				...state,
				availableFeed: action.payload,
				failure: []
			};
		case MEX_SAVE_BANNERS:
			const {
				BENEFIT,
				ANNOUNCEMENT_4,
				ANNOUNCEMENT_2,
				ANNOUNCEMENT_3
			} = action.payload;
			return {
				...state,
				benefits: BENEFIT,
				announcements_1: ANNOUNCEMENT_4,
				announcements_2: ANNOUNCEMENT_2,
				announcements_3: ANNOUNCEMENT_3,
				failure: []
			};
		case MEX_SAVE_TRENDING_APPS:
			return {
				...state,
				trendingApps: action.payload,
				failure: []
			};
		case MEX_HOME_PAGE_FAILURE:
			let FAILURE = state.failure.slice();
			FAILURE.push(action.payload);
			return {
				...state,
				failure: FAILURE
			};
		default:
			return state;
	}
};
