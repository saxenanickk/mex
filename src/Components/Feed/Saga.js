import { takeLatest, put, call } from "redux-saga/effects";
import Api from "./src/Api";
import {
	saveData,
	HOME_PAGE_BANNERS,
	HOME_PAGE_TRENDING_APPS,
	HOME_PAGE_CONTEXT,
	clearData
} from "../../CustomModules/Offline";

/**
 * Constants
 */
export const MEX_GET_CONTEXT = "MEX_GET_CONTEXT";
export const MEX_SAVE_CONTEXT = "MEX_SAVE_CONTEXT";
export const MEX_GET_BANNERS = "MEX_GET_BANNERS";
export const MEX_SAVE_BANNERS = "MEX_SAVE_BANNERS";
export const MEX_GET_TRENDING_APPS = "MEX_GET_TRENDING_APPS";
export const MEX_SAVE_TRENDING_APPS = "MEX_SAVE_TRENDING_APPS";
export const MEX_HOME_PAGE_FAILURE = "MEX_HOME_PAGE_FAILURE";

/**
 * Action Creators
 */
export const mexGetContext = payload => ({
	type: MEX_GET_CONTEXT,
	payload
});

export const mexSaveContext = payload => ({
	type: MEX_SAVE_CONTEXT,
	payload
});

export const mexGetBanners = payload => ({
	type: MEX_GET_BANNERS,
	payload
});

export const mexSaveBanners = payload => ({
	type: MEX_SAVE_BANNERS,
	payload
});

export const mexGetTrendingApps = payload => ({
	type: MEX_GET_TRENDING_APPS,
	payload
});

export const mexSaveTrendingApps = payload => ({
	type: MEX_SAVE_TRENDING_APPS,
	payload
});

export const mexHomePageFailure = payload => ({
	type: MEX_HOME_PAGE_FAILURE,
	payload
});

/**
 * Root Saga
 */
export default function* feedSaga(dispatch) {
	yield takeLatest(MEX_GET_CONTEXT, handleMexGetContext);
	yield takeLatest(MEX_GET_BANNERS, handleMexGetBanners);
	yield takeLatest(MEX_GET_TRENDING_APPS, handleMexGetTrendingApps);
}

/**
 * Handlers
 */
function* handleMexGetContext(action) {
	try {
		const context = yield call(Api.getContext, action.payload);
		console.log("MexGetContext: ", context);
		if (context.length) {
			// Save Context to AsyncStorage
			saveData(HOME_PAGE_CONTEXT, context);
			yield put(mexSaveContext(context));
		} else {
			clearData(HOME_PAGE_CONTEXT);
			yield put(mexHomePageFailure("context"));
		}
	} catch (error) {
		console.log("Error in MexGetContext ", error.message);
		// Failure
		yield put(mexHomePageFailure("context"));
	}
}

function* handleMexGetBanners(action) {
	try {
		const banners = yield call(Api.getBanners, action.payload);
		console.log("MexGetBanners: ", banners);

		const { BENEFIT, ANNOUNCEMENT_4, ANNOUNCEMENT_2, ANNOUNCEMENT_3 } = banners;
		if (
			BENEFIT.length ||
			ANNOUNCEMENT_4.length ||
			ANNOUNCEMENT_2.length ||
			ANNOUNCEMENT_3.length
		) {
			// Save Banners to AsyncStorage
			saveData(HOME_PAGE_BANNERS, banners);
			yield put(mexSaveBanners(banners));
		} else {
			clearData(HOME_PAGE_BANNERS);
			yield put(mexHomePageFailure("banners"));
		}
	} catch (error) {
		console.log("Error in MexGetBanners ", error.message);
		// Failure
		yield put(mexHomePageFailure("banners"));
	}
}

function* handleMexGetTrendingApps(action) {
	try {
		const trendingApps = yield call(Api.getTrendingApps, action.payload);
		console.log("MexGetTrendingApps: ", trendingApps);
		if (trendingApps.length) {
			// Save Trending Apps to AsyncStorage
			saveData(HOME_PAGE_TRENDING_APPS, trendingApps);
			yield put(mexSaveTrendingApps(trendingApps));
		} else {
			clearData(HOME_PAGE_TRENDING_APPS);
			yield put(mexHomePageFailure("trendingApps"));
		}
	} catch (error) {
		console.log("Error in MexGetTrendingApps ", error.message);
		// Failure
		yield put(mexHomePageFailure("trendingApps"));
	}
}
