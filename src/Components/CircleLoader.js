import React from "react";
import { View, StyleSheet, Animated, Easing, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export default class CircleLoader extends React.Component {
	constructor(props) {
		super(props);
		this.animatedValue = new Animated.Value(0);
	}

	shouldComponentUpdate() {
		return true;
	}

	componentDidMount() {
		this.animate();
	}

	componentWillUnmount() {
		this.unmounted = true;
	}

	animate() {
		this.animatedValue.setValue(0);
		Animated.timing(this.animatedValue, {
			toValue: 1,
			duration: 1000,
			easing: Easing.linear(Easing.ease)
		}).start(() => !this.unmounted && this.animate());
	}

	render() {
		const size = this.animatedValue.interpolate({
			inputRange: [0, 0.3, 1],
			outputRange: [width / 5, width / 2.5, width / 2.5]
		});
		const olaSize = this.animatedValue.interpolate({
			inputRange: [0, 0.3, 1],
			outputRange: [0, width / 5, width / 2.5]
		});
		const opacity = this.animatedValue.interpolate({
			inputRange: [0, 0.3, 1],
			outputRange: [3, 0, 0]
		});

		const borderWidth = this.animatedValue.interpolate({
			inputRange: [0, 0.3, 1],
			outputRange: [5, 3, 0]
		});

		return (
			<View
				style={[
					styles.container,
					{
						height: this.props.height,
						width: this.props.width,
						backgroundColor: this.props.backgroundColor
					}
				]}>
				<Animated.View
					style={{
						justifyContent: "center",
						alignItems: "center",
						width: olaSize,
						height: olaSize,
						borderRadius: width / 5,
						borderWidth: borderWidth,
						// borderColor: "#3FAAD3",
						borderColor: this.props.color
					}}>
					{this.props.showRectangle && (
						<View
							style={{
								width: 10,
								height: 10,
								backgroundColor: this.props.color
								// backgroundColor: "#3FAAD3"
							}}
						/>
					)}
				</Animated.View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		position: "absolute",
		width: width,
		height: height,
		backgroundColor: "#232522",
		opacity: 0.9,
		justifyContent: "center",
		alignItems: "center",
		zIndex: 10,
		elevation: 6
	}
});
