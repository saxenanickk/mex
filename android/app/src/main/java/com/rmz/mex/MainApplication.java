package com.rmz.mex;

import android.app.Application;

import com.facebook.react.PackageList;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import com.rmz.mex.BuildConfig;
import io.branch.rnbranch.RNBranchModule;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import io.invertase.firebase.fabric.crashlytics.RNFirebaseCrashlyticsPackage;
import com.wix.interactable.Interactable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainApplication extends Application implements ReactApplication {

  private static Set<String> appList = new HashSet<String>();;

  public static boolean isAppLaunched(String app) {
    if (appList == null)
      return false;

    if (appList.contains(app))
      return true;

    return false;
  }

  public static void addApp(String app) {
    appList.add(app);
  }

  public static void removeApp(String app) {
    appList.remove(app);
  }

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      @SuppressWarnings("UnnecessaryLocalVariable")
      List<ReactPackage> packages = new PackageList(this).getPackages();
      // Packages that cannot be autolinked yet can be added manually here, for
      // example:
      packages.add(new SplashActivityPackage());
      packages.add(new GoDeviceInfoPackage());
      packages.add(new GoLocationPackage());
      packages.add(new RNFirebaseMessagingPackage());
      packages.add(new RNFirebaseNotificationsPackage());
      packages.add(new RNFirebaseCrashlyticsPackage());
      packages.add(new MiniApp());
      packages.add(new AccessPackage());
      packages.add(new Interactable());
      return packages;
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
    MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(this, ServerConstants.MIXPANEL_TOKEN);
    RNBranchModule.getAutoInstance(this);
  }
}
