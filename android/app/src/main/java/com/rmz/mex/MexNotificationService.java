package com.rmz.mex;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mixpanel.android.mpmetrics.MixpanelFCMMessagingService;

import io.invertase.firebase.messaging.RNFirebaseMessagingService;

public class MexNotificationService extends FirebaseMessagingService {
    RNFirebaseMessagingService firebaseMessagingService;

    public MexNotificationService() {
        super();
        if (firebaseMessagingService == null) {
            firebaseMessagingService = new RNFirebaseMessagingService();
        }
    }
    @Override
    public void onNewToken(@NonNull String s) {
        MixpanelFCMMessagingService.addToken(s);

        firebaseMessagingService.onNewToken(s);
        super.onNewToken(s);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if(remoteMessage.getData().get("mp_message") != null) {
            MixpanelFCMMessagingService.showPushNotification(getApplicationContext(),remoteMessage.toIntent());
        } else {
            firebaseMessagingService.onMessageReceived(remoteMessage);
        }
    }
}
