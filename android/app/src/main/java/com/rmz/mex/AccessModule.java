package com.rmz.mex;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import javax.annotation.Nonnull;

import simsim.lockn.com.locknandroidsdk.LocknBLESDK;
import simsim.lockn.com.locknandroidsdk.callbacks.LocknBLECallBack;
import simsim.lockn.com.locknandroidsdk.enums.LocknStatusEnum;

public class AccessModule extends ReactContextBaseJavaModule {

    ReactApplicationContext mContext;

    public AccessModule(ReactApplicationContext reactContext) {
        super(reactContext);
        mContext = reactContext;
    }

    @Nonnull
    @Override
    public String getName() {
        return "Access";
    }

    @ReactMethod @SuppressWarnings("unused")
    public void init(String token, Callback success, Callback error) {
        LocknBLESDK locknBLESDK = new LocknBLESDK(mContext);
        if (!locknBLESDK.isSDKInitialized()) {
            locknBLESDK.initializeSDK(ServerConstants.LOCKN_AUTH_STRING, token, new LocknBLECallBack<Boolean>() {
                @Override
                public void onResponse(LocknStatusEnum locknStatusEnum) {
                    switch (locknStatusEnum) {
                        case FAILED:
                            error.invoke("Access Reader not in range.\nPlease try again.");
                            break;
                        case LOCKED:
                            error.invoke("Locked.");
                            break;
                        case SUCCESS:
                            success.invoke("Access Granted.");
                        case UNLOCKED:
                            success.invoke("Unlocked.");
                            break;
                        case UPDATING:
                            error.invoke("Updating.");
                            break;
                        case NO_DEVICES_IN_RANGE:
                            error.invoke("Access Reader not in range.\nPlease try again.");
                            break;
                        case BLUETOOTH_OR_GPS_OFF:
                            error.invoke("Bluetooth or gps off.");
                            break;
                        case DEVICE_NOT_SUPPORTED:
                            error.invoke("Device not supported.\nPlease connect MEX. Support.");
                            break;
                        case TIMED_OUT:
                            error.invoke("Connection timeout!\nPlease try again.");
                            break;
                        case TOO_MANY_DEVICES_IN_RANGE:
                            error.invoke("Too many devices in range.");
                            break;
                        case BLUETOOTH_COMMUNICATION_FAILED:
                            error.invoke("Connection timeout!\nAccess Reader not in range.");
                            break;
                        case OUT_OF_RANGE:
                            error.invoke("Connection timeout!\nAccess Reader not in range.");
                            break;
                        case UNAUTHORISED:
                            error.invoke("Unauthorised.");
                            break;
                        case SOMETHING_WENT_WRONG:
                            error.invoke("Connection Timeout!\nPlease try again.");
                            break;
                        case USER_TEMPID_ACTIVATION_SUCCESSFUL:
                            success.invoke("MEX. Access Activated.");
                            break;
                        case USER_TEMPID_ACTIVATION_FAILED:
                            error.invoke("Invalid Code, Retry or connect\nwith your Organisation Admin.");
                            break;
                        case INITIALIZATION_NO_DONE:
                            error.invoke("Access not initialized.\nPlease restart the app.");
                            break;
                        default:
                            error.invoke("Something went wrong.");
                            break;
                    }
                }
            });
        }
    }

    @ReactMethod @SuppressWarnings("unused")
    public void isInitialized(Callback initilized) {
        LocknBLESDK locknBLESDK = new LocknBLESDK(mContext);
        initilized.invoke(locknBLESDK.isSDKInitialized());
    }

    @ReactMethod @SuppressWarnings("unused")
    public void openLock(Callback success, Callback error) {
        LocknBLESDK locknBLESDK = new LocknBLESDK(mContext);

        locknBLESDK.openLocknLock(new LocknBLECallBack<Boolean>() {
            @Override
            public void onResponse(LocknStatusEnum locknStatusEnum) {
                switch (locknStatusEnum) {
                    case FAILED:
                        error.invoke("Access Reader not in range.\nPlease try again.");
                        break;
                    case LOCKED:
                        error.invoke("Locked.");
                        break;
                    case SUCCESS:
                        success.invoke("Access Granted.");
                    case UNLOCKED:
                        success.invoke("Unlocked.");
                        break;
                    case UPDATING:
                        error.invoke("Updating.");
                        break;
                    case NO_DEVICES_IN_RANGE:
                        error.invoke("Access Reader not in range.\nPlease try again.");
                        break;
                    case BLUETOOTH_OR_GPS_OFF:
                        error.invoke("Bluetooth or gps off.");
                        break;
                    case DEVICE_NOT_SUPPORTED:
                        error.invoke("Device not supported.\nPlease connect MEX. Support.");
                        break;
                    case TIMED_OUT:
                        error.invoke("Connection timeout!\nPlease try again.");
                        break;
                    case TOO_MANY_DEVICES_IN_RANGE:
                        error.invoke("Too many devices in range.");
                        break;
                    case BLUETOOTH_COMMUNICATION_FAILED:
                        error.invoke("Connection timeout!\nAccess Reader not in range.");
                        break;
                    case OUT_OF_RANGE:
                        error.invoke("Connection timeout!\nAccess Reader not in range.");
                        break;
                    case UNAUTHORISED:
                        error.invoke("Unauthorised.");
                        break;
                    case SOMETHING_WENT_WRONG:
                        error.invoke("Connection Timeout!\nPlease try again.");
                        break;
                    case USER_TEMPID_ACTIVATION_SUCCESSFUL:
                        success.invoke("MEX. Access Activated.");
                        break;
                    case USER_TEMPID_ACTIVATION_FAILED:
                        error.invoke("Invalid Code, Retry or connect\nwith your Organisation Admin.");
                        break;
                    case INITIALIZATION_NO_DONE:
                        error.invoke("Access not initialized.\nPlease restart the app.");
                        break;
                    default:
                        error.invoke("Something went wrong.");
                        break;
                }
            }
        });
    }

    @ReactMethod @SuppressWarnings("unused")
    public void factoryReset() {
        LocknBLESDK locknBLESDK = new LocknBLESDK(mContext);
        locknBLESDK.factoryReset();
    }
}
