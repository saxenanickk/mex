package com.rmz.mex;

import android.provider.Settings;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class GoDeviceInfoModule extends ReactContextBaseJavaModule {
    public GoDeviceInfoModule(ReactApplicationContext reactApplicationContext) {
        super(reactApplicationContext);
    }

    @Override
    public String getName() {
        return "GoDeviceInfo";
    }

    @ReactMethod
    public void getDeviceId(final Callback success, final Callback error) {
        String DEVICE_ID = Settings.Secure.getString(this.getReactApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        success.invoke(DEVICE_ID);
    }
}
