package com.rmz.mex;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import static android.webkit.WebView.setWebContentsDebuggingEnabled;

@TargetApi(21)
public class SwiggyActivity extends AppCompatActivity {

    WebView myWebView;
    RelativeLayout myAddressView;
    SharedPreferences sharedPref;
    MixpanelAPI mixpanelAPIInstance;
    boolean isAddressViewVisible;

    private ImageView close;
    private TextView confirm;
    private EditText campus,floor,company,building;
    private CardView closeCardView;
    private String mOrderID;
    private String mOrderCreated = "Not Started";

    @SuppressLint({"AddJavascriptInterface", "SetJavaScriptEnabled"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swiggy_address);

        setTaskDescription(new ActivityManager.TaskDescription("Swiggy"));

        mixpanelAPIInstance = MixpanelAPI.getInstance(getApplicationContext(), ServerConstants.MIXPANEL_TOKEN);
        if(mixpanelAPIInstance != null && mixpanelAPIInstance.getDistinctId() != null && mixpanelAPIInstance.getPeople().getDistinctId() != null) {
            JSONObject event = new JSONObject();
            try {
                event.put("name", "swiggy");
            }catch (JSONException e) {

            }
            mixpanelAPIInstance.track("app_launch",event);
        }
        sharedPref = getPreferences(Context.MODE_PRIVATE);

        HashMap<String, String> hashMap = new HashMap<>();
        //Get cookie to sharepref here
        String cookie = sharedPref.getString("swiggy_cookie",null);

        if(cookie != null)
            hashMap.put("x-platform-cookie", cookie);

        myWebView = findViewById(R.id.webview);
        myAddressView = findViewById(R.id.addressView);

//        setContentView(myWebView);

        myWebView.setWebViewClient(new CustomWebViewClient());
        myWebView.addJavascriptInterface(new WebAppInterface(this), "NativeApp");

        WebSettings webSetting = myWebView.getSettings();
        webSetting.setJavaScriptEnabled(true);
        setWebContentsDebuggingEnabled(true);
        webSetting.setGeolocationEnabled(true);
        myWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
//                super.onGeolocationPermissionsShowPrompt(origin, callback);
                callback.invoke(origin, true, true);
            }
        });

        myWebView.loadUrl(ServerConstants.SWIGGY_URL, hashMap);

        setAddressView();
    }

    private void setAddressView() {
        close = findViewById(R.id.swiggy_closeButton);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMixpanelEvent();
                hideAddress();
            }
        });
        findViewById(R.id.swiggy_confirm_cardview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmClicked();
            }
        });
        confirm = findViewById(R.id.swiggy_confirm_textview);
        campus = findViewById(R.id.swiggy_campus_edittext);
        floor = findViewById(R.id.swiggy_floor_edittext);
        company = findViewById(R.id.swiggy_company_edittext);
        building = findViewById(R.id.swiggy_building_edittext);
        ((TextView)findViewById(R.id.no_desk_delivery_textview)).setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        ((TextView)findViewById(R.id.how_itworks_textview)).setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
    }

    private void getLastSwiggyAddress() {
        new GetSwiggyAddress().execute();
    }

    private void confirmClicked() {
        //Save the updated address for the next time
        String campusVal = campus.getText().toString();
        String floorVal = floor.getText().toString();
        String companyVal = company.getText().toString();
        String buildingVal = building.getText().toString();

        new SaveSwiggyAddress(campusVal,floorVal,companyVal,buildingVal).execute();
        new ConfirmOrder(campusVal, floorVal, companyVal, buildingVal).execute();
        hideAddress();
    }

    private void hideAddress() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                myAddressView.setVisibility(View.GONE);
                myWebView.setVisibility(View.VISIBLE);
            }
        });

    }

    private void showAddress() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                myAddressView.setVisibility(View.VISIBLE);
                myWebView.setVisibility(View.GONE);
            }
        });
    }

    private void sendMixpanelEvent() {

        String campusVal = campus.getText().toString();
        String floorVal = floor.getText().toString();
        String companyVal = company.getText().toString();
        String buildingVal = building.getText().toString();

        MixpanelAPI mixpanelAPIInstance = MixpanelAPI.getInstance(getApplicationContext(),ServerConstants.MIXPANEL_TOKEN);

        if(mixpanelAPIInstance != null && mOrderID != null) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("order_id", mOrderID);
                obj.put("campus", campusVal);
                obj.put("floor", floorVal);
                obj.put("company", companyVal);
                obj.put("building", buildingVal);
                obj.put("order_created", mOrderCreated);
            }catch (JSONException e) {

            }
            mixpanelAPIInstance.track("swiggy-order-flow-confirm",obj);
            mixpanelAPIInstance.flush();
        }
    }

    @TargetApi(19)
    private final class SaveSwiggyAddress extends AsyncTask<Void, Void, Void> {

        String campus,floor,company,building;
        public SaveSwiggyAddress() {

        }

        public SaveSwiggyAddress(String campus,String floor,String company, String building) {
            this.campus = campus;
            this.floor = floor;
            this.company = company;
            this.building = building;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                JSONObject inputParams = new JSONObject();
                try {
                    inputParams.put("campus", campus);
                    inputParams.put("app_name", "swiggy");
                    inputParams.put("floor", floor);
                    inputParams.put("company_name", company);
                    inputParams.put("building", building);

                }catch (JSONException ex) {

                }
                URL u = new URL(ServerConstants.SWIGGY_SAVE_ADDRESS_URL);
                HttpURLConnection connection = (HttpURLConnection) u.openConnection();

                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("co.goapp.lib", Context.MODE_PRIVATE);
                String token =  sharedPref.getString("mex_app_token",null);

                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("X-ACCESS-TOKEN", token);

                Log.d("INput params",inputParams.toString());
                try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                    wr.write(inputParams.toString().getBytes(StandardCharsets.UTF_8));
                }

                String json_response = "";
                InputStreamReader in = new InputStreamReader(connection.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String text = "";
                while ((text = br.readLine()) != null) {
                    json_response += text;
                }

                JsonParser jsonParser = new JsonParser();
                JsonObject jsonObject = (JsonObject) jsonParser.parse(json_response);
                boolean isSuccess = jsonObject.get("success").getAsBoolean();
                return null;

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private final class GetSwiggyAddress extends AsyncTask<Void, Void, JsonObject> {

        @Override
        protected JsonObject doInBackground(Void... params) {
            try {
                URL u = new URL(ServerConstants.SWIGGY_ADDRESS_URL);
                HttpURLConnection connection = (HttpURLConnection) u.openConnection();

                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("co.goapp.lib", Context.MODE_PRIVATE);
                String token =  sharedPref.getString("mex_app_token",null);

                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("X-ACCESS-TOKEN", token);

                String json_response = "";
                InputStreamReader in = new InputStreamReader(connection.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String text = "";
                while ((text = br.readLine()) != null) {
                    json_response += text;
                }

                JsonParser jsonParser = new JsonParser();
                JsonObject jsonObject = (JsonObject) jsonParser.parse(json_response);
                int isSuccess = jsonObject.get("success").getAsInt();

                if(isSuccess == 1) {
                    JsonObject data = jsonObject.get("data").getAsJsonObject();
                    return data;
                } else {
                    return null;
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JsonObject data) {
            if(data != null) {
//                "building": "6D",
//                        "campus": "Ecoworld",
//                        "company_name": "Quant",
//                        "created_at": "2019-06-26T09:28:51.080Z",
//                        "floor": "1",

                if(data.has("building"))
                    building.setText(data.get("building").getAsString());

                if(data.has("campus"))
                    campus.setText(data.get("campus").getAsString());

                if(data.has("floor"))
                    floor.setText(data.get("floor").getAsString());

                if(data.has("company_name"))
                    company.setText(data.get("company_name").getAsString());
            }
        }
    }

    private final class ConfirmOrder extends AsyncTask<Void, Void, Void> {
        String campus, floor, company, building;
        public ConfirmOrder(String campus, String floor, String company, String building) {
            this.campus = campus;
            this.floor = floor;
            this.company = company;
            this.building = building;
        }

        @Override
        protected Void doInBackground(Void... params) {
            JSONObject inputParams = new JSONObject();
            if (mOrderID != null) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("co.goapp.lib", Context.MODE_PRIVATE);
                String user_name = sharedPref.getString("mex_user_name", null);
                String user_phone = sharedPref.getString("mex_user_phone", null);
                String token =  sharedPref.getString("mex_app_token",null);
                try {
                    inputParams.put("campus", campus);
                    inputParams.put("app_name", "swiggy");
                    inputParams.put("floor", floor);
                    inputParams.put("company", company);
                    inputParams.put("building", building);
                    inputParams.put("app_order_id", mOrderID);
                    inputParams.put("app_order_status", "PENDING");
                    inputParams.put("name", user_name);
                    inputParams.put("phone", user_phone);
                }catch (JSONException ex) {
                    ex.printStackTrace();
                }

                URL url;
                HttpURLConnection client = null;
                try {
                    url = new URL(ServerConstants.SWIGGY_ORDER_CONFIRM_URL_MEX);
                    client = (HttpURLConnection) url.openConnection();
                    client.setRequestMethod("POST");
                    client.setRequestProperty("X-ACCESS-TOKEN", token);
                    client.setRequestProperty("Content-Type", "application/json");
                    client.setDoOutput(true);
                    OutputStream outputStream = new BufferedOutputStream(client.getOutputStream());
                    outputStream.write(inputParams.toString().getBytes(StandardCharsets.UTF_8));
                    // Add output stream
                    outputStream.flush();
                    outputStream.close();
                    // Response
                    String json_response = "";
                    InputStreamReader in = new InputStreamReader(client.getInputStream());
                    BufferedReader br = new BufferedReader(in);
                    String text = "";
                    while ((text = br.readLine()) != null) {
                        json_response += text;
                    }

                    JsonParser jsonParser = new JsonParser();
                    JsonObject jsonObject = (JsonObject) jsonParser.parse(json_response);
                    int isSuccess = jsonObject.get("success").getAsInt();
                    if(isSuccess == 1) {
                        mOrderCreated = "Success";
                    } else {
                        mOrderCreated = "Failed";
                    }
                } catch (MalformedURLException error) {
                    error.printStackTrace();
                    mOrderCreated = "Failed";
                } catch (IOException error) {
                    error.printStackTrace();
                    mOrderCreated = "Failed";
                } finally {
                    if (client != null) {
                        client.disconnect();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            sendMixpanelEvent();
        }
    }

    private class CustomWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Nullable
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
//            if (request.getUrl().toString().startsWith("https://www.swiggy.com")) {
//                Log.d("Request:", request.getUrl().toString());
//            }
            return super.shouldInterceptRequest(view, request);
        }
    }


    public class WebAppInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        WebAppInterface(Context c) {
            mContext = c;
        }

        /** Show a toast from the web page */
        @JavascriptInterface
        @SuppressWarnings("unused")
        public void setCookie(String cookie) {
            Log.d("SET COOKIE", cookie);

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("swiggy_cookie", cookie);
            editor.commit();
        }

        @JavascriptInterface @SuppressWarnings("unused")
        public void fireEvent(String event, String order_id) {
            Log.d("EVENT", event);
            Log.d("ORDER ID", order_id);
            mixpanelAPIInstance = MixpanelAPI.getInstance(getApplicationContext(), ServerConstants.MIXPANEL_TOKEN);
            if(mixpanelAPIInstance != null) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put("order_id", order_id);
                }catch (JSONException e) {

                }
                mixpanelAPIInstance.track("swiggy-order-flow-confirm",obj);
                mixpanelAPIInstance.flush();
            }
            try {
                mOrderID = order_id;
                getLastSwiggyAddress();
                showAddress();
            }catch (Exception ex) {

            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if(myAddressView.getVisibility() == View.VISIBLE) {
                        hideAddress();
                    }
                    else if (myWebView.canGoBack()) {
                        myWebView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }
}
