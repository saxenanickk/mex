package com.rmz.mex;

public class ServerConstants {
//    public static final String ota_server = BuildConfig.SERVER_TYPE; // test or live

    public static final String  MIXPANEL_TOKEN = BuildConfig.MIX_PANEL_TOKEN;

    public static final String AZGO_TOKEN_URL = BuildConfig.HB_TOKEN_URL;

    public static final String AZGO_WEB_URL_PREFIX = BuildConfig.AZGO_URL_PREFIX;

    public static final  String SWIGGY_ADDRESS_URL = BuildConfig.MEX_ADDRESS + "getAddresses?app_name=swiggy";

    public static final  String SWIGGY_SAVE_ADDRESS_URL = BuildConfig.MEX_ADDRESS + "saveAddress";

    public static final String HB_TOKEN_URL = BuildConfig.HB_TOKEN_URL;

    public static final String HB_URL_PREFIX = BuildConfig.HB_URL_PREFIX;

    public static final String SWIGGY_URL = "https://www.swiggy.com/?utm_source=RMZ&utm_campaign=RMZ&platform=RMZ&token=61B667C88FB270F558FB9254F94A2D73";

    public static final String SWIGGY_ORDER_CONFIRM_URL_MEX = BuildConfig.SWIGGY_CONFIRM_ORDER;

    public static final String LOCKN_AUTH_STRING = BuildConfig.LOCKN_AUTH_STRING;
}