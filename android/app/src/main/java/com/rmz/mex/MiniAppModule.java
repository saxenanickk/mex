package com.rmz.mex;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Looper;
import android.util.Log;
import java.util.List;
import android.app.ActivityManager;
import android.widget.Toast;

import androidx.browser.customtabs.CustomTabsIntent;

import com.bigbasket.bbinstant.bb_sdk_interfaces.BBSDKParam;
import com.bigbasket.bbinstant.bb_sdk_interfaces.InitializeException;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;
import com.freshchat.consumer.sdk.Freshchat;
import com.freshchat.consumer.sdk.FreshchatConfig;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

public class MiniAppModule extends ReactContextBaseJavaModule {
    private ReactApplicationContext mContext;

    public MiniAppModule(ReactApplicationContext reactContext) {
        super(reactContext);
        mContext = reactContext;
        FreshchatConfig freshchatConfig=new FreshchatConfig("8513eeeb-e972-4c2b-be05-1ae46bfe326c","52ca0c9d-88e2-4c24-976d-84adf41e0e38");

        /**
         * Added this hack:
         *
         * For Error: Can't toast on a thread that has not called Looper.prepare()
         *
         */
        Looper.prepare();
        Freshchat.setImageLoader(com.freshchat.consumer.sdk.j.af.aw(mContext));

        Freshchat.getInstance(mContext.getApplicationContext()).init(freshchatConfig);
    }


    @Override
    public String getName() {
        return "MiniApp";
    }

    @ReactMethod
    public void setSessionToken(String mex_app_token) {
        if(mex_app_token == null)
            return;

        SharedPreferences sharedPref = mContext.getApplicationContext().getSharedPreferences("co.goapp.lib", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("mex_app_token", mex_app_token);
        editor.commit();
    }

    @ReactMethod
    public void setUserInfo(String name,String email,String phone) {

        SharedPreferences sharedPref = mContext.getApplicationContext().getSharedPreferences("co.goapp.lib", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        if(name != null)
            editor.putString("mex_user_name", name);
        if(email != null)
            editor.putString("mex_user_email", email);
        if(phone != null) {
            if(phone.contains("-"))
                phone = phone.replace("-","");
            editor.putString("mex_user_phone", phone);
        }
        editor.commit();
    }

    @ReactMethod
    public void setSiteInfo(String site_id,String ref_site_id,String site_name) {
        if(site_id == null || site_name == null)
            return;

        SharedPreferences sharedPref = mContext.getApplicationContext().getSharedPreferences("co.goapp.lib", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("qc_site_id", site_id);
        editor.putString("mex_site_id", ref_site_id);
        editor.putString("mex_site_name", site_name);
        editor.commit();
    }

    @ReactMethod
    public void setTenantInfo(String tenant_id,String tenant_name) {
        if(tenant_id == null || tenant_name == null)
            return;

        SharedPreferences sharedPref = mContext.getApplicationContext().getSharedPreferences("co.goapp.lib", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("mex_tenant_id", tenant_id);
        editor.putString("mex_tenant_name", tenant_name);
        editor.commit();
    }

    @TargetApi(21)
    @ReactMethod @SuppressWarnings("unused")
    public void launchApp(String app) {
        ActivityManager mgr = (ActivityManager)mContext.getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.AppTask> tasks = mgr.getAppTasks();
        String label;
        try {
            for (ActivityManager.AppTask task : tasks) {
                label = task.getTaskInfo().taskDescription.getLabel();
                if (label != null && label.equalsIgnoreCase(app)) {
                    task.moveToFront();
                    return;
                }
            }

        } catch (IllegalArgumentException ex) {
            //Mostly unable to find task id
            //Go ahead and create new task
        }

        switch (app) {
            case "swiggy":
                startSwiggy();
                break;
            case "bbinstant":
                startBBInstant();
                break;
            case "access" :
                break;
            default:
                break;
        }
    }

    private void startBBInstant() {
        SharedPreferences sharedPref = mContext.getSharedPreferences("co.goapp.lib", Context.MODE_PRIVATE);
        String name =  sharedPref.getString("mex_user_name",null);
        String email =  sharedPref.getString("mex_user_email",null);
        String phone =  sharedPref.getString("mex_user_phone",null);

        if(name == null || email == null || phone == null) {
            return;
        }

        sendMixpanelLaunchEvent("bbinstant");

        try {
            BBSDKParam param = new BBSDKParam.SDKBuilder()
                    .withUserName(name)
                    .withEmail(email)
                    .withMerchantId("DKsn3XYPjHexvbK9g7T9RSklQZjEZp8") // Fore MEX prod
                    //.withMerchantId("SWPH7VYGoHrxvbtPgfT7NgklPZJEZu2") // For MEX stage
                    .withBalanceAvailable(true)
                    .withAppType("MEX")
                    .withUserToken("SWPH7VYGoHrxvbtPgfT7NgklPZJEZu2")
                    .withPhoneNum(phone)
                    .build();
            param.startBBApp(mContext.getCurrentActivity());
        } catch (InitializeException e) {
            e.printStackTrace();
        }
    }

    @TargetApi(21)
    @ReactMethod @SuppressWarnings("unused")
    public  void launchWebApp(String app, String url) {
        Activity context = mContext.getCurrentActivity();

        if (context == null || context.isFinishing()) {
            return;
        }

        if(url == null) {
            Toast.makeText(mContext.getCurrentActivity(), app + " not found", Toast.LENGTH_SHORT).show();
        }

        sendMixpanelLaunchEvent(app);

        //HB & Azgo
        if(app.equalsIgnoreCase("hungerbox")  || app.equalsIgnoreCase("Azgo")) {
            ActivityManager mgr = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.AppTask> tasks = mgr.getAppTasks();
            String label;
            for (ActivityManager.AppTask task: tasks){
               label = task.getTaskInfo().taskDescription.getLabel();
               if(label != null && label.equalsIgnoreCase(app)) {
                   task.moveToFront();
                   return;
               }
            }

            Intent rctActivityIntent = new Intent(context, PWAActivity.class);
            if(app.equalsIgnoreCase("hungerbox"))
                rctActivityIntent.putExtra("app", "MEX. Cafeteria");
            else
                rctActivityIntent.putExtra("app", app);

           if(url != null) {
               rctActivityIntent.putExtra("params", url);
           }
           context.startActivity(rctActivityIntent);
        }
        //PWA
        else {
            launchPWA(app,url);
        }
   }

   private void sendMixpanelLaunchEvent(String app) {
       MixpanelAPI mixpanelAPIInstance = MixpanelAPI.getInstance(mContext.getApplicationContext(),ServerConstants.MIXPANEL_TOKEN);
       if(mixpanelAPIInstance != null && mixpanelAPIInstance.getDistinctId() != null && mixpanelAPIInstance.getPeople().getDistinctId() != null) {
           JSONObject event = new JSONObject();
           try {
               event.put("name", app);
           }catch (JSONException e) {

           }
           mixpanelAPIInstance.track("app_launch",event);
       }
   }

   private void launchPWA(String app, String url) {
        Activity context = mContext.getCurrentActivity();

        if (context == null || context.isFinishing()) {
            return;
        }

       CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
       CustomTabsIntent customTabsIntent = builder.build();
       customTabsIntent.launchUrl(context, Uri.parse(url));
   }

    @ReactMethod @SuppressWarnings("unused")
    public void launchSupportChat() {
        Freshchat.showConversations(mContext.getApplicationContext());
    }


    @ReactMethod @SuppressWarnings("unused")
    public void startSwiggy() {
        Intent rctActivityIntent = new Intent(mContext.getCurrentActivity(), SwiggyActivity.class);
        mContext.getCurrentActivity().startActivity(rctActivityIntent);
    }
}
