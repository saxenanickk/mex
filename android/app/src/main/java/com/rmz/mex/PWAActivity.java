package com.rmz.mex;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.browser.customtabs.CustomTabsIntent;

//import com.bigbasket.bbinstant.bb_sdk_interfaces.BBSDKParam;
//import com.bigbasket.bbinstant.bb_sdk_interfaces.InitializeException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;


@TargetApi(21)
public class PWAActivity extends AppCompatActivity {

    WebView myWebView;
    public static final String USER_AGENT = "Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19";

    private String mURL;
    private ImageView closeButton;

    @SuppressLint({"AddJavascriptInterface", "SetJavaScriptEnabled"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String app = getIntent().getStringExtra("app");


        setTaskDescription(new ActivityManager.TaskDescription(app));
        setContentView(R.layout.activity_custom_webview);

        myWebView = findViewById(R.id.webview);
        closeButton = findViewById(R.id.closeButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ((TextView)findViewById(R.id.webview_app_name)).setText(app);
//        setContentView(myWebView);

        myWebView.setWebViewClient(new CustomWebViewClient());

        WebSettings webSetting = myWebView.getSettings();
        webSetting.setJavaScriptEnabled(true);
        webSetting.setDomStorageEnabled(true);
        webSetting.setGeolocationEnabled(true);
        webSetting.setAllowFileAccess(true);
        webSetting.setAllowFileAccessFromFileURLs(true);
        webSetting.setUserAgentString(PWAActivity.USER_AGENT);
        webSetting.setCacheMode(WebSettings.LOAD_NO_CACHE);

         if(app.equalsIgnoreCase("MEX. Cafeteria")) {
            new GetHBToken().execute();
        } else if(app.equalsIgnoreCase("azgo")) {
             new GetAzgoToken().execute();
         } else {
             finish();
         }
    }

    private final class GetHBToken extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL u = new URL(ServerConstants.HB_TOKEN_URL);
                HttpURLConnection connection = (HttpURLConnection) u.openConnection();

                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("co.goapp.lib",Context.MODE_PRIVATE);
                String token =  sharedPref.getString("mex_app_token",null);

                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("x-access-token", token);

                String json_response = "";
                InputStreamReader in = new InputStreamReader(connection.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String text = "";
                while ((text = br.readLine()) != null) {
                    json_response += text;
                }

                JsonParser jsonParser = new JsonParser();
                try {
                    JsonObject jsonObject = (JsonObject) jsonParser.parse(json_response);
                    int isSuccess = jsonObject.get("success").getAsInt();
                    JsonObject data = jsonObject.getAsJsonObject("data");

                    if (isSuccess == 1 && data != null && data.has("access_token")) {
                        String sessionToken = data.get("access_token").getAsString();
                        return sessionToken;
                    } else {
                        return null;
                    }
                } catch (Exception ex) {
                    return null;
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String sessionToken) {
            if(sessionToken != null) {
                try {
                    String url = ServerConstants.HB_URL_PREFIX;
                    SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("co.goapp.lib",Context.MODE_PRIVATE);
                    String site_id =  sharedPref.getString("qc_site_id",null);
                    String postData = "access_token=" + URLEncoder.encode(sessionToken, "UTF-8");
                    if(site_id != null) {
                        Log.d("location id", site_id);
                        postData = postData.concat("&location_identifier=" + URLEncoder.encode(site_id, "UTF-8"));
                    }
                    myWebView.postUrl(url, postData.getBytes());
                } catch (UnsupportedEncodingException ex) {
                    Toast.makeText(PWAActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                    finish();
                }
            } else {
                Toast.makeText(PWAActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private final class  GetAzgoToken extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL u = new URL(ServerConstants.AZGO_TOKEN_URL);
                HttpURLConnection connection = (HttpURLConnection) u.openConnection();

                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("co.goapp.lib", Context.MODE_PRIVATE);
                String token =  sharedPref.getString("mex_app_token",null);

                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("X-ACCESS-TOKEN", token);

                String json_response = "";
                InputStreamReader in = new InputStreamReader(connection.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String text = "";
                while ((text = br.readLine()) != null) {
                    json_response += text;
                }

                JsonParser jsonParser = new JsonParser();
                try {
                    JsonObject jsonObject = (JsonObject) jsonParser.parse(json_response);
                    boolean isSuccess = jsonObject.get("success").getAsBoolean();

                    if (isSuccess) {
                        String sessionToken = jsonObject.get("sessionToken").getAsString();
                        return sessionToken;
                    } else {
                        return null;
                    }
                } catch (Exception ex) {
                    return null;
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String sessionToken) {
            if(sessionToken != null) {
                myWebView.loadUrl(ServerConstants.AZGO_WEB_URL_PREFIX+sessionToken);
            } else {
                //Something went wrong thing
            }
        }
    }

    private class CustomWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("tel:") || url.startsWith("sms:") || url.startsWith("smsto:") || url.startsWith("mms:") ||
                    url.startsWith("mmsto:") || url.startsWith("mailto:") )
            {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                return true;
            } else {
                return false;
            }
        }

        @Nullable
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
            return super.shouldInterceptRequest(view, request);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (myWebView.canGoBack()) {
                        myWebView.goBack();
                    }
                   else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }
}