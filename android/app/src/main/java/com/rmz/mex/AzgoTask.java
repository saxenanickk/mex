package com.rmz.mex;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;

import androidx.browser.customtabs.CustomTabsIntent;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class AzgoTask extends AsyncTask<Void, Void, String> {

    private Context mContext;
    CustomTabsIntent customTabsIntent;

    public AzgoTask(Context context) {
        mContext = context;
    }

    @Override
    protected String doInBackground(Void... params) {
        try {
            URL u = new URL(ServerConstants.AZGO_TOKEN_URL);
            HttpURLConnection connection = (HttpURLConnection) u.openConnection();

            SharedPreferences sharedPref = mContext.getSharedPreferences("co.goapp.lib", Context.MODE_PRIVATE);
            String token =  sharedPref.getString("mex_app_token",null);

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("X-ACCESS-TOKEN", token);

            String json_response = "";
            InputStreamReader in = new InputStreamReader(connection.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String text = "";
            while ((text = br.readLine()) != null) {
                json_response += text;
            }

            JsonParser jsonParser = new JsonParser();
            try {
                JsonObject jsonObject = (JsonObject) jsonParser.parse(json_response);
                boolean isSuccess = jsonObject.get("success").getAsBoolean();

                if (isSuccess) {
                    String sessionToken = jsonObject.get("sessionToken").getAsString();
                    return sessionToken;
                } else {
                    return null;
                }
            } catch (Exception ex) {
                return null;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String sessionToken) {
        if(sessionToken != null) {
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            builder.setShowTitle(true);
            customTabsIntent = builder.build();
            customTabsIntent.launchUrl(mContext, Uri.parse(ServerConstants.AZGO_WEB_URL_PREFIX+sessionToken));
        } else {
            //Something went wrong thing
        }
    }
}