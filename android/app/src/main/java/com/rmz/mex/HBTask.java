package com.rmz.mex;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

import androidx.browser.customtabs.CustomTabsIntent;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public   class HBTask extends AsyncTask<Void, Void, String> {

    private Context mContext;

    public HBTask(Context context) {
        mContext = context;
    }
    @Override
    protected String doInBackground(Void... params) {
        try {
            URL u = new URL(ServerConstants.HB_TOKEN_URL);
            HttpURLConnection connection = (HttpURLConnection) u.openConnection();

            SharedPreferences sharedPref = mContext.getSharedPreferences("co.goapp.lib", Context.MODE_PRIVATE);
            String token =  sharedPref.getString("mex_app_token",null);

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("x-access-token", token);

            String json_response = "";
            InputStreamReader in = new InputStreamReader(connection.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String text = "";
            while ((text = br.readLine()) != null) {
                json_response += text;
            }

            JsonParser jsonParser = new JsonParser();
            try {
                JsonObject jsonObject = (JsonObject) jsonParser.parse(json_response);
                int isSuccess = jsonObject.get("success").getAsInt();
                JsonObject data = jsonObject.getAsJsonObject("data");

                if (isSuccess == 1 && data != null && data.has("access_token")) {
                    String sessionToken = data.get("access_token").getAsString();
                    return sessionToken;
                } else {
                    return null;
                }
            } catch (Exception ex) {
                return null;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String sessionToken) {
        if(sessionToken != null) {
            try {
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(mContext, Uri.parse(ServerConstants.AZGO_WEB_URL_PREFIX+sessionToken));

                String url = ServerConstants.HB_URL_PREFIX;
                String postData = "access_token=" + URLEncoder.encode(sessionToken, "UTF-8");
//                myWebView.postUrl(url, postData.getBytes());
            } catch (UnsupportedEncodingException ex) {
//                Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        } else {
//            Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        }
    }
}