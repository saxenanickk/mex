package com.rmz.mex;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

public class GoLocationModule extends ReactContextBaseJavaModule implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {

    private GoogleApiClient googleApiClient;
    private FusedLocationProviderClient mFusedLocationClient;
    LocationCallback mLocationCallback;
    private Callback success;
    private Callback error;
    private ReadableMap options;

    private static Callback permissionSuccess;
    private static Callback permissionError;
    LocationRequest mLocationRequest;

    public GoLocationModule(ReactApplicationContext reactApplicationContext) {
        super(reactApplicationContext);

        //Setting up the location callback in case we have fetch the location
        mLocationRequest = createLocationRequest();
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                stopLocationUpdates();
                if (locationResult == null) {
                    locationNotReceived("LocationRequest failed");
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    // Update UI with location data
                    // ...
                    WritableMap params = Arguments.createMap();
                    WritableMap coords = Arguments.createMap();
                    coords.putDouble("latitude",location.getLatitude());
                    coords.putDouble("longitude",location.getLongitude());
                    params.putMap("coords",coords);
                    locationReceived(params);
                    return;
                }
            };
        };
    }

    @Override
    public String getName() {
        return "Location";
    }

    @ReactMethod
    public void getCurrentPosition(final Callback success,final Callback error) {
        getCurrentPosition3(success,error,null);
    }

    @ReactMethod
    public void getCurrentPosition3(final Callback success, final Callback error, ReadableMap options) {
        this.success = success;
        this.error = error;
        this.options = options;
        connectGoogleClient();
    }

    @ReactMethod
    private void checkLocationPermissions(final Callback success,final Callback error) {
        permissionSuccess = success;
        permissionError = error;
        if (getCurrentActivity() != null && ContextCompat.checkSelfPermission(getCurrentActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Log.d("Location","Location Is Granted");
            onPermissionSuccess();
        } else if(ContextCompat.checkSelfPermission(getCurrentActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("Location","requestPermissions");
            requestPermissions();
        }
    }

    private void requestPermissions() {
        if(getCurrentActivity() != null)
            ActivityCompat.requestPermissions(getCurrentActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    12);
    }

    public static void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.d("Location","onRequestPermissionsResult");

        if (requestCode == 12) {
            // Received permission result for camera permission.
            Log.i("GoLocationModule", "Received response for Location permission request.");

            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onPermissionSuccess();
            } else {
                onPermissionError();
            }
        }
    }

    private static void onPermissionError() {
        if(permissionError != null )
            permissionError.invoke("no-permission-given");
        permissionError = null;
    }

    private static void onPermissionSuccess() {
        if(permissionSuccess != null)
            permissionSuccess.invoke("permission-given");
        permissionSuccess = null;
    }

    private void connectGoogleClient() {
        googleApiClient = new GoogleApiClient.Builder(getCurrentActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        googleApiClient.connect();
    }

    protected LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }


    @SuppressLint("MissingPermission")
    protected void startLocationUpdates(){
        LocationServices.getFusedLocationProviderClient(getCurrentActivity()).requestLocationUpdates(mLocationRequest,mLocationCallback,null);
    }

    protected void stopLocationUpdates() {
        LocationServices.getFusedLocationProviderClient(getCurrentActivity()).removeLocationUpdates(mLocationCallback);
    }

    private void locationReceived(WritableMap params) {
        if(success != null) {
            success.invoke(params);
            success = null;
        }
        else if(error != null) {
            error.invoke("successIsNull");
            error = null;
        }
    }

    private void locationNotReceived(String errorString) {
        if(error != null) {
            error.invoke(errorString);
            error = null;
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getCurrentActivity());

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(getCurrentActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if(location == null) {
                            //call for real time update
                            startLocationUpdates();
                            return;
                        }
                        WritableMap params = Arguments.createMap();
                        WritableMap coords = Arguments.createMap();
                        coords.putDouble("latitude",location.getLatitude());
                        coords.putDouble("longitude",location.getLongitude());
                        params.putMap("coords",coords);
                        locationReceived(params);
                    }
                })
                .addOnFailureListener(getCurrentActivity(), new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        locationNotReceived(e.toString());
                    }
                });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if(error != null) {
            error.invoke("onConnectionFailed");
            error = null;
        }
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {

    }
}
