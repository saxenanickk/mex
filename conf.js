"use strict";
var md5 = require("md5");
var fs = require("fs");
var fileSave = -1;

function createModuleIdFactory(path) {
	const fileToIdMap = new Map();
	let nextId = 0;
	return path => {
		if (path.includes("node_modules")) {
			let id = fileToIdMap.get(path);
			if (typeof id !== "number") {
				id = nextId++;
				//console.log(path);
				fileToIdMap.set(path, id);
			}
			return parseInt(id);
		} else {
			//console.log(path);
			if (path.includes("GoAppImpl.js")) {
				console.log("Core modules md5 is :" + md5(path));
				if (fileSave == -1) {
					fileSave = 1;
					var filename = "commonModule.json";
					fs.writeFile(
						filename,
						'{"GoAppImpl" : "' + md5(path) + '"}',
						function(err) {
							if (err) {
								return console.log(err);
							}
						}
					);
				}
			}
			return md5(path);
		}
	};
}

module.exports = createModuleIdFactory;
